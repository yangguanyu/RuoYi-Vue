package com.ruoyi.web.controller.service.form.partner;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 找搭子对象 tfp_find_partner
 *
 * @author ruoyi
 * @date 2023-11-01
 */
@Data
@ApiModel("TfpFindPartnerForm-找搭子列表")
public class TfpFindPartnerForm
{
    private static final long serialVersionUID = 1L;


    /** 标题 */
    @ApiModelProperty("标题")
    private String title;

    /** 描述 */
    @ApiModelProperty("描述")
    private String activityDesc;

    /** 类型code */
    @ApiModelProperty("类型code")
    private String typeCode;

    /** 类型名称 */
    @ApiModelProperty("类型名称")
    private String typeName;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty("开始时间")
    private Date startTime;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty("开始时间-起")
    private Date startTimeStart;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty("开始时间-止")
    private Date startTimeEnd;

    /** 报名截止时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty("报名截止时间")
    private Date signUpEndTime;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty("报名截止时间-开始")
    private Date signUpEndTimeStart;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty("报名截止时间-截止")
    private Date signUpEndTimeEnd;

    /** 活动人数 */
    @ApiModelProperty("活动人数")
    private Long activityPersonNumber;

    /** 活动费用 */
    @ApiModelProperty("活动费用")
    private BigDecimal activityPrice;

    /** 活动地点 */
    @ApiModelProperty("活动地点")
    private String activityPlace;

    /** 找搭子发布和撤销等状态 0-未发布 1-审核通过 2-审核未通过 3-已发布待审核 4-因疑似非法而下架 */
    @ApiModelProperty("找搭子发布和撤销等状态 0-未发布 1-审核通过 2-审核未通过 3-已发布待审核 4-因疑似非法而下架")
    private Integer status;

    @ApiModelProperty("找搭子状态 1：找搭子进行中 2：即将发车 3:已结束")
    private Integer activityStatus;


}
