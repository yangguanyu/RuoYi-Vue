import request from '@/utils/request'

// 查询用户肖像信息列表
export function listUserPortraitInformation(query) {
  return request({
    url: '/service/userPortraitInformation/list',
    method: 'get',
    params: query
  })
}

// 查询用户肖像信息详细
export function getUserPortraitInformation(id) {
  return request({
    url: '/service/userPortraitInformation/' + id,
    method: 'get'
  })
}

// 新增用户肖像信息
export function addUserPortraitInformation(data) {
  return request({
    url: '/service/userPortraitInformation',
    method: 'post',
    data: data
  })
}

// 修改用户肖像信息
export function updateUserPortraitInformation(data) {
  return request({
    url: '/service/userPortraitInformation',
    method: 'put',
    data: data
  })
}

// 删除用户肖像信息
export function delUserPortraitInformation(id) {
  return request({
    url: '/service/userPortraitInformation/' + id,
    method: 'delete'
  })
}
