package com.ruoyi.service.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 经停信息对象 tfp_flight_stop_info
 * 
 * @author ruoyi
 * @date 2024-04-10
 */
public class TfpFlightStopInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 航班信息表id */
    @Excel(name = "航班信息表id")
    private Long flightId;

    /** 城市三字码 */
    @Excel(name = "城市三字码")
    private String city;

    /** 城市名称 */
    @Excel(name = "城市名称")
    private String cityName;

    /** 城市类型 */
    @Excel(name = "城市类型")
    private String cityType;

    /** 起飞时间 */
    @Excel(name = "起飞时间")
    private String depTime;

    /** 降落时间 */
    @Excel(name = "降落时间")
    private String arrTime;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    @Excel(name = "创建者id")
    private Long createUserId;

    /** 更新者id */
    @Excel(name = "更新者id")
    private Long updateUserId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setFlightId(Long flightId) 
    {
        this.flightId = flightId;
    }

    public Long getFlightId() 
    {
        return flightId;
    }
    public void setCity(String city) 
    {
        this.city = city;
    }

    public String getCity() 
    {
        return city;
    }
    public void setCityName(String cityName) 
    {
        this.cityName = cityName;
    }

    public String getCityName() 
    {
        return cityName;
    }
    public void setCityType(String cityType) 
    {
        this.cityType = cityType;
    }

    public String getCityType() 
    {
        return cityType;
    }
    public void setDepTime(String depTime)
    {
        this.depTime = depTime;
    }

    public String getDepTime()
    {
        return depTime;
    }
    public void setArrTime(String arrTime)
    {
        this.arrTime = arrTime;
    }

    public String getArrTime()
    {
        return arrTime;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }
    public void setUpdateUserId(Long updateUserId) 
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId() 
    {
        return updateUserId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("flightId", getFlightId())
            .append("city", getCity())
            .append("cityName", getCityName())
            .append("cityType", getCityType())
            .append("depTime", getDepTime())
            .append("arrTime", getArrTime())
            .append("delFlag", getDelFlag())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
