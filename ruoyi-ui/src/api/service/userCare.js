import request from '@/utils/request'

// 查询关注列表
export function listUserCare(query) {
  return request({
    url: '/service/userCare/list',
    method: 'get',
    params: query
  })
}

// 查询关注详细
export function getUserCare(id) {
  return request({
    url: '/service/userCare/' + id,
    method: 'get'
  })
}

// 新增关注
export function addUserCare(data) {
  return request({
    url: '/service/userCare',
    method: 'post',
    data: data
  })
}

// 修改关注
export function updateUserCare(data) {
  return request({
    url: '/service/userCare',
    method: 'put',
    data: data
  })
}

// 删除关注
export function delUserCare(id) {
  return request({
    url: '/service/userCare/' + id,
    method: 'delete'
  })
}

export function ownerUserSelect() {
  return request({
    url: '/service/userCare/ownerUserSelect',
    method: 'post'
  })
}

export function fansUserSelect() {
  return request({
    url: '/service/userCare/fansUserSelect',
    method: 'post'
  })
}
