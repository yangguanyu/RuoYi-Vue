package com.ruoyi.service.vo;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.List;

/**
 * 舱位信息对象 tfp_flight_seat
 * 
 * @author ruoyi
 * @date 2024-04-10
 */
@Data
public class TfpFlightSeatVo extends BaseEntity
{
    /** 主键 */
    private Long id;

    /** 航班信息表id */
    private Long flightId;

    /** 舱位编码 */
    private String seatCode;

    /** 儿童舱位编码 */
    private String chdSeatCode;

    /** 舱位状态 0-9 或者 英文字母 */
    private String seatStatus;

    /** 儿童舱位状态 1-9代表剩余座位数，A代表座位数大于9 */
    private String chdSeatStatus;

    /** 婴儿舱位编码 */
    private String infSeatCode;

    /** 婴儿舱位状态 1-9代表剩余座位数，A代表座位数大于9 */
    private String infSeatStatus;

    /** 服务级别 1-头等舱 2-商务舱 3-经济舱 4-舒适A舱 5-超级经济舱 6-高端经济舱 7-尊享经济 8-超值经济 9-超值公务 10-超值头等 11-舒适经济 12-优惠商务 13-明珠经济 14-超值商务 15-空中商务舱 16-公务舱产品舱 */
    private Integer cabinClass;

    /** 舱位说明 */
    private String seatMsg;

    /** 是否特价舱位 1-普通 3-特价 */
    private Integer seatType;

    /** 折扣 */
    private Double discount;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    private Long createUserId;

    /** 更新者id */
    private Long updateUserId;

    // 产品信息
    private TfpFlightPolicyVo policys;
    // 通程前后航段舱位对应信息
    private List<TfpFlightSeatDetailVo> seatDetails;
}
