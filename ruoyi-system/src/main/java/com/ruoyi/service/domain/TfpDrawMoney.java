package com.ruoyi.service.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 提款对象 tfp_draw_money
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public class TfpDrawMoney extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 用户银行卡id */
    @Excel(name = "用户银行卡id")
    private Long userBankCardId;

    /** 银行卡号 */
    @Excel(name = "银行卡号")
    private String bankCardNumber;

    /** 银行id */
    @Excel(name = "银行id")
    private String bankId;

    /** 银行名称 */
    @Excel(name = "银行名称")
    private String bankName;

    /** 提款金额 */
    @Excel(name = "提款金额")
    private BigDecimal drawMoneyPrice;

    /** 提现编号 */
    @Excel(name = "提现编号")
    private String drawMoneyNumber;

    /** 状态 0-提交申请/审核中 1-提现完成 2-提现失败/审核不通过 */
    @Excel(name = "状态 0-提交申请/审核中 1-提现完成 2-提现失败/审核不通过")
    private Integer status;

    /** 审核不通过原因 */
    @Excel(name = "审核不通过原因")
    private String reason;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    @Excel(name = "创建者id")
    private Long createUserId;

    /** 更新者id */
    @Excel(name = "更新者id")
    private Long updateUserId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setUserBankCardId(Long userBankCardId) 
    {
        this.userBankCardId = userBankCardId;
    }

    public Long getUserBankCardId() 
    {
        return userBankCardId;
    }
    public void setBankCardNumber(String bankCardNumber) 
    {
        this.bankCardNumber = bankCardNumber;
    }

    public String getBankCardNumber() 
    {
        return bankCardNumber;
    }
    public void setBankId(String bankId) 
    {
        this.bankId = bankId;
    }

    public String getBankId() 
    {
        return bankId;
    }
    public void setBankName(String bankName) 
    {
        this.bankName = bankName;
    }

    public String getBankName() 
    {
        return bankName;
    }
    public void setDrawMoneyPrice(BigDecimal drawMoneyPrice) 
    {
        this.drawMoneyPrice = drawMoneyPrice;
    }

    public BigDecimal getDrawMoneyPrice() 
    {
        return drawMoneyPrice;
    }
    public void setDrawMoneyNumber(String drawMoneyNumber) 
    {
        this.drawMoneyNumber = drawMoneyNumber;
    }

    public String getDrawMoneyNumber() 
    {
        return drawMoneyNumber;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setReason(String reason) 
    {
        this.reason = reason;
    }

    public String getReason() 
    {
        return reason;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }
    public void setUpdateUserId(Long updateUserId) 
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId() 
    {
        return updateUserId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("userBankCardId", getUserBankCardId())
            .append("bankCardNumber", getBankCardNumber())
            .append("bankId", getBankId())
            .append("bankName", getBankName())
            .append("drawMoneyPrice", getDrawMoneyPrice())
            .append("drawMoneyNumber", getDrawMoneyNumber())
            .append("status", getStatus())
            .append("reason", getReason())
            .append("delFlag", getDelFlag())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
