package com.ruoyi.common.enuma;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum SaleStatusEnum implements BaseEnum {

    PRE_SHELIVES(0, "待上架"),
    ON_SHELIVES(1, "已上架"),
    AUTH_SHELIVES(2, "下架待审批"),
    DOWN_SHELIVES(3, "已下架"),

    ;

    private Integer value;
    private String desc;
}
