package com.ruoyi.service.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel("DicDataVo")
public class DicDataVo {
    private Long dictCode;
    private Long parentCode;
    @ApiModelProperty("字典类型")
    private String type;
    @ApiModelProperty("字典键值")
    private String value;
    @ApiModelProperty("字典标签")
    private String label;
    @ApiModelProperty("字典排序")
    private String sort;
    @ApiModelProperty("字典信息")
    private List<DicDataVo> dicDataVoList;
}
