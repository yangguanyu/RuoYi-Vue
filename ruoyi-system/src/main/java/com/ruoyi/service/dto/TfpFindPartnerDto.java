package com.ruoyi.service.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 找搭子对象 tfp_find_partner
 *
 * @author ruoyi
 * @date 2023-11-01
 */
public class TfpFindPartnerDto extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    //查询我关注的搭子时使用
    private Long fansId;

    /** 标题 */
    private String title;

    private String phonenumber;

    /** 描述 */
    private String activityDesc;

    /** 类型code */
    private String typeCode;

    /** 类型名称 */
    private String typeName;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startTime;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startTimeStart;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startTimeEnd;

    /** 报名截止时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date signUpEndTime;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date signUpEndTimeStart;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date signUpEndTimeEnd;

    /** 活动人数 */
    private Long activityPersonNumber;

    /** 活动费用 */
    private BigDecimal activityPrice;

    /** 活动地点 */
    private String activityPlace;

    /** 找搭子发布和撤销等状态 0-未发布 1-审核通过 2-审核未通过 3-已发布待审核 4-因疑似非法而下架 */
    private Integer status;

    /** 找搭子发布和撤销等状态 0-未发布 1-审核通过 2-审核未通过 3-已发布待审核 4-因疑似非法而下架 */
    private Integer auditStatus;

    /** 审核不通过原因 */
    private String reason;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    private Long createUserId;

    /** 更新者id */
    private Long updateUserId;


    //找搭子状态 1：找搭子进行中 2：即将发车 3:已结束
    private Integer activityStatus;

    private Boolean activityRefresh;

    /** 活动地点详情 */
    private String activityPlaceDetail;


    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public Boolean getActivityRefresh() {
        return activityRefresh;
    }

    public void setActivityRefresh(Boolean activityRefresh) {
        this.activityRefresh = activityRefresh;
    }

    public Integer getActivityStatus() {
        return activityStatus;
    }

    public void setActivityStatus(Integer activityStatus) {
        this.activityStatus = activityStatus;
    }

    public Date getStartTimeStart() {
        return startTimeStart;
    }

    public void setStartTimeStart(Date startTimeStart) {
        this.startTimeStart = startTimeStart;
    }

    public Date getStartTimeEnd() {
        return startTimeEnd;
    }

    public void setStartTimeEnd(Date startTimeEnd) {
        this.startTimeEnd = startTimeEnd;
    }

    public Date getSignUpEndTimeStart() {
        return signUpEndTimeStart;
    }

    public void setSignUpEndTimeStart(Date signUpEndTimeStart) {
        this.signUpEndTimeStart = signUpEndTimeStart;
    }

    public Date getSignUpEndTimeEnd() {
        return signUpEndTimeEnd;
    }

    public void setSignUpEndTimeEnd(Date signUpEndTimeEnd) {
        this.signUpEndTimeEnd = signUpEndTimeEnd;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getTitle()
    {
        return title;
    }

    public String getActivityDesc() {
        return activityDesc;
    }

    public void setActivityDesc(String activityDesc) {
        this.activityDesc = activityDesc;
    }

    public void setTypeCode(String typeCode)
    {
        this.typeCode = typeCode;
    }

    public String getTypeCode()
    {
        return typeCode;
    }
    public void setTypeName(String typeName)
    {
        this.typeName = typeName;
    }

    public String getTypeName()
    {
        return typeName;
    }
    public void setStartTime(Date startTime)
    {
        this.startTime = startTime;
    }

    public Date getStartTime()
    {
        return startTime;
    }
    public void setSignUpEndTime(Date signUpEndTime)
    {
        this.signUpEndTime = signUpEndTime;
    }

    public Date getSignUpEndTime()
    {
        return signUpEndTime;
    }
    public void setActivityPersonNumber(Long activityPersonNumber)
    {
        this.activityPersonNumber = activityPersonNumber;
    }

    public Long getActivityPersonNumber()
    {
        return activityPersonNumber;
    }
    public void setActivityPrice(BigDecimal activityPrice)
    {
        this.activityPrice = activityPrice;
    }

    public BigDecimal getActivityPrice()
    {
        return activityPrice;
    }
    public void setActivityPlace(String activityPlace)
    {
        this.activityPlace = activityPlace;
    }

    public String getActivityPlace()
    {
        return activityPlace;
    }
    public void setStatus(Integer status)
    {
        this.status = status;
    }

    public Integer getStatus()
    {
        return status;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }
    public void setCreateUserId(Long createUserId)
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId()
    {
        return createUserId;
    }
    public void setUpdateUserId(Long updateUserId)
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId()
    {
        return updateUserId;
    }

    public Long getFansId() {
        return fansId;
    }

    public void setFansId(Long fansId) {
        this.fansId = fansId;
    }

    public String getActivityPlaceDetail() {
        return activityPlaceDetail;
    }

    public void setActivityPlaceDetail(String activityPlaceDetail) {
        this.activityPlaceDetail = activityPlaceDetail;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("title", getTitle())
                .append("activityDesc", getActivityDesc())
                .append("typeCode", getTypeCode())
                .append("typeName", getTypeName())
                .append("startTime", getStartTime())
                .append("startTimeStart", getStartTimeStart())
                .append("startTimeEnd", getStartTimeEnd())
                .append("signUpEndTime", getSignUpEndTime())
                .append("signUpEndTimeStart", getSignUpEndTimeStart())
                .append("signUpEndTimeEnd", getSignUpEndTimeEnd())
                .append("activityPersonNumber", getActivityPersonNumber())
                .append("activityPrice", getActivityPrice())
                .append("activityPlace", getActivityPlace())
                .append("activityPlaceDetail", getActivityPlaceDetail())
                .append("status", getStatus())
                .append("reason", getReason())
                .append("delFlag", getDelFlag())
                .append("createUserId", getCreateUserId())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateUserId", getUpdateUserId())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}
