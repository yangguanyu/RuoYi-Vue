package com.ruoyi.web.controller.service;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.service.dto.SaveUserBankCardInfoDto;
import com.ruoyi.service.vo.DetailUserBankCardVo;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.TfpUserBankCard;
import com.ruoyi.service.service.ITfpUserBankCardService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 用户银行卡Controller
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@RestController
@RequestMapping("/service/userBankCard")
public class TfpUserBankCardController extends BaseController
{
    @Autowired
    private ITfpUserBankCardService tfpUserBankCardService;

    /**
     * 查询用户银行卡列表
     */
    @PreAuthorize("@ss.hasPermi('service:userBankCard:list')")
    @GetMapping("/list")
    public TableDataInfo list(TfpUserBankCard tfpUserBankCard)
    {
        startPage();
        List<TfpUserBankCard> list = tfpUserBankCardService.selectTfpUserBankCardList(tfpUserBankCard);
        return getDataTable(list);
    }

    /**
     * 导出用户银行卡列表
     */
    @PreAuthorize("@ss.hasPermi('service:userBankCard:export')")
    @Log(title = "用户银行卡", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TfpUserBankCard tfpUserBankCard)
    {
        List<TfpUserBankCard> list = tfpUserBankCardService.selectTfpUserBankCardList(tfpUserBankCard);
        ExcelUtil<TfpUserBankCard> util = new ExcelUtil<TfpUserBankCard>(TfpUserBankCard.class);
        util.exportExcel(response, list, "用户银行卡数据");
    }

    /**
     * 获取用户银行卡详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:userBankCard:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tfpUserBankCardService.selectTfpUserBankCardById(id));
    }

    /**
     * 新增用户银行卡
     */
    @PreAuthorize("@ss.hasPermi('service:userBankCard:add')")
    @Log(title = "用户银行卡", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TfpUserBankCard tfpUserBankCard)
    {
        return toAjax(tfpUserBankCardService.insertTfpUserBankCard(tfpUserBankCard));
    }

    /**
     * 修改用户银行卡
     */
    @PreAuthorize("@ss.hasPermi('service:userBankCard:edit')")
    @Log(title = "用户银行卡", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TfpUserBankCard tfpUserBankCard)
    {
        return toAjax(tfpUserBankCardService.updateTfpUserBankCard(tfpUserBankCard));
    }

    /**
     * 删除用户银行卡
     */
    @PreAuthorize("@ss.hasPermi('service:userBankCard:remove')")
    @Log(title = "用户银行卡", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tfpUserBankCardService.deleteTfpUserBankCardByIds(ids));
    }

    @ApiOperation("小程序-保存用户银行卡信息(新增/编辑)")
    @PostMapping("/saveUserBankCardInfo")
    @ResponseBody
    public R saveUserBankCardInfo(@RequestBody SaveUserBankCardInfoDto saveUserBankCardInfoDto){
        return R.ok(tfpUserBankCardService.saveUserBankCardInfo(saveUserBankCardInfoDto));
    }

    @ApiOperation("小程序-用户银行卡详情")
    @PostMapping("/detailUserBankCard")
    @ResponseBody
    public R<DetailUserBankCardVo> detailUserBankCard(){
        return R.ok(tfpUserBankCardService.detailUserBankCard());
    }

    @ApiOperation("小程序-用户银行卡是否存在")
    @PostMapping("/existsUserBankCardFlag")
    @ResponseBody
    public R<Boolean> existsUserBankCardFlag(){
        return R.ok(tfpUserBankCardService.existsUserBankCardFlag());
    }
}
