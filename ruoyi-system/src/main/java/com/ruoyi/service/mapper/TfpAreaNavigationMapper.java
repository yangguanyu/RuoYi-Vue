package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.TfpAreaNavigation;
import org.apache.ibatis.annotations.Param;

/**
 * 地区导航Mapper接口
 * 
 * @author ruoyi
 * @date 2023-12-25
 */
public interface TfpAreaNavigationMapper 
{
    /**
     * 查询地区导航
     * 
     * @param id 地区导航主键
     * @return 地区导航
     */
    public TfpAreaNavigation selectTfpAreaNavigationById(Long id);

    /**
     * 查询地区导航列表
     * 
     * @param tfpAreaNavigation 地区导航
     * @return 地区导航集合
     */
    public List<TfpAreaNavigation> selectTfpAreaNavigationList(TfpAreaNavigation tfpAreaNavigation);

    /**
     * 新增地区导航
     * 
     * @param tfpAreaNavigation 地区导航
     * @return 结果
     */
    public int insertTfpAreaNavigation(TfpAreaNavigation tfpAreaNavigation);

    /**
     * 修改地区导航
     * 
     * @param tfpAreaNavigation 地区导航
     * @return 结果
     */
    public int updateTfpAreaNavigation(TfpAreaNavigation tfpAreaNavigation);

    /**
     * 删除地区导航
     * 
     * @param id 地区导航主键
     * @return 结果
     */
    public int deleteTfpAreaNavigationById(Long id);

    /**
     * 批量删除地区导航
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpAreaNavigationByIds(Long[] ids);

    /**
     * 查询目的地区域名称列表
     * @param key
     * @return
     */
    List<String> searchDestinationAreaNameByKey(@Param("key") String key);
}
