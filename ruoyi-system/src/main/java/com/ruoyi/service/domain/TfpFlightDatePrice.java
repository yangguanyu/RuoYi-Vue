package com.ruoyi.service.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 航班日期价格对象 tfp_flight_date_price
 * 
 * @author ruoyi
 * @date 2024-04-29
 */
public class TfpFlightDatePrice extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 价格(根据规则获取) */
    @Excel(name = "价格(根据规则获取)")
    private BigDecimal price;

    /** 航班日期(yyyy-MM-dd) */
    @Excel(name = "航班日期(yyyy-MM-dd)")
    private String departureTime;

    /** 起飞城市三字码 */
    @Excel(name = "起飞城市三字码")
    private String departureAirportCode;

    /** 起飞城市名称 */
    @Excel(name = "起飞城市名称")
    private String departureAirportName;

    /** 到达城市三字码 */
    @Excel(name = "到达城市三字码")
    private String arrivalAirportCode;

    /** 到达城市名称 */
    @Excel(name = "到达城市名称")
    private String arrivalAirportName;

    /** 航班id */
    @Excel(name = "航班id")
    private Long flightId;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    @Excel(name = "创建者id")
    private Long createUserId;

    /** 更新者id */
    @Excel(name = "更新者id")
    private Long updateUserId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setPrice(BigDecimal price)
    {
        this.price = price;
    }

    public BigDecimal getPrice()
    {
        return price;
    }
    public void setDepartureTime(String departureTime)
    {
        this.departureTime = departureTime;
    }

    public String getDepartureTime()
    {
        return departureTime;
    }
    public void setDepartureAirportCode(String departureAirportCode) 
    {
        this.departureAirportCode = departureAirportCode;
    }

    public String getDepartureAirportCode() 
    {
        return departureAirportCode;
    }
    public void setDepartureAirportName(String departureAirportName) 
    {
        this.departureAirportName = departureAirportName;
    }

    public String getDepartureAirportName() 
    {
        return departureAirportName;
    }
    public void setArrivalAirportCode(String arrivalAirportCode) 
    {
        this.arrivalAirportCode = arrivalAirportCode;
    }

    public String getArrivalAirportCode() 
    {
        return arrivalAirportCode;
    }
    public void setArrivalAirportName(String arrivalAirportName) 
    {
        this.arrivalAirportName = arrivalAirportName;
    }

    public String getArrivalAirportName() 
    {
        return arrivalAirportName;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }
    public void setUpdateUserId(Long updateUserId) 
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId() 
    {
        return updateUserId;
    }

    public Long getFlightId() {
        return flightId;
    }

    public void setFlightId(Long flightId) {
        this.flightId = flightId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("price", getPrice())
            .append("departureTime", getDepartureTime())
            .append("departureAirportCode", getDepartureAirportCode())
            .append("departureAirportName", getDepartureAirportName())
            .append("arrivalAirportCode", getArrivalAirportCode())
            .append("arrivalAirportName", getArrivalAirportName())
            .append("flightId", getFlightId())
            .append("delFlag", getDelFlag())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
