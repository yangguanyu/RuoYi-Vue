import request from '@/utils/request'

// 查询用户收益列表
export function listUserIncome(query) {
  return request({
    url: '/service/userIncome/list',
    method: 'get',
    params: query
  })
}

// 查询用户收益详细
export function getUserIncome(id) {
  return request({
    url: '/service/userIncome/' + id,
    method: 'get'
  })
}

// 新增用户收益
export function addUserIncome(data) {
  return request({
    url: '/service/userIncome',
    method: 'post',
    data: data
  })
}

// 修改用户收益
export function updateUserIncome(data) {
  return request({
    url: '/service/userIncome',
    method: 'put',
    data: data
  })
}

// 删除用户收益
export function delUserIncome(id) {
  return request({
    url: '/service/userIncome/' + id,
    method: 'delete'
  })
}
