package com.ruoyi.service.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
@ApiModel(value = "ProductSnapshotDetailVo", description = "商品快照详情")
public class ProductSnapshotDetailVo {
    @ApiModelProperty("商品id")
    private Long id;

    @ApiModelProperty("商品名称")
    private String productName;
    @ApiModelProperty("商品编号")
    private String productNo;
    @ApiModelProperty("价格")
    private BigDecimal price;
    @ApiModelProperty("服务类型id 1-全国散 2-一家一团 3-自营 4-全国散-小包团")
    private Long categoryId;
    @ApiModelProperty("服务类型名称")
    private String categoryName;
    @ApiModelProperty("想去人数")
    private Integer wantToGoPersonNumber;

    @ApiModelProperty("行程天数 几天")
    private Integer dayNum;
    @ApiModelProperty("行程天数 几晚")
    private Integer nightNum;
    @ApiModelProperty("出发地名称")
    private String goPlaceName;
    @ApiModelProperty("目的地")
    private String backPlaceName;
    @ApiModelProperty("商品归属")
    private Integer commoditiesType;
    @ApiModelProperty("商品归属名称")
    private String commoditiesTypeName;

    @ApiModelProperty("线路亮点集合")
    private List<String> routeHighlightList;

    @ApiModelProperty("是否想去 0-否 1-是")
    private Integer wantToGoFlag;

    @ApiModelProperty("聊天群组id")
    private Long chatGroupId;
    @ApiModelProperty("聊天群信息集合")
    private ChatGroupInfoVo chatGroupInfo;

    @ApiModelProperty("日期价格集合")
    private List<DateMonthVO> datePriceVoList;

    @ApiModelProperty("想去人数头像url集合")
    private List<String> wanToGoPersonHeadPortraitUrlList;

    @ApiModelProperty("商品图片url集合")
    private List<String> productImageUrlList;
    @ApiModelProperty("商品线路亮点url")
    private List<String> routeHighlightImageUrlList;
    @ApiModelProperty("商品线路亮点url--待删除")
    private String routeHighlightImageUrl;

    @ApiModelProperty("出行方式 0-组队出行 1-独立出行")
    private Integer travelType;
    @ApiModelProperty("出发时间")
    private String goDateStr;

    @ApiModelProperty("商品描述")
    private String productDescribe;
    @ApiModelProperty("是否选择低价机票 0-否 1-是")
    private Integer lowPriceAirTicketFlag;
    @ApiModelProperty("规则id")
    private Long ruleId;
    @ApiModelProperty("规则文案")
    private String text;
}
