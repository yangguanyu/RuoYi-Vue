package com.ruoyi.web.controller.service;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.service.vo.MyVisitorListVo;
import com.ruoyi.service.vo.VisitorVo;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.SysUserVisitorRecord;
import com.ruoyi.service.service.ISysUserVisitorRecordService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 访客记录Controller
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@RestController
@RequestMapping("/service/userVisitorRecord")
public class SysUserVisitorRecordController extends BaseController
{
    @Autowired
    private ISysUserVisitorRecordService sysUserVisitorRecordService;

    /**
     * 查询访客记录列表
     */
    @PreAuthorize("@ss.hasPermi('service:userVisitorRecord:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysUserVisitorRecord sysUserVisitorRecord)
    {
        startPage();
        List<SysUserVisitorRecord> list = sysUserVisitorRecordService.selectSysUserVisitorRecordList(sysUserVisitorRecord);
        return getDataTable(list);
    }

    /**
     * 导出访客记录列表
     */
    @PreAuthorize("@ss.hasPermi('service:userVisitorRecord:export')")
    @Log(title = "访客记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysUserVisitorRecord sysUserVisitorRecord)
    {
        List<SysUserVisitorRecord> list = sysUserVisitorRecordService.selectSysUserVisitorRecordList(sysUserVisitorRecord);
        ExcelUtil<SysUserVisitorRecord> util = new ExcelUtil<SysUserVisitorRecord>(SysUserVisitorRecord.class);
        util.exportExcel(response, list, "访客记录数据");
    }

    /**
     * 获取访客记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:userVisitorRecord:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(sysUserVisitorRecordService.selectSysUserVisitorRecordById(id));
    }

    /**
     * 新增访客记录
     */
    @PreAuthorize("@ss.hasPermi('service:userVisitorRecord:add')")
    @Log(title = "访客记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysUserVisitorRecord sysUserVisitorRecord)
    {
        return toAjax(sysUserVisitorRecordService.insertSysUserVisitorRecord(sysUserVisitorRecord));
    }

    /**
     * 修改访客记录
     */
    @PreAuthorize("@ss.hasPermi('service:userVisitorRecord:edit')")
    @Log(title = "访客记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysUserVisitorRecord sysUserVisitorRecord)
    {
        return toAjax(sysUserVisitorRecordService.updateSysUserVisitorRecord(sysUserVisitorRecord));
    }

    /**
     * 删除访客记录
     */
    @PreAuthorize("@ss.hasPermi('service:userVisitorRecord:remove')")
    @Log(title = "访客记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(sysUserVisitorRecordService.deleteSysUserVisitorRecordByIds(ids));
    }

    @ApiOperation("小程序-访客列表")
    @PostMapping("/visitorList")
    @ResponseBody
    public TableDataInfo<VisitorVo> visitorList(@RequestParam("userId") Long userId)
    {
        return getDataTable(sysUserVisitorRecordService.visitorList(userId));
    }
}
