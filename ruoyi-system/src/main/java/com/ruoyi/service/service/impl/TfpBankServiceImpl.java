package com.ruoyi.service.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpBankMapper;
import com.ruoyi.service.domain.TfpBank;
import com.ruoyi.service.service.ITfpBankService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 银行Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@Service
@Transactional
public class TfpBankServiceImpl implements ITfpBankService 
{
    @Autowired
    private TfpBankMapper tfpBankMapper;

    /**
     * 查询银行
     * 
     * @param id 银行主键
     * @return 银行
     */
    @Override
    public TfpBank selectTfpBankById(Long id)
    {
        return tfpBankMapper.selectTfpBankById(id);
    }

    /**
     * 查询银行列表
     * 
     * @param tfpBank 银行
     * @return 银行
     */
    @Override
    public List<TfpBank> selectTfpBankList(TfpBank tfpBank)
    {
        return tfpBankMapper.selectTfpBankList(tfpBank);
    }

    /**
     * 新增银行
     * 
     * @param tfpBank 银行
     * @return 结果
     */
    @Override
    public int insertTfpBank(TfpBank tfpBank)
    {
        tfpBank.setCreateTime(DateUtils.getNowDate());
        return tfpBankMapper.insertTfpBank(tfpBank);
    }

    /**
     * 修改银行
     * 
     * @param tfpBank 银行
     * @return 结果
     */
    @Override
    public int updateTfpBank(TfpBank tfpBank)
    {
        tfpBank.setUpdateTime(DateUtils.getNowDate());
        return tfpBankMapper.updateTfpBank(tfpBank);
    }

    /**
     * 批量删除银行
     * 
     * @param ids 需要删除的银行主键
     * @return 结果
     */
    @Override
    public int deleteTfpBankByIds(Long[] ids)
    {
        return tfpBankMapper.deleteTfpBankByIds(ids);
    }

    /**
     * 删除银行信息
     * 
     * @param id 银行主键
     * @return 结果
     */
    @Override
    public int deleteTfpBankById(Long id)
    {
        return tfpBankMapper.deleteTfpBankById(id);
    }
}
