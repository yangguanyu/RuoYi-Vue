package com.ruoyi.common.enuma;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum GoAndBackFlagEnum implements BaseEnum {
    GO(0, "去程"),
    BACK(1, "返程"),
    ;


    private Integer value;
    private String desc;
}
