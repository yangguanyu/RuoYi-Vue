package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel(value = "OperationShelivesDto")
public class OperationShelivesDto {
    @ApiModelProperty("商品id")
    @NotNull(message = "商品id不能为空")
    private Long id;
    @ApiModelProperty("上架状态 1-上架 3-下架")
    @NotNull(message = "上架状态不能为空")
    private Integer salesStatus;
}
