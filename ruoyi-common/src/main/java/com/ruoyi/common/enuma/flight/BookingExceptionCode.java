package com.ruoyi.common.enuma.flight;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum BookingExceptionCode {
    ORDER_CONTAINS_DISHONORED_PERSON("010321", "订单包含失信人"),
    POLICY_STATUS_INVALID("010310", "政策状态无效"),
    SEAT_INSUFFICIENT("010320", "座位不足"),
    RESTRICTED_IDENTITY_NUMBER("010323", "证件号码被限制，请联系法院！"),
    BASIC_POLICY_ID_REQUIRED("010312", "除单程、往返以外的航程只能传入基本政策ID"),
    PURCHASE_PRICE_INCONSISTENT("010301", "采购价格与平台价格不一致"),
    ALREADY_PAID_ORDER_WITH_SAME_CONDITIONS("010322", "存在相同条件已支付的订单"),
    INTERFACE不支持ROUND_TRIP("010311", "此接口不支持往返"),
    INSUFFICIENT_REMAINING_SEATS("010325", "余位不足！"),
    POLICY_ID_INVALID("010314", "政策ID无效"),
    ORDER_GENERATION_FAILED("010303", "订单生成失败！"),
    ONLY_IDENTITY_CARD_PURCHASE_SUPPORTED("010324", "只支持身份证购买！"),
    TICKET_FACE_VALUE_FEES_INCORRECT("010313", "票面价、机建、燃油不符"),
    ORDER_PRICE_VERIFICATION_FAILED("010302", "价格校验失败"),
    ADULT_MOBILE_NUMBER_REQUIRED("010316", "成人手机号不能为空"),
    CONFIGURATION_MISSING("010305", "没有配置"),
    PASSENGERS_WITH_SAME_NAME_OR_HOMOPHONE_CANNOT_BE_RESERVED("010315", "相同姓名或者姓名谐音相同的乘客不能预定在同一订单中。请分开预定，否则无法登机！"),
    COMPANION_MOBILE_NUMBER_REQUIRED("010318", "请输入陪同人手机号码"),
    ADULT_MOBILE_NUMBER_DUPLICATE("010317", "成人手机号不能重复"),
    MOBILE_NUMBER_FORMAT_ERROR("010319", "phoneNum 手机号格式错误");

    private String value;
    private String desc;

}

