package com.ruoyi.service.dto;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 自费项目对象 tfp_self_funded_project
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@Data
public class TfpSelfFundedProjectDto extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 商品表id */
    private Long productId;

    /** 地点名称 */
    private String areaName;

    /** 项目名称和内容 */
    private String nameAndContent;

    /** 费用（元） */
    private BigDecimal price;

    /** 项目时长（分钟） */
    private Long duration;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    private Long createUserId;

    /** 更新者id */
    private Long updateUserId;
}
