package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.SysIdentity;

/**
 * 用户身份Mapper接口
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public interface SysIdentityMapper 
{
    /**
     * 查询用户身份
     * 
     * @param id 用户身份主键
     * @return 用户身份
     */
    public SysIdentity selectSysIdentityById(Long id);

    /**
     * 查询用户身份列表
     * 
     * @param sysIdentity 用户身份
     * @return 用户身份集合
     */
    public List<SysIdentity> selectSysIdentityList(SysIdentity sysIdentity);

    /**
     * 新增用户身份
     * 
     * @param sysIdentity 用户身份
     * @return 结果
     */
    public int insertSysIdentity(SysIdentity sysIdentity);

    /**
     * 修改用户身份
     * 
     * @param sysIdentity 用户身份
     * @return 结果
     */
    public int updateSysIdentity(SysIdentity sysIdentity);

    /**
     * 删除用户身份
     * 
     * @param id 用户身份主键
     * @return 结果
     */
    public int deleteSysIdentityById(Long id);

    /**
     * 批量删除用户身份
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysIdentityByIds(Long[] ids);
}
