package com.ruoyi.web.controller.service;

import java.math.BigDecimal;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.service.vo.MySummoningEnvoyInfoVo;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.TfpUserIncome;
import com.ruoyi.service.service.ITfpUserIncomeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 用户收益Controller
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@RestController
@RequestMapping("/service/userIncome")
public class TfpUserIncomeController extends BaseController
{
    @Autowired
    private ITfpUserIncomeService tfpUserIncomeService;

    /**
     * 查询用户收益列表
     */
    @PreAuthorize("@ss.hasPermi('service:userIncome:list')")
    @GetMapping("/list")
    public TableDataInfo list(TfpUserIncome tfpUserIncome)
    {
        startPage();
        List<TfpUserIncome> list = tfpUserIncomeService.selectTfpUserIncomeList(tfpUserIncome);
        return getDataTable(list);
    }

    /**
     * 导出用户收益列表
     */
    @PreAuthorize("@ss.hasPermi('service:userIncome:export')")
    @Log(title = "用户收益", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TfpUserIncome tfpUserIncome)
    {
        List<TfpUserIncome> list = tfpUserIncomeService.selectTfpUserIncomeList(tfpUserIncome);
        ExcelUtil<TfpUserIncome> util = new ExcelUtil<TfpUserIncome>(TfpUserIncome.class);
        util.exportExcel(response, list, "用户收益数据");
    }

    /**
     * 获取用户收益详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:userIncome:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tfpUserIncomeService.selectTfpUserIncomeById(id));
    }

    /**
     * 新增用户收益
     */
    @PreAuthorize("@ss.hasPermi('service:userIncome:add')")
    @Log(title = "用户收益", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TfpUserIncome tfpUserIncome)
    {
        return toAjax(tfpUserIncomeService.insertTfpUserIncome(tfpUserIncome));
    }

    /**
     * 修改用户收益
     */
    @PreAuthorize("@ss.hasPermi('service:userIncome:edit')")
    @Log(title = "用户收益", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TfpUserIncome tfpUserIncome)
    {
        return toAjax(tfpUserIncomeService.updateTfpUserIncome(tfpUserIncome));
    }

    /**
     * 删除用户收益
     */
    @PreAuthorize("@ss.hasPermi('service:userIncome:remove')")
    @Log(title = "用户收益", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tfpUserIncomeService.deleteTfpUserIncomeByIds(ids));
    }

    @PostMapping("/saveNewInfo")
    @ResponseBody
    public R saveNewInfo(@RequestParam("userId") Long userId, @RequestParam("userName") String userName){
        return R.ok(tfpUserIncomeService.saveNewInfo(userId, userName));
    }

    @ApiOperation("小程序-我的召唤使者信息")
    @PostMapping("/mySummoningEnvoyInfo")
    @ResponseBody
    public R<MySummoningEnvoyInfoVo> mySummoningEnvoyInfo(){
        return R.ok(tfpUserIncomeService.mySummoningEnvoyInfo());
    }

    @ApiOperation("小程序-获取我的余额")
    @PostMapping("/getMyRemainingPrice")
    @ResponseBody
    public R<BigDecimal> getMyRemainingPrice(){
        return R.ok(tfpUserIncomeService.getMyRemainingPrice());
    }
}
