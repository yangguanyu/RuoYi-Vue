package com.ruoyi.web.controller.service;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.TfpAfterSalesFeedback;
import com.ruoyi.service.service.ITfpAfterSalesFeedbackService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 售后反馈Controller
 * 
 * @author ruoyi
 * @date 2023-11-01
 */
@RestController
@RequestMapping("/service/afterSalesFeedback")
public class TfpAfterSalesFeedbackController extends BaseController
{
    @Autowired
    private ITfpAfterSalesFeedbackService tfpAfterSalesFeedbackService;

    /**
     * 查询售后反馈列表
     */
    @PreAuthorize("@ss.hasPermi('service:afterSalesFeedback:list')")
    @GetMapping("/list")
    public TableDataInfo list(TfpAfterSalesFeedback tfpAfterSalesFeedback)
    {
        startPage();
        List<TfpAfterSalesFeedback> list = tfpAfterSalesFeedbackService.selectTfpAfterSalesFeedbackList(tfpAfterSalesFeedback);
        return getDataTable(list);
    }

    /**
     * 导出售后反馈列表
     */
    @PreAuthorize("@ss.hasPermi('service:afterSalesFeedback:export')")
    @Log(title = "售后反馈", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TfpAfterSalesFeedback tfpAfterSalesFeedback)
    {
        List<TfpAfterSalesFeedback> list = tfpAfterSalesFeedbackService.selectTfpAfterSalesFeedbackList(tfpAfterSalesFeedback);
        ExcelUtil<TfpAfterSalesFeedback> util = new ExcelUtil<TfpAfterSalesFeedback>(TfpAfterSalesFeedback.class);
        util.exportExcel(response, list, "售后反馈数据");
    }

    /**
     * 获取售后反馈详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:afterSalesFeedback:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tfpAfterSalesFeedbackService.selectTfpAfterSalesFeedbackById(id));
    }

    /**
     * 新增售后反馈
     */
    @PreAuthorize("@ss.hasPermi('service:afterSalesFeedback:add')")
    @Log(title = "售后反馈", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TfpAfterSalesFeedback tfpAfterSalesFeedback)
    {
        return toAjax(tfpAfterSalesFeedbackService.insertTfpAfterSalesFeedback(tfpAfterSalesFeedback));
    }

    /**
     * 修改售后反馈
     */
    @PreAuthorize("@ss.hasPermi('service:afterSalesFeedback:edit')")
    @Log(title = "售后反馈", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TfpAfterSalesFeedback tfpAfterSalesFeedback)
    {
        return toAjax(tfpAfterSalesFeedbackService.updateTfpAfterSalesFeedback(tfpAfterSalesFeedback));
    }

    /**
     * 删除售后反馈
     */
    @PreAuthorize("@ss.hasPermi('service:afterSalesFeedback:remove')")
    @Log(title = "售后反馈", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tfpAfterSalesFeedbackService.deleteTfpAfterSalesFeedbackByIds(ids));
    }
}
