package com.ruoyi.web.controller.service;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.utils.bean.BeanUtils;
import com.ruoyi.common.utils.bean.CustomizeBeanCopier;
import com.ruoyi.web.controller.service.form.banner.GeneralBannerForm;
import com.ruoyi.web.controller.vo.GeneralBannerVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.TfpGeneralBanner;
import com.ruoyi.service.service.ITfpGeneralBannerService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * bannerController
 *
 * @author ruoyi
 * @date 2023-11-01
 */
@RestController
@RequestMapping("/service/generalBanner")
@Api("Banner位")
public class TfpGeneralBannerController extends BaseController
{
    @Autowired
    private ITfpGeneralBannerService tfpGeneralBannerService;

    /**
     * 查询banner列表
     */
    @PreAuthorize("@ss.hasPermi('service:generalBanner:list')")
    @GetMapping("/list")
    public TableDataInfo list(TfpGeneralBanner tfpGeneralBanner)
    {
        startPage();
        List<TfpGeneralBanner> list = tfpGeneralBannerService.selectTfpGeneralBannerList(tfpGeneralBanner);
        return getDataTable(list);
    }

    /**
     * 导出banner列表
     */
    @PreAuthorize("@ss.hasPermi('service:generalBanner:export')")
    @Log(title = "banner", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TfpGeneralBanner tfpGeneralBanner)
    {
        List<TfpGeneralBanner> list = tfpGeneralBannerService.selectTfpGeneralBannerList(tfpGeneralBanner);
        ExcelUtil<TfpGeneralBanner> util = new ExcelUtil<TfpGeneralBanner>(TfpGeneralBanner.class);
        util.exportExcel(response, list, "banner数据");
    }

    /**
     * 获取banner详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:generalBanner:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tfpGeneralBannerService.selectTfpGeneralBannerById(id));
    }

    /**
     * 新增banner
     */
    @PreAuthorize("@ss.hasPermi('service:generalBanner:add')")
    @Log(title = "banner", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TfpGeneralBanner tfpGeneralBanner)
    {
        return toAjax(tfpGeneralBannerService.insertTfpGeneralBanner(tfpGeneralBanner));
    }

    /**
     * 修改banner
     */
    @PreAuthorize("@ss.hasPermi('service:generalBanner:edit')")
    @Log(title = "banner", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TfpGeneralBanner tfpGeneralBanner)
    {
        return toAjax(tfpGeneralBannerService.updateTfpGeneralBanner(tfpGeneralBanner));
    }

    /**
     * 删除banner
     */
    @PreAuthorize("@ss.hasPermi('service:generalBanner:remove')")
    @Log(title = "banner", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tfpGeneralBannerService.deleteTfpGeneralBannerByIds(ids));
    }

    @GetMapping("/applet/list")
    @ApiOperation("轮播图列表")
    public R<List<GeneralBannerVO>> appletList(GeneralBannerForm form)
    {
        TfpGeneralBanner tfpGeneralBanner = new TfpGeneralBanner();
        BeanUtils.copyProperties(form,tfpGeneralBanner);
        List<TfpGeneralBanner> list = tfpGeneralBannerService.selectTfpGeneralBannerList(tfpGeneralBanner);

        List<GeneralBannerVO> bannerVOList = CustomizeBeanCopier.copyPropertiesOfList(list, GeneralBannerVO.class);
        return R.ok(bannerVOList);
    }
}
