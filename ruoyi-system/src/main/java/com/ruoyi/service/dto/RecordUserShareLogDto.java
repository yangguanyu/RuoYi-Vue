package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("RecordUserShareLogDto")
public class RecordUserShareLogDto {
    @ApiModelProperty("类型 0-商品 1-搭子")
    private Integer type;
    @ApiModelProperty("对应业务id")
    private Long businessId;
}
