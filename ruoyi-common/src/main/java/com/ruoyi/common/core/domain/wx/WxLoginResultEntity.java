package com.ruoyi.common.core.domain.wx;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 *
 * @author
 * @date
 * @time
 */
@Data
public class WxLoginResultEntity {
    @ApiModelProperty("token令牌")
    private String wxToken;
}
