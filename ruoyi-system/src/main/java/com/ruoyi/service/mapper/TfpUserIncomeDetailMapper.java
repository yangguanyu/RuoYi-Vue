package com.ruoyi.service.mapper;

import java.math.BigDecimal;
import java.util.List;
import com.ruoyi.service.domain.TfpUserIncomeDetail;
import com.ruoyi.service.dto.ExportDto;
import com.ruoyi.service.dto.UserIncomeDetailManageListDto;
import com.ruoyi.service.vo.SettlementDetailListVo;
import com.ruoyi.service.vo.UserIncomeDetailListVo;
import com.ruoyi.service.vo.UserIncomeDetailManageListVo;
import com.ruoyi.service.vo.UserIncomeStatisticsInfo;
import org.apache.ibatis.annotations.Param;

/**
 * 用户收益明细Mapper接口
 * 
 * @author ruoyi
 * @date 2024-01-16
 */
public interface TfpUserIncomeDetailMapper 
{
    /**
     * 查询用户收益明细
     * 
     * @param id 用户收益明细主键
     * @return 用户收益明细
     */
    public TfpUserIncomeDetail selectTfpUserIncomeDetailById(Long id);

    /**
     * 查询用户收益明细列表
     * 
     * @param tfpUserIncomeDetail 用户收益明细
     * @return 用户收益明细集合
     */
    public List<TfpUserIncomeDetail> selectTfpUserIncomeDetailList(TfpUserIncomeDetail tfpUserIncomeDetail);

    /**
     * 新增用户收益明细
     * 
     * @param tfpUserIncomeDetail 用户收益明细
     * @return 结果
     */
    public int insertTfpUserIncomeDetail(TfpUserIncomeDetail tfpUserIncomeDetail);

    /**
     * 修改用户收益明细
     * 
     * @param tfpUserIncomeDetail 用户收益明细
     * @return 结果
     */
    public int updateTfpUserIncomeDetail(TfpUserIncomeDetail tfpUserIncomeDetail);

    /**
     * 删除用户收益明细
     * 
     * @param id 用户收益明细主键
     * @return 结果
     */
    public int deleteTfpUserIncomeDetailById(Long id);

    /**
     * 批量删除用户收益明细
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpUserIncomeDetailByIds(Long[] ids);

    TfpUserIncomeDetail getTfpUserIncomeDetailBySourceInfoOfOrderNumberIsNull(TfpUserIncomeDetail tfpUserIncomeDetail);

    BigDecimal getCumulativeIncomeByUserId(@Param("userId") Long userId);

    List<UserIncomeDetailListVo> getUserIncomeDetailListByUserId(@Param("userId") Long userId);

    List<TfpUserIncomeDetail> getSettlementDetailList(@Param("userId") Long userId, @Param("settlementType") Integer settlementType);

    List<UserIncomeStatisticsInfo> selectViewProductList(@Param("userId") Long userId, @Param("productIdList") List<Long> productIdList);

    List<UserIncomeStatisticsInfo> selectPlaceOrderProductList(@Param("userId") Long userId, @Param("productIdList") List<Long> productIdList);

    List<SettlementDetailListVo> settlementDetailList(@Param("userId") Long userId, @Param("settlementType") Integer settlementType, @Param("defineOrderIdList") List<Long> defineOrderIdList);

    List<UserIncomeDetailManageListVo> userIncomeDetailManageList(@Param("userIncomeDetailManageListDto") UserIncomeDetailManageListDto userIncomeDetailManageListDto, @Param("productIdList") List<Long> productIdList, @Param("orderIdList") List<Long> orderIdList);

    List<UserIncomeDetailManageListVo> exportUserIncomeDetailManageList(@Param("exportDto") ExportDto exportDto, @Param("orderIdList") List<Long> orderIdList);

    List<TfpUserIncomeDetail> selectOverRefundTimeCompleteSettlementList(@Param("orderIdList") List<Long> orderIdList);
}
