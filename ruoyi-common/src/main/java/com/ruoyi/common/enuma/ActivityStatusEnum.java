package com.ruoyi.common.enuma;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 角色类型
 *
 * @author
 * @date
 * @time
 */
@Getter
@AllArgsConstructor
public enum ActivityStatusEnum implements BaseEnum {



    ING(1, "进行中"),

    REARY_GO(2, "即将发车"),

    FINISH(3, "已结束"),


    ;


    private Integer value;
    private String desc;
}
