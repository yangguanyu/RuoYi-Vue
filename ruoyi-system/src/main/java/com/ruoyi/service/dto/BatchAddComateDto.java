package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "BatchAddComateDto", description = "添加同行人dto")
public class BatchAddComateDto {
    @ApiModelProperty("同行人姓名")
    private String mateName;
    @ApiModelProperty("证件类型 1.身份证 2.台胞证 3.港澳通行证 4.护照 5.士兵证")
    private Integer mateCardType;
    @ApiModelProperty("证件号码")
    private String mateCardNum;
    @ApiModelProperty("手机号码")
    private String matePhone;
}
