package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("ListFindPartnerSignUpDto")
public class ListFindPartnerSignUpDto {
    @ApiModelProperty("找搭子id")
    private Long findPartnerId;
    @ApiModelProperty("状态 0-待确认 1-已确认")
    private Integer status;
}
