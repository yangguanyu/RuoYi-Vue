package com.ruoyi.service.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.service.domain.TfpChatGroupPerson;

/**
 * 聊天群组成员Service接口
 *
 * @author ruoyi
 * @date 2023-10-31
 */
public interface ITfpChatGroupPersonService
{
    /**
     * 查询聊天群组成员
     *
     * @param id 聊天群组成员主键
     * @return 聊天群组成员
     */
    public TfpChatGroupPerson selectTfpChatGroupPersonById(Long id);

    /**
     * 查询聊天群组成员列表
     *
     * @param tfpChatGroupPerson 聊天群组成员
     * @return 聊天群组成员集合
     */
    public List<TfpChatGroupPerson> selectTfpChatGroupPersonList(TfpChatGroupPerson tfpChatGroupPerson);


    /**
     * 查看已报名的群组成员
     *
     * @param groupIds 群组id
     * @return 聊天群组成员
     */
    public List<TfpChatGroupPerson> selectListByGroupIdAndSignUp(List<Long> groupIds);


    /**
     * 通过用户id和群组id查看当前人在群组的信息
     * @param userId
     * @param groupId
     * @return
     */
    public TfpChatGroupPerson selectOneByUserIdAndGroupId(Long userId,Long groupId);


    /**
     * 新增聊天群组成员
     *
     * @param tfpChatGroupPerson 聊天群组成员
     * @return 结果
     */
    public int insertTfpChatGroupPerson(TfpChatGroupPerson tfpChatGroupPerson);

    /**
     * 小程序在-我想去-的时候进行添加，同时还需要在我想去那张表添加记录
     *
     * @param tfpChatGroupPerson 聊天群组成员
     * @return 结果
     */
    public int appletInsert(TfpChatGroupPerson tfpChatGroupPerson);
    /**
     * 修改聊天群组成员
     *
     * @param tfpChatGroupPerson 聊天群组成员
     * @return 结果
     */
    public int updateTfpChatGroupPerson(TfpChatGroupPerson tfpChatGroupPerson);

    /**
     * 批量删除聊天群组成员
     *
     * @param ids 需要删除的聊天群组成员主键集合
     * @return 结果
     */
    public int deleteTfpChatGroupPersonByIds(Long[] ids);

    /**
     * 删除聊天群组成员信息
     *
     * @param id 聊天群组成员主键
     * @return 结果
     */
    public int deleteTfpChatGroupPersonById(Long id);

    List<TfpChatGroupPerson> selectTfpChatGroupPersonByIdList(List<Long> idList);

    Map<Long,Integer>  selectCountPersonByGroupIdList(List<Long> groupIds);

    List<Long> selectChatGroupPersonIdByUserId(Long userId);

    List<TfpChatGroupPerson> selectTfpChatGroupPersonListByGroupIdList(List<Long> groupIdList);

    List<TfpChatGroupPerson> getSignUpPersonInfoListByProductIdList(List<Long> productIdList);

    TfpChatGroupPerson selectTfpChatGroupPersonByGroupIdAndUserId(Long groupId, Long userId);
}
