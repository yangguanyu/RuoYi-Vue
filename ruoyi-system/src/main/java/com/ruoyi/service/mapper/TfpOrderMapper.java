package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.TfpOrder;
import com.ruoyi.service.dto.ExportDto;
import com.ruoyi.service.dto.OrderListDto;
import com.ruoyi.service.dto.TfpOrderDto;
import com.ruoyi.service.vo.MyOrderListVo;
import com.ruoyi.service.vo.OrderListVo;
import com.ruoyi.service.vo.TfpOrderVo;
import org.apache.ibatis.annotations.Param;

/**
 * 订单Mapper接口
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public interface TfpOrderMapper 
{
    /**
     * 查询订单
     * 
     * @param id 订单主键
     * @return 订单
     */
    public TfpOrder selectTfpOrderById(Long id);

    /**
     * 查询订单列表
     * 
     * @param tfpOrder 订单
     * @return 订单集合
     */
    public List<TfpOrder> selectTfpOrderList(TfpOrder tfpOrder);

    /**
     * 新增订单
     * 
     * @param tfpOrder 订单
     * @return 结果
     */
    public int insertTfpOrder(TfpOrder tfpOrder);

    /**
     * 修改订单
     * 
     * @param tfpOrder 订单
     * @return 结果
     */
    public int updateTfpOrder(TfpOrder tfpOrder);

    /**
     * 删除订单
     * 
     * @param id 订单主键
     * @return 结果
     */
    public int deleteTfpOrderById(Long id);

    /**
     * 批量删除订单
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpOrderByIds(Long[] ids);

    List<Long> selectSupplierIdList();

    List<TfpOrderVo> selectTfpOrderVoList(TfpOrderDto tfpOrderDto);

    TfpOrder selectTfpOrder(TfpOrder tfpOrder);

    List<TfpOrder> selectListByProductIdList(@Param("productIdList") List<Long> productIdList, @Param("statusList") List<Integer> statusList);

    TfpOrder selectUserEffectiveOrder(TfpOrder tfpOrderDto);

    List<TfpOrder> selectTfpOrderByIdList(@Param("idList") List<Long> idList);

    TfpOrder selectTfpOrderByOrderNo(@Param("orderNo") String orderNo);

    List<TfpOrder> selectExpireOrderList();

    List<MyOrderListVo> myOrderList(@Param("userId") Long userId, @Param("status") Integer status);

    List<TfpOrder> selectNotDownShelivesOrderList(@Param("productId") Long productId);

    List<OrderListVo> orderList(OrderListDto orderListDto);

    List<TfpOrder> queryUserIncomeDetailOrderList(@Param("productIdList") List<Long> productIdList, @Param("afterSalesTimeFlag") Integer afterSalesTimeFlag);

    List<OrderListVo> orderListExport(ExportDto exportDto);

    List<TfpOrder> queryOverRefundTimeCompleteOrderList(@Param("shareUserId") Long shareUserId);

    List<TfpOrder> selectCanCompleteOrderList();

    List<TfpOrder> selectOverRefundTimeCompleteOrderList();
}
