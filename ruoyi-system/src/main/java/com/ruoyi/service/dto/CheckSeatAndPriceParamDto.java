package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "CheckSeatAndPriceParamDto")
public class CheckSeatAndPriceParamDto {
    @ApiModelProperty("航程类型 OW 单程，RT 往返，MS 多程？ 联程？ 缺口 目前仅支持单程")
    private String routeType;
    @ApiModelProperty("产品ID")
    private String policyId;
    @ApiModelProperty("产品类型")
    private Integer productType;
    @ApiModelProperty("航程航线数据")
    private List<CheckSeatAndPriceParamSegmentDto> segmentList;
}
