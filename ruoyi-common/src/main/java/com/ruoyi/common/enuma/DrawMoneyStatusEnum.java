package com.ruoyi.common.enuma;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum DrawMoneyStatusEnum implements BaseEnum {
    APPLY(0, "提交申请/审核中"),
    COMPLETE(1, "提现完成"),
    FAIL(2, "提现失败/审核不通过"),
    ;

    private Integer value;
    private String desc;
}
