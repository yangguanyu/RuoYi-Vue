package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.TfpCustomerService;

/**
 * 客服Mapper接口
 * 
 * @author ruoyi
 * @date 2023-11-01
 */
public interface TfpCustomerServiceMapper 
{
    /**
     * 查询客服
     * 
     * @param id 客服主键
     * @return 客服
     */
    public TfpCustomerService selectTfpCustomerServiceById(Long id);

    /**
     * 查询客服列表
     * 
     * @param tfpCustomerService 客服
     * @return 客服集合
     */
    public List<TfpCustomerService> selectTfpCustomerServiceList(TfpCustomerService tfpCustomerService);

    /**
     * 新增客服
     * 
     * @param tfpCustomerService 客服
     * @return 结果
     */
    public int insertTfpCustomerService(TfpCustomerService tfpCustomerService);

    /**
     * 修改客服
     * 
     * @param tfpCustomerService 客服
     * @return 结果
     */
    public int updateTfpCustomerService(TfpCustomerService tfpCustomerService);

    /**
     * 删除客服
     * 
     * @param id 客服主键
     * @return 结果
     */
    public int deleteTfpCustomerServiceById(Long id);

    /**
     * 批量删除客服
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpCustomerServiceByIds(Long[] ids);
}
