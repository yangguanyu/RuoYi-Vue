package com.ruoyi.service.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 找搭子聊天群组成员聊天信息对象 tfp_find_partner_chat_group_person_info
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public class TfpFindPartnerChatGroupPersonInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 聊天群组id */
    @Excel(name = "聊天群组id")
    private Long chatGroupId;

    /** 聊天群组成员id */
    @Excel(name = "聊天群组成员id")
    private Long chatGroupPersonId;

    /** 聊天群组成员用户Id */
    @Excel(name = "聊天群组成员用户Id")
    private Long chatGroupPersonUserId;

    /** 聊天信息 */
    @Excel(name = "聊天信息")
    private String chatInfo;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    @Excel(name = "创建者id")
    private Long createUserId;

    /** 更新者id */
    @Excel(name = "更新者id")
    private Long updateUserId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setChatGroupId(Long chatGroupId) 
    {
        this.chatGroupId = chatGroupId;
    }

    public Long getChatGroupId() 
    {
        return chatGroupId;
    }
    public void setChatGroupPersonId(Long chatGroupPersonId) 
    {
        this.chatGroupPersonId = chatGroupPersonId;
    }

    public Long getChatGroupPersonId() 
    {
        return chatGroupPersonId;
    }
    public void setChatInfo(String chatInfo) 
    {
        this.chatInfo = chatInfo;
    }

    public String getChatInfo() 
    {
        return chatInfo;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }
    public void setUpdateUserId(Long updateUserId) 
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId() 
    {
        return updateUserId;
    }

    public Long getChatGroupPersonUserId() {
        return chatGroupPersonUserId;
    }

    public void setChatGroupPersonUserId(Long chatGroupPersonUserId) {
        this.chatGroupPersonUserId = chatGroupPersonUserId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("chatGroupId", getChatGroupId())
            .append("chatGroupPersonId", getChatGroupPersonId())
            .append("chatGroupPersonUserId", getChatGroupPersonUserId())
            .append("chatInfo", getChatInfo())
            .append("delFlag", getDelFlag())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
