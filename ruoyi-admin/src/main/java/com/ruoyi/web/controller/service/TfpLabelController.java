package com.ruoyi.web.controller.service;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.TfpLabel;
import com.ruoyi.service.service.ITfpLabelService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 标签Controller
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@RestController
@RequestMapping("/service/label")
public class TfpLabelController extends BaseController
{
    @Autowired
    private ITfpLabelService tfpLabelService;

    /**
     * 查询标签列表
     */
    @PreAuthorize("@ss.hasPermi('service:label:list')")
    @GetMapping("/list")
    public TableDataInfo list(TfpLabel tfpLabel)
    {
        startPage();
        List<TfpLabel> list = tfpLabelService.selectTfpLabelList(tfpLabel);
        return getDataTable(list);
    }

    /**
     * 导出标签列表
     */
    @PreAuthorize("@ss.hasPermi('service:label:export')")
    @Log(title = "标签", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TfpLabel tfpLabel)
    {
        List<TfpLabel> list = tfpLabelService.selectTfpLabelList(tfpLabel);
        ExcelUtil<TfpLabel> util = new ExcelUtil<TfpLabel>(TfpLabel.class);
        util.exportExcel(response, list, "标签数据");
    }

    /**
     * 获取标签详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:label:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tfpLabelService.selectTfpLabelById(id));
    }

    /**
     * 新增标签
     */
    @PreAuthorize("@ss.hasPermi('service:label:add')")
    @Log(title = "标签", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TfpLabel tfpLabel)
    {
        return toAjax(tfpLabelService.insertTfpLabel(tfpLabel));
    }

    /**
     * 修改标签
     */
    @PreAuthorize("@ss.hasPermi('service:label:edit')")
    @Log(title = "标签", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TfpLabel tfpLabel)
    {
        return toAjax(tfpLabelService.updateTfpLabel(tfpLabel));
    }

    /**
     * 删除标签
     */
    @PreAuthorize("@ss.hasPermi('service:label:remove')")
    @Log(title = "标签", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tfpLabelService.deleteTfpLabelByIds(ids));
    }

    @PreAuthorize("@ss.hasPermi('service:label:query')")
    @GetMapping(value = "/getLabelInfoByType/{labelType}")
    public AjaxResult getLabelInfoByType(@PathVariable("labelType") Long labelType)
    {
        return success(tfpLabelService.getLabelInfoByType(labelType));
    }
}
