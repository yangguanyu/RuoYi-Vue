package com.ruoyi.service.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
@ApiModel(value = "FindPartnerDetailAppletVo", description = "找搭子详情-小程序")
public class FindPartnerDetailAppletVo {

    @ApiModelProperty("主键")
    private Long id;

    /** 标题 */
    @ApiModelProperty(value = "标题")
    private String title;

    /** 描述 */
    @ApiModelProperty(value = "描述")
    private String activityDesc;

    /** 类型code */
    @ApiModelProperty(value = "类型code")
    private String typeCode;

    /** 类型名称 */
    @ApiModelProperty(value = "类型名称")
    private String typeName;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "开始时间")
    private Date startTime;

    /** 报名截止时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "报名截止时间")
    private Date signUpEndTime;

    /** 活动人数 */
    @ApiModelProperty(value = "活动人数")
    private Long activityPersonNumber;

    /** 活动费用 */
    @ApiModelProperty(value = "活动费用")
    private BigDecimal activityPrice;

    /** 活动地点 */
    @ApiModelProperty(value = "活动地点")
    private String activityPlace;

    /** 找搭子发布和撤销等状态 0-未发布 1-审核通过 2-审核未通过 3-已发布待审核 4-因疑似非法而下架 */
    @ApiModelProperty(value = "找搭子发布和撤销等状态 0-未发布 1-审核通过 2-审核未通过 3-已发布待审核 4-因疑似非法而下架")
    private Integer status;

    @ApiModelProperty("搭子聊天群组id")
    private Long findPartnerChatGroupId;

    @ApiModelProperty("搭子聊天群组名称")
    private String findPartnerChatGroupName;

    @ApiModelProperty("搭子报名成员")
    private List<UserVo> userList;

    @ApiModelProperty("图片列表")
    private List<AttachmentVO> attachmentList;

    @ApiModelProperty("创建搭子用户id")
    private Long userId;
    @ApiModelProperty("创建搭子用户昵称")
    private String nickName;
    @ApiModelProperty("创建搭子用户头像")
    private String userHeadImage;
    @ApiModelProperty("是否关注创建搭子用户 0-否 1-是")
    private Integer userCareFlag;

    @ApiModelProperty("开始时间格式化")
    private String startTimeFormat;

    @ApiModelProperty("报名截止时间格式化")
    private String signUpEndTimeFormat;

    //找搭子状态 1：找搭子进行中 2：即将发车 3:已结束
    @ApiModelProperty("找搭子状态 1：找搭子进行中 2：即将发车 3:已结束")
    private Integer activityStatus;

    @ApiModelProperty("我是否报名 0-否 1-是")
    private Integer mySignUpFlag;

    @ApiModelProperty(value = "活动地点详情")
    private String activityPlaceDetail;

    //价格类型编码，免费。线下付，发起人代付
    @ApiModelProperty(value = "价格类型编码，免费，线下付，发起人代付，前端传给我数字")
    private Long priceCode;
    @ApiModelProperty(value = "价格类型编码名称")
    private String priceCodeName;

    //报名人数
    private Integer signUpCount=0;
}
