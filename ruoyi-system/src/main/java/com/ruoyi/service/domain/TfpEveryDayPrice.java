package com.ruoyi.service.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 每日价格对象 tfp_every_day_price
 * 
 * @author ruoyi
 * @date 2023-12-12
 */
public class TfpEveryDayPrice extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 商品id */
    @Excel(name = "商品id")
    private Long productId;

    /** 类型 0-标准 1-特殊(特殊日期) */
    @Excel(name = "类型 0-标准 1-特殊(特殊日期)")
    private Integer type;

    /** 特殊日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "特殊日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date specialDate;

    /** 计划出团人数 */
    @Excel(name = "计划出团人数")
    private Integer expectedPersonNumber;

    /** 预收数 */
    @Excel(name = "预收数")
    private Integer expectedNumber;

    /** 最低成团人数 */
    @Excel(name = "最低成团人数")
    private Integer minPersonNumber;

    /** 成人成本价 */
    @Excel(name = "成人成本价")
    private BigDecimal adultCostPrice;

    /** 儿童成本价 */
    @Excel(name = "儿童成本价")
    private BigDecimal childrenCostPrice;

    /** 建议成人零售价 */
    @Excel(name = "建议成人零售价")
    private BigDecimal adultRetailPrice;

    /** 建议儿童零售价 */
    @Excel(name = "建议儿童零售价")
    private BigDecimal childrenRetailPrice;

    /** 单房差成本价 */
    @Excel(name = "单房差成本价")
    private BigDecimal singleSupplementCostPrice;

    /** 订金 */
    @Excel(name = "订金")
    private BigDecimal deposit;

    /** 自备签成本价 */
    @Excel(name = "自备签成本价")
    private BigDecimal selfPreparedSignatureCostPrice;

    /** 升舱成本价 */
    @Excel(name = "升舱成本价")
    private BigDecimal upgradeCostPrice;

    /** 拒签成本价 */
    @Excel(name = "拒签成本价")
    private BigDecimal refusalCostPrice;

    /** 签证费成本价 */
    @Excel(name = "签证费成本价")
    private BigDecimal visaFeeCostPrice;

    /** 特殊加项成本价 */
    @Excel(name = "特殊加项成本价")
    private BigDecimal specialAdditionCostPrice;

    /** 特殊减项成本价 */
    @Excel(name = "特殊减项成本价")
    private BigDecimal specialDeductionsCostPrice;

    /** 学生成本价 */
    @Excel(name = "学生成本价")
    private BigDecimal studentCostPrice;

    /** 老年人成本价 */
    @Excel(name = "老年人成本价")
    private BigDecimal oldPeopleCostPrice;

    /** 起点价格 */
    @Excel(name = "起点价格")
    private BigDecimal startingPointPrice;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    @Excel(name = "创建者id")
    private Long createUserId;

    /** 更新者id */
    @Excel(name = "更新者id")
    private Long updateUserId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setProductId(Long productId) 
    {
        this.productId = productId;
    }

    public Long getProductId() 
    {
        return productId;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }
    public void setSpecialDate(Date specialDate) 
    {
        this.specialDate = specialDate;
    }

    public Date getSpecialDate() 
    {
        return specialDate;
    }
    public void setExpectedPersonNumber(Integer expectedPersonNumber)
    {
        this.expectedPersonNumber = expectedPersonNumber;
    }

    public Integer getExpectedPersonNumber()
    {
        return expectedPersonNumber;
    }
    public void setExpectedNumber(Integer expectedNumber)
    {
        this.expectedNumber = expectedNumber;
    }

    public Integer getExpectedNumber()
    {
        return expectedNumber;
    }
    public void setMinPersonNumber(Integer minPersonNumber)
    {
        this.minPersonNumber = minPersonNumber;
    }

    public Integer getMinPersonNumber()
    {
        return minPersonNumber;
    }
    public void setAdultCostPrice(BigDecimal adultCostPrice) 
    {
        this.adultCostPrice = adultCostPrice;
    }

    public BigDecimal getAdultCostPrice() 
    {
        return adultCostPrice;
    }
    public void setChildrenCostPrice(BigDecimal childrenCostPrice) 
    {
        this.childrenCostPrice = childrenCostPrice;
    }

    public BigDecimal getChildrenCostPrice() 
    {
        return childrenCostPrice;
    }
    public void setAdultRetailPrice(BigDecimal adultRetailPrice) 
    {
        this.adultRetailPrice = adultRetailPrice;
    }

    public BigDecimal getAdultRetailPrice() 
    {
        return adultRetailPrice;
    }
    public void setChildrenRetailPrice(BigDecimal childrenRetailPrice) 
    {
        this.childrenRetailPrice = childrenRetailPrice;
    }

    public BigDecimal getChildrenRetailPrice() 
    {
        return childrenRetailPrice;
    }
    public void setSingleSupplementCostPrice(BigDecimal singleSupplementCostPrice) 
    {
        this.singleSupplementCostPrice = singleSupplementCostPrice;
    }

    public BigDecimal getSingleSupplementCostPrice() 
    {
        return singleSupplementCostPrice;
    }
    public void setDeposit(BigDecimal deposit) 
    {
        this.deposit = deposit;
    }

    public BigDecimal getDeposit() 
    {
        return deposit;
    }
    public void setSelfPreparedSignatureCostPrice(BigDecimal selfPreparedSignatureCostPrice) 
    {
        this.selfPreparedSignatureCostPrice = selfPreparedSignatureCostPrice;
    }

    public BigDecimal getSelfPreparedSignatureCostPrice() 
    {
        return selfPreparedSignatureCostPrice;
    }
    public void setUpgradeCostPrice(BigDecimal upgradeCostPrice) 
    {
        this.upgradeCostPrice = upgradeCostPrice;
    }

    public BigDecimal getUpgradeCostPrice() 
    {
        return upgradeCostPrice;
    }
    public void setRefusalCostPrice(BigDecimal refusalCostPrice) 
    {
        this.refusalCostPrice = refusalCostPrice;
    }

    public BigDecimal getRefusalCostPrice() 
    {
        return refusalCostPrice;
    }
    public void setVisaFeeCostPrice(BigDecimal visaFeeCostPrice) 
    {
        this.visaFeeCostPrice = visaFeeCostPrice;
    }

    public BigDecimal getVisaFeeCostPrice() 
    {
        return visaFeeCostPrice;
    }
    public void setSpecialAdditionCostPrice(BigDecimal specialAdditionCostPrice) 
    {
        this.specialAdditionCostPrice = specialAdditionCostPrice;
    }

    public BigDecimal getSpecialAdditionCostPrice() 
    {
        return specialAdditionCostPrice;
    }
    public void setSpecialDeductionsCostPrice(BigDecimal specialDeductionsCostPrice) 
    {
        this.specialDeductionsCostPrice = specialDeductionsCostPrice;
    }

    public BigDecimal getSpecialDeductionsCostPrice() 
    {
        return specialDeductionsCostPrice;
    }
    public void setStudentCostPrice(BigDecimal studentCostPrice) 
    {
        this.studentCostPrice = studentCostPrice;
    }

    public BigDecimal getStudentCostPrice() 
    {
        return studentCostPrice;
    }
    public void setOldPeopleCostPrice(BigDecimal oldPeopleCostPrice) 
    {
        this.oldPeopleCostPrice = oldPeopleCostPrice;
    }

    public BigDecimal getOldPeopleCostPrice() 
    {
        return oldPeopleCostPrice;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }
    public void setUpdateUserId(Long updateUserId) 
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId() 
    {
        return updateUserId;
    }

    public BigDecimal getStartingPointPrice() {
        return startingPointPrice;
    }

    public void setStartingPointPrice(BigDecimal startingPointPrice) {
        this.startingPointPrice = startingPointPrice;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("productId", getProductId())
            .append("type", getType())
            .append("specialDate", getSpecialDate())
            .append("expectedPersonNumber", getExpectedPersonNumber())
            .append("expectedNumber", getExpectedNumber())
            .append("minPersonNumber", getMinPersonNumber())
            .append("adultCostPrice", getAdultCostPrice())
            .append("childrenCostPrice", getChildrenCostPrice())
            .append("adultRetailPrice", getAdultRetailPrice())
            .append("childrenRetailPrice", getChildrenRetailPrice())
            .append("singleSupplementCostPrice", getSingleSupplementCostPrice())
            .append("deposit", getDeposit())
            .append("selfPreparedSignatureCostPrice", getSelfPreparedSignatureCostPrice())
            .append("upgradeCostPrice", getUpgradeCostPrice())
            .append("refusalCostPrice", getRefusalCostPrice())
            .append("visaFeeCostPrice", getVisaFeeCostPrice())
            .append("specialAdditionCostPrice", getSpecialAdditionCostPrice())
            .append("specialDeductionsCostPrice", getSpecialDeductionsCostPrice())
            .append("studentCostPrice", getStudentCostPrice())
            .append("oldPeopleCostPrice", getOldPeopleCostPrice())
            .append("startingPointPrice", getStartingPointPrice())
            .append("delFlag", getDelFlag())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
