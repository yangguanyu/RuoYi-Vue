package com.ruoyi.service.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.service.domain.TfpFindPartner;
import com.ruoyi.service.domain.TfpFindPartnerChatGroup;
import com.ruoyi.service.dto.TfpFindPartnerChatGroupPersonDto;
import com.ruoyi.service.dto.TfpFindPartnerDto;
import com.ruoyi.service.service.ITfpFindPartnerChatGroupService;
import com.ruoyi.service.vo.TfpFindPartnerChatGroupPersonVo;
import com.ruoyi.service.vo.TfpFindPartnerChatGroupVo;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpFindPartnerChatGroupPersonMapper;
import com.ruoyi.service.domain.TfpFindPartnerChatGroupPerson;
import com.ruoyi.service.service.ITfpFindPartnerChatGroupPersonService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 找搭子聊天群组成员Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@Service
@Transactional
public class TfpFindPartnerChatGroupPersonServiceImpl implements ITfpFindPartnerChatGroupPersonService 
{
    @Autowired
    private TfpFindPartnerChatGroupPersonMapper tfpFindPartnerChatGroupPersonMapper;
    @Autowired
    private ITfpFindPartnerChatGroupService tfpFindPartnerChatGroupService;
    @Autowired
    private ISysUserService userService;

    /**
     * 查询找搭子聊天群组成员
     * 
     * @param id 找搭子聊天群组成员主键
     * @return 找搭子聊天群组成员
     */
    @Override
    public TfpFindPartnerChatGroupPerson selectTfpFindPartnerChatGroupPersonById(Long id)
    {
        return tfpFindPartnerChatGroupPersonMapper.selectTfpFindPartnerChatGroupPersonById(id);
    }

    /**
     * 查询找搭子聊天群组成员列表
     * 
     * @param tfpFindPartnerChatGroupPerson 找搭子聊天群组成员
     * @return 找搭子聊天群组成员
     */
    @Override
    public List<TfpFindPartnerChatGroupPerson> selectTfpFindPartnerChatGroupPersonList(TfpFindPartnerChatGroupPerson tfpFindPartnerChatGroupPerson)
    {
        return tfpFindPartnerChatGroupPersonMapper.selectTfpFindPartnerChatGroupPersonList(tfpFindPartnerChatGroupPerson);
    }

    @Override
    public List<TfpFindPartnerChatGroupPersonVo> selectTfpFindPartnerChatGroupPersonVoList(TfpFindPartnerChatGroupPersonDto tfpFindPartnerChatGroupPersonDto) {
        String chatGroupName = tfpFindPartnerChatGroupPersonDto.getChatGroupName();
        if (chatGroupName != null && !chatGroupName.trim().equals("")){
            tfpFindPartnerChatGroupPersonDto.setChatGroupNameFlag(true);

            TfpFindPartnerChatGroup tfpFindPartnerChatGroup = new TfpFindPartnerChatGroup();
            tfpFindPartnerChatGroup.setName(chatGroupName);
            List<TfpFindPartnerChatGroup> findPartnerChatGroupList = tfpFindPartnerChatGroupService.selectTfpFindPartnerChatGroupList(tfpFindPartnerChatGroup);
            if (findPartnerChatGroupList == null || findPartnerChatGroupList.size() == 0){
                return new ArrayList<>();
            }

            List<Long> groupIdList = findPartnerChatGroupList.stream().map(TfpFindPartnerChatGroup::getId).collect(Collectors.toList());
            tfpFindPartnerChatGroupPersonDto.setChatGroupIdList(groupIdList);
        }


        List<TfpFindPartnerChatGroupPersonVo> findPartnerChatGroupPersonVoList = tfpFindPartnerChatGroupPersonMapper.selectTfpFindPartnerChatGroupPersonVoList(tfpFindPartnerChatGroupPersonDto);
        if (findPartnerChatGroupPersonVoList != null && findPartnerChatGroupPersonVoList.size() > 0){
            List<Long> chatGroupIdList = new ArrayList<>();
            List<Long> userIdList = new ArrayList<>();
            for (TfpFindPartnerChatGroupPersonVo tfpFindPartnerChatGroupPersonVo : findPartnerChatGroupPersonVoList) {
                chatGroupIdList.add(tfpFindPartnerChatGroupPersonVo.getChatGroupId());
                userIdList.add(tfpFindPartnerChatGroupPersonVo.getUserId());
            }

            Map<Long, String> chatGroupMap = new HashMap<>();
            if (chatGroupIdList.size() > 0){
                List<TfpFindPartnerChatGroup> chatGroupList = tfpFindPartnerChatGroupService.selectTfpFindPartnerChatGroupListByIdList(chatGroupIdList);
                if (chatGroupList != null && chatGroupList.size() > 0){
                    chatGroupMap = chatGroupList.stream().collect(Collectors.toMap(TfpFindPartnerChatGroup::getId, TfpFindPartnerChatGroup::getName));
                }
            }

            Map<Long, String> userMap = new HashMap<>();
            if (userIdList.size() > 0){
                List<SysUser> sysUserList = userService.selectUserInfoByUserIdList(userIdList);
                if (sysUserList != null && sysUserList.size() > 0){
                    userMap = sysUserList.stream().distinct().collect(Collectors.toMap(SysUser::getUserId, SysUser::getUserName));
                }
            }


            for (TfpFindPartnerChatGroupPersonVo chatGroupPersonVo : findPartnerChatGroupPersonVoList) {
                chatGroupPersonVo.setChatGroupName(chatGroupMap.get(chatGroupPersonVo.getChatGroupId()));
                chatGroupPersonVo.setUserName(userMap.get(chatGroupPersonVo.getUserId()));
            }
        }

        return findPartnerChatGroupPersonVoList;
    }

    /**
     * 新增找搭子聊天群组成员
     * 
     * @param tfpFindPartnerChatGroupPerson 找搭子聊天群组成员
     * @return 结果
     */
    @Override
    public int insertTfpFindPartnerChatGroupPerson(TfpFindPartnerChatGroupPerson tfpFindPartnerChatGroupPerson)
    {
        tfpFindPartnerChatGroupPerson.setCreateTime(DateUtils.getNowDate());
        return tfpFindPartnerChatGroupPersonMapper.insertTfpFindPartnerChatGroupPerson(tfpFindPartnerChatGroupPerson);
    }

    /**
     * 修改找搭子聊天群组成员
     * 
     * @param tfpFindPartnerChatGroupPerson 找搭子聊天群组成员
     * @return 结果
     */
    @Override
    public int updateTfpFindPartnerChatGroupPerson(TfpFindPartnerChatGroupPerson tfpFindPartnerChatGroupPerson)
    {
        tfpFindPartnerChatGroupPerson.setUpdateTime(DateUtils.getNowDate());
        return tfpFindPartnerChatGroupPersonMapper.updateTfpFindPartnerChatGroupPerson(tfpFindPartnerChatGroupPerson);
    }

    /**
     * 批量删除找搭子聊天群组成员
     * 
     * @param ids 需要删除的找搭子聊天群组成员主键
     * @return 结果
     */
    @Override
    public int deleteTfpFindPartnerChatGroupPersonByIds(Long[] ids)
    {
        return tfpFindPartnerChatGroupPersonMapper.deleteTfpFindPartnerChatGroupPersonByIds(ids);
    }

    /**
     * 删除找搭子聊天群组成员信息
     * 
     * @param id 找搭子聊天群组成员主键
     * @return 结果
     */
    @Override
    public int deleteTfpFindPartnerChatGroupPersonById(Long id)
    {
        return tfpFindPartnerChatGroupPersonMapper.deleteTfpFindPartnerChatGroupPersonById(id);
    }

    @Override
    public List<TfpFindPartnerChatGroupPerson> selectListByFindPartnerChatGroupId(Long findPartnerChatGroupId) {
        return tfpFindPartnerChatGroupPersonMapper.selectListByFindPartnerChatGroupId(findPartnerChatGroupId);
    }

    @Override
    public TfpFindPartnerChatGroupPerson selectByGroupIdAndUserId(Long findPartnerChatGroupId, Long userId) {
        return tfpFindPartnerChatGroupPersonMapper.selectByGroupIdAndUserId(findPartnerChatGroupId, userId);
    }
}
