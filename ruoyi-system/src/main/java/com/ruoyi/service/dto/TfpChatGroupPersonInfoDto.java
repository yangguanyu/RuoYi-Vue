package com.ruoyi.service.dto;

import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 聊天群组成员聊天信息对象 tfp_chat_group_person_info
 *
 * @author ruoyi
 * @date 2023-10-31
 */
@Data
public class TfpChatGroupPersonInfoDto extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 聊天群组id */
    private Long chatGroupId;
    private String chatGroupName;
    private boolean chatGroupNameFlag;
    private List<Long> chatGroupIdList;

    /** 聊天群组成员id */
    private Long chatGroupPersonId;

    private Long userId;
    private boolean userIdFlag;
    private List<Long> chatGroupPersonIdList;

    /** 聊天信息 */
    private String chatInfo;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    private Long createUserId;

    /** 更新者id */
    private Long updateUserId;


    /** 聊天群组成员id */
    @ApiModelProperty("最后一个人发言的时间")
    private Date endTime;

    @ApiModelProperty("最后一个发言的id,我可以根据这个id查看后边的发言")
    private Long chatPersonInfoId;


}
