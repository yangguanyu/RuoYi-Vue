package com.ruoyi.service.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.ruoyi.common.utils.DcListUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpProductCbcMapper;
import com.ruoyi.service.domain.TfpProductCbc;
import com.ruoyi.service.service.ITfpProductCbcService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 商品、合作分公司中间Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-12-12
 */
@Service
@Transactional
public class TfpProductCbcServiceImpl implements ITfpProductCbcService 
{
    @Autowired
    private TfpProductCbcMapper tfpProductCbcMapper;

    /**
     * 查询商品、合作分公司中间
     * 
     * @param id 商品、合作分公司中间主键
     * @return 商品、合作分公司中间
     */
    @Override
    public TfpProductCbc selectTfpProductCbcById(Long id)
    {
        return tfpProductCbcMapper.selectTfpProductCbcById(id);
    }

    /**
     * 查询商品、合作分公司中间列表
     * 
     * @param tfpProductCbc 商品、合作分公司中间
     * @return 商品、合作分公司中间
     */
    @Override
    public List<TfpProductCbc> selectTfpProductCbcList(TfpProductCbc tfpProductCbc)
    {
        return tfpProductCbcMapper.selectTfpProductCbcList(tfpProductCbc);
    }

    /**
     * 新增商品、合作分公司中间
     * 
     * @param tfpProductCbc 商品、合作分公司中间
     * @return 结果
     */
    @Override
    public int insertTfpProductCbc(TfpProductCbc tfpProductCbc)
    {
        return tfpProductCbcMapper.insertTfpProductCbc(tfpProductCbc);
    }

    /**
     * 修改商品、合作分公司中间
     * 
     * @param tfpProductCbc 商品、合作分公司中间
     * @return 结果
     */
    @Override
    public int updateTfpProductCbc(TfpProductCbc tfpProductCbc)
    {
        return tfpProductCbcMapper.updateTfpProductCbc(tfpProductCbc);
    }

    /**
     * 批量删除商品、合作分公司中间
     * 
     * @param ids 需要删除的商品、合作分公司中间主键
     * @return 结果
     */
    @Override
    public int deleteTfpProductCbcByIds(Long[] ids)
    {
        return tfpProductCbcMapper.deleteTfpProductCbcByIds(ids);
    }

    /**
     * 删除商品、合作分公司中间信息
     * 
     * @param id 商品、合作分公司中间主键
     * @return 结果
     */
    @Override
    public int deleteTfpProductCbcById(Long id)
    {
        return tfpProductCbcMapper.deleteTfpProductCbcById(id);
    }

    @Override
    public void deleteTfpProductCbcByProductId(Long productId)
    {
        tfpProductCbcMapper.deleteTfpProductCbcByProductId(productId);
    }

    @Override
    public void saveProductCbc(Long productId, List<Long> cooperativeBranchCompanyIdList) {
        if (DcListUtils.isNotEmpty(cooperativeBranchCompanyIdList)){
            for (Long cbcId : cooperativeBranchCompanyIdList) {
                TfpProductCbc tfpProductCbc = new TfpProductCbc();
                tfpProductCbc.setId(null);
                tfpProductCbc.setProductId(productId);
                tfpProductCbc.setCbcId(cbcId);
                tfpProductCbcMapper.insertTfpProductCbc(tfpProductCbc);
            }
        }
    }

    @Override
    public List<TfpProductCbc> selectTfpProductCbcByProductIdList(List<Long> productIdList) {
        return tfpProductCbcMapper.selectTfpProductCbcByProductIdList(productIdList);
    }

    @Override
    public List<TfpProductCbc> selectTfpProductCbcListByProductId(Long productId) {
        List<Long> productIdList = new ArrayList<>();
        productIdList.add(productId);
        return tfpProductCbcMapper.selectTfpProductCbcByProductIdList(productIdList);
    }

    @Override
    public void deleteTfpProductCbcByProductIds(List<Long> productIdList) {
        tfpProductCbcMapper.deleteTfpProductCbcByProductIds(productIdList);
    }
}
