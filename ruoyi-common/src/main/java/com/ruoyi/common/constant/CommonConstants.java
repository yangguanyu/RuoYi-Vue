package com.ruoyi.common.constant;

public class CommonConstants {

    public static final Integer FIRST=0;
    public static final Integer ZERO=0;
    public static final Integer ONE=1;
    public static final Integer TWO=2;
    public static final Integer FIVE=5;
    public static final Integer THREE=3;
    public static final Integer EIGHT=8;
    public static final Integer TEN=10;
    public static final Integer HUNDRED=100;
    public static final String SPACE=" ";
    public static final String COMMA=",";
    public static final String CHAT_ROOM="聊天室";
    public static final String WHOLE_COUNTRY="全国散";
    public static final String SEMICOLON=";";
    public static final String DOC=".";
    public static final String UNDER="_";
    public static final String STRIGULA="-";
    public static final String COLONE=":";
    public static final String ZHONG_SHAN_DISTRICT="445429";
//    public static final Integer TWO_THOUSAND=5000;
    public static final Integer TWO_THOUSAND=2000;
    public static final Integer FIVE_THOUSAND=5000;
    public static final Integer TEN_THOUSAND=10000;
    public static final Integer TWENTY_THOUSAND =20000;
    public static final Integer INFINITE_VALUE=99999;
    public static final String LEFT_SLASH="/";
    public static final String WAN_EN="w";


}
