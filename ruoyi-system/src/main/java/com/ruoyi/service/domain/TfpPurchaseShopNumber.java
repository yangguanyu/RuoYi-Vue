package com.ruoyi.service.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 购物店数量对象 tfp_purchase_shop_number
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public class TfpPurchaseShopNumber extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 商品表id */
    @Excel(name = "商品表id")
    private Long productId;

    /** 地点名称 */
    @Excel(name = "地点名称")
    private String areaName;

    /** 购物场所名称 */
    @Excel(name = "购物场所名称")
    private String purchasePlaceName;

    /** 主要商品信息 */
    @Excel(name = "主要商品信息")
    private String mainProductInfo;

    /** 最长停留时间 */
    @Excel(name = "最长停留时间")
    private String maxResidenceTime;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    @Excel(name = "创建者id")
    private Long createUserId;

    /** 更新者id */
    @Excel(name = "更新者id")
    private Long updateUserId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public void setAreaName(String areaName)
    {
        this.areaName = areaName;
    }

    public String getAreaName() 
    {
        return areaName;
    }
    public void setPurchasePlaceName(String purchasePlaceName) 
    {
        this.purchasePlaceName = purchasePlaceName;
    }

    public String getPurchasePlaceName() 
    {
        return purchasePlaceName;
    }
    public void setMainProductInfo(String mainProductInfo) 
    {
        this.mainProductInfo = mainProductInfo;
    }

    public String getMainProductInfo() 
    {
        return mainProductInfo;
    }
    public void setMaxResidenceTime(String maxResidenceTime) 
    {
        this.maxResidenceTime = maxResidenceTime;
    }

    public String getMaxResidenceTime() 
    {
        return maxResidenceTime;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }
    public void setUpdateUserId(Long updateUserId) 
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId() 
    {
        return updateUserId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("productId", getProductId())
            .append("areaName", getAreaName())
            .append("purchasePlaceName", getPurchasePlaceName())
            .append("mainProductInfo", getMainProductInfo())
            .append("maxResidenceTime", getMaxResidenceTime())
            .append("remark", getRemark())
            .append("delFlag", getDelFlag())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
