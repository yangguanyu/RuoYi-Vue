package com.ruoyi.system.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.entity.TfpAttachment;
import com.ruoyi.service.dto.EditSeparateUserInfoDto;
import com.ruoyi.service.dto.EditUserInfoDto;
import com.ruoyi.service.dto.RealNameAuthenticationDto;
import com.ruoyi.service.dto.RealNameAuthenticationEditDto;
import com.ruoyi.service.vo.*;

/**
 * 用户 业务层
 *
 * @author ruoyi
 */
public interface ISysUserService
{
    /**
     * 根据条件分页查询用户列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    public List<SysUser> selectUserList(SysUser user);

    /**
     * 根据条件分页查询已分配用户角色列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    public List<SysUser> selectAllocatedList(SysUser user);

    /**
     * 根据条件分页查询未分配用户角色列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    public List<SysUser> selectUnallocatedList(SysUser user);

    /**
     * 通过用户名查询用户
     *
     * @param userName 用户名
     * @return 用户对象信息
     */
    public SysUser selectUserByUserName(String userName);

    /**
     * 通过用户ID查询用户
     *
     * @param userId 用户ID
     * @return 用户对象信息
     */
    public SysUser selectUserById(Long userId);

    /**
     * 根据用户ID查询用户所属角色组
     *
     * @param userName 用户名
     * @return 结果
     */
    public String selectUserRoleGroup(String userName);

    /**
     * 根据用户ID查询用户所属岗位组
     *
     * @param userName 用户名
     * @return 结果
     */
    public String selectUserPostGroup(String userName);

    /**
     * 校验用户名称是否唯一
     *
     * @param user 用户信息
     * @return 结果
     */
    public boolean checkUserNameUnique(SysUser user);

    /**
     * 校验手机号码是否唯一
     *
     * @param user 用户信息
     * @return 结果
     */
    public boolean checkPhoneUnique(SysUser user);

    /**
     * 校验手机号码是否唯一
     *
     * @return 结果
     */
    SysUser getUserInfoByPhoneNumber(String mobile);

    /**
     * 校验email是否唯一
     *
     * @param user 用户信息
     * @return 结果
     */
    public boolean checkEmailUnique(SysUser user);

    /**
     * 校验用户是否允许操作
     *
     * @param user 用户信息
     */
    public void checkUserAllowed(SysUser user);

    /**
     * 校验用户是否有数据权限
     *
     * @param userId 用户id
     */
    public void checkUserDataScope(Long userId);

    /**
     * 新增用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    public int insertUser(SysUser user);

    /**
     * 注册用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    public boolean registerUser(SysUser user);

    /**
     * 修改用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    public int updateUser(SysUser user);

    /**
     * 用户授权角色
     *
     * @param userId 用户ID
     * @param roleIds 角色组
     */
    public void insertUserAuth(Long userId, Long[] roleIds);

    /**
     * 修改用户状态
     *
     * @param user 用户信息
     * @return 结果
     */
    public int updateUserStatus(SysUser user);

    /**
     * 修改用户基本信息
     *
     * @param user 用户信息
     * @return 结果
     */
    public int updateUserProfile(SysUser user);

    /**
     * 修改用户头像
     *
     * @param userName 用户名
     * @param avatar 头像地址
     * @return 结果
     */
    public boolean updateUserAvatar(String userName, String avatar);

    /**
     * 重置用户密码
     *
     * @param user 用户信息
     * @return 结果
     */
    public int resetPwd(SysUser user);

    /**
     * 重置用户密码
     *
     * @param userName 用户名
     * @param password 密码
     * @return 结果
     */
    public int resetUserPwd(String userName, String password);

    /**
     * 通过用户ID删除用户
     *
     * @param userId 用户ID
     * @return 结果
     */
    public int deleteUserById(Long userId);

    /**
     * 批量删除用户信息
     *
     * @param userIds 需要删除的用户ID
     * @return 结果
     */
    public int deleteUserByIds(Long[] userIds);

    /**
     * 导入用户数据
     *
     * @param userList 用户数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importUser(List<SysUser> userList, Boolean isUpdateSupport, String operName);

    List<SysUser> selectUserInfoByUserIdList(List<Long> userIdList);

    List<UserVo> handleUserVo(List<Long> userIdList);

    List<UserVo> handleUserAndAvatarVo(List<Long> userIdList);

    /*/**
     * @Author yangguanyu
     * @Description :获取人员信息和头像信息，请注意头像可能为null,使用时请注意空指针判断
     * @Date 21:47 2024/2/13
     * @Param [java.util.List<java.lang.Long>]
     * @return java.util.Map<java.lang.Long,com.ruoyi.common.core.domain.entity.SysUser>
     **/
    Map<Long, SysUser> getUserInfoAndAvatar(List<Long> userList);

    /*/**
     * @Author yangguanyu
     * @Description :获取人员信息，此处没头像，有头像的在上班
     * @Date 21:47 2024/2/13
     * @Param [java.util.List<java.lang.Long>]
     * @return java.util.Map<java.lang.Long,com.ruoyi.common.core.domain.entity.SysUser>
     **/
    Map<Long, SysUser> getUserInfoTransMap(List<Long> userList);

    List<TfpAttachment> getHeadPortraitListByUserIdList(List<Long> userIdList);

    UserInfoDetailVo userInfoDetail(Long userId);

    int editUserInfo(EditUserInfoDto editUserInfoDto);

    DetailEditUserVo detailEditUser();

    UserBaseInfoVo getUserBaseInfo();

    int editSeparateUserInfo(EditSeparateUserInfoDto editSeparateUserInfoDto);

    int realNameAuthentication(RealNameAuthenticationDto realNameAuthenticationDto);

    int realNameAuthenticationEdit(RealNameAuthenticationEditDto realNameAuthenticationEditDto);

    RealNameAuthenticationInfoVo realNameAuthenticationInfo();

    Map<Long, String> getNickNameMap(List<Long> userIdList);

    Map<Long, String> getRealNameMap(List<Long> userIdList);
}
