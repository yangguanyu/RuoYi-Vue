package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.TfpTravelRule;
import com.ruoyi.service.dto.ExportDto;
import com.ruoyi.service.dto.ListTravelRuleDto;
import com.ruoyi.service.excel.ListTravelRuleExcel;
import com.ruoyi.service.vo.ListTravelRuleVo;
import org.apache.ibatis.annotations.Param;

/**
 * 旅行规则Mapper接口
 * 
 * @author ruoyi
 * @date 2024-05-22
 */
public interface TfpTravelRuleMapper 
{
    /**
     * 查询旅行规则
     * 
     * @param id 旅行规则主键
     * @return 旅行规则
     */
    public TfpTravelRule selectTfpTravelRuleById(Long id);

    /**
     * 查询旅行规则列表
     * 
     * @param tfpTravelRule 旅行规则
     * @return 旅行规则集合
     */
    public List<TfpTravelRule> selectTfpTravelRuleList(TfpTravelRule tfpTravelRule);

    /**
     * 新增旅行规则
     * 
     * @param tfpTravelRule 旅行规则
     * @return 结果
     */
    public int insertTfpTravelRule(TfpTravelRule tfpTravelRule);

    /**
     * 修改旅行规则
     * 
     * @param tfpTravelRule 旅行规则
     * @return 结果
     */
    public int updateTfpTravelRule(TfpTravelRule tfpTravelRule);

    /**
     * 删除旅行规则
     * 
     * @param id 旅行规则主键
     * @return 结果
     */
    public int deleteTfpTravelRuleById(Long id);

    /**
     * 批量删除旅行规则
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpTravelRuleByIds(Long[] ids);

    TfpTravelRule selectEffectiveTfpTravelRuleByTravelType(@Param("travelType") Integer travelType);

    List<ListTravelRuleVo> listTravelRule(ListTravelRuleDto listTravelRuleDto);

    List<ListTravelRuleExcel> travelRuleListExport(ExportDto exportDto);
}
