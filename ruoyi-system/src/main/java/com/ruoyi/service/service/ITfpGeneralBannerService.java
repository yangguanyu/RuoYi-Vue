package com.ruoyi.service.service;

import java.util.List;
import com.ruoyi.service.domain.TfpGeneralBanner;

/**
 * bannerService接口
 * 
 * @author ruoyi
 * @date 2023-11-01
 */
public interface ITfpGeneralBannerService 
{
    /**
     * 查询banner
     * 
     * @param id banner主键
     * @return banner
     */
    public TfpGeneralBanner selectTfpGeneralBannerById(Long id);

    /**
     * 查询banner列表
     * 
     * @param tfpGeneralBanner banner
     * @return banner集合
     */
    public List<TfpGeneralBanner> selectTfpGeneralBannerList(TfpGeneralBanner tfpGeneralBanner);

    /**
     * 新增banner
     * 
     * @param tfpGeneralBanner banner
     * @return 结果
     */
    public int insertTfpGeneralBanner(TfpGeneralBanner tfpGeneralBanner);

    /**
     * 修改banner
     * 
     * @param tfpGeneralBanner banner
     * @return 结果
     */
    public int updateTfpGeneralBanner(TfpGeneralBanner tfpGeneralBanner);

    /**
     * 批量删除banner
     * 
     * @param ids 需要删除的banner主键集合
     * @return 结果
     */
    public int deleteTfpGeneralBannerByIds(Long[] ids);

    /**
     * 删除banner信息
     * 
     * @param id banner主键
     * @return 结果
     */
    public int deleteTfpGeneralBannerById(Long id);
}
