package com.ruoyi.common.enuma.flight;

import com.ruoyi.common.enuma.BaseStrEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ApplicationExceptionEnum implements BaseStrEnum {
    UNCLEAR_INTERFACE_REQUEST("000003", "不明确的接口用户请求"),
    CONCURRENCY_CONTROL("000014", "并发控制"),
    VAGUE_INTERFACE_URL("000002", "传入的接口与URL不明确"),
    CONTROLLED_ACCESS_TIME("000013", "受控制的访问时间间隔"),
    RESOURCES_EXHAUSTED("000012", "用尽资源流量"),
    INTERFACE_ACCESS_DENIED("000011", "接口不允许访问"),
    EXPIRED_USER("000007", "已过期的用户"),
    MISSING_SECURITY_CODE("000017", "无安全码"),
    UNAUTHORIZED_ACCESS("000006", "用户未开通接口账号"),
    INVALID_TIMESTAMP("000016", "timestamp 时间戳无效"),
    SIGNATURE_VERIFICATION_FAILED("000005", "验证签名失败"),
    LIMITED_IP("000015", "受限的IP"),
    OBSOLETE_INTERFACE("000009", "已失效的接口"),
    NON_EXISTENT_INTERFACE("000008", "不存在的接口"),
    AGENT_DATA_NOT_FOUND("01020101", "无此代理数据"),
    POLICY_ORDER_DATA_MISSING("01020102", "NO_POLICYORDER_DATA"),
    UNKNOWN_ERROR("01009997", "异常，可能是个未知的．请联系平台管理员"),
    UNKNOWN("01009999", "Unknown"),
    NO_DATA("0100002", "无数据"),
    REQUEST_TIMEOUT_OR_EMPTY("0100001", "请求超时或者数据为空"),
    ACCOUNT_ABNORMALITY("01099999", "您的账户存在异常，请联系销售经理"),
    RESTRICTED_INTERFACE_ACCESS("000010", "受限制的接口访问");

    private final String value;
    private final String desc;


}
