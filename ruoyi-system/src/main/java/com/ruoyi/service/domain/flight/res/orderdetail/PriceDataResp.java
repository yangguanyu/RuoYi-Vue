package com.ruoyi.service.domain.flight.res.orderdetail;

import lombok.Data;

@Data
public class PriceDataResp {
    // 乘机人类型，1表示成人，2表示儿童，3表示婴儿（暂不使用）
    private Integer crewType;

    // 票面价
    private Integer price;

    // 机建费
    private Integer airportTax;

    // 燃油费
    private Integer fuelTax;

    // 返点，0.1表示0.1%
    private Double commisionPoint;

    // 定额
    private Double commisionMoney;

    // 支付手续费
    private Double payFee;

    // 单人结算价
    private Double singlePerson;

    // 工作时间，例如：08-20, 09-18
    private String workTime;

    // 退票工作时间，例如：08-20, 09-18
    private String vtWorkTime;

    // 产品备注
    private String comment;
}
