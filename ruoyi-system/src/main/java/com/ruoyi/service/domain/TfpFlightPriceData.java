package com.ruoyi.service.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 价格数据对象 tfp_flight_price_data
 * 
 * @author ruoyi
 * @date 2024-04-10
 */
public class TfpFlightPriceData extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 航班信息表id */
    @Excel(name = "航班信息表id")
    private Long flightId;

    /** 舱位信息表id */
    @Excel(name = "舱位信息表id")
    private Long seatId;

    /** 政策信息表id */
    @Excel(name = "政策信息表id")
    private Long policyId;

    /** 乘机人类型 1-成人 2-儿童 3-婴儿(暂不使用) */
    @Excel(name = "乘机人类型 1-成人 2-儿童 3-婴儿(暂不使用)")
    private String crewType;

    /** 票面价 */
    @Excel(name = "票面价")
    private Integer price;

    /** 单人结算价 （price*（1- commisionPoint%）- commisionMoney）不含税 */
    @Excel(name = "单人结算价")
    private Double settlement;

    /** 单人机建费 */
    @Excel(name = "单人机建费")
    private Integer airportTax;

    /** 单人燃油费 */
    @Excel(name = "单人燃油费")
    private Integer fuelTax;

    /** 返点 0.1=0.1% */
    @Excel(name = "返点 0.1=0.1%")
    private Double commisionPoint;

    /** 定额 */
    @Excel(name = "定额")
    private Double commisionMoney;

    /** 支付手续费 */
    @Excel(name = "支付手续费")
    private Double payFee;

    /** 是否换编码出票 1-原编码出票 2-需要换编码出票 */
    @Excel(name = "是否换编码出票 1-原编码出票 2-需要换编码出票")
    private Integer isSwitchPnr;

    /** 政策类型（BSP或B2B） */
    @Excel(name = "政策类型", readConverterExp = "B=SP或B2B")
    private String policyType;

    /** 出票效率 */
    @Excel(name = "出票效率")
    private String ticketSpeed;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    @Excel(name = "创建者id")
    private Long createUserId;

    /** 更新者id */
    @Excel(name = "更新者id")
    private Long updateUserId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setFlightId(Long flightId) 
    {
        this.flightId = flightId;
    }

    public Long getFlightId() 
    {
        return flightId;
    }
    public void setSeatId(Long seatId) 
    {
        this.seatId = seatId;
    }

    public Long getSeatId() 
    {
        return seatId;
    }
    public void setPolicyId(Long policyId) 
    {
        this.policyId = policyId;
    }

    public Long getPolicyId() 
    {
        return policyId;
    }
    public void setCrewType(String crewType) 
    {
        this.crewType = crewType;
    }

    public String getCrewType() 
    {
        return crewType;
    }
    public void setPrice(Integer price)
    {
        this.price = price;
    }

    public Integer getPrice()
    {
        return price;
    }
    public void setSettlement(Double settlement)
    {
        this.settlement = settlement;
    }

    public Double getSettlement()
    {
        return settlement;
    }
    public void setAirportTax(Integer airportTax)
    {
        this.airportTax = airportTax;
    }

    public Integer getAirportTax()
    {
        return airportTax;
    }
    public void setFuelTax(Integer fuelTax)
    {
        this.fuelTax = fuelTax;
    }

    public Integer getFuelTax()
    {
        return fuelTax;
    }
    public void setCommisionPoint(Double commisionPoint)
    {
        this.commisionPoint = commisionPoint;
    }

    public Double getCommisionPoint()
    {
        return commisionPoint;
    }
    public void setCommisionMoney(Double commisionMoney)
    {
        this.commisionMoney = commisionMoney;
    }

    public Double getCommisionMoney()
    {
        return commisionMoney;
    }
    public void setPayFee(Double payFee)
    {
        this.payFee = payFee;
    }

    public Double getPayFee()
    {
        return payFee;
    }
    public void setIsSwitchPnr(Integer isSwitchPnr) 
    {
        this.isSwitchPnr = isSwitchPnr;
    }

    public Integer getIsSwitchPnr() 
    {
        return isSwitchPnr;
    }
    public void setPolicyType(String policyType) 
    {
        this.policyType = policyType;
    }

    public String getPolicyType() 
    {
        return policyType;
    }
    public void setTicketSpeed(String ticketSpeed) 
    {
        this.ticketSpeed = ticketSpeed;
    }

    public String getTicketSpeed() 
    {
        return ticketSpeed;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }
    public void setUpdateUserId(Long updateUserId) 
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId() 
    {
        return updateUserId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("flightId", getFlightId())
            .append("seatId", getSeatId())
            .append("policyId", getPolicyId())
            .append("crewType", getCrewType())
            .append("price", getPrice())
            .append("settlement", getSettlement())
            .append("airportTax", getAirportTax())
            .append("fuelTax", getFuelTax())
            .append("commisionPoint", getCommisionPoint())
            .append("commisionMoney", getCommisionMoney())
            .append("payFee", getPayFee())
            .append("isSwitchPnr", getIsSwitchPnr())
            .append("policyType", getPolicyType())
            .append("ticketSpeed", getTicketSpeed())
            .append("delFlag", getDelFlag())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
