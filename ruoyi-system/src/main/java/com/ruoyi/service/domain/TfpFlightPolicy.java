package com.ruoyi.service.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 政策信息对象 tfp_flight_policy
 * 
 * @author ruoyi
 * @date 2024-04-10
 */
public class TfpFlightPolicy extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 航班信息表id */
    @Excel(name = "航班信息表id")
    private Long flightId;

    /** 舱位信息表id */
    @Excel(name = "舱位信息表id")
    private Long seatId;

    /** 产品ID */
    @Excel(name = "产品ID")
    private String policyId;

    /** 产品类型 商旅优选：1、2、3、4、12、13、9、 低价推荐：41、42、43、44、45、26、 官网：21、22、23、24、25、6 生成订单需传入出生日期：4、41、42、43、44、45、21、22、23、24、25、6、9、26 */
    @Excel(name = "产品类型 商旅优选：1、2、3、4、12、13、9、 低价推荐：41、42、43、44、45、26、 官网：21、22、23、24、25、6 生成订单需传入出生日期：4、41、42、43、44、45、21、22、23、24、25、6、9、26")
    private String productType;

    /** 证件限制 false-不限 true-有限制 */
    @Excel(name = "证件限制 false-不限 true-有限制")
    private Boolean certificates;

    /** 证件 */
    @Excel(name = "证件")
    private String certificatesList;

    /** 出票工作时间 */
    @Excel(name = "出票工作时间")
    private String workTime;

    /** 退票工作时间 */
    @Excel(name = "退票工作时间")
    private String vtWorkTime;

    /** 产品备注 */
    @Excel(name = "产品备注")
    private String comment;

    /** 适用乘客类型 1-成人 2-儿童 3-成人+儿童 4-成人+婴儿 5-成人+儿童+婴儿 */
    @Excel(name = "适用乘客类型 1-成人 2-儿童 3-成人+儿童 4-成人+婴儿 5-成人+儿童+婴儿")
    private Integer applyPassenger;

    /** 报销凭证类型 1-行程单 2-发票 3-电子发票 4-行程单+发票 5-不提供报销凭证 */
    @Excel(name = "报销凭证类型 1-行程单 2-发票 3-电子发票 4-行程单+发票 5-不提供报销凭证")
    private Integer voucher;

    /** 最晚出票时间计算方式 1-支付后 2-起飞前 */
    @Excel(name = "最晚出票时间计算方式 1-支付后 2-起飞前")
    private Integer latestAlgorithm;

    /** 最晚出票时间（分钟） */
    @Excel(name = "最晚出票时间", readConverterExp = "分=钟")
    private Integer latestTicket;

    /** 产品年龄限制 */
    @Excel(name = "产品年龄限制")
    private String ageLimit;

    /** 人数限制 */
    @Excel(name = "人数限制")
    private String numberLimit;

    /** 通程产品服务说明 */
    @Excel(name = "通程产品服务说明")
    private String description;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    @Excel(name = "创建者id")
    private Long createUserId;

    /** 更新者id */
    @Excel(name = "更新者id")
    private Long updateUserId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setFlightId(Long flightId) 
    {
        this.flightId = flightId;
    }

    public Long getFlightId() 
    {
        return flightId;
    }
    public void setSeatId(Long seatId) 
    {
        this.seatId = seatId;
    }

    public Long getSeatId() 
    {
        return seatId;
    }
    public void setPolicyId(String policyId) 
    {
        this.policyId = policyId;
    }

    public String getPolicyId() 
    {
        return policyId;
    }
    public void setProductType(String productType) 
    {
        this.productType = productType;
    }

    public String getProductType() 
    {
        return productType;
    }
    public void setCertificates(Boolean certificates)
    {
        this.certificates = certificates;
    }

    public Boolean getCertificates()
    {
        return certificates;
    }
    public void setCertificatesList(String certificatesList) 
    {
        this.certificatesList = certificatesList;
    }

    public String getCertificatesList() 
    {
        return certificatesList;
    }
    public void setWorkTime(String workTime)
    {
        this.workTime = workTime;
    }

    public String getWorkTime()
    {
        return workTime;
    }
    public void setVtWorkTime(String vtWorkTime)
    {
        this.vtWorkTime = vtWorkTime;
    }

    public String getVtWorkTime()
    {
        return vtWorkTime;
    }
    public void setComment(String comment) 
    {
        this.comment = comment;
    }

    public String getComment() 
    {
        return comment;
    }
    public void setApplyPassenger(Integer applyPassenger)
    {
        this.applyPassenger = applyPassenger;
    }

    public Integer getApplyPassenger() 
    {
        return applyPassenger;
    }
    public void setVoucher(Integer voucher) 
    {
        this.voucher = voucher;
    }

    public Integer getVoucher() 
    {
        return voucher;
    }
    public void setLatestAlgorithm(Integer latestAlgorithm) 
    {
        this.latestAlgorithm = latestAlgorithm;
    }

    public Integer getLatestAlgorithm() 
    {
        return latestAlgorithm;
    }
    public void setLatestTicket(Integer latestTicket)
    {
        this.latestTicket = latestTicket;
    }

    public Integer getLatestTicket()
    {
        return latestTicket;
    }
    public void setAgeLimit(String ageLimit) 
    {
        this.ageLimit = ageLimit;
    }

    public String getAgeLimit() 
    {
        return ageLimit;
    }
    public void setNumberLimit(String numberLimit) 
    {
        this.numberLimit = numberLimit;
    }

    public String getNumberLimit() 
    {
        return numberLimit;
    }
    public void setDescription(String description) 
    {
        this.description = description;
    }

    public String getDescription() 
    {
        return description;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }
    public void setUpdateUserId(Long updateUserId) 
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId() 
    {
        return updateUserId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("flightId", getFlightId())
            .append("seatId", getSeatId())
            .append("policyId", getPolicyId())
            .append("productType", getProductType())
            .append("certificates", getCertificates())
            .append("certificatesList", getCertificatesList())
            .append("workTime", getWorkTime())
            .append("vtWorkTime", getVtWorkTime())
            .append("comment", getComment())
            .append("applyPassenger", getApplyPassenger())
            .append("voucher", getVoucher())
            .append("latestAlgorithm", getLatestAlgorithm())
            .append("latestTicket", getLatestTicket())
            .append("ageLimit", getAgeLimit())
            .append("numberLimit", getNumberLimit())
            .append("description", getDescription())
            .append("delFlag", getDelFlag())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
