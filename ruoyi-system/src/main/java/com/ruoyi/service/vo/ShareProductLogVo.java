package com.ruoyi.service.vo;

import lombok.Data;

import java.util.Date;

@Data
public class ShareProductLogVo {
    private Long productId;
    private Date createTime;
}
