package com.ruoyi.service.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpUserPortraitInformationMapper;
import com.ruoyi.service.domain.TfpUserPortraitInformation;
import com.ruoyi.service.service.ITfpUserPortraitInformationService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 用户肖像信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-11-01
 */
@Service
@Transactional
public class TfpUserPortraitInformationServiceImpl implements ITfpUserPortraitInformationService 
{
    @Autowired
    private TfpUserPortraitInformationMapper tfpUserPortraitInformationMapper;

    /**
     * 查询用户肖像信息
     * 
     * @param id 用户肖像信息主键
     * @return 用户肖像信息
     */
    @Override
    public TfpUserPortraitInformation selectTfpUserPortraitInformationById(Long id)
    {
        return tfpUserPortraitInformationMapper.selectTfpUserPortraitInformationById(id);
    }

    /**
     * 查询用户肖像信息列表
     * 
     * @param tfpUserPortraitInformation 用户肖像信息
     * @return 用户肖像信息
     */
    @Override
    public List<TfpUserPortraitInformation> selectTfpUserPortraitInformationList(TfpUserPortraitInformation tfpUserPortraitInformation)
    {
        return tfpUserPortraitInformationMapper.selectTfpUserPortraitInformationList(tfpUserPortraitInformation);
    }

    /**
     * 新增用户肖像信息
     * 
     * @param tfpUserPortraitInformation 用户肖像信息
     * @return 结果
     */
    @Override
    public int insertTfpUserPortraitInformation(TfpUserPortraitInformation tfpUserPortraitInformation)
    {
        tfpUserPortraitInformation.setCreateTime(DateUtils.getNowDate());
        return tfpUserPortraitInformationMapper.insertTfpUserPortraitInformation(tfpUserPortraitInformation);
    }

    /**
     * 修改用户肖像信息
     * 
     * @param tfpUserPortraitInformation 用户肖像信息
     * @return 结果
     */
    @Override
    public int updateTfpUserPortraitInformation(TfpUserPortraitInformation tfpUserPortraitInformation)
    {
        tfpUserPortraitInformation.setUpdateTime(DateUtils.getNowDate());
        return tfpUserPortraitInformationMapper.updateTfpUserPortraitInformation(tfpUserPortraitInformation);
    }

    /**
     * 批量删除用户肖像信息
     * 
     * @param ids 需要删除的用户肖像信息主键
     * @return 结果
     */
    @Override
    public int deleteTfpUserPortraitInformationByIds(Long[] ids)
    {
        return tfpUserPortraitInformationMapper.deleteTfpUserPortraitInformationByIds(ids);
    }

    /**
     * 删除用户肖像信息信息
     * 
     * @param id 用户肖像信息主键
     * @return 结果
     */
    @Override
    public int deleteTfpUserPortraitInformationById(Long id)
    {
        return tfpUserPortraitInformationMapper.deleteTfpUserPortraitInformationById(id);
    }
}
