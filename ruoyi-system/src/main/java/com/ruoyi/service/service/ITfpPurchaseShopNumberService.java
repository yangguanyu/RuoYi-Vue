package com.ruoyi.service.service;

import java.util.List;

import com.ruoyi.service.domain.TfpProduct;
import com.ruoyi.service.domain.TfpPurchaseShopNumber;
import com.ruoyi.service.dto.TfpPurchaseShopNumberDto;

/**
 * 购物店数量Service接口
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public interface ITfpPurchaseShopNumberService 
{
    /**
     * 查询购物店数量
     * 
     * @param id 购物店数量主键
     * @return 购物店数量
     */
    public TfpPurchaseShopNumber selectTfpPurchaseShopNumberById(Long id);

    /**
     * 查询购物店数量列表
     * 
     * @param tfpPurchaseShopNumber 购物店数量
     * @return 购物店数量集合
     */
    public List<TfpPurchaseShopNumber> selectTfpPurchaseShopNumberList(TfpPurchaseShopNumber tfpPurchaseShopNumber);

    /**
     * 新增购物店数量
     * 
     * @param tfpPurchaseShopNumber 购物店数量
     * @return 结果
     */
    public int insertTfpPurchaseShopNumber(TfpPurchaseShopNumber tfpPurchaseShopNumber);

    /**
     * 修改购物店数量
     * 
     * @param tfpPurchaseShopNumber 购物店数量
     * @return 结果
     */
    public int updateTfpPurchaseShopNumber(TfpPurchaseShopNumber tfpPurchaseShopNumber);

    /**
     * 批量删除购物店数量
     * 
     * @param ids 需要删除的购物店数量主键集合
     * @return 结果
     */
    public int deleteTfpPurchaseShopNumberByIds(Long[] ids);

    /**
     * 删除购物店数量信息
     * 
     * @param id 购物店数量主键
     * @return 结果
     */
    public int deleteTfpPurchaseShopNumberById(Long id);

    void deleteByProductId(Long productId);

    void saveList(Long productId, List<TfpPurchaseShopNumberDto> purchaseList);

    List<TfpPurchaseShopNumber> selectListByProductId(Long productId);

    void deleteTfpPurchaseShopNumberByProductIds(List<Long> productIdList);
}
