package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel(value = "PriceDetailDto")
public class PriceDetailDto {
    @ApiModelProperty("商品id")
    private Long productId;
    @ApiModelProperty("出发时间")
    private Date goDate;
    @ApiModelProperty("出发地名称")
    private String goPlaceName;
    @ApiModelProperty("人数")
    private Integer personNumber;
    @ApiModelProperty("是否选择低价机票 0-否 1-是")
    private Integer lowPriceAirTicketFlag;
}
