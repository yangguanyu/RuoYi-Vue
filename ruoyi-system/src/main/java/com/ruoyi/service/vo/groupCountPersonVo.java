package com.ruoyi.service.vo;

import lombok.Data;

@Data
public class groupCountPersonVo {
    private Long groupId;
    private Integer num;
}
