package com.ruoyi.common.area.domain.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "AreaDTO", description = "区域基础信息")
public class AreaDTO {
    @ApiModelProperty("id")
    private Long id;
    @ApiModelProperty("名称")
    private String name;
    @ApiModelProperty("展示名称")
    private String showName;
    @ApiModelProperty("拼音")
    private String pinyin;
    @ApiModelProperty("排序")
    private Integer sort;
    @ApiModelProperty("图片")
    private String image;
}
