package com.ruoyi.common.enuma.flight;

import com.ruoyi.common.enuma.BaseStrEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RefundQueryExceptionCode implements BaseStrEnum {
    INVALID_REFUND("011303", "无退款单数据");

    private String value;
    private String desc;


}
