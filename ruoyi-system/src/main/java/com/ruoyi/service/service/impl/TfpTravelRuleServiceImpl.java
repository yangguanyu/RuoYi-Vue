package com.ruoyi.service.service.impl;

import java.util.*;
import java.util.stream.Collectors;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enuma.*;
import com.ruoyi.common.exception.BusinessException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.DcListUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.service.dto.ExportDto;
import com.ruoyi.service.dto.ListTravelRuleDto;
import com.ruoyi.service.dto.TfpTravelRuleDto;
import com.ruoyi.service.excel.ListTravelRuleExcel;
import com.ruoyi.service.vo.DetailTravelRuleVo;
import com.ruoyi.service.vo.EffectiveTravelRuleVo;
import com.ruoyi.service.vo.ListTravelRuleVo;
import com.ruoyi.service.vo.OrderListVo;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpTravelRuleMapper;
import com.ruoyi.service.domain.TfpTravelRule;
import com.ruoyi.service.service.ITfpTravelRuleService;

import static com.ruoyi.common.utils.PageUtils.startPage;

/**
 * 旅行规则Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-05-22
 */
@Service
public class TfpTravelRuleServiceImpl implements ITfpTravelRuleService 
{
    @Autowired
    private TfpTravelRuleMapper tfpTravelRuleMapper;
    @Autowired
    private ISysUserService sysUserService;

    /**
     * 查询旅行规则
     * 
     * @param id 旅行规则主键
     * @return 旅行规则
     */
    @Override
    public TfpTravelRule selectTfpTravelRuleById(Long id)
    {
        return tfpTravelRuleMapper.selectTfpTravelRuleById(id);
    }

    /**
     * 查询旅行规则列表
     * 
     * @param tfpTravelRule 旅行规则
     * @return 旅行规则
     */
    @Override
    public List<TfpTravelRule> selectTfpTravelRuleList(TfpTravelRule tfpTravelRule)
    {
        return tfpTravelRuleMapper.selectTfpTravelRuleList(tfpTravelRule);
    }

    /**
     * 新增旅行规则
     * 
     * @param tfpTravelRule 旅行规则
     * @return 结果
     */
    @Override
    public int insertTfpTravelRule(TfpTravelRule tfpTravelRule)
    {
        tfpTravelRule.setCreateTime(DateUtils.getNowDate());
        return tfpTravelRuleMapper.insertTfpTravelRule(tfpTravelRule);
    }

    /**
     * 修改旅行规则
     * 
     * @param tfpTravelRule 旅行规则
     * @return 结果
     */
    @Override
    public int updateTfpTravelRule(TfpTravelRule tfpTravelRule)
    {
        tfpTravelRule.setUpdateTime(DateUtils.getNowDate());
        return tfpTravelRuleMapper.updateTfpTravelRule(tfpTravelRule);
    }

    /**
     * 批量删除旅行规则
     * 
     * @param ids 需要删除的旅行规则主键
     * @return 结果
     */
    @Override
    public int deleteTfpTravelRuleByIds(Long[] ids)
    {
        return tfpTravelRuleMapper.deleteTfpTravelRuleByIds(ids);
    }

    /**
     * 删除旅行规则信息
     * 
     * @param id 旅行规则主键
     * @return 结果
     */
    @Override
    public int deleteTfpTravelRuleById(Long id)
    {
        return tfpTravelRuleMapper.deleteTfpTravelRuleById(id);
    }

    @Override
    public List<ListTravelRuleVo> listTravelRule(ListTravelRuleDto listTravelRuleDto) {
        startPage();
        // 那是仅有一种1种类型
        listTravelRuleDto.setType(TravelRuleEnum.TRAVEL.getValue());
        List<ListTravelRuleVo> listTravelRuleVoList = tfpTravelRuleMapper.listTravelRule(listTravelRuleDto);
        if (DcListUtils.isNotEmpty(listTravelRuleVoList)){
            List<Long> userIdList = listTravelRuleVoList.stream().map(ListTravelRuleVo::getUserId).distinct().collect(Collectors.toList());
            Map<Long, String> realNameMap = sysUserService.getRealNameMap(userIdList);

            for (ListTravelRuleVo listTravelRuleVo : listTravelRuleVoList) {
                String realName = realNameMap.get(listTravelRuleVo.getUserId());
                listTravelRuleVo.setRealName(Objects.isNull(realName) ? listTravelRuleVo.getUserName() : realName);
                listTravelRuleVo.setTravelTypeName(BaseEnum.getDescByCode(TravelTypeEnum.class, listTravelRuleVo.getTravelType()));
            }
        }

        return listTravelRuleVoList;
    }

    @Override
    public int saveTravelRule(TfpTravelRuleDto tfpTravelRuleDto) {
        String version = tfpTravelRuleDto.getVersion().trim();
        TfpTravelRule param = new TfpTravelRule();
        param.setVersion(version);
        param.setType(tfpTravelRuleDto.getType());
        param.setDelFlag(DeleteEnum.EXIST.getValue());
        List<TfpTravelRule> tfpTravelRuleList = tfpTravelRuleMapper.selectTfpTravelRuleList(param);
        if (DcListUtils.isNotEmpty(tfpTravelRuleList)){
            throw new BusinessException("版本号已存在，请重新输入");
        }

        LoginUser loginUser = SecurityUtils.getLoginUser();
        Integer travelType = tfpTravelRuleDto.getTravelType();

        TfpTravelRule oldTfpTravelRule = tfpTravelRuleMapper.selectEffectiveTfpTravelRuleByTravelType(travelType);

        TfpTravelRule tfpTravelRule = new TfpTravelRule();
        BeanUtils.copyProperties(tfpTravelRuleDto, tfpTravelRule);
        tfpTravelRule.setVersion(version);
        tfpTravelRule.setStatus(EffectiveStatusEnum.EFFECTIVE.getValue());
        tfpTravelRule.setDelFlag(DeleteEnum.EXIST.getValue());
        tfpTravelRule.setCreateUserId(loginUser.getUserId());
        tfpTravelRule.setCreateBy(loginUser.getUsername());
        tfpTravelRule.setCreateTime(DateUtils.getNowDate());
        tfpTravelRuleMapper.insertTfpTravelRule(tfpTravelRule);

        if (Objects.nonNull(oldTfpTravelRule)){
            oldTfpTravelRule.setStatus(EffectiveStatusEnum.NO_EFFECTIVE.getValue());
            oldTfpTravelRule.setUpdateUserId(loginUser.getUserId());
            oldTfpTravelRule.setUpdateBy(loginUser.getUsername());
            oldTfpTravelRule.setUpdateTime(DateUtils.getNowDate());
            tfpTravelRuleMapper.updateTfpTravelRule(oldTfpTravelRule);
        }

        return 1;
    }

    @Override
    public DetailTravelRuleVo detailTravelRule(Long id) {
        TfpTravelRule tfpTravelRule = tfpTravelRuleMapper.selectTfpTravelRuleById(id);
        if (Objects.isNull(tfpTravelRule)){
            throw new BusinessException("规则信息不存在");
        }

        DetailTravelRuleVo detailTravelRuleVo = new DetailTravelRuleVo();
        BeanUtils.copyProperties(tfpTravelRule, detailTravelRuleVo);
        detailTravelRuleVo.setTravelTypeName(BaseEnum.getDescByCode(TravelTypeEnum.class, tfpTravelRule.getTravelType()));

        SysUser sysUser = sysUserService.selectUserById(tfpTravelRule.getCreateUserId());
        String realName = sysUser.getRealName();
        detailTravelRuleVo.setRealName(Objects.isNull(realName) ? sysUser.getUserName() : realName);

        return detailTravelRuleVo;
    }

    @Override
    public List<EffectiveTravelRuleVo> getEffectiveTravelRuleList(Integer type) {
        List<EffectiveTravelRuleVo> effectiveTravelRuleVoList = new ArrayList<>();

        TfpTravelRule param = new TfpTravelRule();
        param.setStatus(EffectiveStatusEnum.EFFECTIVE.getValue());
        param.setType(type);
        List<TfpTravelRule> tfpTravelRuleList = tfpTravelRuleMapper.selectTfpTravelRuleList(param);
        if (DcListUtils.isNotEmpty(tfpTravelRuleList)){
            for (TfpTravelRule tfpTravelRule : tfpTravelRuleList) {
                EffectiveTravelRuleVo effectiveTravelRuleVo = new EffectiveTravelRuleVo();
                effectiveTravelRuleVo.setRuleId(tfpTravelRule.getId());
                effectiveTravelRuleVo.setTravelType(tfpTravelRule.getTravelType());
                effectiveTravelRuleVo.setText(tfpTravelRule.getText());
                effectiveTravelRuleVoList.add(effectiveTravelRuleVo);
            }
        }

        return effectiveTravelRuleVoList;
    }

    @Override
    public List<ListTravelRuleExcel> travelRuleListExport(ExportDto exportDto) {
        Integer allExportFlag = exportDto.getAllExportFlag();
        if (Objects.isNull(allExportFlag)){
            throw new BusinessException("请选择导出的类型");
        }

        List<Long> idList = exportDto.getIdList();
        if (Objects.equals(YesOrNoFlagEnum.NO.getValue(), allExportFlag) && DcListUtils.isEmpty(idList)){
            throw new BusinessException("请选择需要导出的数据");
        }

        List<ListTravelRuleExcel> listTravelRuleExcelList = tfpTravelRuleMapper.travelRuleListExport(exportDto);
        if (DcListUtils.isNotEmpty(listTravelRuleExcelList)){
            List<Long> userIdList = listTravelRuleExcelList.stream().map(ListTravelRuleExcel::getUserId).distinct().collect(Collectors.toList());
            Map<Long, String> realNameMap = sysUserService.getRealNameMap(userIdList);

            for (ListTravelRuleExcel listTravelRuleExcel : listTravelRuleExcelList) {
                String realName = realNameMap.get(listTravelRuleExcel.getUserId());
                listTravelRuleExcel.setRealName(Objects.isNull(realName) ? listTravelRuleExcel.getUserName() : realName);
                listTravelRuleExcel.setTravelTypeName(BaseEnum.getDescByCode(TravelTypeEnum.class, listTravelRuleExcel.getTravelType()));
            }
        }

        return listTravelRuleExcelList;
    }
}
