package com.ruoyi.service.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 订单对象 tfp_order
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public class TfpOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 订单编号 */
    @Excel(name = "订单编号")
    private String orderNo;

    /** 供应商id */
    @Excel(name = "供应商id")
    private Long supplyId;

    /** 商品ID */
    @Excel(name = "商品ID")
    private Long productId;

    /** 售卖单价 */
    @Excel(name = "售卖单价")
    private BigDecimal salePrice;

    /** 商品数量 */
    @Excel(name = "商品数量")
    private Integer quantities;

    /** 订单金额 */
    @Excel(name = "订单金额")
    private BigDecimal finalAmount;

    /** 出发时间 */
    @Excel(name = "出发时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date goDate;

    /** 出发地名称 */
    @Excel(name = "出发地名称")
    private String goPlaceName;

    /** 订单买家备注(预留) */
    @Excel(name = "订单买家备注(预留)")
    private String buyerRemark;

    /** 订单卖家备注（预留） */
    @Excel(name = "订单卖家备注", readConverterExp = "预=留")
    private String sellerRemark;

    /** 订单状态 */
    @Excel(name = "订单状态 0-待支付 1-已完成 2-待确认(已支付) 3-待出行 4-申请退款-审核中 5-申请退款-退款成功 6-过期 7-取消")
    private Integer status;

    /** 退款时临时状态(退款失败后，status重新变为该状态) */
    @Excel(name = "退款时临时状态(退款失败后，status重新变为该状态)")
    private Integer refundTempStatus;

    /** 退款失败原因 */
    @Excel(name = "退款失败原因")
    private String refundFailReason;

    /** 商品快照id */
    @Excel(name = "商品快照id")
    private Long snapshotId;

    /** 是否可售后（0：不可以，1：可以） */
    @Excel(name = "是否可售后", readConverterExp = "0=：不可以，1：可以")
    private Integer afterServiceAble;

    /** 预支付交易会话标识 */
    @Excel(name = "预支付交易会话标识")
    private String prepayId;

    /** 预支付交易会话过期时间 */
    @Excel(name = "预支付交易会话过期时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date expireDate;

    /** 出行方式 0-组队出行 1-独立出行 */
    @Excel(name = "出行方式")
    private Integer travelType;

    /** 群组id */
    @Excel(name = "群组id")
    private Long chatGroupId;

    /** 支付时间 */
    @Excel(name = "支付时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date payTime;;

    /** 出行完成时间 */
    @Excel(name = "出行完成时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date travelCompleteTime;

    /** 是否可以申请退款 0-否 1-是 */
    @Excel(name = "是否可以申请退款 0-否 1-是")
    private Integer applyRefundFlag;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    @Excel(name = "创建者id")
    private Long createUserId;

    /** 更新者id */
    @Excel(name = "更新者id")
    private Long updateUserId;

    /** 退款截止时间 */
    @Excel(name = "退款截止时间")
    private Date refundEndTime;

    /** 分享用户id */
    @Excel(name = "分享用户id")
    private Long shareUserId;

    /** 是否选择低价机票 0-否 1-是 */
    @Excel(name = "是否选择低价机票 0-否 1-是")
    private Integer lowPriceAirTicketFlag;

    /** 航班去程价格 */
    @Excel(name = "航班去程价格")
    private BigDecimal goAirTicketPrice;

    /** 航班返程价格 */
    @Excel(name = "航班返程价格")
    private BigDecimal backAirTicketPrice;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setOrderNo(String orderNo) 
    {
        this.orderNo = orderNo;
    }

    public String getOrderNo() 
    {
        return orderNo;
    }
    public void setSupplyId(Long supplyId) 
    {
        this.supplyId = supplyId;
    }

    public Long getSupplyId() 
    {
        return supplyId;
    }
    public void setProductId(Long productId) 
    {
        this.productId = productId;
    }

    public Long getProductId() 
    {
        return productId;
    }
    public void setSalePrice(BigDecimal salePrice) 
    {
        this.salePrice = salePrice;
    }

    public BigDecimal getSalePrice() 
    {
        return salePrice;
    }
    public void setQuantities(Integer quantities)
    {
        this.quantities = quantities;
    }

    public Integer getQuantities()
    {
        return quantities;
    }
    public void setFinalAmount(BigDecimal finalAmount) 
    {
        this.finalAmount = finalAmount;
    }

    public BigDecimal getFinalAmount() 
    {
        return finalAmount;
    }
    public void setBuyerRemark(String buyerRemark) 
    {
        this.buyerRemark = buyerRemark;
    }

    public String getBuyerRemark() 
    {
        return buyerRemark;
    }
    public void setSellerRemark(String sellerRemark) 
    {
        this.sellerRemark = sellerRemark;
    }

    public String getSellerRemark() 
    {
        return sellerRemark;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setSnapshotId(Long snapshotId)
    {
        this.snapshotId = snapshotId;
    }

    public Long getSnapshotId()
    {
        return snapshotId;
    }
    public void setAfterServiceAble(Integer afterServiceAble) 
    {
        this.afterServiceAble = afterServiceAble;
    }

    public Integer getAfterServiceAble() 
    {
        return afterServiceAble;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }
    public void setUpdateUserId(Long updateUserId) 
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId() 
    {
        return updateUserId;
    }

    public Date getGoDate() {
        return goDate;
    }

    public void setGoDate(Date goDate) {
        this.goDate = goDate;
    }

    public String getGoPlaceName() {
        return goPlaceName;
    }

    public void setGoPlaceName(String goPlaceName) {
        this.goPlaceName = goPlaceName;
    }

    public String getPrepayId() {
        return prepayId;
    }

    public void setPrepayId(String prepayId) {
        this.prepayId = prepayId;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public Integer getRefundTempStatus() {
        return refundTempStatus;
    }

    public void setRefundTempStatus(Integer refundTempStatus) {
        this.refundTempStatus = refundTempStatus;
    }

    public String getRefundFailReason() {
        return refundFailReason;
    }

    public void setRefundFailReason(String refundFailReason) {
        this.refundFailReason = refundFailReason;
    }

    public Integer getTravelType() {
        return travelType;
    }

    public void setTravelType(Integer travelType) {
        this.travelType = travelType;
    }

    public Long getChatGroupId() {
        return chatGroupId;
    }

    public void setChatGroupId(Long chatGroupId) {
        this.chatGroupId = chatGroupId;
    }

    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    public Date getTravelCompleteTime() {
        return travelCompleteTime;
    }

    public void setTravelCompleteTime(Date travelCompleteTime) {
        this.travelCompleteTime = travelCompleteTime;
    }

    public Integer getApplyRefundFlag() {
        return applyRefundFlag;
    }

    public void setApplyRefundFlag(Integer applyRefundFlag) {
        this.applyRefundFlag = applyRefundFlag;
    }

    public Date getRefundEndTime() {
        return refundEndTime;
    }

    public void setRefundEndTime(Date refundEndTime) {
        this.refundEndTime = refundEndTime;
    }

    public Long getShareUserId() {
        return shareUserId;
    }

    public void setShareUserId(Long shareUserId) {
        this.shareUserId = shareUserId;
    }

    public Integer getLowPriceAirTicketFlag() {
        return lowPriceAirTicketFlag;
    }

    public void setLowPriceAirTicketFlag(Integer lowPriceAirTicketFlag) {
        this.lowPriceAirTicketFlag = lowPriceAirTicketFlag;
    }

    public BigDecimal getGoAirTicketPrice() {
        return goAirTicketPrice;
    }

    public void setGoAirTicketPrice(BigDecimal goAirTicketPrice) {
        this.goAirTicketPrice = goAirTicketPrice;
    }

    public BigDecimal getBackAirTicketPrice() {
        return backAirTicketPrice;
    }

    public void setBackAirTicketPrice(BigDecimal backAirTicketPrice) {
        this.backAirTicketPrice = backAirTicketPrice;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("orderNo", getOrderNo())
            .append("supplyId", getSupplyId())
            .append("productId", getProductId())
            .append("salePrice", getSalePrice())
            .append("quantities", getQuantities())
            .append("finalAmount", getFinalAmount())
            .append("goDate", getGoDate())
            .append("goPlaceName", getGoPlaceName())
            .append("buyerRemark", getBuyerRemark())
            .append("sellerRemark", getSellerRemark())
            .append("status", getStatus())
            .append("refundTempStatus", getRefundTempStatus())
            .append("refundFailReason", getRefundFailReason())
            .append("snapshotId", getSnapshotId())
            .append("afterServiceAble", getAfterServiceAble())
            .append("prepayId", getPrepayId())
            .append("expireDate", getExpireDate())
            .append("travelType", getTravelType())
            .append("chatGroupId", getChatGroupId())
            .append("payTime", getPayTime())
            .append("travelCompleteTime", getTravelCompleteTime())
            .append("applyRefundFlag", getApplyRefundFlag())
            .append("refundEndTime", getRefundEndTime())
            .append("shareUserId", getShareUserId())
            .append("lowPriceAirTicketFlag", getLowPriceAirTicketFlag())
            .append("goAirTicketPrice", getGoAirTicketPrice())
            .append("backAirTicketPrice", getBackAirTicketPrice())
            .append("delFlag", getDelFlag())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
