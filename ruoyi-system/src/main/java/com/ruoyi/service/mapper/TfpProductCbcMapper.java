package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.TfpProductCbc;
import org.apache.ibatis.annotations.Param;

/**
 * 商品、合作分公司中间Mapper接口
 * 
 * @author ruoyi
 * @date 2023-12-12
 */
public interface TfpProductCbcMapper 
{
    /**
     * 查询商品、合作分公司中间
     * 
     * @param id 商品、合作分公司中间主键
     * @return 商品、合作分公司中间
     */
    public TfpProductCbc selectTfpProductCbcById(Long id);

    /**
     * 查询商品、合作分公司中间列表
     * 
     * @param tfpProductCbc 商品、合作分公司中间
     * @return 商品、合作分公司中间集合
     */
    public List<TfpProductCbc> selectTfpProductCbcList(TfpProductCbc tfpProductCbc);

    /**
     * 新增商品、合作分公司中间
     * 
     * @param tfpProductCbc 商品、合作分公司中间
     * @return 结果
     */
    public int insertTfpProductCbc(TfpProductCbc tfpProductCbc);

    /**
     * 修改商品、合作分公司中间
     * 
     * @param tfpProductCbc 商品、合作分公司中间
     * @return 结果
     */
    public int updateTfpProductCbc(TfpProductCbc tfpProductCbc);

    /**
     * 删除商品、合作分公司中间
     * 
     * @param id 商品、合作分公司中间主键
     * @return 结果
     */
    public int deleteTfpProductCbcById(Long id);

    /**
     * 批量删除商品、合作分公司中间
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpProductCbcByIds(Long[] ids);

    void deleteTfpProductCbcByProductId(@Param("productId") Long productId);

    List<TfpProductCbc> selectTfpProductCbcByProductIdList(@Param("productIdList") List<Long> productIdList);

    void deleteTfpProductCbcByProductIds(@Param("productIdList") List<Long> productIdList);
}
