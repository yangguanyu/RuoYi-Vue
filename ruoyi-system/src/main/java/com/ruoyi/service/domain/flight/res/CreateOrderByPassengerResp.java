package com.ruoyi.service.domain.flight.res;

import lombok.Data;

import java.util.List;

@Data
public class CreateOrderByPassengerResp {
    // 平台订单号
    private String orderNo;
    // 订单创建时间: 2018-10-12 12:22:00
    private String createTime;
    // 子单集
    private List<SubOrder> subOrders;
    // 航班信息
    private List<Segment> segments;
    // 订单支付总金额
    private Double totalPay;
    // 订单支付手续费
    private Double payCharge;

    @Data
    public class SubOrder {
        // 子订单号
        private String subOrderNo;
        // 子订单创建时间
        private String createTime;
        // 乘机人信息
        private List<Passenger> passengers;
        // 运价信息
        private PriceData priceData;
        // 订单状态，例如1-代表新订单，3-代表订单已支付等 4-订单处理中 5-订单已出票 6-订单已取消
        private Integer status;
        // 子订单结算价
        private Double subTotalPay;
    }

    @Data
    public class Segment {
        // 航班号
        private String flightNo;
        // 航班日期
        private String depDate;
        // 航班抵达日期
        private String arrDate;
        // 出发如：SZX (机场三字码)
        private String depCode;
        // 抵达如：SZX (机场三字码)
        private String arrCode;
        // 舱位Y（大写）
        private String cabin;
        // 儿童舱位C（大写）
        private String chdCabin;
        // 婴儿舱位跟随成人
        private String infCabin;
    }

    @Data
    public class Passenger {
        // 姓名
        private String name;
        // 性别，1代表男性，2代表女性
        private Integer sex;
        // 生日
        private String birthday;
        // 证件类型，1-身份证、港澳台居民居住证 (18位) 2-护照、 (护照号，最长15位) 3-军人证、港澳台通行证、外国人永久居留证,海员证、出生证明、其它有效证件（最长15位) 4-无法识别证件，不支持自动值机(最长18位)
        private Integer identityType;
        // 证件号码
        private String identityNo;
        // 乘机人类型，1代表成人，2代表儿童
        private Integer crewType;
        // 乘客手机号
        private String phoneNum;
        // 用于存储以乘客拆单用户
        private String sonSheNo;
        // 证件有效期
        private String effectiveDate;
    }

    @Data
    public class PriceData {
        // 乘机人类型，1代表成人，2代表儿童，3代表婴儿（暂不使用）
        private String crewType;
        // 票面价
        private Integer price;
        // 机建
        private Integer airportTax;
        // 燃油费
        private Integer fuelTax;
        // 返点，0.1代表0.1%
        private Double commissionPoint;
        // 定额
        private Double commissionMoney;
        // 支付手续费
        private Double payFee;
        // 单人结算价
        private Double singlePerson;
    }
}
