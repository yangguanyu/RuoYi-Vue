package com.ruoyi.service.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "ListProductVo", description = "商品")
public class ListProductVo {
    @ApiModelProperty("商品id")
    private Long id;

    @ApiModelProperty("商品名称")
    private String productName;
    @ApiModelProperty("服务类型id")
    private String categoryId;
    @ApiModelProperty("服务类型名称")
    private String categoryName;
    @ApiModelProperty("业务区域")
    private String areaName;
    @ApiModelProperty("行程天数 几天")
    private Integer dayNum;
    @ApiModelProperty("报名人数")
    private Integer signUpPersonNumber;

    @ApiModelProperty("图片url")
    private String imageUrl;
    @ApiModelProperty("报名人员头像url集合")
    private List<String> signUpPersonHeadPortraitUrlList;
    @ApiModelProperty("商品封面图片url集合")
    private List<String> productCoverImageUrlList;
}
