package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.TfpUserBankCard;
import org.apache.ibatis.annotations.Param;

/**
 * 用户银行卡Mapper接口
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public interface TfpUserBankCardMapper 
{
    /**
     * 查询用户银行卡
     * 
     * @param id 用户银行卡主键
     * @return 用户银行卡
     */
    public TfpUserBankCard selectTfpUserBankCardById(Long id);

    /**
     * 查询用户银行卡列表
     * 
     * @param tfpUserBankCard 用户银行卡
     * @return 用户银行卡集合
     */
    public List<TfpUserBankCard> selectTfpUserBankCardList(TfpUserBankCard tfpUserBankCard);

    /**
     * 新增用户银行卡
     * 
     * @param tfpUserBankCard 用户银行卡
     * @return 结果
     */
    public int insertTfpUserBankCard(TfpUserBankCard tfpUserBankCard);

    /**
     * 修改用户银行卡
     * 
     * @param tfpUserBankCard 用户银行卡
     * @return 结果
     */
    public int updateTfpUserBankCard(TfpUserBankCard tfpUserBankCard);

    /**
     * 删除用户银行卡
     * 
     * @param id 用户银行卡主键
     * @return 结果
     */
    public int deleteTfpUserBankCardById(Long id);

    /**
     * 批量删除用户银行卡
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpUserBankCardByIds(Long[] ids);

    TfpUserBankCard getUserBankCardByUserId(@Param("userId") Long userId);

    List<TfpUserBankCard> selectTfpUserBankCardListByIdList(@Param("idList") List<Long> idList);
}
