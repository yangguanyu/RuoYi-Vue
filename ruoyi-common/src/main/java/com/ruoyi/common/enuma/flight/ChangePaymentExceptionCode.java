package com.ruoyi.common.enuma.flight;

import com.ruoyi.common.enuma.BaseStrEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ChangePaymentExceptionCode implements BaseStrEnum {
    CHANGE_ORDER_NOT_FOUND("011101", "没有查询到变更单。变更单为空！"),
    DATA_IS_EMPTY("011102", "数据为空"),
    PAYMENT_NOT_AUTHORIZED("011105", "支付未授权"),
    AUTO_PAYMENT_FAILED("011106", "自动支付失败"),
    NULL_RESPONSE_BODY("011103", "RSP_IS_NULL_EXCEPTION_BODY"),
    PAYMENT_EXCEPTION("011104", "支付异常"),
    AUTO_PAYMENT_NOT_AUTHORIZED("011109", "自动支付未授权"),
    AUTO_PAYMENT_ACCOUNT_IS_EMPTY("011107", "自动支付账号为空"),
    NETWORK_EXCEPTION("011108", "网络异常");

    private final String value;
    private final String desc;


}
