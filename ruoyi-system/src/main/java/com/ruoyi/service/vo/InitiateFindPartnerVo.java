package com.ruoyi.service.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel(value = "InitiateFindPartnerVo", description = "发起的搭子vo")
public class InitiateFindPartnerVo {
    @ApiModelProperty("找搭子id")
    private Long id;
    @ApiModelProperty("用户昵称")
    private String nickName;
    @ApiModelProperty("头像图片")
    private String headImage;
    @ApiModelProperty("搭子标题")
    private String title;
    @ApiModelProperty("报名人数")
    private Integer signUpPersonNumber;
    @ApiModelProperty("类型名称")
    private String typeName;
    @ApiModelProperty("搭子图片")
    private String image;
    @ApiModelProperty("找搭子发布和撤销等状态 0-未发布 1-审核通过 2-审核未通过 3-已发布待审核 4-因疑似非法而下架")
    private Integer status;
    @ApiModelProperty("展示状态")
    private String showStatus;
    @ApiModelProperty("审核不通过原因")
    private String reason;
    @ApiModelProperty("聊天群id")
    private Long chatGroupId;
    @ApiModelProperty(value = "价格类型编码，免费，线下付，发起人代付，前端传给我字典的dictCode")
    private Long priceCode;

    @JsonIgnore
    private Date startTime;
    @JsonIgnore
    private Date signUpEndTime;
}
