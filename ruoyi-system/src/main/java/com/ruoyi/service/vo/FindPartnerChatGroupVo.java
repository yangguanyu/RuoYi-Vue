package com.ruoyi.service.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "FindPartnerChatGroupVo", description = "找搭子群组详情")
public class FindPartnerChatGroupVo {
    @ApiModelProperty("搭子聊天群组id")
    private Long findPartnerChatGroupId;

    @ApiModelProperty("搭子聊天群组名称")
    private String findPartnerChatGroupName;

    @ApiModelProperty("搭子聊天群组成员集合")
    private List<UserVo> userList;
}
