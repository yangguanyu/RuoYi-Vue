package com.ruoyi.service.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "OrderChatGroupInfoVo", description = "聊天群信息")
public class OrderChatGroupInfoVo {
    @ApiModelProperty("聊天群id")
    private Long id;
    @ApiModelProperty("订单id")
    private Long orderId;
    @ApiModelProperty("聊天群组名称")
    private String name;
    @ApiModelProperty("报名人数")
    private Integer signUpPersonNumber;
    @ApiModelProperty("限制人数")
    private Integer limitPersonNumber;
    @ApiModelProperty("聊天群人数头像url集合")
    private List<String> chatGroupPersonHeadPortraitUrlList;
}
