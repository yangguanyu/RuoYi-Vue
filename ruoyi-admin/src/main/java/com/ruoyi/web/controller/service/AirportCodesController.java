package com.ruoyi.web.controller.service;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.service.service.IAirportCodesService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

@RestController
@RequestMapping("/service/airportCodes")
public class AirportCodesController {
    @Autowired
    private IAirportCodesService airportCodesService;

    @ApiOperation("验证航班城市编码是否存在 0-不存在 1-存在")
    @PostMapping("/judgeAirportCodesExists")
    @ResponseBody
    public R<Integer> judgeAirportCodesExists(@RequestParam String cityName){
        Integer result = 1;
        String threeCode = airportCodesService.getThreeCode(cityName);
        if (Objects.isNull(threeCode)){
            result = 0;
        }

        return R.ok(result);
    }
}
