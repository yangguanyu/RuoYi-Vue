import request from '@/utils/request'

// 查询客服列表
export function listCustomerService(query) {
  return request({
    url: '/service/customerService/list',
    method: 'get',
    params: query
  })
}

// 查询客服详细
export function getCustomerService(id) {
  return request({
    url: '/service/customerService/' + id,
    method: 'get'
  })
}

// 新增客服
export function addCustomerService(data) {
  return request({
    url: '/service/customerService',
    method: 'post',
    data: data
  })
}

// 修改客服
export function updateCustomerService(data) {
  return request({
    url: '/service/customerService',
    method: 'put',
    data: data
  })
}

// 删除客服
export function delCustomerService(id) {
  return request({
    url: '/service/customerService/' + id,
    method: 'delete'
  })
}
