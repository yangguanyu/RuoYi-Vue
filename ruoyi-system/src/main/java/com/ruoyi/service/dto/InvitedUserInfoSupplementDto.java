package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "InvitedUserInfoSupplementDto")
public class InvitedUserInfoSupplementDto {
    @ApiModelProperty(value = "旅客姓名")
    private String name;
    @ApiModelProperty(value = "证件类型")
    private String cardType;
    @ApiModelProperty(value = "证件号码")
    private String cardNum;
    @ApiModelProperty(value = "手机号码")
    private String phonenumber;
}
