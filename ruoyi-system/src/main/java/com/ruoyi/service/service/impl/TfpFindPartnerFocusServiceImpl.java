package com.ruoyi.service.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpFindPartnerFocusMapper;
import com.ruoyi.service.domain.TfpFindPartnerFocus;
import com.ruoyi.service.service.ITfpFindPartnerFocusService;

/**
 * 找搭子关注Service业务层处理
 * 
 * @author yangguanyu
 * @date 2024-01-12
 */
@Service
public class TfpFindPartnerFocusServiceImpl implements ITfpFindPartnerFocusService 
{
    @Autowired
    private TfpFindPartnerFocusMapper tfpFindPartnerFocusMapper;

    /**
     * 查询找搭子关注
     * 
     * @param id 找搭子关注主键
     * @return 找搭子关注
     */
    @Override
    public TfpFindPartnerFocus selectTfpFindPartnerFocusById(Long id)
    {
        return tfpFindPartnerFocusMapper.selectTfpFindPartnerFocusById(id);
    }

    /**
     * 查询找搭子关注列表
     * 
     * @param tfpFindPartnerFocus 找搭子关注
     * @return 找搭子关注
     */
    @Override
    public List<TfpFindPartnerFocus> selectTfpFindPartnerFocusList(TfpFindPartnerFocus tfpFindPartnerFocus)
    {
        return tfpFindPartnerFocusMapper.selectTfpFindPartnerFocusList(tfpFindPartnerFocus);
    }

    /**
     * 新增找搭子关注
     * 
     * @param tfpFindPartnerFocus 找搭子关注
     * @return 结果
     */
    @Override
    public int insertTfpFindPartnerFocus(TfpFindPartnerFocus tfpFindPartnerFocus)
    {
        tfpFindPartnerFocus.setCreateTime(DateUtils.getNowDate());
        return tfpFindPartnerFocusMapper.insertTfpFindPartnerFocus(tfpFindPartnerFocus);
    }

    /**
     * 修改找搭子关注
     * 
     * @param tfpFindPartnerFocus 找搭子关注
     * @return 结果
     */
    @Override
    public int updateTfpFindPartnerFocus(TfpFindPartnerFocus tfpFindPartnerFocus)
    {
        tfpFindPartnerFocus.setUpdateTime(DateUtils.getNowDate());
        return tfpFindPartnerFocusMapper.updateTfpFindPartnerFocus(tfpFindPartnerFocus);
    }

    /**
     * 批量删除找搭子关注
     * 
     * @param ids 需要删除的找搭子关注主键
     * @return 结果
     */
    @Override
    public int deleteTfpFindPartnerFocusByIds(Long[] ids)
    {
        return tfpFindPartnerFocusMapper.deleteTfpFindPartnerFocusByIds(ids);
    }

    /**
     * 删除找搭子关注信息
     * 
     * @param id 找搭子关注主键
     * @return 结果
     */
    @Override
    public int deleteTfpFindPartnerFocusById(Long id)
    {
        return tfpFindPartnerFocusMapper.deleteTfpFindPartnerFocusById(id);
    }
}
