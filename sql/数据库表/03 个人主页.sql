-- 用户表
ALTER TABLE sys_user 
	add column real_name_auth_status int(1) comment '实名认证状态 0-未实名认证 1-已实名认证',
	add column real_name_phone varchar(11) comment '实名认证手机号',
	add column constellation_code varchar(50) comment '星座编码',
	add column constellation_name varchar(20) comment '星座名称',
	add column personal_profile  varchar(200) comment '个人简介',
	add column birthday  datetime comment '生日',
	add column uid varchar(50) comment '用户UID',
	add column card_type varchar(20) comment '证件类型 1.身份证 2.台胞证 3.港澳通行证 4.护照 5.士兵证',
	add column card_num varchar(50) comment '证件号码',
	add column identity_id bigint(20) comment '身份id';

ALTER TABLE sys_user
    add column real_name varchar(30) comment '真实名称' AFTER nick_name;
	
-- 字典表追加父级id，支持分级展示
ALTER TABLE sys_dict_data 
	add column parent_code bigint(20) comment '实名认证状态 0-未实名认证 1-已实名认证' AFTER dict_code;

-- 用户身份表
CREATE TABLE `sys_identity` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` bigint(20) comment '用户id',
  `identity_code` varchar(20) comment '身份编码',
  `identity_name` varchar(50) comment '身份名称',
  PRIMARY KEY (`id`)
) COMMENT='用户身份表';

-- 用户个性标签表
CREATE TABLE `sys_user_label` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` bigint(20) comment '用户id',
  `label_type_code` varchar(20) comment '标签类型code',
  `label_type_name` varchar(50) comment '标签类型名称',
  `label_code` varchar(20) comment '标签code',
  `label_name` varchar(50) comment '标签名称',
  PRIMARY KEY (`id`)
) COMMENT='用户个性标签表';

-- 用户图片表
CREATE TABLE `sys_user_icon` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` bigint(20) comment '用户id',
  `name` varchar(200) comment '图片名称',
  `path` varchar(200) comment '图片地址',
  `head_sculpture_flag` int(1) comment '是否是头像 0-否 1-是',
  PRIMARY KEY (`id`)
) COMMENT='用户图片表';
	
-- 关注表
CREATE TABLE `sys_user_care` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `owner_user_id` bigint(20) comment '主人用户id',
  `fans_user_id` bigint(20) comment '粉丝用户id',
  `mutual_attention_flag` int(1) comment '是否互相关注 0-否 1-是',
  PRIMARY KEY (`id`)
) COMMENT='关注表';

-- 访客记录表
CREATE TABLE `sys_user_visitor_record` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `owner_user_id` bigint(20) comment '主人用户id',
  `visitor_user_id` bigint(20) comment '查看用户id',
  PRIMARY KEY (`id`)
) COMMENT='访客记录表';