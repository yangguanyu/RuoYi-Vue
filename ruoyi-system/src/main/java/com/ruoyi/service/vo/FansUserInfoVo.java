package com.ruoyi.service.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "FansUserInfoVo", description = "粉丝用户信息")
public class FansUserInfoVo {
    @ApiModelProperty("关注用户id")
    private Long userId;
    @ApiModelProperty("关注用户昵称")
    private String nickName;
    @ApiModelProperty("关注用户头像")
    private String userHeadImage;
    @ApiModelProperty("是否互相关注 0-否 1-是")
    private Integer mutualAttentionFlag;
}
