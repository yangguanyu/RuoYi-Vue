package com.ruoyi.service.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 找搭子对象 tfp_find_partner
 *
 * @author ruoyi
 * @date 2023-11-01
 */
@Data
@ApiModel("TfpFindPartnerVO-找搭子列表返回结果")
public class TfpFindPartnerVO {
    @ApiModelProperty("id")
    private Long id;

    /** 标题 */
    @ApiModelProperty("标题")
    private String title;

    /** 描述 */
    @ApiModelProperty("描述")
    private String activityDesc;

    /** 类型code */
    @ApiModelProperty("类型code")
    private String typeCode;

    /** 类型名称 */
    @ApiModelProperty("类型名称")
    private String typeName;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty("开始时间")
    private Date startTime;

    /** 报名截止时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty("报名截止时间")
    private Date signUpEndTime;

    /** 活动人数 */
    @ApiModelProperty("活动人数")
    private Long activityPersonNumber;

    /** 活动费用 */
    @ApiModelProperty("活动费用")
    private BigDecimal activityPrice;

    /** 活动地点 */
    @ApiModelProperty("活动地点")
    private String activityPlace;

    /** 找搭子发布和撤销等状态 0-未发布 1-审核通过 2-审核未通过 3-已发布待审核 4-因疑似非法而下架 */
    @ApiModelProperty("找搭子发布和撤销等状态 0-未发布 1-审核通过 2-审核未通过 3-已发布待审核 4-因疑似非法而下架")
    private Integer status;

    @ApiModelProperty("搭子图片")
    private String image;

    //找搭子状态 1：找搭子进行中 2：即将发车 3:已结束
    @ApiModelProperty("找搭子状态 1：找搭子进行中 2：即将发车 3:已结束")
    private Integer activityStatus;


    private String groupDay;
    @ApiModelProperty("创建人id")
    private Long createUserId;

    @ApiModelProperty("用户名称")
    private String nickName;

    @ApiModelProperty("用户头像")
    private String avatar;

    @ApiModelProperty(value = "价格类型编码，免费，线下付，发起人代付，前端传给我字典的dictCode")
    private Long priceCode;

    @ApiModelProperty("报名人数")
    private Integer signUpPersonNumber;
}
