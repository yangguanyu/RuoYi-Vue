package com.ruoyi.service.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class JudgeDateTravelFlagVo {
    // 是否可以出行 0-否 1-是
    private Integer travelFlag;
    // 成人零售价
    private BigDecimal adultRetailPrice;
}
