-- 用户收益表
CREATE TABLE `tfp_user_income` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` bigint(20) comment '用户id',
  `remaining_price` decimal(20,4) comment '余额',
  `level_code` varchar(20) comment '等级编码',
  `level_name` varchar(20) comment '等级名称',
  PRIMARY KEY (`id`)
) COMMENT='用户收益表';

-- 用户收益明细表
CREATE TABLE `tfp_user_income_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_income_id` bigint(20) comment '用户收益主表id',
  `user_id` bigint(20) comment '用户id',
  `invitee_user_id` bigint(20) comment '受邀人用户id',
  `source_type` tinyint(1) COMMENT '收益来源类型 0-商品 1-搭子',
  `source_id` bigint(20) comment '收益来源id',
  `source_price` decimal(20,4) comment '收益来源金额',
  `price` decimal(20,4) comment '佣金金额',
  `status` int(2) comment '状态 0- 受邀人查看 1-结算佣金/已完成 2-受邀人下单 3-受邀人付款 4-出行完成',
  `order_id` bigint(20) comment '订单id',
  `order_number` varchar(50) comment '订单编码',
  `settlement_number` varchar(50) comment '结算单号',
  `view_time` datetime DEFAULT NULL COMMENT '受邀人查看',
  `place_order_time` datetime DEFAULT NULL COMMENT '受邀人下单时间',
  `pay_time` datetime DEFAULT NULL COMMENT '受邀人付款时间',
  `travel_complete_time` datetime DEFAULT NULL COMMENT '出行完成时间',
  `settlement_commission_time` datetime DEFAULT NULL COMMENT '结算佣金时间',
  PRIMARY KEY (`id`)
) COMMENT='用户收益明细表';

-- 银行表
CREATE TABLE `tfp_bank` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `bank_code` varchar(20) comment '银行编码',
  `bank_name` varchar(50) comment '银行名称',
  `simple_bank_name` varchar(50) comment '银行简称',
  PRIMARY KEY (`id`)
) COMMENT='银行表';

-- 用户银行卡表
CREATE TABLE `tfp_user_bank_card` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` bigint(20) comment '用户id',
  `bank_id` varchar(50) comment '银行id',
  `bank_name` varchar(50) comment '银行名称',
  `bank_card_number` varchar(50) comment '银行卡号',
  `account_opening_branch` varchar(50) comment '开户网点',
  `user_name` varchar(50) comment '账户姓名',
  `phone_number` varchar(50) comment '预留电话',
  PRIMARY KEY (`id`)
) COMMENT='用户银行卡表';

-- 提款表
CREATE TABLE `tfp_draw_money` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` bigint(20) comment '用户id',
  `user_bank_card_id` bigint(20) comment '用户银行卡id',
  
  `bank_card_number` varchar(50) comment '银行卡号',
  `bank_id` varchar(50) comment '银行id',
  `bank_name` varchar(50) comment '银行名称',
  
  `draw_money_price` decimal(20,4) comment '提款金额',
  `draw_money_number` varchar(50) comment '提现编号',
  `status` tinyint comment '状态 0-提交申请/审核中 1-提现完成 2-提现失败/审核不通过',
  `reason` varchar(50) comment '审核不通过原因',
  PRIMARY KEY (`id`)
) COMMENT='提款表';