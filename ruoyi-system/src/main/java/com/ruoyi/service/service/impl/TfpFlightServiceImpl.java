package com.ruoyi.service.service.impl;

import com.ruoyi.common.enuma.DeleteEnum;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.DcListUtils;
import com.ruoyi.service.domain.*;
import com.ruoyi.service.domain.flight.res.FlightSearchResp;
import com.ruoyi.service.mapper.*;
import com.ruoyi.service.vo.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.service.ITfpFlightService;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 航班信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-10
 */
@Service
@Transactional
public class TfpFlightServiceImpl implements ITfpFlightService
{
    @Autowired
    private TfpFlightMapper tfpFlightMapper;

    @Autowired
    private TfpFlightPolicyMapper tfpFlightPolicyMapper;

    @Autowired
    private TfpFlightPriceDataMapper tfpFlightPriceDataMapper;

    @Autowired
    private TfpFlightSeatDetailMapper tfpFlightSeatDetailMapper;

    @Autowired
    private TfpFlightSeatMapper tfpFlightSeatMapper;

    @Autowired
    private TfpFlightSegmentDetailMapper tfpFlightSegmentDetailMapper;

    @Autowired
    private TfpFlightStopInfoMapper tfpFlightStopInfoMapper;

    @Override
    public List<TfpFlightVo> saveFlightInfo(List<FlightSearchResp> flightSearchRespList){
        List<TfpFlightVo> tfpFlightVoList = new ArrayList<>();
        if (DcListUtils.isNotEmpty(flightSearchRespList)){
            for (FlightSearchResp flightSearchResp : flightSearchRespList) {
                TfpFlight param = new TfpFlight();
                param.setFlightNo(flightSearchResp.getFlightNo());
                param.setArrTime(flightSearchResp.getArrTime());
                param.setDepTime(flightSearchResp.getDepTime());
                param.setDelFlag(DeleteEnum.EXIST.getValue());
                List<TfpFlight> tfpFlights = tfpFlightMapper.selectTfpFlightList(param);
                if (DcListUtils.isNotEmpty(tfpFlights)){
                    for (TfpFlight tfpFlight : tfpFlights) {
                        Long id = tfpFlight.getId();
                        tfpFlightMapper.deleteTfpFlightById(id);
                        tfpFlightPolicyMapper.deleteTfpFlightPolicyByFlightId(id);
                        tfpFlightPriceDataMapper.deleteTfpFlightPriceDataByFlightId(id);
                        tfpFlightSeatDetailMapper.deleteTfpFlightSeatDetailByFlightId(id);
                        tfpFlightSeatMapper.deleteTfpFlightSeatByFlightId(id);
                        tfpFlightSegmentDetailMapper.deleteTfpFlightSegmentDetailByFlightId(id);
                        tfpFlightStopInfoMapper.deleteTfpFlightStopInfoByFlightId(id);
                    }
                }

                TfpFlight tfpFlight = new TfpFlight();
                BeanUtils.copyProperties(flightSearchResp, tfpFlight);

                tfpFlight.setDelFlag(DeleteEnum.EXIST.getValue());
                tfpFlight.setCreateBy("航空接口");
                tfpFlight.setCreateTime(DateUtils.getNowDate());

                tfpFlightMapper.insertTfpFlight(tfpFlight);

                TfpFlightVo tfpFlightVo = new TfpFlightVo();
                BeanUtils.copyProperties(tfpFlight, tfpFlightVo);

                Long flightId = tfpFlight.getId();

                List<TfpFlightSeatVo> seatVoList = new ArrayList<>();
                List<FlightSearchResp.Seat> seatItems = flightSearchResp.getSeatItems();
                if (DcListUtils.isNotEmpty(seatItems)){
                    for (FlightSearchResp.Seat seatItem : seatItems) {
                        TfpFlightSeat tfpFlightSeat = new TfpFlightSeat();
                        BeanUtils.copyProperties(seatItem, tfpFlightSeat);

                        tfpFlightSeat.setFlightId(flightId);
                        tfpFlightSeat.setDelFlag(DeleteEnum.EXIST.getValue());
                        tfpFlightSeat.setCreateBy("航空接口");
                        tfpFlightSeat.setCreateTime(DateUtils.getNowDate());

                        tfpFlightSeatMapper.insertTfpFlightSeat(tfpFlightSeat);

                        TfpFlightSeatVo tfpFlightSeatVo = new TfpFlightSeatVo();
                        BeanUtils.copyProperties(tfpFlightSeat, tfpFlightSeatVo);

                        Long seatId = tfpFlightSeat.getFlightId();

                        List<TfpFlightSeatDetailVo> tfpFlightSeatDetailVoList = new ArrayList<>();
                        List<FlightSearchResp.SeatDetail> seatDetails = seatItem.getSeatDetails();
                        if (DcListUtils.isNotEmpty(seatDetails)){
                            for (FlightSearchResp.SeatDetail seatDetail : seatDetails) {
                                TfpFlightSeatDetail tfpFlightSeatDetail = new TfpFlightSeatDetail();
                                BeanUtils.copyProperties(seatDetail, tfpFlightSeatDetail);

                                tfpFlightSeatDetail.setFlightId(flightId);
                                tfpFlightSeatDetail.setSeatId(seatId);
                                tfpFlightSeatDetail.setDelFlag(DeleteEnum.EXIST.getValue());
                                tfpFlightSeatDetail.setCreateBy("航空接口");
                                tfpFlightSeatDetail.setCreateTime(DateUtils.getNowDate());

                                tfpFlightSeatDetailMapper.insertTfpFlightSeatDetail(tfpFlightSeatDetail);

                                TfpFlightSeatDetailVo tfpFlightSeatDetailVo = new TfpFlightSeatDetailVo();
                                BeanUtils.copyProperties(tfpFlightSeatDetail, tfpFlightSeatDetailVo);
                                tfpFlightSeatDetailVoList.add(tfpFlightSeatDetailVo);
                            }
                        }
                        tfpFlightSeatVo.setSeatDetails(tfpFlightSeatDetailVoList);

                        FlightSearchResp.Policy policys = seatItem.getPolicys();
                        if (Objects.nonNull(policys)){
                            TfpFlightPolicy tfpFlightPolicy = new TfpFlightPolicy();
                            BeanUtils.copyProperties(policys, tfpFlightPolicy);

                            StringBuffer certificatesStr = new StringBuffer();
                            List<String> certificatesList = policys.getCertificatesList();
                            if (DcListUtils.isNotEmpty(certificatesList)){
                                int size = certificatesList.size();
                                for (int i = 0; i < size; i++) {
                                    String certificates = certificatesList.get(i);
                                    if (i + 1 == size){
                                        certificatesStr.append(certificates);
                                    } else {
                                        certificatesStr.append(certificates + ",");
                                    }
                                }
                            }

                            tfpFlightPolicy.setCertificatesList(certificatesStr.toString());
                            tfpFlightPolicy.setFlightId(flightId);
                            tfpFlightPolicy.setSeatId(seatId);
                            tfpFlightPolicy.setDelFlag(DeleteEnum.EXIST.getValue());
                            tfpFlightPolicy.setCreateBy("航空接口");
                            tfpFlightPolicy.setCreateTime(DateUtils.getNowDate());

                            tfpFlightPolicyMapper.insertTfpFlightPolicy(tfpFlightPolicy);
                            Long policyId = tfpFlightPolicy.getId();

                            TfpFlightPolicyVo tfpFlightPolicyVo = new TfpFlightPolicyVo();
                            BeanUtils.copyProperties(tfpFlightPolicy, tfpFlightPolicyVo);

                            List<TfpFlightPriceDataVo> tfpFlightPriceDataVoList = new ArrayList<>();
                            List<FlightSearchResp.PriceData> priceDatas = policys.getPriceDatas();
                            if (DcListUtils.isNotEmpty(priceDatas)){
                                for (FlightSearchResp.PriceData priceData : priceDatas) {
                                    TfpFlightPriceData tfpFlightPriceData = new TfpFlightPriceData();
                                    BeanUtils.copyProperties(priceData, tfpFlightPriceData);

                                    tfpFlightPriceData.setFlightId(flightId);
                                    tfpFlightPriceData.setSeatId(seatId);
                                    tfpFlightPriceData.setPolicyId(policyId);
                                    tfpFlightPriceData.setDelFlag(DeleteEnum.EXIST.getValue());
                                    tfpFlightPriceData.setCreateBy("航空接口");
                                    tfpFlightPriceData.setCreateTime(DateUtils.getNowDate());

                                    tfpFlightPriceDataMapper.insertTfpFlightPriceData(tfpFlightPriceData);

                                    TfpFlightPriceDataVo tfpFlightPriceDataVo = new TfpFlightPriceDataVo();
                                    BeanUtils.copyProperties(tfpFlightPriceData, tfpFlightPriceDataVo);
                                    tfpFlightPriceDataVoList.add(tfpFlightPriceDataVo);
                                }
                            }

                            tfpFlightPolicyVo.setPriceDatas(tfpFlightPriceDataVoList);
                            tfpFlightSeatVo.setPolicys(tfpFlightPolicyVo);
                        }

                        seatVoList.add(tfpFlightSeatVo);
                    }
                }
                tfpFlightVo.setSeatItems(seatVoList);

                List<TfpFlightSegmentDetailVo> tfpFlightSegmentDetailVoList = new ArrayList<>();
                List<FlightSearchResp.SegmentDetail> segmentDetails = flightSearchResp.getSegmentDetails();
                if (DcListUtils.isNotEmpty(segmentDetails)){
                    for (FlightSearchResp.SegmentDetail segmentDetail : segmentDetails) {
                        TfpFlightSegmentDetail tfpFlightSegmentDetail = new TfpFlightSegmentDetail();
                        BeanUtils.copyProperties(segmentDetail, tfpFlightSegmentDetail);

                        tfpFlightSegmentDetail.setFlightId(flightId);
                        tfpFlightSegmentDetail.setDelFlag(DeleteEnum.EXIST.getValue());
                        tfpFlightSegmentDetail.setCreateBy("航空接口");
                        tfpFlightSegmentDetail.setCreateTime(DateUtils.getNowDate());

                        tfpFlightSegmentDetailMapper.insertTfpFlightSegmentDetail(tfpFlightSegmentDetail);

                        TfpFlightSegmentDetailVo tfpFlightSegmentDetailVo = new TfpFlightSegmentDetailVo();
                        BeanUtils.copyProperties(tfpFlightSegmentDetail, tfpFlightSegmentDetailVo);
                        tfpFlightSegmentDetailVoList.add(tfpFlightSegmentDetailVo);
                    }
                }
                tfpFlightVo.setSegmentDetails(tfpFlightSegmentDetailVoList);

                List<TfpFlightStopInfoVo> tfpFlightStopInfoVoList = new ArrayList<>();
                List<FlightSearchResp.StopInfo> stopInfos = flightSearchResp.getStopInfos();
                if (DcListUtils.isNotEmpty(stopInfos)){
                    for (FlightSearchResp.StopInfo stopInfo : stopInfos) {
                        TfpFlightStopInfo tfpFlightStopInfo = new TfpFlightStopInfo();
                        BeanUtils.copyProperties(stopInfo, tfpFlightStopInfo);

                        tfpFlightStopInfo.setFlightId(flightId);
                        tfpFlightStopInfo.setDelFlag(DeleteEnum.EXIST.getValue());
                        tfpFlightStopInfo.setCreateBy("航空接口");
                        tfpFlightStopInfo.setCreateTime(DateUtils.getNowDate());

                        tfpFlightStopInfoMapper.insertTfpFlightStopInfo(tfpFlightStopInfo);

                        TfpFlightStopInfoVo tfpFlightStopInfoVo = new TfpFlightStopInfoVo();
                        BeanUtils.copyProperties(tfpFlightStopInfo, tfpFlightStopInfoVo);
                        tfpFlightStopInfoVoList.add(tfpFlightStopInfoVo);
                    }
                }

                tfpFlightVo.setStopInfos(tfpFlightStopInfoVoList);
                tfpFlightVoList.add(tfpFlightVo);
            }
        }

        return tfpFlightVoList;
    }
}
