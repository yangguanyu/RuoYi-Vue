package com.ruoyi.common.core.domain.wx;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "WxLoginBody", description = "微信登录dto")
public class WxLoginBody {
    @ApiModelProperty("登录校验code")
    private String code;
    @ApiModelProperty("获取手机号code")
    private String phoneCode;
}
