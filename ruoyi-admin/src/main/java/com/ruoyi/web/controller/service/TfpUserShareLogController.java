package com.ruoyi.web.controller.service;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.service.dto.RecordUserShareLogDto;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.TfpUserShareLog;
import com.ruoyi.service.service.ITfpUserShareLogService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 用户分享日志Controller
 * 
 * @author ruoyi
 * @date 2024-01-25
 */
@RestController
@RequestMapping("/service/userShareLog")
public class TfpUserShareLogController extends BaseController
{
    @Autowired
    private ITfpUserShareLogService tfpUserShareLogService;

    /**
     * 查询用户分享日志列表
     */
    @PreAuthorize("@ss.hasPermi('service:userShareLog:list')")
    @GetMapping("/list")
    public TableDataInfo list(TfpUserShareLog tfpUserShareLog)
    {
        startPage();
        List<TfpUserShareLog> list = tfpUserShareLogService.selectTfpUserShareLogList(tfpUserShareLog);
        return getDataTable(list);
    }

    /**
     * 导出用户分享日志列表
     */
    @PreAuthorize("@ss.hasPermi('service:userShareLog:export')")
    @Log(title = "用户分享日志", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TfpUserShareLog tfpUserShareLog)
    {
        List<TfpUserShareLog> list = tfpUserShareLogService.selectTfpUserShareLogList(tfpUserShareLog);
        ExcelUtil<TfpUserShareLog> util = new ExcelUtil<TfpUserShareLog>(TfpUserShareLog.class);
        util.exportExcel(response, list, "用户分享日志数据");
    }

    /**
     * 获取用户分享日志详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:userShareLog:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tfpUserShareLogService.selectTfpUserShareLogById(id));
    }

    /**
     * 新增用户分享日志
     */
    @PreAuthorize("@ss.hasPermi('service:userShareLog:add')")
    @Log(title = "用户分享日志", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TfpUserShareLog tfpUserShareLog)
    {
        return toAjax(tfpUserShareLogService.insertTfpUserShareLog(tfpUserShareLog));
    }

    /**
     * 修改用户分享日志
     */
    @PreAuthorize("@ss.hasPermi('service:userShareLog:edit')")
    @Log(title = "用户分享日志", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TfpUserShareLog tfpUserShareLog)
    {
        return toAjax(tfpUserShareLogService.updateTfpUserShareLog(tfpUserShareLog));
    }

    /**
     * 删除用户分享日志
     */
    @PreAuthorize("@ss.hasPermi('service:userShareLog:remove')")
    @Log(title = "用户分享日志", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tfpUserShareLogService.deleteTfpUserShareLogByIds(ids));
    }

    @ApiOperation("小程序-记录用户分享日志")
    @PostMapping("/recordUserShareLog")
    @ResponseBody
    public R recordUserShareLog(@RequestBody RecordUserShareLogDto recordUserShareLogDto){
        return R.ok(tfpUserShareLogService.recordUserShareLog(recordUserShareLogDto));
    }
}
