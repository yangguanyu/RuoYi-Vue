package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.TfpComateOrder;
import org.apache.ibatis.annotations.Param;

/**
 * 同行人和订单关联Mapper接口
 *
 * @author ruoyi
 * @date 2023-10-31
 */
public interface TfpComateOrderMapper
{
    /**
     * 查询同行人和订单关联
     *
     * @param id 同行人和订单关联主键
     * @return 同行人和订单关联
     */
    public TfpComateOrder selectTfpComateOrderById(Long id);

    /**
     * 查询同行人和订单关联列表
     *
     * @param tfpComateOrder 同行人和订单关联
     * @return 同行人和订单关联集合
     */
    public List<TfpComateOrder> selectTfpComateOrderList(TfpComateOrder tfpComateOrder);

    /**
     * 新增同行人和订单关联
     *
     * @param tfpComateOrder 同行人和订单关联
     * @return 结果
     */
    public int insertTfpComateOrder(TfpComateOrder tfpComateOrder);

    /**
     * 修改同行人和订单关联
     *
     * @param tfpComateOrder 同行人和订单关联
     * @return 结果
     */
    public int updateTfpComateOrder(TfpComateOrder tfpComateOrder);

    /**
     * 删除同行人和订单关联
     *
     * @param id 同行人和订单关联主键
     * @return 结果
     */
    public int deleteTfpComateOrderById(Long id);

    /**
     * 批量删除同行人和订单关联
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpComateOrderByIds(Long[] ids);

    List<TfpComateOrder> selectTfpComateOrderByMateIdList(@Param("mateIdList") List<Long> mateIdList);

    List<TfpComateOrder> selectTfpComateOrderListByOrderIdList(@Param("orderIdList") List<Long> orderIdList);
}
