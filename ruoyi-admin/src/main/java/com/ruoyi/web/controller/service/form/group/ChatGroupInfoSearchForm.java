package com.ruoyi.web.controller.service.form.group;

import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * 聊天群组成员聊天信息对象 tfp_chat_group_person_info
 *
 * @author ruoyi
 * @date 2023-10-31
 */
@ApiModel(value = "ChatGroupInfoSearchForm", description = "获取聊天信息列表")
@Data
public class ChatGroupInfoSearchForm extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("当前最后一句话的id，我们查大于这句话的数据")
    private Long currentId;

    /** 聊天群组id */
    @ApiModelProperty("群组id")
    @NotNull(message = "群组id不能为空")
    private Long chatGroupId;

    /** 聊天群组成员id */
    @ApiModelProperty("最后一个人发言的时间")
    private Date endTime;

    @ApiModelProperty("聊天人的id,目前列表中不需要传这个")
    private Long chatPersonInfoId;

}
