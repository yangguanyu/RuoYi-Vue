package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "WantToGoDto", description = "商品想去dto")
public class WantToGoDto {
    @ApiModelProperty("商品id")
    private Long id;
    @ApiModelProperty("想去类型 0-取消 1-想去")
    private Integer wantToGoType;
}
