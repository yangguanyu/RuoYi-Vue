package com.ruoyi.service.dto;

import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 商品主对象 tfp_product
 * 
 * @author ruoyi
 * @date 2023-11-01
 */
@Data
@ApiModel(value = "TfpProductDto")
public class TfpProductDto extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 客服id */
    private Long customerServiceId;

    /** 商品归属 0-国内 1-海外 */
    @NotNull(message = "商品归属不能为空")
    private Integer commoditiesType;

    /** 业务地区(1级) 能够选多个地区 下拉 */
    private Long parentAreaId;

    /** 业务地区(1级) 能够选多个地区 下拉 */
    private String parentAreaName;

    /** 业务地区(2级) 能够选多个地区 下拉 */
    private Long areaId;

    /** 业务地区(2级) 能够选多个地区 下拉 */
    private String areaName;

    /** 业务地区(3级) 能够选多个地区 下拉 */
    @NotNull(message = "业务地区(3级)不能为空")
    private Long cityId;

    /** 业务地区(3级) 能够选多个地区 下拉 */
    private String cityName;

    /** 服务类型id 散拼 单团 单项 下拉（怎么和咱们的全国散，一家一团对应） */
    @NotNull(message = "服务类型不能为空")
    private Long categoryId;

    /** 服务类型名称 散拼 单团 单项 下拉（怎么和咱们的全国散，一家一团对应） */
    @NotNull(message = "服务类型不能为空")
    private String categoryName;

    /** 线路类型 出发地参团 目的地参团 字典code */
    @NotNull(message = "线路类型不能为空")
    private String lineTypeCode;

    /** 线路类型 出发地参团 目的地参团 字典name */
    @NotNull(message = "线路类型不能为空")
    private String lineTypeName;

    /** 商品名称 */
    @NotNull(message = "商品名称不能为空")
    private String productName;

    /** 商品编号 */
    @NotNull(message = "商品编号不能为空")
    private String productNo;

    /** 供应商名称 */
    private String supplierName;

    /** 供应商id */
    private Long supplierId;

    /** 线路亮点 */
    private String routeHighlight;

    /** 儿童标准 */
    private Integer age;

    /** 儿童标准-年龄-最小值 */
    @NotNull(message = "儿童标准不能为空")
    private Integer childrenAgeLimitMin;

    /** 儿童标准-年龄-最大值 */
    @NotNull(message = "儿童标准不能为空")
    private Integer childrenAgeLimitMax;

    /** 儿童标准说明 */
    private String ageIntro;

    /** 出发城市id */
    @NotNull(message = "出发城市不能为空")
    private Long goPlaceId;

    /** 出发城市名称 */
    private String goPlaceName;

    /** 返回城市id */
    @NotNull(message = "返回城市不能为空")
    private Long backPlaceId;

    /** 返回城市名称 */
    private String backPlaceName;

    /** 字典 去程交通 飞机 火车 轮船 汽车 高铁 其它 */
    private String goTrafficCode;

    /** 去程交通 飞机 火车 轮船 汽车 高铁 其它 */
    private String goTrafficName;

    /** dict 回程交通 */
    private String backTrafficCode;

    /** 回程交通 */
    private String backTrafficName;

    /** 行程天数 几天 */
    @NotNull(message = "行程天数(几天)不能为空")
    private Integer dayNum;

    /** 行程天数 几晚 */
    @NotNull(message = "行程天数(几晚)不能为空")
    private Integer nightNum;

    /** 是否包含保险 单选框 0-否 1-是 */
    @NotNull(message = "是否包含保险不能为空")
    private Integer isInsurance;

    /** 保险公司 */
    private String insuranceCompany;

    /** 险种code：人身意外险 旅游团队先 */
    private String insuranceTypeCode;

    /** 险种名称：人身意外险 旅游团队先 */
    private String insuranceTypeName;

    /** 保险具体内容 */
    private String insuranceContent;

    /** 商品状态 0-待审核 1-审核通过 2-审核不通过 3-草稿*/
    private Integer itemStatus;

    /** 航空公司code 下拉 需要爬数据 */
    private String airCompanyCode;

    /** 航空公司名称 下拉 需要爬数据 */
    private String airCompanyName;

    /** 交通说明 */
    private String trafficInstructions;

    /** 是否有购物 0-否 1-是 */
    @NotNull(message = "是否有购物不能为空")
    private Integer purchaseFlag;

    /** 是否自费项目 0-否 1-是 */
    @NotNull(message = "是否自费项目不能为空")
    private Integer selfFundedProjectFlag;

    /** 提前截止天数 */
    @NotNull(message = "提前截止天数不能为空")
    private Integer earlyDeadlineDay;

    /** 资源确认方式 1：二次确认  2：即使确认 */
    @NotNull(message = "资源确认方式不能为空")
    private Integer confirmationType;

    /** 分值，定时任务10分钟 */
    private Long score;

    /** 计划出团人数 */
    @NotNull(message = "计划出团人数不能为空")
    private Integer expectedPersonNumber;

    /** 预收数 */
    @NotNull(message = "预收数不能为空")
    private Integer expectedNumber;

    /** 最低成团人数 */
    @NotNull(message = "最低成团人数不能为空")
    private Integer minPersonNumber;

    /** 成人成本价 */
    @NotNull(message = "成人成本价不能为空")
    private BigDecimal adultCostPrice;

    /** 儿童成本价 */
    private BigDecimal childrenCostPrice;

    /** 建议成人零售价 */
    private BigDecimal adultRetailPrice;

    // TODO
    /** 起点价格 */
//    @NotNull(message = "起点价格不能为空")
    private BigDecimal startingPointPrice;

    /** 建议儿童零售价 */
    private BigDecimal childrenRetailPrice;

    /** 单房差成本价 */
    @NotNull(message = "单房差成本价不能为空")
    private BigDecimal singleSupplementCostPrice;

    /** 订金 */
    @NotNull(message = "订金不能为空")
    private BigDecimal deposit;

    /** 自备签成本价 */
    private BigDecimal selfPreparedSignatureCostPrice;

    /** 升舱成本价 */
    private BigDecimal upgradeCostPrice;

    /** 拒签成本价 */
    private BigDecimal refusalCostPrice;

    /** 签证费成本价 */
    private BigDecimal visaFeeCostPrice;

    /** 特殊加项成本价 */
    private BigDecimal specialAdditionCostPrice;

    /** 特殊减项成本价 */
    private BigDecimal specialDeductionsCostPrice;

    /** 学生成本价 */
    private BigDecimal studentCostPrice;

    /** 老年人成本价 */
    private BigDecimal oldPeopleCostPrice;

    /** 购物店数量对象 */
    private List<TfpPurchaseShopNumberDto> purchaseList;

    /** 自费项目对象 */
    private List<TfpSelfFundedProjectDto> selfFundedProjectList;

    /** 合作分公司id集合 */
    @NotNull(message = "合作分公司不能为空")
    private List<Long> cooperativeBranchCompanyIdList;

    @ApiModelProperty("商品图片url集合")
    private List<String> productImageUrlList;
    @ApiModelProperty("线路亮点图片url集合")
    private List<String> routeHighlightUrlList;
    @ApiModelProperty("商品封面图片url集合")
    private List<String> productCoverImageUrlList;
    @ApiModelProperty("商品描述")
    private String productDescribe;

    @ApiModelProperty("是否选择低价机票服务 0-否 1-是")
    private Integer lowPriceAirTicketServiceFlag;

    @NotNull
    @ApiModelProperty("团期截止集合")
    private List<DepartureDateDto> departureDateList;
}
