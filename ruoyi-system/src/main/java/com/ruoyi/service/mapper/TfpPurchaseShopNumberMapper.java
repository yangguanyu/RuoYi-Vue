package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.TfpPurchaseShopNumber;
import org.apache.ibatis.annotations.Param;

/**
 * 购物店数量Mapper接口
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public interface TfpPurchaseShopNumberMapper 
{
    /**
     * 查询购物店数量
     * 
     * @param id 购物店数量主键
     * @return 购物店数量
     */
    public TfpPurchaseShopNumber selectTfpPurchaseShopNumberById(Long id);

    /**
     * 查询购物店数量列表
     * 
     * @param tfpPurchaseShopNumber 购物店数量
     * @return 购物店数量集合
     */
    public List<TfpPurchaseShopNumber> selectTfpPurchaseShopNumberList(TfpPurchaseShopNumber tfpPurchaseShopNumber);

    /**
     * 新增购物店数量
     * 
     * @param tfpPurchaseShopNumber 购物店数量
     * @return 结果
     */
    public int insertTfpPurchaseShopNumber(TfpPurchaseShopNumber tfpPurchaseShopNumber);

    /**
     * 修改购物店数量
     * 
     * @param tfpPurchaseShopNumber 购物店数量
     * @return 结果
     */
    public int updateTfpPurchaseShopNumber(TfpPurchaseShopNumber tfpPurchaseShopNumber);

    /**
     * 删除购物店数量
     * 
     * @param id 购物店数量主键
     * @return 结果
     */
    public int deleteTfpPurchaseShopNumberById(Long id);

    /**
     * 批量删除购物店数量
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpPurchaseShopNumberByIds(Long[] ids);

    void deleteByProductId(@Param("productId") Long productId);

    List<TfpPurchaseShopNumber> selectListByProductId(@Param("productId") Long productId);

    void deleteTfpPurchaseShopNumberByProductIds(@Param("productIdList") List<Long> productIdList);
}
