package com.ruoyi.service.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "MyProductSnapshotListVo", description = "我的商品快照列表")
public class MyProductSnapshotListVo {
    @ApiModelProperty("商品id")
    private Long id;

    @ApiModelProperty("商品名称")
    private String productName;
    @ApiModelProperty("服务类型id 1-全国散 2-一家一团 3-自营 4-全国散-小包团")
    private Long categoryId;
    @ApiModelProperty("服务类型名称")
    private String categoryName;

    @ApiModelProperty("线路亮点集合")
    private List<String> routeHighlightList;
    @ApiModelProperty("商品图片url集合")
    private List<String> productImageUrlList;
}
