package com.ruoyi.service.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 聊天群组对象 tfp_chat_group
 *
 * @author ruoyi
 * @date 2023-10-31
 */
@Data
@ApiModel
public class TfpChatGroupVo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty("主键")
    private Long id;

    /** 聊天群组名称 */
    @ApiModelProperty("聊天群组名称")
    private String name;

    /** 商品id */
    @ApiModelProperty("商品id")
    private Long productId;

    /** 商品名称 */
    @ApiModelProperty("商品名称")
    private String productName;

    /** 限制人数 */
    @ApiModelProperty("限制人数")
    private Integer limitPersonNumber;

    /** 报名人数 */
    @ApiModelProperty("限制人数")
    private Integer signUpPersonNumber;

    /** 出发日期 */
    @ApiModelProperty("出发日期")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date goDate;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    private Long createUserId;

    /** 更新者id */
    private Long updateUserId;

    @ApiModelProperty("已报名人数")
    private Integer peopleCount;

    @ApiModelProperty("已报名人员的信息")
    private List<UserVo> userList;

}
