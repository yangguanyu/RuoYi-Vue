package com.ruoyi.service.mapper;

import java.util.Date;
import java.util.List;
import com.ruoyi.service.domain.TfpChatGroup;
import com.ruoyi.service.dto.ChatGroupStatisticsDto;
import com.ruoyi.service.dto.TfpChatGroupDto;
import com.ruoyi.service.vo.JoinChatGroupVo;
import com.ruoyi.service.vo.MoreListMonthGroupVo;
import com.ruoyi.service.vo.TfpChatGroupVo;
import org.apache.ibatis.annotations.Param;

/**
 * 聊天群组Mapper接口
 *
 * @author ruoyi
 * @date 2023-10-31
 */
public interface TfpChatGroupMapper
{
    /**
     * 查询聊天群组
     *
     * @param id 聊天群组主键
     * @return 聊天群组
     */
    public TfpChatGroup selectTfpChatGroupById(Long id);

    /**
     * 查询聊天群组列表
     *
     * @param tfpChatGroup 聊天群组
     * @return 聊天群组集合
     */
    public List<TfpChatGroup> selectTfpChatGroupList(TfpChatGroup tfpChatGroup);

    /**
     * 新增聊天群组
     *
     * @param tfpChatGroup 聊天群组
     * @return 结果
     */
    public int insertTfpChatGroup(TfpChatGroup tfpChatGroup);

    /**
     * 修改聊天群组
     *
     * @param tfpChatGroup 聊天群组
     * @return 结果
     */
    public int updateTfpChatGroup(TfpChatGroup tfpChatGroup);

    /**
     * 删除聊天群组
     *
     * @param id 聊天群组主键
     * @return 结果
     */
    public int deleteTfpChatGroupById(Long id);

    /**
     * 批量删除聊天群组
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpChatGroupByIds(Long[] ids);

    List<TfpChatGroupVo> selectTfpChatGroupVoList(TfpChatGroupDto tfpChatGroupDto);

    List<TfpChatGroupVo> selectChatGroupsByProductId(TfpChatGroupDto tfpChatGroupDto);

    List<TfpChatGroup> selectTfpChatGroupListByIdList(@Param("idList") List<Long> idList);

    List<ChatGroupStatisticsDto> selectGroupPersonList(@Param("productIdList") List<Long> productIdList);

    List<TfpChatGroup> selectTfpChatGroupListByProductIdList(@Param("productIdList") List<Long> productIdList);

    TfpChatGroup getLastGroupByProductIdAndGoDate(@Param("productId") Long productId, @Param("goDate") Date goDate);

    List<TfpChatGroup> selectGroupByProductIdAndDate(@Param("productId") Long productId, @Param("startDate") Date startDate, @Param("endDate") Date endDate);

    void deleteTfpChatGroupByProductIds(@Param("productIdList") List<Long> productIdList);

    List<MoreListMonthGroupVo> moreGroupList(@Param("productId") Long productId, @Param("startDate") Date startDate, @Param("endDate") Date endDate);

    List<JoinChatGroupVo> visitorList(@Param("idList") List<Long> idList);
}
