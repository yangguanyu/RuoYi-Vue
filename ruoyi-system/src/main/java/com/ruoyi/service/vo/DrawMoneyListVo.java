package com.ruoyi.service.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@ApiModel("DrawMoneyListVo")
public class DrawMoneyListVo {
    @ApiModelProperty("提现id")
    private Long id;
    @ApiModelProperty("提款金额")
    private BigDecimal drawMoneyPrice;
    @ApiModelProperty("提现编号")
    private String drawMoneyNumber;
    @ApiModelProperty("创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    @ApiModelProperty("银行卡号")
    private String bankCardNumber;
    @ApiModelProperty("银行名称")
    private String bankName;
    @ApiModelProperty("状态 0-提交申请/审核中 1-提现完成 2-提现失败/审核不通过")
    private Integer status;
    @ApiModelProperty("审核不通过原因")
    private String reason;
}
