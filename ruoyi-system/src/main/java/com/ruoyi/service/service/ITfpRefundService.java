package com.ruoyi.service.service;

import java.util.List;
import com.ruoyi.service.domain.TfpRefund;
import com.ruoyi.service.dto.ApplyRefundDto;
import com.ruoyi.service.dto.AuditDto;
import com.ruoyi.service.dto.AuditRefundDto;
import com.ruoyi.service.dto.TfpRefundDto;
import com.ruoyi.service.vo.ApplyRefundDetailVo;
import com.ruoyi.service.vo.RefundDetailVo;
import com.ruoyi.service.vo.TfpRefundVo;
import com.ruoyi.service.vo.UserVo;

/**
 * 退款Service接口
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public interface ITfpRefundService 
{
    /**
     * 查询退款
     * 
     * @param id 退款主键
     * @return 退款
     */
    public TfpRefund selectTfpRefundById(Long id);

    /**
     * 查询退款列表
     * 
     * @param tfpRefund 退款
     * @return 退款集合
     */
    public List<TfpRefund> selectTfpRefundList(TfpRefund tfpRefund);

    List<TfpRefundVo> selectTfpRefundVoList(TfpRefundDto tfpRefundDto);

    /**
     * 新增退款
     * 
     * @param tfpRefund 退款
     * @return 结果
     */
    public int insertTfpRefund(TfpRefund tfpRefund);

    /**
     * 修改退款
     * 
     * @param tfpRefund 退款
     * @return 结果
     */
    public int updateTfpRefund(TfpRefund tfpRefund);

    /**
     * 批量删除退款
     * 
     * @param ids 需要删除的退款主键集合
     * @return 结果
     */
    public int deleteTfpRefundByIds(Long[] ids);

    /**
     * 删除退款信息
     * 
     * @param id 退款主键
     * @return 结果
     */
    public int deleteTfpRefundById(Long id);

    List<UserVo> refundUserSelect();

    ApplyRefundDetailVo applyRefundDetail(Long orderId);

    int applyRefund(ApplyRefundDto applyRefundDto);

    RefundDetailVo refundDetail(Long id);

    int auditRefund(AuditRefundDto auditRefundDto);

    List<TfpRefund> selectCompleteTfpRefundListByRefundType(Integer refundType);

    List<TfpRefund> selectCompleteTfpRefundListByOrderIdList(List<Long> orderIdList);

    TfpRefund selectCompleteTfpRefundByOrderId(Long orderId);
}
