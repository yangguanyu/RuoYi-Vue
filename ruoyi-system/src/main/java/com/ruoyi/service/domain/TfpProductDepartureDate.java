package com.ruoyi.service.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 商品班期对象 tfp_product_departure_date
 * 
 * @author ruoyi
 * @date 2024-04-17
 */
public class TfpProductDepartureDate extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 主表id */
    @Excel(name = "主表id")
    private Long productId;

    /** 班期开始 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "班期开始", width = 30, dateFormat = "yyyy-MM-dd")
    private Date departureDateStart;

    /** 班期截止 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "班期截止", width = 30, dateFormat = "yyyy-MM-dd")
    private Date departureDateEnd;

    /** 成人零售价 */
    @Excel(name = "成人零售价")
    private BigDecimal adultRetailPrice;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    @Excel(name = "创建者id")
    private Long createUserId;

    /** 更新者id */
    @Excel(name = "更新者id")
    private Long updateUserId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setProductId(Long productId) 
    {
        this.productId = productId;
    }

    public Long getProductId() 
    {
        return productId;
    }
    public void setDepartureDateStart(Date departureDateStart) 
    {
        this.departureDateStart = departureDateStart;
    }

    public Date getDepartureDateStart() 
    {
        return departureDateStart;
    }
    public void setDepartureDateEnd(Date departureDateEnd) 
    {
        this.departureDateEnd = departureDateEnd;
    }

    public Date getDepartureDateEnd() 
    {
        return departureDateEnd;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }
    public void setUpdateUserId(Long updateUserId) 
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId() 
    {
        return updateUserId;
    }

    public BigDecimal getAdultRetailPrice() {
        return adultRetailPrice;
    }

    public void setAdultRetailPrice(BigDecimal adultRetailPrice) {
        this.adultRetailPrice = adultRetailPrice;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("productId", getProductId())
            .append("departureDateStart", getDepartureDateStart())
            .append("departureDateEnd", getDepartureDateEnd())
            .append("adultRetailPrice", getAdultRetailPrice())
            .append("delFlag", getDelFlag())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
