package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.TfpCooperativeBranchCompany;
import com.ruoyi.service.vo.SupplierVo;
import org.apache.ibatis.annotations.Param;

/**
 * 合作分公司Mapper接口
 * 
 * @author ruoyi
 * @date 2023-12-12
 */
public interface TfpCooperativeBranchCompanyMapper 
{
    /**
     * 查询合作分公司
     * 
     * @param id 合作分公司主键
     * @return 合作分公司
     */
    public TfpCooperativeBranchCompany selectTfpCooperativeBranchCompanyById(Long id);

    /**
     * 查询合作分公司列表
     * 
     * @param tfpCooperativeBranchCompany 合作分公司
     * @return 合作分公司集合
     */
    public List<TfpCooperativeBranchCompany> selectTfpCooperativeBranchCompanyList(TfpCooperativeBranchCompany tfpCooperativeBranchCompany);

    /**
     * 新增合作分公司
     * 
     * @param tfpCooperativeBranchCompany 合作分公司
     * @return 结果
     */
    public int insertTfpCooperativeBranchCompany(TfpCooperativeBranchCompany tfpCooperativeBranchCompany);

    /**
     * 修改合作分公司
     * 
     * @param tfpCooperativeBranchCompany 合作分公司
     * @return 结果
     */
    public int updateTfpCooperativeBranchCompany(TfpCooperativeBranchCompany tfpCooperativeBranchCompany);

    /**
     * 删除合作分公司
     * 
     * @param id 合作分公司主键
     * @return 结果
     */
    public int deleteTfpCooperativeBranchCompanyById(Long id);

    /**
     * 批量删除合作分公司
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpCooperativeBranchCompanyByIds(Long[] ids);

    List<SupplierVo> cooperativeBranchCompanySelect();

    List<TfpCooperativeBranchCompany> selectTfpCooperativeBranchCompanyByIdList(@Param("idList") List<Long> idList);
}
