package com.ruoyi.service.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 用户银行卡对象 tfp_user_bank_card
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public class TfpUserBankCard extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 银行id */
    @Excel(name = "银行id")
    private String bankId;

    /** 银行名称 */
    @Excel(name = "银行名称")
    private String bankName;

    /** 银行卡号 */
    @Excel(name = "银行卡号")
    private String bankCardNumber;

    /** 开户网点 */
    @Excel(name = "开户网点")
    private String accountOpeningBranch;

    /** 账户姓名 */
    @Excel(name = "账户姓名")
    private String userName;

    /** 预留电话 */
    @Excel(name = "预留电话")
    private String phoneNumber;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    @Excel(name = "创建者id")
    private Long createUserId;

    /** 更新者id */
    @Excel(name = "更新者id")
    private Long updateUserId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setBankId(String bankId) 
    {
        this.bankId = bankId;
    }

    public String getBankId() 
    {
        return bankId;
    }
    public void setBankName(String bankName) 
    {
        this.bankName = bankName;
    }

    public String getBankName() 
    {
        return bankName;
    }
    public void setBankCardNumber(String bankCardNumber) 
    {
        this.bankCardNumber = bankCardNumber;
    }

    public String getBankCardNumber() 
    {
        return bankCardNumber;
    }
    public void setAccountOpeningBranch(String accountOpeningBranch) 
    {
        this.accountOpeningBranch = accountOpeningBranch;
    }

    public String getAccountOpeningBranch() 
    {
        return accountOpeningBranch;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setPhoneNumber(String phoneNumber) 
    {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber() 
    {
        return phoneNumber;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }
    public void setUpdateUserId(Long updateUserId) 
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId() 
    {
        return updateUserId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("bankId", getBankId())
            .append("bankName", getBankName())
            .append("bankCardNumber", getBankCardNumber())
            .append("accountOpeningBranch", getAccountOpeningBranch())
            .append("userName", getUserName())
            .append("phoneNumber", getPhoneNumber())
            .append("delFlag", getDelFlag())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
