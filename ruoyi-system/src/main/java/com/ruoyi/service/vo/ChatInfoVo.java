package com.ruoyi.service.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel(value = "ChatInfoVo", description = "聊天信息")
public class ChatInfoVo {
    @ApiModelProperty("用户名称")
    private String userName;
    @ApiModelProperty("聊天信息")
    private String chatInfo;
    @ApiModelProperty("聊天时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date chatDate;
}
