package com.ruoyi.web.controller.service;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.TfpPurchaseShopNumber;
import com.ruoyi.service.service.ITfpPurchaseShopNumberService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 购物店数量Controller
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@RestController
@RequestMapping("/service/purchaseShopNumber")
public class TfpPurchaseShopNumberController extends BaseController
{
    @Autowired
    private ITfpPurchaseShopNumberService tfpPurchaseShopNumberService;

    /**
     * 查询购物店数量列表
     */
    @PreAuthorize("@ss.hasPermi('service:purchaseShopNumber:list')")
    @GetMapping("/list")
    public TableDataInfo list(TfpPurchaseShopNumber tfpPurchaseShopNumber)
    {
        startPage();
        List<TfpPurchaseShopNumber> list = tfpPurchaseShopNumberService.selectTfpPurchaseShopNumberList(tfpPurchaseShopNumber);
        return getDataTable(list);
    }

    /**
     * 导出购物店数量列表
     */
    @PreAuthorize("@ss.hasPermi('service:purchaseShopNumber:export')")
    @Log(title = "购物店数量", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TfpPurchaseShopNumber tfpPurchaseShopNumber)
    {
        List<TfpPurchaseShopNumber> list = tfpPurchaseShopNumberService.selectTfpPurchaseShopNumberList(tfpPurchaseShopNumber);
        ExcelUtil<TfpPurchaseShopNumber> util = new ExcelUtil<TfpPurchaseShopNumber>(TfpPurchaseShopNumber.class);
        util.exportExcel(response, list, "购物店数量数据");
    }

    /**
     * 获取购物店数量详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:purchaseShopNumber:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tfpPurchaseShopNumberService.selectTfpPurchaseShopNumberById(id));
    }

    /**
     * 新增购物店数量
     */
    @PreAuthorize("@ss.hasPermi('service:purchaseShopNumber:add')")
    @Log(title = "购物店数量", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TfpPurchaseShopNumber tfpPurchaseShopNumber)
    {
        return toAjax(tfpPurchaseShopNumberService.insertTfpPurchaseShopNumber(tfpPurchaseShopNumber));
    }

    /**
     * 修改购物店数量
     */
    @PreAuthorize("@ss.hasPermi('service:purchaseShopNumber:edit')")
    @Log(title = "购物店数量", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TfpPurchaseShopNumber tfpPurchaseShopNumber)
    {
        return toAjax(tfpPurchaseShopNumberService.updateTfpPurchaseShopNumber(tfpPurchaseShopNumber));
    }

    /**
     * 删除购物店数量
     */
    @PreAuthorize("@ss.hasPermi('service:purchaseShopNumber:remove')")
    @Log(title = "购物店数量", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tfpPurchaseShopNumberService.deleteTfpPurchaseShopNumberByIds(ids));
    }
}
