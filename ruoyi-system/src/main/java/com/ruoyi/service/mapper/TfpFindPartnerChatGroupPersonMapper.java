package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.TfpFindPartnerChatGroupPerson;
import com.ruoyi.service.dto.TfpFindPartnerChatGroupPersonDto;
import com.ruoyi.service.vo.TfpFindPartnerChatGroupPersonVo;
import org.apache.ibatis.annotations.Param;

/**
 * 找搭子聊天群组成员Mapper接口
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public interface TfpFindPartnerChatGroupPersonMapper 
{
    /**
     * 查询找搭子聊天群组成员
     * 
     * @param id 找搭子聊天群组成员主键
     * @return 找搭子聊天群组成员
     */
    public TfpFindPartnerChatGroupPerson selectTfpFindPartnerChatGroupPersonById(Long id);

    /**
     * 查询找搭子聊天群组成员列表
     * 
     * @param tfpFindPartnerChatGroupPerson 找搭子聊天群组成员
     * @return 找搭子聊天群组成员集合
     */
    public List<TfpFindPartnerChatGroupPerson> selectTfpFindPartnerChatGroupPersonList(TfpFindPartnerChatGroupPerson tfpFindPartnerChatGroupPerson);

    /**
     * 新增找搭子聊天群组成员
     * 
     * @param tfpFindPartnerChatGroupPerson 找搭子聊天群组成员
     * @return 结果
     */
    public int insertTfpFindPartnerChatGroupPerson(TfpFindPartnerChatGroupPerson tfpFindPartnerChatGroupPerson);

    /**
     * 修改找搭子聊天群组成员
     * 
     * @param tfpFindPartnerChatGroupPerson 找搭子聊天群组成员
     * @return 结果
     */
    public int updateTfpFindPartnerChatGroupPerson(TfpFindPartnerChatGroupPerson tfpFindPartnerChatGroupPerson);

    /**
     * 删除找搭子聊天群组成员
     * 
     * @param id 找搭子聊天群组成员主键
     * @return 结果
     */
    public int deleteTfpFindPartnerChatGroupPersonById(Long id);

    /**
     * 批量删除找搭子聊天群组成员
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpFindPartnerChatGroupPersonByIds(Long[] ids);

    List<TfpFindPartnerChatGroupPersonVo> selectTfpFindPartnerChatGroupPersonVoList(TfpFindPartnerChatGroupPersonDto tfpFindPartnerChatGroupPersonDto);

    List<TfpFindPartnerChatGroupPerson> selectListByFindPartnerChatGroupId(@Param("findPartnerChatGroupId") Long findPartnerChatGroupId);

    TfpFindPartnerChatGroupPerson selectByGroupIdAndUserId(@Param("findPartnerChatGroupId") Long findPartnerChatGroupId, @Param("userId") Long userId);
}
