import request from '@/utils/request'

// 查询找搭子聊天群组成员列表
export function listFindPartnerChatGroupPerson(query) {
  return request({
    url: '/service/findPartnerChatGroupPerson/list',
    method: 'get',
    params: query
  })
}

// 查询找搭子聊天群组成员详细
export function getFindPartnerChatGroupPerson(id) {
  return request({
    url: '/service/findPartnerChatGroupPerson/' + id,
    method: 'get'
  })
}

// 新增找搭子聊天群组成员
export function addFindPartnerChatGroupPerson(data) {
  return request({
    url: '/service/findPartnerChatGroupPerson',
    method: 'post',
    data: data
  })
}

// 修改找搭子聊天群组成员
export function updateFindPartnerChatGroupPerson(data) {
  return request({
    url: '/service/findPartnerChatGroupPerson',
    method: 'put',
    data: data
  })
}

// 删除找搭子聊天群组成员
export function delFindPartnerChatGroupPerson(id) {
  return request({
    url: '/service/findPartnerChatGroupPerson/' + id,
    method: 'delete'
  })
}
