package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("FlightSearchParamSegmentDto")
public class FlightSearchParamSegmentDto {
    @ApiModelProperty("航班日期(yyyy-MM-dd)")
    private String departureTime;

    @ApiModelProperty("起飞城市或机场三字码")
    private String departureAirport;

    @ApiModelProperty("到达城市机场三字码")
    private String arrivalAirport;
}
