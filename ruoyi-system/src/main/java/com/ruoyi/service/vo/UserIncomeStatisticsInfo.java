package com.ruoyi.service.vo;

import lombok.Data;

@Data
public class UserIncomeStatisticsInfo {
    private Long sourceId;
    // 查看人数
    private Integer viewNumber;
    // 下单人数
    private Integer placeOrderNumber;
}
