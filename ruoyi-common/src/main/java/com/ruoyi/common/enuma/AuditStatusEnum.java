package com.ruoyi.common.enuma;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AuditStatusEnum implements BaseEnum {
    WAIT_AUDIT(0, "待审核"),
    AUDIT_PASS(1, "审核通过"),
    AUDIT_NO_PASS(2, "审核未通过")
    ;


    private Integer value;
    private String desc;
}
