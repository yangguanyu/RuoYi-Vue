package com.ruoyi.web.controller.service;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.service.dto.GetQrCodeDto;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.TfpQrCode;
import com.ruoyi.service.service.ITfpQrCodeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 二维码Controller
 * 
 * @author ruoyi
 * @date 2023-12-25
 */
@RestController
@RequestMapping("/service/qrCode")
@ApiOperation("二维码")
public class TfpQrCodeController extends BaseController
{
    @Autowired
    private ITfpQrCodeService tfpQrCodeService;

    /**
     * 查询二维码列表
     */
    @PreAuthorize("@ss.hasPermi('service:qrCode:list')")
    @GetMapping("/list")
    public TableDataInfo list(TfpQrCode tfpQrCode)
    {
        startPage();
        List<TfpQrCode> list = tfpQrCodeService.selectTfpQrCodeList(tfpQrCode);
        return getDataTable(list);
    }

    /**
     * 导出二维码列表
     */
    @PreAuthorize("@ss.hasPermi('service:qrCode:export')")
    @Log(title = "二维码", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TfpQrCode tfpQrCode)
    {
        List<TfpQrCode> list = tfpQrCodeService.selectTfpQrCodeList(tfpQrCode);
        ExcelUtil<TfpQrCode> util = new ExcelUtil<TfpQrCode>(TfpQrCode.class);
        util.exportExcel(response, list, "二维码数据");
    }

    /**
     * 获取二维码详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:qrCode:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tfpQrCodeService.selectTfpQrCodeById(id));
    }

    /**
     * 新增二维码
     */
    @PreAuthorize("@ss.hasPermi('service:qrCode:add')")
    @Log(title = "二维码", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TfpQrCode tfpQrCode)
    {
        return toAjax(tfpQrCodeService.insertTfpQrCode(tfpQrCode));
    }

    /**
     * 修改二维码
     */
    @PreAuthorize("@ss.hasPermi('service:qrCode:edit')")
    @Log(title = "二维码", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TfpQrCode tfpQrCode)
    {
        return toAjax(tfpQrCodeService.updateTfpQrCode(tfpQrCode));
    }

    /**
     * 删除二维码
     */
    @PreAuthorize("@ss.hasPermi('service:qrCode:remove')")
    @Log(title = "二维码", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tfpQrCodeService.deleteTfpQrCodeByIds(ids));
    }

    /**
     * 获取二维码
     * @param getQrCodeDto
     * @return
     */
    @ApiOperation("获取二维码")
    @PostMapping("/getQrCode")
    @ResponseBody
    public R<String> getQrCode(@RequestBody GetQrCodeDto getQrCodeDto){
        return R.ok(tfpQrCodeService.getQrCode(getQrCodeDto));
    }
}
