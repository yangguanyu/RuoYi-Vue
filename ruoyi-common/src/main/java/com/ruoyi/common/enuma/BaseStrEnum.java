package com.ruoyi.common.enuma;

import java.util.Objects;

/**
 * 整形枚举基类
 */
public interface BaseStrEnum {
    String getValue();

    String getDesc();

    /**
     * 根据code获取枚举值
     * @param enumClass
     * @param <E>
     * @return
     */
    static  <E extends Enum<?> & BaseStrEnum> E getEnumByCode(Class<E> enumClass, String value) {
        if (!Objects.isNull(value)) {
            E[] enumConstants = enumClass.getEnumConstants();
            for (E e : enumConstants) {
                if (e.getValue().equals(value)) {
                    return e;
                }
            }
        }
        return null;
    }

    /**
     * 根据code获取value
     * @param enumClass
     * @param <E>
     * @return
     */
    static  <E extends Enum<?> & BaseStrEnum> String getDescByCode(Class<E> enumClass, String value) {
        if (!Objects.isNull(value)) {
            E enumObj = BaseStrEnum.getEnumByCode(enumClass, value);
            if (enumObj == null) {
                return "";
            } else {
                return enumObj.getDesc();
            }
        }
        return "";
    }
}
