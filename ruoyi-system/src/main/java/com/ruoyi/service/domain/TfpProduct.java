package com.ruoyi.service.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;

/**
 * 商品主对象 tfp_product
 *
 * @author ruoyi
 * @date 2023-11-01
 */
public class TfpProduct extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 客服id */
    @Excel(name = "客服id")
    private Long customerServiceId;

    /** 商品归属 0-国内 1-海外 */
    @Excel(name = "商品归属 0-国内 1-海外")
    private Integer commoditiesType;

    /** 业务地区(1级) 能够选多个地区 下拉 */
    @Excel(name = "业务地区(1级) 能够选多个地区 下拉")
    private Long parentAreaId;

    /** 业务地区(1级) 能够选多个地区 下拉 */
    @Excel(name = "业务地区(1级) 能够选多个地区 下拉")
    private String parentAreaName;

    /** 业务地区(2级) 能够选多个地区 下拉 */
    @Excel(name = "业务地区(2级) 能够选多个地区 下拉")
    private Long areaId;

    /** 业务地区(2级) 能够选多个地区 下拉 */
    @Excel(name = "业务地区(2级) 能够选多个地区 下拉")
    private String areaName;

    /** 业务地区(3级) 能够选多个地区 下拉 */
    @Excel(name = "业务地区(3级) 能够选多个地区 下拉")
    private Long cityId;

    /** 业务地区(3级) 能够选多个地区 下拉 */
    @Excel(name = "业务地区(3级) 能够选多个地区 下拉")
    private String cityName;

    /** 服务类型id 散拼 单团 单项 下拉（怎么和咱们的全国散，一家一团对应） */
    @Excel(name = "服务类型id 1-全国散 2-一家一团 3-自营 4-全国散-小包团")
    private Long categoryId;

    /** 服务类型名称 散拼 单团 单项 下拉（怎么和咱们的全国散，一家一团对应） */
    @Excel(name = "服务类型名称 散拼 单团 单项 下拉", readConverterExp = "怎=么和咱们的全国散，一家一团对应")
    private String categoryName;

    /** 线路类型 出发地参团 目的地参团 字典code */
    @Excel(name = "线路类型 出发地参团 目的地参团 字典code")
    private String lineTypeCode;

    /** 线路类型 出发地参团 目的地参团 字典name */
    @Excel(name = "线路类型 出发地参团 目的地参团 字典name")
    private String lineTypeName;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String productName;

    /** 商品编号 */
    @Excel(name = "商品编号")
    private String productNo;

    /** 供应商名称 */
    @Excel(name = "供应商名称")
    private String supplierName;

    /** 供应商id */
    @Excel(name = "供应商id")
    private Long supplierId;

    /** 儿童标准 */
    @Excel(name = "儿童标准")
    private Integer age;

    /** 儿童标准-年龄-最小值 */
    @Excel(name = "儿童标准-年龄-最小值")
    private Integer childrenAgeLimitMin;

    /** 儿童标准-年龄-最大值 */
    @Excel(name = "儿童标准-年龄-最大值")
    private Integer childrenAgeLimitMax;

    /** 出发城市id */
    @Excel(name = "出发城市id")
    private Long goPlaceId;

    /** 出发城市名称 */
    @Excel(name = "出发城市名称")
    private String goPlaceName;

    /** 返回城市id */
    @Excel(name = "返回城市id")
    private Long backPlaceId;

    /** 返回城市名称 */
    @Excel(name = "返回城市名称")
    private String backPlaceName;

    /** 字典 去程交通 飞机 火车 轮船 汽车 高铁 其它 */
    @Excel(name = "字典 去程交通 飞机 火车 轮船 汽车 高铁 其它")
    private String goTrafficCode;

    /** 去程交通 飞机 火车 轮船 汽车 高铁 其它 */
    @Excel(name = "去程交通 飞机 火车 轮船 汽车 高铁 其它")
    private String goTrafficName;

    /** dict 回程交通 */
    @Excel(name = "dict 回程交通")
    private String backTrafficCode;

    /** 回程交通 */
    @Excel(name = "回程交通")
    private String backTrafficName;

    /** 行程天数 几天 */
    @Excel(name = "行程天数 几天")
    private Integer dayNum;

    /** 行程天数 几晚 */
    @Excel(name = "行程天数 几晚")
    private Integer nightNum;

    /** 是否包含保险 单选框 0-否 1-是 */
    @Excel(name = "是否包含保险 单选框 0-否 1-是")
    private Integer isInsurance;

    /** 保险公司 */
    @Excel(name = "保险公司")
    private String insuranceCompany;

    /** 险种code：人身意外险 旅游团队先 */
    @Excel(name = "险种code：人身意外险 旅游团队先")
    private String insuranceTypeCode;

    /** 险种名称：人身意外险 旅游团队先 */
    @Excel(name = "险种名称：人身意外险 旅游团队先")
    private String insuranceTypeName;

    /** 商品状态 */
    @Excel(name = "商品状态")
    private Integer itemStatus;

    /** 航空公司code 下拉 需要爬数据 */
    @Excel(name = "航空公司code 下拉 需要爬数据")
    private String airCompanyCode;

    /** 航空公司名称 下拉 需要爬数据 */
    @Excel(name = "航空公司名称 下拉 需要爬数据")
    private String airCompanyName;

    /** 是否有购物 0-否 1-是 */
    @Excel(name = "是否有购物 0-否 1-是")
    private Integer purchaseFlag;

    /** 是否自费项目 0-否 1-是 */
    @Excel(name = "是否自费项目 0-否 1-是")
    private Integer selfFundedProjectFlag;

    /** 提前截止天数 */
    @Excel(name = "提前截止天数")
    private Integer earlyDeadlineDay;

    /** 资源确认方式 1：二次确认  2：即使确认 */
    @Excel(name = "资源确认方式 1：二次确认  2：即使确认")
    private Integer confirmationType;

    //定时任务查询所需时间，目前是从两个月前开始到现在的商品走定时任务
    private Date taskDateTime;

    /** 分值，定时任务10分钟 */
    @Excel(name = "分值，定时任务10分钟")
    private Long score;

    /** 是否选择低价机票服务 0-否 1-是 */
    @Excel(name = "是否选择低价机票服务 0-否 1-是")
    private Integer lowPriceAirTicketServiceFlag;

    /** 机票价格是否已经查询完成 0-否 1-是 */
    @Excel(name = "机票价格是否已经查询完成 0-否 1-是")
    private Integer airTicketPriceQueryCompleteFlag;

    /** 提交后机票价格是否已经查询 0-否 1-是 */
    @Excel(name = "提交后机票价格是否已经查询 0-否 1-是")
    private Integer submitAirTicketPriceQueryFlag;

    /** 产品上下架状态字段 0-新增待上架 1-已上架 2-下架待审批 3-已下架 */
    @Excel(name = "产品上下架状态字段 0-新增待上架 1-已上架 2-下架待审批 3-已下架")
    private Integer salesStatus;

    /** 上架日期 */
    @Excel(name = "上架日期")
    private Date salesDate;

    /** 机票价格查询时间 */
    @Excel(name = "机票价格查询时间")
    private Date airTicketPriceQueryDate;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    @Excel(name = "创建者id")
    private Long createUserId;

    /** 更新者id */
    @Excel(name = "更新者id")
    private Long updateUserId;

    //是否按照分数排序
    private Boolean sortByScore;

    /** 商品描述 */
    @Excel(name = "商品描述")
    private String productDescribe;

    public Boolean getSortByScore() {
        return sortByScore;
    }

    public void setSortByScore(Boolean sortByScore) {
        this.sortByScore = sortByScore;
    }

    public Date getTaskDateTime() {
        return taskDateTime;
    }

    public void setTaskDateTime(Date taskDateTime) {
        this.taskDateTime = taskDateTime;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setCustomerServiceId(Long customerServiceId)
    {
        this.customerServiceId = customerServiceId;
    }

    public Long getCustomerServiceId()
    {
        return customerServiceId;
    }
    public void setCommoditiesType(Integer commoditiesType)
    {
        this.commoditiesType = commoditiesType;
    }

    public Integer getCommoditiesType()
    {
        return commoditiesType;
    }
    public void setParentAreaId(Long parentAreaId)
    {
        this.parentAreaId = parentAreaId;
    }

    public Long getParentAreaId()
    {
        return parentAreaId;
    }
    public void setParentAreaName(String parentAreaName)
    {
        this.parentAreaName = parentAreaName;
    }

    public String getParentAreaName()
    {
        return parentAreaName;
    }
    public void setAreaId(Long areaId)
    {
        this.areaId = areaId;
    }

    public Long getAreaId()
    {
        return areaId;
    }
    public void setAreaName(String areaName)
    {
        this.areaName = areaName;
    }

    public String getAreaName()
    {
        return areaName;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public void setCategoryId(Long categoryId)
    {
        this.categoryId = categoryId;
    }

    public Long getCategoryId()
    {
        return categoryId;
    }
    public void setCategoryName(String categoryName)
    {
        this.categoryName = categoryName;
    }

    public String getCategoryName()
    {
        return categoryName;
    }
    public void setLineTypeCode(String lineTypeCode)
    {
        this.lineTypeCode = lineTypeCode;
    }

    public String getLineTypeCode()
    {
        return lineTypeCode;
    }
    public void setLineTypeName(String lineTypeName)
    {
        this.lineTypeName = lineTypeName;
    }

    public String getLineTypeName()
    {
        return lineTypeName;
    }
    public void setProductName(String productName)
    {
        this.productName = productName;
    }

    public String getProductName()
    {
        return productName;
    }
    public void setProductNo(String productNo)
    {
        this.productNo = productNo;
    }

    public String getProductNo()
    {
        return productNo;
    }
    public void setSupplierName(String supplierName)
    {
        this.supplierName = supplierName;
    }

    public String getSupplierName()
    {
        return supplierName;
    }
    public void setSupplierId(Long supplierId)
    {
        this.supplierId = supplierId;
    }

    public Long getSupplierId()
    {
        return supplierId;
    }
    public void setAge(Integer age)
    {
        this.age = age;
    }

    public Integer getAge()
    {
        return age;
    }
    public void setGoPlaceId(Long goPlaceId)
    {
        this.goPlaceId = goPlaceId;
    }

    public Long getGoPlaceId()
    {
        return goPlaceId;
    }
    public void setGoPlaceName(String goPlaceName)
    {
        this.goPlaceName = goPlaceName;
    }

    public String getGoPlaceName()
    {
        return goPlaceName;
    }
    public void setBackPlaceId(Long backPlaceId)
    {
        this.backPlaceId = backPlaceId;
    }

    public Long getBackPlaceId()
    {
        return backPlaceId;
    }
    public void setBackPlaceName(String backPlaceName)
    {
        this.backPlaceName = backPlaceName;
    }

    public String getBackPlaceName()
    {
        return backPlaceName;
    }
    public void setGoTrafficCode(String goTrafficCode)
    {
        this.goTrafficCode = goTrafficCode;
    }

    public String getGoTrafficCode()
    {
        return goTrafficCode;
    }
    public void setGoTrafficName(String goTrafficName)
    {
        this.goTrafficName = goTrafficName;
    }

    public String getGoTrafficName()
    {
        return goTrafficName;
    }
    public void setBackTrafficCode(String backTrafficCode)
    {
        this.backTrafficCode = backTrafficCode;
    }

    public String getBackTrafficCode()
    {
        return backTrafficCode;
    }
    public void setBackTrafficName(String backTrafficName)
    {
        this.backTrafficName = backTrafficName;
    }

    public String getBackTrafficName()
    {
        return backTrafficName;
    }
    public void setDayNum(Integer dayNum)
    {
        this.dayNum = dayNum;
    }

    public Integer getDayNum()
    {
        return dayNum;
    }
    public void setNightNum(Integer nightNum)
    {
        this.nightNum = nightNum;
    }

    public Integer getNightNum()
    {
        return nightNum;
    }
    public void setIsInsurance(Integer isInsurance)
    {
        this.isInsurance = isInsurance;
    }

    public Integer getIsInsurance()
    {
        return isInsurance;
    }
    public void setInsuranceCompany(String insuranceCompany)
    {
        this.insuranceCompany = insuranceCompany;
    }

    public String getInsuranceCompany()
    {
        return insuranceCompany;
    }
    public void setInsuranceTypeCode(String insuranceTypeCode)
    {
        this.insuranceTypeCode = insuranceTypeCode;
    }

    public String getInsuranceTypeCode()
    {
        return insuranceTypeCode;
    }
    public void setInsuranceTypeName(String insuranceTypeName)
    {
        this.insuranceTypeName = insuranceTypeName;
    }

    public String getInsuranceTypeName()
    {
        return insuranceTypeName;
    }
    public void setItemStatus(Integer itemStatus)
    {
        this.itemStatus = itemStatus;
    }

    public Integer getItemStatus()
    {
        return itemStatus;
    }
    public void setAirCompanyCode(String airCompanyCode)
    {
        this.airCompanyCode = airCompanyCode;
    }

    public String getAirCompanyCode()
    {
        return airCompanyCode;
    }
    public void setAirCompanyName(String airCompanyName)
    {
        this.airCompanyName = airCompanyName;
    }

    public String getAirCompanyName()
    {
        return airCompanyName;
    }
    public void setEarlyDeadlineDay(Integer earlyDeadlineDay)
    {
        this.earlyDeadlineDay = earlyDeadlineDay;
    }

    public Integer getEarlyDeadlineDay()
    {
        return earlyDeadlineDay;
    }

    public void setConfirmationType(Integer confirmationType)
    {
        this.confirmationType = confirmationType;
    }

    public Integer getConfirmationType()
    {
        return confirmationType;
    }

    public void setScore(Long score)
    {
        this.score = score;
    }

    public Long getScore()
    {
        return score;
    }
    public void setSalesStatus(Integer salesStatus)
    {
        this.salesStatus = salesStatus;
    }

    public Integer getSalesStatus()
    {
        return salesStatus;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }
    public void setCreateUserId(Long createUserId)
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId()
    {
        return createUserId;
    }
    public void setUpdateUserId(Long updateUserId)
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId()
    {
        return updateUserId;
    }

    public Integer getPurchaseFlag() {
        return purchaseFlag;
    }

    public void setPurchaseFlag(Integer purchaseFlag) {
        this.purchaseFlag = purchaseFlag;
    }

    public Integer getSelfFundedProjectFlag() {
        return selfFundedProjectFlag;
    }

    public void setSelfFundedProjectFlag(Integer selfFundedProjectFlag) {
        this.selfFundedProjectFlag = selfFundedProjectFlag;
    }

    public Integer getChildrenAgeLimitMin() {
        return childrenAgeLimitMin;
    }

    public void setChildrenAgeLimitMin(Integer childrenAgeLimitMin) {
        this.childrenAgeLimitMin = childrenAgeLimitMin;
    }

    public Integer getChildrenAgeLimitMax() {
        return childrenAgeLimitMax;
    }

    public void setChildrenAgeLimitMax(Integer childrenAgeLimitMax) {
        this.childrenAgeLimitMax = childrenAgeLimitMax;
    }

    public String getProductDescribe() {
        return productDescribe;
    }

    public void setProductDescribe(String productDescribe) {
        this.productDescribe = productDescribe;
    }

    public Integer getLowPriceAirTicketServiceFlag() {
        return lowPriceAirTicketServiceFlag;
    }

    public void setLowPriceAirTicketServiceFlag(Integer lowPriceAirTicketServiceFlag) {
        this.lowPriceAirTicketServiceFlag = lowPriceAirTicketServiceFlag;
    }

    public Integer getAirTicketPriceQueryCompleteFlag() {
        return airTicketPriceQueryCompleteFlag;
    }

    public void setAirTicketPriceQueryCompleteFlag(Integer airTicketPriceQueryCompleteFlag) {
        this.airTicketPriceQueryCompleteFlag = airTicketPriceQueryCompleteFlag;
    }

    public Date getSalesDate() {
        return salesDate;
    }

    public void setSalesDate(Date salesDate) {
        this.salesDate = salesDate;
    }

    public Date getAirTicketPriceQueryDate() {
        return airTicketPriceQueryDate;
    }

    public void setAirTicketPriceQueryDate(Date airTicketPriceQueryDate) {
        this.airTicketPriceQueryDate = airTicketPriceQueryDate;
    }

    public Integer getSubmitAirTicketPriceQueryFlag() {
        return submitAirTicketPriceQueryFlag;
    }

    public void setSubmitAirTicketPriceQueryFlag(Integer submitAirTicketPriceQueryFlag) {
        this.submitAirTicketPriceQueryFlag = submitAirTicketPriceQueryFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("customerServiceId", getCustomerServiceId())
            .append("commoditiesType", getCommoditiesType())
            .append("parentAreaId", getParentAreaId())
            .append("parentAreaName", getParentAreaName())
            .append("areaId", getAreaId())
            .append("areaName", getAreaName())
            .append("cityId", getCityId())
            .append("cityName", getCityName())
            .append("categoryId", getCategoryId())
            .append("categoryName", getCategoryName())
            .append("lineTypeCode", getLineTypeCode())
            .append("lineTypeName", getLineTypeName())
            .append("productName", getProductName())
            .append("productNo", getProductNo())
            .append("supplierName", getSupplierName())
            .append("supplierId", getSupplierId())
            .append("age", getAge())
            .append("childrenAgeLimitMin", getChildrenAgeLimitMin())
            .append("childrenAgeLimitMax", getChildrenAgeLimitMax())
            .append("goPlaceId", getGoPlaceId())
            .append("goPlaceName", getGoPlaceName())
            .append("backPlaceId", getBackPlaceId())
            .append("backPlaceName", getBackPlaceName())
            .append("goTrafficCode", getGoTrafficCode())
            .append("goTrafficName", getGoTrafficName())
            .append("backTrafficCode", getBackTrafficCode())
            .append("backTrafficName", getBackTrafficName())
            .append("purchaseFlag", getPurchaseFlag())
            .append("selfFundedProjectFlag", getSelfFundedProjectFlag())
            .append("dayNum", getDayNum())
            .append("nightNum", getNightNum())
            .append("isInsurance", getIsInsurance())
            .append("insuranceCompany", getInsuranceCompany())
            .append("insuranceTypeCode", getInsuranceTypeCode())
            .append("insuranceTypeName", getInsuranceTypeName())
            .append("itemStatus", getItemStatus())
            .append("airCompanyCode", getAirCompanyCode())
            .append("airCompanyName", getAirCompanyName())
            .append("earlyDeadlineDay", getEarlyDeadlineDay())
            .append("confirmationType", getConfirmationType())
            .append("score", getScore())
            .append("lowPriceAirTicketServiceFlag", getLowPriceAirTicketServiceFlag())
            .append("airTicketPriceQueryCompleteFlag", getAirTicketPriceQueryCompleteFlag())
            .append("submitAirTicketPriceQueryFlag", getSubmitAirTicketPriceQueryFlag())
            .append("salesStatus", getSalesStatus())
            .append("salesDate", getSalesDate())
            .append("airTicketPriceQueryDate", getAirTicketPriceQueryDate())
            .append("productDescribe", getProductDescribe())
            .append("delFlag", getDelFlag())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
