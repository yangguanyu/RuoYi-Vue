package com.ruoyi.service.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@ApiModel("UserIncomeDetailListVo")
public class UserIncomeDetailListVo {
    @ApiModelProperty("结算佣金时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date settlementCommissionTime;
    @ApiModelProperty("佣金金额")
    private BigDecimal price;
}
