package com.ruoyi.web.controller.service;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.TfpProductDepartureDate;
import com.ruoyi.service.service.ITfpProductDepartureDateService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商品班期Controller
 * 
 * @author ruoyi
 * @date 2024-04-17
 */
@RestController
@RequestMapping("/service/productDepartureDate")
public class TfpProductDepartureDateController extends BaseController
{
    @Autowired
    private ITfpProductDepartureDateService tfpProductDepartureDateService;

    /**
     * 查询商品班期列表
     */
    @PreAuthorize("@ss.hasPermi('service:productDepartureDate:list')")
    @GetMapping("/list")
    public TableDataInfo list(TfpProductDepartureDate tfpProductDepartureDate)
    {
        startPage();
        List<TfpProductDepartureDate> list = tfpProductDepartureDateService.selectTfpProductDepartureDateList(tfpProductDepartureDate);
        return getDataTable(list);
    }

    /**
     * 导出商品班期列表
     */
    @PreAuthorize("@ss.hasPermi('service:productDepartureDate:export')")
    @Log(title = "商品班期", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TfpProductDepartureDate tfpProductDepartureDate)
    {
        List<TfpProductDepartureDate> list = tfpProductDepartureDateService.selectTfpProductDepartureDateList(tfpProductDepartureDate);
        ExcelUtil<TfpProductDepartureDate> util = new ExcelUtil<TfpProductDepartureDate>(TfpProductDepartureDate.class);
        util.exportExcel(response, list, "商品班期数据");
    }

    /**
     * 获取商品班期详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:productDepartureDate:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tfpProductDepartureDateService.selectTfpProductDepartureDateById(id));
    }

    /**
     * 新增商品班期
     */
    @PreAuthorize("@ss.hasPermi('service:productDepartureDate:add')")
    @Log(title = "商品班期", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TfpProductDepartureDate tfpProductDepartureDate)
    {
        return toAjax(tfpProductDepartureDateService.insertTfpProductDepartureDate(tfpProductDepartureDate));
    }

    /**
     * 修改商品班期
     */
    @PreAuthorize("@ss.hasPermi('service:productDepartureDate:edit')")
    @Log(title = "商品班期", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TfpProductDepartureDate tfpProductDepartureDate)
    {
        return toAjax(tfpProductDepartureDateService.updateTfpProductDepartureDate(tfpProductDepartureDate));
    }

    /**
     * 删除商品班期
     */
    @PreAuthorize("@ss.hasPermi('service:productDepartureDate:remove')")
    @Log(title = "商品班期", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tfpProductDepartureDateService.deleteTfpProductDepartureDateByIds(ids));
    }
}
