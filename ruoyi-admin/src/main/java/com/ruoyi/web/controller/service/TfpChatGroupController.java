package com.ruoyi.web.controller.service;

import java.util.*;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.exception.BusinessException;
import com.ruoyi.common.utils.DcListUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.service.domain.TfpChatGroupPerson;
import com.ruoyi.service.domain.TfpChatGroupPersonInfo;
import com.ruoyi.service.dto.MoreGroupListDto;
import com.ruoyi.service.dto.TfpChatGroupDto;
import com.ruoyi.common.enuma.BaseEnum;
import com.ruoyi.common.enuma.ChatUserTypeEnum;
import com.ruoyi.service.service.ITfpChatGroupPersonInfoService;
import com.ruoyi.service.service.ITfpChatGroupPersonService;
import com.ruoyi.service.vo.*;
import com.ruoyi.system.service.ISysUserService;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.compress.utils.Lists;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.TfpChatGroup;
import com.ruoyi.service.service.ITfpChatGroupService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 聊天群组Controller
 *
 * @author ruoyi
 * @date 2023-10-31
 */
@RestController
@RequestMapping("/service/chatGroup")
public class TfpChatGroupController extends BaseController
{
    @Autowired
    private ITfpChatGroupService tfpChatGroupService;

    @Autowired
    private ITfpChatGroupPersonService iTfpChatGroupPersonService;

    @Autowired
    private ISysUserService sysUserService;

    @Autowired
    private ITfpChatGroupPersonInfoService personInfoService;


    /**
     * 查询聊天群组列表
     */
    @PreAuthorize("@ss.hasPermi('service:chatGroup:list')")
    @GetMapping("/list")
    public TableDataInfo list(TfpChatGroupDto tfpChatGroupDto)
    {
        startPage();
        List<TfpChatGroupVo> list = tfpChatGroupService.selectTfpChatGroupVoList(tfpChatGroupDto);
        return getDataTable(list);
    }

    /**
     * 导出聊天群组列表
     */
    @PreAuthorize("@ss.hasPermi('service:chatGroup:export')")
    @Log(title = "聊天群组", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TfpChatGroup tfpChatGroup)
    {
        List<TfpChatGroup> list = tfpChatGroupService.selectTfpChatGroupList(tfpChatGroup);
        ExcelUtil<TfpChatGroup> util = new ExcelUtil<TfpChatGroup>(TfpChatGroup.class);
        util.exportExcel(response, list, "聊天群组数据");
    }

    /**
     * 获取聊天群组详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:chatGroup:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tfpChatGroupService.selectTfpChatGroupById(id));
    }

    @PreAuthorize("@ss.hasPermi('service:chatGroup:query')")
    @GetMapping(value = "detail/{id}")
    public R<ChatGroupDetailVO> detail(@PathVariable("id") Long id)
    {
        //返回信息
        ChatGroupDetailVO chatGroupDetailVO = new ChatGroupDetailVO();
        List<ChatGroupPersonVO> personVOList = Lists.newArrayList();
        //开始处理逻辑
        TfpChatGroup tfpChatGroup = tfpChatGroupService.selectTfpChatGroupById(id);
        if(Objects.nonNull(tfpChatGroup)){
            handlePersonInfo(chatGroupDetailVO,personVOList,tfpChatGroup);
            return R.ok(chatGroupDetailVO);
        }
        return R.fail();
    }


    @PreAuthorize("@ss.hasPermi('service:chatGroup:list')")
    @GetMapping("/chatlist")
    public TableDataInfo chatlist() {
        startPage();
        TfpChatGroupPersonInfo tfpChatGroupPersonInfo = new TfpChatGroupPersonInfo();
        List<TfpChatGroupPersonInfo> list = personInfoService.selectTfpChatGroupPersonInfoList(tfpChatGroupPersonInfo);
        return getDataTable(list);
    }
    /*
     * @Author yangguanyu
     * @Description :处理返回的人员信息
     * @Date 16:30 2023/11/22
     * @Param [com.ruoyi.service.vo.ChatGroupDetailVO, java.util.List<com.ruoyi.service.vo.ChatGroupPersonVO>, com.ruoyi.service.domain.TfpChatGroup]
     * @return void
     **/
    private void handlePersonInfo(ChatGroupDetailVO chatGroupDetailVO,List<ChatGroupPersonVO> personVOList,TfpChatGroup tfpChatGroup){
        //放入groupName
        chatGroupDetailVO.setGroupName(tfpChatGroup.getName());
        //获取人员逻辑
        TfpChatGroupPerson person = new TfpChatGroupPerson();
        person.setChatGroupId(tfpChatGroup.getId());
        List<TfpChatGroupPerson> tfpChatGroupPeople = iTfpChatGroupPersonService.selectTfpChatGroupPersonList(person);
        //处理人员
        if(CollectionUtils.isNotEmpty(tfpChatGroupPeople)){
            List<Long> userList = tfpChatGroupPeople.stream().map(TfpChatGroupPerson::getUserId).collect(Collectors.toList());
            List<SysUser> sysUsers = sysUserService.selectUserInfoByUserIdList(userList);
            Map<Long, String> userMap = new HashMap<>();
            if(CollectionUtils.isNotEmpty(sysUsers)){
                userMap = sysUsers.stream().collect(Collectors.toMap(SysUser::getUserId, SysUser::getUserName,(k1,k2)->k1));
            }
            for (TfpChatGroupPerson tfpChatGroupPerson : tfpChatGroupPeople) {
                Long chatGroupId = tfpChatGroupPerson.getChatGroupId();
                String userName = userMap.getOrDefault(tfpChatGroupPerson.getUserId(), "未获取");
                String userTypeName = BaseEnum.getDescByCode(ChatUserTypeEnum.class, tfpChatGroupPerson.getUserType());
                ChatGroupPersonVO chatGroupPersonVO =
                        new ChatGroupPersonVO(chatGroupId,userName,tfpChatGroupPerson.getUserType(),userTypeName);
                personVOList.add(chatGroupPersonVO);
            }
        }
        chatGroupDetailVO.setPersonVOList(personVOList);
    }


    /**
     * 新增聊天群组
     */
    @PreAuthorize("@ss.hasPermi('service:chatGroup:add')")
    @Log(title = "聊天群组", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TfpChatGroup tfpChatGroup)
    {
        return toAjax(tfpChatGroupService.insertTfpChatGroup(tfpChatGroup));
    }

    /**
     * 修改聊天群组
     */
    @PreAuthorize("@ss.hasPermi('service:chatGroup:edit')")
    @Log(title = "聊天群组", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TfpChatGroup tfpChatGroup)
    {
        return toAjax(tfpChatGroupService.updateTfpChatGroup(tfpChatGroup));
    }

    /**
     * 删除聊天群组
     */
    @PreAuthorize("@ss.hasPermi('service:chatGroup:remove')")
    @Log(title = "聊天群组", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tfpChatGroupService.deleteTfpChatGroupByIds(ids));
    }


    @ApiOperation("小程序-点击获取更多列表")
    @PostMapping("applet/moreList")
    @ResponseBody
    public R<MoreListVo> moreList(TfpChatGroupDto tfpChatGroupDto) {
        if(Objects.isNull(tfpChatGroupDto.getProductId())){
            throw new BusinessException("未获取到对应的旅游商品id");
        }
        return R.ok(tfpChatGroupService.moreList(tfpChatGroupDto));
    }

    @ApiOperation("小程序-获取该月的群组信息列表")
    @PostMapping("applet/moreGroupList")
    @ResponseBody
    public TableDataInfo<MoreListMonthGroupVo> moreGroupList(@RequestBody MoreGroupListDto moreGroupListDto)
    {
        return getDataTable(tfpChatGroupService.moreGroupList(moreGroupListDto));
    }

    @ApiOperation("小程序-我加入的群组集合")
    @PostMapping("/myJoinChatGroupList")
    @ResponseBody
    public TableDataInfo<JoinChatGroupVo> myJoinChatGroupList()
    {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = loginUser.getUserId();
        return getDataTable(tfpChatGroupService.visitorList(userId));
    }

    @ApiOperation("小程序-他人加入的群组集合")
    @PostMapping("/otherJoinChatGroupList")
    @ResponseBody
    public TableDataInfo<JoinChatGroupVo> otherJoinChatGroupList(@RequestParam Long userId)
    {
        return getDataTable(tfpChatGroupService.visitorList(userId));
    }


    @ApiOperation("小程序-获取指定日期下的群组")
    @GetMapping("applet/assignList")
    public R<List<TfpChatGroupVo>> assignList(TfpChatGroupDto tfpChatGroupDto) {
        if(Objects.isNull(tfpChatGroupDto.getProductId())){
            throw new BusinessException("未获取到对应的旅游商品id");
        }
        if(Objects.isNull(tfpChatGroupDto.getGoDate())){
            throw new BusinessException("未获取到对应的时间");
        }

        List<TfpChatGroupVo> tfpChatGroupVoList = tfpChatGroupService.selectGroupList(tfpChatGroupDto);
        if (DcListUtils.isNotEmpty(tfpChatGroupVoList)){
            tfpChatGroupVoList = tfpChatGroupVoList.stream().sorted(Comparator.comparing(TfpChatGroupVo::getCreateTime)).collect(Collectors.toList());
        }
        return R.ok(tfpChatGroupVoList);
    }
}
