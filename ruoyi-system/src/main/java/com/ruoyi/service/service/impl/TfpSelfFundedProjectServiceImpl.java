package com.ruoyi.service.service.impl;

import java.util.List;

import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.DcListUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.service.dto.TfpSelfFundedProjectDto;
import com.ruoyi.common.enuma.DeleteEnum;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpSelfFundedProjectMapper;
import com.ruoyi.service.domain.TfpSelfFundedProject;
import com.ruoyi.service.service.ITfpSelfFundedProjectService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 自费项目Service业务层处理
 *
 * @author ruoyi
 * @date 2023-10-31
 */
@Service
@Transactional
public class TfpSelfFundedProjectServiceImpl implements ITfpSelfFundedProjectService
{
    @Autowired
    private TfpSelfFundedProjectMapper tfpSelfFundedProjectMapper;

    /**
     * 查询自费项目
     *
     * @param id 自费项目主键
     * @return 自费项目
     */
    @Override
    public TfpSelfFundedProject selectTfpSelfFundedProjectById(Long id)
    {
        return tfpSelfFundedProjectMapper.selectTfpSelfFundedProjectById(id);
    }

    /**
     * 查询自费项目列表
     *
     * @param tfpSelfFundedProject 自费项目
     * @return 自费项目
     */
    @Override
    public List<TfpSelfFundedProject> selectTfpSelfFundedProjectList(TfpSelfFundedProject tfpSelfFundedProject)
    {
        return tfpSelfFundedProjectMapper.selectTfpSelfFundedProjectList(tfpSelfFundedProject);
    }

    /**
     * 新增自费项目
     *
     * @param tfpSelfFundedProject 自费项目
     * @return 结果
     */
    @Override
    public int insertTfpSelfFundedProject(TfpSelfFundedProject tfpSelfFundedProject)
    {
        tfpSelfFundedProject.setCreateTime(DateUtils.getNowDate());
        return tfpSelfFundedProjectMapper.insertTfpSelfFundedProject(tfpSelfFundedProject);
    }

    /**
     * 修改自费项目
     *
     * @param tfpSelfFundedProject 自费项目
     * @return 结果
     */
    @Override
    public int updateTfpSelfFundedProject(TfpSelfFundedProject tfpSelfFundedProject)
    {
        tfpSelfFundedProject.setUpdateTime(DateUtils.getNowDate());
        return tfpSelfFundedProjectMapper.updateTfpSelfFundedProject(tfpSelfFundedProject);
    }

    /**
     * 批量删除自费项目
     *
     * @param ids 需要删除的自费项目主键
     * @return 结果
     */
    @Override
    public int deleteTfpSelfFundedProjectByIds(Long[] ids)
    {
        return tfpSelfFundedProjectMapper.deleteTfpSelfFundedProjectByIds(ids);
    }

    /**
     * 删除自费项目信息
     *
     * @param id 自费项目主键
     * @return 结果
     */
    @Override
    public int deleteTfpSelfFundedProjectById(Long id)
    {
        return tfpSelfFundedProjectMapper.deleteTfpSelfFundedProjectById(id);
    }

    @Override
    public void deleteByProductId(Long productId) {
        tfpSelfFundedProjectMapper.deleteByProductId(productId);
    }

    @Override
    public void saveList(Long productId, List<TfpSelfFundedProjectDto> selfFundedProjectList) {
        if (DcListUtils.isNotEmpty(selfFundedProjectList)){
            LoginUser loginUser = SecurityUtils.getLoginUser();

            for (TfpSelfFundedProjectDto tfpSelfFundedProjectDto : selfFundedProjectList) {
                TfpSelfFundedProject tfpSelfFundedProject = new TfpSelfFundedProject();
                BeanUtils.copyProperties(tfpSelfFundedProjectDto, tfpSelfFundedProject);
                tfpSelfFundedProject.setId(null);
                tfpSelfFundedProject.setProductId(productId);
                tfpSelfFundedProject.setDelFlag(DeleteEnum.EXIST.getValue());
                tfpSelfFundedProject.setCreateUserId(loginUser.getUserId());
                tfpSelfFundedProject.setCreateBy(loginUser.getUsername());
                tfpSelfFundedProject.setCreateTime(DateUtils.getNowDate());
                tfpSelfFundedProjectMapper.insertTfpSelfFundedProject(tfpSelfFundedProject);
            }
        }
    }

    @Override
    public List<TfpSelfFundedProject> selectListByProductId(Long productId) {
        return tfpSelfFundedProjectMapper.selectListByProductId(productId);
    }

    @Override
    public void deleteTfpSelfFundedProjectByProductIds(List<Long> productIdList) {
        tfpSelfFundedProjectMapper.deleteTfpSelfFundedProjectByProductIds(productIdList);
    }
}
