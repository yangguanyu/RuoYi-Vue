package com.ruoyi.service.service;

import java.util.List;
import com.ruoyi.service.domain.TfpComate;
import com.ruoyi.service.dto.BatchAddComateDto;
import com.ruoyi.service.dto.TfpComateDto;
import com.ruoyi.service.vo.ComateVo;
import com.ruoyi.service.vo.TfpComateVo;
import com.ruoyi.service.vo.UserVo;

/**
 * 同行人-独立出行Service接口
 *
 * @author ruoyi
 * @date 2023-10-31
 */
public interface ITfpComateService
{
    /**
     * 查询同行人-独立出行
     *
     * @param id 同行人-独立出行主键
     * @return 同行人-独立出行
     */
    public TfpComate selectTfpComateById(Long id);

    /**
     * 查询同行人-独立出行列表
     *
     * @param tfpComate 同行人-独立出行
     * @return 同行人-独立出行集合
     */
    public List<TfpComate> selectTfpComateList(TfpComate tfpComate);

    List<TfpComateVo> selectTfpComateVoList(TfpComateDto tfpComateDto);

    /**
     * 新增同行人-独立出行
     *
     * @param tfpComate 同行人-独立出行
     * @return 结果
     */
    public int insertTfpComate(TfpComate tfpComate);

    /**
     * 修改同行人-独立出行
     *
     * @param tfpComate 同行人-独立出行
     * @return 结果
     */
    public int updateTfpComate(TfpComate tfpComate);

    /**
     * 批量删除同行人-独立出行
     *
     * @param ids 需要删除的同行人-独立出行主键集合
     * @return 结果
     */
    public int deleteTfpComateByIds(Long[] ids);

    /**
     * 删除同行人-独立出行信息
     *
     * @param id 同行人-独立出行主键
     * @return 结果
     */
    public int deleteTfpComateById(Long id);

    List<UserVo> mainUserSelect();

    /**
     * 添加同行人
     * @param batchAddComateDtoList
     * @return
     */
    int batchAddComate(List<BatchAddComateDto> batchAddComateDtoList);

    List<TfpComate> selectTfpComateByIdList(List<Long> idList);

    int deleteComate(Long mateId);

    List<ComateVo> getMyComateList();
}
