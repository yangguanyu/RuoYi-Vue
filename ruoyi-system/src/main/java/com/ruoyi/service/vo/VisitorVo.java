package com.ruoyi.service.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "VisitorVo", description = "访客vo")
public class VisitorVo {
    @ApiModelProperty("访客用户id")
    private Long userId;
    @ApiModelProperty("访客用户昵称")
    private String nickName;
    @ApiModelProperty("访客用户头像")
    private String userHeadImage;
    @ApiModelProperty("访问时间")
    private String visitDate;
}
