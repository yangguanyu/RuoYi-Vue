package com.ruoyi.service.service;

import java.util.List;
import com.ruoyi.service.domain.TfpTravelRule;
import com.ruoyi.service.dto.ExportDto;
import com.ruoyi.service.dto.ListTravelRuleDto;
import com.ruoyi.service.dto.TfpTravelRuleDto;
import com.ruoyi.service.excel.ListTravelRuleExcel;
import com.ruoyi.service.vo.DetailTravelRuleVo;
import com.ruoyi.service.vo.EffectiveTravelRuleVo;
import com.ruoyi.service.vo.ListTravelRuleVo;

/**
 * 旅行规则Service接口
 * 
 * @author ruoyi
 * @date 2024-05-22
 */
public interface ITfpTravelRuleService 
{
    /**
     * 查询旅行规则
     * 
     * @param id 旅行规则主键
     * @return 旅行规则
     */
    public TfpTravelRule selectTfpTravelRuleById(Long id);

    /**
     * 查询旅行规则列表
     * 
     * @param tfpTravelRule 旅行规则
     * @return 旅行规则集合
     */
    public List<TfpTravelRule> selectTfpTravelRuleList(TfpTravelRule tfpTravelRule);

    /**
     * 新增旅行规则
     * 
     * @param tfpTravelRule 旅行规则
     * @return 结果
     */
    public int insertTfpTravelRule(TfpTravelRule tfpTravelRule);

    /**
     * 修改旅行规则
     * 
     * @param tfpTravelRule 旅行规则
     * @return 结果
     */
    public int updateTfpTravelRule(TfpTravelRule tfpTravelRule);

    /**
     * 批量删除旅行规则
     * 
     * @param ids 需要删除的旅行规则主键集合
     * @return 结果
     */
    public int deleteTfpTravelRuleByIds(Long[] ids);

    /**
     * 删除旅行规则信息
     * 
     * @param id 旅行规则主键
     * @return 结果
     */
    public int deleteTfpTravelRuleById(Long id);

    List<ListTravelRuleVo> listTravelRule(ListTravelRuleDto listTravelRuleDto);

    int saveTravelRule(TfpTravelRuleDto tfpTravelRuleDto);

    DetailTravelRuleVo detailTravelRule(Long id);

    List<EffectiveTravelRuleVo> getEffectiveTravelRuleList(Integer type);

    List<ListTravelRuleExcel> travelRuleListExport(ExportDto exportDto);
}
