package com.ruoyi.service.domain.flight.res.prepaycheck;

import lombok.Data;

@Data
public class PrePayCheckResp {
    //-------------------------------------未支付，返回的信息-------------------------------------------

    // 成人价格
    private Double adultTotalPay;

    // 成人手续费
    private Double adultPayCharge;

    // 儿童价格
    private Double chdTotalPay;

    // 儿童手续费
    private Double chdPayCharge;

    // 婴儿价格
    private Double infTotalPay;

    // 婴儿手续费
    private Double infPayCharge;

    //--------------------------------------已支付返回的信息-----------------------------------------------

    // 订单结算价，如果这个没返回，代表没支付
    private Double totalPay;

    // 票面总价
    private Double totalPrice;

    // 订单支付手续费
    private Double payCharge;

    // 燃油总计
    private Double totalFuelTax;

    // 机建总计
    private Double totalAirportTax;

    // 支付交易号
    private String paytradeNo;

    // 付款账号
    private String payerAccount;

    // 支付完成时间
    private String payTime;
}
