package com.ruoyi.web.controller.service;

import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.service.service.IPayService;
import com.wechat.pay.java.core.RSAAutoCertificateConfig;
import com.wechat.pay.java.core.exception.ValidationException;
import com.wechat.pay.java.core.notification.NotificationConfig;
import com.wechat.pay.java.core.notification.NotificationParser;
import com.wechat.pay.java.core.notification.RequestParam;
import com.wechat.pay.java.service.partnerpayments.jsapi.model.Transaction;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@Slf4j
@RestController
@RequestMapping("/service/payNotify")
@ApiOperation("支付回调")
public class PayNotifyController {
    private static final Logger logger = LoggerFactory.getLogger(PayNotifyController.class);

    @Autowired
    private IPayService payService;

    /** 商户号 */
    @Value("${merchant.id}")
    public String merchantId;
    /** 商户API私钥路径 */
    @Value("${private.key.path}")
    public String privateKeyPath;
    /** 商户证书序列号 */
    @Value("${merchant.serial.number}")
    public String merchantSerialNumber;
    /** 商户APIV3密钥 */
    @Value("${api.v3.key}")
    public String apiV3Key;

    @PostMapping("/payNotify")
    public void payNotify(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String body = this.getRequestBody(request);

        String wechatSignature = request.getHeader("Wechatpay-Signature");
        String requestwechatPaySerial = request.getHeader("Wechatpay-Serial");
        String wechatpayNonce = request.getHeader("Wechatpay-Nonce");
        String wechatTimestamp = request.getHeader("Wechatpay-Timestamp");
//        String signatureType = request.getHeader("Wechatpay-Signature-Type");

        // 构造 RequestParam
        RequestParam requestParam = new RequestParam.Builder()
                .serialNumber(requestwechatPaySerial)
                .nonce(wechatpayNonce)
                .signature(wechatSignature)
                .timestamp(wechatTimestamp)
                .body(body)
                .build();

        // 如果已经初始化了 RSAAutoCertificateConfig，可直接使用
        // 没有的话，则构造一个
        NotificationConfig config = new RSAAutoCertificateConfig.Builder()
                .merchantId(merchantId)
                .privateKeyFromPath(privateKeyPath)
                .merchantSerialNumber(merchantSerialNumber)
                .apiV3Key(apiV3Key)
                .build();

        // 初始化 NotificationParser
        NotificationParser parser = new NotificationParser(config);

        try {
            // 以支付通知回调为例，验签、解密并转换成 Transaction
            Transaction transaction = parser.parse(requestParam, Transaction.class);
            Transaction.TradeStateEnum state = transaction.getTradeState();
            if (!StringUtils.equals("SUCCESS", state.toString())) {
                log.error("微信回调失败,PayNotifyController.payNotify.transaction：{}",transaction.toString());
                //通知微信回调失败
                response.getWriter().write("<xml><return_code><![CDATA[FAIL]]></return_code></xml>");
            }

            payService.payNotify(transaction);
        } catch (ValidationException e) {
            // 签名验证失败，返回 401 UNAUTHORIZED 状态码
            logger.error("sign verification failed", e);
            response.getWriter().write("<xml><return_code><![CDATA[FAIL]]></return_code></xml>");
        }

        // 处理成功，返回 200 OK 状态码
        //通知微信回调成功
        response.getWriter().write("<xml><return_code><![CDATA[SUCCESS]]></return_code></xml>");
    }

    /**
     * 读取请求数据流
     * @param request
     * @return
     */
    private String getRequestBody(HttpServletRequest request) {
        StringBuffer sb = new StringBuffer();
        try (ServletInputStream inputStream = request.getInputStream();
             BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        ) {
            String line;

            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            log.error("读取数据流异常:{}", e);
        }
        return sb.toString();
    }
}
