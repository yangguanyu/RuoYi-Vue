package com.ruoyi.service.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@ApiModel(value = "DateCalendarVo", description = "日历上的价格")
public class DateCalendarVo {
    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("每月的第几天")
    private  Integer dayOfMonth;

    @ApiModelProperty("每星期的第几天")
    private  Integer dayOfWeek;

    @ApiModelProperty("几月几日，只有在外边的时候才显示这个")
    private  String day;

    @ApiModelProperty("日期")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date date;

    @ApiModelProperty("这个日期是否有价格")
    private Boolean hasPrice = Boolean.FALSE;

    @ApiModelProperty("价格")
    private BigDecimal price = BigDecimal.ZERO;

    @ApiModelProperty("总价")
    private BigDecimal finalPrice;


    @ApiModelProperty("是否可以出行 0-否 1-是")
    private Integer travelFlag;

    @ApiModelProperty("是否有机票价格 0-否 1-是")
    private Integer airTicketPriceFlag;
}
