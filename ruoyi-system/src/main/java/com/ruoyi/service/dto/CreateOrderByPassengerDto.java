package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel("CreateOrderByPassengerDto")
public class CreateOrderByPassengerDto {
    @ApiModelProperty("商家订单号")
    private String outOrderNo;
    @ApiModelProperty("通过查询返回的产品ID，例如：1-19291223")
    private String policyId;
    @ApiModelProperty("航程类型 OW单程，RT往返，MS多程")
    private String routeType;
    @ApiModelProperty("航程航线数据")
    private List<CreateOrderByPassengerSegmentDto> segments;
    @ApiModelProperty("乘客类型：1.成人、2.儿童、3.成人+儿童")
    private Integer passengerType;
    @ApiModelProperty("乘客信息")
    private List<CreateOrderByPassengerPassengerDto> passengers;
    @ApiModelProperty("价格信息：传入时平台验价后生成订单，不传入价格有平台规则生成订单")
    private List<CreateOrderByPassengerPriceDataDto> priceDatas;
    @ApiModelProperty("代理人电话")
    private String ctct;
    @ApiModelProperty("代理人姓名")
    private String ctctName;
    @ApiModelProperty("通知url （订单出票后往该地址回调通知）")
    private String notifiedUrl;
    @ApiModelProperty("传入关系证明附件地址，注：未传入订单拒单率提高")
    private String infProve;
    @ApiModelProperty("订单创建人登录账号，可填写51book营业员账号，不填默认主账号")
    private String createdBy;
    @ApiModelProperty("用于用户防止自身客户恶意占座，用户可以传入Boolean为false系统会支付后占座")
    private Boolean isBooking;
}
