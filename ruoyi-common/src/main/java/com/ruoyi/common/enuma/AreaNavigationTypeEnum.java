package com.ruoyi.common.enuma;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AreaNavigationTypeEnum implements BaseEnum {

    NAVIGATION(0, "导航"),
    AREA(1, "区域")
    ;

    private Integer value;
    private String desc;
}
