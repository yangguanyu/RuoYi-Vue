package com.ruoyi.service.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.SysUserIconMapper;
import com.ruoyi.service.domain.SysUserIcon;
import com.ruoyi.service.service.ISysUserIconService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 用户图片Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-11-01
 */
@Service
@Transactional
public class SysUserIconServiceImpl implements ISysUserIconService 
{
    @Autowired
    private SysUserIconMapper sysUserIconMapper;

    /**
     * 查询用户图片
     * 
     * @param id 用户图片主键
     * @return 用户图片
     */
    @Override
    public SysUserIcon selectSysUserIconById(Long id)
    {
        return sysUserIconMapper.selectSysUserIconById(id);
    }

    /**
     * 查询用户图片列表
     * 
     * @param sysUserIcon 用户图片
     * @return 用户图片
     */
    @Override
    public List<SysUserIcon> selectSysUserIconList(SysUserIcon sysUserIcon)
    {
        return sysUserIconMapper.selectSysUserIconList(sysUserIcon);
    }

    /**
     * 新增用户图片
     * 
     * @param sysUserIcon 用户图片
     * @return 结果
     */
    @Override
    public int insertSysUserIcon(SysUserIcon sysUserIcon)
    {
        sysUserIcon.setCreateTime(DateUtils.getNowDate());
        return sysUserIconMapper.insertSysUserIcon(sysUserIcon);
    }

    /**
     * 修改用户图片
     * 
     * @param sysUserIcon 用户图片
     * @return 结果
     */
    @Override
    public int updateSysUserIcon(SysUserIcon sysUserIcon)
    {
        sysUserIcon.setUpdateTime(DateUtils.getNowDate());
        return sysUserIconMapper.updateSysUserIcon(sysUserIcon);
    }

    /**
     * 批量删除用户图片
     * 
     * @param ids 需要删除的用户图片主键
     * @return 结果
     */
    @Override
    public int deleteSysUserIconByIds(Long[] ids)
    {
        return sysUserIconMapper.deleteSysUserIconByIds(ids);
    }

    /**
     * 删除用户图片信息
     * 
     * @param id 用户图片主键
     * @return 结果
     */
    @Override
    public int deleteSysUserIconById(Long id)
    {
        return sysUserIconMapper.deleteSysUserIconById(id);
    }
}
