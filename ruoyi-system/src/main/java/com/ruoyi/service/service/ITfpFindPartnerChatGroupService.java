package com.ruoyi.service.service;

import java.util.List;

import com.ruoyi.service.domain.TfpFindPartner;
import com.ruoyi.service.domain.TfpFindPartnerChatGroup;
import com.ruoyi.service.dto.TfpFindPartnerChatGroupDto;
import com.ruoyi.service.vo.FindPartnerDetailVo;
import com.ruoyi.service.vo.TfpFindPartnerChatGroupVo;

/**
 * 找搭子聊天群组Service接口
 *
 * @author ruoyi
 * @date 2023-10-31
 */
public interface ITfpFindPartnerChatGroupService
{
    /**
     * 查询找搭子聊天群组
     *
     * @param id 找搭子聊天群组主键
     * @return 找搭子聊天群组
     */
    public TfpFindPartnerChatGroup selectTfpFindPartnerChatGroupById(Long id);


    List<TfpFindPartnerChatGroupVo> selectTfpFindPartnerChatGroupVoList(TfpFindPartnerChatGroupDto tfpFindPartnerChatGroupDto);
    /**
     * 查询找搭子聊天群组列表
     *
     * @param tfpFindPartnerChatGroup 找搭子聊天群组
     * @return 找搭子聊天群组集合
     */
    public List<TfpFindPartnerChatGroup> selectTfpFindPartnerChatGroupList(TfpFindPartnerChatGroup tfpFindPartnerChatGroup);

    /**
     * 新增找搭子聊天群组
     *
     * @param tfpFindPartnerChatGroup 找搭子聊天群组
     * @return 结果
     */
    public int insertTfpFindPartnerChatGroup(TfpFindPartnerChatGroup tfpFindPartnerChatGroup);

    /**
     * 修改找搭子聊天群组
     *
     * @param tfpFindPartnerChatGroup 找搭子聊天群组
     * @return 结果
     */
    public int updateTfpFindPartnerChatGroup(TfpFindPartnerChatGroup tfpFindPartnerChatGroup);

    /**
     * 批量删除找搭子聊天群组
     *
     * @param ids 需要删除的找搭子聊天群组主键集合
     * @return 结果
     */
    public int deleteTfpFindPartnerChatGroupByIds(Long[] ids);

    /**
     * 删除找搭子聊天群组信息
     *
     * @param id 找搭子聊天群组主键
     * @return 结果
     */
    public int deleteTfpFindPartnerChatGroupById(Long id);

    List<TfpFindPartnerChatGroup> selectTfpFindPartnerChatGroupListByIdList(List<Long> idList);

    TfpFindPartnerChatGroup selectByFindPartnerId(Long findPartnerId);

    FindPartnerDetailVo detail(Long id);

    List<TfpFindPartnerChatGroup> selectTfpFindPartnerChatGroupListByFindPartnerIdList(List<Long> findPartnerIdList);

    void saveFindPartnerChatGroup(TfpFindPartner tfpFindPartner);

    void saveFindPartnerChatGroupTimeTask(TfpFindPartner tfpFindPartner);
}
