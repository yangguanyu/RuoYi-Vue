package com.ruoyi.system.service.impl;

import java.util.*;
import java.util.stream.Collectors;

import com.ruoyi.common.area.domain.entity.SysArea;
import com.ruoyi.common.area.domain.entity.SysAreaHot;
import com.ruoyi.common.area.domain.model.AreaDTO;
import com.ruoyi.common.area.domain.model.AreaTreeDTO;
import com.ruoyi.common.area.domain.model.DepartureAreaDTO;
import com.ruoyi.common.area.domain.model.DepartureAreaSonDTO;
import com.ruoyi.common.utils.DcListUtils;
import com.ruoyi.system.service.ISysAreaHotService;
import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysAreaMapper;
import com.ruoyi.system.service.ISysAreaService;
import com.ruoyi.common.core.text.Convert;

/**
 * 地区Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-10-15
 */
@Service
public class SysAreaServiceImpl implements ISysAreaService
{
    @Autowired
    private SysAreaMapper sysAreaMapper;

    @Autowired
    private ISysAreaHotService sysAreaHotService;

    /**
     * 查询地区
     * 
     * @param id 地区主键
     * @return 地区
     */
    @Override
    public SysArea selectSysAreaById(Long id)
    {
        return sysAreaMapper.selectSysAreaById(id);
    }

    /**
     * 查询地区列表
     * 
     * @param sysArea 地区
     * @return 地区
     */
    @Override
    public List<SysArea> selectSysAreaList(SysArea sysArea)
    {
        return sysAreaMapper.selectSysAreaList(sysArea);
    }

    /**
     * 新增地区
     * 
     * @param sysArea 地区
     * @return 结果
     */
    @Override
    public int insertSysArea(SysArea sysArea)
    {
        return sysAreaMapper.insertSysArea(sysArea);
    }

    /**
     * 修改地区
     * 
     * @param sysArea 地区
     * @return 结果
     */
    @Override
    public int updateSysArea(SysArea sysArea)
    {
        return sysAreaMapper.updateSysArea(sysArea);
    }

    /**
     * 批量删除地区
     * 
     * @param ids 需要删除的地区主键
     * @return 结果
     */
    @Override
    public int deleteSysAreaByIds(String ids)
    {
        return sysAreaMapper.deleteSysAreaByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除地区信息
     * 
     * @param id 地区主键
     * @return 结果
     */
    @Override
    public int deleteSysAreaById(Long id)
    {
        return sysAreaMapper.deleteSysAreaById(id);
    }

    /**
     * 获取出发地
     * @return
     */
    @Override
    public DepartureAreaDTO getDepartureArea() {
        DepartureAreaDTO departureAreaDTO = new DepartureAreaDTO();

        // 国内区域字母列表处理
        List<DepartureAreaSonDTO> chinaAreaList = this.handleChinaAreaList(0);
        departureAreaDTO.setChinaAreaList(chinaAreaList);

        // 海外区域字母列表处理
        List<DepartureAreaSonDTO> otherAreaOrChinaGATList = this.handleChinaAreaList(1);
        departureAreaDTO.setOtherAreaOrChinaGATList(otherAreaOrChinaGATList);

        return departureAreaDTO;
    }

    /**
     * 获取业务区域
     * @return
     */
    @Override
    public List<AreaTreeDTO> getServiceAreaList() {
        List<AreaTreeDTO> areaTreeDTOList = new ArrayList<>();

        // type 0-省上一级 1 省级 countryType 0-中国
        List<SysArea> areaDTOList = sysAreaMapper.selectListByTypeAndCountryType(0, "0");
        if (areaDTOList != null && areaDTOList.size() > 0){
            List<SysArea> provinceList = sysAreaMapper.selectListByTypeAndCountryType(1, "0");
            Map<Long, List<SysArea>> provinceMap = new HashMap<>();
            List<Long> provinceIdList = new ArrayList<>();
            if (provinceList != null && provinceList.size() > 0){
                provinceMap = provinceList.stream().collect(Collectors.groupingBy(SysArea::getParentId));
                provinceIdList = provinceList.stream().map(SysArea::getId).collect(Collectors.toList());
            }

            Map<Long, List<SysArea>> cityMap = new HashMap<>();
            List<SysArea> cityList = sysAreaMapper.selectSysAreaListByParentIdList(provinceIdList);
            if (DcListUtils.isNotEmpty(cityList)){
                cityMap = cityList.stream().collect(Collectors.groupingBy(SysArea::getParentId));
            }

            for (SysArea area : areaDTOList) {
                AreaTreeDTO areaTreeDTO = this.copyAreaToAreaTreeDTO(area);
                List<SysArea> provinceAreaList = provinceMap.get(area.getId());

                List<AreaTreeDTO> provinceChildrenList = this.batchCopyAreaToAreaTreeDTO(provinceAreaList);
                if (DcListUtils.isNotEmpty(provinceChildrenList)){
                    for (AreaTreeDTO provinceChildren : provinceChildrenList) {
                        List<SysArea> cityAreaList = cityMap.get(provinceChildren.getId());
                        this.handleSpecialArea(cityAreaList);
                        provinceChildren.setChildren(this.batchCopyAreaToAreaTreeDTO(cityAreaList));
                    }
                }

                areaTreeDTO.setChildren(provinceChildrenList);
                areaTreeDTOList.add(areaTreeDTO);
            }
        }

        return areaTreeDTOList;
    }

    /**
     * 获取省市区域
     * @return
     */
    @Override
    public List<AreaTreeDTO> getAreaList(boolean containAreaFlag) {
        List<AreaTreeDTO> provinceUrbanTreeDTOList = new ArrayList<>();

        List<SysArea> provinceList = sysAreaMapper.selectListByTypeAndCountryType(1, "0");
        if (provinceList != null && provinceList.size() > 0){
            List<Long> provinceIdList = provinceList.stream().map(SysArea::getId).collect(Collectors.toList());

            Map<Long, List<SysArea>> urbanMap = new HashMap<>();
            Map<Long, List<SysArea>> areaMap = new HashMap<>();
            List<SysArea> urbanList = sysAreaMapper.selectSysAreaListByParentIdList(provinceIdList);
            if (urbanList != null && urbanList.size() > 0){
                urbanMap = urbanList.stream().collect(Collectors.groupingBy(SysArea::getParentId));

                if (containAreaFlag){
                    List<Long> urbanIdList = urbanList.stream().map(SysArea::getId).collect(Collectors.toList());
                    List<SysArea> areaList = sysAreaMapper.selectSysAreaListByParentIdList(urbanIdList);
                    if (areaList != null && areaList.size() > 0){
                        areaMap = areaList.stream().collect(Collectors.groupingBy(SysArea::getParentId));
                    }
                }
            }

            for (SysArea province : provinceList) {
                AreaTreeDTO provinceTreeDTO = this.copyAreaToAreaTreeDTO(province);
                List<SysArea> sonAreaList = urbanMap.get(province.getId());
                List<AreaTreeDTO> urbanTreeDTOList = this.batchCopyAreaToAreaTreeDTO(sonAreaList);
                if (containAreaFlag && urbanTreeDTOList != null && urbanTreeDTOList.size() > 0){
                    for (AreaTreeDTO areaTreeDTO : urbanTreeDTOList) {
                        areaTreeDTO.setChildren(this.batchCopyAreaToAreaTreeDTO(areaMap.get(areaTreeDTO.getId())));
                    }
                }

                provinceTreeDTO.setChildren(urbanTreeDTOList);
                provinceUrbanTreeDTOList.add(provinceTreeDTO);
            }
        }

        return provinceUrbanTreeDTOList;
    }

    private void handleSpecialArea(List<SysArea> areaList) {
        if (DcListUtils.isNotEmpty(areaList)){
            Map<String, AreaTreeDTO> replaceMap = new HashMap<>();
            replaceMap.put("大理白族自治州", new AreaTreeDTO(7303L, "大理市"));
            replaceMap.put("迪庆藏族自治州", new AreaTreeDTO(7319L, "香格里拉市"));
            replaceMap.put("喀什地区", new AreaTreeDTO(7706L, "喀什市"));
            replaceMap.put("三沙市", new AreaTreeDTO(129487L, "西沙群岛"));

            for (SysArea area : areaList) {
                AreaTreeDTO areaTreeDTO = replaceMap.get(area.getName());
                if (Objects.nonNull(areaTreeDTO)){
                    area.setId(areaTreeDTO.getId());
                    area.setName(areaTreeDTO.getLabel());
                }
            }
        }
    }

    @Override
    public List<String> searchDepartureAreaNameByKey(String key) {
        return sysAreaMapper.searchDepartureAreaNameByKey(key);
    }

    /**
     * copy区域到AreaTreeDTO
     * @param area
     * @return
     */
    private AreaTreeDTO copyAreaToAreaTreeDTO(SysArea area){
        return new AreaTreeDTO(area.getId(), area.getName());
    }

    /**
     * copy区域到AreaTreeDTO
     * @param areaList
     * @return
     */
    private List<AreaTreeDTO> batchCopyAreaToAreaTreeDTO(List<SysArea> areaList){
        List<AreaTreeDTO> areaTreeDTOList = new ArrayList<>();
        if (areaList != null && areaList.size() > 0){
            for (SysArea area : areaList) {
                areaTreeDTOList.add(this.copyAreaToAreaTreeDTO(area));
            }
        }
        return areaTreeDTOList;
    }

    /**
     * 国内区域字母列表处理
     * @return
     */
    private List<DepartureAreaSonDTO> handleChinaAreaList(Integer countryType){
        List<DepartureAreaSonDTO> departureAreaSonDTOList = new ArrayList<>();

        // 国内区域处理
        SysArea queryArea = new SysArea();
        queryArea.setCountryType(countryType);
        queryArea.setType(1);
        // 省
        List<SysArea> provinceAreaList = sysAreaMapper.selectSysAreaList(queryArea);
        Map<Long, SysArea> provinceAreaMap = provinceAreaList.stream().collect(Collectors.toMap(SysArea::getId, SysArea -> SysArea));
        List<Long> provinceAreaIdList = provinceAreaList.stream().map(SysArea::getId).collect(Collectors.toList());

        Map<String, List<AreaDTO>> map = new HashMap<>();

        // 为了前端展示特殊处理
        if (countryType == 0){
            // 7759 台湾  7785 香港  7806 澳门
            provinceAreaIdList.remove(7759L);
            provinceAreaIdList.remove(7785L);
            provinceAreaIdList.remove(7806L);
        }

        //市
        List<SysArea> cityAreaList = sysAreaMapper.selectSysAreaListByParentIdList(provinceAreaIdList);
        for (SysArea sysArea : cityAreaList) {
            Long parentId = sysArea.getParentId();
            String parentName = provinceAreaMap.get(parentId).getName();

            String showName = sysArea.getName() + "(" + parentName + ")";
            this.handleAreaMap(map, sysArea, showName);
        }

        // 海外需要处理特殊区域
        if (countryType == 1){
            queryArea = new SysArea();
            queryArea.setCountryType(1);
            queryArea.setParentId(4341L);
            queryArea.setType(99);
            List<SysArea> areaList = sysAreaMapper.selectSysAreaList(queryArea);

            // 中国港澳台因为前端展示区域，特殊处理
            areaList.add(sysAreaMapper.selectSysAreaById(7759L));
            areaList.add(sysAreaMapper.selectSysAreaById(7785L));
            areaList.add(sysAreaMapper.selectSysAreaById(7806L));

            for (SysArea sysArea : areaList) {
                String showName = sysArea.getName();
                this.handleAreaMap(map, sysArea, showName);
            }
        }

        Set<Map.Entry<String, List<AreaDTO>>> entries = map.entrySet();
        for (Map.Entry<String, List<AreaDTO>> entry : entries) {
            List<AreaDTO> list = entry.getValue();
            String key = entry.getKey();
            List<AreaDTO> sortList = list.stream().sorted(Comparator.comparing(AreaDTO::getSort)).collect(Collectors.toList());

            DepartureAreaSonDTO departureAreaSonDTO = new DepartureAreaSonDTO();
            departureAreaSonDTO.setKey(key);
            departureAreaSonDTO.setTitle(key.toUpperCase());
            departureAreaSonDTO.setAreaDTOList(sortList);

            departureAreaSonDTOList.add(departureAreaSonDTO);
        }

        // 热门区域
        List<AreaDTO> hotList = new ArrayList<>();

        List<SysAreaHot> sysAreaHotList = sysAreaHotService.getAreaHotListByCountryType(countryType);
        if (sysAreaHotList != null && sysAreaHotList.size() > 0){
            for (SysAreaHot sysAreaHot : sysAreaHotList) {
                String parentAreaName = sysAreaHot.getParentAreaName();

                String showName;
                if (parentAreaName == null || parentAreaName.trim().equals(Strings.EMPTY)){
                    showName = sysAreaHot.getAreaName();
                } else {
                    showName = sysAreaHot.getAreaName() + "(" +parentAreaName + ")";
                }

                AreaDTO areaDTO = new AreaDTO();
                areaDTO.setId(sysAreaHot.getAreaId());
                areaDTO.setName(sysAreaHot.getAreaName());
                areaDTO.setShowName(showName);
                areaDTO.setPinyin(sysAreaHot.getPinyin());
                areaDTO.setSort(sysAreaHot.getSort());

                hotList.add(areaDTO);
            }

            hotList = hotList.stream().sorted(Comparator.comparing(AreaDTO::getSort)).collect(Collectors.toList());
        }

        DepartureAreaSonDTO departureAreaSonDTO = new DepartureAreaSonDTO();
        departureAreaSonDTO.setKey("hot");
        departureAreaSonDTO.setTitle("hot");
        departureAreaSonDTO.setAreaDTOList(hotList);

        departureAreaSonDTOList.add(0, departureAreaSonDTO);
        return departureAreaSonDTOList;
    }


    /**
     * 处理区域map
     * @param map
     * @param sysArea
     */
    private void handleAreaMap(Map<String, List<AreaDTO>> map, SysArea sysArea, String showName){
        AreaDTO areaDTO = new AreaDTO();
        areaDTO.setId(sysArea.getId());
        areaDTO.setName(sysArea.getName());
        areaDTO.setShowName(showName);
        areaDTO.setPinyin(sysArea.getPinyin());
        areaDTO.setSort(sysArea.getSort());

        String key = sysArea.getPinyin().toLowerCase().substring(0, 1);
        List<AreaDTO> areaDTOList = map.get(key);
        if (areaDTOList == null){
            areaDTOList = new ArrayList<>();
        }

        areaDTOList.add(areaDTO);
        map.put(key, areaDTOList);
    }

    /**
     * 处理汉字拼音
     * @param name
     * @return
     */
    private String handleHanziPinyin(String name){
        char[] chars = name.toCharArray();
        StringBuffer sb = new StringBuffer();
        Character resultChar = null;
        for(char c:chars){
            if (String.valueOf(c).equals("－") || String.valueOf(c).equals("·")){
                continue;
            }

            if (c > 128){
                resultChar = this.getFirstLetter(c);
            } else {
                resultChar = c;
            }
            sb.append(resultChar);
        }

        return sb.toString();
    }

    private char getFirstLetter(char singleHanzi){
        HanyuPinyinOutputFormat outputFormat = new HanyuPinyinOutputFormat();
        outputFormat.setCaseType(HanyuPinyinCaseType.UPPERCASE);
        String[] stirngs = new String[0];

        try {
            stirngs = PinyinHelper.toHanyuPinyinStringArray(singleHanzi, outputFormat);
        } catch (BadHanyuPinyinOutputFormatCombination badHanyuPinyinOutputFormatCombination) {
            badHanyuPinyinOutputFormatCombination.printStackTrace();
        }

        return stirngs[0].charAt(0);
    }
}
