import request from '@/utils/request'

// 查询用户银行卡列表
export function listUserBankCard(query) {
  return request({
    url: '/service/userBankCard/list',
    method: 'get',
    params: query
  })
}

// 查询用户银行卡详细
export function getUserBankCard(id) {
  return request({
    url: '/service/userBankCard/' + id,
    method: 'get'
  })
}

// 新增用户银行卡
export function addUserBankCard(data) {
  return request({
    url: '/service/userBankCard',
    method: 'post',
    data: data
  })
}

// 修改用户银行卡
export function updateUserBankCard(data) {
  return request({
    url: '/service/userBankCard',
    method: 'put',
    data: data
  })
}

// 删除用户银行卡
export function delUserBankCard(id) {
  return request({
    url: '/service/userBankCard/' + id,
    method: 'delete'
  })
}
