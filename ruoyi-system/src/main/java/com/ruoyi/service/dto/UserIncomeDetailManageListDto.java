package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "UserIncomeDetailManageListDto")
public class UserIncomeDetailManageListDto {
    @ApiModelProperty("请求结算状态 0-未结算 1-已结算")
    private Integer settlementReqStatus;
    @ApiModelProperty("结算单编号")
    private String settlementNumber;
    @ApiModelProperty("产品类型")
    private Long categoryId;
    @ApiModelProperty("用户id")
    private Long userId;
    @ApiModelProperty("产品编号")
    private String productNo;
    @ApiModelProperty("是否已过售后期 0-否 1-是")
    private Integer afterSalesTimeFlag;
}
