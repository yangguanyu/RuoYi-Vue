package com.ruoyi.service.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.enuma.OrderStatusEnum;
import com.ruoyi.common.enuma.YesOrNoFlagEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@ApiModel(value = "OrderDetailInfoVo")
public class OrderDetailInfoVo {
    @ApiModelProperty("订单id")
    private Long id;
    @ApiModelProperty("订单编号")
    private String orderNo;
    @ApiModelProperty("订单状态 0-待支付 1-已完成 2-待确认(已支付) 3-待出行 4-申请退款-审核中 5-申请退款-退款成功 6-过期 7-取消")
    private Integer status;
    @ApiModelProperty("支付金额")
    private BigDecimal finalAmount;
    @ApiModelProperty("支付时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date payTime;

    @ApiModelProperty("商品id")
    private Long productId;
    @ApiModelProperty("商品名称")
    private String productName;
    @ApiModelProperty("商品编号")
    private String productNo;
    @ApiModelProperty("出发时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date goDate;
    @ApiModelProperty("出行方式 0-组队出行 1-独立出行")
    private Integer travelType;

    @ApiModelProperty("供应商名称")
    private String supplierName;
    @ApiModelProperty("产品类型id")
    private Long categoryId;
    @ApiModelProperty("产品类型名称")
    private String categoryName;

    @ApiModelProperty("出行人信息")
    private List<OrderListComateInfoVo> comateInfoList;

    @ApiModelProperty("操作人")
    private String operationUserName;

    @ApiModelProperty("支付状态 0-未支付 1-已支付")
    private Integer payStatus;
    @ApiModelProperty("退款类型 0-退款待审核 1-整单退 2-部分退 3-否")
    private Integer refundType;

    @ApiModelProperty("申请退款时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date applyDate;
    @ApiModelProperty("退款原因")
    private String reason;
    @ApiModelProperty("申请退款说明")
    private String illustrate;

    public Integer getPayStatus(){
        List<Integer> payStatusYesOrderStatusList = new ArrayList<>();
        payStatusYesOrderStatusList.add(OrderStatusEnum.COMPLETE.getValue());
        payStatusYesOrderStatusList.add(OrderStatusEnum.WAIT_CONFIRMED.getValue());
        payStatusYesOrderStatusList.add(OrderStatusEnum.WAIT_TRAVEL.getValue());
        payStatusYesOrderStatusList.add(OrderStatusEnum.APPLY_REFUND_AUDITING.getValue());
        payStatusYesOrderStatusList.add(OrderStatusEnum.APPLY_REFUND_SUCCESS.getValue());

        if (payStatusYesOrderStatusList.contains(this.status)){
            return YesOrNoFlagEnum.YES.getValue();
        } else {
            return YesOrNoFlagEnum.NO.getValue();
        }
    }
}
