package com.ruoyi.common.enuma.flight;

import com.ruoyi.common.enuma.BaseStrEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum OrderQueryExceptionCode implements BaseStrEnum {
    ORDER_NUMBER_AND_EXTERNAL_ORDER_NUMBER_REQUIRED("010402", "订单号及外部订单号不能同时为空"),
    NOT_OUR_COMPANY_ORDER("010403", "不是本公司订单，无法查看详情"),
    DUPLICATE_ORDER_NUMBER_AND_EXTERNAL_ORDER_NUMBER("010406", "订单号及外部订单号不能同时为空");

    private final String value;
    private final String desc;

}
