package com.ruoyi.service.dto;

import lombok.Data;

import java.util.List;

@Data
public class ListProductSelectDto {
    private String backPlaceName;
    private String cityName;
    private List<Long> idList;
}
