package com.ruoyi.web.controller.service;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.service.vo.MyProductSnapshotListVo;
import com.ruoyi.service.vo.ProductSnapshotDetailVo;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.TfpProductSnapshot;
import com.ruoyi.service.service.ITfpProductSnapshotService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商品快照Controller
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@RestController
@RequestMapping("/service/productSnapshot")
public class TfpProductSnapshotController extends BaseController
{
    @Autowired
    private ITfpProductSnapshotService tfpProductSnapshotService;

    /**
     * 查询商品快照列表
     */
    @PreAuthorize("@ss.hasPermi('service:productSnapshot:list')")
    @GetMapping("/list")
    public TableDataInfo list(TfpProductSnapshot tfpProductSnapshot)
    {
        startPage();
        List<TfpProductSnapshot> list = tfpProductSnapshotService.selectTfpProductSnapshotList(tfpProductSnapshot);
        return getDataTable(list);
    }

    /**
     * 导出商品快照列表
     */
    @PreAuthorize("@ss.hasPermi('service:productSnapshot:export')")
    @Log(title = "商品快照", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TfpProductSnapshot tfpProductSnapshot)
    {
        List<TfpProductSnapshot> list = tfpProductSnapshotService.selectTfpProductSnapshotList(tfpProductSnapshot);
        ExcelUtil<TfpProductSnapshot> util = new ExcelUtil<TfpProductSnapshot>(TfpProductSnapshot.class);
        util.exportExcel(response, list, "商品快照数据");
    }

    /**
     * 获取商品快照详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:productSnapshot:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tfpProductSnapshotService.selectTfpProductSnapshotById(id));
    }

    /**
     * 新增商品快照
     */
    @PreAuthorize("@ss.hasPermi('service:productSnapshot:add')")
    @Log(title = "商品快照", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TfpProductSnapshot tfpProductSnapshot)
    {
        return toAjax(tfpProductSnapshotService.insertTfpProductSnapshot(tfpProductSnapshot));
    }

    /**
     * 修改商品快照
     */
    @PreAuthorize("@ss.hasPermi('service:productSnapshot:edit')")
    @Log(title = "商品快照", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TfpProductSnapshot tfpProductSnapshot)
    {
        return toAjax(tfpProductSnapshotService.updateTfpProductSnapshot(tfpProductSnapshot));
    }

    /**
     * 删除商品快照
     */
    @PreAuthorize("@ss.hasPermi('service:productSnapshot:remove')")
    @Log(title = "商品快照", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tfpProductSnapshotService.deleteTfpProductSnapshotByIds(ids));
    }

    @ApiOperation("小程序-我的商品快照列表")
    @PostMapping("/myProductSnapshotList")
    @ResponseBody
    public TableDataInfo<MyProductSnapshotListVo> myProductSnapshotList(){
        return getDataTable(tfpProductSnapshotService.myProductSnapshotList());
    }

    @ApiOperation("小程序-商品快照详情")
    @PostMapping("/productSnapshotDetail")
    @ResponseBody
    public R<ProductSnapshotDetailVo> productSnapshotDetail(@RequestParam Long id){
        return R.ok(tfpProductSnapshotService.productSnapshotDetail(id));
    }
}
