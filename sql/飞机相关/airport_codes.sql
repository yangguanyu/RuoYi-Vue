/*
 Navicat Premium Data Transfer

 Source Server         : 101.42.111.25
 Source Server Type    : MySQL
 Source Server Version : 50743
 Source Host           : 101.42.111.25:3306
 Source Schema         : qinglv

 Target Server Type    : MySQL
 Target Server Version : 50743
 File Encoding         : 65001

 Date: 18/03/2024 20:52:11
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for airport_codes
-- ----------------------------
DROP TABLE IF EXISTS `airport_codes`;
CREATE TABLE `airport_codes`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL comment '城市名称',
  `code` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL comment '城市编码',
  `airport_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL comment '机场名称',
  `airport_code` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL comment '机场编码',
  `province` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL comment '省',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 178 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of airport_codes
-- ----------------------------
INSERT INTO `airport_codes` VALUES (1, '北京', 'BJS');
INSERT INTO `airport_codes` VALUES (2, '安康', 'AKA');
INSERT INTO `airport_codes` VALUES (3, '阿克苏', 'AKU');
INSERT INTO `airport_codes` VALUES (4, '安庆', 'AQG');
INSERT INTO `airport_codes` VALUES (5, '阿勒泰', 'AAT');
INSERT INTO `airport_codes` VALUES (6, '安顺', 'AVA');
INSERT INTO `airport_codes` VALUES (7, '毕节', 'BFJ');
INSERT INTO `airport_codes` VALUES (8, '巴彦淖尔', 'RLK');
INSERT INTO `airport_codes` VALUES (9, '巴中', 'BZX');
INSERT INTO `airport_codes` VALUES (10, '包头', 'BAV');
INSERT INTO `airport_codes` VALUES (11, '白山', 'NBS');
INSERT INTO `airport_codes` VALUES (12, '北海', 'BHY');
INSERT INTO `airport_codes` VALUES (13, '白城', 'DBC');
INSERT INTO `airport_codes` VALUES (14, '池州', 'JUH');
INSERT INTO `airport_codes` VALUES (15, '常德', 'CGD');
INSERT INTO `airport_codes` VALUES (16, '成都', 'CTU');
INSERT INTO `airport_codes` VALUES (17, '长春', 'CGQ');
INSERT INTO `airport_codes` VALUES (18, '重庆', 'CKG');
INSERT INTO `airport_codes` VALUES (19, '郴州', 'HCZ');
INSERT INTO `airport_codes` VALUES (20, '昌都', 'BPX');
INSERT INTO `airport_codes` VALUES (21, '长沙', 'CSX');
INSERT INTO `airport_codes` VALUES (22, '常州', 'CZX');
INSERT INTO `airport_codes` VALUES (23, '长治', 'CIH');
INSERT INTO `airport_codes` VALUES (24, '达州', 'DZH');
INSERT INTO `airport_codes` VALUES (25, '东营', 'DOY');
INSERT INTO `airport_codes` VALUES (26, '大理', 'DLU');
INSERT INTO `airport_codes` VALUES (27, '大庆', 'DQA');
INSERT INTO `airport_codes` VALUES (28, '丹东', 'DDG');
INSERT INTO `airport_codes` VALUES (29, '迪庆', 'DIG');
INSERT INTO `airport_codes` VALUES (30, '大连', 'DLC');
INSERT INTO `airport_codes` VALUES (31, '敦煌', 'DNH');
INSERT INTO `airport_codes` VALUES (32, '鄂州', 'EHU');
INSERT INTO `airport_codes` VALUES (33, '二连浩特', 'ERL');
INSERT INTO `airport_codes` VALUES (34, '鄂尔多斯', 'DSN');
INSERT INTO `airport_codes` VALUES (35, '恩施', 'ENH');
INSERT INTO `airport_codes` VALUES (36, '抚远', 'FYJ');
INSERT INTO `airport_codes` VALUES (37, '福州', 'FOC');
INSERT INTO `airport_codes` VALUES (38, '阜阳', 'FUG');
INSERT INTO `airport_codes` VALUES (39, '佛山', 'FUO');
INSERT INTO `airport_codes` VALUES (40, '广州', 'CAN');
INSERT INTO `airport_codes` VALUES (41, '赣州', 'KOW');
INSERT INTO `airport_codes` VALUES (42, '固原', 'GYU');
INSERT INTO `airport_codes` VALUES (43, '贵阳', 'KWE');
INSERT INTO `airport_codes` VALUES (44, '桂林', 'KWL');
INSERT INTO `airport_codes` VALUES (45, '广元', 'GYS');
INSERT INTO `airport_codes` VALUES (46, '黑河', 'HEK');
INSERT INTO `airport_codes` VALUES (47, '合肥', 'HFE');
INSERT INTO `airport_codes` VALUES (48, '惠州', 'HUZ');
INSERT INTO `airport_codes` VALUES (49, '哈密', 'HMI');
INSERT INTO `airport_codes` VALUES (50, '汉中', 'HZG');
INSERT INTO `airport_codes` VALUES (51, '呼和浩特', 'HET');
INSERT INTO `airport_codes` VALUES (52, '和田', 'HTN');
INSERT INTO `airport_codes` VALUES (53, '海口', 'HAK');
INSERT INTO `airport_codes` VALUES (54, '杭州', 'HGH');
INSERT INTO `airport_codes` VALUES (55, '怀化', 'HJJ');
INSERT INTO `airport_codes` VALUES (56, '海拉尔', 'HLD');
INSERT INTO `airport_codes` VALUES (57, '衡阳', 'HNY');
INSERT INTO `airport_codes` VALUES (58, '哈尔滨', 'HRB');
INSERT INTO `airport_codes` VALUES (59, '黄山', 'TXN');
INSERT INTO `airport_codes` VALUES (60, '佳木斯', 'JMU');
INSERT INTO `airport_codes` VALUES (61, '嘉峪关', 'JGN');
INSERT INTO `airport_codes` VALUES (62, '建三江', 'JSJ');
INSERT INTO `airport_codes` VALUES (63, '荆州', 'SHS');
INSERT INTO `airport_codes` VALUES (64, '井冈山', 'JGS');
INSERT INTO `airport_codes` VALUES (65, '九江', 'JIU');
INSERT INTO `airport_codes` VALUES (66, '金昌', 'JIC');
INSERT INTO `airport_codes` VALUES (67, '鸡西', 'JXA');
INSERT INTO `airport_codes` VALUES (68, '揭阳', 'SWA');
INSERT INTO `airport_codes` VALUES (69, '加格达奇', 'JGD');
INSERT INTO `airport_codes` VALUES (70, '景德镇', 'JDZ');
INSERT INTO `airport_codes` VALUES (71, '济宁', 'JNG');
INSERT INTO `airport_codes` VALUES (72, '喀什', 'KHG');
INSERT INTO `airport_codes` VALUES (73, '昆明', 'KMG');
INSERT INTO `airport_codes` VALUES (74, '库尔勒', 'KRL');
INSERT INTO `airport_codes` VALUES (75, '克拉玛依', 'KRY');
INSERT INTO `airport_codes` VALUES (76, '洛阳', 'LYA');
INSERT INTO `airport_codes` VALUES (77, '拉萨', 'LXA');
INSERT INTO `airport_codes` VALUES (78, '阆中', 'LZG');
INSERT INTO `airport_codes` VALUES (79, '丽江', 'LJG');
INSERT INTO `airport_codes` VALUES (80, '泸州', 'LZO');
INSERT INTO `airport_codes` VALUES (81, '林芝', 'LZY');
INSERT INTO `airport_codes` VALUES (82, '柳州', 'LZH');
INSERT INTO `airport_codes` VALUES (83, '陇南', 'LNL');
INSERT INTO `airport_codes` VALUES (84, '龙岩', 'LCX');
INSERT INTO `airport_codes` VALUES (85, '连云港', 'LYG');
INSERT INTO `airport_codes` VALUES (86, '临汾', 'LFQ');
INSERT INTO `airport_codes` VALUES (87, '吕梁', 'LLV');
INSERT INTO `airport_codes` VALUES (88, '兰州', 'LHW');
INSERT INTO `airport_codes` VALUES (89, '临沧', 'LNJ');
INSERT INTO `airport_codes` VALUES (90, '绵阳', 'MIG');
INSERT INTO `airport_codes` VALUES (91, '茅台', 'WMT');
INSERT INTO `airport_codes` VALUES (92, '牡丹江', 'MDG');
INSERT INTO `airport_codes` VALUES (93, '满洲里', 'NZH');
INSERT INTO `airport_codes` VALUES (94, '芒市', 'LUM');
INSERT INTO `airport_codes` VALUES (95, '漠河', 'OHE');
INSERT INTO `airport_codes` VALUES (96, '南宁', 'NNG');
INSERT INTO `airport_codes` VALUES (97, '南充', 'NAO');
INSERT INTO `airport_codes` VALUES (98, '南通', 'NTG');
INSERT INTO `airport_codes` VALUES (99, '宁波', 'NGB');
INSERT INTO `airport_codes` VALUES (100, '南昌', 'KHN');
INSERT INTO `airport_codes` VALUES (101, '南京', 'NKG');
INSERT INTO `airport_codes` VALUES (102, '衢州', 'JUZ');
INSERT INTO `airport_codes` VALUES (103, '琼海', 'BAR');
INSERT INTO `airport_codes` VALUES (104, '庆阳', 'IQN');
INSERT INTO `airport_codes` VALUES (105, '青岛', 'TAO');
INSERT INTO `airport_codes` VALUES (106, '齐齐哈尔', 'NDG');
INSERT INTO `airport_codes` VALUES (107, '泉州', 'JJN');
INSERT INTO `airport_codes` VALUES (108, '黔江', 'JIQ');
INSERT INTO `airport_codes` VALUES (109, '日照', 'RIZ');
INSERT INTO `airport_codes` VALUES (110, '上饶', 'SQD');
INSERT INTO `airport_codes` VALUES (111, '上海', 'SHA');
INSERT INTO `airport_codes` VALUES (112, '十堰', 'WDS');
INSERT INTO `airport_codes` VALUES (113, '沈阳', 'SHE');
INSERT INTO `airport_codes` VALUES (114, '石河子', 'SHF');
INSERT INTO `airport_codes` VALUES (115, '三亚', 'SYX');
INSERT INTO `airport_codes` VALUES (116, '松原', 'YSQ');
INSERT INTO `airport_codes` VALUES (117, '韶关', 'HSC');
INSERT INTO `airport_codes` VALUES (118, '三明', 'SQJ');
INSERT INTO `airport_codes` VALUES (119, '深圳', 'SZX');
INSERT INTO `airport_codes` VALUES (120, '邵阳', 'WGN');
INSERT INTO `airport_codes` VALUES (121, '通辽', 'TGO');
INSERT INTO `airport_codes` VALUES (122, '铜仁', 'TEN');
INSERT INTO `airport_codes` VALUES (123, '台州', 'HYN');
INSERT INTO `airport_codes` VALUES (124, '腾冲', 'TCZ');
INSERT INTO `airport_codes` VALUES (125, '图木舒克', 'TWC');
INSERT INTO `airport_codes` VALUES (126, '通化', 'TNH');
INSERT INTO `airport_codes` VALUES (127, '太原', 'TYN');
INSERT INTO `airport_codes` VALUES (128, '乌鲁木齐', 'URC');
INSERT INTO `airport_codes` VALUES (129, '武夷山', 'WUS');
INSERT INTO `airport_codes` VALUES (130, '无锡', 'WUX');
INSERT INTO `airport_codes` VALUES (131, '武汉', 'WUH');
INSERT INTO `airport_codes` VALUES (132, '文山', 'WNH');
INSERT INTO `airport_codes` VALUES (133, '乌海', 'WUA');
INSERT INTO `airport_codes` VALUES (134, '乌兰浩特', 'HLH');
INSERT INTO `airport_codes` VALUES (135, '五大连池', 'DTU');
INSERT INTO `airport_codes` VALUES (136, '温州', 'WNZ');
INSERT INTO `airport_codes` VALUES (137, '威海', 'WEH');
INSERT INTO `airport_codes` VALUES (138, '梧州', 'WUZ');
INSERT INTO `airport_codes` VALUES (139, '万州', 'WXN');
INSERT INTO `airport_codes` VALUES (140, '锡林浩特', 'XIL');
INSERT INTO `airport_codes` VALUES (141, '襄阳', 'XFN');
INSERT INTO `airport_codes` VALUES (142, '西昌', 'XIC');
INSERT INTO `airport_codes` VALUES (143, '西安', 'SIA');
INSERT INTO `airport_codes` VALUES (144, '西双版纳', 'JHG');
INSERT INTO `airport_codes` VALUES (145, '厦门', 'XMN');
INSERT INTO `airport_codes` VALUES (146, '湘西', 'DXJ');
INSERT INTO `airport_codes` VALUES (147, '西宁', 'XNN');
INSERT INTO `airport_codes` VALUES (148, '兴义', 'ACX');
INSERT INTO `airport_codes` VALUES (149, '伊春', 'LDS');
INSERT INTO `airport_codes` VALUES (150, '盐城', 'YNZ');
INSERT INTO `airport_codes` VALUES (151, '银川', 'INC');
INSERT INTO `airport_codes` VALUES (152, '宜宾', 'YBP');
INSERT INTO `airport_codes` VALUES (153, '岳阳', 'YYA');
INSERT INTO `airport_codes` VALUES (154, '永州', 'LLF');
INSERT INTO `airport_codes` VALUES (155, '义乌', 'YIW');
INSERT INTO `airport_codes` VALUES (156, '运城', 'YCU');
INSERT INTO `airport_codes` VALUES (157, '宜春', 'YIC');
INSERT INTO `airport_codes` VALUES (158, '玉林', 'YLX');
INSERT INTO `airport_codes` VALUES (159, '玉树', 'YUS');
INSERT INTO `airport_codes` VALUES (160, '延吉', 'YNJ');
INSERT INTO `airport_codes` VALUES (161, '扬州', 'YTY');
INSERT INTO `airport_codes` VALUES (162, '延安', 'ENY');
INSERT INTO `airport_codes` VALUES (163, '宜昌', 'YIH');
INSERT INTO `airport_codes` VALUES (164, '榆林', 'UYN');
INSERT INTO `airport_codes` VALUES (165, '烟台', 'YNT');
INSERT INTO `airport_codes` VALUES (166, '伊宁', 'YIN');
INSERT INTO `airport_codes` VALUES (167, '遵义', 'ZYI');
INSERT INTO `airport_codes` VALUES (168, '中卫', 'ZHY');
INSERT INTO `airport_codes` VALUES (169, '昭通', 'ZAT');
INSERT INTO `airport_codes` VALUES (170, '湛江', 'ZHA');
INSERT INTO `airport_codes` VALUES (171, '珠海', 'ZUH');
INSERT INTO `airport_codes` VALUES (172, '张掖', 'YZY');
INSERT INTO `airport_codes` VALUES (173, '舟山', 'HSN');
INSERT INTO `airport_codes` VALUES (174, '郑州', 'CGO');
INSERT INTO `airport_codes` VALUES (175, '扎兰屯', 'NZL');
INSERT INTO `airport_codes` VALUES (176, '张家界', 'DYG');
INSERT INTO `airport_codes` VALUES (177, '阿尔山', 'YIE');

SET FOREIGN_KEY_CHECKS = 1;
