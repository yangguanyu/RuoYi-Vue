import request from '@/utils/request'

// 查询商品快照列表
export function listProductSnapshot(query) {
  return request({
    url: '/service/productSnapshot/list',
    method: 'get',
    params: query
  })
}

// 查询商品快照详细
export function getProductSnapshot(id) {
  return request({
    url: '/service/productSnapshot/' + id,
    method: 'get'
  })
}

// 新增商品快照
export function addProductSnapshot(data) {
  return request({
    url: '/service/productSnapshot',
    method: 'post',
    data: data
  })
}

// 修改商品快照
export function updateProductSnapshot(data) {
  return request({
    url: '/service/productSnapshot',
    method: 'put',
    data: data
  })
}

// 删除商品快照
export function delProductSnapshot(id) {
  return request({
    url: '/service/productSnapshot/' + id,
    method: 'delete'
  })
}
