package com.ruoyi.service.service;

import java.util.Date;
import java.util.List;
import com.ruoyi.service.domain.TfpChatGroup;
import com.ruoyi.service.domain.TfpOrder;
import com.ruoyi.service.dto.ChatGroupStatisticsDto;
import com.ruoyi.service.dto.MoreGroupListDto;
import com.ruoyi.service.dto.TfpChatGroupDto;
import com.ruoyi.service.vo.*;

/**
 * 聊天群组Service接口
 *
 * @author ruoyi
 * @date 2023-10-31
 */
public interface ITfpChatGroupService
{
    /**
     * 查询聊天群组
     *
     * @param id 聊天群组主键
     * @return 聊天群组
     */
    public TfpChatGroup selectTfpChatGroupById(Long id);

    /**
     * 查询聊天群组列表
     *
     * @param tfpChatGroup 聊天群组
     * @return 聊天群组集合
     */
    public List<TfpChatGroup> selectTfpChatGroupList(TfpChatGroup tfpChatGroup);

    List<TfpChatGroupVo> selectTfpChatGroupVoList(TfpChatGroupDto tfpChatGroupDto);

    /*/**
     * @Author yangguanyu
     * @Description :获取群组及对应人数
     * @Date 22:17 2023/12/28
     * @Param [com.ruoyi.service.dto.TfpChatGroupDto]
     * @return java.util.List<com.ruoyi.service.vo.TfpChatGroupVo>
     **/
    List<TfpChatGroupVo> selectGroupList(TfpChatGroupDto tfpChatGroupDto);
    /**
     * 新增聊天群组
     *
     * @param tfpChatGroup 聊天群组
     * @return 结果
     */
    public int insertTfpChatGroup(TfpChatGroup tfpChatGroup);

    /**
     * 修改聊天群组
     *
     * @param tfpChatGroup 聊天群组
     * @return 结果
     */
    public int updateTfpChatGroup(TfpChatGroup tfpChatGroup);

    /**
     * 批量删除聊天群组
     *
     * @param ids 需要删除的聊天群组主键集合
     * @return 结果
     */
    public int deleteTfpChatGroupByIds(Long[] ids);

    /**
     * 删除聊天群组信息
     *
     * @param id 聊天群组主键
     * @return 结果
     */
    public int deleteTfpChatGroupById(Long id);

    List<TfpChatGroup> selectTfpChatGroupListByIdList(List<Long> idList);

    /*/**
     * @Author yangguanyu
     * @Description : 用于商品的排序统计
     * @Date 21:38 2023/12/25
     * @Param [java.util.List<java.lang.Long>]
     * @return java.util.List<com.ruoyi.service.dto.ChatGroupStatisticsDto>
     **/
    List<ChatGroupStatisticsDto> selectGroupPersonList(List<Long> productIdList);

    /**
     * 通过商品id获取聊天组信息
     * @param productId
     * @return
     */
    List<ChatGroupInfoVo> getChatGroupInfoVoListByProductId(Long productId);

    List<OrderChatGroupInfoVo> getChatGroupInfoVoListByOrderList(List<MyOrderListVo> myOrderListVoList);

    /**
     * 小程序-我加入的群组集合
     * @return
     */
    List<JoinChatGroupVo> visitorList(Long userId);

    List<TfpChatGroup> selectTfpChatGroupListByProductIdList(List<Long> productIdList);

    TfpChatGroup getLastGroupByProductIdAndGoDate(Long productId, Date goDate);

    Long handlePaySuccessChatGroupInfo(TfpOrder tfpOrder, Long categoryId);

    MoreListVo moreList(TfpChatGroupDto tfpChatGroupDto);

    List<MoreListMonthGroupVo> moreGroupList(MoreGroupListDto moreGroupListDto);

    void deleteTfpChatGroupByProductIds(List<Long> productIdList);
}
