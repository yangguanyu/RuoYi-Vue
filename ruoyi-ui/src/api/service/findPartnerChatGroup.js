import request from '@/utils/request'

// 查询找搭子聊天群组列表
export function listFindPartnerChatGroup(query) {
  return request({
    url: '/service/findPartnerChatGroup/list',
    method: 'get',
    params: query
  })
}

// 查询找搭子聊天群组详细
export function getFindPartnerChatGroup(id) {
  return request({
    url: '/service/findPartnerChatGroup/' + id,
    method: 'get'
  })
}

// 新增找搭子聊天群组
export function addFindPartnerChatGroup(data) {
  return request({
    url: '/service/findPartnerChatGroup',
    method: 'post',
    data: data
  })
}

// 修改找搭子聊天群组
export function updateFindPartnerChatGroup(data) {
  return request({
    url: '/service/findPartnerChatGroup',
    method: 'put',
    data: data
  })
}

// 删除找搭子聊天群组
export function delFindPartnerChatGroup(id) {
  return request({
    url: '/service/findPartnerChatGroup/' + id,
    method: 'delete'
  })
}

export function listFindPartnerChatGroupInfoList(query) {
  return request({
    url: '/service/findPartnerChatGroupPersonInfo/chatList',
    method: 'get',
    params: query
  })
}

export function findPartnerChatGroupDetail(id) {
  return request({
    url: '/service/findPartnerChatGroup/detail/' + id,
    method: 'get'
  })
}

