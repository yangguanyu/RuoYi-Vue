package com.ruoyi.service.dto;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.List;

/**
 * 找搭子聊天群组成员对象 tfp_find_partner_chat_group_person
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@Data
public class TfpFindPartnerChatGroupPersonDto extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 聊天群组id */
    private Long chatGroupId;

    /** 聊天群组名称 */
    private String chatGroupName;

    private List<Long> chatGroupIdList;

    private boolean chatGroupNameFlag;

    /** 用户id */
    private Long userId;

    /** 用户名称 */
    private String userName;

    /** 用户类型 0-发起者 1-已报名 2-未报名 */
    private Integer userType;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    private Long createUserId;

    /** 更新者id */
    private Long updateUserId;
}
