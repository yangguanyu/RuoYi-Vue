package com.ruoyi.service.service.impl;

import java.util.*;
import java.util.stream.Collectors;

import com.ruoyi.common.constant.CommonConstants;
import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.core.domain.entity.SysDictData;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.entity.TfpAttachment;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enuma.*;
import com.ruoyi.common.exception.BusinessException;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.DcListUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.bean.CustomizeBeanCopier;
import com.ruoyi.service.domain.*;
import com.ruoyi.service.dto.AuditDto;
import com.ruoyi.service.dto.TfpFindPartnerDto;
import com.ruoyi.service.service.*;
import com.ruoyi.service.vo.*;
import com.ruoyi.system.service.ISysDictDataService;
import com.ruoyi.system.service.ISysUserService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.compress.utils.Lists;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpFindPartnerMapper;
import org.springframework.transaction.annotation.Transactional;

import static com.ruoyi.common.utils.PageUtils.startPage;

/**
 * 找搭子Service业务层处理
 *
 * @author ruoyi
 * @date 2023-11-01
 */
@Service
@Transactional
public class TfpFindPartnerServiceImpl implements ITfpFindPartnerService
{
    @Autowired
    private TfpFindPartnerMapper tfpFindPartnerMapper;
    @Autowired
    private ITfpFindPartnerChatGroupService findPartnerChatGroupService;
    @Autowired
    private ITfpFindPartnerSignUpService findPartnerSignUpService;
    @Autowired
    private ISysUserService userService;
    @Autowired
    private ISysUserCareService userCareService;

    @Autowired
    private ITfpAttachmentService attachmentService;

    @Autowired
    private ISysDictDataService dictDataService;
    /**
     * 查询找搭子
     *
     * @param id 找搭子主键
     * @return 找搭子
     */
    @Override
    public TfpFindPartner selectTfpFindPartnerById(Long id)
    {
        return tfpFindPartnerMapper.selectTfpFindPartnerById(id);
    }

    /**
     * 查询找搭子列表
     *
     * @param tfpFindPartner 找搭子
     * @return 找搭子
     */
    @Override
    public List<TfpFindPartner> selectTfpFindPartnerList(TfpFindPartnerDto tfpFindPartner)
    {
        startPage();
        List<TfpFindPartner> tfpFindPartners = tfpFindPartnerMapper.selectTfpFindPartnerList(tfpFindPartner);
        //获取搭子人员信息

        return tfpFindPartners;
    }

    public List<TfpFindPartner> selectManageFindPartnerList(TfpFindPartnerDto tfpFindPartner) {
        //适配前端
        tfpFindPartner.setStatus(tfpFindPartner.getAuditStatus());
        //判断有电话，那么根据电话查看人员id
        if(Objects.nonNull(tfpFindPartner.getPhonenumber())){
            if(tfpFindPartner.getPhonenumber().length() != 11){
                throw new ServiceException("请输入11位的手机号");
            }
            SysUser userInfoByPhoneNumber = userService.getUserInfoByPhoneNumber(tfpFindPartner.getPhonenumber());
            if(Objects.isNull(userInfoByPhoneNumber)){
                return Lists.newArrayList();
            }else {
                tfpFindPartner.setCreateUserId(userInfoByPhoneNumber.getUserId());
            }
        }
        startPage();
        List<TfpFindPartner> tfpFindPartners = tfpFindPartnerMapper.selectTfpFindPartnerList(tfpFindPartner);
        if (CollectionUtils.isEmpty(tfpFindPartners)){
            return tfpFindPartners;
        }

        //获取搭子人员信息
        List<Long> partnerIdList = tfpFindPartners.stream().map(TfpFindPartner::getId).collect(Collectors.toList());
        Map<Long, Integer> findPartnerSignUpMap = getSignUpCountMap(partnerIdList);
        //获取人员信息
        List<Long> userIdList = tfpFindPartners.stream().map(TfpFindPartner::getCreateUserId).collect(Collectors.toList());
        Map<Long, SysUser> userInfoAndAvatar = userService.getUserInfoTransMap(userIdList);
        for (TfpFindPartner findPartner : tfpFindPartners) {
            //放入统计人数
            Integer signUpCount = findPartnerSignUpMap.get(findPartner.getId());
            if(Objects.isNull(signUpCount)){
                findPartner.setSignUpCount(CommonConstants.ZERO);
            }else {
                findPartner.setSignUpCount(signUpCount);
            }
            //放入手机号
            SysUser sysUser = userInfoAndAvatar.get(findPartner.getCreateUserId());
            if(Objects.nonNull(sysUser)){
                findPartner.setPhonenumber(sysUser.getPhonenumber());
            }
            FindPartnerStatusEnum statusEnum = BaseEnum.getEnumByCode(FindPartnerStatusEnum.class, findPartner.getStatus());

            if(Objects.nonNull(statusEnum)){
                findPartner.setStatusName(statusEnum.getDesc());
            }
        }
        return tfpFindPartners;
    }

    /*/**
     * @Author yangguanyu
     * @Description :根据群组Id，获取报名人数信息
     * @Date 16:18 2024/4/17
     * @Param [java.util.List<java.lang.Long>]
     * @return java.util.Map<java.lang.Long,java.lang.Integer>
     **/
    public Map<Long, Integer> getSignUpCountMap(List<Long> partnerIdList){
        List<FindPartnerSignUpVo> findPartnerSignUpVoList = findPartnerSignUpService.selectSignUpPersonNumberByFindPartnerIdList(partnerIdList);
        Map<Long, Integer> findPartnerSignUpMap = new HashMap<>();
        if(CollectionUtils.isNotEmpty(findPartnerSignUpVoList)){
            findPartnerSignUpMap = findPartnerSignUpVoList.stream().collect(Collectors.toMap(FindPartnerSignUpVo::getFindPartnerId, FindPartnerSignUpVo::getSignUpPersonNumber,(k1,k2)->k1));
        }
        return findPartnerSignUpMap;
    }

    /**
     * 查询找搭子列表-进行中列表
     *
     * @param tfpFindPartner 找搭子
     * @return 找搭子
     */
    @Override
    public List<TfpFindPartner> selectProgressList(TfpFindPartnerDto tfpFindPartner)
    {
        startPage();
        List<TfpFindPartner> tfpFindPartners = tfpFindPartnerMapper.selectProgressList(tfpFindPartner);
        //获取搭子人员信息

        return tfpFindPartners;
    }

    //找搭子定时任务，quartz反射调用
    @Transactional
    public void refreshFindPartner()
    {
        //还要注意新增的时候设为1
        //查询出审核通过的，然后activityStatus为1和2的，大于报名截止时间
        TfpFindPartnerDto tfpFindPartner = new TfpFindPartnerDto();
        tfpFindPartner.setActivityRefresh(Boolean.TRUE);
        List<TfpFindPartner> tfpFindPartners = tfpFindPartnerMapper.selectTfpFindPartnerList(tfpFindPartner);
        if(CollectionUtils.isEmpty(tfpFindPartners)){
            return;
        }
        Date nowDate = DateUtils.getNowDate();
        for (TfpFindPartner findPartner : tfpFindPartners) {

            Date startTime = findPartner.getStartTime();
            Date signUpEndTime = findPartner.getSignUpEndTime();
            if(Objects.isNull(startTime)||Objects.isNull(signUpEndTime)){
                continue;
            }
            //如果是null，那么设置为进行中
            if(Objects.isNull(findPartner.getActivityStatus())){
                findPartner.setActivityStatus(ActivityStatusEnum.ING.getValue());
            }


            //找搭子报名中：审核通过就是报名中，当然也要看一看是不是过了开始时间
            if(nowDate.before(signUpEndTime)){
                findPartner.setActivityStatus(ActivityStatusEnum.ING.getValue());
            }
            //即将发车：报名截止时间已过，并且小于开始时间
            if(nowDate.after(signUpEndTime)&&nowDate.before(startTime)){
                findPartner.setActivityStatus(ActivityStatusEnum.REARY_GO.getValue());
            }
            //已结束：开始时间已过
            if(nowDate.after(startTime)){
                findPartner.setActivityStatus(ActivityStatusEnum.FINISH.getValue());
            }
            findPartner.setUpdateTime(nowDate);
            tfpFindPartnerMapper.updateTfpFindPartner(findPartner);

            //判断是否创建群组
            TfpFindPartnerChatGroup group = findPartnerChatGroupService.selectByFindPartnerId(findPartner.getId());
            if(Objects.isNull(group)){
                findPartnerChatGroupService.saveFindPartnerChatGroupTimeTask(findPartner);
            }

        }
        return ;
    }

    /**
     * 新增找搭子
     *
     * @param tfpFindPartner 找搭子
     * @return 结果
     */
    @Override
    public int insertTfpFindPartner(TfpFindPartner tfpFindPartner)
    {
        tfpFindPartner.setCreateTime(DateUtils.getNowDate());
        return tfpFindPartnerMapper.insertTfpFindPartner(tfpFindPartner);
    }

    @Override
    public int appletInsert(TfpFindPartner tfpFindPartner)
    {
        tfpFindPartner.setCreateTime(DateUtils.getNowDate());
        int result = tfpFindPartnerMapper.insertTfpFindPartner(tfpFindPartner);
        Long findPartnerId = tfpFindPartner.getId();
        if(Objects.isNull(findPartnerId)){
            throw new BusinessException("添加后未获取到id");
        }
        List<TfpAttachment> attachmentList = tfpFindPartner.getAttachmentList();
        if(CollectionUtils.isNotEmpty(attachmentList)){
            for (TfpAttachment tfpAttachment : attachmentList) {
                tfpAttachment.setBusinessId(findPartnerId);
                attachmentService.insertTfpAttachment(tfpAttachment);
            }
        }else {
            throw new BusinessException("未获取到对应的图片");
        }

//        if (Objects.equals(AuditStatusEnum.AUDIT_NO_PASS.getValue(), tfpFindPartner.getStatus())){
            findPartnerChatGroupService.saveFindPartnerChatGroup(tfpFindPartner);
//        }

        return result;
    }

    @Override
    public int appletUpdate(TfpFindPartner tfpFindPartner)
    {
        Long findPartnerId = tfpFindPartner.getId();

        //首先判断当前登录人是否是创建的那个人，如果不是，那么不能修改
        TfpFindPartner beforeInfo = selectTfpFindPartnerById(findPartnerId);
        //原来的创建人
        Long createUserId = beforeInfo.getCreateUserId();
        Long userId = SecurityUtils.getUserId();
        if(!userId.equals(createUserId)){
            throw new ServiceException("您不是这个搭子的创建者不能修改", HttpStatus.UNAUTHORIZED);
        }

        tfpFindPartner.setUpdateTime(DateUtils.getNowDate());
        int result = updateTfpFindPartner(tfpFindPartner);

        List<TfpAttachment> attachmentList = tfpFindPartner.getAttachmentList();
        if(CollectionUtils.isNotEmpty(attachmentList)){
            for (TfpAttachment tfpAttachment : attachmentList) {
                //判断原有的图片，如果有id，那么不进行添加，没有id进行添加
                if(Objects.isNull(tfpAttachment.getId())){
                    continue;
                }
                tfpAttachment.setBusinessId(findPartnerId);
                attachmentService.insertTfpAttachment(tfpAttachment);
            }
        }else {
            throw new BusinessException("未获取到对应的图片");
        }
        return result;
    }

    /**
     * 修改找搭子
     *
     * @param tfpFindPartner 找搭子
     * @return 结果
     */
    @Override
    public int updateTfpFindPartner(TfpFindPartner tfpFindPartner)
    {
        tfpFindPartner.setUpdateTime(DateUtils.getNowDate());
        return tfpFindPartnerMapper.updateTfpFindPartner(tfpFindPartner);
    }

    @Override
    public int approve(TfpFindPartner tfpFindPartner) {
        TfpFindPartner partner = tfpFindPartnerMapper.selectTfpFindPartnerById(tfpFindPartner.getId());
        if(Objects.isNull(partner)){
            throw new BusinessException("未获取到具体的搭子");
        }

        // 已发布待审核，如果超过报名截止时间，直接审核失败。失败原因‘业务人员过期未审核‘
        Integer status = partner.getStatus();
        if (Objects.equals(status, FindPartnerStatusEnum.PUBLISH_AND_PRE_AUTH.getValue())){
            Date signUpEndTime = partner.getSignUpEndTime();
            if (new Date().after(signUpEndTime)){
                partner.setStatus(FindPartnerStatusEnum.REJECT.getValue());
                partner.setReason("业务人员过期未审核");
                return tfpFindPartnerMapper.updateTfpFindPartner(tfpFindPartner);
            }
        } else {
            throw new BusinessException("仅已发布待审核状态可以进行审核");
        }

        if (!Objects.equals(AuditStatusEnum.AUDIT_PASS.getValue(), tfpFindPartner.getStatus())
                && !Objects.equals(AuditStatusEnum.AUDIT_NO_PASS.getValue(), tfpFindPartner.getStatus())){
            throw new BusinessException("审核状态有误");
        }
        if(Objects.equals(AuditStatusEnum.AUDIT_PASS.getValue(), tfpFindPartner.getStatus())){
            tfpFindPartner.setActivityStatus(ActivityStatusEnum.ING.getValue());
        }
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = loginUser.getUserId();
        tfpFindPartner.setUpdateUserId(userId);
        tfpFindPartner.setUpdateBy(loginUser.getUsername());
        tfpFindPartner.setUpdateTime(DateUtils.getNowDate());
        return tfpFindPartnerMapper.updateTfpFindPartner(tfpFindPartner);
    }

    @Override
    public Map<Long, List<TfpFindPartnerSignUp>> getSignUpAuditPassListByIdList(List<Long> idList) {
        // 获取报名通过信息
        List<TfpFindPartnerSignUp> findPartnerSignUpList = findPartnerSignUpService.selectListByFindPartnerIdListAndAuditStatus(idList, AuditStatusEnum.AUDIT_PASS.getValue());
        Map<Long, List<TfpFindPartnerSignUp>> signUpAuditPassMap = new HashMap<>();
        if (DcListUtils.isNotEmpty(findPartnerSignUpList)){
            signUpAuditPassMap = findPartnerSignUpList.stream().collect(Collectors.groupingBy(TfpFindPartnerSignUp::getFindPartnerId));
        }

        return signUpAuditPassMap;
    }

    @Override
    public List<TfpFindPartner> selectOverAuditTimeFindPartnerList() {
        return tfpFindPartnerMapper.selectOverAuditTimeFindPartnerList();
    }

    /**
     * 小程序-审核找搭子
     * @param auditDto
     * @return
     */
    @Override
    public int auditFindPartner(AuditDto auditDto) {
//        Integer auditStatus = auditDto.getAuditStatus();
//        if (!Objects.equals(AuditStatusEnum.AUDIT_PASS.getValue(), auditStatus) && !Objects.equals(AuditStatusEnum.AUDIT_NO_PASS.getValue(), auditStatus)){
//            throw new BusinessException("审核状态有误");
//        }
//
//        LoginUser loginUser = SecurityUtils.getLoginUser();
//        Long userId = loginUser.getUserId();
//
//        Long id = auditDto.getId();
//        TfpFindPartner tfpFindPartner = tfpFindPartnerMapper.selectTfpFindPartnerById(id);
//
//        tfpFindPartner.setStatus(auditStatus);
//        if (Objects.equals(AuditStatusEnum.AUDIT_NO_PASS.getValue(), auditStatus)){
//            tfpFindPartner.setReason(auditDto.getReason());
//        }
//
//        tfpFindPartner.setUpdateUserId(userId);
//        tfpFindPartner.setUpdateBy(loginUser.getUsername());
//        tfpFindPartner.setUpdateTime(DateUtils.getNowDate());
//        tfpFindPartnerMapper.updateTfpFindPartner(tfpFindPartner);
//
//        if (Objects.equals(AuditStatusEnum.AUDIT_PASS.getValue(), auditStatus)){
//            findPartnerChatGroupService.saveFindPartnerChatGroup(tfpFindPartner);
//        }
        return 1;
    }

    /**
     * 批量删除找搭子
     *
     * @param ids 需要删除的找搭子主键
     * @return 结果
     */
    @Override
    public int deleteTfpFindPartnerByIds(Long[] ids)
    {
        return tfpFindPartnerMapper.deleteTfpFindPartnerByIds(ids);
    }

    /**
     * 删除找搭子信息
     *
     * @param id 找搭子主键
     * @return 结果
     */
    @Override
    public int deleteTfpFindPartnerById(Long id)
    {
        return tfpFindPartnerMapper.deleteTfpFindPartnerById(id);
    }

    @Override
    public List<TfpFindPartner> selectTfpFindPartnerListByIdList(List<Long> idList) {
        return tfpFindPartnerMapper.selectTfpFindPartnerListByIdList(idList);
    }

    @Override
    public FindPartnerDetailVo detail(Long id) {
        FindPartnerDetailVo findPartnerDetailVo = new FindPartnerDetailVo();

        // 找搭子群组唯一
        TfpFindPartnerChatGroup tfpFindPartnerChatGroup = findPartnerChatGroupService.selectByFindPartnerId(id);
        if (tfpFindPartnerChatGroup != null){
            findPartnerDetailVo.setFindPartnerChatGroupId(tfpFindPartnerChatGroup.getId());
            findPartnerDetailVo.setFindPartnerChatGroupName(tfpFindPartnerChatGroup.getName());
        }

        List<TfpFindPartnerSignUp> findPartnerSignUpList = findPartnerSignUpService.selectListByFindPartnerId(id);
        if (DcListUtils.isNotEmpty(findPartnerSignUpList)){
            List<Long> signUpUserIdList = findPartnerSignUpList.stream().map(TfpFindPartnerSignUp::getSignUpUserId).collect(Collectors.toList());
            List<UserVo> userVoList = userService.handleUserVo(signUpUserIdList);
            findPartnerDetailVo.setUserList(userVoList);
        }
        //获取图片
        List<Long> businessIdList = new ArrayList<>();
        businessIdList.add(id);
        List<TfpAttachment> tfpAttachments = attachmentService.selectList(businessIdList,
                BusinessTypeEnum.FIND_PARTNER.getValue(), BusinessSubTypeEnum.PARTNER_IMAGE.getValue());
        findPartnerDetailVo.setAttachmentList(Lists.newArrayList());
        if(CollectionUtils.isNotEmpty(tfpAttachments)){
            List<AttachmentVO> attachmentVOS = CustomizeBeanCopier.copyPropertiesOfList(tfpAttachments, AttachmentVO.class);
            findPartnerDetailVo.setAttachmentList(attachmentVOS);
        }

        return findPartnerDetailVo;
    }


    @Override
    public FindPartnerDetailAppletVo appletDetail(Long id) {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = loginUser.getUserId();

        TfpFindPartner tfpFindPartner = tfpFindPartnerMapper.selectTfpFindPartnerById(id);
        if(Objects.isNull(tfpFindPartner)){
            throw new BusinessException("未获取到对应的找搭子信息");
        }
        FindPartnerDetailAppletVo findPartnerDetailVo = CustomizeBeanCopier.copyProperties(tfpFindPartner,FindPartnerDetailAppletVo.class);
        // 找搭子群组唯一
        TfpFindPartnerChatGroup tfpFindPartnerChatGroup = findPartnerChatGroupService.selectByFindPartnerId(id);
        if (tfpFindPartnerChatGroup != null){
            findPartnerDetailVo.setFindPartnerChatGroupId(tfpFindPartnerChatGroup.getId());
            findPartnerDetailVo.setFindPartnerChatGroupName(tfpFindPartnerChatGroup.getName());
        }

        findPartnerDetailVo.setMySignUpFlag(YesOrNoFlagEnum.NO.getValue());

        //获取上车人的列表 审核状态 0-待审核 1-审核通过 2-审核未通过
        List<TfpFindPartnerSignUp> findPartnerSignUpList = findPartnerSignUpService.selectListByFindPartnerId(id);
        if (DcListUtils.isNotEmpty(findPartnerSignUpList)){
            List<Long> signUpUserIdList = new ArrayList<>();
            for (TfpFindPartnerSignUp tfpFindPartnerSignUp : findPartnerSignUpList) {
                Long signUpUserId = tfpFindPartnerSignUp.getSignUpUserId();
                Integer auditStatus = tfpFindPartnerSignUp.getAuditStatus();
                if (Objects.equals(AuditStatusEnum.AUDIT_PASS.getValue(), auditStatus)){
                    if (!signUpUserIdList.contains(signUpUserId)){
                        signUpUserIdList.add(signUpUserId);
                    }
                }

                if (Objects.equals(signUpUserId, userId) && Objects.equals(YesOrNoFlagEnum.NO.getValue(), findPartnerDetailVo.getMySignUpFlag())){
                    if (Objects.equals(AuditStatusEnum.WAIT_AUDIT.getValue(), auditStatus)
                    || Objects.equals(AuditStatusEnum.AUDIT_PASS.getValue(), auditStatus)){
                        findPartnerDetailVo.setMySignUpFlag(YesOrNoFlagEnum.YES.getValue());
                    }
                }
            }

            List<UserVo> userVoList = userService.handleUserAndAvatarVo(signUpUserIdList);
            findPartnerDetailVo.setUserList(userVoList);
        }

        //获取图片
        List<Long> businessIdList = new ArrayList<>();
        businessIdList.add(id);
        List<TfpAttachment> tfpAttachments = attachmentService.selectList(businessIdList,
                BusinessTypeEnum.FIND_PARTNER.getValue(), BusinessSubTypeEnum.PARTNER_IMAGE.getValue());
        findPartnerDetailVo.setAttachmentList(Lists.newArrayList());
        if(CollectionUtils.isNotEmpty(tfpAttachments)){
            List<AttachmentVO> attachmentVOS = CustomizeBeanCopier.copyPropertiesOfList(tfpAttachments, AttachmentVO.class);
            findPartnerDetailVo.setAttachmentList(attachmentVOS);
        }

        Long createUserId = tfpFindPartner.getCreateUserId();
        findPartnerDetailVo.setUserId(createUserId);

        SysUser sysUser = userService.selectUserById(createUserId);
        findPartnerDetailVo.setNickName(sysUser.getNickName());

        List<TfpAttachment> attachmentList = attachmentService.selectListByBusinessId(createUserId, BusinessTypeEnum.USER.getValue(), BusinessSubTypeEnum.USER_HEAD_PORTRAIT.getValue());
        if (DcListUtils.isNotEmpty(attachmentList)){
            findPartnerDetailVo.setUserHeadImage(attachmentList.get(0).getUrl());
        }

        if (!Objects.equals(userId, createUserId)){
            SysUserCare param = new SysUserCare();
            param.setOwnerUserId(createUserId);
            param.setFansUserId(userId);
            param.setDelFlag(DeleteEnum.EXIST.getValue());
            List<SysUserCare> sysUserCareList = userCareService.selectSysUserCareList(param);
            if (DcListUtils.isNotEmpty(sysUserCareList)){
                findPartnerDetailVo.setUserCareFlag(YesOrNoFlagEnum.YES.getValue());
            } else {
                findPartnerDetailVo.setUserCareFlag(YesOrNoFlagEnum.NO.getValue());
            }
        }

        //处理活动时间和报名截止时间
        findPartnerDetailVo.setStartTimeFormat(StringUtils.EMPTY);
        findPartnerDetailVo.setSignUpEndTimeFormat(StringUtils.EMPTY);
        if(Objects.nonNull(tfpFindPartner.getStartTime())){
            String formatStart = DateUtils.handlePartnerDay(tfpFindPartner.getStartTime());
            findPartnerDetailVo.setStartTimeFormat(formatStart);
        }
        if(Objects.nonNull(tfpFindPartner.getSignUpEndTime())){
            String formatSignUp = DateUtils.handlePartnerDay(tfpFindPartner.getSignUpEndTime());
            findPartnerDetailVo.setSignUpEndTimeFormat(formatSignUp);
        }

        //获取字典
        if(Objects.nonNull(findPartnerDetailVo.getPriceCode())){
            SysDictData sysDictData = dictDataService.selectDictDataById(findPartnerDetailVo.getPriceCode());
            if(Objects.nonNull(sysDictData)){
                findPartnerDetailVo.setPriceCodeName(sysDictData.getDictLabel());
            }
        }

        List<Long> partnerIds = Lists.newArrayList();
        partnerIds.add(tfpFindPartner.getId());
        Map<Long, Integer> signUpCountMap = getSignUpCountMap(partnerIds);
        Integer signUpCount = signUpCountMap.get(tfpFindPartner.getId());
        if(Objects.nonNull(signUpCount)){
            findPartnerDetailVo.setSignUpCount(signUpCount);
        } else {
            findPartnerDetailVo.setSignUpCount(CommonConstants.ZERO);
        }
        return findPartnerDetailVo;
    }



//    public static void main(String[] args) {
//        LocalDateTime now = LocalDateTime.now();
//        DayOfWeek dayOfWeek = now.getDayOfWeek();
//        String displayName = dayOfWeek.getDisplayName(TextStyle.FULL, Locale.CHINESE);
//        System.out.println(displayName);
//        System.out.println(dayOfWeek.getValue());
//        int monthValue = now.getMonthValue();
//        int dayOfMonth = now.getDayOfMonth();
//        System.out.println(monthValue);
//        System.out.println(String.format("%02d",monthValue));
//        System.out.println(dayOfMonth);
//        System.out.println(String.format("%02d",dayOfMonth));
//        String s = DateUtils.handlePartnerDay(DateUtils.getNowDate());
//        System.out.println(s);
//    }

    /**
     * 小程序-我发起的搭子
     * @return
     */
    @Override
    public List<InitiateFindPartnerVo> initiateFindPartnerList(Long userId) {
        if (Objects.isNull(userId)){
            throw new RuntimeException("用户id不能为空");
        }

        startPage();
        List<InitiateFindPartnerVo> initiateFindPartnerVoList = tfpFindPartnerMapper.initiateFindPartnerList(userId);
        if (DcListUtils.isNotEmpty(initiateFindPartnerVoList)){
            List<TfpAttachment> attachmentList = attachmentService.selectListByBusinessId(userId, BusinessTypeEnum.USER.getValue(), BusinessSubTypeEnum.USER_HEAD_PORTRAIT.getValue());
            String headImage = attachmentList.get(0).getUrl();

            List<Long> idList = initiateFindPartnerVoList.stream().map(InitiateFindPartnerVo::getId).collect(Collectors.toList());

            // 获取报名通过信息
            Map<Long, List<TfpFindPartnerSignUp>> signUpAuditPassMap = this.getSignUpAuditPassListByIdList(idList);

            List<TfpAttachment> findPartnerAttachmentList = attachmentService.selectList(idList, BusinessTypeEnum.FIND_PARTNER.getValue(), BusinessSubTypeEnum.PARTNER_IMAGE.getValue());
            Map<Long, List<TfpAttachment>> findPartnerImageMap;
            if (DcListUtils.isNotEmpty(findPartnerAttachmentList)){
                findPartnerImageMap = findPartnerAttachmentList.stream().collect(Collectors.groupingBy(TfpAttachment::getBusinessId));
            } else {
                findPartnerImageMap = new HashMap<>();
            }

            List<TfpFindPartnerChatGroup> findPartnerChatGroupList = findPartnerChatGroupService.selectTfpFindPartnerChatGroupListByFindPartnerIdList(idList);
            Map<Long, Long> findPartnerChatGroupIdMap;
            if (DcListUtils.isNotEmpty(findPartnerChatGroupList)){
                findPartnerChatGroupIdMap = findPartnerChatGroupList.stream().collect(Collectors.toMap(TfpFindPartnerChatGroup::getFindPartnerId, TfpFindPartnerChatGroup::getId));
            } else {
                findPartnerChatGroupIdMap = new HashMap<>();
            }

            SysUser sysUser = userService.selectUserById(userId);

            for (InitiateFindPartnerVo initiateFindPartnerVo : initiateFindPartnerVoList) {
                Long id = initiateFindPartnerVo.getId();
                initiateFindPartnerVo.setNickName(sysUser.getNickName());
                initiateFindPartnerVo.setHeadImage(headImage);

                initiateFindPartnerVo.setShowStatus(this.handleShowStatus(initiateFindPartnerVo.getStatus(), initiateFindPartnerVo.getStartTime(), initiateFindPartnerVo.getSignUpEndTime()));

                Integer signUpPersonNumber = 0;
                List<TfpFindPartnerSignUp> findPartnerSignUpList = signUpAuditPassMap.get(id);
                if (DcListUtils.isNotEmpty(findPartnerSignUpList)){
                    signUpPersonNumber = findPartnerSignUpList.size();
                }

                initiateFindPartnerVo.setSignUpPersonNumber(signUpPersonNumber);
                initiateFindPartnerVo.setChatGroupId(findPartnerChatGroupIdMap.get(id));

                List<TfpAttachment> findPartnerImageList = findPartnerImageMap.get(id);
                if (DcListUtils.isNotEmpty(findPartnerImageList)){
                    initiateFindPartnerVo.setImage(findPartnerImageList.get(0).getUrl());
                }
            }
        }

        return initiateFindPartnerVoList;
    }

    /**
     * 小程序-我参与的搭子列表
     * @return
     */
    @Override
    public List<MyJoinFindPartnerVo> myJoinFindPartnerList() {
        List<MyJoinFindPartnerVo> myJoinFindPartnerVoList = new ArrayList<>();

        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = loginUser.getUserId();

        List<TfpFindPartnerSignUp> findPartnerSignUpList = findPartnerSignUpService.selectListBySignUpUserId(userId);
        if (DcListUtils.isNotEmpty(findPartnerSignUpList)){
            Map<Long, TfpFindPartnerSignUp> myFindPartnerSignUpMap = new HashMap<>();
            List<Long> findPartnerIdList = new ArrayList<>();
            for (TfpFindPartnerSignUp tfpFindPartnerSignUp : findPartnerSignUpList) {
                myFindPartnerSignUpMap.put(tfpFindPartnerSignUp.getFindPartnerId(), tfpFindPartnerSignUp);

                Long findPartnerId = tfpFindPartnerSignUp.getFindPartnerId();
                if (!findPartnerIdList.contains(findPartnerId)){
                    findPartnerIdList.add(findPartnerId);
                }
            }

            startPage();
            myJoinFindPartnerVoList = tfpFindPartnerMapper.myJoinFindPartnerList(findPartnerIdList);
            if (DcListUtils.isNotEmpty(myJoinFindPartnerVoList)){
                List<Long> userIdList = myJoinFindPartnerVoList.stream().map(MyJoinFindPartnerVo::getCreateUserId).collect(Collectors.toList());

                Map<Long, String> nickNameMap = userService.getNickNameMap(userIdList);

                List<TfpAttachment> headPortraitList = userService.getHeadPortraitListByUserIdList(userIdList);
                Map<Long, String> headImageMap = headPortraitList.stream().collect(Collectors.toMap(TfpAttachment::getBusinessId, TfpAttachment::getUrl));

                Map<Long, TfpFindPartnerChatGroup> findPartnerChatGroupMap = new HashMap<>();
                List<TfpFindPartnerChatGroup> findPartnerChatGroupList = findPartnerChatGroupService.selectTfpFindPartnerChatGroupListByFindPartnerIdList(findPartnerIdList);
                if (DcListUtils.isNotEmpty(findPartnerChatGroupList)){
                    findPartnerChatGroupMap = findPartnerChatGroupList.stream().collect(Collectors.toMap(TfpFindPartnerChatGroup::getFindPartnerId, TfpFindPartnerChatGroup -> TfpFindPartnerChatGroup));
                }

                List<FindPartnerSignUpVo> findPartnerSignUpVoList = findPartnerSignUpService.selectSignUpPersonNumberByFindPartnerIdList(findPartnerIdList);
                Map<Long, Integer> findPartnerSignUpMap;
                if (DcListUtils.isNotEmpty(findPartnerSignUpVoList)){
                    findPartnerSignUpMap = findPartnerSignUpVoList.stream().collect(Collectors.toMap(FindPartnerSignUpVo::getFindPartnerId, FindPartnerSignUpVo::getSignUpPersonNumber));
                } else {
                    findPartnerSignUpMap = new HashMap<>();
                }

                List<TfpAttachment> attachmentList = attachmentService.selectList(findPartnerIdList, BusinessTypeEnum.FIND_PARTNER.getValue(), BusinessSubTypeEnum.PARTNER_IMAGE.getValue());
                Map<Long, List<TfpAttachment>> findPartnerImageMap;
                if (DcListUtils.isNotEmpty(attachmentList)){
                    findPartnerImageMap = attachmentList.stream().collect(Collectors.groupingBy(TfpAttachment::getBusinessId));
                } else {
                    findPartnerImageMap = new HashMap<>();
                }

                for (MyJoinFindPartnerVo myJoinFindPartnerVo : myJoinFindPartnerVoList) {
                    Long id = myJoinFindPartnerVo.getId();
                    TfpFindPartnerChatGroup tfpFindPartnerChatGroup = findPartnerChatGroupMap.get(id);

                    Long createUserId = myJoinFindPartnerVo.getCreateUserId();

                    myJoinFindPartnerVo.setNickName(nickNameMap.get(createUserId));
                    myJoinFindPartnerVo.setHeadImage(headImageMap.get(createUserId));
                    List<TfpAttachment> findPartnerImageList = findPartnerImageMap.get(id);
                    if (DcListUtils.isNotEmpty(findPartnerImageList)){
                        myJoinFindPartnerVo.setImage(findPartnerImageList.get(0).getUrl());
                    }

                    myJoinFindPartnerVo.setShowStatus(this.handleShowStatus(myJoinFindPartnerVo.getStatus(), myJoinFindPartnerVo.getStartTime(), myJoinFindPartnerVo.getSignUpEndTime()));
                    if (Objects.nonNull(tfpFindPartnerChatGroup)){
                        myJoinFindPartnerVo.setGroupId(tfpFindPartnerChatGroup.getId());
                    }

                    Integer signUpPersonNumber = findPartnerSignUpMap.get(id);
                    myJoinFindPartnerVo.setSignUpPersonNumber(Objects.isNull(signUpPersonNumber) ? CommonConstants.ZERO : signUpPersonNumber);
                    TfpFindPartnerSignUp tfpFindPartnerSignUp = myFindPartnerSignUpMap.get(id);
                    if (Objects.nonNull(tfpFindPartnerSignUp)){
                        myJoinFindPartnerVo.setAuditStatus(tfpFindPartnerSignUp.getAuditStatus());
                    }
                }
            }
        }

        return myJoinFindPartnerVoList;
    }

    /**
     * 处理搭子展示状态
     * @param status
     * @param startTime
     * @param signUpEndTime
     * @return
     */
    private String handleShowStatus(Integer status, Date startTime, Date signUpEndTime){
        String showStatus = null;

        if (Objects.equals(FindPartnerStatusEnum.PUBLISH_AND_PRE_AUTH.getValue(), status)){
            showStatus = "审核中";
        }

        if (Objects.equals(FindPartnerStatusEnum.REJECT.getValue(), status)){
            showStatus = "未通过";
        }

        if (Objects.equals(FindPartnerStatusEnum.PASS.getValue(), status)){
            Date now = new Date();
            if (now.before(signUpEndTime)){
                showStatus = "报名中";
            }

            if (now.after(signUpEndTime) && now.before(startTime)){
                showStatus = "即将发车";
            }

            if (now.after(startTime)){
                showStatus = "已结束";
            }
        }

        return showStatus;
    }
}
