package com.ruoyi.common.enuma;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 商品归属
 *
 * @author
 * @date
 * @time
 */
@Getter
@AllArgsConstructor
public enum CommoditiesTypeEnum implements BaseEnum {
    CHINA(0, "国内"),
    OVERSEAS(1, "海外"),
    ;


    private Integer value;
    private String desc;
}
