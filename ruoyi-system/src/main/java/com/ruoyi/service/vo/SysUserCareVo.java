package com.ruoyi.service.vo;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 关注对象 sys_user_care
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@Data
public class SysUserCareVo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 主人用户id */
    private Long ownerUserId;

    private String ownerNickName;

    /** 粉丝用户id */
    private Long fansUserId;

    private String fansNickName;

    /** 是否互相关注 0-否 1-是 */
    private Integer mutualAttentionFlag;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    private Long createUserId;

    /** 更新者id */
    private Long updateUserId;

}
