package com.ruoyi.service.vo;

import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 找搭子聊天群组成员聊天信息对象 tfp_find_partner_chat_group_person_info
 *
 * @author ruoyi
 * @date 2023-10-31
 */
@Data
public class TfpFindPartnerChatGroupPersonInfoVo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty("id")
    private Long id;

    /** 聊天群组id */
    @ApiModelProperty("chatGroupId")
    private Long chatGroupId;

    @ApiModelProperty("聊天群组名称")
    private String chatGroupName;

    /** 聊天群组成员id-父级表id */
    private Long chatGroupPersonId;

    /** 聊天群组成员用户Id */
    private Long chatGroupPersonUserId;

    private String chatGroupPersonUserName;

    /** 聊天信息 */
    @ApiModelProperty("聊天信息")
    private String chatInfo;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    private Long createUserId;

    /** 更新者id */
    private Long updateUserId;

    @ApiModelProperty("是否是自己")
    private Boolean selfFlag = Boolean.FALSE;

    @ApiModelProperty("用户信息-含头像")
    private UserVo userVo;
}
