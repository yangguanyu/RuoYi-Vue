package com.ruoyi.service.domain.flight.param;

import lombok.Data;

import java.util.List;

@Data
public class CreateOrderByPassengerParam {
    // 商家订单号
    private String outOrderNo;
    // 通过查询返回的产品ID，例如：1-19291223
    private String policyId;
    // 航程类型 OW单程，RT往返，MS多程
    private String routeType;
    // 航程航线数据
    private List<Segment> segments;
    // 乘客类型：1.成人、2.儿童、3.成人+儿童
    private Integer passengerType;
    // 乘客信息
    private List<Passenger> passengers;
    // 价格信息：传入时平台验价后生成订单，不传入价格有平台规则生成订单
    private List<PriceData> priceDatas;
    // 代理人电话
    private String ctct;
    // 代理人姓名
    private String ctctName;
    // 通知url （订单出票后往该地址回调通知）
    private String notifiedUrl;
    // 传入关系证明附件地址，注：未传入订单拒单率提高
    private String infProve;
    // 订单创建人登录账号，可填写51book营业员账号，不填默认主账号
    private String createdBy;
    // 用于用户防止自身客户恶意占座，用户可以传入Boolean为false系统会支付后占座
    private Boolean isBooking;

    @Data
    public class Segment {
        // 航班号
        private String flightNo;
        // 航班时间(格式：yyyy-MM-dd HH:mm)
        private String depDate;
        // 航班抵达时间(格式：yyyy-MM-dd HH:mm)
        private String arrDate;
        // 出发如：SZX（机场三字码）
        private String depCode;
        // 抵达如：SZX (机场三字码)
        private String arrCode;
        // 舱位。大写如：Y
        private String cabin;
        // 携带儿童为必填。儿童舱位。大写如：Y
        private String chdCabin;
        // 婴儿跟随成人舱位
        private String infCabin;
    }

    @Data
    public class PriceData {
        // 乘机人类型，1成人，2儿童，3婴儿（暂不使用）
        private String crewType;
        // 票面价
        private Integer price;
        // 机建
        private Integer airportTax;
        // 燃油费
        private Integer fuelTax;
        // 返点 0.1 = 0.1%
        private Double commissionPoint;
        // 定额
        private Double commissionMoney;
        // 支付手续费
        private Double payFee;
    }

    @Data
    public class Passenger {
        // 姓名
        private String name;
        // 性别 1, 男 2, 女
        private Integer sex;
        // 生日（格式：yyyy-MM-dd） 儿童为必填 产品类型为4、41、42、43、44、45、21、22、23、24、25必传入出生年月 新增证件类型必填
        private String birthday;
        // 证件类型 新增证件类型必填
        // 1-身份证、港澳台居民居住证 (18位) 4-无法识别证件，不支持自动值机(最长18位)
        // 11-护照 12-电子护照 13-军官证、警官证、士兵证等军方证件 14-海员证 15-港澳台来往通行证 16-外交部签发的驻华外交人员证
        // 17-十六周岁以下（户口簿、学生证、出生证明、户口所在地公安出具证明）；民航局规定的其它有效乘机的身份证件 18-外国人永久居留证、出入境证
        private Integer identityType;
        // 国籍二字码，例如：CN 新增证件类型必填
        private String nationality;
        // 发证国家二字码，例如：CN 新增证件类型必填
        private String certificateCountry;
        // 证件有效期，格式：yyyy-MM-dd 新增证件类型必填
        private String certificateUsefulLife;
        // 乘客性别，MALE代表男，FEMALE代表女 新增证件类型必填
        private String gender;
        // 证件号码
        private String identityNo;
        // 乘机人类型，1成人，2儿童，3婴儿（暂不使用）
        private Integer crewType;
        // 电话区号，例如：86
        private String preNum;
        // 乘客手机号（11位）
        private String phoneNum;
        // 用于存储以乘客拆单用户
        private String sonSheNo;
        // 证件有效期 2018-10-12
        private String effectiveDate;
        // 随成人姓名
        private String followAdtName;
        // 随成人证件号
        private String followIdentityNo;
        // 跟随成人舱位，单独儿置必项
        private String followAdtSeat;
        // 跟随成人票号，单独儿置必项
        private String followAdtPnrNo;
        // 跟随成人清码，单独儿置必项
        private String followAdtTicketNo;
    }
}
