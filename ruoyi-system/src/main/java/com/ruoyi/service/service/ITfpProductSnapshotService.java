package com.ruoyi.service.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import com.ruoyi.service.domain.TfpProductSnapshot;
import com.ruoyi.service.vo.MyProductSnapshotListVo;
import com.ruoyi.service.vo.ProductSnapshotDetailVo;

/**
 * 商品快照Service接口
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public interface ITfpProductSnapshotService 
{
    /**
     * 查询商品快照
     * 
     * @param id 商品快照主键
     * @return 商品快照
     */
    public TfpProductSnapshot selectTfpProductSnapshotById(Long id);

    /**
     * 查询商品快照列表
     * 
     * @param tfpProductSnapshot 商品快照
     * @return 商品快照集合
     */
    public List<TfpProductSnapshot> selectTfpProductSnapshotList(TfpProductSnapshot tfpProductSnapshot);

    /**
     * 新增商品快照
     * 
     * @param tfpProductSnapshot 商品快照
     * @return 结果
     */
    public int insertTfpProductSnapshot(TfpProductSnapshot tfpProductSnapshot);

    /**
     * 修改商品快照
     * 
     * @param tfpProductSnapshot 商品快照
     * @return 结果
     */
    public int updateTfpProductSnapshot(TfpProductSnapshot tfpProductSnapshot);

    /**
     * 批量删除商品快照
     * 
     * @param ids 需要删除的商品快照主键集合
     * @return 结果
     */
    public int deleteTfpProductSnapshotByIds(Long[] ids);

    /**
     * 删除商品快照信息
     * 
     * @param id 商品快照主键
     * @return 结果
     */
    public int deleteTfpProductSnapshotById(Long id);

    TfpProductSnapshot selectTfpProductSnapshotByProductIdAndUserId(Long productId, Long createUserId);

    /**
     * 创建商品快照
     * @param productId
     * @param finalAmount
     * @param lowPriceAirTicketFlag
     * @param ruleId
     * @return
     */
    Long createSnapshot(Long productId, BigDecimal finalAmount, Date goDate, String goPlaceName, Integer travelType, Long orderChatGroupId, Integer lowPriceAirTicketFlag, Long ruleId);

    List<MyProductSnapshotListVo> myProductSnapshotList();

    ProductSnapshotDetailVo productSnapshotDetail(Long id);
}
