package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.TfpFlightSeatDetail;
import org.apache.ibatis.annotations.Param;

/**
 * 通程舱位对应信息Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-10
 */
public interface TfpFlightSeatDetailMapper 
{
    /**
     * 查询通程舱位对应信息
     * 
     * @param id 通程舱位对应信息主键
     * @return 通程舱位对应信息
     */
    public TfpFlightSeatDetail selectTfpFlightSeatDetailById(Long id);

    /**
     * 查询通程舱位对应信息列表
     * 
     * @param tfpFlightSeatDetail 通程舱位对应信息
     * @return 通程舱位对应信息集合
     */
    public List<TfpFlightSeatDetail> selectTfpFlightSeatDetailList(TfpFlightSeatDetail tfpFlightSeatDetail);

    /**
     * 新增通程舱位对应信息
     * 
     * @param tfpFlightSeatDetail 通程舱位对应信息
     * @return 结果
     */
    public int insertTfpFlightSeatDetail(TfpFlightSeatDetail tfpFlightSeatDetail);

    /**
     * 修改通程舱位对应信息
     * 
     * @param tfpFlightSeatDetail 通程舱位对应信息
     * @return 结果
     */
    public int updateTfpFlightSeatDetail(TfpFlightSeatDetail tfpFlightSeatDetail);

    /**
     * 删除通程舱位对应信息
     * 
     * @param id 通程舱位对应信息主键
     * @return 结果
     */
    public int deleteTfpFlightSeatDetailById(Long id);

    /**
     * 批量删除通程舱位对应信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpFlightSeatDetailByIds(Long[] ids);

    void deleteTfpFlightSeatDetailByFlightId(@Param("flightId") Long flightId);
}
