CREATE TABLE `tfp_travel_rule` (
       `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
       `type` int(4) DEFAULT NULL COMMENT '类型 0-旅行规则',
       `travel_type` int(1) DEFAULT NULL COMMENT '规则类型 0-组队出行 1-独立出行',
       `version` varchar(40) DEFAULT NULL COMMENT '版本号',
       `text` varchar(2000) DEFAULT NULL COMMENT '文案描述',
       `status` int(1) DEFAULT NULL COMMENT '状态 0-失效 1-生效',
       `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
       `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
       `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
       `create_time` datetime DEFAULT NULL COMMENT '创建时间',
       `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
       `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
       `update_time` datetime DEFAULT NULL COMMENT '更新时间',
       PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='旅行规则表';