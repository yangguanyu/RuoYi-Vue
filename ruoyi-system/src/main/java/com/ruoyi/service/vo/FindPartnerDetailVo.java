package com.ruoyi.service.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "FindPartnerDetailVo", description = "找搭子详情")
public class FindPartnerDetailVo {
    @ApiModelProperty("搭子聊天群组id")
    private Long findPartnerChatGroupId;

    @ApiModelProperty("搭子聊天群组名称")
    private String findPartnerChatGroupName;

    @ApiModelProperty("搭子报名成员")
    private List<UserVo> userList;

    @ApiModelProperty("图片列表")
    private List<AttachmentVO> attachmentList;
}
