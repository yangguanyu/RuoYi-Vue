package com.ruoyi.service.mapper;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import com.ruoyi.service.domain.TfpDrawMoney;
import com.ruoyi.service.dto.DrawMoneyAuditListDto;
import com.ruoyi.service.vo.DrawMoneyAuditListVo;
import com.ruoyi.service.vo.DrawMoneyListVo;
import com.ruoyi.service.vo.UserIncomeDetailListVo;
import org.apache.ibatis.annotations.Param;

/**
 * 提款Mapper接口
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public interface TfpDrawMoneyMapper 
{
    /**
     * 查询提款
     * 
     * @param id 提款主键
     * @return 提款
     */
    public TfpDrawMoney selectTfpDrawMoneyById(Long id);

    /**
     * 查询提款列表
     * 
     * @param tfpDrawMoney 提款
     * @return 提款集合
     */
    public List<TfpDrawMoney> selectTfpDrawMoneyList(TfpDrawMoney tfpDrawMoney);

    /**
     * 新增提款
     * 
     * @param tfpDrawMoney 提款
     * @return 结果
     */
    public int insertTfpDrawMoney(TfpDrawMoney tfpDrawMoney);

    /**
     * 修改提款
     * 
     * @param tfpDrawMoney 提款
     * @return 结果
     */
    public int updateTfpDrawMoney(TfpDrawMoney tfpDrawMoney);

    /**
     * 删除提款
     * 
     * @param id 提款主键
     * @return 结果
     */
    public int deleteTfpDrawMoneyById(Long id);

    /**
     * 批量删除提款
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpDrawMoneyByIds(Long[] ids);

    Integer getDrawMoneyRecordSizeByUserId(@Param("userId") Long userId);

    List<TfpDrawMoney> selectTfpDrawMoneyListByNow(@Param("userId") Long userId, @Param("startDate") Date startDate, @Param("endDate") Date endDate);

    List<DrawMoneyListVo> drawMoneyList(@Param("userId") Long userId);

    List<DrawMoneyAuditListVo> drawMoneyAuditList(DrawMoneyAuditListDto drawMoneyAuditListDto);

    BigDecimal getCumulativeIncomeByUserId(@Param("userId") Long userId);

    List<UserIncomeDetailListVo> getUserIncomeDetailListByUserId(@Param("userId") Long userId);
}
