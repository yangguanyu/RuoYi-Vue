package com.ruoyi.service.vo;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 找搭子对象 tfp_find_partner_sign_up
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@Data
public class TfpFindPartnerSignUpVo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 找搭子id */
    private Long findPartnerId;

    /** 找搭子名称 */
    private String title;

    /** 报名用户id */
    private Long signUpUserId;

    /** 报名用户昵称 */
    private String signUpNickName;

    /** 联系电话 */
    private String phoneNumber;

    /** 性别 0-男生 1-女生 */
    private Integer sex;

    /** 自我介绍 */
    private String selfIntroductio;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    private Long createUserId;

    /** 更新者id */
    private Long updateUserId;
}
