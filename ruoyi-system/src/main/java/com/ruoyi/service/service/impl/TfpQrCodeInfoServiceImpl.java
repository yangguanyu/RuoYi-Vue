package com.ruoyi.service.service.impl;

import java.util.List;
import java.util.Objects;

import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.service.vo.OtherParamVo;
import com.ruoyi.service.vo.QrCodeInfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpQrCodeInfoMapper;
import com.ruoyi.service.domain.TfpQrCodeInfo;
import com.ruoyi.service.service.ITfpQrCodeInfoService;

/**
 * 二维码业务数据Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-19
 */
@Service
public class TfpQrCodeInfoServiceImpl implements ITfpQrCodeInfoService 
{
    @Autowired
    private TfpQrCodeInfoMapper tfpQrCodeInfoMapper;

    /**
     * 查询二维码业务数据
     * 
     * @param id 二维码业务数据主键
     * @return 二维码业务数据
     */
    @Override
    public TfpQrCodeInfo selectTfpQrCodeInfoById(Long id)
    {
        return tfpQrCodeInfoMapper.selectTfpQrCodeInfoById(id);
    }

    /**
     * 查询二维码业务数据列表
     * 
     * @param tfpQrCodeInfo 二维码业务数据
     * @return 二维码业务数据
     */
    @Override
    public List<TfpQrCodeInfo> selectTfpQrCodeInfoList(TfpQrCodeInfo tfpQrCodeInfo)
    {
        return tfpQrCodeInfoMapper.selectTfpQrCodeInfoList(tfpQrCodeInfo);
    }

    /**
     * 新增二维码业务数据
     * 
     * @param tfpQrCodeInfo 二维码业务数据
     * @return 结果
     */
    @Override
    public int insertTfpQrCodeInfo(TfpQrCodeInfo tfpQrCodeInfo)
    {
        tfpQrCodeInfo.setCreateTime(DateUtils.getNowDate());
        return tfpQrCodeInfoMapper.insertTfpQrCodeInfo(tfpQrCodeInfo);
    }

    /**
     * 修改二维码业务数据
     * 
     * @param tfpQrCodeInfo 二维码业务数据
     * @return 结果
     */
    @Override
    public int updateTfpQrCodeInfo(TfpQrCodeInfo tfpQrCodeInfo)
    {
        tfpQrCodeInfo.setUpdateTime(DateUtils.getNowDate());
        return tfpQrCodeInfoMapper.updateTfpQrCodeInfo(tfpQrCodeInfo);
    }

    /**
     * 批量删除二维码业务数据
     * 
     * @param ids 需要删除的二维码业务数据主键
     * @return 结果
     */
    @Override
    public int deleteTfpQrCodeInfoByIds(Long[] ids)
    {
        return tfpQrCodeInfoMapper.deleteTfpQrCodeInfoByIds(ids);
    }

    /**
     * 删除二维码业务数据信息
     * 
     * @param id 二维码业务数据主键
     * @return 结果
     */
    @Override
    public int deleteTfpQrCodeInfoById(Long id)
    {
        return tfpQrCodeInfoMapper.deleteTfpQrCodeInfoById(id);
    }

    /**
     * 小程序-获取二维码信息
     * @param qrId
     * @return
     */
    @Override
    public QrCodeInfoVo getQrCodeInfo(Long qrId) {
        QrCodeInfoVo qrCodeInfoVo = new QrCodeInfoVo();
        TfpQrCodeInfo tfpQrCodeInfo = tfpQrCodeInfoMapper.selectTfpQrCodeInfoById(qrId);
        if (Objects.nonNull(tfpQrCodeInfo)){
            qrCodeInfoVo.setShareUserId(tfpQrCodeInfo.getShareUserId());
            qrCodeInfoVo.setBusinessId(tfpQrCodeInfo.getBusinessId());

            String otherParam = tfpQrCodeInfo.getOtherParam();
            if (Objects.nonNull(otherParam)){
                qrCodeInfoVo.setOtherParam(JSONObject.parseObject(otherParam, OtherParamVo.class));
            }
        }
        return qrCodeInfoVo;
    }
}
