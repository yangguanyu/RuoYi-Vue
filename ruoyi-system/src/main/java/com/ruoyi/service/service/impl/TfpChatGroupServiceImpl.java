package com.ruoyi.service.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import com.ruoyi.common.constant.CommonConstants;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.entity.TfpAttachment;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enuma.*;
import com.ruoyi.common.exception.BusinessException;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.DcDateUtils;
import com.ruoyi.common.utils.DcListUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.service.domain.*;
import com.ruoyi.service.dto.ChatGroupStatisticsDto;
import com.ruoyi.service.dto.MoreGroupListDto;
import com.ruoyi.service.dto.TfpChatGroupDto;
import com.ruoyi.service.service.*;
import com.ruoyi.service.vo.*;
import com.ruoyi.system.service.ISysUserService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.compress.utils.Lists;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpChatGroupMapper;
import org.springframework.transaction.annotation.Transactional;

import static com.ruoyi.common.utils.PageUtils.startPage;

/**
 * 聊天群组Service业务层处理
 *
 * @author ruoyi
 * @date 2023-10-31
 */
@Service
@Transactional
public class TfpChatGroupServiceImpl implements ITfpChatGroupService
{
    @Autowired
    private TfpChatGroupMapper tfpChatGroupMapper;
    @Autowired
    private ITfpProductService tfpProductService;
    @Autowired
    private ITfpChatGroupPersonService personService;
    @Autowired
    private ISysUserService userService;
    @Autowired
    private ITfpAttachmentService attachmentService;
    @Autowired
    private ITfpChatGroupPersonInfoService chatGroupPersonInfoService;

    @Value("${default.limit.person.number}")
    private Integer defaultLimitPersonNumber;

    /**
     * 查询聊天群组
     *
     * @param id 聊天群组主键
     * @return 聊天群组
     */
    @Override
    public TfpChatGroup selectTfpChatGroupById(Long id)
    {
        return tfpChatGroupMapper.selectTfpChatGroupById(id);
    }

    /**
     * 查询聊天群组列表
     *
     * @param tfpChatGroup 聊天群组
     * @return 聊天群组
     */
    @Override
    public List<TfpChatGroup> selectTfpChatGroupList(TfpChatGroup tfpChatGroup)
    {
        return tfpChatGroupMapper.selectTfpChatGroupList(tfpChatGroup);
    }

    @Override
    public List<TfpChatGroupVo> selectTfpChatGroupVoList(TfpChatGroupDto tfpChatGroupDto) {
        String productName = tfpChatGroupDto.getProductName();
        if (productName != null && !productName.trim().equals("")){
            tfpChatGroupDto.setProductNameFlag(true);

            TfpProduct tfpProduct = new TfpProduct();
            tfpProduct.setProductName(productName);
            List<TfpProduct> tfpProductList = tfpProductService.selectTfpProductList(tfpProduct);
            if (tfpProductList == null || tfpProductList.size() == 0){
                return new ArrayList<>();
            }

            List<Long> productIdList = tfpProductList.stream().map(TfpProduct::getId).collect(Collectors.toList());
            tfpChatGroupDto.setProductIdList(productIdList);
        }

        List<TfpChatGroupVo> tfpChatGroupVoList = tfpChatGroupMapper.selectTfpChatGroupVoList(tfpChatGroupDto);
        if (tfpChatGroupVoList != null && tfpChatGroupVoList.size() > 0){
            List<Long> productIdList = tfpChatGroupVoList.stream().map(TfpChatGroupVo::getProductId).collect(Collectors.toList());

            Map<Long, String> productNameMap = new HashMap<>();
            List<TfpProduct> tfpProductList = tfpProductService.selectTfpProductListByIdList(productIdList);
            if (tfpProductList != null && tfpProductList.size() > 0){
                productNameMap = tfpProductList.stream().collect(Collectors.toMap(TfpProduct::getId, TfpProduct::getProductName));
            }

            for (TfpChatGroupVo tfpChatGroupVo : tfpChatGroupVoList) {
                tfpChatGroupVo.setProductName(productNameMap.get(tfpChatGroupVo.getProductId()));
            }
        }

        return tfpChatGroupVoList;
    }

    /*/**
     * @Author yangguanyu
     * @Description :可以通过查询获取群组列表，并且带人数
     * @Date 22:08 2023/12/28
     * @Param [com.ruoyi.service.dto.TfpChatGroupDto]
     * @return java.util.List<com.ruoyi.service.vo.TfpChatGroupVo>
     **/
    public List<TfpChatGroupVo> selectGroupList(TfpChatGroupDto tfpChatGroupDto){
        Date goDateParam = tfpChatGroupDto.getGoDate();
        tfpChatGroupDto.setGoDateStart(DcDateUtils.handelDateToDayStart(goDateParam));
        tfpChatGroupDto.setGoDateEnd(DcDateUtils.handelDateToDayEnd(goDateParam));

        List<TfpChatGroupVo> tfpChatGroupVoList = tfpChatGroupMapper.selectChatGroupsByProductId(tfpChatGroupDto);
        if(CollectionUtils.isEmpty(tfpChatGroupVoList)){
            return Lists.newArrayList();
        }
        //找出tfpChatGroupVoList比tfpChatGroupDto里goDate字段日期大的群组
        List<TfpChatGroupVo> bigTanGoDateList = tfpChatGroupVoList.stream().filter(tfpChatGroupVo -> {
            Date goDate = tfpChatGroupVo.getGoDate();
            if (Objects.isNull(goDate)) {
                return false;
            }
            LocalDate goLocalDate = DateUtils.toLocalDate(goDate);
            LocalDate paramDateLocal = DateUtils.toLocalDate(tfpChatGroupDto.getGoDate());
            return (goLocalDate.isAfter(paramDateLocal)||goLocalDate.equals(paramDateLocal));
        }).collect(Collectors.toList());

        //如果bigTanGoDateList的元素大于等于5个，则取5个；如果小于5个，则从tfpChatGroupVoList中取离tfpChatGroupDto里goDate日期最近的元素放入（往前找）bigTanGoDateList中，但是最多放5个
        if(bigTanGoDateList.size()>=CommonConstants.FIVE){
            return bigTanGoDateList.subList(CommonConstants.ZERO,CommonConstants.FIVE);
        }else{
            //如果bigTanGoDateList的元素小于5个，则从tfpChatGroupVoList中取离tfpChatGroupDto里goDate日期最近的元素放入（往前找）bigTanGoDateList中，但是最多放5个
            LocalDate goDateLocal = DateUtils.toLocalDate(tfpChatGroupDto.getGoDate());
            //找出tfpChatGroupVoList中比 goLocalDate 日期小的元素
            List<TfpChatGroupVo> smallTanGoDateList = tfpChatGroupVoList.stream().filter(tfpChatGroupVo -> {
                Date goDate = tfpChatGroupVo.getGoDate();
                if (Objects.isNull(goDate)) {
                    return false;
                }
                LocalDate goLocalDate = DateUtils.toLocalDate(goDate);
                //找出tfpChatGroupVoList中比 goLocalDate 日期小的元素
                return goLocalDate.isBefore(goDateLocal);
            }).collect(Collectors.toList());
            //将smallTanGoDateList按照goDate倒序排序，用lambda
            List<TfpChatGroupVo> sortedSmallTanGoDateList = smallTanGoDateList.stream().sorted(Comparator.comparing(TfpChatGroupVo::getGoDate).reversed()).collect(Collectors.toList());
            //如果bigTanGoDateList 里边有n个元素，那么从smallTanGoDateList中截取5-n个元素addAll到bigTanGoDateList中
            if(bigTanGoDateList.size()>0){
                int needNum = CommonConstants.FIVE-bigTanGoDateList.size();
                if(needNum>0){
                    //if判断，如果needNum大于sortedSmallTanGoDateList的size,那么needNum = sortedSmallTanGoDateList.size
                    if(needNum>sortedSmallTanGoDateList.size()){
                        needNum = sortedSmallTanGoDateList.size();
                    }
                    bigTanGoDateList.addAll(sortedSmallTanGoDateList.subList(CommonConstants.ZERO,needNum));
                }
            }else{
                bigTanGoDateList.addAll(sortedSmallTanGoDateList.subList( CommonConstants.ZERO,sortedSmallTanGoDateList.size() ));
            }

        }

        //查询现在有多少人
        List<Long> groupIds = bigTanGoDateList.stream().map(TfpChatGroupVo::getId).collect(Collectors.toList());
//        Map<Long, Integer> groupPersonCountMap = personService.selectCountPersonByGroupIdList(groupIds);
        if(CollectionUtils.isEmpty(bigTanGoDateList)){
            for (TfpChatGroupVo tfpChatGroupVo : bigTanGoDateList) {
                tfpChatGroupVo.setPeopleCount(CommonConstants.ZERO);
                tfpChatGroupVo.setUserList(Lists.newArrayList());
            }
            return bigTanGoDateList;
        }
        //查询已报名的人
        List<TfpChatGroupPerson> tfpChatGroupPeople = personService.selectListByGroupIdAndSignUp(groupIds);

        if(CollectionUtils.isEmpty(tfpChatGroupPeople)){
            for (TfpChatGroupVo tfpChatGroupVo : bigTanGoDateList) {
                tfpChatGroupVo.setPeopleCount(CommonConstants.ZERO);
                tfpChatGroupVo.setUserList(Lists.newArrayList());
            }
            return bigTanGoDateList;
        }
        //获取已报名的人的头像
        List<Long> userIds = tfpChatGroupPeople.stream().map(TfpChatGroupPerson::getUserId).collect(Collectors.toList());
        Map<Long, SysUser> userInfoAndAvatar = userService.getUserInfoAndAvatar(userIds);

        for (TfpChatGroupVo tfpChatGroupVo : bigTanGoDateList) {
            List<TfpChatGroupPerson> groupPersonList = tfpChatGroupPeople.stream()
                    .filter(e -> Objects.nonNull(e.getChatGroupId()) && e.getChatGroupId().equals(tfpChatGroupVo.getId())).collect(Collectors.toList());
            if(CollectionUtils.isEmpty(groupPersonList)){
                tfpChatGroupVo.setPeopleCount(CommonConstants.ZERO);
                tfpChatGroupVo.setUserList(Lists.newArrayList());
            }else {
                List<UserVo> userVoList = Lists.newArrayList();
                for (TfpChatGroupPerson tfpChatGroupPerson : groupPersonList) {
                    Long userId = tfpChatGroupPerson.getUserId();
                    SysUser sysUser = userInfoAndAvatar.get(userId);
                    UserVo userVo = new UserVo();
                    userVo.setName(sysUser.getNickName());
                    userVo.setAvatar(sysUser.getAvatar());
                    userVo.setId(sysUser.getUserId());
                    userVoList.add(userVo);
                }
                tfpChatGroupVo.setUserList(userVoList);
                tfpChatGroupVo.setPeopleCount(userVoList.size());
            }

        }
//        for (TfpChatGroupVo tfpChatGroupVo : backList) {
//            Integer num = groupPersonCountMap.get(tfpChatGroupVo.getId());
//            tfpChatGroupVo.setPeopleCount(num);
//        }

        return bigTanGoDateList;
    }

    public static void main(String[] args) {
        ArrayList<String> objects = Lists.newArrayList();
        objects.add("a");
        objects.add("b");
        objects.add("c");
        objects.add("d");
        objects.add("e");
        List<String> strings = objects.subList(0, 0);
        System.out.println(strings);
        System.out.println(objects.size());
    }
    /**
     * 新增聊天群组
     *
     * @param tfpChatGroup 聊天群组
     * @return 结果
     */
    @Override
    public int insertTfpChatGroup(TfpChatGroup tfpChatGroup)
    {
        tfpChatGroup.setCreateTime(DateUtils.getNowDate());
        return tfpChatGroupMapper.insertTfpChatGroup(tfpChatGroup);
    }

    /**
     * 修改聊天群组
     *
     * @param tfpChatGroup 聊天群组
     * @return 结果
     */
    @Override
    public int updateTfpChatGroup(TfpChatGroup tfpChatGroup)
    {
        tfpChatGroup.setUpdateTime(DateUtils.getNowDate());
        return tfpChatGroupMapper.updateTfpChatGroup(tfpChatGroup);
    }

    /**
     * 批量删除聊天群组
     *
     * @param ids 需要删除的聊天群组主键
     * @return 结果
     */
    @Override
    public int deleteTfpChatGroupByIds(Long[] ids)
    {
        return tfpChatGroupMapper.deleteTfpChatGroupByIds(ids);
    }

    /**
     * 删除聊天群组信息
     *
     * @param id 聊天群组主键
     * @return 结果
     */
    @Override
    public int deleteTfpChatGroupById(Long id)
    {
        return tfpChatGroupMapper.deleteTfpChatGroupById(id);
    }

    @Override
    public List<TfpChatGroup> selectTfpChatGroupListByIdList(List<Long> idList) {
        return tfpChatGroupMapper.selectTfpChatGroupListByIdList(idList);
    }

    @Override
    public List<ChatGroupStatisticsDto> selectGroupPersonList(List<Long> productIdList) {
        return tfpChatGroupMapper.selectGroupPersonList(productIdList);
    }

    /**
     * 通过商品id获取聊天组信息
     * @param productId
     * @return
     */
    @Override
    public List<ChatGroupInfoVo> getChatGroupInfoVoListByProductId(Long productId) {
        TfpChatGroup chatGroupParam = new TfpChatGroup();
        chatGroupParam.setProductId(productId);
        chatGroupParam.setDelFlag(DeleteEnum.EXIST.getValue());
        List<TfpChatGroup> tfpChatGroupList = tfpChatGroupMapper.selectTfpChatGroupList(chatGroupParam);
        List<ChatGroupInfoVo> chatGroupInfoVoList = this.handleTfpChatGroupVo(tfpChatGroupList);
        return chatGroupInfoVoList;
    }

    @Override
    public List<OrderChatGroupInfoVo> getChatGroupInfoVoListByOrderList(List<MyOrderListVo> myOrderListVoList){
        List<OrderChatGroupInfoVo> orderChatGroupInfoVoList = new ArrayList<>();

        if (DcListUtils.isNotEmpty(myOrderListVoList)){
            List<Long> idList = new ArrayList<>();
            Map<Long, List<Long>> groupOrderListMap = new HashMap<>();
            for (MyOrderListVo myOrderListVo : myOrderListVoList) {
                Long chatGroupId = myOrderListVo.getChatGroupId();
                if (Objects.nonNull(chatGroupId)){
                    if (!idList.contains(chatGroupId)){
                        idList.add(chatGroupId);
                    }

                    List<Long> orderIdList = groupOrderListMap.get(chatGroupId);
                    if (orderIdList == null){
                        orderIdList = new ArrayList<>();
                    }

                    Long orderId = myOrderListVo.getId();
                    if (!orderIdList.contains(orderId)){
                        orderIdList.add(orderId);
                    }

                    groupOrderListMap.put(chatGroupId, orderIdList);
                }
            }

            List<TfpChatGroup> tfpChatGroupList = tfpChatGroupMapper.selectTfpChatGroupListByIdList(idList);
            List<ChatGroupInfoVo> chatGroupInfoVoList = this.handleTfpChatGroupVo(tfpChatGroupList);
            if (DcListUtils.isNotEmpty(chatGroupInfoVoList)){
                for (ChatGroupInfoVo chatGroupInfoVo : chatGroupInfoVoList) {
                    List<Long> orderIdList = groupOrderListMap.get(chatGroupInfoVo.getId());
                    for (Long orderId : orderIdList) {
                        OrderChatGroupInfoVo orderChatGroupInfoVo = new OrderChatGroupInfoVo();
                        BeanUtils.copyProperties(chatGroupInfoVo, orderChatGroupInfoVo);
                        orderChatGroupInfoVo.setOrderId(orderId);
                        orderChatGroupInfoVoList.add(orderChatGroupInfoVo);
                    }
                }
            }
        }

        return orderChatGroupInfoVoList;
    }

    /**
     * 小程序-我加入的群组集合
     * @return
     */
    @Override
    public List<JoinChatGroupVo> visitorList(Long userId) {
        List<JoinChatGroupVo> joinChatGroupVoList = new ArrayList<>();

        if (Objects.isNull(userId)) {
            throw new RuntimeException("用户id不能为空");
        }

        TfpChatGroupPerson param = new TfpChatGroupPerson();
        param.setDelFlag(DeleteEnum.EXIST.getValue());
        param.setUserId(userId);
        List<TfpChatGroupPerson> chatGroupPersonList = personService.selectTfpChatGroupPersonList(param);

        if (DcListUtils.isNotEmpty(chatGroupPersonList)){
            List<Long> chatGroupIdList = chatGroupPersonList.stream().map(TfpChatGroupPerson::getChatGroupId).collect(Collectors.toList());

            startPage();
            joinChatGroupVoList = tfpChatGroupMapper.visitorList(chatGroupIdList);
            if (DcListUtils.isNotEmpty(joinChatGroupVoList)){
                List<Long> productIdList = joinChatGroupVoList.stream().map(JoinChatGroupVo::getProductId).distinct().collect(Collectors.toList());
                List<TfpProduct> tfpProductList = tfpProductService.selectTfpProductListByIdList(productIdList);
                Map<Long, TfpProduct> productMap = tfpProductList.stream().collect(Collectors.toMap(TfpProduct::getId, TfpProduct->TfpProduct));

                List<TfpChatGroupPerson> tfpChatGroupPersonList = personService.selectTfpChatGroupPersonListByGroupIdList(chatGroupIdList);
                List<Long> userIdList = tfpChatGroupPersonList.stream().map(TfpChatGroupPerson::getUserId).distinct().collect(Collectors.toList());

                // 头像
                List<TfpAttachment> attachmentList = attachmentService.selectList(userIdList, BusinessTypeEnum.USER.getValue(), BusinessSubTypeEnum.USER_HEAD_PORTRAIT.getValue());
                Map<Long, String> headImageMap = attachmentList.stream().collect(Collectors.toMap(TfpAttachment::getBusinessId, TfpAttachment::getUrl));

                Map<Long, List<String>> signUpPersonHeadPortraitUrlMap = new HashMap<>();

                for (TfpChatGroupPerson tfpChatGroupPerson : tfpChatGroupPersonList) {
                    Integer userType = tfpChatGroupPerson.getUserType();
                    if (Objects.equals(userType, ChatUserTypeEnum.BOOT_MAN.getValue())
                            || Objects.equals(userType, ChatUserTypeEnum.APPLIED.getValue())){
                        Long chatGroupId = tfpChatGroupPerson.getChatGroupId();

                        List<String> signUpPersonHeadPortraitUrlList = signUpPersonHeadPortraitUrlMap.get(chatGroupId);
                        if (signUpPersonHeadPortraitUrlList == null){
                            signUpPersonHeadPortraitUrlList = new ArrayList<>();
                        }

                        signUpPersonHeadPortraitUrlList.add(headImageMap.get(tfpChatGroupPerson.getUserId()));
                        signUpPersonHeadPortraitUrlMap.put(chatGroupId, signUpPersonHeadPortraitUrlList);
                    }
                }

                for (JoinChatGroupVo joinChatGroupVo : joinChatGroupVoList) {
                    Long id = joinChatGroupVo.getId();

                    joinChatGroupVo.setId(id);
                    joinChatGroupVo.setImage(headImageMap.get(userId));
                    joinChatGroupVo.setSignUpPersonHeadPortraitUrlList(signUpPersonHeadPortraitUrlMap.get(id));

                    TfpProduct tfpProduct = productMap.get(joinChatGroupVo.getProductId());
                    if (Objects.nonNull(tfpProduct)){
                        joinChatGroupVo.setProductName(tfpProduct.getProductName());
                        joinChatGroupVo.setCategoryId(tfpProduct.getCategoryId());
                    }

                    Integer signUpPersonNumber = joinChatGroupVo.getSignUpPersonNumber();
                    if (Objects.isNull(signUpPersonNumber)){
                        joinChatGroupVo.setSignUpPersonNumber(0);
                    }
                }
            }
        }

        return joinChatGroupVoList;
    }

    @Override
    public List<TfpChatGroup> selectTfpChatGroupListByProductIdList(List<Long> productIdList) {
        return tfpChatGroupMapper.selectTfpChatGroupListByProductIdList(productIdList);
    }

    @Override
    public TfpChatGroup getLastGroupByProductIdAndGoDate(Long productId, Date goDate) {
        Date newGoDate = DcDateUtils.handelDateToDayStart(goDate);
        return tfpChatGroupMapper.getLastGroupByProductIdAndGoDate(productId, newGoDate);
    }

    /**
     * 支付成功后群组处理
     * @param tfpOrder
     */
    @Override
    public Long handlePaySuccessChatGroupInfo(TfpOrder tfpOrder, Long categoryId){
        Long chatGroupId = tfpOrder.getChatGroupId();
        if (Objects.isNull(chatGroupId)){
            chatGroupId = this.handleLastGroupInfo(tfpOrder);
        } else {
            if (Objects.equals(ProductCategoryEnum.WHOLE_COUNTRY.getValue(), categoryId)
            || Objects.equals(ProductCategoryEnum.WHOLE_COUNTRY_SMALL.getValue(), categoryId)){
                TfpChatGroupPerson tfpChatGroupPerson = personService.selectTfpChatGroupPersonByGroupIdAndUserId(chatGroupId, tfpOrder.getCreateUserId());
                if (Objects.nonNull(tfpChatGroupPerson)){
                    tfpChatGroupPerson.setUserType(ChatUserTypeEnum.APPLIED.getValue());
                    this.updatePaySuccessNewChatPersonInfo(tfpChatGroupPerson, tfpOrder);
                } else {
                    // 初始化群组人员信息
                    tfpChatGroupPerson = new TfpChatGroupPerson();
                    tfpChatGroupPerson.setChatGroupId(chatGroupId);
                    tfpChatGroupPerson.setProductId(tfpOrder.getProductId());
                    tfpChatGroupPerson.setUserId(tfpOrder.getCreateUserId());
                    tfpChatGroupPerson.setUserType(ChatUserTypeEnum.APPLIED.getValue());
                    tfpChatGroupPerson.setDelFlag(DeleteEnum.EXIST.getValue());
                    tfpChatGroupPerson.setCreateUserId(tfpOrder.getCreateUserId());
                    tfpChatGroupPerson.setCreateBy(tfpOrder.getCreateBy());
                    tfpChatGroupPerson.setCreateTime(DateUtils.getNowDate());
                    personService.insertTfpChatGroupPerson(tfpChatGroupPerson);
                }

                TfpChatGroup tfpChatGroup = tfpChatGroupMapper.selectTfpChatGroupById(chatGroupId);
                Integer signUpPersonNumber = tfpChatGroup.getSignUpPersonNumber();
                tfpChatGroup.setSignUpPersonNumber(++signUpPersonNumber);
                tfpChatGroup.setUpdateUserId(tfpOrder.getCreateUserId());
                tfpChatGroup.setUpdateBy(tfpOrder.getCreateBy());
                tfpChatGroup.setUpdateTime(DateUtils.getNowDate());
                tfpChatGroupMapper.updateTfpChatGroup(tfpChatGroup);

                String chatInfo = "我已报名，现已加入结伴之旅~";
                Long chatGroupPersonId = tfpChatGroupPerson.getId();
                this.saveFirstChatInfo(chatGroupId, chatGroupPersonId, chatInfo, tfpOrder);
            } else {
                Integer travelType = tfpOrder.getTravelType();
                if (Objects.equals(TravelTypeEnum.TEAM_TRAVEL.getValue(), travelType)) {
                    TfpChatGroup tfpChatGroup = tfpChatGroupMapper.selectTfpChatGroupById(chatGroupId);
                    Integer signUpPersonNumber = tfpChatGroup.getSignUpPersonNumber();
                    Integer limitPersonNumber = tfpChatGroup.getLimitPersonNumber();
                    if (!Objects.equals(signUpPersonNumber, limitPersonNumber)) {
                        // 报名人数和限制人数不相等，可以直接处理
                        TfpChatGroupPerson tfpChatGroupPerson = personService.selectTfpChatGroupPersonByGroupIdAndUserId(chatGroupId, tfpOrder.getCreateUserId());
                        tfpChatGroupPerson.setUserType(ChatUserTypeEnum.APPLIED.getValue());
                        this.updatePaySuccessNewChatPersonInfo(tfpChatGroupPerson, tfpOrder);

                        tfpChatGroup.setSignUpPersonNumber(++signUpPersonNumber);
                        tfpChatGroup.setUpdateUserId(tfpOrder.getCreateUserId());
                        tfpChatGroup.setUpdateBy(tfpOrder.getCreateBy());
                        tfpChatGroup.setUpdateTime(DateUtils.getNowDate());
                        tfpChatGroupMapper.updateTfpChatGroup(tfpChatGroup);

                        String chatInfo = "我已报名，现已加入结伴之旅~";
                        Long chatGroupPersonId = tfpChatGroupPerson.getId();
                        this.saveFirstChatInfo(chatGroupId, chatGroupPersonId, chatInfo, tfpOrder);
                    } else {
                        // 报名人数与限制人数相等，处理最新的群组
                        chatGroupId = this.handleLastGroupInfo(tfpOrder);
                    }
                }
            }
        }

        return chatGroupId;
    }

    @Override
    public MoreListVo moreList(TfpChatGroupDto tfpChatGroupDto) {
        MoreListVo moreListVo = new MoreListVo();
        Long productId = tfpChatGroupDto.getProductId();

        DateFormat dateFormat = new SimpleDateFormat("yyyy年MM月");

        TfpProduct tfpProduct = tfpProductService.selectTfpProductById(productId);
        moreListVo.setProductName(tfpProduct.getProductName());

        Date nowDate = new Date();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(nowDate);

        List<MoreListGroupVo> moreListGroupVoList = new ArrayList<>();

        int number = 0;

        // 取数据区间：当前查看月 至 近6月内  条件：月份内已有创建群组  展示规则：最多两月（临近），最少1月。
        for (int i = 0; i < 6; i++) {
            if (i != 0){
                calendar.add(Calendar.MONTH, i);
            }

            Date searchDate = calendar.getTime();
            MoreGroupListDto preMoreGroupListDto = new MoreGroupListDto();
            preMoreGroupListDto.setProductId(tfpProduct.getId());
            preMoreGroupListDto.setDate(searchDate);
            List<MoreListMonthGroupVo> monthGroupVoList = this.moreGroupList(preMoreGroupListDto);
            if (DcListUtils.isNotEmpty(monthGroupVoList)){
                number++;

                MoreListGroupVo preMoreListGroupVo = new MoreListGroupVo();
                preMoreListGroupVo.setMonthDay(searchDate);
                preMoreListGroupVo.setMonthDayStr(dateFormat.format(searchDate));
                preMoreListGroupVo.setMonthGroupList(monthGroupVoList);
                moreListGroupVoList.add(preMoreListGroupVo);
            }

            if (number == 2){
                break;
            }
        }

        moreListVo.setMoreGroupList(moreListGroupVoList);
        return moreListVo;
    }

    @Override
    public List<MoreListMonthGroupVo> moreGroupList(MoreGroupListDto moreGroupListDto) {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = loginUser.getUserId();

        Long productId = moreGroupListDto.getProductId();
        Date date = moreGroupListDto.getDate();

        if (Objects.isNull(date)){
            throw new BusinessException("查询日期为空");
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        // 获得本月第一天
        calendar.add(Calendar.MONTH, 0);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        Date startDate = DcDateUtils.handelDateToDayStart(calendar.getTime());

        // 获得本月最后一天
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        Date endDate = DcDateUtils.handelDateToDayEnd(calendar.getTime());

        Date now = DcDateUtils.handelDateToDayStart(new Date());
        // 今天之前的信息不展示
        if (now.after(endDate)){
            return new ArrayList<>();
        }

        if (now.after(startDate)){
            startDate = now;
        }

        startPage();
        List<MoreListMonthGroupVo> monthGroupList = tfpChatGroupMapper.moreGroupList(productId, startDate, endDate);
        if (DcListUtils.isNotEmpty(monthGroupList)){
            List<Long> chatGroupIdList = monthGroupList.stream().map(MoreListMonthGroupVo::getId).collect(Collectors.toList());
            List<TfpChatGroupPerson> tfpChatGroupPersonList = personService.selectTfpChatGroupPersonListByGroupIdList(chatGroupIdList);
            List<Long> userIdList = tfpChatGroupPersonList.stream().map(TfpChatGroupPerson::getUserId).distinct().collect(Collectors.toList());

            // 头像
            List<TfpAttachment> attachmentList = attachmentService.selectList(userIdList, BusinessTypeEnum.USER.getValue(), BusinessSubTypeEnum.USER_HEAD_PORTRAIT.getValue());
            Map<Long, String> headImageMap = attachmentList.stream().collect(Collectors.toMap(TfpAttachment::getBusinessId, TfpAttachment::getUrl));

            Map<Long, List<String>> signUpPersonHeadPortraitUrlMap = new HashMap<>();

            // 发起者map
            Map<Long, Long> initiatorMap = new HashMap<>();

            for (TfpChatGroupPerson tfpChatGroupPerson : tfpChatGroupPersonList) {
                Integer userType = tfpChatGroupPerson.getUserType();
                if (Objects.equals(userType, ChatUserTypeEnum.BOOT_MAN.getValue())
                        || Objects.equals(userType, ChatUserTypeEnum.APPLIED.getValue())){
                    Long chatGroupId = tfpChatGroupPerson.getChatGroupId();

                    List<String> signUpPersonHeadPortraitUrlList = signUpPersonHeadPortraitUrlMap.get(chatGroupId);
                    if (signUpPersonHeadPortraitUrlList == null){
                        signUpPersonHeadPortraitUrlList = new ArrayList<>();
                    }

                    signUpPersonHeadPortraitUrlList.add(headImageMap.get(tfpChatGroupPerson.getUserId()));
                    signUpPersonHeadPortraitUrlMap.put(chatGroupId, signUpPersonHeadPortraitUrlList);

                    if (Objects.equals(userType, ChatUserTypeEnum.BOOT_MAN.getValue())){
                        initiatorMap.put(tfpChatGroupPerson.getChatGroupId(), tfpChatGroupPerson.getUserId());
                    }
                }
            }

            for (MoreListMonthGroupVo monthGroupVo : monthGroupList) {
                Long id = monthGroupVo.getId();
                Long initiatorUserId = initiatorMap.get(id);

                monthGroupVo.setUserId(initiatorUserId);
                monthGroupVo.setLoginUserFlag(Objects.equals(userId, initiatorUserId));
                monthGroupVo.setImage(headImageMap.get(initiatorUserId));
                monthGroupVo.setSignUpPersonHeadPortraitUrlList(signUpPersonHeadPortraitUrlMap.get(id));
                monthGroupVo.setGoDateOfYear(monthGroupVo.getGoDate());
            }

            monthGroupList = monthGroupList.stream().sorted(Comparator.comparing(MoreListMonthGroupVo::getGoDate)).collect(Collectors.toList());
        }

        return monthGroupList;
    }

    @Override
    public void deleteTfpChatGroupByProductIds(List<Long> productIdList) {
        tfpChatGroupMapper.deleteTfpChatGroupByProductIds(productIdList);
    }

    private List<ChatGroupInfoVo> handleTfpChatGroupVo(List<TfpChatGroup> tfpChatGroupList){
        List<ChatGroupInfoVo> chatGroupInfoVoList = new ArrayList<>();
        if (DcListUtils.isNotEmpty(tfpChatGroupList)) {
            List<Long> idList = new ArrayList<>();
            for (TfpChatGroup tfpChatGroup : tfpChatGroupList) {
                ChatGroupInfoVo chatGroupInfoVo = new ChatGroupInfoVo();
                BeanUtils.copyProperties(tfpChatGroup, chatGroupInfoVo);

                if (Objects.isNull(tfpChatGroup.getSignUpPersonNumber())){
                    chatGroupInfoVo.setSignUpPersonNumber(0);
                }

                String name = tfpChatGroup.getName();

                chatGroupInfoVo.setName(name);
                chatGroupInfoVoList.add(chatGroupInfoVo);

                Long id = tfpChatGroup.getId();
                idList.add(id);
            }

            List<TfpChatGroupPerson> tfpChatGroupPersonList = personService.selectTfpChatGroupPersonListByGroupIdList(idList);
            if (DcListUtils.isNotEmpty(tfpChatGroupPersonList)){
                List<Long> userIdList = new ArrayList<>();
                // key:chatGroupId value:userId
                Map<Long, List<TfpChatGroupPerson>> chatGroupPersonMap = new HashMap<>();
                for (TfpChatGroupPerson tfpChatGroupPerson : tfpChatGroupPersonList) {
                    Long chatGroupId = tfpChatGroupPerson.getChatGroupId();
                    Long userId = tfpChatGroupPerson.getUserId();

                    if (!userIdList.contains(userId)){
                        userIdList.add(userId);
                    }

                    List<TfpChatGroupPerson> groupPersonUserList = chatGroupPersonMap.get(chatGroupId);
                    if (groupPersonUserList == null){
                        groupPersonUserList = new ArrayList<>();
                    }

                    groupPersonUserList.add(tfpChatGroupPerson);
                    chatGroupPersonMap.put(chatGroupId, groupPersonUserList);
                }

                List<TfpAttachment> attchmentList = userService.getHeadPortraitListByUserIdList(userIdList);
                Map<Long, String> headPortraitMap;
                if (DcListUtils.isNotEmpty(attchmentList)){
                    headPortraitMap = attchmentList.stream().collect(Collectors.toMap(TfpAttachment::getBusinessId, TfpAttachment::getUrl));
                } else {
                    headPortraitMap = new HashMap<>();
                }

                Iterator<ChatGroupInfoVo> iterator = chatGroupInfoVoList.iterator();
                while (iterator.hasNext()){
                    ChatGroupInfoVo chatGroupInfoVo = iterator.next();
                    Long id = chatGroupInfoVo.getId();

                    List<TfpChatGroupPerson> groupPersonList = chatGroupPersonMap.get(id);
                    if (DcListUtils.isNotEmpty(groupPersonList)){
                        List<String> chatGroupPersonHeadPortraitUrlList = new ArrayList<>();
                        for (TfpChatGroupPerson chatGroupPerson : groupPersonList) {
                            Integer userType = chatGroupPerson.getUserType();
                            if (Objects.equals(userType, ChatUserTypeEnum.BOOT_MAN.getValue())
                                    || Objects.equals(userType, ChatUserTypeEnum.APPLIED.getValue())){
                                String headPortrait = headPortraitMap.get(chatGroupPerson.getUserId());
                                if (headPortrait != null){
                                    chatGroupPersonHeadPortraitUrlList.add(headPortrait);
                                }
                            }
                        }

                        chatGroupInfoVo.setChatGroupPersonHeadPortraitUrlList(chatGroupPersonHeadPortraitUrlList);
                    } else {
                        iterator.remove();
                    }
                }
            } else {
                chatGroupInfoVoList = new ArrayList<>();
            }
        }
        return chatGroupInfoVoList;
    }

    private Long handleLastGroupInfo(TfpOrder tfpOrder){
        if(Objects.isNull(tfpOrder.getGoDate())){
            throw new ServiceException("未获取到出发时间");
        }
        Long chatGroupId;
        TfpChatGroup chatGroup = tfpChatGroupMapper.getLastGroupByProductIdAndGoDate(tfpOrder.getProductId(), tfpOrder.getGoDate());
        if (Objects.nonNull(chatGroup) && !Objects.equals(chatGroup.getSignUpPersonNumber(), chatGroup.getLimitPersonNumber())){
            TfpChatGroupPerson groupPerson = personService.selectTfpChatGroupPersonByGroupIdAndUserId(chatGroup.getId(), tfpOrder.getCreateUserId());

            Long chatGroupPersonId;
            if (Objects.nonNull(groupPerson)){
                groupPerson.setUserType(ChatUserTypeEnum.APPLIED.getValue());
                this.updatePaySuccessNewChatPersonInfo(groupPerson, tfpOrder);

                chatGroupPersonId = groupPerson.getId();
            } else {
                chatGroupPersonId = this.savePaySuccessNewChatPersonInfo(chatGroup.getId(), ChatUserTypeEnum.APPLIED.getValue(), tfpOrder);
            }

            chatGroup.setSignUpPersonNumber(chatGroup.getSignUpPersonNumber() + 1);
            chatGroup.setUpdateUserId(tfpOrder.getCreateUserId());
            chatGroup.setUpdateBy(tfpOrder.getCreateBy());
            chatGroup.setUpdateTime(DateUtils.getNowDate());
            tfpChatGroupMapper.updateTfpChatGroup(chatGroup);

            chatGroupId = chatGroup.getId();

            String chatInfo = "我已报名，现已加入结伴之旅~";
            this.saveFirstChatInfo(chatGroupId, chatGroupPersonId, chatInfo, tfpOrder);
        } else {
            Long createUserId = tfpOrder.getCreateUserId();
            SysUser sysUser = userService.selectUserById(createUserId);

            LocalDate goLocalDate = DateUtils.toLocalDate(tfpOrder.getGoDate());
            TfpChatGroup newTfpChatGroup = new TfpChatGroup();
            newTfpChatGroup.setName(goLocalDate.getMonthValue()+CommonConstants.STRIGULA+goLocalDate.getDayOfMonth()+CommonConstants.SPACE+sysUser.getNickName() + "的群组");
            newTfpChatGroup.setProductId(tfpOrder.getProductId());
            newTfpChatGroup.setLimitPersonNumber(defaultLimitPersonNumber);
            newTfpChatGroup.setSignUpPersonNumber(1);
            newTfpChatGroup.setGoDate(DcDateUtils.handelDateToDayStart(tfpOrder.getGoDate()));
            newTfpChatGroup.setDelFlag(DeleteEnum.EXIST.getValue());
            newTfpChatGroup.setCreateUserId(tfpOrder.getCreateUserId());
            newTfpChatGroup.setCreateBy(tfpOrder.getCreateBy());
            newTfpChatGroup.setCreateTime(DateUtils.getNowDate());
            tfpChatGroupMapper.insertTfpChatGroup(newTfpChatGroup);

            Long chatGroupPersonId = this.savePaySuccessNewChatPersonInfo(newTfpChatGroup.getId(), ChatUserTypeEnum.BOOT_MAN.getValue(), tfpOrder);

            chatGroupId = newTfpChatGroup.getId();

            // 追加新的信息
            String chatInfo = "我是组队出行发起人" + sysUser.getNickName() + "，我已报名，大家快来加入吧，期待在旅途中与你相识哦～";
            this.saveFirstChatInfo(chatGroupId, chatGroupPersonId, chatInfo, tfpOrder);
        }

        return chatGroupId;
    }

    /**
     * 新增支付成功后群组成员信息
     * @param chatGroupId
     * @param tfpOrder
     */
    private Long savePaySuccessNewChatPersonInfo(Long chatGroupId, Integer userType, TfpOrder tfpOrder){
        TfpChatGroupPerson groupPerson = new TfpChatGroupPerson();
        groupPerson.setChatGroupId(chatGroupId);
        groupPerson.setProductId(tfpOrder.getProductId());
        groupPerson.setUserId(tfpOrder.getCreateUserId());
        groupPerson.setUserType(userType);
        groupPerson.setDelFlag(DeleteEnum.EXIST.getValue());
        groupPerson.setCreateUserId(tfpOrder.getCreateUserId());
        groupPerson.setCreateBy(tfpOrder.getCreateBy());
        groupPerson.setCreateTime(DateUtils.getNowDate());
        personService.insertTfpChatGroupPerson(groupPerson);

        return groupPerson.getId();
    }

    private void updatePaySuccessNewChatPersonInfo(TfpChatGroupPerson groupPerson, TfpOrder tfpOrder){
        groupPerson.setUpdateUserId(tfpOrder.getCreateUserId());
        groupPerson.setUpdateBy(tfpOrder.getCreateBy());
        groupPerson.setUpdateTime(DateUtils.getNowDate());
        personService.updateTfpChatGroupPerson(groupPerson);
    }

    /**
     * 保存发起者或报名者第一次发送的默认信息
     * @param chatGroupId
     * @param chatGroupPersonId
     * @param chatInfo
     * @param tfpOrder
     */
    private void saveFirstChatInfo(Long chatGroupId, Long chatGroupPersonId, String chatInfo, TfpOrder tfpOrder){
        TfpChatGroupPersonInfo chatGroupPersonInfo = new TfpChatGroupPersonInfo();
        chatGroupPersonInfo.setChatGroupId(chatGroupId);
        chatGroupPersonInfo.setChatGroupPersonId(chatGroupPersonId);
        chatGroupPersonInfo.setChatInfo(chatInfo);
        chatGroupPersonInfo.setDelFlag(DeleteEnum.EXIST.getValue());
        chatGroupPersonInfo.setCreateUserId(tfpOrder.getCreateUserId());
        chatGroupPersonInfo.setCreateBy(tfpOrder.getCreateBy());
        chatGroupPersonInfo.setCreateTime(DateUtils.getNowDate());

        chatGroupPersonInfoService.insertTfpChatGroupPersonInfo(chatGroupPersonInfo);
    }
}
