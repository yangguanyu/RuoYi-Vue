package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.domain.entity.SysUserWx;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.mapper.SysUserWxMapper;
import com.ruoyi.system.service.ISysUserWxService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


/**
 * 用户微信Service业务层处理
 *
 * @author ruoyi
 * @date 2023-10-09
 */
@Service
@Slf4j
@Transactional
public class SysUserWxServiceImpl implements ISysUserWxService {
    @Autowired
    private SysUserWxMapper sysUserWxMapper;

    /**
     * 查询用户微信
     *
     * @param id 用户微信主键
     * @return 用户微信
     */
    @Override
    public SysUserWx selectSysUserWxById(Long id)
    {
        return sysUserWxMapper.selectSysUserWxById(id);
    }

    /**
     * 查询用户微信列表
     *
     * @param sysUserWx 用户微信
     * @return 用户微信
     */
    @Override
    public List<SysUserWx> selectSysUserWxList(SysUserWx sysUserWx)
    {
        return sysUserWxMapper.selectSysUserWxList(sysUserWx);
    }

    /**
     * 新增用户微信
     *
     * @param sysUserWx 用户微信
     * @return 结果
     */
    @Override
    public int insertSysUserWx(SysUserWx sysUserWx)
    {
        sysUserWx.setCreateTime(DateUtils.getNowDate());
        return sysUserWxMapper.insertSysUserWx(sysUserWx);
    }

    /**
     * 修改用户微信
     *
     * @param sysUserWx 用户微信
     * @return 结果
     */
    @Override
    public int updateSysUserWx(SysUserWx sysUserWx)
    {
        sysUserWx.setUpdateTime(DateUtils.getNowDate());
        return sysUserWxMapper.updateSysUserWx(sysUserWx);
    }

    /**
     * 批量删除用户微信
     *
     * @param ids 需要删除的用户微信主键
     * @return 结果
     */
    @Override
    public int deleteSysUserWxByIds(String ids)
    {
        return sysUserWxMapper.deleteSysUserWxByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除用户微信信息
     *
     * @param id 用户微信主键
     * @return 结果
     */
    @Override
    public int deleteSysUserWxById(Long id)
    {
        return sysUserWxMapper.deleteSysUserWxById(id);
    }

    @Override
    public SysUserWx findSysUserWxByUserId(Long userId){
        return sysUserWxMapper.findSysUserWxByUserId(userId);
    }
}
