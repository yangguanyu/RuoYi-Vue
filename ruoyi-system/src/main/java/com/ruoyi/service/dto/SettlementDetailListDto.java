package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "SettlementDetailListDto")
public class SettlementDetailListDto {
    @ApiModelProperty("结算类型 0-待结算 1-已结算")
    private Integer settlementType;
}
