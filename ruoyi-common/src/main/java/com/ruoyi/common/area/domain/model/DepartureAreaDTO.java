package com.ruoyi.common.area.domain.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "DepartureAreaDTO", description = "出发地")
public class DepartureAreaDTO {
    @ApiModelProperty("中国区域")
    private List<DepartureAreaSonDTO> chinaAreaList;
    @ApiModelProperty("海外区域/中国港澳台")
    private List<DepartureAreaSonDTO> otherAreaOrChinaGATList;
}
