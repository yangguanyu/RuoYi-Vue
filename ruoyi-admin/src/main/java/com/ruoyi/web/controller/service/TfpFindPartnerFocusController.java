package com.ruoyi.web.controller.service;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.enuma.DeleteEnum;
import com.ruoyi.common.utils.bean.CustomizeBeanCopier;
import com.ruoyi.web.controller.service.form.partner.FindPartnerFocusEditForm;
import com.ruoyi.web.controller.service.form.partner.FindPartnerFocusForm;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.TfpFindPartnerFocus;
import com.ruoyi.service.service.ITfpFindPartnerFocusService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 找搭子关注Controller
 *
 * @author yangguanyu
 * @date 2024-01-12
 */
@RestController
@RequestMapping("/service/findPartnerFocus")
public class TfpFindPartnerFocusController extends BaseController
{
    @Autowired
    private ITfpFindPartnerFocusService tfpFindPartnerFocusService;

    /**
     * 查询找搭子关注列表
     */
    @PreAuthorize("@ss.hasPermi('service:findPartnerFocus:list')")
    @GetMapping("/list")
    public TableDataInfo list(TfpFindPartnerFocus tfpFindPartnerFocus)
    {
        startPage();
        List<TfpFindPartnerFocus> list = tfpFindPartnerFocusService.selectTfpFindPartnerFocusList(tfpFindPartnerFocus);
        return getDataTable(list);
    }

    /**
     * 导出找搭子关注列表
     */
    @PreAuthorize("@ss.hasPermi('service:findPartnerFocus:export')")
    @Log(title = "找搭子关注", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TfpFindPartnerFocus tfpFindPartnerFocus)
    {
        List<TfpFindPartnerFocus> list = tfpFindPartnerFocusService.selectTfpFindPartnerFocusList(tfpFindPartnerFocus);
        ExcelUtil<TfpFindPartnerFocus> util = new ExcelUtil<TfpFindPartnerFocus>(TfpFindPartnerFocus.class);
        util.exportExcel(response, list, "找搭子关注数据");
    }

    /**
     * 获取找搭子关注详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:findPartnerFocus:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tfpFindPartnerFocusService.selectTfpFindPartnerFocusById(id));
    }

    /**
     * 新增找搭子关注
     */
    @PreAuthorize("@ss.hasPermi('service:findPartnerFocus:add')")
    @Log(title = "找搭子关注", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TfpFindPartnerFocus tfpFindPartnerFocus)
    {
        return toAjax(tfpFindPartnerFocusService.insertTfpFindPartnerFocus(tfpFindPartnerFocus));
    }

    /**
     * 修改找搭子关注
     */
    @PreAuthorize("@ss.hasPermi('service:findPartnerFocus:edit')")
    @Log(title = "找搭子关注", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TfpFindPartnerFocus tfpFindPartnerFocus)
    {
        return toAjax(tfpFindPartnerFocusService.updateTfpFindPartnerFocus(tfpFindPartnerFocus));
    }

    /**
     * 删除找搭子关注
     */
    @PreAuthorize("@ss.hasPermi('service:findPartnerFocus:remove')")
    @Log(title = "找搭子关注", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tfpFindPartnerFocusService.deleteTfpFindPartnerFocusByIds(ids));
    }

    /**
     * 小程序-新增找搭子关注
     */
    @PostMapping("/applet/add")
    @ApiOperation("小程序-新增找搭子关注")
    public AjaxResult appletAdd(@RequestBody FindPartnerFocusForm form) {

        TfpFindPartnerFocus tfpFindPartnerFocus = CustomizeBeanCopier.copyProperties(form, TfpFindPartnerFocus.class);
        int result = tfpFindPartnerFocusService.insertTfpFindPartnerFocus(tfpFindPartnerFocus);

        return toAjax(result);
    }

    @PostMapping("applet/cancel")
    @ApiOperation("小程序-取消找搭子关注")
    public AjaxResult appletCancel(@RequestBody FindPartnerFocusEditForm form) {

        TfpFindPartnerFocus tfpFindPartnerFocus = CustomizeBeanCopier.copyProperties(form, TfpFindPartnerFocus.class);
        tfpFindPartnerFocus.setDelFlag(DeleteEnum.DELETE.getValue());
        return toAjax(tfpFindPartnerFocusService.updateTfpFindPartnerFocus(tfpFindPartnerFocus));
    }

}
