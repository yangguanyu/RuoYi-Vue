package com.ruoyi.service.service.impl;

import java.util.*;
import java.util.stream.Collectors;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.entity.TfpAttachment;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enuma.ChatUserTypeEnum;
import com.ruoyi.common.enuma.DeleteEnum;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.DcListUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.bean.BeanUtils;
import com.ruoyi.service.domain.*;
import com.ruoyi.service.dto.ChatGroupPersonInfoDto;
import com.ruoyi.service.dto.TfpChatGroupPersonInfoDto;
import com.ruoyi.service.dto.TfpFindPartnerChatGroupPersonInfoDto;
import com.ruoyi.service.service.ITfpFindPartnerChatGroupPersonService;
import com.ruoyi.service.service.ITfpFindPartnerChatGroupService;
import com.ruoyi.service.vo.ChatInfoVo;
import com.ruoyi.service.vo.TfpChatGroupPersonInfoVo;
import com.ruoyi.service.vo.TfpFindPartnerChatGroupPersonInfoVo;
import com.ruoyi.service.vo.UserVo;
import com.ruoyi.system.service.ISysUserService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpFindPartnerChatGroupPersonInfoMapper;
import com.ruoyi.service.service.ITfpFindPartnerChatGroupPersonInfoService;
import org.springframework.transaction.annotation.Transactional;

import static com.ruoyi.common.utils.SecurityUtils.getLoginUser;

/**
 * 找搭子聊天群组成员聊天信息Service业务层处理
 *
 * @author ruoyi
 * @date 2023-10-31
 */
@Service
@Transactional
public class TfpFindPartnerChatGroupPersonInfoServiceImpl implements ITfpFindPartnerChatGroupPersonInfoService
{
    @Autowired
    private TfpFindPartnerChatGroupPersonInfoMapper tfpFindPartnerChatGroupPersonInfoMapper;
    @Autowired
    private ITfpFindPartnerChatGroupService tfpFindPartnerChatGroupService;
    @Autowired
    private ITfpFindPartnerChatGroupPersonService findPartnerChatGroupPersonService;
    @Autowired
    private ISysUserService userService;

    /**
     * 查询找搭子聊天群组成员聊天信息
     *
     * @param id 找搭子聊天群组成员聊天信息主键
     * @return 找搭子聊天群组成员聊天信息
     */
    @Override
    public TfpFindPartnerChatGroupPersonInfo selectTfpFindPartnerChatGroupPersonInfoById(Long id)
    {
        return tfpFindPartnerChatGroupPersonInfoMapper.selectTfpFindPartnerChatGroupPersonInfoById(id);
    }

    /**
     * 查询找搭子聊天群组成员聊天信息列表
     *
     * @param tfpFindPartnerChatGroupPersonInfo 找搭子聊天群组成员聊天信息
     * @return 找搭子聊天群组成员聊天信息
     */
    @Override
    public List<TfpFindPartnerChatGroupPersonInfo> selectTfpFindPartnerChatGroupPersonInfoList(TfpFindPartnerChatGroupPersonInfo tfpFindPartnerChatGroupPersonInfo)
    {
        return tfpFindPartnerChatGroupPersonInfoMapper.selectTfpFindPartnerChatGroupPersonInfoList(tfpFindPartnerChatGroupPersonInfo);
    }

    @Override
    public List<TfpFindPartnerChatGroupPersonInfoVo> selectTfpFindPartnerChatGroupPersonInfoVoList(TfpFindPartnerChatGroupPersonInfoDto tfpFindPartnerChatGroupPersonInfoDto) {
        String chatGroupName = tfpFindPartnerChatGroupPersonInfoDto.getChatGroupName();
        if (chatGroupName != null && !chatGroupName.trim().equals("")){
            tfpFindPartnerChatGroupPersonInfoDto.setChatGroupNameFlag(true);

            TfpFindPartnerChatGroup tfpFindPartnerChatGroup = new TfpFindPartnerChatGroup();
            tfpFindPartnerChatGroup.setName(chatGroupName);
            List<TfpFindPartnerChatGroup> findPartnerChatGroupList = tfpFindPartnerChatGroupService.selectTfpFindPartnerChatGroupList(tfpFindPartnerChatGroup);
            if (findPartnerChatGroupList == null || findPartnerChatGroupList.size() == 0){
                return new ArrayList<>();
            }

            List<Long> groupIdList = findPartnerChatGroupList.stream().map(TfpFindPartnerChatGroup::getId).collect(Collectors.toList());
            tfpFindPartnerChatGroupPersonInfoDto.setChatGroupIdList(groupIdList);
        }

        String chatGroupPersonUserName = tfpFindPartnerChatGroupPersonInfoDto.getChatGroupPersonUserName();
        if (chatGroupPersonUserName != null && !chatGroupPersonUserName.trim().equals("")){
            tfpFindPartnerChatGroupPersonInfoDto.setChatGroupPersonUserNameFlag(true);

            SysUser sysUser = new SysUser();
            sysUser.setUserName(chatGroupPersonUserName);
            List<SysUser> sysUserList = userService.selectUserList(sysUser);
            if (sysUserList == null || sysUserList.size() == 0){
                return new ArrayList<>();
            }

            List<Long> userList = sysUserList.stream().map(SysUser::getUserId).collect(Collectors.toList());
            tfpFindPartnerChatGroupPersonInfoDto.setChatGroupPersonUserIdList(userList);
        }

        List<TfpFindPartnerChatGroupPersonInfoVo> findPartnerChatGroupPersonInfoVoList = tfpFindPartnerChatGroupPersonInfoMapper.selectTfpFindPartnerChatGroupPersonInfoDtoList(tfpFindPartnerChatGroupPersonInfoDto);
        if (findPartnerChatGroupPersonInfoVoList != null && findPartnerChatGroupPersonInfoVoList.size() > 0){
            List<Long> chatGroupIdList = new ArrayList<>();
            List<Long> chatGroupPersonUserIdList = new ArrayList<>();

            for (TfpFindPartnerChatGroupPersonInfoVo tfpFindPartnerChatGroupPersonInfoVo : findPartnerChatGroupPersonInfoVoList) {
                chatGroupIdList.add(tfpFindPartnerChatGroupPersonInfoVo.getChatGroupId());
                chatGroupPersonUserIdList.add(tfpFindPartnerChatGroupPersonInfoVo.getChatGroupPersonUserId());
            }

            Map<Long, String> chatGroupNameMap = new HashMap<>();
            if (chatGroupIdList.size() > 0){
                List<TfpFindPartnerChatGroup> tfpFindPartnerChatGroupList = tfpFindPartnerChatGroupService.selectTfpFindPartnerChatGroupListByIdList(chatGroupIdList);
                if (tfpFindPartnerChatGroupList != null && tfpFindPartnerChatGroupList.size() > 0){
                    chatGroupNameMap = tfpFindPartnerChatGroupList.stream().distinct().collect(Collectors.toMap(TfpFindPartnerChatGroup::getId, TfpFindPartnerChatGroup::getName));
                }
            }

            Map<Long, String> userNameMap = new HashMap<>();
            if (chatGroupPersonUserIdList.size() > 0){
                List<SysUser> sysUserList = userService.selectUserInfoByUserIdList(chatGroupPersonUserIdList);
                if (sysUserList != null && sysUserList.size() > 0){
                    userNameMap = sysUserList.stream().distinct().collect(Collectors.toMap(SysUser::getUserId, SysUser::getUserName));
                }
            }

            for (TfpFindPartnerChatGroupPersonInfoVo tfpFindPartnerChatGroupPersonInfoVo : findPartnerChatGroupPersonInfoVoList) {
                tfpFindPartnerChatGroupPersonInfoVo.setChatGroupName(chatGroupNameMap.get(tfpFindPartnerChatGroupPersonInfoVo.getChatGroupId()));
                tfpFindPartnerChatGroupPersonInfoVo.setChatGroupPersonUserName(userNameMap.get(tfpFindPartnerChatGroupPersonInfoVo.getChatGroupPersonUserId()));
            }
        }

        return findPartnerChatGroupPersonInfoVoList;
    }

    /**
     * 新增找搭子聊天群组成员聊天信息
     *
     * @param tfpFindPartnerChatGroupPersonInfo 找搭子聊天群组成员聊天信息
     * @return 结果
     */
    @Override
    public int insertTfpFindPartnerChatGroupPersonInfo(TfpFindPartnerChatGroupPersonInfo tfpFindPartnerChatGroupPersonInfo)
    {
        tfpFindPartnerChatGroupPersonInfo.setCreateTime(DateUtils.getNowDate());
        return tfpFindPartnerChatGroupPersonInfoMapper.insertTfpFindPartnerChatGroupPersonInfo(tfpFindPartnerChatGroupPersonInfo);
    }

    /**
     * 修改找搭子聊天群组成员聊天信息
     *
     * @param tfpFindPartnerChatGroupPersonInfo 找搭子聊天群组成员聊天信息
     * @return 结果
     */
    @Override
    public int updateTfpFindPartnerChatGroupPersonInfo(TfpFindPartnerChatGroupPersonInfo tfpFindPartnerChatGroupPersonInfo)
    {
        tfpFindPartnerChatGroupPersonInfo.setUpdateTime(DateUtils.getNowDate());
        return tfpFindPartnerChatGroupPersonInfoMapper.updateTfpFindPartnerChatGroupPersonInfo(tfpFindPartnerChatGroupPersonInfo);
    }

    /**
     * 批量删除找搭子聊天群组成员聊天信息
     *
     * @param ids 需要删除的找搭子聊天群组成员聊天信息主键
     * @return 结果
     */
    @Override
    public int deleteTfpFindPartnerChatGroupPersonInfoByIds(Long[] ids)
    {
        return tfpFindPartnerChatGroupPersonInfoMapper.deleteTfpFindPartnerChatGroupPersonInfoByIds(ids);
    }

    /**
     * 删除找搭子聊天群组成员聊天信息信息
     *
     * @param id 找搭子聊天群组成员聊天信息主键
     * @return 结果
     */
    @Override
    public int deleteTfpFindPartnerChatGroupPersonInfoById(Long id)
    {
        return tfpFindPartnerChatGroupPersonInfoMapper.deleteTfpFindPartnerChatGroupPersonInfoById(id);
    }

    @Override
    public List<ChatInfoVo> chatList(Long findPartnerChatGroupId) {
        List<ChatInfoVo> chatInfoVoList = new ArrayList<>();

        List<TfpFindPartnerChatGroupPersonInfo> findPartnerChatGroupPersonInfoList = tfpFindPartnerChatGroupPersonInfoMapper.selectListByFindPartnerChatGroupId(findPartnerChatGroupId);
        if (DcListUtils.isNotEmpty(findPartnerChatGroupPersonInfoList)){
            List<Long> userIdList = findPartnerChatGroupPersonInfoList.stream().map(TfpFindPartnerChatGroupPersonInfo::getChatGroupPersonUserId).collect(Collectors.toList());

            List<SysUser> sysUserList = userService.selectUserInfoByUserIdList(userIdList);
            Map<Long, String> userNameMap = new HashMap<>();
            if (DcListUtils.isNotEmpty(sysUserList)){
                userNameMap = sysUserList.stream().collect(Collectors.toMap(SysUser::getUserId, SysUser::getUserName));
            }

            for (TfpFindPartnerChatGroupPersonInfo tfpFindPartnerChatGroupPersonInfo : findPartnerChatGroupPersonInfoList) {
                ChatInfoVo chatInfoVo = new ChatInfoVo();
                chatInfoVo.setUserName(userNameMap.get(tfpFindPartnerChatGroupPersonInfo.getChatGroupPersonUserId()));
                chatInfoVo.setChatDate(tfpFindPartnerChatGroupPersonInfo.getCreateTime());
                chatInfoVo.setChatInfo(tfpFindPartnerChatGroupPersonInfo.getChatInfo());
                chatInfoVoList.add(chatInfoVo);
            }
        }

        return chatInfoVoList;
    }

    @Override
    public int appletChatInfoAdd(ChatGroupPersonInfoDto chatGroupPersonInfoDto) {
        LoginUser loginUser = getLoginUser();
        Long userId = loginUser.getUserId();
        Long chatGroupId = chatGroupPersonInfoDto.getChatGroupId();

        TfpFindPartnerChatGroupPerson chatGroupPerson = findPartnerChatGroupPersonService.selectByGroupIdAndUserId(chatGroupId, userId);
        if (Objects.isNull(chatGroupPerson)){
            chatGroupPerson = new TfpFindPartnerChatGroupPerson();
            chatGroupPerson.setChatGroupId(chatGroupId);
            chatGroupPerson.setUserId(userId);
            chatGroupPerson.setUserType(ChatUserTypeEnum.UN_APPLIED.getValue());
            chatGroupPerson.setDelFlag(DeleteEnum.EXIST.getValue());
            chatGroupPerson.setCreateUserId(loginUser.getUserId());
            chatGroupPerson.setCreateBy(loginUser.getUsername());
            chatGroupPerson.setCreateTime(DateUtils.getNowDate());
            findPartnerChatGroupPersonService.insertTfpFindPartnerChatGroupPerson(chatGroupPerson);
        }

        TfpFindPartnerChatGroupPersonInfo tfpChatGroupPersonInfo = new TfpFindPartnerChatGroupPersonInfo();
        BeanUtils.copyProperties(chatGroupPersonInfoDto, tfpChatGroupPersonInfo);
        //处理必填项
        tfpChatGroupPersonInfo.setChatGroupPersonUserId(userId);
        tfpChatGroupPersonInfo.setCreateUserId(loginUser.getUserId());
        tfpChatGroupPersonInfo.setUpdateUserId(loginUser.getUserId());
        tfpChatGroupPersonInfo.setCreateTime(DateUtils.getNowDate());
        tfpChatGroupPersonInfo.setUpdateTime(DateUtils.getNowDate());
        tfpChatGroupPersonInfo.setDelFlag(DeleteEnum.EXIST.getValue());
        tfpChatGroupPersonInfo.setCreateBy(loginUser.getUsername());
        return tfpFindPartnerChatGroupPersonInfoMapper.insertTfpFindPartnerChatGroupPersonInfo(tfpChatGroupPersonInfo);
    }

    @Override
    public List<TfpFindPartnerChatGroupPersonInfoVo> selectAppletInfoVoList(TfpChatGroupPersonInfoDto tfpChatGroupPersonInfoDto) {
        //获取聊天数据
        List<TfpFindPartnerChatGroupPersonInfoVo> tfpChatGroupPersonInfoVoList = tfpFindPartnerChatGroupPersonInfoMapper.selectAppletInfoList(tfpChatGroupPersonInfoDto);
        Long userId = SecurityUtils.getUserId();
        if(CollectionUtils.isNotEmpty(tfpChatGroupPersonInfoVoList)){
            List<Long> userIdList = tfpChatGroupPersonInfoVoList.stream().map(TfpFindPartnerChatGroupPersonInfoVo::getChatGroupPersonUserId).collect(Collectors.toList());
            Map<Long, SysUser> userInfoAndAvatar = userService.getUserInfoAndAvatar(userIdList);
            //排序，让前端好处理数据
            tfpChatGroupPersonInfoVoList = tfpChatGroupPersonInfoVoList.stream().sorted(Comparator.comparing(TfpFindPartnerChatGroupPersonInfoVo::getId)).collect(Collectors.toList());
            for (TfpFindPartnerChatGroupPersonInfoVo info : tfpChatGroupPersonInfoVoList) {
                //判断是不是自己
                if(info.getCreateUserId().equals(userId)){
                    info.setSelfFlag(Boolean.TRUE);
                }
                SysUser sysUser = userInfoAndAvatar.get(info.getChatGroupPersonUserId());
                if(Objects.nonNull(sysUser)){
                    UserVo userVo = new UserVo();
                    userVo.setId(sysUser.getUserId());
                    userVo.setName(sysUser.getNickName());
                    TfpAttachment attachment = sysUser.getAttachment();
                    Optional.ofNullable(attachment).map(TfpAttachment::getUrl).ifPresent(e->{
                        userVo.setAvatar(e);
                    });
                    info.setUserVo(userVo);
                }
            }
        }
        return tfpChatGroupPersonInfoVoList;
    }


}
