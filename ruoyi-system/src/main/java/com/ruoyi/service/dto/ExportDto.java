package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel("ExportDto")
public class ExportDto {
    @ApiModelProperty("不全部导出时，选择的id集合")
    private List<Long> idList;
    @ApiModelProperty("是否导出全部 0-否 1-是")
    private Integer allExportFlag;
}
