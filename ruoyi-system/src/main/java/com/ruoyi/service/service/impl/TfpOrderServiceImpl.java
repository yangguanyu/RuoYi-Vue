package com.ruoyi.service.service.impl;

import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.text.DateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.entity.TfpAttachment;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enuma.*;
import com.ruoyi.common.exception.BusinessException;
import com.ruoyi.common.utils.*;
import com.ruoyi.service.domain.*;
import com.ruoyi.service.domain.flight.res.FlightSearchResp;
import com.ruoyi.service.dto.*;
import com.ruoyi.service.excel.OrderListExportExcel;
import com.ruoyi.service.service.*;
import com.ruoyi.service.vo.*;
import com.ruoyi.system.service.ISysUserService;
import com.wechat.pay.java.core.util.NonceUtil;
import com.wechat.pay.java.core.util.PemUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpOrderMapper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import static com.ruoyi.common.utils.PageUtils.startPage;

/**
 * 订单Service业务层处理
 *
 * @author ruoyi
 * @date 2023-10-31
 */
@Service
@Transactional
public class TfpOrderServiceImpl implements ITfpOrderService
{
    @Value("${appid}")
    private String appid;
    /** 商户API私钥路径 */
    @Value("${private.key.path}")
    private String privateKeyPath;
    /** 商户证书序列号 */
    @Value("${merchant.serial.number}")
    private String merchantSerialNumber;
    @Value("${increase.percent}")
    private BigDecimal increasePercent;

    @Autowired
    private TfpOrderMapper tfpOrderMapper;
    @Autowired
    private ITfpSupplierService tfpSupplierService;
    @Autowired
    private ITfpProductService tfpProductService;
    @Autowired
    private ITfpComateService tfpComateService;
    @Autowired
    private ITfpComateOrderService tfpComateOrderService;
    @Autowired
    private IPayService payService;
    @Autowired
    private ITfpEveryDayPriceService tfpEveryDayPriceService;
    @Autowired
    private ITfpChatGroupService chatGroupService;
    @Autowired
    private ITfpChatGroupPersonService chatGroupPersonService;
    @Autowired
    private ITfpProductSnapshotService productSnapshotService;
    @Autowired
    private ITfpProductSubService productSubService;
    @Autowired
    private ISysUserService userService;
    @Autowired
    private ITfpUserIncomeDetailService userIncomeDetailService;
    @Autowired
    private ITfpAttachmentService attachmentService;
    @Autowired
    private ITfpRefundService refundService;
    @Autowired
    private ITfpProductDepartureDateService productDepartureDateService;
    @Autowired
    private ITfpFlightDatePriceService tfpFlightDatePriceService;

    /**
     * 查询订单
     *
     * @param id 订单主键
     * @return 订单
     */
    @Override
    public TfpOrder selectTfpOrderById(Long id)
    {
        return tfpOrderMapper.selectTfpOrderById(id);
    }

    /**
     * 查询订单列表
     *
     * @param tfpOrder 订单
     * @return 订单
     */
    @Override
    public List<TfpOrder> selectTfpOrderList(TfpOrder tfpOrder)
    {
        return tfpOrderMapper.selectTfpOrderList(tfpOrder);
    }

    @Override
    public List<TfpOrderVo> selectTfpOrderVoList(TfpOrderDto tfpOrderDto) {
        String productName = tfpOrderDto.getProductName();
        if (productName != null && !productName.trim().equals("")){
            tfpOrderDto.setProductNameFlag(true);

            TfpProduct tfpProduct = new TfpProduct();
            tfpProduct.setProductName(productName);
            List<TfpProduct> tfpProductList = tfpProductService.selectTfpProductList(tfpProduct);
            if (tfpProductList == null || tfpProductList.size() == 0){
                return new ArrayList<>();
            }

            List<Long> productIdList = tfpProductList.stream().map(TfpProduct::getId).collect(Collectors.toList());
            tfpOrderDto.setProductIdList(productIdList);
        }

        List<TfpOrderVo> tfpOrderVoList = tfpOrderMapper.selectTfpOrderVoList(tfpOrderDto);
        if (tfpOrderVoList != null && tfpOrderVoList.size() > 0){
            List<Long> supplierIdList = new ArrayList<>();
            List<Long> productIdList = new ArrayList<>();
            for (TfpOrderVo tfpOrderVo : tfpOrderVoList) {
                Long supplyId = tfpOrderVo.getSupplyId();
                if (!supplierIdList.contains(supplyId)){
                    supplierIdList.add(supplyId);
                }

                Long productId = tfpOrderVo.getProductId();
                if (!productIdList.contains(productId)){
                    productIdList.add(productId);
                }
            }

            Map<Long, String> supplierNameMap = new HashMap<>();
            List<TfpSupplier> tfpSupplierList = tfpSupplierService.selectSupplerInfoBySupplierIdList(supplierIdList);
            if (tfpSupplierList != null && tfpSupplierList.size() > 0){
                supplierNameMap = tfpSupplierList.stream().collect(Collectors.toMap(TfpSupplier::getId, TfpSupplier::getSupplierName));
            }

            // 商品
            Map<Long, TfpProduct> productMap = new HashMap<>();
            List<TfpProduct> tfpProductList = tfpProductService.selectTfpProductListByIdList(productIdList);
            if (tfpProductList != null && tfpProductList.size() > 0){
                productMap = tfpProductList.stream().collect(Collectors.toMap(TfpProduct::getId, TfpProduct->TfpProduct));
            }

            for (TfpOrderVo tfpOrderVo : tfpOrderVoList) {
                tfpOrderVo.setSupplyName(supplierNameMap.get(tfpOrderVo.getSupplyId()));
                TfpProduct tfpProduct = productMap.get(tfpOrderVo.getProductId());
                if (tfpProduct != null){
                    tfpOrderVo.setProductContent(tfpProduct.getProductName());
                }
            }
        }

        return tfpOrderVoList;
    }

    /**
     * 新增订单
     *
     * @param tfpOrder 订单
     * @return 结果
     */
    @Override
    public int insertTfpOrder(TfpOrder tfpOrder)
    {
        tfpOrder.setCreateTime(DateUtils.getNowDate());
        return tfpOrderMapper.insertTfpOrder(tfpOrder);
    }

    /**
     * 修改订单
     *
     * @param tfpOrder 订单
     * @return 结果
     */
    @Override
    public int updateTfpOrder(TfpOrder tfpOrder)
    {
        tfpOrder.setUpdateTime(DateUtils.getNowDate());
        return tfpOrderMapper.updateTfpOrder(tfpOrder);
    }

    /**
     * 批量删除订单
     *
     * @param ids 需要删除的订单主键
     * @return 结果
     */
    @Override
    public int deleteTfpOrderByIds(Long[] ids)
    {
        return tfpOrderMapper.deleteTfpOrderByIds(ids);
    }

    /**
     * 删除订单信息
     *
     * @param id 订单主键
     * @return 结果
     */
    @Override
    public int deleteTfpOrderById(Long id)
    {
        return tfpOrderMapper.deleteTfpOrderById(id);
    }

    @Override
    public List<SupplierVo> supplySelect() {
        List<Long> supplierIdList = tfpOrderMapper.selectSupplierIdList();
        return tfpSupplierService.handleSupplierVo(supplierIdList);
    }

    /**
     * 创建订单详情
     * @param createOrderDetailDto
     * @return
     */
    @Override
    public CreateOrderDetailVo createOrderDetail(CreateOrderDetailDto createOrderDetailDto) {
        Long productId = createOrderDetailDto.getProductId();
        LoginUser loginUser = SecurityUtils.getLoginUser();

        TfpProduct tfpProduct = tfpProductService.selectTfpProductById(productId);
        if (Objects.isNull(tfpProduct)){
            throw new RuntimeException("商品不存在");
        }

        CreateOrderDetailVo createOrderDetailVo = new CreateOrderDetailVo();
        BeanUtils.copyProperties(tfpProduct, createOrderDetailVo);
        createOrderDetailVo.setProductId(productId);

        Date goDate = createOrderDetailDto.getGoDate();

        AirTicketPriceVo airTicketPriceVo = this.getAirTicketPrice(createOrderDetailDto.getLowPriceAirTicketFlag(), createOrderDetailDto.getGoDate(), tfpProduct);
        BigDecimal totalAirTicketPrice = airTicketPriceVo.getTotalAirTicketPrice();

        // 价格
        BigDecimal adultRetailPrice = productDepartureDateService.getAdultRetailPriceByProductIdAndDate(productId, goDate);
        BigDecimal finalPrice = tfpProductService.mathTotalPrice(adultRetailPrice, totalAirTicketPrice);
        createOrderDetailVo.setPrice(finalPrice);

        createOrderDetailVo.setGoDate(goDate);

        // 群组
        Integer travelType = createOrderDetailDto.getTravelType();
        if (Objects.equals(TravelTypeEnum.TEAM_TRAVEL.getValue(), travelType)){
            Long chatGroupId = createOrderDetailDto.getChatGroupId();
            String groupName = null;
            if (Objects.nonNull(chatGroupId)){
                TfpChatGroup tfpChatGroup = chatGroupService.selectTfpChatGroupById(chatGroupId);
                groupName = tfpChatGroup.getName();
            } else {
                // 选择最新的群组
                TfpChatGroup tfpChatGroup = chatGroupService.getLastGroupByProductIdAndGoDate(productId, goDate);
                if (tfpChatGroup != null){
                    groupName = tfpChatGroup.getName();
                }
            }

            createOrderDetailVo.setChatGroupName(groupName);
        }

        // 出行人
        List<ComateVo> historyComateInfoList = new ArrayList<>();

        // 线路亮点
        List<String> routeHighlightList = this.getRouteHighlightList(productId);
        createOrderDetailVo.setRouteHighlightList(routeHighlightList);

        SysUser user = loginUser.getUser();

        Long userId = loginUser.getUserId();
        if (Objects.equals(TravelTypeEnum.INDEPENDENT_TRAVEL.getValue(), travelType)){
            // 独立出行 取出行人表信息
            TfpComate param = new TfpComate();
            param.setMainUserId(userId);
            param.setDelFlag(DeleteEnum.EXIST.getValue());
            List<TfpComate>  tfpComateList = tfpComateService.selectTfpComateList(param);

            if (DcListUtils.isNotEmpty(tfpComateList)){
                for (TfpComate tfpComate : tfpComateList) {
                    ComateVo comateVo = new ComateVo();
                    BeanUtils.copyProperties(tfpComate, comateVo);
                    comateVo.setMateId(tfpComate.getId());

                    String mateCardNum = tfpComate.getMateCardNum();
                    if (Objects.nonNull(mateCardNum) && Objects.equals(user.getCardNum(), mateCardNum)){
                        comateVo.setRealNameAuthenticationFlag(YesOrNoFlagEnum.YES.getValue());
                        historyComateInfoList.add(0, comateVo);
                    } else {
                        comateVo.setRealNameAuthenticationFlag(YesOrNoFlagEnum.NO.getValue());
                        historyComateInfoList.add(comateVo);
                    }
                }
            }
        } else {
            // 组队出行 获取用户实名认证信息
            String cardType = user.getCardType();

            ComateVo comateVo = new ComateVo();
            comateVo.setMateCardType(Objects.isNull(cardType) ? null : new Integer(cardType));
            comateVo.setMateCardNum(user.getCardNum());
            comateVo.setMateName(user.getRealName());
            comateVo.setMatePhone(user.getRealNamePhone());
            comateVo.setRealNameAuthenticationFlag(YesOrNoFlagEnum.YES.getValue());
            historyComateInfoList.add(comateVo);
        }

        createOrderDetailVo.setHistoryComateInfoList(historyComateInfoList);

        // 商品图片
        List<TfpAttachment> attachmentList = attachmentService.selectListByBusinessId(productId, BusinessTypeEnum.PRODUCT.getValue(), BusinessSubTypeEnum.PRODUCT_IMAGE.getValue());
        if (DcListUtils.isNotEmpty(attachmentList)) {
            createOrderDetailVo.setProductImage(attachmentList.get(0).getUrl());
        }

        return createOrderDetailVo;
    }

    /**
     * 创建订单
     * @param createOrderDto
     * @return
     */
    @Override
    public CreateOrderVo createOrder(CreateOrderDto createOrderDto) {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = loginUser.getUserId();

        Long productId = createOrderDto.getProductId();
        TfpProduct tfpProduct = tfpProductService.selectTfpProductById(productId);
        if (Objects.isNull(tfpProduct)){
            throw new RuntimeException("商品信息不存在");
        }

        if (Objects.equals(TravelTypeEnum.INDEPENDENT_TRAVEL.getValue(), createOrderDto.getTravelType()) && DcListUtils.isEmpty(createOrderDto.getMateIdList())){
            throw new BusinessException("独立出行必须选择出行人");
        }

        Date goDate = createOrderDto.getGoDate();
        goDate = DcDateUtils.handelDateToDayStart(goDate);

        // 获取机票价格
        Integer lowPriceAirTicketFlag = createOrderDto.getLowPriceAirTicketFlag();
        AirTicketPriceVo airTicketPriceVo = this.getAirTicketPrice(lowPriceAirTicketFlag, createOrderDto.getGoDate(), tfpProduct);
        BigDecimal totalAirTicketPrice = airTicketPriceVo.getTotalAirTicketPrice();

        // 订单信息
        BigDecimal adultRetailPrice = productDepartureDateService.getAdultRetailPriceByProductIdAndDate(productId, goDate);
        BigDecimal finalPrice = tfpProductService.mathTotalPrice(adultRetailPrice, totalAirTicketPrice);
        if (finalPrice.compareTo(createOrderDto.getSalePrice()) != 0){
            throw new RuntimeException("售价信息不一致");
        }

        Integer quantities = createOrderDto.getQuantities();
        BigDecimal finalAmount = finalPrice.multiply(new BigDecimal(quantities));
        if (finalAmount.compareTo(createOrderDto.getFinalAmount()) != 0){
            throw new RuntimeException("信息发生错误");
        }

        Integer travelType = createOrderDto.getTravelType();

        Date nowDate = DateUtils.getNowDate();
        // 过期时间30分钟
        Date expireDate = new Date(nowDate.getTime() + (30 * 60 * 1000));

        String goPlaceName = createOrderDto.getGoPlaceName();

        // 订单信息存储
        TfpOrder tfpOrder = new TfpOrder();
        tfpOrder.setSupplyId(tfpProduct.getSupplierId());
        tfpOrder.setProductId(productId);
        tfpOrder.setSalePrice(createOrderDto.getSalePrice());
        tfpOrder.setQuantities(quantities);
        tfpOrder.setFinalAmount(finalAmount);
        tfpOrder.setGoDate(goDate);
        tfpOrder.setStatus(OrderStatusEnum.WAIT_PAY.getValue());
        tfpOrder.setGoPlaceName(goPlaceName);
        tfpOrder.setTravelType(travelType);
        tfpOrder.setApplyRefundFlag(YesOrNoFlagEnum.YES.getValue());
        tfpOrder.setDelFlag(DeleteEnum.EXIST.getValue());
        tfpOrder.setCreateUserId(loginUser.getUserId());
        tfpOrder.setCreateBy(loginUser.getUsername());
        tfpOrder.setCreateTime(nowDate);
        tfpOrder.setShareUserId(createOrderDto.getShareUserId());
        tfpOrder.setLowPriceAirTicketFlag(createOrderDto.getLowPriceAirTicketFlag());
        tfpOrder.setGoAirTicketPrice(airTicketPriceVo.getGoAirTicketPrice());
        tfpOrder.setBackAirTicketPrice(airTicketPriceVo.getBackAirTicketPrice());
        tfpOrderMapper.insertTfpOrder(tfpOrder);

        Long id = tfpOrder.getId();

        // 订单编号
        String orderNo = this.createOrderNo(id);
        tfpOrder.setOrderNo(orderNo);

        // 开始支付
        String prepayId = this.createWeChatOrder(orderNo, finalAmount, expireDate);

        tfpOrder.setPrepayId(prepayId);
        tfpOrder.setExpireDate(expireDate);

        // 保存同行人
        if (Objects.equals(TravelTypeEnum.INDEPENDENT_TRAVEL.getValue(), createOrderDto.getTravelType())){
            // 独立出行
            List<Long> mateIdList = createOrderDto.getMateIdList();
            if (DcListUtils.isNotEmpty(mateIdList)){
                List<TfpComate> tfpComateList = tfpComateService.selectTfpComateByIdList(mateIdList);
                for (TfpComate tfpComate : tfpComateList) {
                    TfpComateOrder tfpComateOrder = new TfpComateOrder();
                    BeanUtils.copyProperties(tfpComate, tfpComateOrder);

                    tfpComateOrder.setOrderId(id);
                    tfpComateOrder.setOrderNo(orderNo);
                    tfpComateOrder.setMateId(tfpComate.getId());

                    tfpComateOrder.setDelFlag(DeleteEnum.EXIST.getValue());
                    tfpComateOrder.setCreateUserId(loginUser.getUserId());
                    tfpComateOrder.setCreateBy(loginUser.getUsername());
                    tfpComateOrder.setCreateTime(DateUtils.getNowDate());
                    tfpComateOrderService.insertTfpComateOrder(tfpComateOrder);
                }
            }
        } else {
            // 组队出行
            SysUser user = loginUser.getUser();

            TfpComateOrder tfpComateOrder = new TfpComateOrder();
            tfpComateOrder.setOrderId(id);
            tfpComateOrder.setOrderNo(orderNo);
            tfpComateOrder.setMateName(user.getRealName());
            tfpComateOrder.setMatePhone(user.getRealNamePhone());
            tfpComateOrder.setMateCardType(Objects.isNull(user.getCardType()) ? null : new Integer(user.getCardType()));
            tfpComateOrder.setMateCardNum(user.getCardNum());
            tfpComateOrder.setDelFlag(DeleteEnum.EXIST.getValue());
            tfpComateOrder.setCreateUserId(loginUser.getUserId());
            tfpComateOrder.setCreateBy(loginUser.getUsername());
            tfpComateOrder.setCreateTime(DateUtils.getNowDate());
            tfpComateOrderService.insertTfpComateOrder(tfpComateOrder);
        }

        // 处理群组人员信息
        Long orderChatGroupId = null;
        Long categoryId = tfpProduct.getCategoryId();
        if (Objects.equals(ProductCategoryEnum.WHOLE_COUNTRY.getValue(), categoryId)
                || Objects.equals(ProductCategoryEnum.WHOLE_COUNTRY_SMALL.getValue(), categoryId)){
            TfpChatGroup groupParam = new TfpChatGroup();
            groupParam.setProductId(productId);
            groupParam.setDelFlag(DeleteEnum.EXIST.getValue());
            List<TfpChatGroup> chatGroupList = chatGroupService.selectTfpChatGroupList(groupParam);
            if (DcListUtils.isNotEmpty(chatGroupList)){
                TfpChatGroup tfpChatGroup = chatGroupList.get(0);

                orderChatGroupId = tfpChatGroup.getId();

                TfpChatGroupPerson personParam = new TfpChatGroupPerson();
                personParam.setDelFlag(DeleteEnum.EXIST.getValue());
                personParam.setUserId(userId);
                personParam.setChatGroupId(orderChatGroupId);
                List<TfpChatGroupPerson> tfpChatGroupPersonList = chatGroupPersonService.selectTfpChatGroupPersonList(personParam);
                if (DcListUtils.isNotEmpty(tfpChatGroupPersonList)){
                    TfpChatGroupPerson tfpChatGroupPerson = tfpChatGroupPersonList.get(0);
                    tfpChatGroupPerson.setUserType(ChatUserTypeEnum.UN_APPLIED.getValue());
                    tfpChatGroupPerson.setUpdateUserId(loginUser.getUserId());
                    tfpChatGroupPerson.setUpdateBy(loginUser.getUsername());
                    tfpChatGroupPerson.setUpdateTime(DateUtils.getNowDate());
                    chatGroupPersonService.updateTfpChatGroupPerson(tfpChatGroupPerson);
                }
            }
        } else {
            if (Objects.equals(TravelTypeEnum.TEAM_TRAVEL.getValue(), travelType)){
                orderChatGroupId = createOrderDto.getChatGroupId();
                if (Objects.isNull(orderChatGroupId)){
                    // 组队没有传参群组id，获取最新的群组信息
                    TfpChatGroup chatGroup = chatGroupService.getLastGroupByProductIdAndGoDate(productId, goDate);
                    if (Objects.nonNull(chatGroup)){
                        orderChatGroupId = chatGroup.getId();
                    }
                }

                if (Objects.nonNull(orderChatGroupId)){
                    TfpChatGroupPerson tfpChatGroupPerson = chatGroupPersonService.selectTfpChatGroupPersonByGroupIdAndUserId(orderChatGroupId, userId);
                    if (Objects.nonNull(tfpChatGroupPerson)){
                        tfpChatGroupPerson.setUpdateUserId(loginUser.getUserId());
                        tfpChatGroupPerson.setUpdateBy(loginUser.getUsername());
                        tfpChatGroupPerson.setUpdateTime(DateUtils.getNowDate());
                        chatGroupPersonService.updateTfpChatGroupPerson(tfpChatGroupPerson);
                    } else {
                        // 初始化群组人员信息
                        tfpChatGroupPerson = new TfpChatGroupPerson();
                        tfpChatGroupPerson.setChatGroupId(orderChatGroupId);
                        tfpChatGroupPerson.setProductId(productId);
                        tfpChatGroupPerson.setUserId(userId);
                        tfpChatGroupPerson.setUserType(ChatUserTypeEnum.UN_APPLIED.getValue());
                        tfpChatGroupPerson.setDelFlag(DeleteEnum.EXIST.getValue());
                        tfpChatGroupPerson.setCreateUserId(loginUser.getUserId());
                        tfpChatGroupPerson.setCreateBy(loginUser.getUsername());
                        tfpChatGroupPerson.setCreateTime(DateUtils.getNowDate());
                        chatGroupPersonService.insertTfpChatGroupPerson(tfpChatGroupPerson);
                    }
                }
            }
        }

        // 商品快照id
        Long snapshotId = productSnapshotService.createSnapshot(productId, finalAmount, goDate, goPlaceName, travelType, orderChatGroupId, lowPriceAirTicketFlag, createOrderDto.getRuleId());

        tfpOrder.setSnapshotId(snapshotId);
        tfpOrder.setChatGroupId(orderChatGroupId);
        tfpOrderMapper.updateTfpOrder(tfpOrder);

        // 通过分享id 修改用户收益明细信息
        Long shareUserId = createOrderDto.getShareUserId();
        if (Objects.nonNull(shareUserId) && !Objects.equals(userId, shareUserId)){
            userIncomeDetailService.updateShareInfo(shareUserId,UserIncomeSourceTypeEnum.PRODUCT.getValue(), productId, UserIncomeDetailStatusEnum.PLACE_ORDER.getValue(), id, orderNo, adultRetailPrice);
        }

        RequestPaymentVo requestPayment = null;
        try {
            requestPayment = this.handleRequestPaymentVo(prepayId);
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (SignatureException e) {
            e.printStackTrace();
        }

        // 组装返参
        CreateOrderVo createOrderVo = new CreateOrderVo();
        createOrderVo.setOrderId(tfpOrder.getId());
        createOrderVo.setOrderNo(orderNo);
        createOrderVo.setPrepayId(prepayId);
        createOrderVo.setFinalAmount(finalAmount);
        createOrderVo.setRequestPayment(requestPayment);
        return createOrderVo;
    }

    private String createOrderNo(Long id) {
        String orderNo;
        // yyyyMMddHHmmss
        DateFormat yMdDateFormat = DcDateUtils.getDcDateFormat(DcDateUtils.yMdHms);
        String DateFormatStr = yMdDateFormat.format(new Date());

        int idLength = id.toString().length();
        if (idLength > 6){
            orderNo = DateFormatStr + String.format("%0" + idLength + "d", id);
        } else {
            orderNo = DateFormatStr + String.format("%06d", id);
        }

        return orderNo;
    }

    /**
     * 查询商品的订单信息集合
     * @param productIdList
     * @param statusList
     * @return
     */
    @Override
    public List<TfpOrder> selectListByProductIdList(List<Long> productIdList, List<Integer> statusList){
        if (DcListUtils.isEmpty(productIdList)){
            throw new RuntimeException("商品id不能为空");
        }

        return tfpOrderMapper.selectListByProductIdList(productIdList, statusList);
    }

    /**
     * 小程序-取消订单
     * @param orderId
     * @return
     */
    @Override
    public int cancelOrder(Long orderId) {
        TfpOrder tfpOrder = tfpOrderMapper.selectTfpOrderById(orderId);
        this.handleCancelOrderInfo(tfpOrder);

        return 1;
    }

    /**
     * 小程序-我的订单集合
     * @param myOrderListDto
     * @return
     */
    @Override
    public List<MyOrderListVo> myOrderList(MyOrderListDto myOrderListDto) {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = loginUser.getUserId();

        startPage();
        List<MyOrderListVo> myOrderListVoList = tfpOrderMapper.myOrderList(userId, myOrderListDto.getStatus());
        if (DcListUtils.isNotEmpty(myOrderListVoList)){
            List<Long> productIdList = new ArrayList<>();
            List<Long> idList = new ArrayList<>();
            for (MyOrderListVo myOrderListVo : myOrderListVoList) {
                Long productId = myOrderListVo.getProductId();
                if (!productIdList.contains(productId)){
                    productIdList.add(productId);
                }

                idList.add(myOrderListVo.getId());
            }

            List<TfpProduct> tfpProductList = tfpProductService.selectTfpProductListByIdList(productIdList);
            Map<Long, TfpProduct> productMap = tfpProductList.stream().collect(Collectors.toMap(TfpProduct::getId, TfpProduct -> TfpProduct));

            // 商品图片
            List<TfpAttachment> attachmentList = attachmentService.selectList(productIdList, BusinessTypeEnum.PRODUCT.getValue(), BusinessSubTypeEnum.PRODUCT_IMAGE.getValue());
            Map<Long, List<TfpAttachment>> productImageMap;
            if (DcListUtils.isNotEmpty(attachmentList)){
                productImageMap = attachmentList.stream().collect(Collectors.groupingBy(TfpAttachment::getBusinessId));
            } else {
                productImageMap = new HashMap<>();
            }

            List<OrderChatGroupInfoVo> groupInfoVoList = chatGroupService.getChatGroupInfoVoListByOrderList(myOrderListVoList);
            Map<Long, OrderChatGroupInfoVo> groupInfoVoMap;
            if (DcListUtils.isNotEmpty(groupInfoVoList)){
                groupInfoVoMap = groupInfoVoList.stream().collect(Collectors.toMap(OrderChatGroupInfoVo::getOrderId, OrderChatGroupInfoVo -> OrderChatGroupInfoVo));
            } else {
                groupInfoVoMap = new HashMap<>();
            }

            for (MyOrderListVo myOrderListVo : myOrderListVoList) {
                Long id = myOrderListVo.getId();
                Long productId = myOrderListVo.getProductId();
                TfpProduct tfpProduct = productMap.get(productId);

                myOrderListVo.setChatGroupInfo(groupInfoVoMap.get(id));

                if (Objects.nonNull(tfpProduct)){
                    myOrderListVo.setProductName(tfpProduct.getProductName());
                    myOrderListVo.setCategoryId(tfpProduct.getCategoryId());
                }

                // 商品图片
                List<TfpAttachment> productImageList = productImageMap.get(productId);
                if (DcListUtils.isNotEmpty(productImageList)){
                    myOrderListVo.setProductImage(productImageList.get(0).getUrl());
                }
            }
        }

        return myOrderListVoList;
    }

    /**
     * 小程序-订单详情
     * @param orderId
     * @return
     */
    @Override
    public OrderDetailVo orderDetail(Long orderId) {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = loginUser.getUserId();

        TfpOrder tfpOrder = tfpOrderMapper.selectTfpOrderById(orderId);
        if (Objects.isNull(tfpOrder)){
            throw new RuntimeException("订单不存在");
        }

        OrderDetailVo detailVo = new OrderDetailVo();
        detailVo.setOrderId(orderId);

        Long productId = tfpOrder.getProductId();
        TfpProduct tfpProduct = tfpProductService.selectTfpProductById(productId);

        // 线路亮点
        List<String> routeHighlightList = this.getRouteHighlightList(productId);

        TfpComateOrder param = new TfpComateOrder();
        param.setOrderId(orderId);
        param.setDelFlag(DeleteEnum.EXIST.getValue());
        List<TfpComateOrder> tfpComateOrderList = tfpComateOrderService.selectTfpComateOrderList(param);
        if (DcListUtils.isNotEmpty(tfpComateOrderList)){
            List<String> mateNameList = new ArrayList<>();
            for (TfpComateOrder tfpComateOrder : tfpComateOrderList) {
                String mateName = tfpComateOrder.getMateName();
                if (Objects.isNull(tfpComateOrder.getMateId())){
                    mateNameList.add(0, mateName);
                } else {
                    mateNameList.add(mateName);
                }
            }
            detailVo.setMateNameList(mateNameList);
        }

        BeanUtils.copyProperties(tfpOrder, detailVo);

        detailVo.setProductName(tfpProduct.getProductName());
        detailVo.setCategoryId(tfpProduct.getCategoryId());
        detailVo.setCategoryName(tfpProduct.getCategoryName());
        detailVo.setLowPriceAirTicketServiceFlag(tfpProduct.getLowPriceAirTicketServiceFlag());

        detailVo.setRouteHighlightList(routeHighlightList);

        // 群组
        Integer travelType = tfpOrder.getTravelType();
        Long categoryId = tfpProduct.getCategoryId();
        if (Objects.equals(ProductCategoryEnum.WHOLE_COUNTRY.getValue(), categoryId)
                || Objects.equals(ProductCategoryEnum.WHOLE_COUNTRY_SMALL.getValue(), categoryId)
                || Objects.equals(TravelTypeEnum.TEAM_TRAVEL.getValue(), travelType)){
            Long chatGroupId = tfpOrder.getChatGroupId();
            if (Objects.nonNull(chatGroupId)){
                TfpChatGroup tfpChatGroup = chatGroupService.selectTfpChatGroupById(chatGroupId);
                if (Objects.nonNull(tfpChatGroup)){
                    String chatGroupName = tfpChatGroup.getName();

                    detailVo.setChatGroupId(tfpChatGroup.getId());
                    detailVo.setChatGroupName(chatGroupName);
                    detailVo.setLimitPersonNumber(tfpChatGroup.getLimitPersonNumber());

                    TfpChatGroupPerson personParam = new TfpChatGroupPerson();
                    personParam.setChatGroupId(tfpChatGroup.getId());
                    personParam.setDelFlag(DeleteEnum.EXIST.getValue());
                    List<TfpChatGroupPerson> tfpChatGroupPersonList = chatGroupPersonService.selectTfpChatGroupPersonList(personParam);

                    List<Long> groupPersonUserIdList = tfpChatGroupPersonList.stream().map(TfpChatGroupPerson::getUserId).distinct().collect(Collectors.toList());
                    List<TfpAttachment> attchmentList = userService.getHeadPortraitListByUserIdList(groupPersonUserIdList);
                    Map<Long, String> headPortraitMap;
                    if (DcListUtils.isNotEmpty(attchmentList)){
                        headPortraitMap = attchmentList.stream().collect(Collectors.toMap(TfpAttachment::getBusinessId, TfpAttachment::getUrl));
                    } else {
                        headPortraitMap = new HashMap<>();
                    }

                    List<String> chatGroupPersonHeadPortraitUrlList = new ArrayList<>();
                    for (TfpChatGroupPerson chatGroupPerson : tfpChatGroupPersonList) {
                        // 如果仅展示报名用户头像，可以放开
//                    Integer userType = chatGroupPerson.getUserType();
//                    if (Objects.equals(userType, ChatUserTypeEnum.BOOT_MAN.getValue())
//                            || Objects.equals(userType, ChatUserTypeEnum.APPLIED.getValue())){
                        Long personUserId = chatGroupPerson.getUserId();
                        String headPortrait = headPortraitMap.get(personUserId);
                        if (headPortrait != null && !chatGroupPersonHeadPortraitUrlList.contains(headPortrait)){
                            chatGroupPersonHeadPortraitUrlList.add(headPortrait);
                        }
//                    }
                    }

                    detailVo.setSignUpPersonHeadPortraitUrlList(chatGroupPersonHeadPortraitUrlList);
                    detailVo.setSignUpPersonNumber(tfpChatGroup.getSignUpPersonNumber());
                }
            }
        }

        // 商品图片
        List<TfpAttachment> attachmentList = attachmentService.selectListByBusinessId(productId, BusinessTypeEnum.PRODUCT.getValue(), BusinessSubTypeEnum.PRODUCT_IMAGE.getValue());
        if (DcListUtils.isNotEmpty(attachmentList)) {
            detailVo.setProductImage(attachmentList.get(0).getUrl());
        }

        // 退款信息
        TfpRefund refundParam = new TfpRefund();
        refundParam.setOrderId(orderId);
        refundParam.setDelFlag(DeleteEnum.EXIST.getValue());
        List<TfpRefund> tfpRefundList = refundService.selectTfpRefundList(refundParam);
        if (DcListUtils.isNotEmpty(tfpRefundList)){
            // 取最新的申请退款信息
            tfpRefundList = tfpRefundList.stream().sorted(Comparator.comparing(TfpRefund::getCreateTime).reversed()).collect(Collectors.toList());
            TfpRefund tfpRefund = tfpRefundList.get(0);

            RefundInfoVo refundInfo = new RefundInfoVo();
            refundInfo.setRefundStatus(tfpRefund.getRefundStatus());
            refundInfo.setRejectReason(tfpRefund.getRejectReason());
            refundInfo.setSerialNo(tfpRefund.getSerialNo());
            refundInfo.setRefundAmount(tfpRefund.getRefundAmount());
            refundInfo.setApplyTime(tfpRefund.getCreateTime());
            refundInfo.setReason(tfpRefund.getReason());
            refundInfo.setRefundTime(tfpRefund.getRefundTime());
            detailVo.setRefundInfo(refundInfo);
        }

        return detailVo;
    }

    @Override
    public List<TfpOrder> selectListByProductId(Long productId, List<Integer> statusList){
        List<Long> productIdList = new ArrayList<>();
        productIdList.add(productId);
        return this.selectListByProductIdList(productIdList, statusList);
    }

    @Override
    public List<TfpOrder> selectTfpOrderByIdList(List<Long> idList){
        return tfpOrderMapper.selectTfpOrderByIdList(idList);
    }

    /**
     * 获取线路亮点
     * @param productId
     * @return
     */
    @Override
    public List<String> getRouteHighlightList(Long productId) {
        TfpProductSub tfpProductSub = productSubService.selectTfpProductSubByProductId(productId);
        List<String> routeHighlightList = new ArrayList<>();
        String routeHighlight = tfpProductSub.getRouteHighlight();
        if (routeHighlight != null){
            // 多个线路亮点根据中文分号隔开
            String[] routeHighlightArrys = routeHighlight.split("；");
            for (String routeHighlightArry : routeHighlightArrys) {
                routeHighlightList.add(routeHighlightArry);
            }
        }
        return routeHighlightList;
    }

    @Override
    public int handlePaySuccessAfterInfo(String orderNo) {
        if (Objects.isNull(orderNo)){
            throw new RuntimeException("订单编号不能为空");
        }

        TfpOrder tfpOrder = tfpOrderMapper.selectTfpOrderByOrderNo(orderNo);
        if (Objects.isNull(tfpOrder)){
            throw new RuntimeException("订单不存在");
        }

        Integer status = tfpOrder.getStatus();
        if (Objects.equals(OrderStatusEnum.WAIT_PAY.getValue(), status)){
            // 待支付订单进行处理
            tfpOrder.setStatus(OrderStatusEnum.WAIT_CONFIRMED.getValue());
            tfpOrder.setPayTime(DateUtils.getNowDate());

            TfpProduct tfpProduct = tfpProductService.selectTfpProductById(tfpOrder.getProductId());
            Long categoryId = tfpProduct.getCategoryId();

            // 群组处理
            Long chatGroupId = chatGroupService.handlePaySuccessChatGroupInfo(tfpOrder, categoryId);
            tfpOrder.setChatGroupId(chatGroupId);
            tfpOrder.setUpdateUserId(tfpOrder.getCreateUserId());
            tfpOrder.setUpdateBy(tfpOrder.getCreateBy());
            tfpOrder.setUpdateTime(DateUtils.getNowDate());
            tfpOrderMapper.updateTfpOrder(tfpOrder);

            // 修改用户收益明细信息
            Long id = tfpOrder.getId();
            userIncomeDetailService.updateShareInfo(tfpOrder.getShareUserId(), UserIncomeSourceTypeEnum.PRODUCT.getValue(), tfpOrder.getProductId(), UserIncomeDetailStatusEnum.PAY.getValue(), id, orderNo, null);
        }

        return 1;
    }

    /**
     * 小程序-价格明细
     * @param priceDetailDto
     * @return
     */
    @Override
    public PriceDetailVo priceDetail(PriceDetailDto priceDetailDto) {
        PriceDetailVo priceDetailVo = new PriceDetailVo();

        Long productId = priceDetailDto.getProductId();

        // 获取机票价格
        TfpProduct tfpProduct = tfpProductService.selectTfpProductById(productId);
        AirTicketPriceVo airTicketPriceVo = this.getAirTicketPrice(priceDetailDto.getLowPriceAirTicketFlag(), priceDetailDto.getGoDate(), tfpProduct);
        BigDecimal totalAirTicketPrice = airTicketPriceVo.getTotalAirTicketPrice();

        Date goDate = priceDetailDto.getGoDate();
        BigDecimal adultRetailPrice = productDepartureDateService.getAdultRetailPriceByProductIdAndDate(productId, goDate);
        BigDecimal finalPrice = tfpProductService.mathTotalPrice(adultRetailPrice, totalAirTicketPrice);

        Integer quantities = priceDetailDto.getPersonNumber();
        BigDecimal finalAmount = finalPrice.multiply(new BigDecimal(quantities));

        priceDetailVo.setSalePrice(finalPrice);
        priceDetailVo.setPersonNumber(quantities);
        priceDetailVo.setFinalAmount(finalAmount);
        return priceDetailVo;
    }

    /**
     * 获取机票价格
     * @param lowPriceAirTicketFlag
     * @param goDate
     * @param tfpProduct
     * @return
     */
    @Override
    public AirTicketPriceVo getAirTicketPrice(Integer lowPriceAirTicketFlag, Date goDate, TfpProduct tfpProduct){
        AirTicketPriceVo airTicketPriceVo = new AirTicketPriceVo();

        BigDecimal totalAirTicketPrice = BigDecimal.ZERO;
        BigDecimal goAirTicketPrice = null;
        BigDecimal backAirTicketPrice = null;
        if (Objects.equals(lowPriceAirTicketFlag, YesOrNoFlagEnum.YES.getValue())){
            DateFormat dcDateFormat = DcDateUtils.getDcDateFormat(DcDateUtils.yyyyMMdd);
            String departureTime = dcDateFormat.format(goDate);

            // 去程：固定北京出发  抵达目的地：取目的地参团-出发城市。
            String departureAirportName = "北京";
            String arrivalAirportName = tfpProduct.getGoPlaceName();
            TfpFlightDatePrice flightDatePrice = tfpFlightDatePriceService.selectTfpFlightDatePriceByTimeAndName(departureTime, departureAirportName, arrivalAirportName);
            if (Objects.nonNull(flightDatePrice)){
                goAirTicketPrice = flightDatePrice.getPrice();
            }

            // 返程：取目的地参团-返回城市出发 抵达目的地固定北京
            String backDepartureAirportName = tfpProduct.getBackPlaceName();
            String backArrivalAirportName = "北京";

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(goDate);
            calendar.add(Calendar.DAY_OF_MONTH, tfpProduct.getDayNum() - 1);
            Date backDepartureDate = calendar.getTime();
            String backDepartureTime = dcDateFormat.format(backDepartureDate);
            TfpFlightDatePrice backFlightDatePrice = tfpFlightDatePriceService.selectTfpFlightDatePriceByTimeAndName(backDepartureTime, backDepartureAirportName, backArrivalAirportName);
            if (Objects.nonNull(backFlightDatePrice)){
                backAirTicketPrice = backFlightDatePrice.getPrice();
            }

            if (Objects.nonNull(goAirTicketPrice) && Objects.nonNull(backAirTicketPrice)){
                totalAirTicketPrice = totalAirTicketPrice.add(goAirTicketPrice).add(backAirTicketPrice);
            }
        }

        airTicketPriceVo.setGoAirTicketPrice(goAirTicketPrice);
        airTicketPriceVo.setBackAirTicketPrice(backAirTicketPrice);
        airTicketPriceVo.setTotalAirTicketPrice(totalAirTicketPrice);
        return airTicketPriceVo;
    }

    /**
     * 小程序-获取前端调起支付参数
     * @param orderId
     * @return
     */
    @Override
    public RequestPaymentVo getRequestPayment(Long orderId) {
        TfpOrder tfpOrder = tfpOrderMapper.selectTfpOrderById(orderId);
        if (Objects.isNull(tfpOrder)){
            throw new RuntimeException("订单不存在");
        }

        Date expireDate = tfpOrder.getExpireDate();
        if (expireDate.before(new Date())){
            throw new RuntimeException("订单过期，无法进行支付");
        }

        String prepayId = tfpOrder.getPrepayId();
        if (Objects.isNull(prepayId) || prepayId.length() == 0){
            // 重新请求第三方支付
            Date nowDate = DateUtils.getNowDate();
            // 过期时间30分钟
            expireDate = new Date(nowDate.getTime() + (30 * 60 * 1000));
            // 开始支付
            prepayId = this.createWeChatOrder(tfpOrder.getOrderNo(), tfpOrder.getFinalAmount(), expireDate);
        }

        RequestPaymentVo requestPaymentVo = null;
        try {
            requestPaymentVo = this.handleRequestPaymentVo(prepayId);
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (SignatureException e) {
            e.printStackTrace();
        }

        return requestPaymentVo;
    }

    /**
     * 处理过期订单
     * @return
     */
    @Override
    public void handleExpireOrder(){
        List<TfpOrder> expireOrderList = tfpOrderMapper.selectExpireOrderList();
        if (DcListUtils.isNotEmpty(expireOrderList)){
            for (TfpOrder tfpOrder : expireOrderList) {
                this.handleCancelOrderInfo(tfpOrder);
            }
        }
    }

    @Override
    public List<TfpOrder> selectNotDownShelivesOrderList(Long productId) {
        return tfpOrderMapper.selectNotDownShelivesOrderList(productId);
    }

    /**
     * 后台订单列表
     * @param orderListDto
     * @return
     */
    @Override
    public List<OrderListVo> orderList(OrderListDto orderListDto) {
        boolean queryProductFlag = false;
        String productNo = orderListDto.getProductNo();
        Long categoryId = orderListDto.getCategoryId();
        Long supplierId = orderListDto.getSupplierId();
        if (Objects.nonNull(productNo) || Objects.nonNull(categoryId) || Objects.nonNull(supplierId)){
            queryProductFlag = true;
            TfpProduct productParam = new TfpProduct();
            productParam.setProductNo(productNo);
            productParam.setCategoryId(categoryId);
            productParam.setCategoryId(supplierId);
            productParam.setDelFlag(DeleteEnum.EXIST.getValue());
            List<TfpProduct> tfpProductList = tfpProductService.selectTfpProductList(productParam);
            if (DcListUtils.isNotEmpty(tfpProductList)){
                List<Long> productIdList =  tfpProductList.stream().map(TfpProduct::getId).collect(Collectors.toList());
                orderListDto.setProductIdList(productIdList);
            }
        }

        Integer refundType = orderListDto.getRefundType();
        if (Objects.equals(refundType, RefundTypeEnum.ALL_REFUND.getValue()) || Objects.equals(refundType, RefundTypeEnum.PART_REFUND.getValue())){
            List<Long> orderIdList = new ArrayList<>();
            List<TfpRefund> refundList = refundService.selectCompleteTfpRefundListByRefundType(refundType);
            if (DcListUtils.isNotEmpty(refundList)){
                orderIdList = refundList.stream().map(TfpRefund::getOrderId).distinct().collect(Collectors.toList());
            }
            orderListDto.setOrderIdList(orderIdList);
        }

        orderListDto.setQueryProductFlag(queryProductFlag);

        startPage();
        List<OrderListVo> orderListVoList = tfpOrderMapper.orderList(orderListDto);
        this.handleOrderList(orderListVoList);

        return orderListVoList;
    }

    /**
     * 后台-确认支付订单
     * @param confirmPayOrderDto
     * @return
     */
    @Override
    public int confirmPayOrder(ConfirmPayOrderDto confirmPayOrderDto) {
        Integer confirmStatus = confirmPayOrderDto.getConfirmStatus();
        if (!Objects.equals(AuditStatusEnum.AUDIT_PASS.getValue(), confirmStatus) && !Objects.equals(AuditStatusEnum.AUDIT_NO_PASS.getValue(), confirmStatus)){
            throw new BusinessException("确认状态有误");
        }

        LoginUser loginUser = SecurityUtils.getLoginUser();

        Long id = confirmPayOrderDto.getId();
        TfpOrder tfpOrder = tfpOrderMapper.selectTfpOrderById(id);

        if (Objects.equals(AuditStatusEnum.AUDIT_PASS.getValue(), confirmStatus)){
            // 确认
            tfpOrder.setStatus(OrderStatusEnum.WAIT_TRAVEL.getValue());
            tfpOrder.setUpdateUserId(loginUser.getUserId());
            tfpOrder.setUpdateBy(loginUser.getUsername());
            tfpOrder.setUpdateTime(DateUtils.getNowDate());
            tfpOrderMapper.updateTfpOrder(tfpOrder);
        } else {
            // 取消 依据退款规则，部分退或整单退，点击“取消”时，填写退款原因并反馈在小程序端，此时，该订单状态在小程序端为“退款售后”单。
            tfpOrder.setStatus(OrderStatusEnum.APPLY_REFUND_SUCCESS.getValue());
            tfpOrder.setUpdateUserId(loginUser.getUserId());
            tfpOrder.setUpdateBy(loginUser.getUsername());
            tfpOrder.setUpdateTime(DateUtils.getNowDate());
            tfpOrderMapper.updateTfpOrder(tfpOrder);

            AuditRefundDto auditRefundDto = new AuditRefundDto();
            auditRefundDto.setAuditStatus(confirmStatus);
            auditRefundDto.setRefundType(confirmPayOrderDto.getRefundType());
            auditRefundDto.setRefundAmount(confirmPayOrderDto.getRefundAmount());
            auditRefundDto.setReason(confirmPayOrderDto.getReason());
            auditRefundDto.setTfpOrder(tfpOrder);

            refundService.auditRefund(auditRefundDto);
        }

        return 1;
    }

    @Override
    public List<TfpOrder> queryUserIncomeDetailOrderList(List<Long> productIdList, Integer afterSalesTimeFlag) {
        return tfpOrderMapper.queryUserIncomeDetailOrderList(productIdList, afterSalesTimeFlag);
    }

    @Override
    public List<OrderListExportExcel> orderListExport(ExportDto exportDto) {
        Integer allExportFlag = exportDto.getAllExportFlag();
        if (Objects.isNull(allExportFlag)){
            throw new BusinessException("请选择导出的类型");
        }

        List<Long> idList = exportDto.getIdList();
        if (Objects.equals(YesOrNoFlagEnum.NO.getValue(), allExportFlag) && DcListUtils.isEmpty(idList)){
            throw new BusinessException("请选择需要导出的数据");
        }

        List<OrderListVo> orderListVoList = tfpOrderMapper.orderListExport(exportDto);
        this.handleOrderList(orderListVoList);

        List<OrderListExportExcel> orderListExportExcelList = new ArrayList<>();
        if (DcListUtils.isNotEmpty(orderListVoList)){
            for (OrderListVo orderListVo : orderListVoList) {
                OrderListExportExcel orderListExportExcel = new OrderListExportExcel();
                BeanUtils.copyProperties(orderListVo, orderListExportExcel);

                List<OrderListComateInfoVo> comateUserInfoList = orderListVo.getComateUserInfoList();
                if (DcListUtils.isNotEmpty(comateUserInfoList)){
                    StringBuffer str = new StringBuffer();
                    int size = comateUserInfoList.size();
                    for (int i = 0; i < size; i++) {
                        OrderListComateInfoVo comateInfoVo = comateUserInfoList.get(i);
                        String realName = comateInfoVo.getRealName();
                        if (Objects.nonNull(realName)){
                            str.append("姓名:" + realName + "  ");
                        }

                        String cardNum = comateInfoVo.getCardNum();
                        if (Objects.nonNull(cardNum)){
                            str.append("身份证号:" + cardNum + "  ");
                        }

                        String phonenumber = comateInfoVo.getPhonenumber();
                        if (Objects.nonNull(phonenumber)){
                            str.append("手机号:" + phonenumber);
                        }

                        if (!Objects.equals(i + 1, size)){
                            str.append("\t\n");
                        }
                    }

                    orderListExportExcel.setComateUserInfo(str.toString());
                }

                orderListExportExcelList.add(orderListExportExcel);
            }
        }

        return orderListExportExcelList;
    }

    /**
     * 小程序-完成出行
     */
    @Override
    public void completeTravelTimeTask(){
        System.out.println("开始进行完成出行任务");
        List<TfpOrder> tfpOrderList = tfpOrderMapper.selectCanCompleteOrderList();
        for (TfpOrder tfpOrder : tfpOrderList) {
            this.completeTravel(null, tfpOrder);
        }
    }

    @Override
    public List<TfpOrder> selectOverRefundTimeCompleteOrderList(){
        return tfpOrderMapper.selectOverRefundTimeCompleteOrderList();
    }

    /**
     * 小程序-完成出行
     * @param id
     * @return
     */
    @Override
    public int completeTravel(Long id, TfpOrder tfpOrder) {
        if (Objects.isNull(id) && Objects.isNull(tfpOrder)){
            return 0;
        }

        if (Objects.nonNull(id)){
            tfpOrder = tfpOrderMapper.selectTfpOrderById(id);
            Integer status = tfpOrder.getStatus();

            if (!Objects.equals(status, OrderStatusEnum.WAIT_TRAVEL.getValue())){
                throw new BusinessException("仅有待出行的订单可以进行完成操作");
            }
        } else {
            id = tfpOrder.getId();
        }

        // 出行完成后，售后时间为30天
        Long overTime = 30 * 24 * 60 * 60 * 1000L;
        Date refundEndTime = new Date(new Date().getTime() + overTime);

        tfpOrder.setStatus(OrderStatusEnum.COMPLETE.getValue());
        tfpOrder.setTravelCompleteTime(DateUtils.getNowDate());
        tfpOrder.setRefundEndTime(refundEndTime);
        tfpOrder.setApplyRefundFlag(YesOrNoFlagEnum.YES.getValue());
        tfpOrder.setUpdateBy("完成订单定时业务");
        tfpOrder.setUpdateTime(DateUtils.getNowDate());
        tfpOrderMapper.updateTfpOrder(tfpOrder);

        Long shareUserId = tfpOrder.getShareUserId();
        if (Objects.nonNull(shareUserId)){
            userIncomeDetailService.updateShareInfo(shareUserId, UserIncomeSourceTypeEnum.PRODUCT.getValue(), tfpOrder.getProductId(), UserIncomeDetailStatusEnum.TRAVEL_COMPLETE.getValue(), id, tfpOrder.getOrderNo(), null);
        }

        return 1;
    }

    /**
     * 后台-订单详情
     * @param id
     * @return
     */
    @Override
    public OrderDetailInfoVo orderDetailInfo(Long id) {
        TfpOrder tfpOrder = tfpOrderMapper.selectTfpOrderById(id);
        if (Objects.isNull(tfpOrder)){
            throw new BusinessException("订单不存在");
        }

        OrderDetailInfoVo orderDetailInfoVo = new OrderDetailInfoVo();
        LoginUser loginUser = SecurityUtils.getLoginUser();
        orderDetailInfoVo.setOperationUserName(loginUser.getUsername());

        Long productId = tfpOrder.getProductId();
        TfpProduct tfpProduct = tfpProductService.selectTfpProductById(productId);

        BeanUtils.copyProperties(tfpProduct, orderDetailInfoVo);
        BeanUtils.copyProperties(tfpOrder, orderDetailInfoVo);

        orderDetailInfoVo.setProductId(productId);

        List<TfpComateOrder> tfpComateOrderList = tfpComateOrderService.selectTfpComateOrderListByOrderId(id);
        if (DcListUtils.isNotEmpty(tfpComateOrderList)){
            List<OrderListComateInfoVo> comateUserInfoList = new ArrayList<>();
            for (TfpComateOrder tfpComateOrder : tfpComateOrderList) {
                OrderListComateInfoVo comateUserInfo = new OrderListComateInfoVo();

                comateUserInfo.setRealName(tfpComateOrder.getMateName());
                comateUserInfo.setCardNum(tfpComateOrder.getMateCardNum());
                comateUserInfo.setPhonenumber(tfpComateOrder.getMatePhone());

                comateUserInfoList.add(comateUserInfo);
            }
            orderDetailInfoVo.setComateInfoList(comateUserInfoList);
        }

        Integer status = tfpOrder.getStatus();
        if (Objects.equals(OrderStatusEnum.APPLY_REFUND_AUDITING.getValue(), status)){
            orderDetailInfoVo.setRefundType(0);
        } else {
            TfpRefund tfpRefund = refundService.selectCompleteTfpRefundByOrderId(id);
            if (Objects.nonNull(tfpRefund)){
                orderDetailInfoVo.setRefundType(tfpRefund.getRefundType());
            } else {
                orderDetailInfoVo.setRefundType(3);
            }
        }

        // 退款信息
        TfpRefund refundParam = new TfpRefund();
        refundParam.setOrderId(id);
        refundParam.setDelFlag(DeleteEnum.EXIST.getValue());
        List<TfpRefund> tfpRefundList = refundService.selectTfpRefundList(refundParam);
        if (DcListUtils.isNotEmpty(tfpRefundList)){
            // 取最新的申请退款信息
            tfpRefundList = tfpRefundList.stream().sorted(Comparator.comparing(TfpRefund::getCreateTime).reversed()).collect(Collectors.toList());
            TfpRefund tfpRefund = tfpRefundList.get(0);

            orderDetailInfoVo.setApplyDate(tfpRefund.getCreateTime());
            orderDetailInfoVo.setReason(tfpRefund.getReason());
            orderDetailInfoVo.setIllustrate(tfpRefund.getIllustrate());
        }

        return orderDetailInfoVo;
    }

    /**
     * 查询完成且超过售后时间的订单列表
     * @return
     * @param shareUserId
     */
    @Override
    public List<TfpOrder> queryOverRefundTimeCompleteOrderList(Long shareUserId) {
        return tfpOrderMapper.queryOverRefundTimeCompleteOrderList(shareUserId);
    }

    private String  createWeChatOrder(String orderNo, BigDecimal finalAmount, Date expireDate){
        // 开始支付
        String description = "商品订单";
        String prepayId = payService.createWeChatOrder(orderNo, finalAmount, description, expireDate);
        return prepayId;
    }

    /**
     * 处理前端支付需要参数
     * @param prepayId
     * @return
     */
    private RequestPaymentVo handleRequestPaymentVo(String prepayId) throws InvalidKeyException, NoSuchAlgorithmException, SignatureException {
        RequestPaymentVo requestPayment = new RequestPaymentVo();

        Long timeStamp = new Date().getTime();
        String timeStampStr = String.valueOf(timeStamp);
        String nonceStr = NonceUtil.createNonce(32);
        String packageStr = "prepay_id=" + prepayId;
        String signType = "RSA";

        String signatureStr = Stream.of(appid, timeStampStr, nonceStr, "prepay_id=" + prepayId)
                .collect(Collectors.joining("\n", "", "\n"));

        PrivateKey merchantPrivateKey = PemUtil.loadPrivateKeyFromPath(privateKeyPath);
        Signature sign = Signature.getInstance("SHA256withRSA");
        sign.initSign(merchantPrivateKey);
        sign.update(signatureStr.getBytes(StandardCharsets.UTF_8));
        String paySign = Base64Utils.encodeToString(sign.sign());

        requestPayment.setTimeStampStr(timeStampStr);
        requestPayment.setNonceStr(nonceStr);
        requestPayment.setPackageStr(packageStr);
        requestPayment.setSignType(signType);
        requestPayment.setPaySign(paySign);
        return requestPayment;
    }

    /**
     * 处理取消订单信息
     * @param tfpOrder
     */
    private void handleCancelOrderInfo(TfpOrder tfpOrder){
        if (Objects.isNull(tfpOrder)){
            throw new RuntimeException("订单不存在");
        }

        Integer status = tfpOrder.getStatus();
        if (!Objects.equals(OrderStatusEnum.WAIT_PAY.getValue(), status)){
            throw new RuntimeException("仅待支付订单可以取消");
        }

        Long userId;
        String userName;

        try {
            LoginUser loginUser = SecurityUtils.getLoginUser();
            userId = loginUser.getUserId();
            userName = loginUser.getUsername();
        } catch (Exception e) {
            userId = tfpOrder.getCreateUserId();
            userName = tfpOrder.getCreateBy();
        }

        tfpOrder.setStatus(OrderStatusEnum.CANCEL.getValue());
        tfpOrder.setUpdateUserId(userId);
        tfpOrder.setUpdateBy(userName);
        tfpOrder.setUpdateTime(DateUtils.getNowDate());
        tfpOrderMapper.updateTfpOrder(tfpOrder);

        TfpProductSnapshot productSnapshot = productSnapshotService.selectTfpProductSnapshotById(tfpOrder.getSnapshotId());
        productSnapshot.setDelFlag(DeleteEnum.DELETE.getValue());
        productSnapshot.setUpdateUserId(userId);
        productSnapshot.setUpdateBy(userName);
        productSnapshot.setUpdateTime(DateUtils.getNowDate());
        productSnapshotService.updateTfpProductSnapshot(productSnapshot);

        userIncomeDetailService.handleUserIncomeAfterCancelOrder(tfpOrder.getOrderNo());
    }

    private void handleOrderList(List<OrderListVo> orderListVoList){
        if (DcListUtils.isNotEmpty(orderListVoList)){
            List<Long> orderIdList = new ArrayList<>();
            List<Long> productIdList = new ArrayList<>();
            List<Long> refundSuccessOrderIdList = new ArrayList<>();
            for (OrderListVo orderListVo : orderListVoList) {
                orderIdList.add(orderListVo.getId());

                Long productId = orderListVo.getProductId();
                if (!productIdList.contains(productId)){
                    productIdList.add(productId);
                }

                Integer status = orderListVo.getStatus();
                if (Objects.equals(OrderStatusEnum.APPLY_REFUND_SUCCESS.getValue(), status)){
                    refundSuccessOrderIdList.add(orderListVo.getId());
                }
            }

            List<TfpProduct> tfpProductList = tfpProductService.selectTfpProductListByIdList(productIdList);

            // 商品信息
            Map<Long, TfpProduct> productMap;
            if (DcListUtils.isNotEmpty(tfpProductList)){
                productMap = tfpProductList.stream().collect(Collectors.toMap(TfpProduct::getId, TfpProduct -> TfpProduct));
            } else {
                productMap = new HashMap<>();
            }

            // 同行人信息
            List<TfpComateOrder> comateOrderList = tfpComateOrderService.selectTfpComateOrderListByOrderIdList(orderIdList);
            Map<Long, List<TfpComateOrder>> comateOrderMap;
            if (DcListUtils.isNotEmpty(tfpProductList)){
                comateOrderMap = comateOrderList.stream().collect(Collectors.groupingBy(TfpComateOrder::getOrderId));
            } else {
                comateOrderMap = new HashMap<>();
            }

            Map<Long, List<TfpRefund>> orderRefundMap = new HashMap<>();
            if (DcListUtils.isNotEmpty(refundSuccessOrderIdList)){
                List<TfpRefund> refundListList = refundService.selectCompleteTfpRefundListByOrderIdList(refundSuccessOrderIdList);
                if (DcListUtils.isNotEmpty(refundListList)){
                    orderRefundMap = refundListList.stream().collect(Collectors.groupingBy(TfpRefund::getOrderId));
                }
            }

            for (OrderListVo orderListVo : orderListVoList) {
                TfpProduct tfpProduct = productMap.get(orderListVo.getProductId());
                orderListVo.setProductName(Objects.nonNull(tfpProduct) ? tfpProduct.getProductName() : null);

                Integer status = orderListVo.getStatus();
                if (Objects.equals(OrderStatusEnum.APPLY_REFUND_AUDITING.getValue(), status)){
                    orderListVo.setRefundType(0);
                } else if (Objects.equals(OrderStatusEnum.APPLY_REFUND_SUCCESS.getValue(), status)){
                    List<TfpRefund> tfpRefundList = orderRefundMap.get(orderListVo.getId());
                    if (DcListUtils.isNotEmpty(tfpRefundList)){
                        tfpRefundList.stream().sorted(Comparator.comparing(TfpRefund::getCreateTime).reversed()).collect(Collectors.toList());
                        TfpRefund tfpRefund = tfpRefundList.get(0);
                        orderListVo.setRefundType(tfpRefund.getRefundType());
                    }
                } else {
                    orderListVo.setRefundType(3);
                }

                List<TfpComateOrder> tfpComateOrderList = comateOrderMap.get(orderListVo.getId());
                if (DcListUtils.isNotEmpty(tfpComateOrderList)){
                    List<OrderListComateInfoVo> comateUserInfoList = new ArrayList<>();
                    for (TfpComateOrder tfpComateOrder : tfpComateOrderList) {
                        OrderListComateInfoVo comateUserInfo = new OrderListComateInfoVo();

                        comateUserInfo.setRealName(tfpComateOrder.getMateName());
                        comateUserInfo.setCardNum(tfpComateOrder.getMateCardNum());
                        comateUserInfo.setPhonenumber(tfpComateOrder.getMatePhone());

                        comateUserInfoList.add(comateUserInfo);
                    }
                    orderListVo.setComateUserInfoList(comateUserInfoList);
                }
            }
        }
    }
}
