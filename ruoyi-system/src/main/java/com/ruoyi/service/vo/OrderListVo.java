package com.ruoyi.service.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ruoyi.common.enuma.OrderStatusEnum;
import com.ruoyi.common.enuma.YesOrNoFlagEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Data
@ApiModel(value = "OrderListVo")
public class OrderListVo {
    @ApiModelProperty("订单id")
    private Long id;
    @ApiModelProperty("订单编号")
    private String orderNo;
    @ApiModelProperty("出发时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date goDate;
    @ApiModelProperty("订单状态 0-待支付 1-已完成 2-待确认(已支付) 3-待出行 4-申请退款-审核中 5-申请退款-退款成功 6-过期 7-取消")
    private Integer status;
    @ApiModelProperty("订单金额")
    private BigDecimal finalAmount;

    @ApiModelProperty("商品名称")
    private String productName;
    @ApiModelProperty("出行人信息")
    private List<OrderListComateInfoVo> comateUserInfoList;

    @ApiModelProperty("支付时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date payTime;
    @ApiModelProperty("支付状态 0-未支付 1-已支付")
    private Integer payStatus;
    @ApiModelProperty("退款类型 0-退款待审核 1-整单退 2-部分退 3-否")
    private Integer refundType;

    @JsonIgnore
    private Long productId;

    public Integer getPayStatus(){
        List<Integer> payStatusYesList = new ArrayList<>();
        payStatusYesList.add(OrderStatusEnum.COMPLETE.getValue());
        payStatusYesList.add(OrderStatusEnum.WAIT_CONFIRMED.getValue());
        payStatusYesList.add(OrderStatusEnum.WAIT_TRAVEL.getValue());
        payStatusYesList.add(OrderStatusEnum.APPLY_REFUND_AUDITING.getValue());
        payStatusYesList.add(OrderStatusEnum.APPLY_REFUND_SUCCESS.getValue());

        if (payStatusYesList.contains(this.status)){
            return YesOrNoFlagEnum.YES.getValue();
        } else {
            return YesOrNoFlagEnum.NO.getValue();
        }
    }
}
