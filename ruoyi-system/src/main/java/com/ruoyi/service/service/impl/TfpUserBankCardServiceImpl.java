package com.ruoyi.service.service.impl;

import java.util.List;
import java.util.Objects;

import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enuma.DeleteEnum;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.service.dto.SaveUserBankCardInfoDto;
import com.ruoyi.service.vo.DetailUserBankCardVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpUserBankCardMapper;
import com.ruoyi.service.domain.TfpUserBankCard;
import com.ruoyi.service.service.ITfpUserBankCardService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 用户银行卡Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@Service
@Transactional
public class TfpUserBankCardServiceImpl implements ITfpUserBankCardService 
{
    @Autowired
    private TfpUserBankCardMapper tfpUserBankCardMapper;

    /**
     * 查询用户银行卡
     * 
     * @param id 用户银行卡主键
     * @return 用户银行卡
     */
    @Override
    public TfpUserBankCard selectTfpUserBankCardById(Long id)
    {
        return tfpUserBankCardMapper.selectTfpUserBankCardById(id);
    }

    /**
     * 查询用户银行卡列表
     * 
     * @param tfpUserBankCard 用户银行卡
     * @return 用户银行卡
     */
    @Override
    public List<TfpUserBankCard> selectTfpUserBankCardList(TfpUserBankCard tfpUserBankCard)
    {
        return tfpUserBankCardMapper.selectTfpUserBankCardList(tfpUserBankCard);
    }

    /**
     * 新增用户银行卡
     * 
     * @param tfpUserBankCard 用户银行卡
     * @return 结果
     */
    @Override
    public int insertTfpUserBankCard(TfpUserBankCard tfpUserBankCard)
    {
        tfpUserBankCard.setCreateTime(DateUtils.getNowDate());
        return tfpUserBankCardMapper.insertTfpUserBankCard(tfpUserBankCard);
    }

    /**
     * 修改用户银行卡
     * 
     * @param tfpUserBankCard 用户银行卡
     * @return 结果
     */
    @Override
    public int updateTfpUserBankCard(TfpUserBankCard tfpUserBankCard)
    {
        tfpUserBankCard.setUpdateTime(DateUtils.getNowDate());
        return tfpUserBankCardMapper.updateTfpUserBankCard(tfpUserBankCard);
    }

    /**
     * 批量删除用户银行卡
     * 
     * @param ids 需要删除的用户银行卡主键
     * @return 结果
     */
    @Override
    public int deleteTfpUserBankCardByIds(Long[] ids)
    {
        return tfpUserBankCardMapper.deleteTfpUserBankCardByIds(ids);
    }

    /**
     * 删除用户银行卡信息
     * 
     * @param id 用户银行卡主键
     * @return 结果
     */
    @Override
    public int deleteTfpUserBankCardById(Long id)
    {
        return tfpUserBankCardMapper.deleteTfpUserBankCardById(id);
    }

    /**
     * 小程序-保存用户银行卡信息(新增/编辑)
     * @param saveUserBankCardInfoDto
     * @return
     */
    @Override
    public int saveUserBankCardInfo(SaveUserBankCardInfoDto saveUserBankCardInfoDto) {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = loginUser.getUserId();
        TfpUserBankCard userBankCard = tfpUserBankCardMapper.getUserBankCardByUserId(userId);

        boolean insertFlag = true;
        if (Objects.isNull(userBankCard)){
            userBankCard = new TfpUserBankCard();
        } else {
            insertFlag = false;
        }

        BeanUtils.copyProperties(saveUserBankCardInfoDto, userBankCard);

        if (insertFlag){
            userBankCard.setUserId(userId);
            userBankCard.setDelFlag(DeleteEnum.EXIST.getValue());
            userBankCard.setCreateUserId(userId);
            userBankCard.setCreateBy(loginUser.getUsername());
            userBankCard.setCreateTime(DateUtils.getNowDate());
            tfpUserBankCardMapper.insertTfpUserBankCard(userBankCard);
        } else {
            userBankCard.setUpdateUserId(userId);
            userBankCard.setUpdateBy(loginUser.getUsername());
            userBankCard.setUpdateTime(DateUtils.getNowDate());
            tfpUserBankCardMapper.updateTfpUserBankCard(userBankCard);
        }

        return 1;
    }

    /**
     * 小程序-用户银行卡详情
     * @return
     */
    @Override
    public DetailUserBankCardVo detailUserBankCard() {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = loginUser.getUserId();
        TfpUserBankCard userBankCard = tfpUserBankCardMapper.getUserBankCardByUserId(userId);
        if (Objects.isNull(userBankCard)){
            throw new RuntimeException("用户银行卡信息未查到");
        }

        DetailUserBankCardVo detailUserBankCardVo = new DetailUserBankCardVo();
        BeanUtils.copyProperties(userBankCard, detailUserBankCardVo);
        return detailUserBankCardVo;
    }

    @Override
    public boolean existsUserBankCardFlag(Long userId) {
        TfpUserBankCard userBankCard = tfpUserBankCardMapper.getUserBankCardByUserId(userId);
        return Objects.nonNull(userBankCard) ? true : false;
    }

    @Override
    public Boolean existsUserBankCardFlag() {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = loginUser.getUserId();
        return this.existsUserBankCardFlag(userId);
    }

    @Override
    public TfpUserBankCard getMyUserBankCardInfo(){
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = loginUser.getUserId();
        return tfpUserBankCardMapper.getUserBankCardByUserId(userId);
    }

    @Override
    public List<TfpUserBankCard> selectTfpUserBankCardListByIdList(List<Long> idList) {
        return tfpUserBankCardMapper.selectTfpUserBankCardListByIdList(idList);
    }
}
