package com.ruoyi.service.service.impl;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enuma.ChatUserTypeEnum;
import com.ruoyi.common.enuma.DeleteEnum;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.bean.BeanUtils;
import com.ruoyi.service.domain.*;
import com.ruoyi.service.dto.ChatGroupPersonInfoDto;
import com.ruoyi.service.dto.TfpChatGroupPersonInfoDto;
import com.ruoyi.service.service.ITfpChatGroupPersonService;
import com.ruoyi.service.service.ITfpChatGroupService;
import com.ruoyi.service.vo.TfpChatGroupPersonInfoVo;
import com.ruoyi.service.vo.UserVo;
import com.ruoyi.system.service.ISysUserService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.compress.utils.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpChatGroupPersonInfoMapper;
import com.ruoyi.service.service.ITfpChatGroupPersonInfoService;
import org.springframework.transaction.annotation.Transactional;

import static com.ruoyi.common.utils.PageUtils.startPage;
import static com.ruoyi.common.utils.SecurityUtils.getLoginUser;

/**
 * 聊天群组成员聊天信息Service业务层处理
 *
 * @author ruoyi
 * @date 2023-10-31
 */
@Service
@Transactional
public class TfpChatGroupPersonInfoServiceImpl implements ITfpChatGroupPersonInfoService
{
    @Autowired
    private TfpChatGroupPersonInfoMapper tfpChatGroupPersonInfoMapper;
    @Autowired
    private ISysUserService userService;
    @Autowired
    private ITfpChatGroupService tfpChatGroupService;
    @Autowired
    private ITfpChatGroupPersonService tfpChatGroupPersonService;

    /**
     * 查询聊天群组成员聊天信息
     *
     * @param id 聊天群组成员聊天信息主键
     * @return 聊天群组成员聊天信息
     */
    @Override
    public TfpChatGroupPersonInfo selectTfpChatGroupPersonInfoById(Long id)
    {
        return tfpChatGroupPersonInfoMapper.selectTfpChatGroupPersonInfoById(id);
    }

    /**
     * 查询聊天群组成员聊天信息列表
     *
     * @param tfpChatGroupPersonInfo 聊天群组成员聊天信息
     * @return 聊天群组成员聊天信息
     */
    @Override
    public List<TfpChatGroupPersonInfo> selectTfpChatGroupPersonInfoList(TfpChatGroupPersonInfo tfpChatGroupPersonInfo)
    {
        return tfpChatGroupPersonInfoMapper.selectTfpChatGroupPersonInfoList(tfpChatGroupPersonInfo);
    }

    @Override
    public List<TfpChatGroupPersonInfoVo> selectTfpChatGroupPersonInfoVoList(TfpChatGroupPersonInfoDto tfpChatGroupPersonInfoDto) {
        String chatGroupName = tfpChatGroupPersonInfoDto.getChatGroupName();
        if (chatGroupName != null && !chatGroupName.trim().equals("")){
            tfpChatGroupPersonInfoDto.setChatGroupNameFlag(true);

            TfpChatGroup tfpChatGroup = new TfpChatGroup();
            tfpChatGroup.setName(chatGroupName);
            List<TfpChatGroup> tfpChatGroupList = tfpChatGroupService.selectTfpChatGroupList(tfpChatGroup);
            if (tfpChatGroupList == null || tfpChatGroupList.size() == 0){
                return new ArrayList<>();
            }

            List<Long> chatGroupIdList = tfpChatGroupList.stream().map(TfpChatGroup::getId).collect(Collectors.toList());
            tfpChatGroupPersonInfoDto.setChatGroupIdList(chatGroupIdList);
        }

        if (tfpChatGroupPersonInfoDto.getUserId() != null){
            tfpChatGroupPersonInfoDto.setUserIdFlag(true);

            List<Long> queryChatGroupPersonIdList = tfpChatGroupPersonService.selectChatGroupPersonIdByUserId(tfpChatGroupPersonInfoDto.getUserId());
            tfpChatGroupPersonInfoDto.setChatGroupPersonIdList(queryChatGroupPersonIdList);
        }

        startPage();

        //上边是条件传值
        List<TfpChatGroupPersonInfoVo> tfpChatGroupPersonInfoVoList = tfpChatGroupPersonInfoMapper.selectTfpChatGroupPersonInfoVoList(tfpChatGroupPersonInfoDto);
        if (tfpChatGroupPersonInfoVoList != null && tfpChatGroupPersonInfoVoList.size() > 0){
            List<Long> chatGroupIdList = new ArrayList<>();
            List<Long> chatGroupPersonIdList = new ArrayList<>();
            for (TfpChatGroupPersonInfoVo tfpChatGroupPersonInfoVo : tfpChatGroupPersonInfoVoList) {
                chatGroupIdList.add(tfpChatGroupPersonInfoVo.getChatGroupId());
                chatGroupPersonIdList.add(tfpChatGroupPersonInfoVo.getChatGroupPersonId());
            }

            Map<Long, String> chatGroupNameMap = new HashMap<>();
            if (chatGroupIdList.size() > 0){
                List<TfpChatGroup> tfpChatGroupList = tfpChatGroupService.selectTfpChatGroupListByIdList(chatGroupIdList);
                if (tfpChatGroupList != null && tfpChatGroupList.size() > 0){
                    chatGroupNameMap = tfpChatGroupList.stream().distinct().collect(Collectors.toMap(TfpChatGroup::getId, TfpChatGroup::getName));
                }
            }

            Map<Long, String> nickNameMap = new HashMap<>();
            Map<Long, Long> personMap = new HashMap<>();
            if (chatGroupPersonIdList.size() > 0){
                List<TfpChatGroupPerson> tfpChatGroupPersonList = tfpChatGroupPersonService.selectTfpChatGroupPersonByIdList(chatGroupPersonIdList);
                if (tfpChatGroupPersonList != null && tfpChatGroupPersonList.size() > 0){
                    List<Long> userIdList = new ArrayList<>();
                    for (TfpChatGroupPerson tfpChatGroupPerson : tfpChatGroupPersonList) {
                        Long userId = tfpChatGroupPerson.getUserId();
                        personMap.put(tfpChatGroupPerson.getId(), userId);
                        if (!userIdList.contains(userId)){
                            userIdList.add(userId);
                        }
                    }

                    if (userIdList != null && userIdList.size() > 0){
                        nickNameMap = userService.getNickNameMap(userIdList);
                    }
                }
            }

            for (TfpChatGroupPersonInfoVo tfpChatGroupPersonInfoVo : tfpChatGroupPersonInfoVoList) {
                tfpChatGroupPersonInfoVo.setChatGroupName(chatGroupNameMap.get(tfpChatGroupPersonInfoVo.getChatGroupId()));

                Long chatGroupPersonId = tfpChatGroupPersonInfoVo.getChatGroupPersonId();
                Long userId = personMap.get(chatGroupPersonId);
                if (userId != null){
                    tfpChatGroupPersonInfoVo.setChatGroupPersonName(nickNameMap.get(userId));
                }
            }
        }

        return tfpChatGroupPersonInfoVoList;
    }

    /**
     * 小程序获取聊天信息
     * @param tfpChatGroupPersonInfoDto
     * @return
     */
    public List<TfpChatGroupPersonInfoVo> selectAppletInfoVoList(TfpChatGroupPersonInfoDto tfpChatGroupPersonInfoDto) {
        //获取聊天数据
        List<TfpChatGroupPersonInfoVo> tfpChatGroupPersonInfoVoList = tfpChatGroupPersonInfoMapper.selectAppletInfoList(tfpChatGroupPersonInfoDto);
        if(CollectionUtils.isEmpty(tfpChatGroupPersonInfoVoList)){
            return Lists.newArrayList();
        }
        Long userId = SecurityUtils.getUserId();
        //获取聊天群组的创建者信息
        Optional<TfpChatGroupPersonInfoVo> first = tfpChatGroupPersonInfoVoList.stream().findFirst();
        if(!first.isPresent()){
            throw new ServiceException("未获取到群组聊天的信息");
        }
        TfpChatGroupPersonInfoVo groupPersonInfoInfo = first.get();
        TfpChatGroup tfpChatGroup = tfpChatGroupService.selectTfpChatGroupById(groupPersonInfoInfo.getChatGroupId());
        if(Objects.isNull(tfpChatGroup)){
            throw new ServiceException("未获取到群组信息");
        }
        //拿到了群组中的创建人，也就是聊天的发起人的id
        Long groupCreateUserId = tfpChatGroup.getCreateUserId();

        //获取群组中的人
        List<Long> groupIds = Lists.newArrayList();
        groupIds.add(tfpChatGroup.getId());
        List<TfpChatGroupPerson> tfpChatGroupPeople = tfpChatGroupPersonService.selectTfpChatGroupPersonListByGroupIdList(groupIds);
        Map<Long, TfpChatGroupPerson> userTypeMap = new HashMap<>();
        if(CollectionUtils.isNotEmpty(tfpChatGroupPeople)){
            userTypeMap = tfpChatGroupPeople.stream().collect(Collectors.toMap(TfpChatGroupPerson::getUserId, Function.identity(), (k1, k2) -> k1));

        }

        //获取所有用户id，去拿头像
        List<Long> userIds = tfpChatGroupPersonInfoVoList.stream().map(TfpChatGroupPersonInfoVo::getCreateUserId).collect(Collectors.toList());
        Map<Long, SysUser> userInfoAndAvatar = userService.getUserInfoAndAvatar(userIds);
        //循环处理信息
        if(CollectionUtils.isNotEmpty(tfpChatGroupPersonInfoVoList)){
            //排序，让前端好处理数据
            tfpChatGroupPersonInfoVoList = tfpChatGroupPersonInfoVoList.stream().sorted(Comparator.comparing(TfpChatGroupPersonInfoVo::getId)).collect(Collectors.toList());
            for (TfpChatGroupPersonInfoVo info : tfpChatGroupPersonInfoVoList) {
                Long createChatInfoUserId = info.getCreateUserId();
                //判断是不是自己
                if(info.getCreateUserId().equals(userId)){
                    info.setSelfFlag(Boolean.TRUE);
                }
                //获取头像
                SysUser sysUser = userInfoAndAvatar.get(createChatInfoUserId);
                info.setAvatar(sysUser.getAvatar());
                //判断是否是发起者,还是已报名，或者是普通垃圾用户
//                if(groupCreateUserId.equals(createChatInfoUserId)){
//                    info.setIdentity(ChatIdentityEnum.CREATER.getValue());
//                }
                TfpChatGroupPerson person = userTypeMap.get(createChatInfoUserId);
                if(Objects.isNull(person)||Objects.isNull(person.getUserType())){
                    info.setIdentity(ChatUserTypeEnum.UN_APPLIED.getValue());
                }else {
                    info.setIdentity(person.getUserType());
                }
                if(Objects.nonNull(sysUser)){
                    UserVo userVo = new UserVo();
                    userVo.setId(sysUser.getUserId());
                    userVo.setName(sysUser.getNickName());
                    if(Objects.nonNull(sysUser.getAttachment())){
                        userVo.setAvatar(sysUser.getAttachment().getUrl());
                    }
                    info.setUserVo(userVo);

                }

            }
        }
        return tfpChatGroupPersonInfoVoList;
    }

    /**
     * 新增聊天群组成员聊天信息
     *
     * @param tfpChatGroupPersonInfo 聊天群组成员聊天信息
     * @return 结果
     */
    @Override
    public int insertTfpChatGroupPersonInfo(TfpChatGroupPersonInfo tfpChatGroupPersonInfo)
    {
        tfpChatGroupPersonInfo.setCreateTime(DateUtils.getNowDate());
        return tfpChatGroupPersonInfoMapper.insertTfpChatGroupPersonInfo(tfpChatGroupPersonInfo);
    }

    /**
     * 修改聊天群组成员聊天信息
     *
     * @param tfpChatGroupPersonInfo 聊天群组成员聊天信息
     * @return 结果
     */
    @Override
    public int updateTfpChatGroupPersonInfo(TfpChatGroupPersonInfo tfpChatGroupPersonInfo)
    {
        tfpChatGroupPersonInfo.setUpdateTime(DateUtils.getNowDate());
        return tfpChatGroupPersonInfoMapper.updateTfpChatGroupPersonInfo(tfpChatGroupPersonInfo);
    }

    /**
     * 批量删除聊天群组成员聊天信息
     *
     * @param ids 需要删除的聊天群组成员聊天信息主键
     * @return 结果
     */
    @Override
    public int deleteTfpChatGroupPersonInfoByIds(Long[] ids)
    {
        return tfpChatGroupPersonInfoMapper.deleteTfpChatGroupPersonInfoByIds(ids);
    }

    /**
     * 删除聊天群组成员聊天信息信息
     *
     * @param id 聊天群组成员聊天信息主键
     * @return 结果
     */
    @Override
    public int deleteTfpChatGroupPersonInfoById(Long id)
    {
        return tfpChatGroupPersonInfoMapper.deleteTfpChatGroupPersonInfoById(id);
    }

    @Override
    public List<UserVo> chatGroupPersonSelect() {
        List<UserVo> userVoList = new ArrayList<>();
        List<Long> chatGroupPersonIdList = tfpChatGroupPersonInfoMapper.getChatGroupPersonUserIdList();
        if (chatGroupPersonIdList != null && chatGroupPersonIdList.size() > 0){
            List<TfpChatGroupPerson> tfpChatGroupPersonList = tfpChatGroupPersonService.selectTfpChatGroupPersonByIdList(chatGroupPersonIdList);
            if (tfpChatGroupPersonList != null && tfpChatGroupPersonList.size() > 0){
                List<Long> userIdList = tfpChatGroupPersonList.stream().map(TfpChatGroupPerson::getUserId).collect(Collectors.toList());
                userVoList = userService.handleUserVo(userIdList);
            }
        }
        return userVoList;
    }

    @Override
    public int appletChatInfoAdd(ChatGroupPersonInfoDto chatGroupPersonInfoDto) {
        LoginUser loginUser = getLoginUser();
        Long userId = loginUser.getUserId();
        Long chatGroupId = chatGroupPersonInfoDto.getChatGroupId();

        TfpChatGroupPerson chatGroupPerson = tfpChatGroupPersonService.selectTfpChatGroupPersonByGroupIdAndUserId(chatGroupId, userId);
        if (Objects.isNull(chatGroupPerson)){
            TfpChatGroup tfpChatGroup = tfpChatGroupService.selectTfpChatGroupById(chatGroupId);

            chatGroupPerson = new TfpChatGroupPerson();
            chatGroupPerson.setChatGroupId(chatGroupId);
            chatGroupPerson.setProductId(tfpChatGroup.getProductId());
            chatGroupPerson.setUserId(userId);
            chatGroupPerson.setUserType(ChatUserTypeEnum.UN_APPLIED.getValue());
            chatGroupPerson.setDelFlag(DeleteEnum.EXIST.getValue());
            chatGroupPerson.setCreateUserId(loginUser.getUserId());
            chatGroupPerson.setCreateBy(loginUser.getUsername());
            chatGroupPerson.setCreateTime(DateUtils.getNowDate());
            tfpChatGroupPersonService.insertTfpChatGroupPerson(chatGroupPerson);
        }

        TfpChatGroupPersonInfo tfpChatGroupPersonInfo = new TfpChatGroupPersonInfo();
        BeanUtils.copyProperties(chatGroupPersonInfoDto, tfpChatGroupPersonInfo);
        //处理必填项
        tfpChatGroupPersonInfo.setChatGroupPersonId(chatGroupPerson.getId());
        tfpChatGroupPersonInfo.setCreateUserId(loginUser.getUserId());
        tfpChatGroupPersonInfo.setUpdateUserId(loginUser.getUserId());
        tfpChatGroupPersonInfo.setCreateTime(DateUtils.getNowDate());
        tfpChatGroupPersonInfo.setUpdateTime(DateUtils.getNowDate());
        tfpChatGroupPersonInfo.setDelFlag(DeleteEnum.EXIST.getValue());
        tfpChatGroupPersonInfo.setCreateBy(loginUser.getUsername());
        return tfpChatGroupPersonInfoMapper.insertTfpChatGroupPersonInfo(tfpChatGroupPersonInfo);
    }
}
