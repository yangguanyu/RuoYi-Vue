package com.ruoyi.service.service.impl;

import java.util.List;

import com.ruoyi.common.utils.DcListUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.AirportCodesMapper;
import com.ruoyi.service.domain.AirportCodes;
import com.ruoyi.service.service.IAirportCodesService;


import org.springframework.transaction.annotation.Transactional;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-28
 */
@Service
@Transactional
public class AirportCodesServiceImpl implements IAirportCodesService 
{
    @Autowired
    private AirportCodesMapper airportCodesMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public AirportCodes selectAirportCodesById(Long id)
    {
        return airportCodesMapper.selectAirportCodesById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param airportCodes 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<AirportCodes> selectAirportCodesList(AirportCodes airportCodes)
    {
        return airportCodesMapper.selectAirportCodesList(airportCodes);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param airportCodes 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertAirportCodes(AirportCodes airportCodes)
    {
        return airportCodesMapper.insertAirportCodes(airportCodes);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param airportCodes 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateAirportCodes(AirportCodes airportCodes)
    {
        return airportCodesMapper.updateAirportCodes(airportCodes);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteAirportCodesByIds(Long[] ids)
    {
        return airportCodesMapper.deleteAirportCodesByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteAirportCodesById(Long id)
    {
        return airportCodesMapper.deleteAirportCodesById(id);
    }

    @Override
    public String getThreeCode(String cityName) {
        String city = cityName.endsWith("市") ? cityName.substring(0, cityName.length() - 1) : cityName;

        AirportCodes param = new AirportCodes();
        param.setCity(city);
        List<AirportCodes> airportCodesList = airportCodesMapper.selectAirportCodesList(param);
        if (DcListUtils.isNotEmpty(airportCodesList)){
            return airportCodesList.get(0).getCode();
        } else {
            return null;
        }
    }
}
