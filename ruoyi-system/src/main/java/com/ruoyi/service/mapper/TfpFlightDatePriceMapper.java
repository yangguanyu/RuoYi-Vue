package com.ruoyi.service.mapper;

import java.util.Date;
import java.util.List;
import com.ruoyi.service.domain.TfpFlightDatePrice;
import org.apache.ibatis.annotations.Param;

/**
 * 航班日期价格Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-29
 */
public interface TfpFlightDatePriceMapper 
{
    /**
     * 查询航班日期价格
     * 
     * @param id 航班日期价格主键
     * @return 航班日期价格
     */
    public TfpFlightDatePrice selectTfpFlightDatePriceById(Long id);

    /**
     * 查询航班日期价格列表
     * 
     * @param tfpFlightDatePrice 航班日期价格
     * @return 航班日期价格集合
     */
    public List<TfpFlightDatePrice> selectTfpFlightDatePriceList(TfpFlightDatePrice tfpFlightDatePrice);

    /**
     * 新增航班日期价格
     * 
     * @param tfpFlightDatePrice 航班日期价格
     * @return 结果
     */
    public int insertTfpFlightDatePrice(TfpFlightDatePrice tfpFlightDatePrice);

    /**
     * 修改航班日期价格
     * 
     * @param tfpFlightDatePrice 航班日期价格
     * @return 结果
     */
    public int updateTfpFlightDatePrice(TfpFlightDatePrice tfpFlightDatePrice);

    /**
     * 删除航班日期价格
     * 
     * @param id 航班日期价格主键
     * @return 结果
     */
    public int deleteTfpFlightDatePriceById(Long id);

    /**
     * 批量删除航班日期价格
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpFlightDatePriceByIds(Long[] ids);

    TfpFlightDatePrice selectTfpFlightDatePriceByTimeAndName(@Param("departureTime") String departureTime, @Param("departureAirportName") String departureAirportName, @Param("arrivalAirportName") String arrivalAirportName);

    List<TfpFlightDatePrice> getTfpFlightDatePriceListByGoAndBackPlaceAndDateList(@Param("departureTimeList") List<String> departureTimeList, @Param("departureAirportName") String departureAirportName, @Param("arrivalAirportName") String arrivalAirportName);
}
