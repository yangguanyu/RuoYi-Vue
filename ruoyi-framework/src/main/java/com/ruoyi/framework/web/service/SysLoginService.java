package com.ruoyi.framework.web.service;

import javax.annotation.Resource;

import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.common.core.domain.entity.SysUserWx;
import com.ruoyi.common.core.domain.wx.WxLoginBody;
import com.ruoyi.common.core.domain.wx.WxLoginResultDTO;
import com.ruoyi.common.core.domain.wx.WxLoginResultEntity;
import com.ruoyi.common.enuma.BusinessSubTypeEnum;
import com.ruoyi.common.enuma.BusinessTypeEnum;
import com.ruoyi.common.enuma.DeleteEnum;
import com.ruoyi.common.enuma.YesOrNoFlagEnum;
import com.ruoyi.common.utils.DcUrlUtils;
import com.ruoyi.common.utils.http.HttpUtils;
import com.ruoyi.common.utils.uuid.UUID;
import com.ruoyi.service.domain.AccessTokenEntity;
import com.ruoyi.common.core.domain.entity.TfpAttachment;
import com.ruoyi.service.service.ISysIdentityService;
import com.ruoyi.service.service.ITfpAttachmentService;
import com.ruoyi.service.service.ITfpUserIncomeService;
import com.ruoyi.service.vo.WxGetPhoneNumberVo;
import com.ruoyi.system.domain.SysUserRole;
import com.ruoyi.system.mapper.SysUserMapper;
import com.ruoyi.system.mapper.SysUserRoleMapper;
import com.ruoyi.system.mapper.SysUserWxMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import com.ruoyi.common.constant.CacheConstants;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.exception.user.BlackListException;
import com.ruoyi.common.exception.user.CaptchaException;
import com.ruoyi.common.exception.user.CaptchaExpireException;
import com.ruoyi.common.exception.user.UserNotExistsException;
import com.ruoyi.common.exception.user.UserPasswordNotMatchException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.MessageUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.ip.IpUtils;
import com.ruoyi.framework.manager.AsyncManager;
import com.ruoyi.framework.manager.factory.AsyncFactory;
import com.ruoyi.framework.security.context.AuthenticationContextHolder;
import com.ruoyi.system.service.ISysConfigService;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.concurrent.TimeUnit;

import static java.lang.String.format;
import static java.util.Objects.nonNull;
import static java.util.concurrent.TimeUnit.HOURS;

/**
 * 登录校验方法
 *
 * @author ruoyi
 */
@Component
@Slf4j
public class SysLoginService
{
    @Autowired
    private TokenService tokenService;

    @Resource
    private AuthenticationManager authenticationManager;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private ISysUserService userService;

    @Autowired
    private ISysConfigService configService;

    @Autowired
    private SysUserWxMapper sysUserWxMapper;

    @Autowired
    private SysUserMapper sysUserMapper;
    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private RedisTemplate<String, String> redisTemplate;
    @Autowired
    private ITfpAttachmentService attachmentMapper;

    @Autowired
    private ITfpUserIncomeService userIncomeService;
    @Autowired
    private ISysIdentityService identityService;

    @Value("${default.avatar}")
    private String defaultAvatar;

    /**
     * 过期时间(s)
     */
    public static final Integer CAPTCHA_EXPIRE_TIME = 7000;
    /**
     * 获取小程序token的key
     */
    public static final String CAPTCHA_TOKEN = "App:accessToken";

    @Value("${appid}")
    private String appid;
    @Value("${secret}")
    private String secret;

    @Value("${default.dept.id}")
    private Long defaultDeptId;
    @Value("${default.role.id}")
    private Long defaultRoleId;

    private static String WX_USER_SESSION_KEY = "wx:user:session";

    /**
     * 登录验证
     *
     * @param username 用户名
     * @param password 密码
     * @param code 验证码
     * @param uuid 唯一标识
     * @return 结果
     */
    public String login(String username, String password, String code, String uuid)
    {
        // 验证码校验
        validateCaptcha(username, code, uuid);
        // 登录前置校验
        loginPreCheck(username, password);
        // 用户验证
        Authentication authentication = null;
        try
        {
            UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username, password);
            AuthenticationContextHolder.setContext(authenticationToken);
            // 该方法会去调用UserDetailsServiceImpl.loadUserByUsername
            authentication = authenticationManager.authenticate(authenticationToken);
        }
        catch (Exception e)
        {
            if (e instanceof BadCredentialsException)
            {
                AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.password.not.match")));
                throw new UserPasswordNotMatchException();
            }
            else
            {
                AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, e.getMessage()));
                throw new ServiceException(e.getMessage());
            }
        }
        finally
        {
            AuthenticationContextHolder.clearContext();
        }
        AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_SUCCESS, MessageUtils.message("user.login.success")));
        LoginUser loginUser = (LoginUser) authentication.getPrincipal();
        recordLoginInfo(loginUser.getUserId());
        // 生成token
        return tokenService.createToken(loginUser);
    }

    /**
     * 校验验证码
     *
     * @param username 用户名
     * @param code 验证码
     * @param uuid 唯一标识
     * @return 结果
     */
    public void validateCaptcha(String username, String code, String uuid)
    {
        boolean captchaEnabled = configService.selectCaptchaEnabled();
        if (captchaEnabled)
        {
            String verifyKey = CacheConstants.CAPTCHA_CODE_KEY + StringUtils.nvl(uuid, "");
            String captcha = redisCache.getCacheObject(verifyKey);
            redisCache.deleteObject(verifyKey);
            if (captcha == null)
            {
                AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.jcaptcha.expire")));
                throw new CaptchaExpireException();
            }
            if (!code.equalsIgnoreCase(captcha))
            {
                AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.jcaptcha.error")));
                throw new CaptchaException();
            }
        }
    }

    /**
     * 登录前置校验
     * @param username 用户名
     * @param password 用户密码
     */
    public void loginPreCheck(String username, String password)
    {
        // 用户名或密码为空 错误
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password))
        {
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("not.null")));
            throw new UserNotExistsException();
        }
        // 密码如果不在指定范围内 错误
        if (password.length() < UserConstants.PASSWORD_MIN_LENGTH
                || password.length() > UserConstants.PASSWORD_MAX_LENGTH)
        {
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.password.not.match")));
            throw new UserPasswordNotMatchException();
        }
        // 用户名不在指定范围内 错误
        if (username.length() < UserConstants.USERNAME_MIN_LENGTH
                || username.length() > UserConstants.USERNAME_MAX_LENGTH)
        {
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.password.not.match")));
            throw new UserPasswordNotMatchException();
        }
        // IP黑名单校验
        String blackStr = configService.selectConfigByKey("sys.login.blackIPList");
        if (IpUtils.isMatchedIp(blackStr, IpUtils.getIpAddr()))
        {
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("login.blocked")));
            throw new BlackListException();
        }
    }

    /**
     * 记录登录信息
     *
     * @param userId 用户ID
     */
    public void recordLoginInfo(Long userId)
    {
        SysUser sysUser = new SysUser();
        sysUser.setUserId(userId);
        sysUser.setLoginIp(IpUtils.getIpAddr());
        sysUser.setLoginDate(DateUtils.getNowDate());
        userService.updateUserProfile(sysUser);
    }

    /**
     * 微信小程序登录
     * @param wxLoginBody
     */
    public WxLoginResultEntity wxLogin(WxLoginBody wxLoginBody) {
        // 成功后处理用户信息
        String code = wxLoginBody.getCode();

        // 获取authToken
        String url = "https://api.weixin.qq.com/sns/jscode2session";
        Map<String, String> paramMap = new HashMap();
        // 小程序 appId
        paramMap.put("appid", appid);
        // 小程序 appSecret
        paramMap.put("secret", secret);
        paramMap.put("js_code", code);
        paramMap.put("grant_type", "authorization_code");
        String paramStr = HttpUtils.handleGetParam(paramMap);
        String response = HttpUtils.sendGet(url, paramStr);
        log.info("微信小程序登录返回结果: " + response);

        WxLoginResultDTO wxLoginResult = JSONObject.parseObject(response, WxLoginResultDTO.class);
        if (nonNull(wxLoginResult) && nonNull(wxLoginResult.getErrcode()) && !Objects.equals(wxLoginResult.getErrcode(), 0)) {
            throw new RuntimeException("微信登录失败: " + wxLoginResult.getErrmsg());
        }

        SysUser user;

        String openid = wxLoginResult.getOpenid();
        SysUserWx sysUserWx = sysUserWxMapper.selectSysUserWxByOpenId(openid);
        if (Objects.isNull(sysUserWx)){
            // 第一次登录，进行用户注册
            String phonenumber = this.getWxPhone(wxLoginBody.getPhoneCode());
            user = this.initializationWxUserInfo(phonenumber, wxLoginResult);
        } else {
            String unionid = wxLoginResult.getUnionid();
            if (Objects.nonNull(unionid)){
                sysUserWx.setUnionId(unionid);
            }

            sysUserWx.setUpdateTime(new Date());
            sysUserWx.setUpdateUserId(sysUserWx.getUserId());
            sysUserWxMapper.updateSysUserWx(sysUserWx);

            user = sysUserMapper.selectUserById(sysUserWx.getUserId());
        }

        // 登录成功后记录信息
        SysUser updateSysUser = new SysUser();
        updateSysUser.setUserId(user.getUserId());
        updateSysUser.setLoginIp(IpUtils.getIpAddr());
        updateSysUser.setLoginDate(DateUtils.getNowDate());
        sysUserMapper.updateUser(user);

        // 生成令牌
        LoginUser loginUser = new LoginUser();
        loginUser.setUserId(user.getUserId());
        loginUser.setDeptId(defaultDeptId);
        loginUser.setUser(user);
        loginUser.setIdentityId(user.getIdentityId());
        String token = tokenService.createToken(loginUser);

        log.info("生成令牌:" + token);

        // 存redis
        stringRedisTemplate.opsForValue().set(format(WX_USER_SESSION_KEY, token), JSONObject.toJSONString(wxLoginResult), 2, HOURS);
        return buildWxLoginResultEntity(token);
    }

    /**
     * 初次登录获取手机号
     * @param phoneCode
     * @return
     */
    private String getWxPhone(String phoneCode){
        // 获取手机号
        Map<String, String> paramMap = new HashMap();

        //首先获取ACCESS_TOKEN
        String access_token = getAccess_token(appid, secret, CAPTCHA_TOKEN);
        if (StringUtils.isEmpty(access_token)){
            throw new RuntimeException("请稍后重试！！");
        }

        String getPhoneNumberUrl = "https://api.weixin.qq.com/wxa/business/getuserphonenumber";
        // 小程序 code
        paramMap.put("code", phoneCode);
        // 小程序 access_token
        getPhoneNumberUrl += "?access_token=" + access_token;
        String response = HttpUtils.sendPost(getPhoneNumberUrl, JSONObject.toJSONString(paramMap));
        log.info("微信小程序登录获取手机号返回结果: " + response);

        WxGetPhoneNumberVo wxGetPhoneNumberVo = JSONObject.parseObject(response, WxGetPhoneNumberVo.class);
        Integer errcode = wxGetPhoneNumberVo.getErrcode();
        if (errcode != 0){
            throw new RuntimeException("手机号码校验失败");
        }

        String phonenumber = wxGetPhoneNumberVo.getPhone_info().getPurePhoneNumber();
        return phonenumber;
    }

    /**
     * 第一次登录，进行用户注册
     * @param phonenumber
     * @return
     */
    private SysUser initializationWxUserInfo(String phonenumber, WxLoginResultDTO wxLoginResult){
        SysUser user = new SysUser();
        user.setUserName(phonenumber);
        user.setNickName(phonenumber);
        user.setDeptId(defaultDeptId);
        user.setPhonenumber(phonenumber);
        user.setSex("2");
        user.setPassword(UUID.randomUUID().toString());
        user.setStatus("0");
        user.setRealNameAuthStatus(YesOrNoFlagEnum.NO.getValue());
        user.setDelFlag(DeleteEnum.EXIST.getValue());
        user.setLoginIp(IpUtils.getIpAddr());
        user.setLoginDate(new Date());
        user.setCreateBy("admmin");
        user.setCreateTime(new Date());
        sysUserMapper.insertUser(user);

        Long userId = user.getUserId();
        int idLength = userId.toString().length();

        // 用户名 旅行者+用户ID后六位
        String nickName = "旅行者";
        if (idLength > 6){
            nickName = nickName + userId.toString().substring(idLength - 6);
        } else {
            nickName = nickName + String.format("%06d", userId);
        }

        user.setNickName(nickName);

        // UID以1开头，第2-4位为中国编码156，第5位后为注册位数(0001,0010,0100,1000,10000..)；例如第一位用户UID11560001
        String uid = "1156";
        if (idLength > 4){
            uid = uid + String.format("%0" + idLength + "d", userId);
        } else {
            uid = uid + String.format("%04d", userId);
        }

        user.setUid(uid);
        sysUserMapper.updateUser(user);

        // 新增用户角色信息
        SysUserRole userRole = new SysUserRole();
        userRole.setUserId(user.getUserId());
        userRole.setRoleId(defaultRoleId);
        sysUserRoleMapper.insertUserRole(userRole);

        // 新增用户角色微信信息
        this.initializationSysUserWxInfo(user, wxLoginResult);

        // 新增默认头像
        TfpAttachment attachment = new TfpAttachment();
        attachment.setBusinessId(user.getUserId());
        attachment.setBusinessType(BusinessTypeEnum.USER.getValue());
        attachment.setBusinessSubType(BusinessSubTypeEnum.USER_HEAD_PORTRAIT.getValue());
        attachment.setName("头像" + DcUrlUtils.getExtend(defaultAvatar));
        attachment.setUrl(defaultAvatar);
        attachment.setDelFlag(DeleteEnum.EXIST.getValue());
        attachment.setCreateUserId(user.getUserId());
        attachment.setCreateBy(user.getUserName());
        attachment.setCreateTime(DateUtils.getNowDate());
        attachmentMapper.insertTfpAttachment(attachment);

        // 新增用户账户信息
        userIncomeService.saveNewInfo(user.getUserId(), user.getUserName());

        // 初始化身份
        Long defaultIdentityId = identityService.initIdentity(user.getUserId(), user.getUserName());
        user.setIdentityId(defaultIdentityId);
        sysUserMapper.updateUser(user);

        return user;
    }

    /**
     * 新增用户角色微信信息
     * @param user
     * @param wxLoginResult
     */
    private void initializationSysUserWxInfo(SysUser user, WxLoginResultDTO wxLoginResult){
        SysUserWx sysUserWx = new SysUserWx();
        sysUserWx.setUserId(user.getUserId());
        sysUserWx.setUnionId(wxLoginResult.getUnionid());
        sysUserWx.setOpenId(wxLoginResult.getOpenid());
        sysUserWx.setNickName(user.getNickName());
        sysUserWx.setLoginName(user.getUserName());
        sysUserWx.setPhonenumber(user.getPhonenumber());
        sysUserWx.setDelFlag(DeleteEnum.EXIST.getValue());
        sysUserWx.setCreateTime(new Date());
        sysUserWx.setCreateBy("admin");
        sysUserWxMapper.insertSysUserWx(sysUserWx);
    }


    /**
     * 构造微信返回参数
     * @param wxToken
     * @return
     */
    private WxLoginResultEntity buildWxLoginResultEntity(String wxToken) {
        WxLoginResultEntity wxLoginResultEntity = new WxLoginResultEntity();
        wxLoginResultEntity.setWxToken(wxToken);
        return wxLoginResultEntity;
    }

    /*
     * 获取access_token
     * appid和appsecret到小程序后台获取，当然也可以让小程序开发人员给你传过来
     * apptype 1.小程序，2.微信公众号
     * */
    public String getAccess_token(String appid, String appsecret,String apptype) {
        log.info("获取token参数appid:{}",appid);
        log.info("获取token参数appsecret:{}",appsecret);
        log.info("获取token参数apptype:{}",apptype);
        String wxtoken = redisTemplate.opsForValue().get(apptype);
        log.info("走redis获取token:{}",wxtoken);
        if(Objects.isNull(wxtoken)){
            log.info("未走redis获取token开始:{}",wxtoken);
            //获取access_token
            String wurl = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential" +
                    "&appid=" + appid + "&secret=" + appsecret;
            RestTemplate restTemplate = new RestTemplate();
            String json = restTemplate.getForObject(wurl, String.class);
            AccessTokenEntity accessToken = JSONObject.parseObject(json, AccessTokenEntity.class);
            log.info("获取token返回值:{}", JSONObject.from(accessToken));

            wxtoken= accessToken.getAccess_token();
            redisTemplate.opsForValue().set(apptype, wxtoken);
            redisTemplate.expire(apptype, CAPTCHA_EXPIRE_TIME, TimeUnit.SECONDS);
            log.info("未走redis获取token结束:{}",redisTemplate.opsForValue().get(apptype));

        }
        return wxtoken;
    }
}
