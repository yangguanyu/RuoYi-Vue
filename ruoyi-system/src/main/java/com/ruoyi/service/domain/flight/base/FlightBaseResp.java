package com.ruoyi.service.domain.flight.base;

import lombok.Data;

@Data
public class FlightBaseResp<T> {
    // 请求返回代码，例如：000000表示成功
    private String rsCode;

    // 返回已知的错误信息
    private String rsMessage;

    // 请求时传入的唯一标识或者系统生成
    private String rsIdentification;

    // 返回是否接受Gzip 默认true 暂时仅支持返回GZip
    private Boolean rsIsGzip;

    //返回业务数据
    private T rsData;
}
