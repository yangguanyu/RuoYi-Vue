import request from '@/utils/request'

// 查询用户图片列表
export function listUserIcon(query) {
  return request({
    url: '/service/userIcon/list',
    method: 'get',
    params: query
  })
}

// 查询用户图片详细
export function getUserIcon(id) {
  return request({
    url: '/service/userIcon/' + id,
    method: 'get'
  })
}

// 新增用户图片
export function addUserIcon(data) {
  return request({
    url: '/service/userIcon',
    method: 'post',
    data: data
  })
}

// 修改用户图片
export function updateUserIcon(data) {
  return request({
    url: '/service/userIcon',
    method: 'put',
    data: data
  })
}

// 删除用户图片
export function delUserIcon(id) {
  return request({
    url: '/service/userIcon/' + id,
    method: 'delete'
  })
}
