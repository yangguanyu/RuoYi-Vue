package com.ruoyi.service.service.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enuma.BusinessSubTypeEnum;
import com.ruoyi.common.enuma.BusinessTypeEnum;
import com.ruoyi.common.enuma.DeleteEnum;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.DcDateUtils;
import com.ruoyi.common.utils.DcListUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.core.domain.entity.TfpAttachment;
import com.ruoyi.service.service.ITfpAttachmentService;
import com.ruoyi.service.vo.MyVisitorListVo;
import com.ruoyi.service.vo.VisitorVo;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.SysUserVisitorRecordMapper;
import com.ruoyi.service.domain.SysUserVisitorRecord;
import com.ruoyi.service.service.ISysUserVisitorRecordService;
import org.springframework.transaction.annotation.Transactional;

import static com.ruoyi.common.utils.PageUtils.startPage;

/**
 * 访客记录Service业务层处理
 *
 * @author ruoyi
 * @date 2023-10-31
 */
@Service
@Transactional
public class SysUserVisitorRecordServiceImpl implements ISysUserVisitorRecordService
{
    @Autowired
    private SysUserVisitorRecordMapper sysUserVisitorRecordMapper;
    @Autowired
    private ITfpAttachmentService attachmentService;
    @Autowired
    private ISysUserService userService;

    /**
     * 查询访客记录
     *
     * @param id 访客记录主键
     * @return 访客记录
     */
    @Override
    public SysUserVisitorRecord selectSysUserVisitorRecordById(Long id)
    {
        return sysUserVisitorRecordMapper.selectSysUserVisitorRecordById(id);
    }

    /**
     * 查询访客记录列表
     *
     * @param sysUserVisitorRecord 访客记录
     * @return 访客记录
     */
    @Override
    public List<SysUserVisitorRecord> selectSysUserVisitorRecordList(SysUserVisitorRecord sysUserVisitorRecord)
    {
        return sysUserVisitorRecordMapper.selectSysUserVisitorRecordList(sysUserVisitorRecord);
    }

    /**
     * 新增访客记录
     *
     * @param sysUserVisitorRecord 访客记录
     * @return 结果
     */
    @Override
    public int insertSysUserVisitorRecord(SysUserVisitorRecord sysUserVisitorRecord)
    {
        sysUserVisitorRecord.setCreateTime(DateUtils.getNowDate());
        return sysUserVisitorRecordMapper.insertSysUserVisitorRecord(sysUserVisitorRecord);
    }

    /**
     * 修改访客记录
     *
     * @param sysUserVisitorRecord 访客记录
     * @return 结果
     */
    @Override
    public int updateSysUserVisitorRecord(SysUserVisitorRecord sysUserVisitorRecord)
    {
        sysUserVisitorRecord.setUpdateTime(DateUtils.getNowDate());
        return sysUserVisitorRecordMapper.updateSysUserVisitorRecord(sysUserVisitorRecord);
    }

    /**
     * 批量删除访客记录
     *
     * @param ids 需要删除的访客记录主键
     * @return 结果
     */
    @Override
    public int deleteSysUserVisitorRecordByIds(Long[] ids)
    {
        return sysUserVisitorRecordMapper.deleteSysUserVisitorRecordByIds(ids);
    }

    /**
     * 删除访客记录信息
     *
     * @param id 访客记录主键
     * @return 结果
     */
    @Override
    public int deleteSysUserVisitorRecordById(Long id)
    {
        return sysUserVisitorRecordMapper.deleteSysUserVisitorRecordById(id);
    }

    /**
     * 今日访客个数
     * @param userId
     * @return
     */
    @Override
    public Integer selectTodayVisitorNumber(Long userId) {
        DateFormat dcDateFormat = DcDateUtils.getDcDateFormat(DcDateUtils.yyyyMMdd);
        Date startDate = null;
        Date endDate = null;
        try {
            startDate = dcDateFormat.parse(dcDateFormat.format(new Date()) + " 00:00:00");
            endDate = new Date(startDate.getTime() + (24 * 60 * 60 * 1000));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return sysUserVisitorRecordMapper.selectTodayVisitorNumber(userId, startDate, endDate);
    }

    /**
     * 小程序-访客列表
     * @return
     */
    @Override
    public List<VisitorVo> visitorList(Long userId) {
        if (Objects.isNull(userId)){
            LoginUser loginUser = SecurityUtils.getLoginUser();
            userId = loginUser.getUserId();
        }

        startPage();
        List<VisitorVo> visitorVoList = sysUserVisitorRecordMapper.visitorList(userId);

        if (DcListUtils.isNotEmpty(visitorVoList)){
            List<Long> userIdList = visitorVoList.stream().map(VisitorVo::getUserId).distinct().collect(Collectors.toList());

            // 用户信息
            List<SysUser> sysUserList = userService.selectUserInfoByUserIdList(userIdList);
            Map<Long, String> nickNameMap = sysUserList.stream().collect(Collectors.toMap(SysUser::getUserId, SysUser::getNickName));

            // 头像
            List<TfpAttachment> attachmentList = attachmentService.selectList(userIdList, BusinessTypeEnum.USER.getValue(), BusinessSubTypeEnum.USER_HEAD_PORTRAIT.getValue());
            Map<Long, String> headImageMap = attachmentList.stream().collect(Collectors.toMap(TfpAttachment::getBusinessId, TfpAttachment::getUrl));

            for (VisitorVo visitorVo : visitorVoList) {
                Long visitorUserId = visitorVo.getUserId();
                visitorVo.setNickName(nickNameMap.get(visitorUserId));
                visitorVo.setUserHeadImage(headImageMap.get(visitorUserId));
            }
        }

        return visitorVoList;
    }

    @Override
    public void logUserVisitorRecord(Long userId) {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long loginUserId = loginUser.getUserId();

        Date now = new Date();
        Date startDate = DcDateUtils.handelDateToDayStart(now);
        Date endDate = DcDateUtils.handelDateToDayEnd(now);
        List<SysUserVisitorRecord> userVisitorRecordList = sysUserVisitorRecordMapper.selectVisitorInfoToday(userId, loginUserId, startDate, endDate);
        if (DcListUtils.isEmpty(userVisitorRecordList)){
            SysUserVisitorRecord userVisitorRecord = new SysUserVisitorRecord();
            userVisitorRecord.setOwnerUserId(userId);
            userVisitorRecord.setVisitorUserId(loginUserId);
            userVisitorRecord.setDelFlag(DeleteEnum.EXIST.getValue());
            userVisitorRecord.setCreateUserId(loginUserId);
            userVisitorRecord.setCreateBy(loginUser.getUsername());
            userVisitorRecord.setCreateTime(DateUtils.getNowDate());
            sysUserVisitorRecordMapper.insertSysUserVisitorRecord(userVisitorRecord);
        }
    }
}
