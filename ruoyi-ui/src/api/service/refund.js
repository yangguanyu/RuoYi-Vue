import request from '@/utils/request'

// 查询退款列表
export function listRefund(query) {
  return request({
    url: '/service/refund/list',
    method: 'get',
    params: query
  })
}

// 查询退款详细
export function getRefund(id) {
  return request({
    url: '/service/refund/' + id,
    method: 'get'
  })
}

// 新增退款
export function addRefund(data) {
  return request({
    url: '/service/refund',
    method: 'post',
    data: data
  })
}

// 修改退款
export function updateRefund(data) {
  return request({
    url: '/service/refund',
    method: 'put',
    data: data
  })
}

// 删除退款
export function delRefund(id) {
  return request({
    url: '/service/refund/' + id,
    method: 'delete'
  })
}

export function refundUserSelect() {
  return request({
    url: '/service/refund/refundUserSelect',
    method: 'post'
  })
}
