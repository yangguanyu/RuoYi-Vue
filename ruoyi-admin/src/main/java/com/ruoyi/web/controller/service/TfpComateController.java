package com.ruoyi.web.controller.service;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.service.dto.BatchAddComateDto;
import com.ruoyi.service.dto.TfpComateDto;
import com.ruoyi.service.vo.ComateVo;
import com.ruoyi.service.vo.TfpComateVo;
import com.ruoyi.service.vo.UserVo;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.TfpComate;
import com.ruoyi.service.service.ITfpComateService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 同行人-独立出行Controller
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@RestController
@RequestMapping("/service/comate")
public class TfpComateController extends BaseController
{
    @Autowired
    private ITfpComateService tfpComateService;

    /**
     * 查询同行人-独立出行列表
     */
    @PreAuthorize("@ss.hasPermi('service:comate:list')")
    @GetMapping("/list")
    public TableDataInfo list(TfpComateDto tfpComateDto)
    {
        startPage();
        List<TfpComateVo> list = tfpComateService.selectTfpComateVoList(tfpComateDto);
        return getDataTable(list);
    }

    /**
     * 导出同行人-独立出行列表
     */
    @PreAuthorize("@ss.hasPermi('service:comate:export')")
    @Log(title = "同行人-独立出行", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TfpComate tfpComate)
    {
        List<TfpComate> list = tfpComateService.selectTfpComateList(tfpComate);
        ExcelUtil<TfpComate> util = new ExcelUtil<TfpComate>(TfpComate.class);
        util.exportExcel(response, list, "同行人-独立出行数据");
    }

    /**
     * 获取同行人-独立出行详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:comate:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tfpComateService.selectTfpComateById(id));
    }

    /**
     * 新增同行人-独立出行
     */
    @PreAuthorize("@ss.hasPermi('service:comate:add')")
    @Log(title = "同行人-独立出行", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TfpComate tfpComate)
    {
        return toAjax(tfpComateService.insertTfpComate(tfpComate));
    }

    /**
     * 修改同行人-独立出行
     */
    @PreAuthorize("@ss.hasPermi('service:comate:edit')")
    @Log(title = "同行人-独立出行", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TfpComate tfpComate)
    {
        return toAjax(tfpComateService.updateTfpComate(tfpComate));
    }

    /**
     * 删除同行人-独立出行
     */
    @PreAuthorize("@ss.hasPermi('service:comate:remove')")
    @Log(title = "同行人-独立出行", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tfpComateService.deleteTfpComateByIds(ids));
    }

    @PreAuthorize("@ss.hasPermi('service:comate:list')")
    @PostMapping("/mainUserSelect")
    public R<List<UserVo>> mainUserSelect()
    {
        return R.ok(tfpComateService.mainUserSelect());
    }


    @ApiOperation("小程序-添加同行人")
    @PostMapping("/batchAddComate")
    @ResponseBody
    public R batchAddComate(@RequestBody List<BatchAddComateDto> batchAddComateDtoList){
        return R.ok(tfpComateService.batchAddComate(batchAddComateDtoList));
    }

    @ApiOperation("小程序-删除同行人")
    @PostMapping("/deleteComate")
    @ResponseBody
    public R deleteComate(@RequestParam("mateId") Long mateId){
        return R.ok(tfpComateService.deleteComate(mateId));
    }

    @ApiOperation("小程序-获取我的同行人信息")
    @PostMapping("/getMyComateList")
    @ResponseBody
    public R<List<ComateVo>> getMyComateList(){
        return R.ok(tfpComateService.getMyComateList());
    }
}
