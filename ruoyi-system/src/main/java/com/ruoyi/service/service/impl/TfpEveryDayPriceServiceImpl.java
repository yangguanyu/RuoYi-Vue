package com.ruoyi.service.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.DcListUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpEveryDayPriceMapper;
import com.ruoyi.service.domain.TfpEveryDayPrice;
import com.ruoyi.service.service.ITfpEveryDayPriceService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 每日价格Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-12-12
 */
@Service
@Transactional
public class TfpEveryDayPriceServiceImpl implements ITfpEveryDayPriceService 
{
    @Autowired
    private TfpEveryDayPriceMapper tfpEveryDayPriceMapper;

    /**
     * 查询每日价格
     * 
     * @param id 每日价格主键
     * @return 每日价格
     */
    @Override
    public TfpEveryDayPrice selectTfpEveryDayPriceById(Long id)
    {
        return tfpEveryDayPriceMapper.selectTfpEveryDayPriceById(id);
    }

    /**
     * 查询每日价格列表
     * 
     * @param tfpEveryDayPrice 每日价格
     * @return 每日价格
     */
    @Override
    public List<TfpEveryDayPrice> selectTfpEveryDayPriceList(TfpEveryDayPrice tfpEveryDayPrice)
    {
        return tfpEveryDayPriceMapper.selectTfpEveryDayPriceList(tfpEveryDayPrice);
    }

    /**
     * 新增每日价格
     * 
     * @param tfpEveryDayPrice 每日价格
     * @return 结果
     */
    @Override
    public int insertTfpEveryDayPrice(TfpEveryDayPrice tfpEveryDayPrice)
    {
        tfpEveryDayPrice.setCreateTime(DateUtils.getNowDate());
        return tfpEveryDayPriceMapper.insertTfpEveryDayPrice(tfpEveryDayPrice);
    }

    /**
     * 修改每日价格
     * 
     * @param tfpEveryDayPrice 每日价格
     * @return 结果
     */
    @Override
    public int updateTfpEveryDayPrice(TfpEveryDayPrice tfpEveryDayPrice)
    {
        tfpEveryDayPrice.setUpdateTime(DateUtils.getNowDate());
        return tfpEveryDayPriceMapper.updateTfpEveryDayPrice(tfpEveryDayPrice);
    }

    /**
     * 批量删除每日价格
     * 
     * @param ids 需要删除的每日价格主键
     * @return 结果
     */
    @Override
    public int deleteTfpEveryDayPriceByIds(Long[] ids)
    {
        return tfpEveryDayPriceMapper.deleteTfpEveryDayPriceByIds(ids);
    }

    /**
     * 删除每日价格信息
     * 
     * @param id 每日价格主键
     * @return 结果
     */
    @Override
    public int deleteTfpEveryDayPriceById(Long id)
    {
        return tfpEveryDayPriceMapper.deleteTfpEveryDayPriceById(id);
    }

    @Override
    public List<TfpEveryDayPrice> selectDefaultTfpEveryDayPriceListByProductIdList(List<Long> productIdList) {
        return tfpEveryDayPriceMapper.selectDefaultTfpEveryDayPriceListByProductIdList(productIdList);
    }

    @Override
    public TfpEveryDayPrice selectNormalEveryDayPriceByProductId(Long productId) {
        List<Long> productIdList = new ArrayList<>();
        productIdList.add(productId);
        List<TfpEveryDayPrice> tfpEveryDayPriceList = tfpEveryDayPriceMapper.selectDefaultTfpEveryDayPriceListByProductIdList(productIdList);
        if (DcListUtils.isNotEmpty(tfpEveryDayPriceList)){
            return tfpEveryDayPriceList.get(0);
        } else {
            return null;
        }
    }

    @Override
    public void deleteTfpEveryDayPriceByProductIds(List<Long> productIdList) {
        tfpEveryDayPriceMapper.deleteTfpEveryDayPriceByProductIds(productIdList);
    }

    @Override
    public List<TfpEveryDayPrice> selectEveryDayPriceListByProductId(Long productId) {
        return tfpEveryDayPriceMapper.selectEveryDayPriceListByProductId(productId);
    }
}
