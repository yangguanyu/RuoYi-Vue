import request from '@/utils/request'

// 查询用户身份列表
export function listIdentity(query) {
  return request({
    url: '/service/identity/list',
    method: 'get',
    params: query
  })
}

// 查询用户身份详细
export function getIdentity(id) {
  return request({
    url: '/service/identity/' + id,
    method: 'get'
  })
}

// 新增用户身份
export function addIdentity(data) {
  return request({
    url: '/service/identity',
    method: 'post',
    data: data
  })
}

// 修改用户身份
export function updateIdentity(data) {
  return request({
    url: '/service/identity',
    method: 'put',
    data: data
  })
}

// 删除用户身份
export function delIdentity(id) {
  return request({
    url: '/service/identity/' + id,
    method: 'delete'
  })
}
