package com.ruoyi.web.controller.service;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.TfpEveryDayPrice;
import com.ruoyi.service.service.ITfpEveryDayPriceService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 每日价格Controller
 * 
 * @author ruoyi
 * @date 2023-12-12
 */
@RestController
@RequestMapping("/service/everyDayPrice")
public class TfpEveryDayPriceController extends BaseController
{
    @Autowired
    private ITfpEveryDayPriceService tfpEveryDayPriceService;

    /**
     * 查询每日价格列表
     */
    @PreAuthorize("@ss.hasPermi('service:everyDayPrice:list')")
    @GetMapping("/list")
    public TableDataInfo list(TfpEveryDayPrice tfpEveryDayPrice)
    {
        startPage();
        List<TfpEveryDayPrice> list = tfpEveryDayPriceService.selectTfpEveryDayPriceList(tfpEveryDayPrice);
        return getDataTable(list);
    }

    /**
     * 导出每日价格列表
     */
    @PreAuthorize("@ss.hasPermi('service:everyDayPrice:export')")
    @Log(title = "每日价格", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TfpEveryDayPrice tfpEveryDayPrice)
    {
        List<TfpEveryDayPrice> list = tfpEveryDayPriceService.selectTfpEveryDayPriceList(tfpEveryDayPrice);
        ExcelUtil<TfpEveryDayPrice> util = new ExcelUtil<TfpEveryDayPrice>(TfpEveryDayPrice.class);
        util.exportExcel(response, list, "每日价格数据");
    }

    /**
     * 获取每日价格详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:everyDayPrice:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tfpEveryDayPriceService.selectTfpEveryDayPriceById(id));
    }

    /**
     * 新增每日价格
     */
    @PreAuthorize("@ss.hasPermi('service:everyDayPrice:add')")
    @Log(title = "每日价格", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TfpEveryDayPrice tfpEveryDayPrice)
    {
        return toAjax(tfpEveryDayPriceService.insertTfpEveryDayPrice(tfpEveryDayPrice));
    }

    /**
     * 修改每日价格
     */
    @PreAuthorize("@ss.hasPermi('service:everyDayPrice:edit')")
    @Log(title = "每日价格", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TfpEveryDayPrice tfpEveryDayPrice)
    {
        return toAjax(tfpEveryDayPriceService.updateTfpEveryDayPrice(tfpEveryDayPrice));
    }

    /**
     * 删除每日价格
     */
    @PreAuthorize("@ss.hasPermi('service:everyDayPrice:remove')")
    @Log(title = "每日价格", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tfpEveryDayPriceService.deleteTfpEveryDayPriceByIds(ids));
    }
}
