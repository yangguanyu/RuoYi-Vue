package com.ruoyi.common.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DcValidateUtils {

    /**
     * 校验身份证
     * @param idCard
     * @return
     */
    public static boolean validateIdCard(String idCard){
        Pattern pattern = Pattern.compile("^[1-9]\\d{5}(19|20)\\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\\d{3}[0-9Xx]$");
        Matcher matcher = pattern.matcher(idCard);
        return matcher.matches();
    }

    /**
     * 校验电话
     * @param phone
     * @return
     */
    public static boolean validatePhone(String phone){
        Pattern pattern = Pattern.compile("^1\\d{10}$");
        Matcher matcher = pattern.matcher(phone);
        return matcher.matches();
    }

    /**
     * 校验用户名
     * @param userName
     * @return
     */
    public static boolean validateUserName(String userName){
        boolean flag = true;

        Pattern pattern = Pattern.compile("^[`~!@#$%^&*()_+<>?:\"{},.\\/;'[\\]]]$");
        int length = userName.length();
        for (int i = 0; i < length; i++) {
            Matcher matcher;
            if (i + 1 == length){
                matcher = pattern.matcher(userName.substring(i));
            } else {
                matcher = pattern.matcher(userName.substring(i, i + 1));
            }

            if (matcher.matches()){
                flag = false;
                break;
            }
        }

        return flag;
    }
}
