package com.ruoyi.service.service;

import java.util.List;
import com.ruoyi.service.domain.TfpProductCbc;

/**
 * 商品、合作分公司中间Service接口
 * 
 * @author ruoyi
 * @date 2023-12-12
 */
public interface ITfpProductCbcService 
{
    /**
     * 查询商品、合作分公司中间
     * 
     * @param id 商品、合作分公司中间主键
     * @return 商品、合作分公司中间
     */
    public TfpProductCbc selectTfpProductCbcById(Long id);

    /**
     * 查询商品、合作分公司中间列表
     * 
     * @param tfpProductCbc 商品、合作分公司中间
     * @return 商品、合作分公司中间集合
     */
    public List<TfpProductCbc> selectTfpProductCbcList(TfpProductCbc tfpProductCbc);

    /**
     * 新增商品、合作分公司中间
     * 
     * @param tfpProductCbc 商品、合作分公司中间
     * @return 结果
     */
    public int insertTfpProductCbc(TfpProductCbc tfpProductCbc);

    /**
     * 修改商品、合作分公司中间
     * 
     * @param tfpProductCbc 商品、合作分公司中间
     * @return 结果
     */
    public int updateTfpProductCbc(TfpProductCbc tfpProductCbc);

    /**
     * 批量删除商品、合作分公司中间
     * 
     * @param ids 需要删除的商品、合作分公司中间主键集合
     * @return 结果
     */
    public int deleteTfpProductCbcByIds(Long[] ids);

    /**
     * 删除商品、合作分公司中间信息
     * 
     * @param id 商品、合作分公司中间主键
     * @return 结果
     */
    public int deleteTfpProductCbcById(Long id);

    void deleteTfpProductCbcByProductId(Long productId);

    void saveProductCbc(Long productId, List<Long> cooperativeBranchCompanyIdList);

    List<TfpProductCbc> selectTfpProductCbcByProductIdList(List<Long> productIdList);

    List<TfpProductCbc> selectTfpProductCbcListByProductId(Long productId);

    void deleteTfpProductCbcByProductIds(List<Long> productIdList);
}
