package com.ruoyi.web.controller.service;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.service.dto.ApplyRefundDto;
import com.ruoyi.service.dto.AuditDto;
import com.ruoyi.service.dto.AuditRefundDto;
import com.ruoyi.service.dto.TfpRefundDto;
import com.ruoyi.service.vo.*;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.TfpRefund;
import com.ruoyi.service.service.ITfpRefundService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 退款Controller
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@RestController
@RequestMapping("/service/refund")
public class TfpRefundController extends BaseController
{
    @Autowired
    private ITfpRefundService tfpRefundService;

    /**
     * 查询退款列表
     */
    @PreAuthorize("@ss.hasPermi('service:refund:list')")
    @GetMapping("/list")
    public TableDataInfo list(TfpRefundDto tfpRefundDto)
    {
        startPage();
        List<TfpRefundVo> list = tfpRefundService.selectTfpRefundVoList(tfpRefundDto);
        return getDataTable(list);
    }

    /**
     * 导出退款列表
     */
    @PreAuthorize("@ss.hasPermi('service:refund:export')")
    @Log(title = "退款", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TfpRefund tfpRefund)
    {
        List<TfpRefund> list = tfpRefundService.selectTfpRefundList(tfpRefund);
        ExcelUtil<TfpRefund> util = new ExcelUtil<TfpRefund>(TfpRefund.class);
        util.exportExcel(response, list, "退款数据");
    }

    /**
     * 获取退款详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:refund:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tfpRefundService.selectTfpRefundById(id));
    }

    /**
     * 新增退款
     */
    @PreAuthorize("@ss.hasPermi('service:refund:add')")
    @Log(title = "退款", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TfpRefund tfpRefund)
    {
        return toAjax(tfpRefundService.insertTfpRefund(tfpRefund));
    }

    /**
     * 修改退款
     */
    @PreAuthorize("@ss.hasPermi('service:refund:edit')")
    @Log(title = "退款", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TfpRefund tfpRefund)
    {
        return toAjax(tfpRefundService.updateTfpRefund(tfpRefund));
    }

    /**
     * 删除退款
     */
    @PreAuthorize("@ss.hasPermi('service:refund:remove')")
    @Log(title = "退款", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tfpRefundService.deleteTfpRefundByIds(ids));
    }

    @PreAuthorize("@ss.hasPermi('service:refund:list')")
    @PostMapping("/refundUserSelect")
    public R<List<UserVo>> refundUserSelect()
    {
        return R.ok(tfpRefundService.refundUserSelect());
    }

    @ApiOperation("小程序-申请退款详情")
    @PostMapping("/applyRefundDetail")
    @ResponseBody
    public R<ApplyRefundDetailVo> applyRefundDetail(@RequestParam("orderId") Long orderId){
        return R.ok(tfpRefundService.applyRefundDetail(orderId));
    }

    @ApiOperation("小程序-申请退款")
    @PostMapping("/applyRefund")
    @ResponseBody
    public R applyRefund(@RequestBody ApplyRefundDto applyRefundDto){
        return R.ok(tfpRefundService.applyRefund(applyRefundDto));
    }

    @ApiOperation("小程序-退款详情")
    @PostMapping("/refundDetail")
    @ResponseBody
    public R<RefundDetailVo> refundDetail(@RequestParam("id") Long id){
        return R.ok(tfpRefundService.refundDetail(id));
    }

    @ApiOperation("后台-审核退款")
    @PostMapping("/auditRefund")
    @ResponseBody
    public R auditRefund(@RequestBody AuditRefundDto auditRefundDto){
        return R.ok(tfpRefundService.auditRefund(auditRefundDto));
    }
}
