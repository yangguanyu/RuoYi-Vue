package com.ruoyi.service.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpProductSubMapper;
import com.ruoyi.service.domain.TfpProductSub;
import com.ruoyi.service.service.ITfpProductSubService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 商品辅Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@Service
@Transactional
public class TfpProductSubServiceImpl implements ITfpProductSubService 
{
    @Autowired
    private TfpProductSubMapper tfpProductSubMapper;

    /**
     * 查询商品辅
     * 
     * @param id 商品辅主键
     * @return 商品辅
     */
    @Override
    public TfpProductSub selectTfpProductSubById(Long id)
    {
        return tfpProductSubMapper.selectTfpProductSubById(id);
    }

    /**
     * 查询商品辅列表
     * 
     * @param tfpProductSub 商品辅
     * @return 商品辅
     */
    @Override
    public List<TfpProductSub> selectTfpProductSubList(TfpProductSub tfpProductSub)
    {
        return tfpProductSubMapper.selectTfpProductSubList(tfpProductSub);
    }

    /**
     * 新增商品辅
     * 
     * @param tfpProductSub 商品辅
     * @return 结果
     */
    @Override
    public int insertTfpProductSub(TfpProductSub tfpProductSub)
    {
        tfpProductSub.setCreateTime(DateUtils.getNowDate());
        return tfpProductSubMapper.insertTfpProductSub(tfpProductSub);
    }

    /**
     * 修改商品辅
     * 
     * @param tfpProductSub 商品辅
     * @return 结果
     */
    @Override
    public int updateTfpProductSub(TfpProductSub tfpProductSub)
    {
        tfpProductSub.setUpdateTime(DateUtils.getNowDate());
        return tfpProductSubMapper.updateTfpProductSub(tfpProductSub);
    }

    /**
     * 批量删除商品辅
     * 
     * @param ids 需要删除的商品辅主键
     * @return 结果
     */
    @Override
    public int deleteTfpProductSubByIds(Long[] ids)
    {
        return tfpProductSubMapper.deleteTfpProductSubByIds(ids);
    }

    /**
     * 删除商品辅信息
     * 
     * @param id 商品辅主键
     * @return 结果
     */
    @Override
    public int deleteTfpProductSubById(Long id)
    {
        return tfpProductSubMapper.deleteTfpProductSubById(id);
    }

    @Override
    public TfpProductSub selectTfpProductSubByProductId(Long productId) {
        return tfpProductSubMapper.selectTfpProductSubByProductId(productId);
    }

    @Override
    public void deleteTfpProductSubByProductIds(List<Long> productIdList) {
        tfpProductSubMapper.deleteTfpProductSubByProductIds(productIdList);
    }
}
