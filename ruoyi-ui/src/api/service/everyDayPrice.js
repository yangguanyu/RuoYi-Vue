import request from '@/utils/request'

// 查询每日价格列表
export function listEveryDayPrice(query) {
  return request({
    url: '/service/everyDayPrice/list',
    method: 'get',
    params: query
  })
}

// 查询每日价格详细
export function getEveryDayPrice(id) {
  return request({
    url: '/service/everyDayPrice/' + id,
    method: 'get'
  })
}

// 新增每日价格
export function addEveryDayPrice(data) {
  return request({
    url: '/service/everyDayPrice',
    method: 'post',
    data: data
  })
}

// 修改每日价格
export function updateEveryDayPrice(data) {
  return request({
    url: '/service/everyDayPrice',
    method: 'put',
    data: data
  })
}

// 删除每日价格
export function delEveryDayPrice(id) {
  return request({
    url: '/service/everyDayPrice/' + id,
    method: 'delete'
  })
}
