package com.ruoyi.service.service;

import java.util.Date;
import java.util.List;
import com.ruoyi.service.domain.TfpFlightDatePrice;
import com.ruoyi.service.domain.TfpProduct;
import com.ruoyi.service.domain.TfpProductDepartureDate;

/**
 * 航班日期价格Service接口
 * 
 * @author ruoyi
 * @date 2024-04-29
 */
public interface ITfpFlightDatePriceService 
{
    /**
     * 查询航班日期价格
     * 
     * @param id 航班日期价格主键
     * @return 航班日期价格
     */
    public TfpFlightDatePrice selectTfpFlightDatePriceById(Long id);

    /**
     * 查询航班日期价格列表
     * 
     * @param tfpFlightDatePrice 航班日期价格
     * @return 航班日期价格集合
     */
    public List<TfpFlightDatePrice> selectTfpFlightDatePriceList(TfpFlightDatePrice tfpFlightDatePrice);

    /**
     * 新增航班日期价格
     * 
     * @param tfpFlightDatePrice 航班日期价格
     * @return 结果
     */
    public int insertTfpFlightDatePrice(TfpFlightDatePrice tfpFlightDatePrice);

    /**
     * 修改航班日期价格
     * 
     * @param tfpFlightDatePrice 航班日期价格
     * @return 结果
     */
    public int updateTfpFlightDatePrice(TfpFlightDatePrice tfpFlightDatePrice);

    /**
     * 批量删除航班日期价格
     * 
     * @param ids 需要删除的航班日期价格主键集合
     * @return 结果
     */
    public int deleteTfpFlightDatePriceByIds(Long[] ids);

    TfpFlightDatePrice selectTfpFlightDatePriceByTimeAndName(String departureTime, String departureAirportName, String arrivalAirportName);

    /**
     * 删除航班日期价格信息
     * 
     * @param id 航班日期价格主键
     * @return 结果
     */
    public int deleteTfpFlightDatePriceById(Long id);

    void handleFlightDatePrice(TfpProduct tfpProduct, List<TfpProductDepartureDate> tfpProductDepartureDateList) throws Exception;

    List<TfpFlightDatePrice> getTfpFlightDatePriceListByGoAndBackPlaceAndDateList(Integer goAndBackFlag, TfpProduct tfpProduct, List<String> departureTimeList);
}
