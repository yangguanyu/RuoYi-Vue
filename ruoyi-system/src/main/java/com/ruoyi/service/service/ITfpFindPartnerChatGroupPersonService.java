package com.ruoyi.service.service;

import java.util.List;
import com.ruoyi.service.domain.TfpFindPartnerChatGroupPerson;
import com.ruoyi.service.dto.TfpFindPartnerChatGroupPersonDto;
import com.ruoyi.service.vo.TfpFindPartnerChatGroupPersonVo;

/**
 * 找搭子聊天群组成员Service接口
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public interface ITfpFindPartnerChatGroupPersonService 
{
    /**
     * 查询找搭子聊天群组成员
     * 
     * @param id 找搭子聊天群组成员主键
     * @return 找搭子聊天群组成员
     */
    public TfpFindPartnerChatGroupPerson selectTfpFindPartnerChatGroupPersonById(Long id);

    /**
     * 查询找搭子聊天群组成员列表
     * 
     * @param tfpFindPartnerChatGroupPerson 找搭子聊天群组成员
     * @return 找搭子聊天群组成员集合
     */
    public List<TfpFindPartnerChatGroupPerson> selectTfpFindPartnerChatGroupPersonList(TfpFindPartnerChatGroupPerson tfpFindPartnerChatGroupPerson);

    List<TfpFindPartnerChatGroupPersonVo> selectTfpFindPartnerChatGroupPersonVoList(TfpFindPartnerChatGroupPersonDto tfpFindPartnerChatGroupPersonDto);

    /**
     * 新增找搭子聊天群组成员
     * 
     * @param tfpFindPartnerChatGroupPerson 找搭子聊天群组成员
     * @return 结果
     */
    public int insertTfpFindPartnerChatGroupPerson(TfpFindPartnerChatGroupPerson tfpFindPartnerChatGroupPerson);

    /**
     * 修改找搭子聊天群组成员
     * 
     * @param tfpFindPartnerChatGroupPerson 找搭子聊天群组成员
     * @return 结果
     */
    public int updateTfpFindPartnerChatGroupPerson(TfpFindPartnerChatGroupPerson tfpFindPartnerChatGroupPerson);

    /**
     * 批量删除找搭子聊天群组成员
     * 
     * @param ids 需要删除的找搭子聊天群组成员主键集合
     * @return 结果
     */
    public int deleteTfpFindPartnerChatGroupPersonByIds(Long[] ids);

    /**
     * 删除找搭子聊天群组成员信息
     * 
     * @param id 找搭子聊天群组成员主键
     * @return 结果
     */
    public int deleteTfpFindPartnerChatGroupPersonById(Long id);

    List<TfpFindPartnerChatGroupPerson> selectListByFindPartnerChatGroupId(Long findPartnerChatGroupId);

    TfpFindPartnerChatGroupPerson selectByGroupIdAndUserId(Long findPartnerChatGroupId, Long userId);
}
