package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.TfpFlightStopInfo;
import org.apache.ibatis.annotations.Param;

/**
 * 经停信息Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-10
 */
public interface TfpFlightStopInfoMapper 
{
    /**
     * 查询经停信息
     * 
     * @param id 经停信息主键
     * @return 经停信息
     */
    public TfpFlightStopInfo selectTfpFlightStopInfoById(Long id);

    /**
     * 查询经停信息列表
     * 
     * @param tfpFlightStopInfo 经停信息
     * @return 经停信息集合
     */
    public List<TfpFlightStopInfo> selectTfpFlightStopInfoList(TfpFlightStopInfo tfpFlightStopInfo);

    /**
     * 新增经停信息
     * 
     * @param tfpFlightStopInfo 经停信息
     * @return 结果
     */
    public int insertTfpFlightStopInfo(TfpFlightStopInfo tfpFlightStopInfo);

    /**
     * 修改经停信息
     * 
     * @param tfpFlightStopInfo 经停信息
     * @return 结果
     */
    public int updateTfpFlightStopInfo(TfpFlightStopInfo tfpFlightStopInfo);

    /**
     * 删除经停信息
     * 
     * @param id 经停信息主键
     * @return 结果
     */
    public int deleteTfpFlightStopInfoById(Long id);

    /**
     * 批量删除经停信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpFlightStopInfoByIds(Long[] ids);

    void deleteTfpFlightStopInfoByFlightId(@Param("flightId") Long flightId);
}
