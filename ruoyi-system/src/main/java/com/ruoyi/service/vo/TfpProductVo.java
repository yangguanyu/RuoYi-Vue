package com.ruoyi.service.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 商品主对象 tfp_product
 *
 * @author ruoyi
 * @date 2023-11-01
 */
@Data
@ApiModel("TfpProductVo 商品返回VO")
public class TfpProductVo
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 客服id */
    private Long customerServiceId;

    /** 商品归属 0-国内 1-海外 */
    private Integer commoditiesType;

    /** 业务地区(1级) 能够选多个地区 下拉 */
    private Long parentAreaId;

    /** 业务地区(1级) 能够选多个地区 下拉 */
    private String parentAreaName;

    /** 业务地区(2级) 能够选多个地区 下拉 */
    private Long areaId;

    /** 业务地区(2级) 能够选多个地区 下拉 */
    private String areaName;

    /** 业务地区(3级) 能够选多个地区 下拉 */
    private Long cityId;

    /** 业务地区(3级) 能够选多个地区 下拉 */
    private String cityName;

    /** 服务类型id 散拼 单团 单项 下拉（怎么和咱们的全国散，一家一团对应） */
    private Long categoryId;

    /** 服务类型名称 散拼 单团 单项 下拉（怎么和咱们的全国散，一家一团对应） */
    private String categoryName;

    /** 线路类型 出发地参团 目的地参团 字典code */
    private String lineTypeCode;

    /** 线路类型 出发地参团 目的地参团 字典name */
    private String lineTypeName;

    /** 商品名称 */
    private String productName;

    /** 商品编号 */
    private String productNo;

    /** 供应商名称 */
    private String supplierName;

    /** 供应商id */
    private Long supplierId;

    /** 儿童标准 */
    private Integer age;

    /** 儿童标准-年龄-最小值 */
    private Integer childrenAgeLimitMin;

    /** 儿童标准-年龄-最大值 */
    private Integer childrenAgeLimitMax;

    /** 出发城市id */
    private Long goPlaceId;

    /** 出发城市名称 */
    private String goPlaceName;

    /** 返回城市id */
    private Long backPlaceId;

    /** 返回城市名称 */
    private String backPlaceName;

    /** 字典 去程交通 飞机 火车 轮船 汽车 高铁 其它 */
    private String goTrafficCode;

    /** 去程交通 飞机 火车 轮船 汽车 高铁 其它 */
    private String goTrafficName;

    /** dict 回程交通 */
    private String backTrafficCode;

    /** 回程交通 */
    private String backTrafficName;

    /** 行程天数 几天 */
    private Integer dayNum;

    /** 行程天数 几晚 */
    private Integer nightNum;

    /** 是否包含保险 单选框 0-否 1-是 */
    private Integer isInsurance;

    /** 保险公司 */
    private String insuranceCompany;

    /** 险种code：人身意外险 旅游团队先 */
    private String insuranceTypeCode;

    /** 险种名称：人身意外险 旅游团队先 */
    private String insuranceTypeName;

    /** 商品状态 0-待审核 1-审核通过 2-审核不通过 3-草稿 */
    @ApiModelProperty("商品状态 0-待审核 1-审核通过 2-审核不通过 3-草稿")
    private Integer itemStatus;

    /** 航空公司code 下拉 需要爬数据 */
    private String airCompanyCode;

    /** 航空公司名称 下拉 需要爬数据 */
    private String airCompanyName;

    /** 是否有购物 0-否 1-是 */
    private Integer purchaseFlag;

    /** 是否自费项目 0-否 1-是 */
    private Integer selfFundedProjectFlag;

    /** 提前截止天数 */
    private Integer earlyDeadlineDay;

    private List<String> cooperativeBranchCompanyNameList;

    /** 资源确认方式 1：二次确认  2：即使确认 */
    private Integer confirmationType;

    /** 分值，定时任务10分钟 */
    private Long score;

    /** 是否选择低价机票服务 0-否 1-是 */
    private Integer lowPriceAirTicketServiceFlag;

    /** 机票价格是否已经查询完成 0-否 1-是 */
    private Integer airTicketPriceQueryCompleteFlag;

    /** 提交后机票价格是否已经查询 0-否 1-是 */
    private Integer submitAirTicketPriceQueryFlag;

    /** 产品上下架状态字段 0-新增待上架 1-已上架 2-下架待审批 3-已下架 */
    @ApiModelProperty("产品上下架状态字段 0-新增待上架 1-已上架 2-下架待审批 3-已下架")
    private Integer salesStatus;

    /** 上架日期 */
    private Date salesDate;

    /** 机票价格查询时间 */
    private Date airTicketPriceQueryDate;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    private Long createUserId;

    /** 更新者id */
    private Long updateUserId;

    /** 预收数 */
    private Integer expectedNumber;

    @ApiModelProperty("报名人数")
    private Integer signUpPersonNumber;

    @ApiModelProperty("图片url")
    private String imageUrl;
    @ApiModelProperty("报名人员头像url集合")
    private List<String> signUpPersonHeadPortraitUrlList;
    @ApiModelProperty("商品封面图片url集合")
    private List<String> productCoverImageUrlList;
    @ApiModelProperty("商品描述")
    private String productDescribe;
}
