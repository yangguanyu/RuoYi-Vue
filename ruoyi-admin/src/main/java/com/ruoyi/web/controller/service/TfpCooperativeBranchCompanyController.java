package com.ruoyi.web.controller.service;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.service.vo.SupplierVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.TfpCooperativeBranchCompany;
import com.ruoyi.service.service.ITfpCooperativeBranchCompanyService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 合作分公司Controller
 * 
 * @author ruoyi
 * @date 2023-12-12
 */
@RestController
@RequestMapping("/service/cooperativeBranchCompany")
public class TfpCooperativeBranchCompanyController extends BaseController
{
    @Autowired
    private ITfpCooperativeBranchCompanyService tfpCooperativeBranchCompanyService;

    /**
     * 查询合作分公司列表
     */
    @PreAuthorize("@ss.hasPermi('service:cooperativeBranchCompany:list')")
    @GetMapping("/list")
    public TableDataInfo list(TfpCooperativeBranchCompany tfpCooperativeBranchCompany)
    {
        startPage();
        List<TfpCooperativeBranchCompany> list = tfpCooperativeBranchCompanyService.selectTfpCooperativeBranchCompanyList(tfpCooperativeBranchCompany);
        return getDataTable(list);
    }

    /**
     * 导出合作分公司列表
     */
    @PreAuthorize("@ss.hasPermi('service:cooperativeBranchCompany:export')")
    @Log(title = "合作分公司", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TfpCooperativeBranchCompany tfpCooperativeBranchCompany)
    {
        List<TfpCooperativeBranchCompany> list = tfpCooperativeBranchCompanyService.selectTfpCooperativeBranchCompanyList(tfpCooperativeBranchCompany);
        ExcelUtil<TfpCooperativeBranchCompany> util = new ExcelUtil<TfpCooperativeBranchCompany>(TfpCooperativeBranchCompany.class);
        util.exportExcel(response, list, "合作分公司数据");
    }

    /**
     * 获取合作分公司详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:cooperativeBranchCompany:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tfpCooperativeBranchCompanyService.selectTfpCooperativeBranchCompanyById(id));
    }

    /**
     * 新增合作分公司
     */
    @PreAuthorize("@ss.hasPermi('service:cooperativeBranchCompany:add')")
    @Log(title = "合作分公司", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TfpCooperativeBranchCompany tfpCooperativeBranchCompany)
    {
        return toAjax(tfpCooperativeBranchCompanyService.insertTfpCooperativeBranchCompany(tfpCooperativeBranchCompany));
    }

    /**
     * 修改合作分公司
     */
    @PreAuthorize("@ss.hasPermi('service:cooperativeBranchCompany:edit')")
    @Log(title = "合作分公司", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TfpCooperativeBranchCompany tfpCooperativeBranchCompany)
    {
        return toAjax(tfpCooperativeBranchCompanyService.updateTfpCooperativeBranchCompany(tfpCooperativeBranchCompany));
    }

    /**
     * 删除合作分公司
     */
    @PreAuthorize("@ss.hasPermi('service:cooperativeBranchCompany:remove')")
    @Log(title = "合作分公司", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tfpCooperativeBranchCompanyService.deleteTfpCooperativeBranchCompanyByIds(ids));
    }

    @PreAuthorize("@ss.hasPermi('service:cooperativeBranchCompany:list')")
    @GetMapping("/cooperativeBranchCompanySelect")
    public R<List<SupplierVo>> cooperativeBranchCompanySelect()
    {
        return R.ok(tfpCooperativeBranchCompanyService.cooperativeBranchCompanySelect());
    }
}
