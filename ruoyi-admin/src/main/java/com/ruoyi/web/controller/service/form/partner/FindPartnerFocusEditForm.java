package com.ruoyi.web.controller.service.form.partner;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 找搭子关注对象 tfp_find_partner_focus
 *
 * @author yangguanyu
 * @date 2024-01-12
 */
@ApiModel("FindPartnerFocusEditForm-找搭子关注修改")
@Data
public class FindPartnerFocusEditForm {


    @ApiModelProperty(value = "id")
    private Long id;

    /** 用户id */
    //这个用来判断权限的，避免恶意请求
    @ApiModelProperty(value = "用户id")
    private Long userId;

    /** 找搭子id */
    @ApiModelProperty(value = "找搭子id")
    private Long findPartnerId;

}
