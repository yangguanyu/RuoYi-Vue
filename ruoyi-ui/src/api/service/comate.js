import request from '@/utils/request'

// 查询同行人-独立出行列表
export function listComate(query) {
  return request({
    url: '/service/comate/list',
    method: 'get',
    params: query
  })
}

// 查询同行人-独立出行详细
export function getComate(id) {
  return request({
    url: '/service/comate/' + id,
    method: 'get'
  })
}

// 新增同行人-独立出行
export function addComate(data) {
  return request({
    url: '/service/comate',
    method: 'post',
    data: data
  })
}

// 修改同行人-独立出行
export function updateComate(data) {
  return request({
    url: '/service/comate',
    method: 'put',
    data: data
  })
}

// 删除同行人-独立出行
export function delComate(id) {
  return request({
    url: '/service/comate/' + id,
    method: 'delete'
  })
}


export function mainUserSelect() {
  return request({
    url: '/service/comate/mainUserSelect',
    method: 'post'
  })
}
