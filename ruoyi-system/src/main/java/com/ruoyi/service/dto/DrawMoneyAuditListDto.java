package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel("DrawMoneyAuditListDto")
public class DrawMoneyAuditListDto {
    @ApiModelProperty("状态 0-提交申请/审核中 1-提现完成 2-提现失败/审核不通过")
    private Integer status;
    @ApiModelProperty("提现编号")
    private String drawMoneyNumber;
    @ApiModelProperty("提现发起时间-起始")
    private Date createTimeStart;
    @ApiModelProperty("提现发起时间-截止")
    private Date createTimeEnd;
}
