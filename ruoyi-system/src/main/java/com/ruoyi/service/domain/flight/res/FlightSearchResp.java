package com.ruoyi.service.domain.flight.res;

import lombok.Data;

import java.util.List;

@Data
public class FlightSearchResp {
    // 航班号
    private String flightNo;
    // 起飞时间
    private String depTime;
    // 起飞机场三字码
    private String depCode;
    // 到达机场
    private String arrCode;
    // 抵达时间
    private String arrTime;
    // 出发航站楼
    private String depJetquay;
    // 抵达航站楼
    private String arrJetquay;
    // 经停次数 数字代表次数 0-直达 1-经停1次
    private String stopNum;
    // 经停信息
    private List<StopInfo> stopInfos;
    // 机型
    private String planeType;
    // 飞机大小 1-大 2-中 3-小
    private Integer planeSize;
    // 中文机型描述
    private String planeCnName;
    // 飞行里程
    private String distance;
    // 基础价格
    private Integer basePrics;
    // 餐食标识
    private String meal;
    // 舱位信息
    private List<Seat> seatItems;
    // 通程前后航段信息
    private List<SegmentDetail> segmentDetails;

    @Data
    public class Seat {
        // 舱位 W
        private String seatCode;
        // 儿童舱位 W
        private String chdSeatCode;
        // 舱位状态 0-9或者英文字母
        private String seatStatus;
        // 儿童舱位状态1-9代表剩余座位数，A代表座位数大于9
        private String chdSeatStatus;
        // 儿童舱位 W
        private String infSeatCode;
        // 婴儿舱位状态 1-9代表剩余座位数，A代表座位数大于9
        private String infSeatStatus;
        // 服务级别 1-头等舱 2-商务舱 3-经济舱 4-舒适A舱 5-超级经济舱 6-高端经济舱 7-尊享经济 8-超值经济 9-超值公务 10-超值头等 11-舒适经济 12-优惠商务 13-明珠经济 14-超值商务 15-空中商务舱 16-公务舱产品舱
        private Integer cabinClass;
        // 舱位说明
        private String seatMsg;
        // 是否特价舱位 1-普通 3-特价
        private Integer seatType;
        // 折扣
        private Double discount;
        // 产品信息
        private Policy policys;
        // 通程前后航段舱位对应信息
        private List<SeatDetail> seatDetails;
    }

    @Data
    public class SegmentDetail {
        // 航班号
        private String flightNo;
        // 出发城市三字码
        private String depCode;
        // 起飞城市名称
        private String depCity;
        // 出发机场名称
        private String depAirport;
        // 抵达城市三字码
        private String arrCode;
        // 抵达城市名称
        private String arrCity;
        // 抵达机场名称
        private String arrAirport;
        // 航段
        private String segment;
        // 出发日期
        private String depDate;
        // 出发时间
        private String depTime;
        // 抵达日期
        private String arrDate;
        // 抵达时间
        private String arrTime;
        // 航司
        private String carrier;
        // 出发航站
        private String depTerm;
        // 抵达航站
        private String arrTerm;
    }

    @Data
    public class Policy {
        // 产品ID用于验舱验价及下单
        private String policyId;
        // 产品类型
        //  商旅优选：1、2、3、4、12、13、9、
        //  低价推荐：41、42、43、44、45、26、
        //  官网：21、22、23、24、25、6
        //  生成订单需传入出生日期：4、41、42、43、44、45、21、22、23、24、25、6、9、26
        private String productType;
        // 证件限制 false-不限 true-有限制
        private Boolean certificates;
        // 证件类型列表
        private List<String> certificatesList;
        // 出票工作时间
        private String workTime;
        // 退票工作时间
        private String vtWorkTime;
        // 备注（产品备注）
        private String comment;
        // 价格信息
        private List<PriceData> priceDatas;
        // 适用乘客类型 1-成人 2-儿童 3-成人+儿童 4-成人+婴儿 5-成人+儿童+婴儿
        private Integer applyPassenger;
        // 报销凭证类型 1-行程单 2-发票 3-电子发票 4-行程单+发票 5-不提供报销凭证
        private Integer voucher;
        // 最晚出票时间计算方式 1-支付后 2-起飞前
        private Integer latestAlgorithm;
        // 最晚出票时间（分钟）
        private Integer latestTicket;
        // 产品年龄限制
        private String ageLimit;
        // 人数限制
        private String numberLimit;
        // 通程产品服务说明
        private String description;
        // 通程舱位对应信息
        private List<SeatDetail> seatDetails;
    }

    @Data
    public class PriceData {
        // 乘机人类型 1-成人 2-儿童 3-婴儿(暂不使用)
        private String crewType;
        // 票面价
        private Integer price;
        // 单人结算价 (price * (1 - commisionPoint%) - commisionMoney) 不含税
        private Double settlement;
        // 单人机建
        private Integer airportTax;
        // 单人燃油费
        private Integer fuelTax;
        // 返点 0.1=0.1%
        private Double commisionPoint;
        // 定额
        private Double commisionMoney;
        // 支付手续费
        private Double payFee;
        // 是否换编码出票 1-原编码出票 2-需要换编码出票
        private Integer isSwitchPnr;
        // 政策类型 BSP或者B2B
        private String policyType;
        // 出票效率
        private String ticketSpeed;
    }

    @Data
    public class SeatDetail {
        // 出发三字码
        private String depCode;
        // 抵达三字码
        private String arrCode;
        // 座位
        private String seatCode;
        // 座位数
        private String seatClassStatus;
        // 价格
        private Double ticketPrice;
    }

    @Data
    public class StopInfo {
        // 城市三字码
        private String city;
        // 城市名称
        private String cityName;
        // 城市类型
        private String cityType;
        // 起飞时间
        private String depTime;
        // 降落时间
        private String arrTime;
    }
}
