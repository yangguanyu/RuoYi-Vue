package com.ruoyi.service.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel(value = "RealNameAuthenticationInfoVo")
public class RealNameAuthenticationInfoVo {
    @ApiModelProperty(value = "真实姓名")
    private String realName;
    @ApiModelProperty(value = "证件号码")
    private String cardNum;
    @ApiModelProperty(value = "手机号码")
    private String phonenumber;
}
