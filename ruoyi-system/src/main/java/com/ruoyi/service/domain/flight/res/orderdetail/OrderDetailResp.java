package com.ruoyi.service.domain.flight.res.orderdetail;

import lombok.Data;

import java.util.List;

@Data
public class OrderDetailResp {
    // 订单号
    private String orderNo;

    // 商家订单号
    private String outOrderNo;

    // 订单创建时间：格式为YYYY-MM-DD HH:MM:SS，例如：2018-10-12 12:22:00
    private String createTime;

    // 航班信息，类型为List<Segment>，包含多个航班段信息
    private List<SegmentResp> segments;

    // 订单信息，类型为List<SubOrder>，包含多个子订单信息
    private List<SubOrderResp> subOrders;

    // 支付数据，类型为PayData，仅当订单已支付时存在
    private PayDataResp payData;

    // 最后支付时间，需要在该时间之前完成支付
    private String latestPayTime;

    // 订单状态：1表示新预定等待支付，3表示支付成功，5表示芭票，6表示已取消，其他状态表示有退款待定、有变更待定等
    private int orderStatus;

    // 订单创建人
    private String createdBy;
}
