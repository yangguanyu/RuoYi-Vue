package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "CheckSeatAndPriceParamSegmentDto")
public class CheckSeatAndPriceParamSegmentDto {
    @ApiModelProperty("航班日期(yyyy-MM-dd)")
    private String depDate;
    @ApiModelProperty("航班到达日期(yyyy-MM-dd)")
    private String arrDate;
    @ApiModelProperty("必须机场三字码")
    private String depCode;
    @ApiModelProperty("必须机场三字码")
    private String arrCode;
    @ApiModelProperty("航班号：CA1180")
    private String flightNo;
    @ApiModelProperty("舱位如：Y")
    private String cabin;
    @ApiModelProperty("坐位状态 A-0")
    private String cabinStatus;
    @ApiModelProperty("乘客类型 1.成人 2.儿童 3.婴儿暂不支持")
    private Integer passengerType;
    @ApiModelProperty("票面价格")
    private Integer price;
    @ApiModelProperty("单人结算价 (price * (1 - commisionPoint%) - commisionMoney) 不含税")
    private Double settlement;
    @ApiModelProperty("单人机建")
    private Integer airportTax;
    @ApiModelProperty("单人燃油费")
    private Integer fuelTax;
}
