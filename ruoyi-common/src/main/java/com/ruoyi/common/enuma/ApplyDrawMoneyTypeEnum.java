package com.ruoyi.common.enuma;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ApplyDrawMoneyTypeEnum implements BaseEnum{
    PART(0, "非全部提现"),
    ALL(1, "全部提现"),
    ;

    private Integer value;
    private String desc;
}
