package com.ruoyi.service.service.impl;

import java.util.List;

import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.DcListUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.service.dto.TfpPurchaseShopNumberDto;
import com.ruoyi.common.enuma.DeleteEnum;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpPurchaseShopNumberMapper;
import com.ruoyi.service.domain.TfpPurchaseShopNumber;
import com.ruoyi.service.service.ITfpPurchaseShopNumberService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 购物店数量Service业务层处理
 *
 * @author ruoyi
 * @date 2023-10-31
 */
@Service
@Transactional
public class TfpPurchaseShopNumberServiceImpl implements ITfpPurchaseShopNumberService
{
    @Autowired
    private TfpPurchaseShopNumberMapper tfpPurchaseShopNumberMapper;

    /**
     * 查询购物店数量
     *
     * @param id 购物店数量主键
     * @return 购物店数量
     */
    @Override
    public TfpPurchaseShopNumber selectTfpPurchaseShopNumberById(Long id)
    {
        return tfpPurchaseShopNumberMapper.selectTfpPurchaseShopNumberById(id);
    }

    /**
     * 查询购物店数量列表
     *
     * @param tfpPurchaseShopNumber 购物店数量
     * @return 购物店数量
     */
    @Override
    public List<TfpPurchaseShopNumber> selectTfpPurchaseShopNumberList(TfpPurchaseShopNumber tfpPurchaseShopNumber)
    {
        return tfpPurchaseShopNumberMapper.selectTfpPurchaseShopNumberList(tfpPurchaseShopNumber);
    }

    /**
     * 新增购物店数量
     *
     * @param tfpPurchaseShopNumber 购物店数量
     * @return 结果
     */
    @Override
    public int insertTfpPurchaseShopNumber(TfpPurchaseShopNumber tfpPurchaseShopNumber)
    {
        tfpPurchaseShopNumber.setCreateTime(DateUtils.getNowDate());
        return tfpPurchaseShopNumberMapper.insertTfpPurchaseShopNumber(tfpPurchaseShopNumber);
    }

    /**
     * 修改购物店数量
     *
     * @param tfpPurchaseShopNumber 购物店数量
     * @return 结果
     */
    @Override
    public int updateTfpPurchaseShopNumber(TfpPurchaseShopNumber tfpPurchaseShopNumber)
    {
        tfpPurchaseShopNumber.setUpdateTime(DateUtils.getNowDate());
        return tfpPurchaseShopNumberMapper.updateTfpPurchaseShopNumber(tfpPurchaseShopNumber);
    }

    /**
     * 批量删除购物店数量
     *
     * @param ids 需要删除的购物店数量主键
     * @return 结果
     */
    @Override
    public int deleteTfpPurchaseShopNumberByIds(Long[] ids)
    {
        return tfpPurchaseShopNumberMapper.deleteTfpPurchaseShopNumberByIds(ids);
    }

    /**
     * 删除购物店数量信息
     *
     * @param id 购物店数量主键
     * @return 结果
     */
    @Override
    public int deleteTfpPurchaseShopNumberById(Long id)
    {
        return tfpPurchaseShopNumberMapper.deleteTfpPurchaseShopNumberById(id);
    }

    @Override
    public void deleteByProductId(Long productId) {
        tfpPurchaseShopNumberMapper.deleteByProductId(productId);
    }

    @Override
    public void saveList(Long productId, List<TfpPurchaseShopNumberDto> purchaseList) {
        if (DcListUtils.isNotEmpty(purchaseList)){
            LoginUser loginUser = SecurityUtils.getLoginUser();

            for (TfpPurchaseShopNumberDto tfpPurchaseShopNumberDto : purchaseList) {
                TfpPurchaseShopNumber tfpPurchaseShopNumber = new TfpPurchaseShopNumber();
                BeanUtils.copyProperties(tfpPurchaseShopNumberDto, tfpPurchaseShopNumber);
                tfpPurchaseShopNumber.setId(null);
                tfpPurchaseShopNumber.setProductId(productId);
                tfpPurchaseShopNumber.setDelFlag(DeleteEnum.EXIST.getValue());
                tfpPurchaseShopNumber.setCreateUserId(loginUser.getUserId());
                tfpPurchaseShopNumber.setCreateBy(loginUser.getUsername());
                tfpPurchaseShopNumber.setCreateTime(DateUtils.getNowDate());
                tfpPurchaseShopNumberMapper.insertTfpPurchaseShopNumber(tfpPurchaseShopNumber);
            }
        }
    }

    @Override
    public List<TfpPurchaseShopNumber> selectListByProductId(Long productId) {
        return tfpPurchaseShopNumberMapper.selectListByProductId(productId);
    }

    @Override
    public void deleteTfpPurchaseShopNumberByProductIds(List<Long> productIdList) {
        tfpPurchaseShopNumberMapper.deleteTfpPurchaseShopNumberByProductIds(productIdList);
    }
}
