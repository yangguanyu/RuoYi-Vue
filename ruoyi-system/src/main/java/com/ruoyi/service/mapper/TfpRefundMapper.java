package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.TfpRefund;
import com.ruoyi.service.dto.TfpRefundDto;
import com.ruoyi.service.vo.TfpRefundVo;
import org.apache.ibatis.annotations.Param;

/**
 * 退款Mapper接口
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public interface TfpRefundMapper 
{
    /**
     * 查询退款
     * 
     * @param id 退款主键
     * @return 退款
     */
    public TfpRefund selectTfpRefundById(Long id);

    /**
     * 查询退款列表
     * 
     * @param tfpRefund 退款
     * @return 退款集合
     */
    public List<TfpRefund> selectTfpRefundList(TfpRefund tfpRefund);

    /**
     * 新增退款
     * 
     * @param tfpRefund 退款
     * @return 结果
     */
    public int insertTfpRefund(TfpRefund tfpRefund);

    /**
     * 修改退款
     * 
     * @param tfpRefund 退款
     * @return 结果
     */
    public int updateTfpRefund(TfpRefund tfpRefund);

    /**
     * 删除退款
     * 
     * @param id 退款主键
     * @return 结果
     */
    public int deleteTfpRefundById(Long id);

    /**
     * 批量删除退款
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpRefundByIds(Long[] ids);

    List<Long> selectRefundUserIdList();

    List<TfpRefundVo> selectTfpRefundVoList(TfpRefundDto tfpRefundDto);

    List<TfpRefund> selectCompleteTfpRefundListByRefundType(@Param("refundType") Integer refundType);

    List<TfpRefund> selectCompleteTfpRefundListByOrderIdList(@Param("orderIdList") List<Long> orderIdList);

    TfpRefund selectWaitAuditTfpRefundByOrderId(@Param("orderId") Long orderId);

    TfpRefund selectCompleteTfpRefundByOrderId(@Param("orderId") Long orderId);
}
