package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.TfpProductDepartureDate;
import org.apache.ibatis.annotations.Param;

/**
 * 商品班期Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-17
 */
public interface TfpProductDepartureDateMapper 
{
    /**
     * 查询商品班期
     * 
     * @param id 商品班期主键
     * @return 商品班期
     */
    public TfpProductDepartureDate selectTfpProductDepartureDateById(Long id);

    /**
     * 查询商品班期列表
     * 
     * @param tfpProductDepartureDate 商品班期
     * @return 商品班期集合
     */
    public List<TfpProductDepartureDate> selectTfpProductDepartureDateList(TfpProductDepartureDate tfpProductDepartureDate);

    /**
     * 新增商品班期
     * 
     * @param tfpProductDepartureDate 商品班期
     * @return 结果
     */
    public int insertTfpProductDepartureDate(TfpProductDepartureDate tfpProductDepartureDate);

    /**
     * 修改商品班期
     * 
     * @param tfpProductDepartureDate 商品班期
     * @return 结果
     */
    public int updateTfpProductDepartureDate(TfpProductDepartureDate tfpProductDepartureDate);

    /**
     * 删除商品班期
     * 
     * @param id 商品班期主键
     * @return 结果
     */
    public int deleteTfpProductDepartureDateById(Long id);

    /**
     * 批量删除商品班期
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpProductDepartureDateByIds(Long[] ids);

    void deleteByProductId(@Param("productId") Long productId);

    void deleteTfpProductDepartureDateByProductIds(@Param("productIdList") List<Long> productIdList);

    List<TfpProductDepartureDate> selectTfpProductDepartureDateByProductId(@Param("productId") Long productId);

    List<TfpProductDepartureDate> selectTfpProductDepartureDateByProductIdList(@Param("productIdList") List<Long> productIdList);
}
