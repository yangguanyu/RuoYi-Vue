package com.ruoyi.service.domain.flight.param;

import lombok.Data;

@Data
public class OrderSearchParam {
    // 平台订单号
    private String orderNo;

    // 商家订单号－暂不支持
    private String outOrderNo;
}
