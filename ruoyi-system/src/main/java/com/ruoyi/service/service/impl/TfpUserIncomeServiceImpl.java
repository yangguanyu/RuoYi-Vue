package com.ruoyi.service.service.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enuma.DeleteEnum;
import com.ruoyi.common.enuma.IncomeLevelCodeEnum;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.service.service.ITfpDrawMoneyService;
import com.ruoyi.service.service.ITfpUserBankCardService;
import com.ruoyi.service.service.ITfpUserIncomeDetailService;
import com.ruoyi.service.vo.MySummoningEnvoyInfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpUserIncomeMapper;
import com.ruoyi.service.domain.TfpUserIncome;
import com.ruoyi.service.service.ITfpUserIncomeService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 用户收益Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@Service
@Transactional
public class TfpUserIncomeServiceImpl implements ITfpUserIncomeService 
{
    @Autowired
    private TfpUserIncomeMapper tfpUserIncomeMapper;
    @Autowired
    private ITfpUserIncomeDetailService userIncomeDetailService;
    @Autowired
    private ITfpDrawMoneyService drawMoneyService;
    @Autowired
    private ITfpUserBankCardService userBankCardService;

    /**
     * 查询用户收益
     * 
     * @param id 用户收益主键
     * @return 用户收益
     */
    @Override
    public TfpUserIncome selectTfpUserIncomeById(Long id)
    {
        return tfpUserIncomeMapper.selectTfpUserIncomeById(id);
    }

    /**
     * 查询用户收益列表
     * 
     * @param tfpUserIncome 用户收益
     * @return 用户收益
     */
    @Override
    public List<TfpUserIncome> selectTfpUserIncomeList(TfpUserIncome tfpUserIncome)
    {
        return tfpUserIncomeMapper.selectTfpUserIncomeList(tfpUserIncome);
    }

    /**
     * 新增用户收益
     * 
     * @param tfpUserIncome 用户收益
     * @return 结果
     */
    @Override
    public int insertTfpUserIncome(TfpUserIncome tfpUserIncome)
    {
        tfpUserIncome.setCreateTime(DateUtils.getNowDate());
        return tfpUserIncomeMapper.insertTfpUserIncome(tfpUserIncome);
    }

    /**
     * 修改用户收益
     * 
     * @param tfpUserIncome 用户收益
     * @return 结果
     */
    @Override
    public int updateTfpUserIncome(TfpUserIncome tfpUserIncome)
    {
        tfpUserIncome.setUpdateTime(DateUtils.getNowDate());
        return tfpUserIncomeMapper.updateTfpUserIncome(tfpUserIncome);
    }

    /**
     * 批量删除用户收益
     * 
     * @param ids 需要删除的用户收益主键
     * @return 结果
     */
    @Override
    public int deleteTfpUserIncomeByIds(Long[] ids)
    {
        return tfpUserIncomeMapper.deleteTfpUserIncomeByIds(ids);
    }

    /**
     * 删除用户收益信息
     * 
     * @param id 用户收益主键
     * @return 结果
     */
    @Override
    public int deleteTfpUserIncomeById(Long id)
    {
        return tfpUserIncomeMapper.deleteTfpUserIncomeById(id);
    }

    @Override
    public TfpUserIncome getUserIncomeByUserId(Long userId) {
        return tfpUserIncomeMapper.getUserIncomeByUserId(userId);
    }

    @Override
    public int saveNewInfo(Long userId, String userName) {
        TfpUserIncome userIncome = tfpUserIncomeMapper.getUserIncomeByUserId(userId);
        if (Objects.isNull(userIncome)){
            // 初始登记
            String levelCode = IncomeLevelCodeEnum.ONE.getValue();
            String levelName = IncomeLevelCodeEnum.ONE.getDesc();

            userIncome = new TfpUserIncome();
            userIncome.setUserId(userId);
            userIncome.setRemainingPrice(BigDecimal.ZERO);
            userIncome.setLevelCode(levelCode);
            userIncome.setLevelName(levelName);
            userIncome.setDelFlag(DeleteEnum.EXIST.getValue());
            userIncome.setCreateUserId(userId);
            userIncome.setCreateBy(userName);
            userIncome.setCreateTime(DateUtils.getNowDate());
            tfpUserIncomeMapper.insertTfpUserIncome(userIncome);
        }
        return 1;
    }

    /**
     * 小程序-我的召唤使者信息
     * @return
     */
    @Override
    public MySummoningEnvoyInfoVo mySummoningEnvoyInfo() {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = loginUser.getUserId();

        TfpUserIncome tfpUserIncome = tfpUserIncomeMapper.getUserIncomeByUserId(userId);

        MySummoningEnvoyInfoVo mySummoningEnvoyInfoVo = new MySummoningEnvoyInfoVo();
        mySummoningEnvoyInfoVo.setLevelName(tfpUserIncome.getLevelName());
        mySummoningEnvoyInfoVo.setRemainingPrice(tfpUserIncome.getRemainingPrice());

        // 累计收益(元) 此处为佣金累计收益，暂时不用
//        BigDecimal cumulativeIncome = userIncomeDetailService.getCumulativeIncomeByUserId(userId);
//        if (Objects.isNull(cumulativeIncome)){
//            cumulativeIncome = BigDecimal.ZERO;
//        }
//        mySummoningEnvoyInfoVo.setCumulativeIncome(cumulativeIncome);

        // 累计提现收益(元)
        BigDecimal cumulativeDrawMoneyIncome = drawMoneyService.getUserCumulativeIncome();
        mySummoningEnvoyInfoVo.setCumulativeDrawMoneyIncome(Objects.isNull(cumulativeDrawMoneyIncome) ? BigDecimal.ZERO : cumulativeDrawMoneyIncome);

        // 提现记录数量
        Integer drawMoneyRecordSize = drawMoneyService.getDrawMoneyRecordSizeByUserId(userId);
        mySummoningEnvoyInfoVo.setDrawMoneyRecordSize(Objects.isNull(drawMoneyRecordSize) ? 0 : drawMoneyRecordSize);

        // 银行卡数量
        boolean userBankCardFlag = userBankCardService.existsUserBankCardFlag(userId);
        mySummoningEnvoyInfoVo.setUserBankCardFlag(userBankCardFlag);
        return mySummoningEnvoyInfoVo;
    }

    /**
     * 小程序-获取我的余额
     * @return
     */
    @Override
    public BigDecimal getMyRemainingPrice() {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = loginUser.getUserId();

        TfpUserIncome tfpUserIncome = tfpUserIncomeMapper.getUserIncomeByUserId(userId);
        return tfpUserIncome.getRemainingPrice();
    }
}
