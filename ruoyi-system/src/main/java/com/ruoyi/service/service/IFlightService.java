package com.ruoyi.service.service;

import com.ruoyi.service.domain.flight.res.CheckSeatAndPriceResp;
import com.ruoyi.service.domain.flight.res.CreateOrderByPassengerResp;
import com.ruoyi.service.domain.flight.res.FlightSearchResp;
import com.ruoyi.service.dto.CheckSeatAndPriceParamDto;
import com.ruoyi.service.dto.CreateOrderByPassengerDto;
import com.ruoyi.service.dto.FlightSearchParamDto;
import com.ruoyi.service.vo.TfpFlightVo;

import java.util.List;

public interface IFlightService {
    List<TfpFlightVo> searchFlight(FlightSearchParamDto flightSearchParamDto);

    CheckSeatAndPriceResp checkSeatAndPrice(CheckSeatAndPriceParamDto checkSeatAndPriceParamDto);

    CreateOrderByPassengerResp createOrderByPassenger(CreateOrderByPassengerDto createOrderByPassengerDto);
}
