package com.ruoyi.service.vo;

import lombok.Data;

@Data
public class CustomerServiceInfoVo {
    private Long id;
    private String telephone;
}
