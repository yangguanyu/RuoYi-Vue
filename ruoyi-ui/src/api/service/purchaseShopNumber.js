import request from '@/utils/request'

// 查询购物店数量列表
export function listPurchaseShopNumber(query) {
  return request({
    url: '/service/purchaseShopNumber/list',
    method: 'get',
    params: query
  })
}

// 查询购物店数量详细
export function getPurchaseShopNumber(id) {
  return request({
    url: '/service/purchaseShopNumber/' + id,
    method: 'get'
  })
}

// 新增购物店数量
export function addPurchaseShopNumber(data) {
  return request({
    url: '/service/purchaseShopNumber',
    method: 'post',
    data: data
  })
}

// 修改购物店数量
export function updatePurchaseShopNumber(data) {
  return request({
    url: '/service/purchaseShopNumber',
    method: 'put',
    data: data
  })
}

// 删除购物店数量
export function delPurchaseShopNumber(id) {
  return request({
    url: '/service/purchaseShopNumber/' + id,
    method: 'delete'
  })
}
