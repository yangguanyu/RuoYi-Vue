package com.ruoyi.service.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "ComateVo", description = "出行人信息")
public class ComateVo {
    @ApiModelProperty("出行人id")
    private Long mateId;
    @ApiModelProperty("姓名")
    private String mateName;
    @ApiModelProperty("证件类型 1.身份证 2.台胞证 3.港澳通行证 4.护照 5.士兵证")
    private Integer mateCardType;
    @ApiModelProperty("证件号码")
    private String mateCardNum;
    @ApiModelProperty("手机号码")
    private String matePhone;
    @ApiModelProperty("是否实名认证 0-否 1-是")
    private Integer realNameAuthenticationFlag;
}
