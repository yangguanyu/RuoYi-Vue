package com.ruoyi.service.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
@ApiModel(value = "RefundDetailVo", description = "退款详情vo")
public class RefundDetailVo {
    @ApiModelProperty("退款id")
    private Long id;
    @ApiModelProperty("商品名称")
    private String productName;

    @ApiModelProperty("服务类型id")
    private Long categoryId;
    @ApiModelProperty("服务类型名称")
    private String categoryName;
    @ApiModelProperty("线路亮点集合")
    private List<String> routeHighlightList;

    @ApiModelProperty("订单编号")
    private String orderNo;
    @ApiModelProperty("交易快照id")
    private Long snapshotId;
    @ApiModelProperty("创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    @ApiModelProperty("支付金额")
    private BigDecimal finalAmount;

    @ApiModelProperty("退款编号")
    private String serialNo;
    @ApiModelProperty("申请退款时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date applyTime;
    @ApiModelProperty("申请退款原因")
    private String reason;
    @ApiModelProperty("退款金额")
    private BigDecimal refundAmount;
    @ApiModelProperty("退款时间")
    @JsonFormat(pattern = "yyyy年MM月dd日")
    private Date refundTime;

    @ApiModelProperty("出发时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date goDate;
    @ApiModelProperty("出发地名称")
    private String goPlaceName;

    @ApiModelProperty("同行人姓名集合")
    private List<String> mateNameList;
}
