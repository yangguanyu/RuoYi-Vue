package com.ruoyi.service.vo;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.List;

/**
 * 航班信息对象 tfp_flight
 * 
 * @author ruoyi
 * @date 2024-04-10
 */
@Data
public class TfpFlightVo extends BaseEntity
{
    /** 主键 */
    private Long id;

    /** 航班号 MU5318 */
    private String flightNo;

    /** 起飞时间 yyyy-MM-dd hh:mm:ss */
    private String depTime;

    /** 起飞机场三字码 */
    private String depCode;

    /** 到达机场 */
    private String arrCode;

    /** 抵达时间 yyyy-MM-dd hh:mm:ss */
    private String arrTime;

    /** 出发航站楼 T1 */
    private String depJetquay;

    /** 抵达航站楼 T1 */
    private String arrJetquay;

    /** 经停次数 数字代表次数 0-直达 1-经停1次 */
    private String stopNum;

    /** 机型 */
    private String planeType;

    /** 飞机大小 1-大 2-中 3-小 */
    private Integer planeSize;

    /** 中文机型描述 */
    private String planeCnName;

    /** 飞行里程 800 */
    private String distance;

    /** 基础价格 */
    private Integer basePrics;

    /** 餐食标识 */
    private String meal;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    private Long createUserId;

    /** 更新者id */
    private Long updateUserId;

    // 经停信息
    private List<TfpFlightStopInfoVo> stopInfos;
    // 舱位信息
    private List<TfpFlightSeatVo> seatItems;
    // 通程前后航段信息
    private List<TfpFlightSegmentDetailVo> segmentDetails;
}
