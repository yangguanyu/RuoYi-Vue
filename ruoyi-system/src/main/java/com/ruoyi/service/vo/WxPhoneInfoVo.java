package com.ruoyi.service.vo;

import lombok.Data;

@Data
public class WxPhoneInfoVo {
    private String phoneNumber;
    private String purePhoneNumber;
    private String countryCode;
    private WxWatermarkVo watermark;
}
