package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel
public class MoreGroupListDto {
    @ApiModelProperty("产品id")
    private Long productId;
    @ApiModelProperty("日期")
    private Date date;
}
