package com.ruoyi.service.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "DetailUserBankCardVo", description = "用户银行卡信息")
public class DetailUserBankCardVo {
    @ApiModelProperty("银行卡id")
    private Long id;
    @ApiModelProperty("银行名称")
    private String bankName;
    @ApiModelProperty("银行卡号")
    private String bankCardNumber;
    @ApiModelProperty("开户网点")
    private String accountOpeningBranch;
    @ApiModelProperty("账户姓名")
    private String userName;
    @ApiModelProperty("预留电话")
    private String phoneNumber;
}
