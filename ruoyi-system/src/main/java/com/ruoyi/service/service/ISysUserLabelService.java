package com.ruoyi.service.service;

import java.util.List;
import com.ruoyi.service.domain.SysUserLabel;
import com.ruoyi.service.dto.UserLabelDto;

/**
 * 用户个性标签Service接口
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public interface ISysUserLabelService 
{
    /**
     * 查询用户个性标签
     * 
     * @param id 用户个性标签主键
     * @return 用户个性标签
     */
    public SysUserLabel selectSysUserLabelById(Long id);

    /**
     * 查询用户个性标签列表
     * 
     * @param sysUserLabel 用户个性标签
     * @return 用户个性标签集合
     */
    public List<SysUserLabel> selectSysUserLabelList(SysUserLabel sysUserLabel);

    /**
     * 新增用户个性标签
     * 
     * @param sysUserLabel 用户个性标签
     * @return 结果
     */
    public int insertSysUserLabel(SysUserLabel sysUserLabel);

    /**
     * 修改用户个性标签
     * 
     * @param sysUserLabel 用户个性标签
     * @return 结果
     */
    public int updateSysUserLabel(SysUserLabel sysUserLabel);

    /**
     * 批量删除用户个性标签
     * 
     * @param ids 需要删除的用户个性标签主键集合
     * @return 结果
     */
    public int deleteSysUserLabelByIds(Long[] ids);

    /**
     * 删除用户个性标签信息
     * 
     * @param id 用户个性标签主键
     * @return 结果
     */
    public int deleteSysUserLabelById(Long id);

    void saveList(Long userId, List<UserLabelDto> userLabelList);
}
