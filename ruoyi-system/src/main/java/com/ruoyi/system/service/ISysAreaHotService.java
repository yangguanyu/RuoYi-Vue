package com.ruoyi.system.service;

import com.ruoyi.common.area.domain.entity.SysAreaHot;

import java.util.List;

/**
 * 地区热门推荐Service接口
 * 
 * @author ruoyi
 * @date 2023-10-17
 */
public interface ISysAreaHotService 
{
    /**
     * 查询地区热门推荐
     * 
     * @param id 地区热门推荐主键
     * @return 地区热门推荐
     */
    public SysAreaHot selectSysAreaHotById(Long id);

    /**
     * 查询地区热门推荐列表
     * 
     * @param sysAreaHot 地区热门推荐
     * @return 地区热门推荐集合
     */
    public List<SysAreaHot> selectSysAreaHotList(SysAreaHot sysAreaHot);

    /**
     * 新增地区热门推荐
     * 
     * @param sysAreaHot 地区热门推荐
     * @return 结果
     */
    public int insertSysAreaHot(SysAreaHot sysAreaHot);

    /**
     * 修改地区热门推荐
     * 
     * @param sysAreaHot 地区热门推荐
     * @return 结果
     */
    public int updateSysAreaHot(SysAreaHot sysAreaHot);

    /**
     * 批量删除地区热门推荐
     * 
     * @param ids 需要删除的地区热门推荐主键集合
     * @return 结果
     */
    public int deleteSysAreaHotByIds(String ids);

    /**
     * 删除地区热门推荐信息
     * 
     * @param id 地区热门推荐主键
     * @return 结果
     */
    public int deleteSysAreaHotById(Long id);

    /**
     * 通过国家类型获取热门区域信息
     * @param countryType
     * @return
     */
    List<SysAreaHot> getAreaHotListByCountryType(Integer countryType);
}
