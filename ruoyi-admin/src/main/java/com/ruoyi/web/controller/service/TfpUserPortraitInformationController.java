package com.ruoyi.web.controller.service;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.TfpUserPortraitInformation;
import com.ruoyi.service.service.ITfpUserPortraitInformationService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 用户肖像信息Controller
 * 
 * @author ruoyi
 * @date 2023-11-01
 */
@RestController
@RequestMapping("/service/userPortraitInformation")
public class TfpUserPortraitInformationController extends BaseController
{
    @Autowired
    private ITfpUserPortraitInformationService tfpUserPortraitInformationService;

    /**
     * 查询用户肖像信息列表
     */
    @PreAuthorize("@ss.hasPermi('service:userPortraitInformation:list')")
    @GetMapping("/list")
    public TableDataInfo list(TfpUserPortraitInformation tfpUserPortraitInformation)
    {
        startPage();
        List<TfpUserPortraitInformation> list = tfpUserPortraitInformationService.selectTfpUserPortraitInformationList(tfpUserPortraitInformation);
        return getDataTable(list);
    }

    /**
     * 导出用户肖像信息列表
     */
    @PreAuthorize("@ss.hasPermi('service:userPortraitInformation:export')")
    @Log(title = "用户肖像信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TfpUserPortraitInformation tfpUserPortraitInformation)
    {
        List<TfpUserPortraitInformation> list = tfpUserPortraitInformationService.selectTfpUserPortraitInformationList(tfpUserPortraitInformation);
        ExcelUtil<TfpUserPortraitInformation> util = new ExcelUtil<TfpUserPortraitInformation>(TfpUserPortraitInformation.class);
        util.exportExcel(response, list, "用户肖像信息数据");
    }

    /**
     * 获取用户肖像信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:userPortraitInformation:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tfpUserPortraitInformationService.selectTfpUserPortraitInformationById(id));
    }

    /**
     * 新增用户肖像信息
     */
    @PreAuthorize("@ss.hasPermi('service:userPortraitInformation:add')")
    @Log(title = "用户肖像信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TfpUserPortraitInformation tfpUserPortraitInformation)
    {
        return toAjax(tfpUserPortraitInformationService.insertTfpUserPortraitInformation(tfpUserPortraitInformation));
    }

    /**
     * 修改用户肖像信息
     */
    @PreAuthorize("@ss.hasPermi('service:userPortraitInformation:edit')")
    @Log(title = "用户肖像信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TfpUserPortraitInformation tfpUserPortraitInformation)
    {
        return toAjax(tfpUserPortraitInformationService.updateTfpUserPortraitInformation(tfpUserPortraitInformation));
    }

    /**
     * 删除用户肖像信息
     */
    @PreAuthorize("@ss.hasPermi('service:userPortraitInformation:remove')")
    @Log(title = "用户肖像信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tfpUserPortraitInformationService.deleteTfpUserPortraitInformationByIds(ids));
    }
}
