package com.ruoyi.common.utils.bean;

import com.esotericsoftware.reflectasm.ConstructorAccess;
import com.ruoyi.common.enuma.BaseEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeansException;
import org.springframework.beans.FatalBeanException;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;
import org.springframework.util.ReflectionUtils;

import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static java.lang.String.format;
import static org.springframework.beans.BeanUtils.getPropertyDescriptor;
import static org.springframework.beans.BeanUtils.getPropertyDescriptors;

/**
 * 对象 集合数据拷贝工具
 *
 * @author
 */
@Slf4j
public class CustomizeBeanCopier {
    private static final int DEFAULT_SET_PREFIX = 3;
    /**
     * 默认set方面前缀长度
     */
    private static final Map<String, ConstructorAccess> CONSTRUCTOR_ACCESS_MAP = new ConcurrentHashMap<>();

    private CustomizeBeanCopier() {
    }

    /**
     * 拷贝单个对象
     *
     * @param source      拷贝来源
     * @param targetClass 拷贝目标
     * @param <T>         拷贝发型
     * @return 返回泛型
     */
    public static <T> T copyProperties(Object source, Class<T> targetClass) {
        return copyProperties(source, targetClass, (String[]) null);
    }

    /**
     * 拷贝单个对象
     *
     * @param source           拷贝来源
     * @param targetClass      拷贝目标
     * @param ignoreProperties 忽略字段名
     * @param <T>              拷贝发型
     * @return
     */
    public static <T> T copyProperties(Object source, Class<T> targetClass, @Nullable String... ignoreProperties) {
        if (source == null) {
            return null;
        }
        T t;
        try {
            t = targetClass.getDeclaredConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            throw new RuntimeException(format("Create new instance of %s failed: %s", targetClass, e.getMessage()));
        }
        copyProperties(source, t, null, ignoreProperties);
        return t;
    }

    /**
     * 拷贝List对象
     *
     * @param sourceList  拷贝来源集合
     * @param targetClass 拷贝目标
     * @param <T>         拷贝发型
     * @return 返回泛型
     */
    public static <T> List<T> copyPropertiesOfList(List<?> sourceList, Class<T> targetClass) {
        return (List<T>) copyPropertiesOfCollection(sourceList, targetClass);
    }

    /**
     * 拷贝List对象
     *
     * @param sourceList  拷贝来源集合
     * @param targetClass 拷贝目标
     * @param <T>         拷贝发型
     * @return 返回泛型
     */
    public static <T> Collection<T> copyPropertiesOfCollection(Collection<?> sourceList, Class<T> targetClass) {
        if (sourceList == null) {
            return null;
        }
        Collection<T> resultList = null;
        try {
            resultList = sourceList.getClass().getDeclaredConstructor().newInstance();
        } catch (Exception e) {
            if (sourceList.size() == 0) {
                return new ArrayList<>();
            }
            throw new RuntimeException(format("Create new instance of %s failed: %s", sourceList.getClass(), e.getMessage()));
        }
        if (sourceList.size() == 0) {
            return resultList;
        }
        ConstructorAccess<T> constructorAccess = getConstructorAccess(targetClass);
        for (Object o : sourceList) {
            T t = null;
            try {
                if (o != null) {
                    t = constructorAccess.newInstance();
                    copyProperties(o, t);
                }
                resultList.add(t);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        return resultList;
    }

    /**
     * 拷贝对象非空属性
     *
     * @param source 拷贝数据源
     * @param target 拷贝目标
     */
    public static void copyNotNullProperties(Object source, Object target) {
        Assert.notNull(source, "Source must not be null");
        Assert.notNull(target, "Target must not be null");
        Class<?> actualEditable = target.getClass();
        PropertyDescriptor[] targetPds = getPropertyDescriptors(actualEditable);
        for (PropertyDescriptor targetPd : targetPds) {
            if (targetPd.getWriteMethod() != null) {
                PropertyDescriptor sourcePd = getPropertyDescriptor(source.getClass(), targetPd.getName());
                if (sourcePd != null && sourcePd.getReadMethod() != null) {
                    try {
                        Method readMethod = sourcePd.getReadMethod();
                        if (!Modifier.isPublic(readMethod.getDeclaringClass().getModifiers())) {
                            readMethod.setAccessible(true);
                        }
                        Object value = readMethod.invoke(source);
                        if (value != null) {
                            Method writeMethod = targetPd.getWriteMethod();
                            if (!Modifier.isPublic(writeMethod.getDeclaringClass().getModifiers())) {
                                writeMethod.setAccessible(true);
                            }
                            writeMethod.invoke(target, value);
                        }
                    } catch (Throwable ex) {
                        throw new FatalBeanException("Could not copy properties from source to target", ex);
                    }
                }
            }
        }
    }


    /**
     * Copy the property values of the given source bean into the target bean.
     * <p>Note: The source and target classes do not have to match or even be derived
     * from each other, as long as the properties match. Any bean properties that the
     * source bean exposes but the target bean does not will silently be ignored.
     * <p>This is just a convenience method. For more complex transfer needs,
     * consider using a full BeanWrapper.
     *
     * @param source the source bean
     * @param target the target bean
     * @throws BeansException if the copying failed
     * @see BeanWrapper
     */
    public static void copyProperties(Object source, Object target) throws BeansException {
        copyProperties(source, target, null, (String[]) null);
    }

    /**
     * Copy the property values of the given source bean into the given target bean.
     * <p>Note: The source and target classes do not have to match or even be derived
     * from each other, as long as the properties match. Any bean properties that the
     * source bean exposes but the target bean does not will silently be ignored.
     *
     * @param source           the source bean
     * @param target           the target bean
     * @param editable         the class (or interface) to restrict property setting to
     * @param ignoreProperties array of property names to ignore
     * @throws BeansException if the copying failed
     * @see BeanWrapper
     */
    public static void copyProperties(Object source, Object target, @Nullable Class<?> editable,
                                      @Nullable String... ignoreProperties) throws BeansException {

        Assert.notNull(source, "Source must not be null");
        Assert.notNull(target, "Target must not be null");

        Class<?> actualEditable = target.getClass();
        if (editable != null) {
            if (!editable.isInstance(target)) {
                throw new IllegalArgumentException("Target class [" + target.getClass().getName() +
                        "] not assignable to Editable class [" + editable.getName() + "]");
            }
            actualEditable = editable;
        }
        PropertyDescriptor[] targetPds = getPropertyDescriptors(actualEditable);
        List<String> ignoreList = (ignoreProperties != null ? Arrays.asList(ignoreProperties) : null);

        for (PropertyDescriptor targetPd : targetPds) {
            Method writeMethod = targetPd.getWriteMethod();
            if (writeMethod != null && (ignoreList == null || !ignoreList.contains(targetPd.getName()))) {
                PropertyDescriptor sourcePd = getPropertyDescriptor(source.getClass(), targetPd.getName());
                if (sourcePd != null) {
                    Method readMethod = sourcePd.getReadMethod();
                    boolean enumCopy = enumCopyable(writeMethod, readMethod);
                    if (readMethod != null) {
                        try {
                            if (!Modifier.isPublic(readMethod.getDeclaringClass().getModifiers())) {
                                readMethod.setAccessible(true);
                            }
                            Object value = readMethod.invoke(source);
                            if (!Modifier.isPublic(writeMethod.getDeclaringClass().getModifiers())) {
                                writeMethod.setAccessible(true);
                            }
                            if (enumCopy) {
                                copyEnum(writeMethod, target, value);
                            } else {
                                if (ClassUtils.isAssignable(Collection.class, readMethod.getReturnType()) && ClassUtils.isAssignable(Collection.class, writeMethod.getParameterTypes()[0])) {
                                    copyCollection(source, target, targetPd, value);
                                } else if (ClassUtils.isAssignable(writeMethod.getParameterTypes()[0], readMethod.getReturnType())) {
                                    writeMethod.invoke(target, value);
                                } else if (isIdf10Class(writeMethod.getParameterTypes()[0])) {
                                    writeMethod.invoke(target, copyProperties(value, writeMethod.getParameterTypes()[0]));
                                } else {
                                    log.info("Could not copy property '" + targetPd.getName() + "' from " + source.getClass().getName() + " to " + target.getClass().getName() + ", Class type not match");
                                }
                            }
                        } catch (Throwable ex) {
                            throw new FatalBeanException(
                                    "Could not copy property '" + targetPd.getName() + "' from source to target", ex);
                        }
                    }
                }
            }
        }
    }

    /**
     * 集合copy处理，支持集合类型不同（如List-Set），集合元素类型不同且是com.idf10.下面的类(List<Dto> - Set<Entity>)
     */
    private static void copyCollection(Object source, Object target, PropertyDescriptor targetPd, Object value) throws InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        if (value == null) {
            return;
        }
        Method writeMethod = targetPd.getWriteMethod();
        Collection collection = null;
        try {
            if (ClassUtils.isAssignable(writeMethod.getParameterTypes()[0], value.getClass())) {
                collection = (Collection) value.getClass().getDeclaredConstructor().newInstance();
            } else {
                collection = (Collection) writeMethod.getParameterTypes()[0].getDeclaredConstructor().newInstance();
            }
        } catch (Exception e) {
            if (ClassUtils.isAssignable(writeMethod.getParameterTypes()[0], ArrayList.class)) {
                collection = new ArrayList();
            } else if (ClassUtils.isAssignable(writeMethod.getParameterTypes()[0], HashSet.class)) {
                collection = new HashSet();
            } else {
                collection = (Collection) value.getClass().getDeclaredConstructor().newInstance();
            }
        }
        String fieldNam = targetPd.getName();
        Class<?> writeParameterizedClass = getParameterizedClass(target.getClass(), fieldNam);
        Class<?> readParameterizedClass = getParameterizedClass(source.getClass(), fieldNam);
        if (writeParameterizedClass != null && readParameterizedClass != null
                && isIdf10Class(readParameterizedClass) && isIdf10Class(writeParameterizedClass)
                && !ClassUtils.isAssignable(writeParameterizedClass, readParameterizedClass)) {
            collection.addAll(copyPropertiesOfCollection((Collection) value, writeParameterizedClass));
        } else {
            collection.addAll((Collection) value);
        }
        writeMethod.invoke(target, collection);

    }

    /**
     * 获取集合字段参数类型
     */
    private static Class<?> getParameterizedClass(Class writeClass, String field) {
        ParameterizedType listType = (ParameterizedType) ReflectionUtils.findField(writeClass, field).getGenericType();
        if (listType != null && listType.getActualTypeArguments() != null && listType.getActualTypeArguments().length > 0) {
            return (Class<?>) listType.getActualTypeArguments()[0];
        }
        return null;
    }

    /**
     * 不是原始类型或原始类型的包装类型
     */
    private static boolean isIdf10Class(Class<?> type) {
        return ClassUtils.getPackageName(type) != null && ClassUtils.getPackageName(type).startsWith("com.idf10.") && !type.isEnum() && notPrimitiveAndWrapper(type);
    }

    /**
     * 不是原始类型或原始类型的包装类型
     */
    private static boolean notPrimitiveAndWrapper(Class<?> type) {
        return !ClassUtils.isPrimitiveOrWrapper(type)
                && !ClassUtils.isPrimitiveArray(type)
                && !ClassUtils.isPrimitiveWrapperArray(type)
                && notOtherIgnoreType(type);
    }

    /**
     * 其他需要忽略的类型
     */
    private static boolean notOtherIgnoreType(Class<?> type) {
        return !ClassUtils.isAssignable(BigDecimal.class, type)
                && !(type.isArray() && ClassUtils.isAssignable(BigDecimal.class, type.getComponentType()))
                && !ClassUtils.isAssignable(BigInteger.class, type)
                && !(type.isArray() && ClassUtils.isAssignable(BigInteger.class, type.getComponentType()))
                && !String.class.equals(type)
                && !(type.isArray() && String.class.equals(type.getComponentType()));
    }


    /**
     * 枚举类型-int、byte类型转换
     *
     * @param writeMethod
     * @param target
     * @param value
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     */
    private static void copyEnum(Method writeMethod, Object target, Object value) throws InvocationTargetException, IllegalAccessException {
        if (value == null) {
            return;
        }
        Class<?> parameterType = writeMethod.getParameterTypes()[0];
        if (value instanceof BaseEnum) {
            Integer intValue = ((BaseEnum) value).getValue();
            if (ClassUtils.isAssignable(byte.class, parameterType)
                    && intValue <= Byte.MAX_VALUE && intValue >= Byte.MIN_VALUE) {
                writeMethod.invoke(target, Byte.valueOf(intValue.toString()));
            } else {
                writeMethod.invoke(target, intValue);
            }
        } else {
            BaseEnum[] enumConstants = (BaseEnum[]) parameterType.getEnumConstants();
            for (BaseEnum baseEnum : enumConstants) {
                if (baseEnum.getValue().equals(Integer.valueOf(String.valueOf(value)))) {
                    writeMethod.invoke(target, baseEnum);
                }
            }
        }
    }

    /**
     * 是否执行 枚举类型-int、byte类型转换
     *
     * @param writeMethod
     * @param readMethod
     * @return
     */
    private static boolean enumCopyable(Method writeMethod, Method readMethod) {
        if (writeMethod != null && readMethod != null) {
            Class<?> writeType = writeMethod.getParameterTypes()[0];
            Class<?> readType = readMethod.getReturnType();
            if (writeType != null && readType != null) {
                return (ClassUtils.isAssignable(BaseEnum.class, writeType) && (ClassUtils.isAssignable(int.class, readType) || ClassUtils.isAssignable(byte.class, readType)))
                        || (ClassUtils.isAssignable(BaseEnum.class, readType) && (ClassUtils.isAssignable(int.class, writeType) || ClassUtils.isAssignable(byte.class, writeType)));
            }
        }
        return false;
    }

    /**
     * gitHub开源工具，性能比JDK自带反射性能更高
     * https://github.com/EsotericSoftware/reflectasm
     *
     * @param targetClass
     * @param <T>
     * @return
     */
    private static <T> ConstructorAccess<T> getConstructorAccess(Class<T> targetClass) {
        ConstructorAccess<T> constructorAccess = CONSTRUCTOR_ACCESS_MAP.get(targetClass.toString());
        if (constructorAccess != null) {
            return constructorAccess;
        }
        try {
            constructorAccess = ConstructorAccess.get(targetClass);
            constructorAccess.newInstance();
            CONSTRUCTOR_ACCESS_MAP.put(targetClass.toString(), constructorAccess);
        } catch (Exception e) {
            throw new RuntimeException(format("Create new instance of %s failed: %s", targetClass, e.getMessage()), e);
        }
        return constructorAccess;
    }
}
