package com.ruoyi.web.controller.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.utils.DcListUtils;
import com.ruoyi.service.domain.TfpOrder;
import com.ruoyi.service.dto.SettlementDetailListDto;
import com.ruoyi.service.dto.ExportDto;
import com.ruoyi.service.dto.UserIncomeDetailManageListDto;
import com.ruoyi.service.dto.ViewShareInfoDto;
import com.ruoyi.service.excel.UserIncomeDetailExportExcel;
import com.ruoyi.service.service.ITfpDrawMoneyService;
import com.ruoyi.service.service.ITfpOrderService;
import com.ruoyi.service.vo.InvitingAchievementsVo;
import com.ruoyi.service.vo.SettlementDetailListVo;
import com.ruoyi.service.vo.UserIncomeDetailListVo;
import com.ruoyi.service.vo.UserIncomeDetailManageListVo;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.TfpUserIncomeDetail;
import com.ruoyi.service.service.ITfpUserIncomeDetailService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 用户收益明细Controller
 * 
 * @author ruoyi
 * @date 2024-01-16
 */
@RestController
@RequestMapping("/service/userIncomeDetail")
public class TfpUserIncomeDetailController extends BaseController
{
    @Autowired
    private ITfpUserIncomeDetailService tfpUserIncomeDetailService;
    @Autowired
    private ITfpOrderService tfpOrderService;
    @Autowired
    private ITfpDrawMoneyService tfpDrawMoneyService;

    /**
     * 查询用户收益明细列表
     */
    @PreAuthorize("@ss.hasPermi('service:userIncomeDetail:list')")
    @GetMapping("/list")
    public TableDataInfo list(TfpUserIncomeDetail tfpUserIncomeDetail)
    {
        startPage();
        List<TfpUserIncomeDetail> list = tfpUserIncomeDetailService.selectTfpUserIncomeDetailList(tfpUserIncomeDetail);
        return getDataTable(list);
    }

    /**
     * 导出用户收益明细列表
     */
    @PreAuthorize("@ss.hasPermi('service:userIncomeDetail:export')")
    @Log(title = "用户收益明细", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TfpUserIncomeDetail tfpUserIncomeDetail)
    {
        List<TfpUserIncomeDetail> list = tfpUserIncomeDetailService.selectTfpUserIncomeDetailList(tfpUserIncomeDetail);
        ExcelUtil<TfpUserIncomeDetail> util = new ExcelUtil<TfpUserIncomeDetail>(TfpUserIncomeDetail.class);
        util.exportExcel(response, list, "用户收益明细数据");
    }

    /**
     * 获取用户收益明细详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:userIncomeDetail:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tfpUserIncomeDetailService.selectTfpUserIncomeDetailById(id));
    }

    /**
     * 新增用户收益明细
     */
    @PreAuthorize("@ss.hasPermi('service:userIncomeDetail:add')")
    @Log(title = "用户收益明细", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TfpUserIncomeDetail tfpUserIncomeDetail)
    {
        return toAjax(tfpUserIncomeDetailService.insertTfpUserIncomeDetail(tfpUserIncomeDetail));
    }

    /**
     * 修改用户收益明细
     */
    @PreAuthorize("@ss.hasPermi('service:userIncomeDetail:edit')")
    @Log(title = "用户收益明细", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TfpUserIncomeDetail tfpUserIncomeDetail)
    {
        return toAjax(tfpUserIncomeDetailService.updateTfpUserIncomeDetail(tfpUserIncomeDetail));
    }

    /**
     * 删除用户收益明细
     */
    @PreAuthorize("@ss.hasPermi('service:userIncomeDetail:remove')")
    @Log(title = "用户收益明细", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tfpUserIncomeDetailService.deleteTfpUserIncomeDetailByIds(ids));
    }

    @ApiOperation("小程序-受邀人查看分享信息")
    @PostMapping("/viewShareInfo")
    @ResponseBody
    public R viewShareInfo(@RequestBody ViewShareInfoDto viewShareInfoDto){
        return R.ok(tfpUserIncomeDetailService.viewShareInfo(viewShareInfoDto));
    }



    @ApiOperation("小程序-获取用户累计收益")
    @PostMapping("/getUserCumulativeIncome")
    @ResponseBody
    public R<BigDecimal> getUserCumulativeIncome(){
//        return R.ok(tfpUserIncomeDetailService.getUserCumulativeIncome());
        // 产品设计为提现记录
        BigDecimal userCumulativeIncome = tfpDrawMoneyService.getUserCumulativeIncome();
        return R.ok(Objects.isNull(userCumulativeIncome) ? BigDecimal.ZERO : userCumulativeIncome);
    }

    @ApiOperation("小程序-用户收益列表")
    @PostMapping("/userIncomeDetailList")
    @ResponseBody
    public TableDataInfo<UserIncomeDetailListVo> userIncomeDetailList(){
//        return getDataTable(tfpUserIncomeDetailService.userIncomeDetailList());
        // 产品设计为提现记录
        return getDataTable(tfpDrawMoneyService.userIncomeDetailList());
    }

    @ApiOperation("小程序-结算明细")
    @PostMapping("/settlementDetailList")
    @ResponseBody
    public TableDataInfo<SettlementDetailListVo> settlementDetailList(@RequestBody SettlementDetailListDto settlementDetailListDto){
        return getDataTable(tfpUserIncomeDetailService.settlementDetailList(settlementDetailListDto));
    }

    @ApiOperation("小程序-邀请战绩")
    @PostMapping("/invitingAchievements")
    @ResponseBody
    public TableDataInfo<InvitingAchievementsVo> invitingAchievements(){
        return getDataTable(tfpUserIncomeDetailService.invitingAchievements());
    }

    @ApiOperation("后台-佣金管理")
    @PostMapping("/userIncomeDetailManageList")
    @ResponseBody
    public TableDataInfo<UserIncomeDetailManageListVo> userIncomeDetailManageList(@RequestBody UserIncomeDetailManageListDto userIncomeDetailManageListDto){
        return getDataTable(tfpUserIncomeDetailService.userIncomeDetailManageList(userIncomeDetailManageListDto));
    }

    @ApiOperation("后台-佣金管理导出")
    @PostMapping("/userIncomeDetailManageListExport")
    public void userIncomeDetailManageListExport(HttpServletResponse response, ExportDto exportDto)
    {
        List<UserIncomeDetailExportExcel> list = tfpUserIncomeDetailService.userIncomeDetailManageListExport(exportDto);
        ExcelUtil<UserIncomeDetailExportExcel> util = new ExcelUtil<UserIncomeDetailExportExcel>(UserIncomeDetailExportExcel.class);
        util.exportExcel(response, list, "佣金管理数据");
    }

    /**
     * 处理结算单结算完成任务
     */
    public void handleSettlementCompleteTask(){
        System.out.println("--------------------------------处理结算单结算完成任务--------------------------------");
        List<TfpOrder> tfpOrderList = tfpOrderService.selectOverRefundTimeCompleteOrderList();
        if (DcListUtils.isNotEmpty(tfpOrderList)){
            List<Long> orderIdList = tfpOrderList.stream().map(TfpOrder::getId).collect(Collectors.toList());
            tfpUserIncomeDetailService.handleSettlementCompleteTask(orderIdList);
        }
    }
}
