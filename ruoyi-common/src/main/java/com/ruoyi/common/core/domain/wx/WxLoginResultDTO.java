package com.ruoyi.common.core.domain.wx;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 微信code请求结果dto
 *
 * @author soonphe
 * @since 1.0
 */
@Data
public class WxLoginResultDTO {

    private String openid;
    private String unionid;
    private Integer errcode;
    private String errmsg;
    @JsonProperty("session_key")
    private String sessionKey;
}
