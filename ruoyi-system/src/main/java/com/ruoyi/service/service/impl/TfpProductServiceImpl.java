package com.ruoyi.service.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.ruoyi.common.area.domain.entity.SysArea;
import com.ruoyi.common.constant.CommonConstants;
import com.ruoyi.common.core.domain.entity.TfpAttachment;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enuma.*;
import com.ruoyi.common.exception.BusinessException;
import com.ruoyi.common.utils.*;
import com.ruoyi.service.domain.*;
import com.ruoyi.service.dto.*;
import com.ruoyi.service.service.*;
import com.ruoyi.service.vo.*;
import com.ruoyi.system.service.ISysAreaService;
import com.ruoyi.system.service.ISysUserService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.compress.utils.Lists;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpProductMapper;
import org.springframework.transaction.annotation.Transactional;

import static com.ruoyi.common.constant.CommonConstants.*;
import static com.ruoyi.common.utils.PageUtils.startPage;

/**
 * 商品主Service业务层处理
 *
 * @author ruoyi
 * @date 2023-11-01
 */
@Service
@Transactional
public class TfpProductServiceImpl implements ITfpProductService {
    @Autowired
    private TfpProductMapper tfpProductMapper;
    @Autowired
    private ITfpPurchaseShopNumberService purchaseShopNumberService;
    @Autowired
    private ITfpSelfFundedProjectService selfFundedProjectService;
    @Autowired
    private ITfpProductSubService productSubService;
    @Autowired
    private ITfpProductCbcService productCbcService;
    @Autowired
    private ITfpEveryDayPriceService everyDayPriceService;
    @Autowired
    private ISysAreaService areaService;
    @Autowired
    private ITfpCooperativeBranchCompanyService cooperativeBranchCompanyService;
    @Autowired
    private ITfpChatGroupService chatGroupService;
    @Autowired
    private ITfpProductWantToGoService productWantToGoService;
    @Autowired
    private ISysUserService userService;
    @Autowired
    private ITfpChatGroupPersonService chatGroupPersonService;
    @Autowired
    private ITfpOrderService orderService;
    @Autowired
    private ITfpAttachmentService attachmentService;
    @Autowired
    private ITfpProductDepartureDateService productDepartureDateService;
    @Autowired
    private IAirportCodesService airportCodesService;
    @Autowired
    private ITfpFlightDatePriceService tfpFlightDatePriceService;

    @Value("${home.page.carousel.chart.id}")
    private Long homePageCarouselChartId;
    @Value("${increase.percent}")
    private BigDecimal increasePercent;

    /**
     * 查询商品主
     *
     * @param id 商品主主键
     * @return 商品主
     */
    @Override
    public TfpProduct selectTfpProductById(Long id) {
        return tfpProductMapper.selectTfpProductById(id);
    }

    /**
     * 查询商品主列表
     *
     * @param tfpProduct 商品主
     * @return 商品主
     */
    @Override
    public List<TfpProduct> selectTfpProductList(TfpProduct tfpProduct) {
        return tfpProductMapper.selectTfpProductList(tfpProduct);
    }

    @Override
    public List<TfpProductVo> selectTfpProductVoList(TfpProduct tfpProduct) {
        List<TfpProductVo> tfpProductVoList = tfpProductMapper.selectTfpProductVoList(tfpProduct);
        if (DcListUtils.isNotEmpty(tfpProductVoList)) {
            List<Long> productIdList = tfpProductVoList.stream().map(TfpProductVo::getId).collect(Collectors.toList());
            List<TfpEveryDayPrice> tfpEveryDayPriceList = everyDayPriceService.selectDefaultTfpEveryDayPriceListByProductIdList(productIdList);
            Map<Long, TfpEveryDayPrice> everyDayPriceMap = new HashMap<>();
            if (DcListUtils.isNotEmpty(tfpEveryDayPriceList)) {
                everyDayPriceMap = tfpEveryDayPriceList.stream().collect(Collectors.toMap(TfpEveryDayPrice::getProductId, TfpEveryDayPrice -> TfpEveryDayPrice));
            }

            productIdList = tfpProductVoList.stream().map(TfpProductVo::getId).collect(Collectors.toList());
            Map<Long, List<TfpCooperativeBranchCompany>> cooperativeBranchCompanyMap = cooperativeBranchCompanyService.selectCooperativeBranchCompanyMapByProductIdList(productIdList);

            for (TfpProductVo tfpProductVo : tfpProductVoList) {
                Long id = tfpProductVo.getId();
                TfpEveryDayPrice tfpEveryDayPrice = everyDayPriceMap.get(id);
                if (tfpEveryDayPrice != null) {
                    tfpProductVo.setExpectedNumber(tfpEveryDayPrice.getExpectedNumber());
                }

                List<String> cooperativeBranchCompanyNameList = new ArrayList<>();
                List<TfpCooperativeBranchCompany> tfpCooperativeBranchCompanyList = cooperativeBranchCompanyMap.get(id);
                if (DcListUtils.isNotEmpty(tfpCooperativeBranchCompanyList)) {
                    for (TfpCooperativeBranchCompany tfpCooperativeBranchCompany : tfpCooperativeBranchCompanyList) {
                        cooperativeBranchCompanyNameList.add(tfpCooperativeBranchCompany.getName());
                    }
                }

                tfpProductVo.setCooperativeBranchCompanyNameList(cooperativeBranchCompanyNameList);
            }
        }

        return tfpProductVoList;
    }

    /**
     * 定时任务
     * 具体定时任务配置可查看若依文档，非常详细
     *
     * @param
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    public void sortProduct() {
        TfpProduct tfpProduct = new TfpProduct();
        //查询两个月以内的商品
        tfpProduct.setTaskDateTime(DateUtils.getRecommendDate());
        List<TfpProductVo> tfpProductVoList = tfpProductMapper.selectTfpProductVoList(tfpProduct);
        tfpProductVoList = tfpProductVoList.stream().sorted(Comparator.comparing(TfpProductVo::getId)).collect(Collectors.toList());
        List<Long> productIds = tfpProductVoList.stream().map(TfpProductVo::getId).collect(Collectors.toList());
        //获取群组信息，做表联查
        List<ChatGroupStatisticsDto> chatGroupStatisticsDtos = chatGroupService.selectGroupPersonList(productIds);
        //获取所有标签，以后再用
        //List<TfpLabel> tfpLabels = labelService.selectTfpLabelList(new TfpLabel());
        for (TfpProductVo tfpProductVo : tfpProductVoList) {
            //定义一个分数，int类型即可
            Integer socre = CommonConstants.ZERO;
            List<ChatGroupStatisticsDto> groupStatisticsDtoList = chatGroupStatisticsDtos.stream()
                    .filter(e -> Objects.nonNull(e.getProductId()) &&
                            e.getProductId().equals(tfpProductVo.getId()))
                    .collect(Collectors.toList());
            //判断是自营和一家一团为20000分,全国散分数为10000分（加分到19999封顶）
            if (tfpProductVo.getCategoryId() == 1) {
                //全国散
                socre += TEN_THOUSAND;
                Long reportCount = groupStatisticsDtoList.stream().filter(e -> Objects.nonNull(e.getUserType()) && (ChatUserTypeEnum.APPLIED.getValue() == e.getUserType() || ChatUserTypeEnum.BOOT_MAN.getValue() == e.getUserType())).count();
                if (reportCount > CommonConstants.ZERO) {
                    //已有报名的为2000分，每多一个在此基础上+10
                    socre += reportCount.intValue() * TEN + TWO_THOUSAND;
                } else {
                    //全国散：没有报名，想去人数，每多一个想去人数+5分，（2000分封顶）
                }
            } else {
                //一家一团和自营
                socre += TWENTY_THOUSAND;
                Integer count = groupStatisticsDtoList.size();
                if (count > CommonConstants.ZERO) {
                    //判断报名小组有没有，有多少人：已有报名小组5000分
                    socre += FIVE_THOUSAND;
                    //获取报名的人数 每多一人报名再此基础上+10分
                    Long reportCount = groupStatisticsDtoList.stream().filter(e -> Objects.nonNull(e.getUserType()) && (ChatUserTypeEnum.APPLIED.getValue() == e.getUserType() || ChatUserTypeEnum.BOOT_MAN.getValue() == e.getUserType())).count();
                    socre += reportCount.intValue() * TEN;
                } else {
                    //获取想去的人数 没有报名小组，想去人数多，每多一个想去人数+5分（5000分封顶）
                }
            }
            //最后，更新分数
            TfpProduct updateProductEntiry = new TfpProduct();
            updateProductEntiry.setId(tfpProductVo.getId());
            updateProductEntiry.setScore(Long.valueOf(socre));
            tfpProductMapper.updateTfpProduct(updateProductEntiry);
        }

    }

    /**
     * 新增商品主
     *
     * @param tfpProductDto 商品主
     * @return 结果
     */
    @Override
    public int insertTfpProduct(TfpProductDto tfpProductDto) {
        LoginUser loginUser = SecurityUtils.getLoginUser();

        this.judgeSaveProduct(tfpProductDto, null);

        TfpProduct tfpProduct = new TfpProduct();
        BeanUtils.copyProperties(tfpProductDto, tfpProduct);

        // 临时处理区域名称存储
        this.tempHandleArea(tfpProduct);

        // 判断城市三字码
        this.judgeCityThreeCode(tfpProduct);

        Integer lowPriceAirTicketServiceFlag = tfpProductDto.getLowPriceAirTicketServiceFlag();
        if (Objects.isNull(lowPriceAirTicketServiceFlag)){
            lowPriceAirTicketServiceFlag  = YesOrNoFlagEnum.NO.getValue();
        }
        tfpProduct.setLowPriceAirTicketServiceFlag(lowPriceAirTicketServiceFlag);

        tfpProduct.setCategoryName(BaseLongEnum.getDescByCode(ProductCategoryEnum.class, tfpProduct.getCategoryId()));

        // 0-待审核 1-审核通过 2-审核不通过 3-草稿
        tfpProduct.setItemStatus(ProductItemStatusEnum.AUDIT_PASS.getValue());
        // 产品上下架状态字段 0-新增待上架 1-已上架 2-下架待审批 3-已下架
        tfpProduct.setSalesStatus(0);
        tfpProduct.setAirTicketPriceQueryCompleteFlag(YesOrNoFlagEnum.NO.getValue());
        tfpProduct.setSubmitAirTicketPriceQueryFlag(YesOrNoFlagEnum.NO.getValue());
        tfpProduct.setDelFlag(DeleteEnum.EXIST.getValue());
        tfpProduct.setCreateUserId(loginUser.getUserId());
        tfpProduct.setCreateBy(loginUser.getUsername());
        tfpProduct.setCreateTime(DateUtils.getNowDate());
        tfpProductMapper.insertTfpProduct(tfpProduct);

        Long id = tfpProduct.getId();

        TfpProductSub tfpProductSub = new TfpProductSub();
        BeanUtils.copyProperties(tfpProductDto, tfpProductSub);
        tfpProductSub.setProductId(id);
        tfpProductSub.setDelFlag(DeleteEnum.EXIST.getValue());
        tfpProductSub.setCreateUserId(loginUser.getUserId());
        tfpProductSub.setCreateBy(loginUser.getUsername());
        tfpProductSub.setCreateTime(DateUtils.getNowDate());
        productSubService.insertTfpProductSub(tfpProductSub);

        // 处理购物店，自我消费，合作分公司
        this.handleProductOtherInfo(id, tfpProductDto);

        // 保存每日价格
        TfpEveryDayPrice tfpEveryDayPrice = new TfpEveryDayPrice();
        BeanUtils.copyProperties(tfpProductDto, tfpEveryDayPrice);

        tfpEveryDayPrice.setProductId(id);
        // 类型 0-标准 1-特殊(特殊日期)
        tfpEveryDayPrice.setType(0);
        tfpEveryDayPrice.setDelFlag(DeleteEnum.EXIST.getValue());
        tfpEveryDayPrice.setCreateUserId(loginUser.getUserId());
        tfpEveryDayPrice.setCreateBy(loginUser.getUsername());
        tfpEveryDayPrice.setCreateTime(DateUtils.getNowDate());
        everyDayPriceService.insertTfpEveryDayPrice(tfpEveryDayPrice);

        // 处理班期
        List<DepartureDateDto> departureDateList = tfpProductDto.getDepartureDateList();
        productDepartureDateService.saveDepartureDateList(id, departureDateList);

        //判断全国散的时候创建一个群组
        Long categoryId = tfpProduct.getCategoryId();
        if (Objects.equals(ProductCategoryEnum.WHOLE_COUNTRY.getValue(), categoryId)
                || Objects.equals(ProductCategoryEnum.WHOLE_COUNTRY_SMALL.getValue(), categoryId)) {
            DepartureDateDto departureDateDto = departureDateList.get(0);

            TfpChatGroup tfpChatGroup = new TfpChatGroup();
            tfpChatGroup.setProductId(id);
            tfpChatGroup.setGoDate(DcDateUtils.handelDateToDayStart(departureDateDto.getDepartureDateStart()));
            tfpChatGroup.setSignUpPersonNumber(ZERO);
            tfpChatGroup.setLimitPersonNumber(INFINITE_VALUE);
            tfpChatGroup.setName(WHOLE_COUNTRY + SPACE + tfpProduct.getProductName());
            tfpChatGroup.setDelFlag(DeleteEnum.EXIST.getValue());
            tfpChatGroup.setCreateUserId(loginUser.getUserId());
            tfpChatGroup.setCreateBy(loginUser.getUsername());
            tfpChatGroup.setCreateTime(DateUtils.getNowDate());
            chatGroupService.insertTfpChatGroup(tfpChatGroup);
        }

        // 保存商品图片
        List<String> productImageUrlList = tfpProductDto.getProductImageUrlList();
        this.saveProductImageUrl(id, BusinessSubTypeEnum.PRODUCT_IMAGE.getValue(), productImageUrlList);

        List<String> routeHighlightUrlList = tfpProductDto.getRouteHighlightUrlList();
        this.saveProductImageUrl(id, BusinessSubTypeEnum.ROUTE_HIGHLIGHT_IMAGE.getValue(), routeHighlightUrlList);

        List<String> productCoverImageUrlList = tfpProductDto.getProductCoverImageUrlList();
        this.saveProductImageUrl(id, BusinessSubTypeEnum.PRODUCT_COVER_IMAGE.getValue(), productCoverImageUrlList);

        return ONE;
    }

    /**
     * 修改商品主
     *
     * @param tfpProductDto
     * @return 结果
     */
    @Override
    public int updateTfpProduct(TfpProductDto tfpProductDto) {
        LoginUser loginUser = SecurityUtils.getLoginUser();

        Long id = tfpProductDto.getId();
        TfpProduct tfpProduct = tfpProductMapper.selectTfpProductById(id);

        String productNo = tfpProduct.getProductNo();

        this.judgeSaveProduct(tfpProductDto, productNo);

        BeanUtils.copyProperties(tfpProductDto, tfpProduct);

        // 临时处理区域名称存储
        this.tempHandleArea(tfpProduct);

        // 判断城市三字码
        this.judgeCityThreeCode(tfpProduct);

        Integer lowPriceAirTicketServiceFlag = tfpProductDto.getLowPriceAirTicketServiceFlag();
        if (Objects.isNull(lowPriceAirTicketServiceFlag)){
            lowPriceAirTicketServiceFlag  = YesOrNoFlagEnum.NO.getValue();
        }
        tfpProduct.setLowPriceAirTicketServiceFlag(lowPriceAirTicketServiceFlag);

        tfpProduct.setCategoryName(BaseLongEnum.getDescByCode(ProductCategoryEnum.class, tfpProduct.getCategoryId()));

        // 0-待审核 1-审核通过 2-审核不通过 3-草稿
        tfpProduct.setItemStatus(ProductItemStatusEnum.AUDIT_PASS.getValue());
        // 产品上下架状态字段 0-新增待上架 1-已上架 2-下架待审批 3-已下架
        tfpProduct.setSalesStatus(0);
        tfpProduct.setUpdateUserId(loginUser.getUserId());
        tfpProduct.setUpdateBy(loginUser.getUsername());
        tfpProduct.setUpdateTime(DateUtils.getNowDate());
        tfpProduct.setAirTicketPriceQueryCompleteFlag(YesOrNoFlagEnum.NO.getValue());
        tfpProduct.setSubmitAirTicketPriceQueryFlag(YesOrNoFlagEnum.NO.getValue());
        tfpProductMapper.updateTfpProduct(tfpProduct);

        TfpProductSub tfpProductSub = productSubService.selectTfpProductSubByProductId(id);
        Long productSubId = tfpProductSub.getId();
        BeanUtils.copyProperties(tfpProductDto, tfpProductSub);
        tfpProductSub.setId(productSubId);
        tfpProductSub.setUpdateUserId(loginUser.getUserId());
        tfpProductSub.setUpdateBy(loginUser.getUsername());
        tfpProductSub.setUpdateTime(DateUtils.getNowDate());
        productSubService.updateTfpProductSub(tfpProductSub);

        // 处理购物店，自我消费，合作分公司
        this.handleProductOtherInfo(id, tfpProductDto);

        // 保存每日价格
        TfpEveryDayPrice tfpEveryDayPrice = everyDayPriceService.selectNormalEveryDayPriceByProductId(id);
        Long everyDayPriceId = tfpEveryDayPrice.getId();
        BeanUtils.copyProperties(tfpProductDto, tfpEveryDayPrice);

        tfpEveryDayPrice.setId(everyDayPriceId);
        tfpEveryDayPrice.setUpdateUserId(loginUser.getUserId());
        tfpEveryDayPrice.setUpdateBy(loginUser.getUsername());
        tfpEveryDayPrice.setUpdateTime(DateUtils.getNowDate());
        everyDayPriceService.updateTfpEveryDayPrice(tfpEveryDayPrice);

        // 处理班期
        productDepartureDateService.deleteByProductId(id);
        List<DepartureDateDto> departureDateList = tfpProductDto.getDepartureDateList();
        productDepartureDateService.saveDepartureDateList(id, departureDateList);

        // 保存商品图片
        attachmentService.deleteTfpAttachmentByBusinessId(id, BusinessTypeEnum.PRODUCT.getValue(), null);
        List<String> productImageUrlList = tfpProductDto.getProductImageUrlList();
        this.saveProductImageUrl(id, BusinessSubTypeEnum.PRODUCT_IMAGE.getValue(), productImageUrlList);

        List<String> routeHighlightUrlList = tfpProductDto.getRouteHighlightUrlList();
        this.saveProductImageUrl(id, BusinessSubTypeEnum.ROUTE_HIGHLIGHT_IMAGE.getValue(), routeHighlightUrlList);

        List<String> productCoverImageUrlList = tfpProductDto.getProductCoverImageUrlList();
        this.saveProductImageUrl(id, BusinessSubTypeEnum.PRODUCT_COVER_IMAGE.getValue(), productCoverImageUrlList);

        return 1;
    }

    @Override
    public int updateTfpProduct(TfpProduct tfpProduct) {
        return tfpProductMapper.updateTfpProduct(tfpProduct);
    }

    /**
     * 批量删除商品主
     *
     * @param ids 需要删除的商品主主键
     * @return 结果
     */
    @Override
    public int deleteTfpProductByIds(Long[] ids)
    {
        List<Long> idList = new ArrayList<>();
        for (Long id : ids) {
            idList.add(id);
        }

        List<TfpProduct> tfpProductList = tfpProductMapper.selectTfpProductListByIdList(idList);
        if (DcListUtils.isNotEmpty(tfpProductList)){
            this.judgeDelete(tfpProductList);

            // 删除主表
            tfpProductMapper.deleteTfpProductByIds(idList);
            // 删除商品辅对象
            productSubService.deleteTfpProductSubByProductIds(idList);
            // 删除购物店数量对象
            purchaseShopNumberService.deleteTfpPurchaseShopNumberByProductIds(idList);
            // 删除自费项目对象
            selfFundedProjectService.deleteTfpSelfFundedProjectByProductIds(idList);
            // 删除商品、合作分公司中间对象
            productCbcService.deleteTfpProductCbcByProductIds(idList);
            // 删除每日价格对象
            everyDayPriceService.deleteTfpEveryDayPriceByProductIds(idList);
            // 删除群组表
            chatGroupService.deleteTfpChatGroupByProductIds(idList);
            // 删除班期表
            productDepartureDateService.deleteTfpProductDepartureDateByProductIds(idList);
        }

        return 1;
    }

    /**
     * 删除商品主信息
     *
     * @param id 商品主主键
     * @return 结果
     */
    @Override
    public int deleteTfpProductById(Long id)
    {
        return tfpProductMapper.deleteTfpProductById(id);
    }

    @Override
    public List<TfpProduct> selectTfpProductListByIdList(List<Long> idList) {
        return tfpProductMapper.selectTfpProductListByIdList(idList);
    }

    @Override
    public TfpProductDetailVo detail(Long id) {
        TfpProductDetailVo tfpProductDetailVo = new TfpProductDetailVo();

        TfpProduct tfpProduct = tfpProductMapper.selectTfpProductById(id);
        if (Objects.isNull(tfpProduct)){
            throw new RuntimeException("商品信息不存在");
        }

        BeanUtils.copyProperties(tfpProduct, tfpProductDetailVo);
        TfpProductSub tfpProductSub = productSubService.selectTfpProductSubByProductId(id);
        BeanUtils.copyProperties(tfpProductSub, tfpProductDetailVo);

        Integer purchaseFlag = tfpProduct.getPurchaseFlag();
        if (Objects.equals(purchaseFlag, 1)){
            List<TfpPurchaseShopNumber> tfpPurchaseShopNumberList = purchaseShopNumberService.selectListByProductId(id);
            if (DcListUtils.isNotEmpty(tfpPurchaseShopNumberList)){
                List<TfpPurchaseShopNumberDto> purchaseList = new ArrayList<>();
                for (TfpPurchaseShopNumber tfpPurchaseShopNumber : tfpPurchaseShopNumberList) {
                    TfpPurchaseShopNumberDto tfpPurchaseShopNumberDto = new TfpPurchaseShopNumberDto();
                    BeanUtils.copyProperties(tfpPurchaseShopNumber, tfpPurchaseShopNumberDto);
                    purchaseList.add(tfpPurchaseShopNumberDto);
                }
                tfpProductDetailVo.setPurchaseList(purchaseList);
            }
        }

        Integer selfFundedProjectFlag = tfpProduct.getSelfFundedProjectFlag();
        if (Objects.equals(selfFundedProjectFlag, 1)){
            List<TfpSelfFundedProject> tfpSelfFundedProjectList = selfFundedProjectService.selectListByProductId(id);
            if (DcListUtils.isNotEmpty(tfpSelfFundedProjectList)){
                List<TfpSelfFundedProjectDto> selfFundedProjectList = new ArrayList<>();
                for (TfpSelfFundedProject tfpSelfFundedProject : tfpSelfFundedProjectList) {
                    TfpSelfFundedProjectDto tfpSelfFundedProjectDto = new TfpSelfFundedProjectDto();
                    BeanUtils.copyProperties(tfpSelfFundedProject, tfpSelfFundedProjectDto);
                    selfFundedProjectList.add(tfpSelfFundedProjectDto);
                }
                tfpProductDetailVo.setSelfFundedProjectList(selfFundedProjectList);
            }
        }

        List<TfpProductCbc> tfpProductCbcList = productCbcService.selectTfpProductCbcListByProductId(id);
        if (DcListUtils.isNotEmpty(tfpProductCbcList)){
            List<Long> cooperativeBranchCompanyIdList = tfpProductCbcList.stream().map(TfpProductCbc::getCbcId).collect(Collectors.toList());
            tfpProductDetailVo.setCooperativeBranchCompanyIdList(cooperativeBranchCompanyIdList);
        }

        TfpEveryDayPrice tfpEveryDayPrice = everyDayPriceService.selectNormalEveryDayPriceByProductId(id);
        BeanUtils.copyProperties(tfpEveryDayPrice, tfpProductDetailVo);

        List<TfpAttachment> productAttachmentList = attachmentService.selectListByBusinessId(id, BusinessTypeEnum.PRODUCT.getValue(), null);
        if (DcListUtils.isNotEmpty(productAttachmentList)){
            List<String> productImageUrlList = new ArrayList<>();
            List<String> routeHighlightUrlList = new ArrayList<>();
            List<String> productCoverImageUrlList = new ArrayList<>();
            for (TfpAttachment attachment : productAttachmentList) {
                String url = attachment.getUrl();
                String businessSubType = attachment.getBusinessSubType();
                if (Objects.equals(BusinessSubTypeEnum.PRODUCT_IMAGE.getValue(), businessSubType)){
                    productImageUrlList.add(url);
                }

                if (Objects.equals(BusinessSubTypeEnum.ROUTE_HIGHLIGHT_IMAGE.getValue(), businessSubType)){
                    routeHighlightUrlList.add(url);
                }

                if (Objects.equals(BusinessSubTypeEnum.PRODUCT_COVER_IMAGE.getValue(), businessSubType)){
                    productCoverImageUrlList.add(url);
                }
            }

            tfpProductDetailVo.setProductImageUrlList(productImageUrlList);
            tfpProductDetailVo.setRouteHighlightUrlList(routeHighlightUrlList);
            tfpProductDetailVo.setProductCoverImageUrlList(productCoverImageUrlList);
        }

        TfpProductDepartureDate param = new TfpProductDepartureDate();
        param.setProductId(id);
        param.setDelFlag(DeleteEnum.EXIST.getValue());
        List<TfpProductDepartureDate> tfpProductDepartureDates = productDepartureDateService.selectTfpProductDepartureDateList(param);
        if (DcListUtils.isNotEmpty(tfpProductDepartureDates)){
            List<DepartureDateVo> departureDateList = new ArrayList<>();
            for (TfpProductDepartureDate tfpProductDepartureDate : tfpProductDepartureDates) {
                DepartureDateVo departureDateVo = new DepartureDateVo();
                BeanUtils.copyProperties(tfpProductDepartureDate, departureDateVo);
                departureDateList.add(departureDateVo);
            }
            tfpProductDetailVo.setDepartureDateList(departureDateList);
        }

        tfpProductDetailVo.setId(id);
        tfpProductDetailVo.setRemark(tfpProductSub.getRemark());
        tfpProductDetailVo.setCategoryName(this.getSupplierShowCategoryName(tfpProductDetailVo.getCategoryId()));
        return tfpProductDetailVo;
    }

    /**
     * 目的地商品信息列表
     * @param cityName
     * @return
     */
    @Override
    public List<ListProductVo> listDestinationProductSelect(String cityName) {
        ListProductSelectDto listProductSelectDto = new ListProductSelectDto();
        listProductSelectDto.setCityName(cityName);
        startPage();
        return this.handleListProductVo(listProductSelectDto);
    }

    @Override
    public List<ListProductVo> handleListProductVo(ListProductSelectDto listProductSelectDto){
        List<ListProductVo> listProductVoList = tfpProductMapper.listProductSelect(listProductSelectDto);
        if (DcListUtils.isNotEmpty(listProductVoList)){
            List<Long> idList = listProductVoList.stream().map(ListProductVo::getId).collect(Collectors.toList());

            Map<Long, String> headPortraitMap = new HashMap<>();
            Map<Long, List<TfpChatGroup>> productChatGroupMap = new HashMap<>();
            Map<Long, List<TfpChatGroupPerson>> productGroupPersonMap = new HashMap<>();

            // 查询报名人数
            List<TfpChatGroup> allChatGroupList = chatGroupService.selectTfpChatGroupListByProductIdList(idList);
            if (DcListUtils.isNotEmpty(allChatGroupList)){
                productChatGroupMap = allChatGroupList.stream().collect(Collectors.groupingBy(TfpChatGroup::getProductId));

                List<Long> userIdList = new ArrayList<>();
                List<TfpChatGroupPerson> chatGroupPersonList = chatGroupPersonService.getSignUpPersonInfoListByProductIdList(idList);
                for (TfpChatGroupPerson tfpChatGroupPerson : chatGroupPersonList) {
                    Integer userType = tfpChatGroupPerson.getUserType();
                    if (Objects.equals(userType, ChatUserTypeEnum.BOOT_MAN.getValue())
                            || Objects.equals(userType, ChatUserTypeEnum.APPLIED.getValue())){
                        Long userId = tfpChatGroupPerson.getUserId();
                        if (!userIdList.contains(userId)){
                            userIdList.add(userId);
                        }

                        Long productId = tfpChatGroupPerson.getProductId();
                        List<TfpChatGroupPerson> tfpChatGroupPersonList = productGroupPersonMap.get(productId);
                        if (tfpChatGroupPersonList == null){
                            tfpChatGroupPersonList = new ArrayList<>();
                        }

                        tfpChatGroupPersonList.add(tfpChatGroupPerson);
                        productGroupPersonMap.put(productId, tfpChatGroupPersonList);
                    }
                }

                List<TfpAttachment> attchmentList = userService.getHeadPortraitListByUserIdList(userIdList);
                if (DcListUtils.isNotEmpty(attchmentList)){
                    headPortraitMap = attchmentList.stream().collect(Collectors.toMap(TfpAttachment::getBusinessId, TfpAttachment::getUrl));
                }
            }

            List<TfpAttachment> tfpAttachmentList = attachmentService.selectList(idList, BusinessTypeEnum.PRODUCT.getValue(), BusinessSubTypeEnum.PRODUCT_COVER_IMAGE.getValue());
            Map<Long, List<TfpAttachment>> productCoverImageUrlListMap = tfpAttachmentList.stream().collect(Collectors.groupingBy(TfpAttachment::getBusinessId));

            for (ListProductVo listProductVo : listProductVoList) {
                Integer signUpPersonNumber = 0;
                Long id = listProductVo.getId();
                List<TfpChatGroup> chatGroupList = productChatGroupMap.get(id);
                if (DcListUtils.isNotEmpty(chatGroupList)) {
                    for (TfpChatGroup tfpChatGroup : chatGroupList) {
                        Integer groupSignUpPersonNumber = tfpChatGroup.getSignUpPersonNumber();
                        if (Objects.nonNull(groupSignUpPersonNumber)){
                            signUpPersonNumber += groupSignUpPersonNumber;
                        }
                    }
                }

                List<String> headPortraitList = new ArrayList<>();
                List<TfpChatGroupPerson> tfpChatGroupPersonList = productGroupPersonMap.get(id);
                if (DcListUtils.isNotEmpty(tfpChatGroupPersonList)){
                    for (TfpChatGroupPerson chatGroupPerson : tfpChatGroupPersonList) {
                        Integer userType = chatGroupPerson.getUserType();
                        if (Objects.equals(userType, ChatUserTypeEnum.BOOT_MAN.getValue())
                                || Objects.equals(userType, ChatUserTypeEnum.APPLIED.getValue())){
                            String headPortrait = headPortraitMap.get(chatGroupPerson.getUserId());
                            if (headPortrait != null && !headPortraitList.contains(headPortrait)){
                                headPortraitList.add(headPortrait);
                            }
                        }
                    }
                }

                List<String> productCoverImageUrlList = new ArrayList<>();
                List<TfpAttachment> productCoverImageUrlAttachmentList = productCoverImageUrlListMap.get(id);
                if (DcListUtils.isNotEmpty(productCoverImageUrlAttachmentList)){
                    productCoverImageUrlList = productCoverImageUrlAttachmentList.stream().map(TfpAttachment::getUrl).collect(Collectors.toList());
                }

                listProductVo.setSignUpPersonHeadPortraitUrlList(headPortraitList);
                listProductVo.setSignUpPersonNumber(signUpPersonNumber);
                listProductVo.setProductCoverImageUrlList(productCoverImageUrlList);
            }
        }
        return listProductVoList;
    }

    /**
     * 小程序-商品详情
     * @param id
     * @return
     */
    @Override
    public ProductDetailVo productDetail(Long id, SnapshotTextDto snapshotTextDto) {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        if (Objects.isNull(loginUser)){
            throw new RuntimeException("用户信息不存在");
        }

        Long userId = loginUser.getUserId();

        TfpProduct tfpProduct;
        if (Objects.nonNull(snapshotTextDto)){
            tfpProduct = snapshotTextDto.getTfpProduct();
        } else {
            tfpProduct = tfpProductMapper.selectTfpProductById(id);
        }

        if (Objects.isNull(tfpProduct)){
            throw new RuntimeException("商品信息不存在");
        }

        ProductDetailVo productDetailVo = new ProductDetailVo();
        BeanUtils.copyProperties(tfpProduct, productDetailVo);

        // 想去人数 想去人数头像url集合
        productDetailVo.setWantToGoPersonNumber(0);
        productDetailVo.setWantToGoFlag(YesOrNoFlagEnum.NO.getValue());

        TfpEveryDayPrice tfpEveryDayPrice = null;
        if (Objects.nonNull(snapshotTextDto)){
            List<TfpEveryDayPrice> tfpEveryDayPriceList = snapshotTextDto.getTfpEveryDayPriceList();
            for (TfpEveryDayPrice everyDayPrice : tfpEveryDayPriceList) {
                Integer type = everyDayPrice.getType();
                if (type == 0){
                    tfpEveryDayPrice = everyDayPrice;
                }
            }
        } else {
            tfpEveryDayPrice = everyDayPriceService.selectNormalEveryDayPriceByProductId(id);
        }

        productDetailVo.setPrice(tfpEveryDayPrice.getStartingPointPrice());
        productDetailVo.setCommoditiesTypeName(BaseEnum.getDescByCode(CommoditiesTypeEnum.class, productDetailVo.getCommoditiesType()));

        List<TfpProductWantToGo> tfpProductWantToGoList;
        if (Objects.nonNull(snapshotTextDto)){
            tfpProductWantToGoList = snapshotTextDto.getTfpProductWantToGoList();
        } else {
            TfpProductWantToGo param = new TfpProductWantToGo();
            param.setProductId(id);
            param.setDelFlag(DeleteEnum.EXIST.getValue());
            tfpProductWantToGoList = productWantToGoService.selectTfpProductWantToGoList(param);
        }

        if (DcListUtils.isNotEmpty(tfpProductWantToGoList)){
            List<Long> wantToGoUserIdList = tfpProductWantToGoList.stream().map(TfpProductWantToGo::getUserId).collect(Collectors.toList());
            productDetailVo.setWantToGoPersonNumber(wantToGoUserIdList.size());

            if (wantToGoUserIdList.contains(userId)){
                productDetailVo.setWantToGoFlag(YesOrNoFlagEnum.YES.getValue());
            }

            List<TfpAttachment> attachmentList = userService.getHeadPortraitListByUserIdList(wantToGoUserIdList);
            if (DcListUtils.isNotEmpty(attachmentList)){
                List<String> wanToGoPersonHeadPortraitUrlList = attachmentList.stream().map(TfpAttachment::getUrl).collect(Collectors.toList());
                productDetailVo.setWanToGoPersonHeadPortraitUrlList(wanToGoPersonHeadPortraitUrlList);
            }
        }

        TfpProductSub tfpProductSub;
        if (Objects.nonNull(snapshotTextDto)){
            tfpProductSub = snapshotTextDto.getTfpProductSub();
        } else {
            tfpProductSub = productSubService.selectTfpProductSubByProductId(id);
        }

        List<String> routeHighlightList = new ArrayList<>();
        String routeHighlight = tfpProductSub.getRouteHighlight();
        if (routeHighlight != null){
            // 多个线路亮点根据中文分号隔开
            String[] routeHighlightArrys = routeHighlight.split("；");
            for (String routeHighlightArry : routeHighlightArrys) {
                routeHighlightList.add(routeHighlightArry);
            }
        }
        productDetailVo.setRouteHighlightList(routeHighlightList);

        // 聊天群信息集合
        List<ChatGroupInfoVo> chatGroupInfoVoList = chatGroupService.getChatGroupInfoVoListByProductId(id);
        productDetailVo.setChatGroupInfoList(chatGroupInfoVoList);

        // 日期价格集合
        if (Objects.isNull(snapshotTextDto)){
            //
            TfpProductDepartureDate param = new TfpProductDepartureDate();
            param.setProductId(id);
            param.setDelFlag(DeleteEnum.EXIST.getValue());
            List<TfpProductDepartureDate> tfpProductDepartureDateList = productDepartureDateService.selectTfpProductDepartureDateList(param);
            if (DcListUtils.isNotEmpty(tfpProductDepartureDateList)){

                // 处理OutPriceVoList 取10条最近日期的在团期范围内的数据
                List<DateCalendarVo> outletDayPriceList = this.handleOutletDayPriceList(tfpProduct, tfpProductDepartureDateList);
                productDetailVo.setOutPriceVoList(outletDayPriceList);

                // 处理DatePriceVoList 取最近包含团期的2个月数据
                List<DateMonthVO> datePriceVoList = this.handleDatePriceVoList(tfpProduct, tfpProductDepartureDateList);
                productDetailVo.setDatePriceVoList(datePriceVoList);
            }
        }

        // 商品图片
        List<TfpAttachment> attachmentList = attachmentService.selectListByBusinessId(id, BusinessTypeEnum.PRODUCT.getValue(), null);
        if (DcListUtils.isNotEmpty(attachmentList)){
            List<String> productImageUrlList = new ArrayList<>();
            List<String> routeHighlightImageUrlList = new ArrayList<>();
            List<String> productCoverImageUrlList = new ArrayList<>();
            for (TfpAttachment attachment : attachmentList) {
                String url = attachment.getUrl();
                String businessSubType = attachment.getBusinessSubType();
                if (Objects.equals(BusinessSubTypeEnum.PRODUCT_IMAGE.getValue(), businessSubType)){
                    productImageUrlList.add(url);
                }

                if (Objects.equals(BusinessSubTypeEnum.ROUTE_HIGHLIGHT_IMAGE.getValue(), businessSubType)){
                    routeHighlightImageUrlList.add(url);
                }

                if (Objects.equals(BusinessSubTypeEnum.PRODUCT_COVER_IMAGE.getValue(), businessSubType)){
                    productCoverImageUrlList.add(url);
                }
            }

            productDetailVo.setProductImageUrlList(productImageUrlList);
            productDetailVo.setRouteHighlightImageUrlList(routeHighlightImageUrlList);
            productDetailVo.setProductCoverImageUrlList(productCoverImageUrlList);
        }

        return productDetailVo;
    }

    /**
     * 日期价格集合
     * @param departureDateStart
     * @param departureDateEnd
     */
    private List<DatePriceVo> datePriceVoList(Date departureDateStart, Date departureDateEnd) {
        List<DatePriceVo> datePriceVoList = new ArrayList<>();

        DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");

        try {
            Date startDate = dateFormat.parse(dateFormat.format(departureDateStart) + "00:00:00");
            Date endDate = dateFormat.parse(dateFormat.format(departureDateEnd) + "23:59:59");

            Integer oneDay = 24 * 60 * 60 * 1000;
            // 团期总天数
            Date priceDate = startDate;
            long departureDateNumber = ((endDate.getTime() + 1 - startDate.getTime()) / oneDay) + 1;
            for (int i = 0; i < departureDateNumber; i++) {
                priceDate = new Date(priceDate.getTime() + oneDay * i);

                DatePriceVo datePriceVo = new DatePriceVo();
                datePriceVo.setDate(priceDate);
                datePriceVo.setPrice(new BigDecimal(1000));

                datePriceVoList.add(datePriceVo);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return datePriceVoList;
    }

    private List<DateMonthVO> datePriceVoList2(Date departureDateStart, Date departureDateEnd) {
        //返回信息结构
        List<DateMonthVO> dateMonthList = new ArrayList<>();
        //处理
        LocalDate startLocal = DateUtils.toLocalDate(departureDateStart);
        LocalDate endLocal = DateUtils.toLocalDate(departureDateEnd);
        int totalMonthStart = startLocal.getMonth().maxLength();
        int totalMonthEnd = endLocal.getMonth().maxLength();
        List<DateCalendarVo> dateCalendarStartList = handleSpecialDayListStart(totalMonthStart, startLocal, endLocal);
        DateMonthVO dateMonthStart = new DateMonthVO();
        dateMonthStart.setMonth(handleMonth(startLocal));
        dateMonthStart.setList(dateCalendarStartList);
        List<DateCalendarVo> dateCalendarEndList = handleSpecialDayListEnd(totalMonthEnd, startLocal, endLocal);
        DateMonthVO dateMonthEnd = new DateMonthVO();
        dateMonthEnd.setMonth(handleMonth(endLocal));
        dateMonthEnd.setList(dateCalendarEndList);
        //填装
        dateMonthList.add(dateMonthStart);
        dateMonthList.add(dateMonthEnd);
        return dateMonthList;
    }

    private String handleMonth(LocalDate localDate){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(localDate.getYear());
        stringBuilder.append("年");
        stringBuilder.append(localDate.getMonthValue());
        stringBuilder.append("月");
        return stringBuilder.toString();
    }
    private List<DateCalendarVo> handleSpecialDayListStart(int totalMonth,LocalDate startLocal,LocalDate endLocal){
        List<DateCalendarVo> dateCalendarVos = new ArrayList<>();
        for (int i=1;i<=totalMonth;i++){
            DateCalendarVo dateCalendarVo = new DateCalendarVo();
            LocalDate everyDate = LocalDate.of(startLocal.getYear(), startLocal.getMonthValue(), i);
            int dayOfWeek = everyDate.getDayOfWeek().getValue();
            int dayOfMonth = everyDate.getDayOfMonth();
            if(
                    (everyDate.isAfter(startLocal)||everyDate.isEqual(startLocal))
                            &&
                            (everyDate.isBefore(endLocal)||everyDate.isEqual(endLocal))
            ){
                dateCalendarVo.setHasPrice(Boolean.TRUE);
                dateCalendarVo.setPrice(new BigDecimal(1000));
            }
            dateCalendarVo.setDate(Date.from(everyDate.atStartOfDay(ZoneId.systemDefault()).toInstant()));
            dateCalendarVo.setDayOfMonth(dayOfMonth);
            dateCalendarVo.setDayOfWeek(dayOfWeek);
            StringBuilder builder = new StringBuilder(String.format("%02d",everyDate.getMonthValue()));
            builder.append("-");
            builder.append(String.format("%02d",everyDate.getDayOfMonth()));
            dateCalendarVo.setDay(builder.toString());
            dateCalendarVos.add(dateCalendarVo);
        }

        return dateCalendarVos;
    }

    private List<DateCalendarVo> handleSpecialDayListEnd(int totalMonth,LocalDate startLocal,LocalDate endLocal){
        List<DateCalendarVo> dateCalendarVos = new ArrayList<>();
        for (int i=1;i<=totalMonth;i++){
            DateCalendarVo dateCalendarVo = new DateCalendarVo();
            LocalDate everyDate = LocalDate.of(endLocal.getYear(), endLocal.getMonthValue(), i);
            int dayOfWeek = everyDate.getDayOfWeek().getValue();
            int dayOfMonth = everyDate.getDayOfMonth();
            if(
                    (everyDate.isAfter(startLocal)||everyDate.isEqual(startLocal))
                            &&
                            (everyDate.isBefore(endLocal)||everyDate.isEqual(endLocal))
            ){
                dateCalendarVo.setHasPrice(Boolean.TRUE);
                dateCalendarVo.setPrice(new BigDecimal(1000));
            }
            dateCalendarVo.setDate(Date.from(everyDate.atStartOfDay(ZoneId.systemDefault()).toInstant()));
            dateCalendarVo.setDayOfMonth(dayOfMonth);
            dateCalendarVo.setDayOfWeek(dayOfWeek);
            StringBuilder builder = new StringBuilder(String.format("%02d",everyDate.getMonthValue()));
            builder.append("-");
            builder.append(String.format("%02d",everyDate.getDayOfMonth()));
            dateCalendarVo.setDay(builder.toString());
            dateCalendarVos.add(dateCalendarVo);
        }

        return dateCalendarVos;
    }

    private List<DateCalendarVo> handleOutletDay(Date departureDateStart, Date departureDateEnd){
        LocalDate startLocal = DateUtils.toLocalDate(departureDateStart);
        LocalDate endLocal = DateUtils.toLocalDate(departureDateEnd);
        LocalDate now = LocalDate.now();
        //判断团期截止时间
        if(now.isAfter(endLocal)){
            return handleOutDayInner(endLocal,startLocal,endLocal);
        }
        //首先判断当前的时间是否大于等于 `startLocal`，如果大于，那么按照当前时间来，如果小于则按照开始时间（startLocal）来
        if(now.isAfter(startLocal)){
            return handleOutDayInner(now,startLocal,endLocal);
        }else {
            return handleOutDayInner(startLocal,startLocal,endLocal);
        }
    }

    private List<DateCalendarVo> handleOutDayInner(LocalDate firstDay,LocalDate startLocal,LocalDate endLocal){
        List<DateCalendarVo> dateCalendarVos = new ArrayList<>();

        for (int i=0;i<=9;i++){
            DateCalendarVo dateCalendarVo = new DateCalendarVo();
            LocalDate localDate = firstDay.plusDays(i);
            LocalDate everyDate = LocalDate.of(localDate.getYear(),localDate.getMonthValue(),localDate.getDayOfMonth());
            int dayOfWeek = everyDate.getDayOfWeek().getValue();
            int dayOfMonth = everyDate.getDayOfMonth();
            if(
                    (everyDate.isAfter(startLocal)||everyDate.isEqual(startLocal))
                            &&
                            (everyDate.isBefore(endLocal)||everyDate.isEqual(endLocal))
            ){
                dateCalendarVo.setHasPrice(Boolean.TRUE);
                dateCalendarVo.setPrice(new BigDecimal(1000));
            }
            dateCalendarVo.setDate(Date.from(everyDate.atStartOfDay(ZoneId.systemDefault()).toInstant()));
            dateCalendarVo.setDayOfMonth(dayOfMonth);
            dateCalendarVo.setDayOfWeek(dayOfWeek);

            StringBuilder builder = new StringBuilder(String.format("%02d",localDate.getMonthValue()));
            builder.append("-");
            builder.append(String.format("%02d",localDate.getDayOfMonth()));
            dateCalendarVo.setDay(builder.toString());

            dateCalendarVos.add(dateCalendarVo);
        }
        return dateCalendarVos;
    }

    public static void main(String[] args) {
        List<LocalDate> locallist = new ArrayList<>();
        LocalDate now = LocalDate.now();
        for (int i=1;i<=10;i++){
            LocalDate localDate = now.plusDays(1L);
            locallist.add(localDate);
        }
        System.out.println(locallist);
    }
    /**
     * 处理购物店，自我消费，合作分公司
     * @param id
     * @param tfpProductDto
     */
    private void handleProductOtherInfo(Long id, TfpProductDto tfpProductDto){
        // 保存购物店
        Integer purchaseFlag = tfpProductDto.getPurchaseFlag();
        purchaseShopNumberService.deleteByProductId(id);
        if (purchaseFlag == 1){
            purchaseShopNumberService.saveList(id, tfpProductDto.getPurchaseList());
        }
        // 保存自我消费
        Integer selfFundedProjectFlag = tfpProductDto.getSelfFundedProjectFlag();
        selfFundedProjectService.deleteByProductId(id);
        if (selfFundedProjectFlag == 1){
            selfFundedProjectService.saveList(id, tfpProductDto.getSelfFundedProjectList());
        }

        // 保存合作分公司
        List<Long> cooperativeBranchCompanyIdList = tfpProductDto.getCooperativeBranchCompanyIdList();
        productCbcService.deleteTfpProductCbcByProductId(id);
        if (DcListUtils.isNotEmpty(cooperativeBranchCompanyIdList)){
            productCbcService.saveProductCbc(id, cooperativeBranchCompanyIdList);
        }
    }

    /**
     * 临时处理区域名称存储
     * @param tfpProduct
     */
    public void tempHandleArea(TfpProduct tfpProduct){
        // 临时处理区域
        Long cityId = tfpProduct.getCityId();
        if (cityId != null){
            SysArea city = areaService.selectSysAreaById(cityId);
            if (city != null){
                tfpProduct.setCityName(city.getName());

                Long provinceParentId = city.getParentId();
                if (provinceParentId != null){
                    tfpProduct.setAreaId(provinceParentId);

                    SysArea province = areaService.selectSysAreaById(provinceParentId);
                    if (province != null){
                        tfpProduct.setAreaName(province.getName());
                    }

                    Long parentId = province.getParentId();
                    if (parentId != null){
                        tfpProduct.setParentAreaId(parentId);

                        SysArea parentArea = areaService.selectSysAreaById(parentId);
                        if (parentArea != null){
                            tfpProduct.setParentAreaName(parentArea.getName());
                        }
                    }
                }
            }
        }

        Long goPlaceId = tfpProduct.getGoPlaceId();
        if (goPlaceId != null){
            SysArea goPlaceArea = areaService.selectSysAreaById(goPlaceId);
            if (goPlaceArea != null){
                tfpProduct.setGoPlaceName(goPlaceArea.getName());
            }
        }

        Long backPlaceId = tfpProduct.getBackPlaceId();
        if (backPlaceId != null){
            SysArea backPlaceArea = areaService.selectSysAreaById(backPlaceId);
            if (backPlaceArea != null){
                tfpProduct.setBackPlaceName(backPlaceArea.getName());
            }
        }
    }

    @Override
    public List<TfpProductVo> selectAppletHotRecommendList(TfpProduct tfpProduct) {
        //搜索
        startPage();
        List<TfpProductVo> tfpProductVoList = tfpProductMapper.selectTfpProductVoList(tfpProduct);
        if(CollectionUtils.isEmpty(tfpProductVoList)){
            return Lists.newArrayList();
        }
        List<Long> idList = tfpProductVoList.stream().map(TfpProductVo::getId).collect(Collectors.toList());
        //多少人下单，获取订单信息
        List<TfpChatGroup> allChatGroupList = chatGroupService.selectTfpChatGroupListByProductIdList(idList);
        Map<Long, List<TfpChatGroup>> productChatGroupMap = allChatGroupList.stream().collect(Collectors.groupingBy(TfpChatGroup::getProductId));
        Map<Long, List<TfpChatGroupPerson>> productGroupPersonMap = new HashMap<>();
        List<TfpChatGroupPerson> chatGroupPersonList = chatGroupPersonService.getSignUpPersonInfoListByProductIdList(idList);
        List<Long> userIdList = new ArrayList<>();
        for (TfpChatGroupPerson tfpChatGroupPerson : chatGroupPersonList) {
            Integer userType = tfpChatGroupPerson.getUserType();
            if (Objects.equals(userType, ChatUserTypeEnum.BOOT_MAN.getValue())
                    || Objects.equals(userType, ChatUserTypeEnum.APPLIED.getValue())){
                Long userId = tfpChatGroupPerson.getUserId();
                if (!userIdList.contains(userId)){
                    userIdList.add(userId);
                }

                Long productId = tfpChatGroupPerson.getProductId();
                List<TfpChatGroupPerson> tfpChatGroupPersonList = productGroupPersonMap.get(productId);
                if (tfpChatGroupPersonList == null){
                    tfpChatGroupPersonList = new ArrayList<>();
                }

                tfpChatGroupPersonList.add(tfpChatGroupPerson);
                productGroupPersonMap.put(productId, tfpChatGroupPersonList);
            }
        }


        List<TfpAttachment> attchmentList = userService.getHeadPortraitListByUserIdList(userIdList);
        Map<Long, String> headPortraitMap;
        if (DcListUtils.isNotEmpty(attchmentList)){
            headPortraitMap = attchmentList.stream().collect(Collectors.toMap(TfpAttachment::getBusinessId, TfpAttachment::getUrl));
        } else {
            headPortraitMap = new HashMap<>();
        }

        List<TfpAttachment> tfpAttachmentList = attachmentService.selectList(idList, BusinessTypeEnum.PRODUCT.getValue(), BusinessSubTypeEnum.PRODUCT_COVER_IMAGE.getValue());
        Map<Long, List<TfpAttachment>> productCoverImageUrlListMap = tfpAttachmentList.stream().collect(Collectors.groupingBy(TfpAttachment::getBusinessId));

        for (TfpProductVo listProductVo : tfpProductVoList) {
            Integer signUpPersonNumber = 0;
            Long id = listProductVo.getId();
            List<TfpChatGroup> chatGroupList = productChatGroupMap.get(id);
            if (DcListUtils.isNotEmpty(chatGroupList)) {
                for (TfpChatGroup tfpChatGroup : chatGroupList) {
                    Integer groupSignUpPersonNumber = tfpChatGroup.getSignUpPersonNumber();
                    if (Objects.nonNull(groupSignUpPersonNumber)){
                        signUpPersonNumber += groupSignUpPersonNumber;
                    }
                }
            }

            List<String> headPortraitList = new ArrayList<>();
            List<TfpChatGroupPerson> tfpChatGroupPersonList = productGroupPersonMap.get(id);
            if (DcListUtils.isNotEmpty(tfpChatGroupPersonList)){
                for (TfpChatGroupPerson chatGroupPerson : tfpChatGroupPersonList) {
                    String headPortrait = headPortraitMap.get(chatGroupPerson.getUserId());
                    if (headPortrait != null && !headPortraitList.contains(headPortrait)){
                        headPortraitList.add(headPortrait);
                    }
                }
            }

            List<String> productCoverImageUrlList = new ArrayList<>();
            List<TfpAttachment> productCoverImageUrlAttachmentList = productCoverImageUrlListMap.get(id);
            if (DcListUtils.isNotEmpty(productCoverImageUrlAttachmentList)){
                productCoverImageUrlList = productCoverImageUrlAttachmentList.stream().map(TfpAttachment::getUrl).collect(Collectors.toList());
            }

            listProductVo.setSignUpPersonHeadPortraitUrlList(headPortraitList);
            listProductVo.setSignUpPersonNumber(signUpPersonNumber);
            listProductVo.setProductCoverImageUrlList(productCoverImageUrlList);
        }

        return tfpProductVoList;
    }

    @Override
    public List<String> homePageCarouselChart() {
        List<TfpAttachment> attachmentList = attachmentService.selectListByBusinessId(homePageCarouselChartId, BusinessTypeEnum.Carousel_Chart.getValue(), BusinessSubTypeEnum.CAROUSEL_CHART_HOME_PAGE.getValue());
        if (DcListUtils.isNotEmpty(attachmentList)){
            return attachmentList.stream().map(TfpAttachment::getUrl).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    @Override
    public ProductSnapshotDetailVo productSnapshotDetail(Long productId, SnapshotTextDto snapshotTextDto, Integer travelType, Long orderChatGroupId) {
        ProductDetailVo productDetailVo = this.productDetail(productId, snapshotTextDto);

        ProductSnapshotDetailVo productSnapshotDetailVo = new ProductSnapshotDetailVo();
        BeanUtils.copyProperties(productDetailVo, productSnapshotDetailVo);
        productSnapshotDetailVo.setTravelType(travelType);
        productSnapshotDetailVo.setChatGroupId(orderChatGroupId);

        Long categoryId = productDetailVo.getCategoryId();
        if (Objects.equals(ProductCategoryEnum.WHOLE_COUNTRY.getValue(), categoryId)
                || Objects.equals(ProductCategoryEnum.WHOLE_COUNTRY_SMALL.getValue(), categoryId)
                || Objects.equals(TravelTypeEnum.TEAM_TRAVEL.getValue(), travelType)){
            if (Objects.nonNull(orderChatGroupId)){
                ChatGroupInfoVo chatGroupInfo = new ChatGroupInfoVo();
                TfpChatGroup tfpChatGroup = chatGroupService.selectTfpChatGroupById(orderChatGroupId);
                if (Objects.nonNull(tfpChatGroup)){
                    String chatGroupName = tfpChatGroup.getName();

                    chatGroupInfo.setId(tfpChatGroup.getId());
                    chatGroupInfo.setName(chatGroupName);
                    chatGroupInfo.setLimitPersonNumber(tfpChatGroup.getLimitPersonNumber());

                    TfpChatGroupPerson personParam = new TfpChatGroupPerson();
                    personParam.setChatGroupId(tfpChatGroup.getId());
                    personParam.setDelFlag(DeleteEnum.EXIST.getValue());
                    List<TfpChatGroupPerson> tfpChatGroupPersonList = chatGroupPersonService.selectTfpChatGroupPersonList(personParam);

                    List<Long> groupPersonUserIdList = tfpChatGroupPersonList.stream().map(TfpChatGroupPerson::getUserId).distinct().collect(Collectors.toList());
                    List<TfpAttachment> attchmentList = userService.getHeadPortraitListByUserIdList(groupPersonUserIdList);
                    Map<Long, String> headPortraitMap;
                    if (DcListUtils.isNotEmpty(attchmentList)){
                        headPortraitMap = attchmentList.stream().collect(Collectors.toMap(TfpAttachment::getBusinessId, TfpAttachment::getUrl));
                    } else {
                        headPortraitMap = new HashMap<>();
                    }

                    List<String> chatGroupPersonHeadPortraitUrlList = new ArrayList<>();
                    for (TfpChatGroupPerson chatGroupPerson : tfpChatGroupPersonList) {
                        // 如果仅展示报名用户头像，可以放开
//                    Integer userType = chatGroupPerson.getUserType();
//                    if (Objects.equals(userType, ChatUserTypeEnum.BOOT_MAN.getValue())
//                            || Objects.equals(userType, ChatUserTypeEnum.APPLIED.getValue())){
                        Long personUserId = chatGroupPerson.getUserId();
                        String headPortrait = headPortraitMap.get(personUserId);
                        if (headPortrait != null && !chatGroupPersonHeadPortraitUrlList.contains(headPortrait)){
                            chatGroupPersonHeadPortraitUrlList.add(headPortrait);
                        }
//                    }
                    }

                    chatGroupInfo.setChatGroupPersonHeadPortraitUrlList(chatGroupPersonHeadPortraitUrlList);
                    chatGroupInfo.setSignUpPersonNumber(tfpChatGroup.getSignUpPersonNumber());
                    productSnapshotDetailVo.setChatGroupInfo(chatGroupInfo);
                }
            }
        }

        return productSnapshotDetailVo;
    }

    /**
     * 商品上架/下架
     * @param operationShelivesDto
     * @return
     */
    @Override
    public int operationShelives(OperationShelivesDto operationShelivesDto) {
        Integer salesStatus = operationShelivesDto.getSalesStatus();
        if (!Objects.equals(SaleStatusEnum.ON_SHELIVES.getValue(), salesStatus)
                && !Objects.equals(SaleStatusEnum.DOWN_SHELIVES.getValue(), salesStatus)){
            throw new BusinessException("操作上下架状态错误");
        }

        Long id = operationShelivesDto.getId();

        // 下架
        if (Objects.equals(SaleStatusEnum.DOWN_SHELIVES.getValue(), salesStatus)){
            // 待确认、待支付、待出行
            List<TfpOrder> orderList = orderService.selectNotDownShelivesOrderList(id);
            if (DcListUtils.isNotEmpty(orderList)){
                throw new BusinessException("当前有销售订单，请处理后再试");
            }
        }

        TfpProduct tfpProduct = tfpProductMapper.selectTfpProductById(id);
        Integer lowPriceAirTicketServiceFlag = tfpProduct.getLowPriceAirTicketServiceFlag();
        if (Objects.equals(SaleStatusEnum.ON_SHELIVES.getValue(), salesStatus) &&
        Objects.equals(YesOrNoFlagEnum.YES.getValue(), lowPriceAirTicketServiceFlag)){
            // 上架
            Integer airTicketPriceQueryCompleteFlag = tfpProduct.getAirTicketPriceQueryCompleteFlag();
            if (!Objects.equals(airTicketPriceQueryCompleteFlag, YesOrNoFlagEnum.YES.getValue())){
                throw new BusinessException("获取总价中，请稍后尝试");
            }
        }

        LoginUser loginUser = SecurityUtils.getLoginUser();

        tfpProduct.setSalesStatus(salesStatus);
        tfpProduct.setSalesDate(DateUtils.getNowDate());
        tfpProduct.setAirTicketPriceQueryDate(tfpProduct.getSalesDate());
        tfpProduct.setUpdateUserId(loginUser.getUserId());
        tfpProduct.setUpdateBy(loginUser.getUsername());
        tfpProduct.setUpdateTime(DateUtils.getNowDate());
        tfpProductMapper.updateTfpProduct(tfpProduct);

        return 1;
    }

    /**
     * 计算总价
     * @param adultRetailPrice
     * @param totalAirTicketPrice
     * @return
     */
    @Override
    public BigDecimal mathTotalPrice(BigDecimal adultRetailPrice, BigDecimal totalAirTicketPrice){
        // 总价=建议成人零售价+调用机票接口获取机票价（价格中等航班）+机票费用2%的涨幅（该涨幅比例固定，比例调整需做成可配置项）；
        // 总价=每组建议成人零售价+调用机票接口获取机票价+往返机票价之和2%的涨幅
        BigDecimal price = adultRetailPrice.add(totalAirTicketPrice).add(totalAirTicketPrice.multiply(increasePercent));
        BigDecimal newPrice = price.setScale(0, RoundingMode.DOWN);

        // 若该步骤计算产生小数，则去除小数点进1。保证总价展示为整数。
        if (price.compareTo(newPrice) != 0){
            price = newPrice.add(new BigDecimal(1));
        }

        return price;
    }

    /**
     * 保存商品图片
     * @param id
     * @param businessSubType
     * @param productImageUrlList
     */
    private void saveProductImageUrl(Long id, String businessSubType, List<String> productImageUrlList){
        if (DcListUtils.isNotEmpty(productImageUrlList)){
            LoginUser loginUser = SecurityUtils.getLoginUser();
            for (String url : productImageUrlList) {
                TfpAttachment attachment = new TfpAttachment();
                attachment.setBusinessId(id);
                attachment.setBusinessType(BusinessTypeEnum.PRODUCT.getValue());
                attachment.setBusinessSubType(businessSubType);
                attachment.setName("商品图片" + DcUrlUtils.getExtend(url));
                attachment.setUrl(url);
                attachment.setDelFlag(DeleteEnum.EXIST.getValue());
                attachment.setCreateUserId(loginUser.getUserId());
                attachment.setCreateBy(loginUser.getUsername());
                attachment.setCreateTime(DateUtils.getNowDate());
                attachmentService.insertTfpAttachment(attachment);
            }
        }
    }

    private void judgeDelete(List<TfpProduct> tfpProductList) {
        if (DcListUtils.isNotEmpty(tfpProductList)){
            List<Long> judgeOrderProductIdList = new ArrayList<>();
            for (TfpProduct tfpProduct : tfpProductList) {
                Integer salesStatus = tfpProduct.getSalesStatus();
                if (Objects.equals(salesStatus, SaleStatusEnum.ON_SHELIVES.getValue())){
                    throw new RuntimeException("上架的商品不可以删除");
                }

                if (Objects.equals(salesStatus, SaleStatusEnum.DOWN_SHELIVES.getValue())){
                    judgeOrderProductIdList.add(tfpProduct.getId());
                }
            }

            if (DcListUtils.isNotEmpty(judgeOrderProductIdList)){
                List<TfpOrder> orderList = orderService.selectListByProductIdList(judgeOrderProductIdList, null);
                if (DcListUtils.isNotEmpty(orderList)){
                    throw new RuntimeException("商品已存在订单，不可以删除");
                }
            }
        }
    }

    private void judgeSaveProduct(TfpProductDto tfpProductDto, String oldProductNo){
        String productNo = tfpProductDto.getProductNo().trim();
        if (Objects.isNull(oldProductNo) || (Objects.nonNull(oldProductNo) && !Objects.equals(productNo, oldProductNo))){
            TfpProduct param = new TfpProduct();
            param.setProductNo(productNo);
            param.setDelFlag(DeleteEnum.EXIST.getValue());
            List<TfpProduct> tfpProductList = tfpProductMapper.selectTfpProductList(param);
            if (DcListUtils.isNotEmpty(tfpProductList)){
                throw new BusinessException("该商品编号线路已录入，请勿重复提交");
            }
        }

        tfpProductDto.setProductNo(productNo);

        Integer childrenAgeLimitMin = tfpProductDto.getChildrenAgeLimitMin();
        Integer childrenAgeLimitMax = tfpProductDto.getChildrenAgeLimitMax();
        if (childrenAgeLimitMin > childrenAgeLimitMax){
            throw new BusinessException("儿童标准最小年龄不能大于最大年龄");
        }

        // 线路类型 0-出发地参团 1-目的地参团
        String lineTypeCode = tfpProductDto.getLineTypeCode();
        if (Objects.equals(lineTypeCode, "0")){
//            Long goPlaceId = tfpProductDto.getGoPlaceId();
//            if (Objects.isNull(goPlaceId)){
//                throw new BusinessException("出发城市不能为空");
//            }
//
//            Long backPlaceId = tfpProductDto.getBackPlaceId();
//            if (Objects.isNull(backPlaceId)){
//                throw new BusinessException("返回城市不能为空");
//            }

            String goTrafficCode = tfpProductDto.getGoTrafficCode();
            String goTrafficName = tfpProductDto.getGoTrafficName();
            if (Objects.isNull(goTrafficCode) || Objects.isNull(goTrafficName)){
                throw new BusinessException("去程交通不能为空");
            }

            String backTrafficCode = tfpProductDto.getBackTrafficCode();
            String backTrafficName = tfpProductDto.getBackTrafficName();
            if (Objects.isNull(backTrafficCode) || Objects.isNull(backTrafficName)){
                throw new BusinessException("回程交通不能为空");
            }
        }
    }

    private void judgeCityThreeCode(TfpProduct tfpProduct){
        Integer lowPriceAirTicketServiceFlag = tfpProduct.getLowPriceAirTicketServiceFlag();
        if (Objects.equals(lowPriceAirTicketServiceFlag, YesOrNoFlagEnum.YES.getValue())){
            String goPlaceName = tfpProduct.getGoPlaceName();
            String backPlaceName = tfpProduct.getBackPlaceName();

            String goPlaceCode = airportCodesService.getThreeCode(goPlaceName);
            if (Objects.isNull(goPlaceCode)){
                throw new BusinessException("当前城市(" + goPlaceName + ")暂不支持，无法提供低价机票服务");
            }

            String backPlaceCode = airportCodesService.getThreeCode(backPlaceName);
            if (Objects.isNull(backPlaceCode)){
                throw new BusinessException("当前城市(" + backPlaceName + ")暂不支持，无法提供低价机票服务");
            }
        }
    }

    /**
     * 处理OutPriceVoList 取10条最近日期的在团期范围内的数据
     * @param tfpProductDepartureDateList
     * @return
     */
    private List<DateCalendarVo> handleOutletDayPriceList(TfpProduct tfpProduct, List<TfpProductDepartureDate> tfpProductDepartureDateList){
        List<DateCalendarVo> outletDayPriceList = new ArrayList<>();

        if (DcListUtils.isNotEmpty(tfpProductDepartureDateList)) {
            Date now = DcDateUtils.handelDateToDayStart(new Date());
            Calendar calendar = Calendar.getInstance();

            Integer oneDay = 24 * 60 * 60 * 1000;

            List<Date> outDateList = new ArrayList<>();
            for (TfpProductDepartureDate tfpProductDepartureDate : tfpProductDepartureDateList) {
                // 当前日期在团期截止时间之后
                Date departureDateEnd = DcDateUtils.handelDateToDayStart(tfpProductDepartureDate.getDepartureDateEnd());
                if (now.after(departureDateEnd)) {
                    continue;
                }

                int length;

                Date departureDateStart = DcDateUtils.handelDateToDayStart(tfpProductDepartureDate.getDepartureDateStart());
                if (now.after(departureDateStart)) {
                    // 当前日期在团期开始和截止时间之间
                    length = new Long((departureDateEnd.getTime() - now.getTime()) / oneDay).intValue() + 1;
                    calendar.setTime(now);
                } else {
                    // 当前日期在团期开始时间之前
                    length = new Long((departureDateEnd.getTime() - departureDateStart.getTime()) / oneDay).intValue() + 1;
                    calendar.setTime(departureDateStart);
                }

                length = Math.min(length, 10);
                for (int i = 0; i < length; i++) {
                    if (i != 0){
                        calendar.add(Calendar.DAY_OF_MONTH, 1);
                    }

                    Date time = calendar.getTime();
                    if (!outDateList.contains(time)) {
                        outDateList.add(time);
                    }
                }
            }

            if (DcListUtils.isNotEmpty(outDateList)) {
                outDateList = outDateList.stream().sorted(Comparator.comparing(Date::getTime)).collect(Collectors.toList());
                outDateList = outDateList.subList(0, Math.min(outDateList.size(), 10));

                Map<Integer, Map<String, BigDecimal>> goAndBackDatePriceMap = this.getDatePriceMap(tfpProduct, outDateList);

                for (Date date : outDateList) {
                    calendar.setTime(date);
                    DateCalendarVo dateCalendarVo = this.handleDateCalendarVo(tfpProduct, calendar, now, tfpProductDepartureDateList, goAndBackDatePriceMap);
                    outletDayPriceList.add(dateCalendarVo);
                }
            }
        }

        return outletDayPriceList;
    }

    /**
     * 处理DatePriceVoList 取最近包含团期的2个月数据
     * @param tfpProductDepartureDateList
     * @return
     */
    private List<DateMonthVO> handleDatePriceVoList(TfpProduct tfpProduct, List<TfpProductDepartureDate> tfpProductDepartureDateList) {
        List<DateMonthVO> dateMonthVOList = new ArrayList<>();

        Date now = DcDateUtils.handelDateToDayStart(new Date());
        Calendar calendar = Calendar.getInstance();

        List<Date> monthDateList = new ArrayList<>();

        for (TfpProductDepartureDate tfpProductDepartureDate : tfpProductDepartureDateList) {
            // 当前日期在团期截止时间之后
            Date departureDateEnd = DcDateUtils.handelDateToDayStart(tfpProductDepartureDate.getDepartureDateEnd());
            if (now.after(departureDateEnd)) {
                continue;
            }

            calendar.setTime(departureDateEnd);
            int endYear = calendar.get(Calendar.YEAR);
            int endMonth = calendar.get(Calendar.MONTH) + 1;

            int length;
            Date departureDateStart = DcDateUtils.handelDateToDayStart(tfpProductDepartureDate.getDepartureDateStart());
            if (now.after(departureDateStart)) {
                // 当前日期在团期开始和截止时间之间
                calendar.setTime(now);
                int nowYear = calendar.get(Calendar.YEAR);
                int nowMonth = calendar.get(Calendar.MONTH) + 1;

                length = endMonth - nowMonth + 1 + (endYear - nowYear) * 12;
            } else {
                // 当前日期在团期开始时间之前
                calendar.setTime(departureDateStart);
                int startYear = calendar.get(Calendar.YEAR);
                int startMonth = calendar.get(Calendar.MONTH) + 1;

                length = endMonth - startMonth + 1 + (endYear - startYear) * 12;
            }

            // 设置为第一天
            calendar.set(Calendar.DAY_OF_MONTH, 1);

            length = Math.min(length, 2);
            for (int i = 0; i < length; i++) {
                if (i != 0){
                    calendar.add(Calendar.MONTH, 1);
                }

                Date time = calendar.getTime();
                if (!monthDateList.contains(time)) {
                    monthDateList.add(time);
                }
            }
        }

        if (DcListUtils.isNotEmpty(monthDateList)){
            monthDateList = monthDateList.stream().sorted(Comparator.comparing(Date::getTime)).collect(Collectors.toList());
            monthDateList = monthDateList.subList(0, Math.min(monthDateList.size(), 2));

            List<Date> dateList = new ArrayList<>();
            for (Date date : monthDateList) {
                calendar.setTime(date);
                int dateSize = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
                for (int j = 0; j < dateSize; j++) {
                    if (j != 0){
                        calendar.add(Calendar.DAY_OF_MONTH, 1);
                    }

                    dateList.add(calendar.getTime());
                }
            }

            Map<Integer, Map<String, BigDecimal>> goAndBackDatePriceMap = this.getDatePriceMap(tfpProduct, dateList);

            Map<Integer, List<DateCalendarVo>> dateCalendarVoMap = new HashMap<>();
            for (Date date : dateList) {
                calendar.setTime(date);
                int month = calendar.get(Calendar.MONTH) + 1;

                List<DateCalendarVo> dateCalendarVoList = dateCalendarVoMap.get(month);
                dateCalendarVoList = DcListUtils.isEmpty(dateCalendarVoList) ? new ArrayList<>() : dateCalendarVoList;

                DateCalendarVo dateCalendarVo = this.handleDateCalendarVo(tfpProduct, calendar, now, tfpProductDepartureDateList, goAndBackDatePriceMap);
                dateCalendarVoList.add(dateCalendarVo);
                dateCalendarVoMap.put(month, dateCalendarVoList);
            }

            for (Date date : monthDateList) {
                calendar.setTime(date);

                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH) + 1;

                DateMonthVO dateMonthVO = new DateMonthVO();
                dateMonthVO.setMonth(year + "年" + month + "月");
                dateMonthVO.setList(dateCalendarVoMap.get(month));
                dateMonthVOList.add(dateMonthVO);
            }
        }

        return dateMonthVOList;
    }

    /**
     * 处理日期价格返参
     * @param calendar
     * @param tfpProductDepartureDateList
     * @return
     */
    private DateCalendarVo handleDateCalendarVo(TfpProduct tfpProduct, Calendar calendar, Date now, List<TfpProductDepartureDate> tfpProductDepartureDateList, Map<Integer, Map<String, BigDecimal>> goAndBackDatePriceMap){
        DateCalendarVo dateCalendarVo = new DateCalendarVo();

        Date date = calendar.getTime();
        dateCalendarVo.setDate(date);

        dateCalendarVo.setTravelFlag(YesOrNoFlagEnum.NO.getValue());
        dateCalendarVo.setHasPrice(Boolean.FALSE);

        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        // 此处生成的id不能以数字开头
        dateCalendarVo.setId("a" + UUID.randomUUID().toString());
        dateCalendarVo.setDayOfMonth(dayOfMonth);
        dateCalendarVo.setDayOfWeek(calendar.get(Calendar.DAY_OF_WEEK));
        dateCalendarVo.setDay(String.format("%02d", calendar.get(Calendar.MONTH) + 1) + "-" + String.format("%02d", dayOfMonth));

        if (date.equals(now) || date.after(now)){
            JudgeDateTravelFlagVo judgeDateTravelFlagVo = productDepartureDateService.judgeDateTravelFlag(date, tfpProductDepartureDateList);
            Integer travelFlag = judgeDateTravelFlagVo.getTravelFlag();
            dateCalendarVo.setTravelFlag(travelFlag);
            if (Objects.equals(travelFlag, YesOrNoFlagEnum.YES.getValue())){
                BigDecimal adultRetailPrice = judgeDateTravelFlagVo.getAdultRetailPrice();
                dateCalendarVo.setHasPrice(Boolean.TRUE);
                dateCalendarVo.setPrice(adultRetailPrice);
                dateCalendarVo.setAirTicketPriceFlag(YesOrNoFlagEnum.NO.getValue());

                Integer lowPriceAirTicketServiceFlag = tfpProduct.getLowPriceAirTicketServiceFlag();
                if (Objects.equals(lowPriceAirTicketServiceFlag, YesOrNoFlagEnum.YES.getValue())){
                    DateFormat dcDateFormat = DcDateUtils.getDcDateFormat(DcDateUtils.yyyyMMdd);

                    Map<String, BigDecimal> goDatePriceMap = goAndBackDatePriceMap.get(GoAndBackFlagEnum.GO.getValue());
                    BigDecimal goPrice = goDatePriceMap.get(dcDateFormat.format(date));


                    calendar.add(Calendar.DAY_OF_MONTH, tfpProduct.getDayNum() - 1);
                    Date backDate = calendar.getTime();
                    Map<String, BigDecimal> backDatePriceMap = goAndBackDatePriceMap.get(GoAndBackFlagEnum.BACK.getValue());
                    BigDecimal backPrice = backDatePriceMap.get(dcDateFormat.format(backDate));

                    // 仅去程和返程都有价格时，才计算成人零售价+往返机票价格
                    if (Objects.nonNull(goPrice) && Objects.nonNull(backPrice)){
                        dateCalendarVo.setAirTicketPriceFlag(YesOrNoFlagEnum.YES.getValue());

                        BigDecimal totalAirTicketPrice = goPrice.add(backPrice);
                        BigDecimal finalPrice = this.mathTotalPrice(adultRetailPrice, totalAirTicketPrice);

                        dateCalendarVo.setFinalPrice(finalPrice);
                    } else {
                        dateCalendarVo.setFinalPrice(adultRetailPrice);
                    }
                } else {
                    dateCalendarVo.setFinalPrice(adultRetailPrice);
                }
            }
        }

        return dateCalendarVo;
    }

    private Map<Integer, Map<String, BigDecimal>> getDatePriceMap(TfpProduct tfpProduct, List<Date> dateList){
        Map<Integer, Map<String, BigDecimal>> goAndBackDatePriceMap = new HashMap<>();
        Integer lowPriceAirTicketServiceFlag = tfpProduct.getLowPriceAirTicketServiceFlag();
        if (Objects.equals(lowPriceAirTicketServiceFlag, YesOrNoFlagEnum.YES.getValue())){
            DateFormat dcDateFormat = DcDateUtils.getDcDateFormat(DcDateUtils.yyyyMMdd);
            List<String> goDateList = new ArrayList<>();
            List<String> backDateList = new ArrayList<>();

            Calendar calendar = Calendar.getInstance();
            Integer dayNum = tfpProduct.getDayNum();
            for (Date date : dateList) {
                goDateList.add(dcDateFormat.format(date));

                calendar.setTime(date);
                calendar.add(Calendar.DAY_OF_MONTH, dayNum - 1);
                Date backDepartureDate = calendar.getTime();
                backDateList.add(dcDateFormat.format(backDepartureDate));
            }

            List<TfpFlightDatePrice> tfpFlightDatePriceList = new ArrayList<>();
            List<TfpFlightDatePrice> goTfpFlightDatePriceList = tfpFlightDatePriceService.getTfpFlightDatePriceListByGoAndBackPlaceAndDateList(GoAndBackFlagEnum.GO.getValue(), tfpProduct, goDateList);
            List<TfpFlightDatePrice> backTfpFlightDatePriceList = tfpFlightDatePriceService.getTfpFlightDatePriceListByGoAndBackPlaceAndDateList(GoAndBackFlagEnum.BACK.getValue(), tfpProduct, backDateList);

            Map<String, BigDecimal> goDatePriceMap = new HashMap<>();
            if (DcListUtils.isNotEmpty(goTfpFlightDatePriceList)){
                goDatePriceMap = goTfpFlightDatePriceList.stream().collect(Collectors.toMap(TfpFlightDatePrice::getDepartureTime, TfpFlightDatePrice::getPrice));
            }

            Map<String, BigDecimal> backDatePriceMap = new HashMap<>();
            if (DcListUtils.isNotEmpty(backTfpFlightDatePriceList)){
                backDatePriceMap = backTfpFlightDatePriceList.stream().collect(Collectors.toMap(TfpFlightDatePrice::getDepartureTime, TfpFlightDatePrice::getPrice));
            }


            goAndBackDatePriceMap.put(GoAndBackFlagEnum.GO.getValue(), goDatePriceMap);
            goAndBackDatePriceMap.put(GoAndBackFlagEnum.BACK.getValue(), backDatePriceMap);
        }

        return goAndBackDatePriceMap;
    }

    /**
     * 创建商品编号
     *
     * @param id
     * @return
     */
    private String createProductNo(Long id) {
        String productNo;
        // yyMMdd
        DateFormat yMdDateFormat = DcDateUtils.getDcDateFormat(DcDateUtils.yMd);
        String DateFormatStr = yMdDateFormat.format(new Date());

        int idLength = id.toString().length();
        if (idLength > 5) {
            productNo = DateFormatStr + String.format("%0" + idLength + "d", id);
        } else {
            productNo = DateFormatStr + String.format("%05d", id);
        }

        return productNo;
    }

    private String getSupplierShowCategoryName(Long categoryId){
        String showCategoryName = null;
        if (Objects.equals(categoryId, ProductCategoryEnum.WHOLE_COUNTRY.getValue())){
            showCategoryName = "全国散";
        }

        if (Objects.equals(categoryId, ProductCategoryEnum.ONE_FAMILY.getValue())){
            showCategoryName = "一家一团";
        }

        if (Objects.equals(categoryId, ProductCategoryEnum.SELF.getValue())){
            showCategoryName = "自营";
        }

        if (Objects.equals(categoryId, ProductCategoryEnum.WHOLE_COUNTRY_SMALL.getValue())){
            showCategoryName = "全国散-小包团";
        }

        return showCategoryName;
    }
}
