import request from '@/utils/request'

// 查询banner列表
export function listGeneralBanner(query) {
  return request({
    url: '/service/generalBanner/list',
    method: 'get',
    params: query
  })
}

// 查询banner详细
export function getGeneralBanner(id) {
  return request({
    url: '/service/generalBanner/' + id,
    method: 'get'
  })
}

// 新增banner
export function addGeneralBanner(data) {
  return request({
    url: '/service/generalBanner',
    method: 'post',
    data: data
  })
}

// 修改banner
export function updateGeneralBanner(data) {
  return request({
    url: '/service/generalBanner',
    method: 'put',
    data: data
  })
}

// 删除banner
export function delGeneralBanner(id) {
  return request({
    url: '/service/generalBanner/' + id,
    method: 'delete'
  })
}
