package com.ruoyi.service.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 退款对象 tfp_refund
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public class TfpRefund extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 商品ID */
    @Excel(name = "商品ID")
    private Long productId;

    /** 订单ID */
    @Excel(name = "订单ID")
    private Long orderId;

    /** 订单编号 */
    @Excel(name = "订单编号")
    private String orderNo;

    /** 订单金额 */
    @Excel(name = "订单金额")
    private BigDecimal orderPrice;

    /** 接收退款用户 */
    @Excel(name = "接收退款用户")
    private Long refundUserId;

    /** 1：全额退款   2：部分退款 */
    @Excel(name = "1：全额退款   2：部分退款")
    private Integer refundType;

    /** 退款金额 */
    @Excel(name = "退款金额")
    private BigDecimal refundAmount;

    /** 退款时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "退款时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date refundTime;

    /** 退款状态（1：退款审核中，2：审核成功-退款中，3：审核失败-退款失败，4：退款失败，5：退款成功） */
    @Excel(name = "退款状态", readConverterExp = "1=：退款审核中，2：审核成功-退款中，3：审核失败-退款失败，4：退款失败，5：退款成功")
    private Integer refundStatus;

    /** 申请退款原因 */
    @Excel(name = "申请退款原因")
    private String reason;

    /** 申请退款说明 */
    @Excel(name = "申请退款说明")
    private String illustrate;

    /** 申请退款驳回的原因 */
    @Excel(name = "申请退款驳回的原因")
    private String rejectReason;

    /** 退款失败原因 */
    @Excel(name = "退款失败原因")
    private String failReason;

    /** 对账状态（0-未对账 1-成功 2-交易金额异常 3-交易状态异常) */
    @Excel(name = "对账状态", readConverterExp = "对账状态（0-未对账 1-成功 2-交易金额异常 3-交易状态异常)")
    private Integer actStatus;

    /** 对账时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "对账时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date actTime;

    /** 账户信息（预留） */
    @Excel(name = "账户信息", readConverterExp = "预=留")
    private String accountInfo;

    /** 退款信息（预留） */
    @Excel(name = "退款信息", readConverterExp = "预=留")
    private String refundInfo;

    /** 退款单流水号 */
    @Excel(name = "退款单流水号")
    private String serialNo;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    @Excel(name = "创建者id")
    private Long createUserId;

    /** 更新者id */
    @Excel(name = "更新者id")
    private Long updateUserId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setOrderId(Long orderId) 
    {
        this.orderId = orderId;
    }

    public Long getOrderId() 
    {
        return orderId;
    }
    public void setRefundUserId(Long refundUserId) 
    {
        this.refundUserId = refundUserId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Long getRefundUserId()
    {
        return refundUserId;
    }
    public void setRefundType(Integer refundType) 
    {
        this.refundType = refundType;
    }

    public Integer getRefundType() 
    {
        return refundType;
    }
    public void setRefundAmount(BigDecimal refundAmount) 
    {
        this.refundAmount = refundAmount;
    }

    public BigDecimal getRefundAmount() 
    {
        return refundAmount;
    }
    public void setRefundTime(Date refundTime) 
    {
        this.refundTime = refundTime;
    }

    public Date getRefundTime() 
    {
        return refundTime;
    }
    public void setRefundStatus(Integer refundStatus) 
    {
        this.refundStatus = refundStatus;
    }

    public Integer getRefundStatus() 
    {
        return refundStatus;
    }
    public void setReason(String reason) 
    {
        this.reason = reason;
    }

    public String getReason() 
    {
        return reason;
    }

    public String getIllustrate() {
        return illustrate;
    }

    public void setIllustrate(String illustrate) {
        this.illustrate = illustrate;
    }

    public void setRejectReason(String rejectReason)
    {
        this.rejectReason = rejectReason;
    }

    public String getRejectReason() 
    {
        return rejectReason;
    }
    public void setFailReason(String failReason) 
    {
        this.failReason = failReason;
    }

    public String getFailReason() 
    {
        return failReason;
    }
    public void setActStatus(Integer actStatus) 
    {
        this.actStatus = actStatus;
    }

    public Integer getActStatus() 
    {
        return actStatus;
    }
    public void setActTime(Date actTime) 
    {
        this.actTime = actTime;
    }

    public Date getActTime() 
    {
        return actTime;
    }
    public void setAccountInfo(String accountInfo) 
    {
        this.accountInfo = accountInfo;
    }

    public String getAccountInfo() 
    {
        return accountInfo;
    }
    public void setRefundInfo(String refundInfo) 
    {
        this.refundInfo = refundInfo;
    }

    public String getRefundInfo() 
    {
        return refundInfo;
    }
    public void setSerialNo(String serialNo) 
    {
        this.serialNo = serialNo;
    }

    public String getSerialNo() 
    {
        return serialNo;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }
    public void setUpdateUserId(Long updateUserId) 
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId() 
    {
        return updateUserId;
    }

    public BigDecimal getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(BigDecimal orderPrice) {
        this.orderPrice = orderPrice;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("productId", getProductId())
            .append("orderId", getOrderId())
            .append("orderNo", getOrderNo())
            .append("orderPrice", getOrderPrice())
            .append("refundUserId", getRefundUserId())
            .append("refundType", getRefundType())
            .append("refundAmount", getRefundAmount())
            .append("refundTime", getRefundTime())
            .append("refundStatus", getRefundStatus())
            .append("reason", getReason())
            .append("illustrate", getIllustrate())
            .append("rejectReason", getRejectReason())
            .append("failReason", getFailReason())
            .append("actStatus", getActStatus())
            .append("actTime", getActTime())
            .append("accountInfo", getAccountInfo())
            .append("refundInfo", getRefundInfo())
            .append("serialNo", getSerialNo())
            .append("delFlag", getDelFlag())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
