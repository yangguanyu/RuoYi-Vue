package com.ruoyi.web.controller.service.form.group;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * 聊天群组成员聊天信息对象 tfp_chat_group_person_info
 *
 * @author ruoyi
 * @date 2023-10-31
 */
@ApiModel(value = "ChatGroupPersonInfoForm", description = "群组聊天内容添加")
public class ChatGroupPersonInfoForm extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty(hidden = true,value = "主键")
    private Long id;

    /** 聊天群组id */
    @ApiModelProperty("群组id")
    @NotNull(message = "群组id不能为空")
    private Long chatGroupId;

    /** 聊天信息 */
    @ApiModelProperty("发送的聊天内容")
    @Max(value = 200,message = "聊天内容不能超过200字")
    private String chatInfo;

    /** 删除标志（0代表存在 2代表删除） */
    @ApiModelProperty(hidden = true,value = "删除标志（0代表存在 2代表删除）")
    private String delFlag;

    /** 创建者id */
    @ApiModelProperty(hidden = true,value = "创建者id")
    private Long createUserId;

    /** 更新者id */
    @ApiModelProperty(hidden = true,value = "更新者id")
    private Long updateUserId;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setChatGroupId(Long chatGroupId)
    {
        this.chatGroupId = chatGroupId;
    }

    public Long getChatGroupId()
    {
        return chatGroupId;
    }
    public void setChatInfo(String chatInfo)
    {
        this.chatInfo = chatInfo;
    }

    public String getChatInfo()
    {
        return chatInfo;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }
    public void setCreateUserId(Long createUserId)
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId()
    {
        return createUserId;
    }
    public void setUpdateUserId(Long updateUserId)
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId()
    {
        return updateUserId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("chatGroupId", getChatGroupId())
            .append("chatInfo", getChatInfo())
            .append("delFlag", getDelFlag())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
