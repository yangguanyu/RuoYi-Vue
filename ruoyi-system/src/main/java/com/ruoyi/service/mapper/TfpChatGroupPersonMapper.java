package com.ruoyi.service.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.service.domain.TfpChatGroupPerson;
import com.ruoyi.service.vo.groupCountPersonVo;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Param;

/**
 * 聊天群组成员Mapper接口
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public interface TfpChatGroupPersonMapper 
{
    /**
     * 查询聊天群组成员
     * 
     * @param id 聊天群组成员主键
     * @return 聊天群组成员
     */
    public TfpChatGroupPerson selectTfpChatGroupPersonById(Long id);

    /**
     * 查询聊天群组成员列表
     * 
     * @param tfpChatGroupPerson 聊天群组成员
     * @return 聊天群组成员集合
     */
    public List<TfpChatGroupPerson> selectTfpChatGroupPersonList(TfpChatGroupPerson tfpChatGroupPerson);

    public List<TfpChatGroupPerson> selectListByGroupIdAndSignUp(@Param("groupIds") List<Long> groupIds);

    public TfpChatGroupPerson selectOneByUserIdAndGroupId(@Param("userId") Long userId,@Param("chatGroupId") Long chatGroupId);

    /**
     * 新增聊天群组成员
     * 
     * @param tfpChatGroupPerson 聊天群组成员
     * @return 结果
     */
    public int insertTfpChatGroupPerson(TfpChatGroupPerson tfpChatGroupPerson);

    /**
     * 修改聊天群组成员
     * 
     * @param tfpChatGroupPerson 聊天群组成员
     * @return 结果
     */
    public int updateTfpChatGroupPerson(TfpChatGroupPerson tfpChatGroupPerson);

    /**
     * 删除聊天群组成员
     * 
     * @param id 聊天群组成员主键
     * @return 结果
     */
    public int deleteTfpChatGroupPersonById(Long id);

    /**
     * 批量删除聊天群组成员
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpChatGroupPersonByIds(Long[] ids);

    List<TfpChatGroupPerson> selectTfpChatGroupPersonByIdList(@Param("idList") List<Long> idList);

    List<groupCountPersonVo> selectCountPersonByGroupIdList(@Param("groupIds") List<Long> groupIds);

    List<Long> selectChatGroupPersonIdByUserId(@Param("userId") Long userId);

    List<TfpChatGroupPerson> selectTfpChatGroupPersonListByGroupIdList(@Param("groupIdList") List<Long> groupIdList);

    List<TfpChatGroupPerson> getSignUpPersonInfoListByProductIdList(@Param("productIdList") List<Long> productIdList);

    TfpChatGroupPerson selectTfpChatGroupPersonByGroupIdAndUserId(@Param("groupId") Long groupId, @Param("userId") Long userId);
}
