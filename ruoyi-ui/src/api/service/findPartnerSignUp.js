import request from '@/utils/request'

// 查询找搭子列表
export function listFindPartnerSignUp(query) {
  return request({
    url: '/service/findPartnerSignUp/list',
    method: 'get',
    params: query
  })
}

// 查询找搭子详细
export function getFindPartnerSignUp(id) {
  return request({
    url: '/service/findPartnerSignUp/' + id,
    method: 'get'
  })
}

// 新增找搭子
export function addFindPartnerSignUp(data) {
  return request({
    url: '/service/findPartnerSignUp',
    method: 'post',
    data: data
  })
}

// 修改找搭子
export function updateFindPartnerSignUp(data) {
  return request({
    url: '/service/findPartnerSignUp',
    method: 'put',
    data: data
  })
}

// 删除找搭子
export function delFindPartnerSignUp(id) {
  return request({
    url: '/service/findPartnerSignUp/' + id,
    method: 'delete'
  })
}

export function signUpUserSelect(){
  return request({
    url: '/service/findPartnerSignUp/signUpUserSelect',
    method: 'post'
  })
}
