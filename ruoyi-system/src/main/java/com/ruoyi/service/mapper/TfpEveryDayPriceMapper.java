package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.TfpEveryDayPrice;
import org.apache.ibatis.annotations.Param;

/**
 * 每日价格Mapper接口
 * 
 * @author ruoyi
 * @date 2023-12-12
 */
public interface TfpEveryDayPriceMapper 
{
    /**
     * 查询每日价格
     * 
     * @param id 每日价格主键
     * @return 每日价格
     */
    public TfpEveryDayPrice selectTfpEveryDayPriceById(Long id);

    /**
     * 查询每日价格列表
     * 
     * @param tfpEveryDayPrice 每日价格
     * @return 每日价格集合
     */
    public List<TfpEveryDayPrice> selectTfpEveryDayPriceList(TfpEveryDayPrice tfpEveryDayPrice);

    /**
     * 新增每日价格
     * 
     * @param tfpEveryDayPrice 每日价格
     * @return 结果
     */
    public int insertTfpEveryDayPrice(TfpEveryDayPrice tfpEveryDayPrice);

    /**
     * 修改每日价格
     * 
     * @param tfpEveryDayPrice 每日价格
     * @return 结果
     */
    public int updateTfpEveryDayPrice(TfpEveryDayPrice tfpEveryDayPrice);

    /**
     * 删除每日价格
     * 
     * @param id 每日价格主键
     * @return 结果
     */
    public int deleteTfpEveryDayPriceById(Long id);

    /**
     * 批量删除每日价格
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpEveryDayPriceByIds(Long[] ids);

    List<TfpEveryDayPrice> selectDefaultTfpEveryDayPriceListByProductIdList(@Param("productIdList") List<Long> productIdList);

    void deleteTfpEveryDayPriceByProductIds(@Param("productIdList") List<Long> productIdList);

    List<TfpEveryDayPrice> selectEveryDayPriceListByProductId(@Param("productId") Long productId);
}
