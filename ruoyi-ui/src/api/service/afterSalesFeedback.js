import request from '@/utils/request'

// 查询售后反馈列表
export function listAfterSalesFeedback(query) {
  return request({
    url: '/service/afterSalesFeedback/list',
    method: 'get',
    params: query
  })
}

// 查询售后反馈详细
export function getAfterSalesFeedback(id) {
  return request({
    url: '/service/afterSalesFeedback/' + id,
    method: 'get'
  })
}

// 新增售后反馈
export function addAfterSalesFeedback(data) {
  return request({
    url: '/service/afterSalesFeedback',
    method: 'post',
    data: data
  })
}

// 修改售后反馈
export function updateAfterSalesFeedback(data) {
  return request({
    url: '/service/afterSalesFeedback',
    method: 'put',
    data: data
  })
}

// 删除售后反馈
export function delAfterSalesFeedback(id) {
  return request({
    url: '/service/afterSalesFeedback/' + id,
    method: 'delete'
  })
}
