package com.ruoyi.service.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@ApiModel
public class MoreListMonthGroupVo {
    @ApiModelProperty("群组id")
    private Long id;
    @ApiModelProperty("群组名称")
    private String name;
    @ApiModelProperty("图片")
    private String image;
    @ApiModelProperty("用户id")
    private Long userId;
    @ApiModelProperty("是否是当前登录人 0-否 1-是")
    private boolean loginUserFlag;
    @ApiModelProperty("出发日期")
    @JsonFormat(pattern = "MM-dd")
    private Date goDate;
    @ApiModelProperty("出发日期")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date goDateOfYear;
    @ApiModelProperty("报名人数")
    private Integer signUpPersonNumber;
    @ApiModelProperty("限制人数")
    private Integer limitPersonNumber;
    @ApiModelProperty("报名人员头像url集合")
    private List<String> signUpPersonHeadPortraitUrlList;
}
