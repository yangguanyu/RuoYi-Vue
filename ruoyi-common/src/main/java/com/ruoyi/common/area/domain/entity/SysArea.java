package com.ruoyi.common.area.domain.entity;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 地区对象 sys_area
 * 
 * @author ruoyi
 * @date 2023-10-17
 */
public class SysArea extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 地区父节点ID */
    @Excel(name = "地区父节点ID")
    private Long parentId;

    /** 地区名 */
    @Excel(name = "地区名")
    private String name;

    /** 区域全名，ex: 四川省/成都市/武侯区 */
    @Excel(name = "区域全名，ex: 四川省/成都市/武侯区")
    private String fullName;

    /** 地区名的拼音 */
    @Excel(name = "地区名的拼音")
    private String pinyin;

    /** 地区代码 */
    @Excel(name = "地区代码")
    private String areaCode;

    /** 国家标准地区代码 */
    @Excel(name = "国家标准地区代码")
    private String standardAreaCode;

    /** 区域类型 */
    @Excel(name = "区域类型")
    private Integer type;

    /** 经度-180和+180 */
    @Excel(name = "经度-180和+180")
    private BigDecimal lng;

    /** 维度-90和+90 */
    @Excel(name = "维度-90和+90")
    private BigDecimal lat;

    /** label */
    @Excel(name = "label")
    private String label;

    /** 国家类型 0-中国 1-其他 */
    @Excel(name = "国家类型 0-中国 1-其他")
    private Integer countryType;

    /** 排序值 */
    @Excel(name = "排序值")
    private Integer sort;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    @Excel(name = "创建者id")
    private Long createUserId;

    /** 更新者id */
    @Excel(name = "更新者id")
    private Long updateUserId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setParentId(Long parentId) 
    {
        this.parentId = parentId;
    }

    public Long getParentId() 
    {
        return parentId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setFullName(String fullName) 
    {
        this.fullName = fullName;
    }

    public String getFullName() 
    {
        return fullName;
    }
    public void setPinyin(String pinyin) 
    {
        this.pinyin = pinyin;
    }

    public String getPinyin() 
    {
        return pinyin;
    }
    public void setAreaCode(String areaCode) 
    {
        this.areaCode = areaCode;
    }

    public String getAreaCode() 
    {
        return areaCode;
    }
    public void setStandardAreaCode(String standardAreaCode) 
    {
        this.standardAreaCode = standardAreaCode;
    }

    public String getStandardAreaCode() 
    {
        return standardAreaCode;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }
    public void setLng(BigDecimal lng) 
    {
        this.lng = lng;
    }

    public BigDecimal getLng() 
    {
        return lng;
    }
    public void setLat(BigDecimal lat) 
    {
        this.lat = lat;
    }

    public BigDecimal getLat() 
    {
        return lat;
    }
    public void setLabel(String label) 
    {
        this.label = label;
    }

    public String getLabel() 
    {
        return label;
    }
    public void setCountryType(Integer countryType) 
    {
        this.countryType = countryType;
    }

    public Integer getCountryType()
    {
        return countryType;
    }
    public void setSort(Integer sort)
    {
        this.sort = sort;
    }

    public Integer getSort()
    {
        return sort;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }
    public void setUpdateUserId(Long updateUserId) 
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId() 
    {
        return updateUserId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("parentId", getParentId())
            .append("name", getName())
            .append("fullName", getFullName())
            .append("pinyin", getPinyin())
            .append("areaCode", getAreaCode())
            .append("standardAreaCode", getStandardAreaCode())
            .append("type", getType())
            .append("lng", getLng())
            .append("lat", getLat())
            .append("label", getLabel())
            .append("countryType", getCountryType())
            .append("sort", getSort())
            .append("delFlag", getDelFlag())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
