package com.ruoyi.common.enuma;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum SettlementStatusEnum implements BaseEnum {

    NOT_SETTLEMENT(0, "未结算"),
    SETTLEMENT(1, "已结算")
    ;

    private Integer value;
    private String desc;
}
