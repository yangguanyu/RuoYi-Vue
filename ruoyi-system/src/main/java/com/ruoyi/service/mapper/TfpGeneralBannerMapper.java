package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.TfpGeneralBanner;

/**
 * bannerMapper接口
 * 
 * @author ruoyi
 * @date 2023-11-01
 */
public interface TfpGeneralBannerMapper 
{
    /**
     * 查询banner
     * 
     * @param id banner主键
     * @return banner
     */
    public TfpGeneralBanner selectTfpGeneralBannerById(Long id);

    /**
     * 查询banner列表
     * 
     * @param tfpGeneralBanner banner
     * @return banner集合
     */
    public List<TfpGeneralBanner> selectTfpGeneralBannerList(TfpGeneralBanner tfpGeneralBanner);

    /**
     * 新增banner
     * 
     * @param tfpGeneralBanner banner
     * @return 结果
     */
    public int insertTfpGeneralBanner(TfpGeneralBanner tfpGeneralBanner);

    /**
     * 修改banner
     * 
     * @param tfpGeneralBanner banner
     * @return 结果
     */
    public int updateTfpGeneralBanner(TfpGeneralBanner tfpGeneralBanner);

    /**
     * 删除banner
     * 
     * @param id banner主键
     * @return 结果
     */
    public int deleteTfpGeneralBannerById(Long id);

    /**
     * 批量删除banner
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpGeneralBannerByIds(Long[] ids);
}
