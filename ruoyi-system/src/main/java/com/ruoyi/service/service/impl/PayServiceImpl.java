package com.ruoyi.service.service.impl;

import com.ruoyi.common.core.domain.entity.SysUserWx;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.service.service.IPayService;
import com.ruoyi.service.service.ITfpOrderService;
import com.ruoyi.system.service.ISysUserWxService;
import com.wechat.pay.java.core.Config;
import com.wechat.pay.java.core.RSAAutoCertificateConfig;
import com.wechat.pay.java.core.exception.ValidationException;
import com.wechat.pay.java.service.partnerpayments.jsapi.model.Transaction;
import com.wechat.pay.java.service.payments.jsapi.model.Amount;
import com.wechat.pay.java.service.payments.jsapi.JsapiService;
import com.wechat.pay.java.service.payments.jsapi.model.PrepayResponse;
import com.wechat.pay.java.service.payments.jsapi.model.PrepayRequest;
import com.wechat.pay.java.service.payments.jsapi.model.Payer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

@Service
@Transactional
@Slf4j
public class PayServiceImpl implements IPayService {
    /** 商户号 */
    @Value("${merchant.id}")
    public String merchantId;
    /** 商户API私钥路径 */
    @Value("${private.key.path}")
    public String privateKeyPath;
    /** 商户证书序列号 */
    @Value("${merchant.serial.number}")
    public String merchantSerialNumber;
    /** 商户APIV3密钥 */
    @Value("${api.v3.key}")
    public String apiV3Key;

    @Value("${appid}")
    public String appid;

    @Value("${pay.notify.url}")
    private String notifyUrl;

    @Autowired
    private ISysUserWxService userWxService;
    @Autowired
    private ITfpOrderService orderService;

    private DateFormat expireDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss+08:00");

    @Override
    public String createWeChatOrder(String outTradeNo, BigDecimal finalAmount, String description, Date expireDate){
        LoginUser loginUser = SecurityUtils.getLoginUser();
        if (Objects.isNull(loginUser)){
            throw new RuntimeException("用户请登录");
        }

        SysUserWx sysUserWx = userWxService.findSysUserWxByUserId(loginUser.getUserId());
        if (Objects.isNull(sysUserWx)){
            throw new RuntimeException("用户微信基础信息不完善");
        }

        String openId = sysUserWx.getOpenId();
        Integer total = finalAmount.multiply(new BigDecimal(100)).intValue();

        // 使用自动更新平台证书的RSA配置
        // 一个商户号只能初始化一个配置，否则会因为重复的下载任务报错
        Config config =
                new RSAAutoCertificateConfig.Builder()
                        .merchantId(merchantId)
                        .privateKeyFromPath(privateKeyPath)
                        .merchantSerialNumber(merchantSerialNumber)
                        .apiV3Key(apiV3Key)
                        .build();
        // 构建service
        JsapiService service = new JsapiService.Builder().config(config).build();

        // request.setXxx(val)设置所需参数，具体参数可见Request定义
        PrepayRequest request = new PrepayRequest();
        Amount amount = new Amount();
        amount.setTotal(total);
        request.setAmount(amount);
        request.setAppid(appid);
        request.setMchid(merchantId);
        request.setDescription(description);
        request.setNotifyUrl(notifyUrl);
        request.setOutTradeNo(outTradeNo);
        if (Objects.nonNull(expireDate)){
            request.setTimeExpire(expireDateFormat.format(expireDate));
        }

        Payer payer = new Payer();
        payer.setOpenid(openId);
        request.setPayer(payer);

        // 调用下单方法，得到应答
        PrepayResponse response = service.prepay(request);
        String prepayId = response.getPrepayId();
        System.out.println("prepayId = " + prepayId);
        return prepayId;
    }

    /**
     * 微信支付回调业务处理
     * @param transaction
     */
    @Override
    public void payNotify(Transaction transaction) throws ValidationException {
        String outTradeNo = transaction.getOutTradeNo();
        log.info("payNotify:outTradeNo:" + outTradeNo);

        orderService.handlePaySuccessAfterInfo(outTradeNo);
    }
}
