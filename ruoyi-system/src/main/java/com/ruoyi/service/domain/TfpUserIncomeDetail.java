package com.ruoyi.service.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 用户收益明细对象 tfp_user_income_detail
 * 
 * @author ruoyi
 * @date 2024-01-16
 */
public class TfpUserIncomeDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 用户收益主表id */
    @Excel(name = "用户收益主表id")
    private Long userIncomeId;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 受邀人用户id */
    @Excel(name = "受邀人用户id")
    private Long inviteeUserId;

    /** 收益来源类型 0-商品 1-搭子 */
    @Excel(name = "收益来源类型 0-商品 1-搭子")
    private Integer sourceType;

    /** 收益来源id */
    @Excel(name = "收益来源id")
    private Long sourceId;

    /** 收益来源金额 */
    @Excel(name = "收益来源金额")
    private BigDecimal sourcePrice;

    /** 佣金金额 */
    @Excel(name = "佣金金额")
    private BigDecimal price;

    /** 状态 0- 受邀人查看 1-结算佣金/已完成 2-受邀人下单 3-受邀人付款 4-出行完成 */
    @Excel(name = "状态 0- 受邀人查看 1-结算佣金/已完成 2-受邀人下单 3-受邀人付款 4-出行完成")
    private Integer status;

    /** 订单id */
    @Excel(name = "订单id")
    private Long orderId;

    /** 订单编码 */
    @Excel(name = "订单编码")
    private String orderNumber;

    /** 结算单号 */
    @Excel(name = "结算单号")
    private String settlementNumber;

    /** 受邀人查看 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "受邀人查看", width = 30, dateFormat = "yyyy-MM-dd")
    private Date viewTime;

    /** 受邀人下单时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "受邀人下单时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date placeOrderTime;

    /** 受邀人付款时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "受邀人付款时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date payTime;

    /** 出行完成时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "出行完成时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date travelCompleteTime;

    /** 结算佣金时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结算佣金时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date settlementCommissionTime;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    @Excel(name = "创建者id")
    private Long createUserId;

    /** 更新者id */
    @Excel(name = "更新者id")
    private Long updateUserId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserIncomeId(Long userIncomeId) 
    {
        this.userIncomeId = userIncomeId;
    }

    public Long getUserIncomeId() 
    {
        return userIncomeId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setInviteeUserId(Long inviteeUserId) 
    {
        this.inviteeUserId = inviteeUserId;
    }

    public Long getInviteeUserId() 
    {
        return inviteeUserId;
    }
    public void setSourceType(Integer sourceType) 
    {
        this.sourceType = sourceType;
    }

    public Integer getSourceType() 
    {
        return sourceType;
    }
    public void setSourceId(Long sourceId) 
    {
        this.sourceId = sourceId;
    }

    public Long getSourceId() 
    {
        return sourceId;
    }

    public BigDecimal getSourcePrice() {
        return sourcePrice;
    }

    public void setSourcePrice(BigDecimal sourcePrice) {
        this.sourcePrice = sourcePrice;
    }

    public void setPrice(BigDecimal price)
    {
        this.price = price;
    }

    public BigDecimal getPrice() 
    {
        return price;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setOrderNumber(String orderNumber) 
    {
        this.orderNumber = orderNumber;
    }

    public String getOrderNumber() 
    {
        return orderNumber;
    }

    public String getSettlementNumber() {
        return settlementNumber;
    }

    public void setSettlementNumber(String settlementNumber) {
        this.settlementNumber = settlementNumber;
    }

    public void setViewTime(Date viewTime)
    {
        this.viewTime = viewTime;
    }

    public Date getViewTime() 
    {
        return viewTime;
    }
    public void setPlaceOrderTime(Date placeOrderTime) 
    {
        this.placeOrderTime = placeOrderTime;
    }

    public Date getPlaceOrderTime() 
    {
        return placeOrderTime;
    }
    public void setPayTime(Date payTime) 
    {
        this.payTime = payTime;
    }

    public Date getPayTime() 
    {
        return payTime;
    }
    public void setTravelCompleteTime(Date travelCompleteTime) 
    {
        this.travelCompleteTime = travelCompleteTime;
    }

    public Date getTravelCompleteTime() 
    {
        return travelCompleteTime;
    }
    public void setSettlementCommissionTime(Date settlementCommissionTime) 
    {
        this.settlementCommissionTime = settlementCommissionTime;
    }

    public Date getSettlementCommissionTime() 
    {
        return settlementCommissionTime;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }
    public void setUpdateUserId(Long updateUserId) 
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId() 
    {
        return updateUserId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userIncomeId", getUserIncomeId())
            .append("userId", getUserId())
            .append("inviteeUserId", getInviteeUserId())
            .append("sourceType", getSourceType())
            .append("sourceId", getSourceId())
            .append("sourcePrice", getSourcePrice())
            .append("price", getPrice())
            .append("status", getStatus())
            .append("orderId", getOrderId())
            .append("orderNumber", getOrderNumber())
            .append("settlementNumber", getSettlementNumber())
            .append("viewTime", getViewTime())
            .append("placeOrderTime", getPlaceOrderTime())
            .append("payTime", getPayTime())
            .append("travelCompleteTime", getTravelCompleteTime())
            .append("settlementCommissionTime", getSettlementCommissionTime())
            .append("delFlag", getDelFlag())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
