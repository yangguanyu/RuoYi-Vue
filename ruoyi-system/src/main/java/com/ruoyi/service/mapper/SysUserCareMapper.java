package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.SysUserCare;
import com.ruoyi.service.vo.CareUserInfoVo;
import com.ruoyi.service.vo.FansUserInfoVo;
import org.apache.ibatis.annotations.Param;

/**
 * 关注Mapper接口
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public interface SysUserCareMapper 
{
    /**
     * 查询关注
     * 
     * @param id 关注主键
     * @return 关注
     */
    public SysUserCare selectSysUserCareById(Long id);

    /**
     * 查询关注列表
     * 
     * @param sysUserCare 关注
     * @return 关注集合
     */
    public List<SysUserCare> selectSysUserCareList(SysUserCare sysUserCare);

    /**
     * 新增关注
     * 
     * @param sysUserCare 关注
     * @return 结果
     */
    public int insertSysUserCare(SysUserCare sysUserCare);

    /**
     * 修改关注
     * 
     * @param sysUserCare 关注
     * @return 结果
     */
    public int updateSysUserCare(SysUserCare sysUserCare);

    /**
     * 删除关注
     * 
     * @param id 关注主键
     * @return 结果
     */
    public int deleteSysUserCareById(Long id);

    /**
     * 批量删除关注
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysUserCareByIds(Long[] ids);

    List<Long> selectOwnerUserIdList();

    List<Long> selectFansUserIdList();

    Integer selectCareNumber(@Param("userId") Long userId);

    Integer selectFansNumber(@Param("userId") Long userId);

    List<CareUserInfoVo> careUserInfoList(@Param("userId") Long userId);

    List<FansUserInfoVo> fansUserInfoList(@Param("userId") Long userId);
}
