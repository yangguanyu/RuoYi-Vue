package com.ruoyi.common.enuma;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 角色类型
 *
 * @author
 * @date
 * @time
 */
@Getter
@AllArgsConstructor
public enum ChatUserTypeEnum implements BaseEnum {


    BOOT_MAN(0, "发起者"),


    APPLIED(1, "已报名"),

    UN_APPLIED(2, "未报名")
    ;


    private Integer value;
    private String desc;
}
