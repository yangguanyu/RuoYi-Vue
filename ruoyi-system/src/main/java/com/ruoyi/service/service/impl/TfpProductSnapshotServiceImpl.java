package com.ruoyi.service.service.impl;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.*;
import java.util.stream.Collectors;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enuma.DeleteEnum;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.DcDateUtils;
import com.ruoyi.common.utils.DcListUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.service.domain.*;
import com.ruoyi.service.dto.SnapshotTextDto;
import com.ruoyi.service.service.*;
import com.ruoyi.service.vo.MyProductSnapshotListVo;
import com.ruoyi.service.vo.ProductSnapshotDetailVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpProductSnapshotMapper;
import org.springframework.transaction.annotation.Transactional;

import static com.ruoyi.common.utils.PageUtils.startPage;

/**
 * 商品快照Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@Service
@Transactional
public class TfpProductSnapshotServiceImpl implements ITfpProductSnapshotService 
{
    @Autowired
    private TfpProductSnapshotMapper tfpProductSnapshotMapper;
    @Autowired
    private ITfpProductService productService;
    @Autowired
    private ITfpProductSubService productSubService;
    @Autowired
    private ITfpPurchaseShopNumberService purchaseShopNumberService;
    @Autowired
    private ITfpSelfFundedProjectService selfFundedProjectService;
    @Autowired
    private ITfpProductCbcService productCbcService;
    @Autowired
    private ITfpEveryDayPriceService everyDayPriceService;
    @Autowired
    private ITfpProductWantToGoService productWantToGoService;
    @Autowired
    private ITfpTravelRuleService tfpTravelRuleService;

    /**
     * 查询商品快照
     * 
     * @param id 商品快照主键
     * @return 商品快照
     */
    @Override
    public TfpProductSnapshot selectTfpProductSnapshotById(Long id)
    {
        return tfpProductSnapshotMapper.selectTfpProductSnapshotById(id);
    }

    /**
     * 查询商品快照列表
     * 
     * @param tfpProductSnapshot 商品快照
     * @return 商品快照
     */
    @Override
    public List<TfpProductSnapshot> selectTfpProductSnapshotList(TfpProductSnapshot tfpProductSnapshot)
    {
        return tfpProductSnapshotMapper.selectTfpProductSnapshotList(tfpProductSnapshot);
    }

    /**
     * 新增商品快照
     * 
     * @param tfpProductSnapshot 商品快照
     * @return 结果
     */
    @Override
    public int insertTfpProductSnapshot(TfpProductSnapshot tfpProductSnapshot)
    {
        tfpProductSnapshot.setCreateTime(DateUtils.getNowDate());
        return tfpProductSnapshotMapper.insertTfpProductSnapshot(tfpProductSnapshot);
    }

    /**
     * 修改商品快照
     * 
     * @param tfpProductSnapshot 商品快照
     * @return 结果
     */
    @Override
    public int updateTfpProductSnapshot(TfpProductSnapshot tfpProductSnapshot)
    {
        tfpProductSnapshot.setUpdateTime(DateUtils.getNowDate());
        return tfpProductSnapshotMapper.updateTfpProductSnapshot(tfpProductSnapshot);
    }

    /**
     * 批量删除商品快照
     * 
     * @param ids 需要删除的商品快照主键
     * @return 结果
     */
    @Override
    public int deleteTfpProductSnapshotByIds(Long[] ids)
    {
        return tfpProductSnapshotMapper.deleteTfpProductSnapshotByIds(ids);
    }

    /**
     * 删除商品快照信息
     * 
     * @param id 商品快照主键
     * @return 结果
     */
    @Override
    public int deleteTfpProductSnapshotById(Long id)
    {
        return tfpProductSnapshotMapper.deleteTfpProductSnapshotById(id);
    }

    @Override
    public TfpProductSnapshot selectTfpProductSnapshotByProductIdAndUserId(Long productId, Long createUserId) {
        return tfpProductSnapshotMapper.selectTfpProductSnapshotByProductIdAndUserId(productId, createUserId);
    }

    /**
     * 创建商品快照
     * @param productId
     * @param finalAmount
     * @param lowPriceAirTicketFlag
     * @param ruleId
     * @return
     */
    @Override
    public Long createSnapshot(Long productId, BigDecimal finalAmount, Date goDate, String goPlaceName, Integer travelType, Long orderChatGroupId, Integer lowPriceAirTicketFlag, Long ruleId) {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        SnapshotTextDto snapshotTextDto = new SnapshotTextDto();

        TfpProduct tfpProduct = productService.selectTfpProductById(productId);
        TfpProductSub tfpProductSub = productSubService.selectTfpProductSubByProductId(productId);
        List<TfpPurchaseShopNumber> tfpPurchaseShopNumberList = purchaseShopNumberService.selectListByProductId(productId);
        List<TfpSelfFundedProject> tfpSelfFundedProjectList = selfFundedProjectService.selectListByProductId(productId);
        List<TfpProductCbc> tfpProductCbcList = productCbcService.selectTfpProductCbcListByProductId(productId);
        List<TfpEveryDayPrice> tfpEveryDayPriceList = everyDayPriceService.selectEveryDayPriceListByProductId(productId);

        TfpProductWantToGo param = new TfpProductWantToGo();
        param.setId(productId);
        param.setDelFlag(DeleteEnum.EXIST.getValue());
        List<TfpProductWantToGo> tfpProductWantToGoList = productWantToGoService.selectTfpProductWantToGoList(param);

        snapshotTextDto.setTfpProduct(tfpProduct);
        snapshotTextDto.setTfpProductSub(tfpProductSub);
        snapshotTextDto.setTfpPurchaseShopNumberList(tfpPurchaseShopNumberList);
        snapshotTextDto.setTfpSelfFundedProjectList(tfpSelfFundedProjectList);
        snapshotTextDto.setTfpProductCbcList(tfpProductCbcList);
        snapshotTextDto.setTfpEveryDayPriceList(tfpEveryDayPriceList);
        snapshotTextDto.setTfpProductWantToGoList(tfpProductWantToGoList);

        ProductSnapshotDetailVo productSnapshotDetailVo = productService.productSnapshotDetail(productId, snapshotTextDto, travelType, orderChatGroupId);
        productSnapshotDetailVo.setGoPlaceName(goPlaceName);

        DateFormat dcDateFormat = DcDateUtils.getDcDateFormat(DcDateUtils.yyyyMMdd);
        productSnapshotDetailVo.setGoDateStr(dcDateFormat.format(goDate));
        productSnapshotDetailVo.setLowPriceAirTicketFlag(lowPriceAirTicketFlag);
        productSnapshotDetailVo.setRuleId(ruleId);
        snapshotTextDto.setProductSnapshotDetailVo(productSnapshotDetailVo);

        TfpProductSnapshot tfpProductSnapshot = new TfpProductSnapshot();
        tfpProductSnapshot.setProductId(productId);
        tfpProductSnapshot.setSupplyId(tfpProduct.getSupplierId());
        tfpProductSnapshot.setSnapshotValue(JSON.toJSONString(snapshotTextDto));
        tfpProductSnapshot.setSalePrice(finalAmount);
        tfpProductSnapshot.setDelFlag(DeleteEnum.EXIST.getValue());
        tfpProductSnapshot.setCreateUserId(loginUser.getUserId());
        tfpProductSnapshot.setCreateBy(loginUser.getUsername());
        tfpProductSnapshot.setCreateTime(DateUtils.getNowDate());
        tfpProductSnapshotMapper.insertTfpProductSnapshot(tfpProductSnapshot);
        return tfpProductSnapshot.getId();
    }

    @Override
    public List<MyProductSnapshotListVo> myProductSnapshotList() {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = loginUser.getUserId();

        startPage();
        List<MyProductSnapshotListVo> myProductSnapshotListVoList = tfpProductSnapshotMapper.myProductSnapshotList(userId);
        if (DcListUtils.isNotEmpty(myProductSnapshotListVoList)){
            List<Long> idList = myProductSnapshotListVoList.stream().map(MyProductSnapshotListVo::getId).collect(Collectors.toList());
            List<TfpProductSnapshot> tfpProductSnapshotList = tfpProductSnapshotMapper.selectTfpProductSnapshotByIdList(idList);
            Map<Long, TfpProductSnapshot> tfpProductSnapshotMap = tfpProductSnapshotList.stream().collect(Collectors.toMap(TfpProductSnapshot::getId, TfpProductSnapshot -> TfpProductSnapshot));

            for (MyProductSnapshotListVo myProductSnapshotListVo : myProductSnapshotListVoList) {
                TfpProductSnapshot productSnapshot = tfpProductSnapshotMap.get(myProductSnapshotListVo.getId());
                String snapshotValue = productSnapshot.getSnapshotValue();
                SnapshotTextDto snapshotTextDto = JSON.parseObject(snapshotValue, SnapshotTextDto.class);
                ProductSnapshotDetailVo productSnapshotDetailVo = snapshotTextDto.getProductSnapshotDetailVo();

                BeanUtils.copyProperties(productSnapshotDetailVo, myProductSnapshotListVo);
                myProductSnapshotListVo.setId(productSnapshot.getId());
            }
        }

        return myProductSnapshotListVoList;
    }

    @Override
    public ProductSnapshotDetailVo productSnapshotDetail(Long id) {
        TfpProductSnapshot productSnapshot = tfpProductSnapshotMapper.selectTfpProductSnapshotById(id);
        String snapshotValue = productSnapshot.getSnapshotValue();
        SnapshotTextDto snapshotTextDto = JSONObject.parseObject(snapshotValue, SnapshotTextDto.class);
        ProductSnapshotDetailVo productSnapshotDetailVo = snapshotTextDto.getProductSnapshotDetailVo();

        Long chatGroupId = productSnapshotDetailVo.getChatGroupId();
        if (Objects.isNull(chatGroupId)){
            productSnapshotDetailVo.setChatGroupInfo(null);
        }

        Long ruleId = productSnapshotDetailVo.getRuleId();
        if (Objects.nonNull(ruleId)){
            TfpTravelRule tfpTravelRule = tfpTravelRuleService.selectTfpTravelRuleById(ruleId);
            if (Objects.nonNull(tfpTravelRule)){
                productSnapshotDetailVo.setText(tfpTravelRule.getText());
            }
        }

        return productSnapshotDetailVo;
    }
}
