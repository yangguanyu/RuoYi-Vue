import request from '@/utils/request'

// 查询聊天群组列表
export function listChatGroup(query) {
  return request({
    url: '/service/chatGroup/list',
    method: 'get',
    params: query
  })
}

// 查询聊天群组详细
export function getChatGroup(id) {
  return request({
    url: '/service/chatGroup/' + id,
    method: 'get'
  })
}

// 新增聊天群组
export function addChatGroup(data) {
  return request({
    url: '/service/chatGroup',
    method: 'post',
    data: data
  })
}

// 修改聊天群组
export function updateChatGroup(data) {
  return request({
    url: '/service/chatGroup',
    method: 'put',
    data: data
  })
}

// 删除聊天群组
export function delChatGroup(id) {
  return request({
    url: '/service/chatGroup/' + id,
    method: 'delete'
  })
}

export function findChatGroupDetail(id) {
  return request({
    url: '/service/chatGroup/detail/' + id,
    method: 'get'
  })
}

export function chatGroupInfoList(query) {
  return request({
    url: '/service/chatGroupPersonInfo/list',
    method: 'get',
    params: query
  })
}
