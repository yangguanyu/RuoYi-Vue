package com.ruoyi.service.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 airport_codes
 * 
 * @author ruoyi
 * @date 2024-04-28
 */
public class AirportCodes extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    private Long id;

    /** 城市名称 */
    @Excel(name = "城市名称")
    private String city;

    /** 城市三字码 */
    @Excel(name = "城市编码")
    private String code;

    /** 机场名称 */
    @Excel(name = "机场名称")
    private String airportName;

    /** 机场三字码 */
    @Excel(name = "机场编码")
    private String airportCode;

    /** 省 */
    @Excel(name = "省")
    private String province;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCity(String city) 
    {
        this.city = city;
    }

    public String getCity() 
    {
        return city;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }

    public String getAirportName() {
        return airportName;
    }

    public void setAirportName(String airportName) {
        this.airportName = airportName;
    }

    public String getAirportCode() {
        return airportCode;
    }

    public void setAirportCode(String airportCode) {
        this.airportCode = airportCode;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("city", getCity())
            .append("code", getCode())
            .append("airportName", getAirportName())
            .append("airportCode", getAirportCode())
            .append("province", getProvince())
            .toString();
    }
}
