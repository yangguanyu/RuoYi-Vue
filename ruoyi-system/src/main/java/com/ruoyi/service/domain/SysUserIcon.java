package com.ruoyi.service.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 用户图片对象 sys_user_icon
 * 
 * @author ruoyi
 * @date 2023-11-01
 */
public class SysUserIcon extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 图片名称 */
    @Excel(name = "图片名称")
    private String name;

    /** 图片地址 */
    @Excel(name = "图片地址")
    private String path;

    /** 是否是头像 0-否 1-是 */
    @Excel(name = "是否是头像 0-否 1-是")
    private Integer headSculptureFlag;

    /** 是否本人状态审核 0-未上传 1-已上传未审核 2-已上传审核通过 */
    @Excel(name = "是否本人状态审核 0-未上传 1-已上传未审核 2-已上传审核通过")
    private Integer uploadHeadImageFlag;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    @Excel(name = "创建者id")
    private Long createUserId;

    /** 更新者id */
    @Excel(name = "更新者id")
    private Long updateUserId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setPath(String path) 
    {
        this.path = path;
    }

    public String getPath() 
    {
        return path;
    }
    public void setHeadSculptureFlag(Integer headSculptureFlag) 
    {
        this.headSculptureFlag = headSculptureFlag;
    }

    public Integer getHeadSculptureFlag() 
    {
        return headSculptureFlag;
    }
    public void setUploadHeadImageFlag(Integer uploadHeadImageFlag) 
    {
        this.uploadHeadImageFlag = uploadHeadImageFlag;
    }

    public Integer getUploadHeadImageFlag() 
    {
        return uploadHeadImageFlag;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }
    public void setUpdateUserId(Long updateUserId) 
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId() 
    {
        return updateUserId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("name", getName())
            .append("path", getPath())
            .append("headSculptureFlag", getHeadSculptureFlag())
            .append("uploadHeadImageFlag", getUploadHeadImageFlag())
            .append("delFlag", getDelFlag())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
