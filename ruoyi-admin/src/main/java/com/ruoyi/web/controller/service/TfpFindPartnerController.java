package com.ruoyi.web.controller.service;

import java.util.*;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.page.TfpDataInfo;
import com.ruoyi.common.enuma.*;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.DcListUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.bean.CustomizeBeanCopier;
import com.ruoyi.common.core.domain.entity.TfpAttachment;
import com.ruoyi.service.domain.TfpFindPartnerSignUp;
import com.ruoyi.service.dto.AuditDto;
import com.ruoyi.service.dto.TfpFindPartnerDto;
import com.ruoyi.service.service.ITfpAttachmentService;
import com.ruoyi.service.service.ITfpFindPartnerSignUpService;
import com.ruoyi.service.vo.*;
import com.ruoyi.system.service.ISysUserService;
import com.ruoyi.web.controller.service.form.partner.FindPartnerUpdForm;
import com.ruoyi.web.controller.service.form.partner.TfpFindPartnerForm;
import com.ruoyi.web.controller.service.form.attachment.AttachmentForm;
import com.ruoyi.web.controller.service.form.partner.FindPartnerAddForm;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.compress.utils.Lists;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.TfpFindPartner;
import com.ruoyi.service.service.ITfpFindPartnerService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 找搭子Controller
 *
 * @author ruoyi
 * @date 2023-11-01
 */
@RestController
@RequestMapping("/service/findPartner")
@Api("找搭子接口")
public class TfpFindPartnerController extends BaseController
{
    @Autowired
    private ITfpFindPartnerService tfpFindPartnerService;
    @Autowired
    private ITfpAttachmentService attachmentService;
    @Autowired
    private ISysUserService userService;
    @Autowired
    private ITfpFindPartnerSignUpService tfpFindPartnerSignUpService;

    /**
     * 查询找搭子列表
     */
    @PreAuthorize("@ss.hasPermi('service:findPartner:list')")
    @GetMapping("/list")
    public TableDataInfo list(TfpFindPartnerDto tfpFindPartner)
    {
        List<TfpFindPartner> list = tfpFindPartnerService.selectManageFindPartnerList(tfpFindPartner);
        return getDataTable(list);
    }

    /**
     * 导出找搭子列表
     */
    @PreAuthorize("@ss.hasPermi('service:findPartner:export')")
    @Log(title = "找搭子", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TfpFindPartnerDto tfpFindPartner)
    {
        List<TfpFindPartner> list = tfpFindPartnerService.selectTfpFindPartnerList(tfpFindPartner);
        ExcelUtil<TfpFindPartner> util = new ExcelUtil<TfpFindPartner>(TfpFindPartner.class);
        util.exportExcel(response, list, "找搭子数据");
    }

    /**
     * 获取找搭子详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:findPartner:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tfpFindPartnerService.selectTfpFindPartnerById(id));
    }

    /**
     * 新增找搭子
     */
    @PreAuthorize("@ss.hasPermi('service:findPartner:add')")
    @Log(title = "找搭子", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TfpFindPartner tfpFindPartner)
    {
        return toAjax(tfpFindPartnerService.insertTfpFindPartner(tfpFindPartner));
    }

    /**
     * 修改找搭子
     */
    @PreAuthorize("@ss.hasPermi('service:findPartner:edit')")
    @Log(title = "找搭子", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TfpFindPartner tfpFindPartner)
    {
        return toAjax(tfpFindPartnerService.updateTfpFindPartner(tfpFindPartner));
    }

    /**
     * 删除找搭子
     */
    @PreAuthorize("@ss.hasPermi('service:findPartner:remove')")
    @Log(title = "找搭子", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tfpFindPartnerService.deleteTfpFindPartnerByIds(ids));
    }

    @PreAuthorize("@ss.hasPermi('service:findPartner:query')")
    @GetMapping(value = "/detail/{id}")
    public R<FindPartnerDetailVo> detail(@PathVariable("id") Long id)
    {
        return R.ok(tfpFindPartnerService.detail(id));
    }

    @PostMapping("/applet/addPartner")
    @ApiOperation("小程序-添加找搭子信息")
    public AjaxResult appletAddPartner(@RequestBody @Valid FindPartnerAddForm form) {
        //处理图片-待封装，添加状态，创建人等等
//        tfpFindPartner.setStatus(FindPartnerStatusEnum.PUBLISH_AND_PRE_AUTH.getValue());handleAddPartner已经做了
        int result = 0;
        if(Objects.isNull(form.getId())){
            TfpFindPartner tfpFindPartner = handleAddPartner(form);
            result = tfpFindPartnerService.appletInsert(tfpFindPartner);
        }else {
            TfpFindPartner tfpFindPartner = handleUpdatePartner(form);
            result = tfpFindPartnerService.appletUpdate(tfpFindPartner);
        }
        return toAjax(result);
    }

//    @ApiOperation("小程序-审核找搭子")
//    @PostMapping("/auditFindPartner")
//    @ResponseBody
//    public R auditFindPartner(@RequestBody AuditDto auditDto){
//        return R.ok(tfpFindPartnerService.auditFindPartner(auditDto));
//    }

    @GetMapping(value = "/applet/detail/{id}")
    @ApiOperation("小程序-找搭子详情，小程序专用")
    public R<FindPartnerDetailAppletVo> appletDetail(@PathVariable("id") Long id) {
        FindPartnerDetailAppletVo detail = tfpFindPartnerService.appletDetail(id);
        return R.ok(detail);
    }

    @ApiOperation("小程序-我发起的搭子")
    @PostMapping("/myInitiateFindPartnerList")
    @ResponseBody
    public TableDataInfo<InitiateFindPartnerVo> myInitiateFindPartnerList()
    {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = loginUser.getUserId();
        return getDataTable(tfpFindPartnerService.initiateFindPartnerList(userId));
    }

    @ApiOperation("小程序-他发起的搭子")
    @PostMapping("/otherInitiateFindPartnerList")
    @ResponseBody
    public TableDataInfo<InitiateFindPartnerVo> otherInitiateFindPartnerList(@RequestParam Long userId)
    {
        return getDataTable(tfpFindPartnerService.initiateFindPartnerList(userId));
    }


    @ApiOperation("小程序-找搭子列表请求")
    @PostMapping("/applet/list")
    public TfpDataInfo<TfpFindPartnerVO> appletList(@RequestBody TfpFindPartnerForm form) {
        form.setStatus(FindPartnerStatusEnum.PASS.getValue());
        TfpFindPartnerDto tfpFindPartner = CustomizeBeanCopier.copyProperties(form, TfpFindPartnerDto.class);
        startPage();
        List<TfpFindPartner> list = tfpFindPartnerService.selectTfpFindPartnerList(tfpFindPartner);
        TfpDataInfo tfpDataTable = getTfpDataTable(list);

        if(CollectionUtils.isNotEmpty(list)){
            List<Long> idList = list.stream().map(TfpFindPartner::getId).collect(Collectors.toList());
            List<TfpAttachment> findPartnerAttachmentList = attachmentService.selectList(idList, BusinessTypeEnum.FIND_PARTNER.getValue(), BusinessSubTypeEnum.PARTNER_IMAGE.getValue());
            Map<Long, List<TfpAttachment>> findPartnerImageMap;
            if (DcListUtils.isNotEmpty(findPartnerAttachmentList)){
                findPartnerImageMap = findPartnerAttachmentList.stream().collect(Collectors.groupingBy(TfpAttachment::getBusinessId));
            } else {
                findPartnerImageMap = new HashMap<>();
            }

            // 获取报名通过信息
            Map<Long, List<TfpFindPartnerSignUp>> signUpAuditPassMap = tfpFindPartnerService.getSignUpAuditPassListByIdList(idList);

            //获取用户id
            List<Long> createUserList = list.stream().map(TfpFindPartner::getCreateUserId).collect(Collectors.toList());
            //获取用户信息和头像
            Map<Long, SysUser> userInfoAndAvatar = userService.getUserInfoAndAvatar(createUserList);

            List<TfpFindPartnerVO> tfpFindPartnerVOS = CustomizeBeanCopier.copyPropertiesOfList(list, TfpFindPartnerVO.class);
            for (TfpFindPartnerVO tfpFindPartnerVO : tfpFindPartnerVOS) {
                List<TfpAttachment> findPartnerImageList = findPartnerImageMap.get(tfpFindPartnerVO.getId());
                if (DcListUtils.isNotEmpty(findPartnerImageList)){
                    tfpFindPartnerVO.setImage(findPartnerImageList.get(0).getUrl());
                }

                //获取头像和人员信息
                SysUser sysUser = userInfoAndAvatar.get(tfpFindPartnerVO.getCreateUserId());
                if(Objects.nonNull(sysUser)){
                    tfpFindPartnerVO.setNickName(sysUser.getNickName());
                    if(Objects.nonNull(sysUser.getAttachment())){
                        tfpFindPartnerVO.setAvatar(sysUser.getAttachment().getUrl());
                    }
                }

                Integer signUpPersonNumber = 0;
                List<TfpFindPartnerSignUp> signUpList = signUpAuditPassMap.get(tfpFindPartnerVO.getId());
                if (DcListUtils.isNotEmpty(signUpList)){
                    signUpPersonNumber = signUpList.size();
                }
                tfpFindPartnerVO.setSignUpPersonNumber(signUpPersonNumber);
            }
            tfpDataTable.setRows(tfpFindPartnerVOS);
            return tfpDataTable;
        }
        return getTfpDataTable(Lists.newArrayList());
    }

    @ApiOperation("小程序-找搭子进行中列表")
    @PostMapping("/applet/progressList")
    public TfpDataInfo<PartnerGroupVO> progressList(@RequestBody TfpFindPartnerForm form) {
        form.setActivityStatus(ActivityStatusEnum.ING.getValue());
        form.setStatus(FindPartnerStatusEnum.PASS.getValue());
        TfpFindPartnerDto tfpFindPartner = CustomizeBeanCopier.copyProperties(form, TfpFindPartnerDto.class);
        startPage();
        List<TfpFindPartner> list = tfpFindPartnerService.selectProgressList(tfpFindPartner);
        TfpDataInfo tfpDataTable = getTfpDataTable(list);
        if(CollectionUtils.isNotEmpty(list)){
            List<Long> idList = list.stream().map(TfpFindPartner::getId).collect(Collectors.toList());
            List<TfpAttachment> findPartnerAttachmentList = attachmentService.selectList(idList, BusinessTypeEnum.FIND_PARTNER.getValue(), BusinessSubTypeEnum.PARTNER_IMAGE.getValue());
            Map<Long, List<TfpAttachment>> findPartnerImageMap;
            if (DcListUtils.isNotEmpty(findPartnerAttachmentList)){
                findPartnerImageMap = findPartnerAttachmentList.stream().collect(Collectors.groupingBy(TfpAttachment::getBusinessId));
            } else {
                findPartnerImageMap = new HashMap<>();
            }

            // 获取报名通过信息
            Map<Long, List<TfpFindPartnerSignUp>> signUpAuditPassMap = tfpFindPartnerService.getSignUpAuditPassListByIdList(idList);

            //获取用户id
            List<Long> createUserList = list.stream().map(TfpFindPartner::getCreateUserId).collect(Collectors.toList());
            //获取用户信息和头像
            Map<Long, SysUser> userInfoAndAvatar = userService.getUserInfoAndAvatar(createUserList);
            List<TfpFindPartnerVO> tfpFindPartnerVOS = CustomizeBeanCopier.copyPropertiesOfList(list, TfpFindPartnerVO.class);
            for (TfpFindPartnerVO tfpFindPartnerVO : tfpFindPartnerVOS) {
                List<TfpAttachment> findPartnerImageList = findPartnerImageMap.get(tfpFindPartnerVO.getId());
                if (DcListUtils.isNotEmpty(findPartnerImageList)){
                    tfpFindPartnerVO.setImage(findPartnerImageList.get(0).getUrl());
                }
                Date signUpEndTime = tfpFindPartnerVO.getSignUpEndTime();
                if(Objects.isNull(signUpEndTime)){
                    signUpEndTime = new Date();
                }
                //此处处理了日期，方便一会进行分组
                tfpFindPartnerVO.setGroupDay(DateUtils.handleSimpleDate(signUpEndTime));
                //获取头像和人员信息
                SysUser sysUser = userInfoAndAvatar.get(tfpFindPartnerVO.getCreateUserId());
                if(Objects.nonNull(sysUser)){
                    tfpFindPartnerVO.setNickName(sysUser.getNickName());
                    if(Objects.nonNull(sysUser.getAttachment())){
                        tfpFindPartnerVO.setAvatar(sysUser.getAttachment().getUrl());
                    }
                }

                Integer signUpPersonNumber = 0;
                List<TfpFindPartnerSignUp> signUpList = signUpAuditPassMap.get(tfpFindPartnerVO.getId());
                if (DcListUtils.isNotEmpty(signUpList)){
                    signUpPersonNumber = signUpList.size();
                }

                tfpFindPartnerVO.setSignUpPersonNumber(signUpPersonNumber);
            }

            //此时进行分组
            Map<String, List<TfpFindPartnerVO>> progressMap = tfpFindPartnerVOS.stream().collect(Collectors.groupingBy(TfpFindPartnerVO::getGroupDay));
            List<PartnerGroupVO> groupList = Lists.newArrayList();
            for (Map.Entry<String, List<TfpFindPartnerVO>> entry : progressMap.entrySet()) {
                PartnerGroupVO partnerGroupVO = new PartnerGroupVO();
                partnerGroupVO.setGroupDay(entry.getKey());
                partnerGroupVO.setPartnerList(entry.getValue());
                groupList.add(partnerGroupVO);
            }
            List<PartnerGroupVO> collect = groupList.stream().sorted(Comparator.comparing(PartnerGroupVO::getGroupDay).reversed()).collect(Collectors.toList());
            tfpDataTable.setRows(collect);
            return tfpDataTable;
        }
        return getTfpDataTable(Lists.newArrayList());
    }

//    public static void main(String[] args) {
//        List<PartnerGroupVO> list = new ArrayList<>();
//        PartnerGroupVO partnerGroupVO = new PartnerGroupVO();
//        partnerGroupVO.setGroupDay("06.25");
//        list.add(partnerGroupVO);
//
//        PartnerGroupVO partnerGroupVO2 = new PartnerGroupVO();
//        partnerGroupVO2.setGroupDay("06.30");
//        list.add(partnerGroupVO2);
//
//        PartnerGroupVO partnerGroupVO3 = new PartnerGroupVO();
//        partnerGroupVO3.setGroupDay("05.22");
//        list.add(partnerGroupVO3);
//
//        PartnerGroupVO partnerGroupVO4 = new PartnerGroupVO();
//        partnerGroupVO4.setGroupDay("07.01");
//        list.add(partnerGroupVO4);
//
//        List<PartnerGroupVO> collect = list.stream().sorted(Comparator.comparing(PartnerGroupVO::getGroupDay).reversed()).collect(Collectors.toList());
//        System.out.println(collect);
//    }
    @ApiOperation("小程序-我关注的人的搭子-真TND奇怪的需求")
    @PostMapping("/applet/focusList")
    public TfpDataInfo<TfpFindPartnerVO> appletFocusList(@RequestBody TfpFindPartnerForm form) {
        TfpFindPartnerDto tfpFindPartner = CustomizeBeanCopier.copyProperties(form, TfpFindPartnerDto.class);
        startPage();
        tfpFindPartner.setStatus(FindPartnerStatusEnum.PASS.getValue());
        tfpFindPartner.setFansId(SecurityUtils.getUserId());
        List<TfpFindPartner> list = tfpFindPartnerService.selectTfpFindPartnerList(tfpFindPartner);
        TfpDataInfo tfpDataTable = getTfpDataTable(list);
        if(CollectionUtils.isNotEmpty(list)){
            List<TfpFindPartnerVO> tfpFindPartnerVOS = CustomizeBeanCopier.copyPropertiesOfList(list, TfpFindPartnerVO.class);

            List<Long> createUserList = list.stream().map(TfpFindPartner::getCreateUserId).collect(Collectors.toList());
            List<Long> idList = list.stream().map(TfpFindPartner::getId).collect(Collectors.toList());

            //获取找搭子的图片
            List<TfpAttachment> findPartnerAttachmentList = attachmentService.selectList(idList, BusinessTypeEnum.FIND_PARTNER.getValue(), BusinessSubTypeEnum.PARTNER_IMAGE.getValue());
            Map<Long, List<TfpAttachment>> findPartnerImageMap;
            if (DcListUtils.isNotEmpty(findPartnerAttachmentList)){
                findPartnerImageMap = findPartnerAttachmentList.stream().collect(Collectors.groupingBy(TfpAttachment::getBusinessId));
            } else {
                findPartnerImageMap = new HashMap<>();
            }

            // 获取报名通过信息
            Map<Long, List<TfpFindPartnerSignUp>> signUpAuditPassMap = tfpFindPartnerService.getSignUpAuditPassListByIdList(idList);

            //获取用户信息和用户头像
            Map<Long, SysUser> userInfoAndAvatar = userService.getUserInfoAndAvatar(createUserList);
//            List<TfpFindPartnerVO> tfpFindPartnerVOS = CustomizeBeanCopier.copyPropertiesOfList(list, TfpFindPartnerVO.class);
            for (TfpFindPartnerVO tfpFindPartnerVO : tfpFindPartnerVOS) {
                List<TfpAttachment> findPartnerImageList = findPartnerImageMap.get(tfpFindPartnerVO.getId());
                if (DcListUtils.isNotEmpty(findPartnerImageList)){
                    tfpFindPartnerVO.setImage(findPartnerImageList.get(0).getUrl());
                }
                Date signUpEndTime = tfpFindPartnerVO.getSignUpEndTime();
                if(Objects.isNull(signUpEndTime)){
                    signUpEndTime = new Date();
                }
                //此处处理了日期，方便一会进行分组
//                tfpFindPartnerVO.setGroupDay(DateUtils.handleSimpleDate(signUpEndTime));
                //获取头像和人员信息
                SysUser sysUser = userInfoAndAvatar.get(tfpFindPartnerVO.getCreateUserId());
                if(Objects.nonNull(sysUser)){
                    tfpFindPartnerVO.setNickName(sysUser.getNickName());
                    if(Objects.nonNull(sysUser.getAttachment())){
                        tfpFindPartnerVO.setAvatar(sysUser.getAttachment().getUrl());
                    }
                }

                Integer signUpPersonNumber = 0;
                List<TfpFindPartnerSignUp> signUpList = signUpAuditPassMap.get(tfpFindPartnerVO.getId());
                if (DcListUtils.isNotEmpty(signUpList)){
                    signUpPersonNumber = signUpList.size();
                }
                tfpFindPartnerVO.setSignUpPersonNumber(signUpPersonNumber);
            }
            tfpDataTable.setRows(tfpFindPartnerVOS);
            return tfpDataTable;
        }
        return getTfpDataTable(Lists.newArrayList());
    }

    @ApiOperation("小程序-我参与的搭子列表")
    @PostMapping("/myJoinFindPartnerList")
    @ResponseBody
    public TableDataInfo<MyJoinFindPartnerVo> myJoinFindPartnerList(){
        return getDataTable(tfpFindPartnerService.myJoinFindPartnerList());
    }

    @PostMapping("/approve")
    @ApiOperation("后台-审核找搭子")
    public AjaxResult approve(@RequestBody @Valid FindPartnerUpdForm form) {
        //处理图片-待封装，添加状态，创建人等等
        TfpFindPartner tfpFindPartner = CustomizeBeanCopier.copyProperties(form, TfpFindPartner.class);
        int result = tfpFindPartnerService.approve(tfpFindPartner);
        return toAjax(result);

    }

    /**
     * 处理超过审核时间的搭子定时任务
     */
    public void handleOverAuditTimeTask(){
        List<TfpFindPartner> tfpFindPartnerList = tfpFindPartnerService.selectOverAuditTimeFindPartnerList();
        for (TfpFindPartner tfpFindPartner : tfpFindPartnerList) {
            tfpFindPartner.setStatus(FindPartnerStatusEnum.REJECT.getValue());
            tfpFindPartner.setReason("业务人员未审核");
            tfpFindPartnerService.updateTfpFindPartner(tfpFindPartner);
        }
    }

    /**
     * 对接口 {@link com.ruoyi.web.controller.service.TfpFindPartnerController#appletAddPartner(FindPartnerAddForm) }
     * 进行初始化处理
     * @param form
     * @return
     */
    private TfpFindPartner handleAddPartner(FindPartnerAddForm form){
        List<AttachmentForm> attachmentList = form.getAttachmentList();
        if(CollectionUtils.isNotEmpty(attachmentList)){
            for (AttachmentForm attachmentForm : attachmentList) {
                attachmentForm.setBusinessType(BusinessTypeEnum.FIND_PARTNER.getValue());
                attachmentForm.setBusinessSubType(BusinessSubTypeEnum.PARTNER_IMAGE.getValue());
            }
        }
        List<TfpAttachment> tfpAttachments = CustomizeBeanCopier.copyPropertiesOfList(attachmentList, TfpAttachment.class);

        TfpFindPartner tfpFindPartner = CustomizeBeanCopier.copyProperties(form, TfpFindPartner.class);
        LoginUser loginUser = getLoginUser();
        tfpFindPartner.setCreateUserId(loginUser.getUserId());
        tfpFindPartner.setCreateBy(loginUser.getUsername());
        tfpFindPartner.setCreateTime(DateUtils.getNowDate());
        tfpFindPartner.setStatus(FindPartnerStatusEnum.PUBLISH_AND_PRE_AUTH.getValue());
        tfpFindPartner.setAttachmentList(tfpAttachments);
        return tfpFindPartner;
    }

    /**
     * 对接口 {@link com.ruoyi.web.controller.service.TfpFindPartnerController#appletAddPartner(FindPartnerAddForm) }
     * 进行初始化处理
     * @param form
     * @return
     */
    private TfpFindPartner handleUpdatePartner(FindPartnerAddForm form){
        List<AttachmentForm> attachmentList = form.getAttachmentList();
        if(CollectionUtils.isNotEmpty(attachmentList)){
            for (AttachmentForm attachmentForm : attachmentList) {
                attachmentForm.setBusinessType(BusinessTypeEnum.FIND_PARTNER.getValue());
                attachmentForm.setBusinessSubType(BusinessSubTypeEnum.PARTNER_IMAGE.getValue());
            }
        }
        List<TfpAttachment> tfpAttachments = CustomizeBeanCopier.copyPropertiesOfList(attachmentList, TfpAttachment.class);

        TfpFindPartner tfpFindPartner = CustomizeBeanCopier.copyProperties(form, TfpFindPartner.class);
        LoginUser loginUser = getLoginUser();
        tfpFindPartner.setUpdateUserId(loginUser.getUserId());
        tfpFindPartner.setUpdateBy(loginUser.getUsername());
        tfpFindPartner.setUpdateTime(DateUtils.getNowDate());
        tfpFindPartner.setStatus(FindPartnerStatusEnum.PUBLISH_AND_PRE_AUTH.getValue());
        tfpFindPartner.setAttachmentList(tfpAttachments);
        return tfpFindPartner;
    }
}
