package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel(value = "RealNameAuthenticationEditDto")
public class RealNameAuthenticationEditDto {
    @ApiModelProperty(value = "手机号码")
    @NotNull(message = "手机号码不能为空")
    private String phonenumber;
}
