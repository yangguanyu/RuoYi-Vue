package com.ruoyi.service.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(value = "MySummoningEnvoyInfoVo", description = "我的召唤使者信息")
public class MySummoningEnvoyInfoVo {
    @ApiModelProperty("等级名称")
    private String levelName;
    @ApiModelProperty("余额")
    private BigDecimal remainingPrice;
    @ApiModelProperty("累计收益(元)")
    private BigDecimal cumulativeIncome;
    @ApiModelProperty("累计提现收益(元)")
    private BigDecimal cumulativeDrawMoneyIncome;
    @ApiModelProperty("提现记录数量")
    private Integer drawMoneyRecordSize;
    @ApiModelProperty("是否存在银行卡 true:存在 false:不存在")
    private boolean userBankCardFlag;
}
