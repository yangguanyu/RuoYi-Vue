package com.ruoyi.common.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class DcDateUtils {
    public static String yyyy = "yyyy";
    public static String yyyyMM = "yyyy-MM";
    public static String yyyyMMdd = "yyyy-MM-dd";
    public static String yyyyMMddHH = "yyyy-MM-dd HH";
    public static String yyyyMMddHHmm = "yyyy-MM-dd HH:mm";
    public static String yyyyMMddHHmmss = "yyyy-MM-dd HH:mm:ss";
    public static String yMd = "yyMMdd";
    public static String yMdHms = "yyyyMMddHHmmss";

    public static Integer ONE_DAY = 24 * 60 * 60 * 1000;

    private static DateFormat yyyyDateFormat = new SimpleDateFormat(DcDateUtils.yyyy);
    private static DateFormat yyyyMMDateFormat = new SimpleDateFormat(DcDateUtils.yyyyMM);
    private static DateFormat yyyyMMddDateFormat = new SimpleDateFormat(DcDateUtils.yyyyMMdd);
    private static DateFormat yyyyMMddHHDateFormat = new SimpleDateFormat(DcDateUtils.yyyyMMddHH);
    private static DateFormat yyyyMMddHHmmDateFormat = new SimpleDateFormat(DcDateUtils.yyyyMMddHHmm);
    private static DateFormat yyyyMMddHHmmssDateFormat = new SimpleDateFormat(DcDateUtils.yyyyMMddHHmmss);
    private static DateFormat yMdDateFormat = new SimpleDateFormat(DcDateUtils.yMd);
    private static DateFormat yMdHmsDateFormat = new SimpleDateFormat(DcDateUtils.yMdHms);

    public static DateFormat getDcDateFormat(String DcDateMatter){
        if (Objects.equals(DcDateMatter, DcDateUtils.yyyy)){
            return DcDateUtils.yyyyDateFormat;
        }

        if (Objects.equals(DcDateMatter, DcDateUtils.yyyyMM)){
            return DcDateUtils.yyyyMMDateFormat;
        }

        if (Objects.equals(DcDateMatter, DcDateUtils.yyyyMMdd)){
            return DcDateUtils.yyyyMMddDateFormat;
        }

        if (Objects.equals(DcDateMatter, DcDateUtils.yyyyMMddHH)){
            return DcDateUtils.yyyyMMddHHDateFormat;
        }

        if (Objects.equals(DcDateMatter, DcDateUtils.yyyyMMddHHmm)){
            return DcDateUtils.yyyyMMddHHmmDateFormat;
        }

        if (Objects.equals(DcDateMatter, DcDateUtils.yyyyMMddHHmmss)){
            return DcDateUtils.yyyyMMddHHmmssDateFormat;
        }

        if (Objects.equals(DcDateMatter, DcDateUtils.yMd)){
            return DcDateUtils.yMdDateFormat;
        }

        if (Objects.equals(DcDateMatter, DcDateUtils.yMdHms)){
            return DcDateUtils.yMdHmsDateFormat;
        }

        throw new RuntimeException("时间类型无法匹配");
    }

    public static Date handelDateToDayStart(Date date){
        if (Objects.isNull(date)){
            return null;
        }

        Date newDate = null;
        try {
            newDate = yyyyMMddHHmmssDateFormat.parse(yyyyMMddDateFormat.format(date) + " 00:00:00");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return newDate;
    }

    public static Date handelDateToDayEnd(Date date){
        if (Objects.isNull(date)){
            return null;
        }

        Date newDate = null;
        try {
            newDate = yyyyMMddHHmmssDateFormat.parse(yyyyMMddDateFormat.format(date) + " 23:59:59");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return newDate;
    }
}
