package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.TfpComate;
import com.ruoyi.service.dto.TfpComateDto;
import com.ruoyi.service.vo.TfpComateVo;
import org.apache.ibatis.annotations.Param;

/**
 * 同行人-独立出行Mapper接口
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public interface TfpComateMapper 
{
    /**
     * 查询同行人-独立出行
     * 
     * @param id 同行人-独立出行主键
     * @return 同行人-独立出行
     */
    public TfpComate selectTfpComateById(Long id);

    /**
     * 查询同行人-独立出行列表
     * 
     * @param tfpComate 同行人-独立出行
     * @return 同行人-独立出行集合
     */
    public List<TfpComate> selectTfpComateList(TfpComate tfpComate);

    /**
     * 新增同行人-独立出行
     * 
     * @param tfpComate 同行人-独立出行
     * @return 结果
     */
    public int insertTfpComate(TfpComate tfpComate);

    /**
     * 修改同行人-独立出行
     * 
     * @param tfpComate 同行人-独立出行
     * @return 结果
     */
    public int updateTfpComate(TfpComate tfpComate);

    /**
     * 删除同行人-独立出行
     * 
     * @param id 同行人-独立出行主键
     * @return 结果
     */
    public int deleteTfpComateById(Long id);

    /**
     * 批量删除同行人-独立出行
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpComateByIds(Long[] ids);

    List<Long> selectMainUserIdList();

    List<TfpComateVo> selectTfpComateVoList(TfpComateDto tfpComateDto);

    List<TfpComate> selectTfpComateByIdList(@Param("idList") List<Long> idList);
}
