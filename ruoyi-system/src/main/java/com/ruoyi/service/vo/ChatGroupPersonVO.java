package com.ruoyi.service.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChatGroupPersonVO {

    private Long userId;

    private String userName;

    private Integer userType;

    private String userTypeName;

}
