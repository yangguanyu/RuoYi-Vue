package com.ruoyi.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "OrderListDto")
public class OrderListDto {
    @ApiModelProperty("产品编号")
    private String productNo;
    @ApiModelProperty("产品类型id 1-全国散 2-一家一团 3-自营")
    private Long categoryId;
    @ApiModelProperty("供应商渠道")
    private Long supplierId;

    @ApiModelProperty("订单编号")
    private String orderNo;
    @ApiModelProperty("订单状态 1-已完成 2-待确认(已支付) 3-(已确认)待出行 4-申请退款-审核中 5-申请退款-退款成功 6-过期 7-取消")
    private Integer status;
    @ApiModelProperty("退款类型 0-退款待审核 1-整单退 2-部分退 3-否")
    private Integer refundType;

    @JsonIgnore
    private List<Long> productIdList;
    @JsonIgnore
    private List<Long> orderIdList;
    @JsonIgnore
    private boolean queryProductFlag;
}
