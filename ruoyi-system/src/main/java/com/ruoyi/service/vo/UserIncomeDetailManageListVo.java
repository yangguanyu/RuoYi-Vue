package com.ruoyi.service.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(value = "UserIncomeDetailManageListVo")
public class UserIncomeDetailManageListVo {
    @ApiModelProperty("id")
    private Long id;
    @ApiModelProperty("结算单编号")
    private String settlementNumber;
    @ApiModelProperty("产品编号")
    private String productNo;
    @ApiModelProperty("产品类型名称")
    private String categoryName;
    @ApiModelProperty("用户id")
    private Long userId;
    @ApiModelProperty("分享者")
    private String realName;
    @ApiModelProperty("本单收益（元）")
    private BigDecimal price;
    @ApiModelProperty("关联订单编号")
    private String orderNo;

    @ApiModelProperty("是否已过售后期 0-否 1-是")
    private Integer afterSalesTimeFlag;

    @ApiModelProperty("结算状态 0-待结算 1-已结算")
    private Integer settlementReqStatus;

    @JsonIgnore
    private Long productId;
    @JsonIgnore
    private Long orderId;
}
