package com.ruoyi.common.enuma;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum IncomeLevelCodeEnum implements BaseStrEnum {
    ONE("one", "普通等级"),

    ;

    private String value;
    private String desc;
}
