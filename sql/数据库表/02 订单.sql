-- 订单主表
CREATE TABLE `tfp_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `order_no` varchar(50) DEFAULT NULL COMMENT '订单编号',
  `supply_id` bigint(20) DEFAULT NULL COMMENT '供应商id',
  `product_id` bigint(20) DEFAULT NULL COMMENT '商品ID',
  `sale_price` decimal(15,2) DEFAULT NULL COMMENT '售卖单价',
  `quantities` int(11) DEFAULT NULL COMMENT '商品数量',
  `final_amount` decimal(15,2) DEFAULT NULL COMMENT '订单金额',
  `go_date` datetime DEFAULT NULL COMMENT '出发时间',
  `go_place_name` varchar(200) DEFAULT NULL COMMENT '出发地名称',
  `buyer_remark` varchar(100) DEFAULT NULL COMMENT '订单买家备注(预留)',
  `seller_remark` varchar(100) DEFAULT NULL COMMENT '订单卖家备注（预留）',
  `status` int(4) DEFAULT NULL COMMENT '订单状态 0-待支付 1-已完成 2-待确认(已支付) 3-待出行 4-申请退款-审核中 5-申请退款-退款成功 6-过期 7-取消',
  `refund_temp_status` int(4) COMMENT '退款时临时状态(退款失败后，status重新变为该状态)',
  `refund_fail_reason` varchar(100) COMMENT '退款失败原因',
  `snapshot_id` bigint(20) COMMENT '商品快照id',
  `after_service_able` int(4) NOT NULL DEFAULT '1' COMMENT '是否可售后（0：不可以，1：可以）',
  `prepay_id` varchar(100) COMMENT '预支付交易会话标识',
  `expire_date` datetime COMMENT '预支付交易会话过期时间',
  `travel_type` int(1) COMMENT '出行方式 0-组队出行 1-独立出行',
  `chat_group_id` bigint(20) COMMENT '群组id',
  `pay_time` datetime DEFAULT NULL COMMENT '支付时间',
  `travel_complete_time` datetime DEFAULT NULL COMMENT '出行完成时间',
  `apply_refund_flag` int(1) COMMENT '是否可以申请退款 0-否 1-是',
  `refund_end_time` datetime DEFAULT NULL COMMENT '退款截止时间',
  `share_user_id` bigint(20) DEFAULT NULL COMMENT '分享用户id',
  `low_price_air_ticket_flag` int(1) DEFAULT NULL COMMENT '是否选择低价机票 0-否 1-是',
  `go_air_ticket_price` decimal(15,2) DEFAULT NULL COMMENT '航班去程价格',
  `back_air_ticket_price` decimal(15,2) DEFAULT NULL COMMENT '航班返程价格'
  PRIMARY KEY (`id`)
) COMMENT='订单表';

-- 独立出行出行人表（同行人表）
CREATE TABLE `tfp_comate` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `main_user_id` bigint(20) DEFAULT NULL COMMENT '主用户id',
  `mate_name` varchar(50) DEFAULT NULL COMMENT '同行人姓名',
  `mate_card_type` varchar(50) DEFAULT NULL COMMENT '证件类型 1.身份证 2.台胞证 3.港澳通行证 4.护照 5.士兵证',
  `mate_card_num` varchar(50) DEFAULT NULL COMMENT '证件号码',
  `mate_phone` varchar(50) DEFAULT NULL COMMENT '手机号码',
  `vice_user_id` bigint(20) DEFAULT NULL COMMENT '此人的user_id(备用)',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) COMMENT='同行人表-独立出行';

-- 订单和同行人关联表
CREATE TABLE `tfp_comate_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `order_id` bigint(20) NOT NULL COMMENT '订单ID',
  `order_no` varchar(50) NOT NULL COMMENT '订单编码',
  `mate_id` bigint(20) DEFAULT NULL COMMENT '同行人表主键',
  `mate_name` varchar(50) DEFAULT NULL COMMENT '同行人姓名',
  `mate_card_type` varchar(50) DEFAULT NULL COMMENT '证件类型 1.身份证 2.台胞证 3.港澳通行证 4.护照 5.士兵证',
  `mate_card_num` varchar(50) DEFAULT NULL COMMENT '证件号码',
  `mate_phone` varchar(50) DEFAULT NULL COMMENT '手机号码',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) COMMENT='同行人和订单关联表';

-- 交易快照表
CREATE TABLE `tfp_product_snapshot` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `supply_id` bigint(20) DEFAULT NULL COMMENT '供应商ID',
  `product_id` bigint(20) NOT NULL COMMENT '商品ID',
  `snapshot_value` longtext COMMENT '快照信息',
  `sale_price` decimal(15,2) DEFAULT NULL COMMENT '当时售卖价格',
  PRIMARY KEY (`id`)
) COMMENT='商品快照表';

-- 退款申请
CREATE TABLE `tfp_refund` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `product_id` bigint(20) NOT NULL COMMENT '商品ID',
  `order_id` bigint(20) NOT NULL COMMENT '订单ID',
  `order_no` varchar(50) NOT NULL COMMENT '订单编号',
  `order_price` decimal(15,2) NOT NULL COMMENT '退款金额',
  `refund_user_id` bigint(20) NOT NULL COMMENT '接收退款用户',
  `refund_type` int(4) NOT NULL COMMENT '1：全额退款   2：部分退款',
  `refund_amount` decimal(15,2) NOT NULL COMMENT '退款金额',
  `refund_time` datetime NOT NULL COMMENT '退款时间',
  `refund_status` int(4) NOT NULL DEFAULT '1' COMMENT '退款状态（1：退款审核中，2：审核成功-退款中，3：审核失败-退款失败，4：退款失败，5：退款成功）',
  `reason` varchar(255) COMMENT '申请退款原因',
  `illustrate` varchar(200) COMMENT '申请退款说明',
  `reject_reason` varchar(255) COMMENT '申请退款驳回的原因',
  `fail_reason` varchar(255) COMMENT '退款失败原因',
  `act_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '对账状态（0-未对账 1-成功 2-交易金额异常 3-交易状态异常)',
  `act_time` datetime DEFAULT NULL COMMENT '对账时间',
  `account_info` text COMMENT '账户信息（预留）',
  `refund_info` text COMMENT '退款信息（预留）',
  `serial_no` varchar(100) NOT NULL DEFAULT '' COMMENT '退款单流水号',
  PRIMARY KEY (`id`)
) COMMENT='退款表';