package com.ruoyi.web.controller.service;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.service.vo.MyIdentityListVo;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.SysIdentity;
import com.ruoyi.service.service.ISysIdentityService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 用户身份Controller
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@RestController
@RequestMapping("/service/identity")
public class SysIdentityController extends BaseController
{
    @Autowired
    private ISysIdentityService sysIdentityService;

    /**
     * 查询用户身份列表
     */
    @PreAuthorize("@ss.hasPermi('service:identity:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysIdentity sysIdentity)
    {
        startPage();
        List<SysIdentity> list = sysIdentityService.selectSysIdentityList(sysIdentity);
        return getDataTable(list);
    }

    /**
     * 导出用户身份列表
     */
    @PreAuthorize("@ss.hasPermi('service:identity:export')")
    @Log(title = "用户身份", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysIdentity sysIdentity)
    {
        List<SysIdentity> list = sysIdentityService.selectSysIdentityList(sysIdentity);
        ExcelUtil<SysIdentity> util = new ExcelUtil<SysIdentity>(SysIdentity.class);
        util.exportExcel(response, list, "用户身份数据");
    }

    /**
     * 获取用户身份详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:identity:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(sysIdentityService.selectSysIdentityById(id));
    }

    /**
     * 新增用户身份
     */
    @PreAuthorize("@ss.hasPermi('service:identity:add')")
    @Log(title = "用户身份", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysIdentity sysIdentity)
    {
        return toAjax(sysIdentityService.insertSysIdentity(sysIdentity));
    }

    /**
     * 修改用户身份
     */
    @PreAuthorize("@ss.hasPermi('service:identity:edit')")
    @Log(title = "用户身份", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysIdentity sysIdentity)
    {
        return toAjax(sysIdentityService.updateSysIdentity(sysIdentity));
    }

    /**
     * 删除用户身份
     */
    @PreAuthorize("@ss.hasPermi('service:identity:remove')")
    @Log(title = "用户身份", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(sysIdentityService.deleteSysIdentityByIds(ids));
    }

    @ApiOperation("小程序-我的身份列表")
    @PostMapping("/myIdentityList")
    @ResponseBody
    public R<List<MyIdentityListVo>> myIdentityList(){
        return R.ok(sysIdentityService.myIdentityList());
    }

    @ApiOperation("小程序-切换身份")
    @PostMapping("/switchIdentity")
    @ResponseBody
    public R switchIdentity(@RequestParam("id") Long id){
        return R.ok(sysIdentityService.switchIdentity(id));
    }
}
