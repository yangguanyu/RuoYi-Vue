package com.ruoyi.web.controller.service;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.SysUserIcon;
import com.ruoyi.service.service.ISysUserIconService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 用户图片Controller
 * 
 * @author ruoyi
 * @date 2023-11-01
 */
@RestController
@RequestMapping("/service/userIcon")
public class SysUserIconController extends BaseController
{
    @Autowired
    private ISysUserIconService sysUserIconService;

    /**
     * 查询用户图片列表
     */
    @PreAuthorize("@ss.hasPermi('service:userIcon:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysUserIcon sysUserIcon)
    {
        startPage();
        List<SysUserIcon> list = sysUserIconService.selectSysUserIconList(sysUserIcon);
        return getDataTable(list);
    }

    /**
     * 导出用户图片列表
     */
    @PreAuthorize("@ss.hasPermi('service:userIcon:export')")
    @Log(title = "用户图片", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysUserIcon sysUserIcon)
    {
        List<SysUserIcon> list = sysUserIconService.selectSysUserIconList(sysUserIcon);
        ExcelUtil<SysUserIcon> util = new ExcelUtil<SysUserIcon>(SysUserIcon.class);
        util.exportExcel(response, list, "用户图片数据");
    }

    /**
     * 获取用户图片详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:userIcon:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(sysUserIconService.selectSysUserIconById(id));
    }

    /**
     * 新增用户图片
     */
    @PreAuthorize("@ss.hasPermi('service:userIcon:add')")
    @Log(title = "用户图片", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysUserIcon sysUserIcon)
    {
        return toAjax(sysUserIconService.insertSysUserIcon(sysUserIcon));
    }

    /**
     * 修改用户图片
     */
    @PreAuthorize("@ss.hasPermi('service:userIcon:edit')")
    @Log(title = "用户图片", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysUserIcon sysUserIcon)
    {
        return toAjax(sysUserIconService.updateSysUserIcon(sysUserIcon));
    }

    /**
     * 删除用户图片
     */
    @PreAuthorize("@ss.hasPermi('service:userIcon:remove')")
    @Log(title = "用户图片", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(sysUserIconService.deleteSysUserIconByIds(ids));
    }
}
