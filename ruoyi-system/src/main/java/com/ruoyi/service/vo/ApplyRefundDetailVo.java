package com.ruoyi.service.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
@ApiModel(value = "ApplyRefundDetailVo", description = "申请退款详情vo")
public class ApplyRefundDetailVo {
    @ApiModelProperty("订单id")
    private Long orderId;
    @ApiModelProperty("商品名称")
    private String productName;

    @ApiModelProperty("服务类型id")
    private Long categoryId;
    @ApiModelProperty("服务类型名称")
    private String categoryName;
    @ApiModelProperty("线路亮点集合")
    private List<String> routeHighlightList;

    @ApiModelProperty("订单编号")
    private String orderNo;
    @ApiModelProperty("支付金额")
    private BigDecimal finalAmount;

    @ApiModelProperty("出发时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date goDate;

    @ApiModelProperty("聊天群组名称")
    private String ChatGroupName;

    @ApiModelProperty("商品图片url集合")
    private List<String> productImageUrlList;
    @ApiModelProperty("出行方式 0-组队出行 1-独立出行")
    private Integer travelType;
}
