package com.ruoyi.service.service.impl;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.common.enuma.flight.FlightSuccessEnum;
import com.ruoyi.common.exception.BusinessException;
import com.ruoyi.common.utils.DcHttpClientUtils;
import com.ruoyi.common.utils.bean.BeanUtils;
import com.ruoyi.common.utils.flight.FlightUtils;
import com.ruoyi.common.utils.uuid.UUID;
import com.ruoyi.service.domain.flight.base.FlightBaseParam;
import com.ruoyi.service.domain.flight.base.FlightBaseResp;
import com.ruoyi.service.domain.flight.param.CheckSeatAndPriceParam;
import com.ruoyi.service.domain.flight.param.CreateOrderByPassengerParam;
import com.ruoyi.service.domain.flight.param.FlightSearchParam;
import com.ruoyi.service.domain.flight.res.CheckSeatAndPriceResp;
import com.ruoyi.service.domain.flight.res.CreateOrderByPassengerResp;
import com.ruoyi.service.domain.flight.res.FlightSearchResp;
import com.ruoyi.service.dto.*;
import com.ruoyi.service.service.IFlightService;
import com.ruoyi.service.service.ITfpFlightService;
import com.ruoyi.service.vo.TfpFlightVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
@Transactional
public class FlightServiceImpl implements IFlightService {

    @Value("${flight.userName}")
    private String userName;
    @Value("${flight.userSecurityCode}")
    private String userSecurityCode;

    @Value("${flight.url.searchFlight}")
    private String searchFlightUrl;
    @Value("${flight.url.checkSeatAndPrice}")
    private String checkSeatAndPriceUrl;
    @Value("${flight.url.createOrderByPassenger}")
    private String createOrderByPassengerUrl;

    @Autowired
    private ITfpFlightService tfpFlightService;

    /**
     * 航班查询接口
     * @return
     */
    @Override
    public List<TfpFlightVo> searchFlight(FlightSearchParamDto flightSearchParamDto) {
        String url = searchFlightUrl.replace("{flight.userName}", userName);

        FlightSearchParam flightSearchParam = new FlightSearchParam();
        BeanUtils.copyProperties(flightSearchParamDto, flightSearchParam);

        List<FlightSearchParam.Segment> segments = new ArrayList<>();
        List<FlightSearchParamSegmentDto> segmentList = flightSearchParamDto.getSegments();
        for (FlightSearchParamSegmentDto segmentDto : segmentList) {
            FlightSearchParam.Segment segment = flightSearchParam.new Segment();
            BeanUtils.copyProperties(segmentDto, segment);
            segments.add(segment);
        }
        flightSearchParam.setSegments(segments);

        FlightBaseParam flightBaseParam = this.handleFlightBaseParam();
        flightBaseParam.setRqData(flightSearchParam);

        FlightBaseResp flightBaseResp = this.sendInfo(url, flightBaseParam);

        List<FlightSearchResp> flightSearchRespList = JSON.parseArray(flightBaseResp.getRsData().toString(), FlightSearchResp.class);

        // 保存查询到的航班数据
        return tfpFlightService.saveFlightInfo(flightSearchRespList);
    }

    /**
     * 验证舱位价格接口
     * @param checkSeatAndPriceParamDto
     * @return
     */
    @Override
    public CheckSeatAndPriceResp checkSeatAndPrice(CheckSeatAndPriceParamDto checkSeatAndPriceParamDto) {
        String url = checkSeatAndPriceUrl.replace("{flight.userName}", userName);

        CheckSeatAndPriceParam checkSeatAndPriceParam = new CheckSeatAndPriceParam();
        BeanUtils.copyProperties(checkSeatAndPriceParamDto, checkSeatAndPriceParam);

        List<CheckSeatAndPriceParam.Segment> segmentList = new ArrayList<>();
        List<CheckSeatAndPriceParamSegmentDto> segments = checkSeatAndPriceParamDto.getSegmentList();
        for (CheckSeatAndPriceParamSegmentDto segmentDto : segments) {
            CheckSeatAndPriceParam.Segment segment = checkSeatAndPriceParam.new Segment();
            BeanUtils.copyProperties(segmentDto, segment);
            segmentList.add(segment);
        }
        checkSeatAndPriceParam.setSegmentList(segmentList);

        FlightBaseParam flightBaseParam = this.handleFlightBaseParam();
        flightBaseParam.setRqData(checkSeatAndPriceParam);

        FlightBaseResp flightBaseResp = this.sendInfo(url, flightBaseParam);
        CheckSeatAndPriceResp checkSeatAndPriceResp = JSON.parseObject(flightBaseResp.getRsData().toString(), CheckSeatAndPriceResp.class);
        return checkSeatAndPriceResp;
    }

    /**
     * 机票预订接口
     * @param createOrderByPassengerDto
     * @return
     */
    @Override
    public CreateOrderByPassengerResp createOrderByPassenger(CreateOrderByPassengerDto createOrderByPassengerDto) {
        String url = createOrderByPassengerUrl.replace("{flight.userName}", userName);

        CreateOrderByPassengerParam createOrderByPassengerParam = new CreateOrderByPassengerParam();
        BeanUtils.copyProperties(createOrderByPassengerDto, createOrderByPassengerParam);

        List<CreateOrderByPassengerSegmentDto> segments = createOrderByPassengerDto.getSegments();
        List<CreateOrderByPassengerParam.Segment> segmentList = new ArrayList<>();
        for (CreateOrderByPassengerSegmentDto segmentDto : segments) {
            CreateOrderByPassengerParam.Segment segment = createOrderByPassengerParam.new Segment();
            BeanUtils.copyProperties(segmentDto, segment);
            segmentList.add(segment);
        }
        createOrderByPassengerParam.setSegments(segmentList);


        List<CreateOrderByPassengerPassengerDto> passengers = createOrderByPassengerDto.getPassengers();
        List<CreateOrderByPassengerParam.Passenger> passengerList = new ArrayList<>();
        for (CreateOrderByPassengerPassengerDto passengerDto : passengers) {
            CreateOrderByPassengerParam.Passenger passenger = createOrderByPassengerParam.new Passenger();
            BeanUtils.copyProperties(passengerDto, passenger);
            passengerList.add(passenger);
        }
        createOrderByPassengerParam.setPassengers(passengerList);

        List<CreateOrderByPassengerPriceDataDto> priceDatas = createOrderByPassengerDto.getPriceDatas();
        List<CreateOrderByPassengerParam.PriceData> priceDataList = new ArrayList<>();
        for (CreateOrderByPassengerPriceDataDto priceDataDto : priceDatas) {
            CreateOrderByPassengerParam.PriceData priceData = createOrderByPassengerParam.new PriceData();
            BeanUtils.copyProperties(priceDataDto, priceData);
            priceDataList.add(priceData);
        }
        createOrderByPassengerParam.setPriceDatas(priceDataList);

        FlightBaseParam flightBaseParam = this.handleFlightBaseParam();
        flightBaseParam.setRqData(createOrderByPassengerParam);

        FlightBaseResp flightBaseResp = this.sendInfo(url, flightBaseParam);
        CreateOrderByPassengerResp createOrderByPassengerResp = JSON.parseObject(flightBaseResp.getRsData().toString(), CreateOrderByPassengerResp.class);
        return createOrderByPassengerResp;
    }

    /**
     * 发送信息
     * @param url
     * @param flightBaseParam
     * @return
     */
    private FlightBaseResp sendInfo(String url, FlightBaseParam flightBaseParam){
        long start = System.currentTimeMillis();
        String requestJson = JSONObject.toJSONString(flightBaseParam);
        String sign = FlightUtils.generateSignature(requestJson, userSecurityCode);
        String result = DcHttpClientUtils.httpPost(url, requestJson, userName, sign);
        long end = System.currentTimeMillis();
        System.out.println("查询时间:" + ((end - start) / 1000) + "s");
//        System.out.println("请求结果:" + result);
        FlightBaseResp flightBaseResp = JSONObject.parseObject(result, FlightBaseResp.class);
        String rsCode = flightBaseResp.getRsCode();
        if (!Objects.equals(rsCode, FlightSuccessEnum.SUCCESS.getValue())){
            throw new BusinessException(flightBaseResp.getRsCode() + ": " + flightBaseResp.getRsMessage());
        }
        return flightBaseResp;
    }

    /**
     * 处理发送的基本信息
     * @return
     */
    private FlightBaseParam handleFlightBaseParam(){
        String qqIdentification = UUID.randomUUID().toString().replace("-", "");

        FlightBaseParam flightBaseParam = new FlightBaseParam();
        flightBaseParam.setRsIsGzip(true);
        flightBaseParam.setRqIdentification(qqIdentification);
        flightBaseParam.setTimeStamp(new Date().getTime());
        flightBaseParam.setThirdUsername(userName);
        return flightBaseParam;
    }
}
