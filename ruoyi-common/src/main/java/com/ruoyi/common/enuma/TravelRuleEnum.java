package com.ruoyi.common.enuma;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TravelRuleEnum implements BaseEnum{
    TRAVEL(0, "旅行方式"),
    ;

    private Integer value;
    private String desc;
}
