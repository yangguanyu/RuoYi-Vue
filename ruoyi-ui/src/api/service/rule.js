import request from '@/utils/request'

// 查询某条规则详情
export function detailTravelRule(id) {
  return request({
    url: `/service/travelRule/detailTravelRule/${id}`,
    method: 'get',
  })
}

// 查询规则列表
export function listTravelRule(query) {
  return request({
    url: '/service/travelRule/listTravelRule',
    method: 'get',
    params: query
  })
}

// 保存规则
export function saveTravelRule(query) {
  return request({
    url: '/service/travelRule/saveTravelRule',
    method: 'post',
    data: query
  })
}
