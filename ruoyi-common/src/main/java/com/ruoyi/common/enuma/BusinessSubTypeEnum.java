package com.ruoyi.common.enuma;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum BusinessSubTypeEnum implements BaseStrEnum {
    SYSTEM_DEFAULT_IMAGE("system_default_image", "默认系统图片"),

    CAROUSEL_CHART_HOME_PAGE("carousel_chart_home_page", "首页轮播图"),
    USER_HEAD_PORTRAIT("user_head_portrait", "用户头像"),
    USER_IMAGE("user_image", "用户图片"),

    PARTNER_IMAGE("partner_image","找搭子图片"),

    PRODUCT_IMAGE("product_image","商品轮播图片"),
    ROUTE_HIGHLIGHT_IMAGE("route_highlight_image","商品亮点图片"),
    PRODUCT_COVER_IMAGE("product_cover_image","商品封面图片"),

    DRAW_MONEY_SETTLEMENT_VOUCHER("draw_money_settlement_voucher","提款-结算凭证"),

    REFUND_VOUCHER("draw_money_settlement_voucher","提款-结算凭证"),

    AREA_IMAGE("area_image","区域图片"),
    ;

    private String value;
    private String desc;
}
