package com.ruoyi.service.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("DetailFindPartnerSignUpVo")
public class DetailFindPartnerSignUpVo {
    @ApiModelProperty("报名id")
    private Long id;
    @ApiModelProperty("用户昵称")
    private String nickName;
    @ApiModelProperty("头像图片")
    private String headImage;
    @ApiModelProperty("联系电话")
    private String phoneNumber;
    @ApiModelProperty("性别 0-男生 1-女生")
    private String sex;
    @ApiModelProperty("自我介绍")
    private String selfIntroductio;
    @ApiModelProperty("审核状态 0-待确认 1-已确认")
    private Integer status;
}
