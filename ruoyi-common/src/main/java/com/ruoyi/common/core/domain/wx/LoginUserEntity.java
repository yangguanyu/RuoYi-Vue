package com.ruoyi.common.core.domain.wx;

import com.ruoyi.common.enums.LoginModeEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 登录实体
 *
 * @author
 * @date
 */
@Data
public class LoginUserEntity {
    /**
     * 用户名
     */
    private String userName;

    private String loginName;
    /**
     * 密码
     */
    private String password;
    /**
     * 手机号
     */
    private String phoneNumber;
    /**
     * 验证码
     */
    private String codeKey;
    /**
     * 输入的验证码
     */
    private String verifyCode;

    /**
     * CA登录签名字符串
     */
    private String signToken;

    /**
     * 登录模式
     */
    private LoginModeEnum loginMode;

    /**
     * 登录签名字符串
     */
    private String token;

    /**
     * 滑块验证码流水号
     */
    private String challenge;

    /**
     * 滑块验证码checkcode
     */
    private String seccode;

    /**
     * API版本
     */
    private int version;

    @ApiModelProperty("邀请方式类型: 0:二维码邀请 1:链接邀请")
    private Integer inviteType;

    @ApiModelProperty("邀请方式子类型: 0:个人 1:公司 2:首页")
    private Integer inviteSubType;

    @ApiModelProperty("邀请人身份id")
    private Long inviteId;

    @ApiModelProperty("选择供应商信息，true:返回供应商信息和身份id，fal")
    private Boolean chooseSupplierFlag = false;
}
