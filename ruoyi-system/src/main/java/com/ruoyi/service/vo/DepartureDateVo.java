package com.ruoyi.service.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Data
@ApiModel(value = "DepartureDateVo")
public class DepartureDateVo {
    @ApiModelProperty("团期开始")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date departureDateStart;

    @ApiModelProperty("团期截止")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date departureDateEnd;

    @ApiModelProperty("成人零售价")
    private BigDecimal adultRetailPrice;
}
