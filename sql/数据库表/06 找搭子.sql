-- 找搭子表
CREATE TABLE `tfp_find_partner` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(50) COMMENT '标题',
  `activity_desc` varchar(550) COMMENT '描述',
  `type_code` varchar(50) COMMENT '类型code',
  `type_name` varchar(50) COMMENT '类型名称',
  `start_time` datetime COMMENT '开始时间',
  `sign_up_end_time` datetime COMMENT '报名截止时间',
  `activity_person_number` int COMMENT '活动人数',
  `activity_price` decimal(20,2) COMMENT '活动费用',
  `activity_place` varchar(50) COMMENT '活动地点',
  `activity_place_detail` varchar(200) COMMENT '活动地点详情',
  `status` int(2) COMMENT '状态 0-未发布 1-已发布待审核 2-审核通过 3-审核未通过 4-因疑似非法而下架',
  PRIMARY KEY (`id`)
) COMMENT='找搭子表';

-- 找搭子报名表
CREATE TABLE `tfp_find_partner_sign_up` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `find_partner_id` bigint(20) COMMENT '找搭子id',
  `sign_up_user_id` bigint(20) COMMENT '报名用户id',
  `phone_number` varchar(20) COMMENT '联系电话',
  `sex` int(1) COMMENT '性别 0-男生 1-女生',
  `self_introductio` varchar(500) COMMENT '自我介绍',
  `audit_status` tinyint(1) COMMENT '审核状态 0-待审核 1-审核通过 2-审核未通过',
  PRIMARY KEY (`id`)
) COMMENT='找搭子报名表';

-- 找搭子聊天群组表
CREATE TABLE `tfp_find_partner_chat_group` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(50) comment '聊天群组名称',
  `find_partner_id` bigint(20) COMMENT '找搭子id',
  `limit_person_number` int comment '限制人数',
  PRIMARY KEY (`id`)
) COMMENT='找搭子聊天群组表';

-- 找搭子聊天群组成员表
CREATE TABLE `tfp_find_partner_chat_group_person` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chat_group_id` bigint(20) comment '聊天群组id',
  `user_id` bigint(20) comment '用户id',
  `user_type` int(4) comment '用户类型 0-发起者 1-已报名 2-未报名',
  PRIMARY KEY (`id`)
) COMMENT='找搭子聊天群组成员表';

-- 找搭子聊天群组成员聊天信息表
CREATE TABLE `tfp_find_partner_chat_group_person_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chat_group_id` bigint(20) comment '聊天群组id',
  `chat_group_person_id` bigint(20) comment '聊天群组成员id',
  `chat_group_person_user_id` bigint(20) comment '聊天群组成员用户id',
  `user_id` bigint(20) comment '用户id',
  `chat_info` varchar(300) COMMENT '聊天信息',
  PRIMARY KEY (`id`)
) COMMENT='找搭子聊天群组成员聊天信息表';