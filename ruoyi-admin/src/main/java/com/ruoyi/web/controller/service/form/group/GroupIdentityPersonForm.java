package com.ruoyi.web.controller.service.form.group;

import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.validation.constraints.NotNull;

/**
 * 聊天群组成员对象 tfp_chat_group_person
 *
 * @author ruoyi
 * @date 2023-10-31
 */
@ApiModel(value = "GroupIdentityPersonForm",description = "查询在群组里什么身份")
@Data
public class GroupIdentityPersonForm extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 聊天群组id */
    @ApiModelProperty("群组id")
    @NotNull(message = "群组id不能为空")
    private Long chatGroupId;

}
