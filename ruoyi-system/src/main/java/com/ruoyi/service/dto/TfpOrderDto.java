package com.ruoyi.service.dto;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * 订单对象 tfp_order
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@Data
public class TfpOrderDto extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 订单编号 */
    private String orderNo;

    /** 供应商id */
    private Long supplyId;

    /** 商品ID */
    private Long productId;

    private String productName;
    private boolean productNameFlag;
    private List<Long> productIdList;

    /** 售卖单价 */
    private BigDecimal salePrice;

    /** 商品数量 */
    private Long quantities;

    /** 订单金额 */
    private BigDecimal finalAmount;

    /** 订单买家备注(预留) */
    private String buyerRemark;

    /** 订单卖家备注（预留） */
    private String sellerRemark;

    /** 订单状态 */
    private Integer status;

    /** 商品快照id */
    private String snapshotId;

    /** 是否可售后（0：不可以，1：可以） */
    private Integer afterServiceAble;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    private Long createUserId;

    /** 更新者id */
    private Long updateUserId;

}
