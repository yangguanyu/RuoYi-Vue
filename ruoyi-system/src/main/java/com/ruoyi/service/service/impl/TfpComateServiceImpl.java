package com.ruoyi.service.service.impl;

import java.util.*;
import java.util.stream.Collectors;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enuma.DeleteEnum;
import com.ruoyi.common.exception.BusinessException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.DcListUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.service.domain.TfpComateOrder;
import com.ruoyi.service.dto.BatchAddComateDto;
import com.ruoyi.service.dto.TfpComateDto;
import com.ruoyi.service.service.ITfpComateOrderService;
import com.ruoyi.service.vo.ComateVo;
import com.ruoyi.service.vo.TfpComateVo;
import com.ruoyi.service.vo.UserVo;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpComateMapper;
import com.ruoyi.service.domain.TfpComate;
import com.ruoyi.service.service.ITfpComateService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 同行人-独立出行Service业务层处理
 *
 * @author ruoyi
 * @date 2023-10-31
 */
@Service
@Transactional
public class TfpComateServiceImpl implements ITfpComateService
{
    @Autowired
    private TfpComateMapper tfpComateMapper;
    @Autowired
    private ISysUserService sysUserService;
    @Autowired
    private ITfpComateOrderService tfpComateOrderService;

    /**
     * 查询同行人-独立出行
     *
     * @param id 同行人-独立出行主键
     * @return 同行人-独立出行
     */
    @Override
    public TfpComate selectTfpComateById(Long id)
    {
        return tfpComateMapper.selectTfpComateById(id);
    }

    /**
     * 查询同行人-独立出行列表
     *
     * @param tfpComate 同行人-独立出行
     * @return 同行人-独立出行
     */
    @Override
    public List<TfpComate> selectTfpComateList(TfpComate tfpComate)
    {
        return tfpComateMapper.selectTfpComateList(tfpComate);
    }

    @Override
    public List<TfpComateVo> selectTfpComateVoList(TfpComateDto tfpComateDto) {
        List<TfpComateVo> tfpComateVoList = tfpComateMapper.selectTfpComateVoList(tfpComateDto);
        if (tfpComateVoList != null && tfpComateVoList.size() > 0){
            List<Long> mainUserIdList = new ArrayList<>();
            List<Long> idList = new ArrayList<>();
            for (TfpComateVo tfpComateVo : tfpComateVoList) {
                Long mainUserId = tfpComateVo.getMainUserId();
                if (!mainUserIdList.contains(mainUserId)){
                    mainUserIdList.add(mainUserId);
                }

                Long id = tfpComateVo.getId();
                if (!idList.contains(id)){
                    idList.add(id);
                }
            }

            Map<Long, String> mainUserMap = sysUserService.getNickNameMap(mainUserIdList);

            Map<Long, String> orderNoMap = new HashMap<>();
            List<TfpComateOrder> tfpComateOrderList = tfpComateOrderService.selectTfpComateOrderByMateIdList(idList);
            if (tfpComateOrderList != null && tfpComateOrderList.size() > 0){
                orderNoMap = tfpComateOrderList.stream().collect(Collectors.toMap(TfpComateOrder::getMateId, TfpComateOrder::getOrderNo));
            }

            for (TfpComateVo tfpComateVo : tfpComateVoList) {
                tfpComateVo.setMainNickName(mainUserMap.get(tfpComateVo.getMainUserId()));
                tfpComateVo.setOrderNo(orderNoMap.get(tfpComateVo.getId()));
            }
        }

        return tfpComateVoList;
    }

    /**
     * 新增同行人-独立出行
     *
     * @param tfpComate 同行人-独立出行
     * @return 结果
     */
    @Override
    public int insertTfpComate(TfpComate tfpComate)
    {
        tfpComate.setCreateTime(DateUtils.getNowDate());
        return tfpComateMapper.insertTfpComate(tfpComate);
    }

    /**
     * 修改同行人-独立出行
     *
     * @param tfpComate 同行人-独立出行
     * @return 结果
     */
    @Override
    public int updateTfpComate(TfpComate tfpComate)
    {
        tfpComate.setUpdateTime(DateUtils.getNowDate());
        return tfpComateMapper.updateTfpComate(tfpComate);
    }

    /**
     * 批量删除同行人-独立出行
     *
     * @param ids 需要删除的同行人-独立出行主键
     * @return 结果
     */
    @Override
    public int deleteTfpComateByIds(Long[] ids)
    {
        return tfpComateMapper.deleteTfpComateByIds(ids);
    }

    /**
     * 删除同行人-独立出行信息
     *
     * @param id 同行人-独立出行主键
     * @return 结果
     */
    @Override
    public int deleteTfpComateById(Long id)
    {
        return tfpComateMapper.deleteTfpComateById(id);
    }

    @Override
    public List<UserVo> mainUserSelect() {
        List<Long> userIdList = tfpComateMapper.selectMainUserIdList();
        return sysUserService.handleUserVo(userIdList);
    }

    /**
     * 添加同行人
     * @param batchAddComateDtoList
     * @return
     */
    @Override
    public int batchAddComate(List<BatchAddComateDto> batchAddComateDtoList) {
        if (DcListUtils.isNotEmpty(batchAddComateDtoList)){
            LoginUser loginUser = SecurityUtils.getLoginUser();

            List<String> checkRepeatList = new ArrayList<>();
            for (BatchAddComateDto batchAddComateDto : batchAddComateDtoList) {
                String checkRepeat = batchAddComateDto.getMateCardType() + "," + batchAddComateDto.getMateCardNum();
                if (checkRepeatList.contains(checkRepeat)){
                    throw new RuntimeException("证件一致的信息不可重复添加");
                }

                checkRepeatList.add(checkRepeat);
            }

            TfpComate param = new TfpComate();
            param.setMainUserId(loginUser.getUserId());
            param.setDelFlag(DeleteEnum.EXIST.getValue());
            List<TfpComate> tfpComateList = tfpComateMapper.selectTfpComateList(param);
            if (DcListUtils.isNotEmpty(tfpComateList)){
                for (TfpComate tfpComate : tfpComateList) {
                    if (checkRepeatList.contains(tfpComate.getMateCardType() + "," + tfpComate.getMateCardNum())){
                        throw new RuntimeException("证件一致的信息不可重复添加");
                    }
                }
            }

            for (BatchAddComateDto batchAddComateDto : batchAddComateDtoList) {
                TfpComate tfpComate = new TfpComate();
                BeanUtils.copyProperties(batchAddComateDto, tfpComate);

                tfpComate.setMainUserId(loginUser.getUserId());
                tfpComate.setDelFlag(DeleteEnum.EXIST.getValue());
                tfpComate.setCreateUserId(loginUser.getUserId());
                tfpComate.setCreateBy(loginUser.getUsername());
                tfpComate.setCreateTime(DateUtils.getNowDate());

                tfpComateMapper.insertTfpComate(tfpComate);
            }
        }

        return 1;
    }

    @Override
    public List<TfpComate> selectTfpComateByIdList(List<Long> idList) {
        return tfpComateMapper.selectTfpComateByIdList(idList);
    }

    /**
     * 小程序-删除同行人
     * @param mateId
     * @return
     */
    @Override
    public int deleteComate(Long mateId) {
        if (Objects.isNull(mateId)){
            throw new BusinessException("同行人id不能为空");
        }

        return tfpComateMapper.deleteTfpComateById(mateId);
    }

    @Override
    public List<ComateVo> getMyComateList() {
        List<ComateVo> comateVoList = new ArrayList<>();
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = loginUser.getUserId();

        TfpComate param = new TfpComate();
        param.setMainUserId(userId);
        param.setDelFlag(DeleteEnum.EXIST.getValue());
        List<TfpComate> tfpComateList = tfpComateMapper.selectTfpComateList(param);

        if (DcListUtils.isNotEmpty(tfpComateList)){
            for (TfpComate tfpComate : tfpComateList) {
                ComateVo comateVo = new ComateVo();
                BeanUtils.copyProperties(tfpComate, comateVo);

                Long viceUserId = tfpComate.getViceUserId();
                if (Objects.nonNull(viceUserId) && Objects.equals(viceUserId, userId)){
                    comateVoList.add(0, comateVo);
                } else {
                    comateVoList.add(comateVo);
                }
            }
        }

        return comateVoList;
    }
}
