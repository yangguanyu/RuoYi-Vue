package com.ruoyi.service.vo;

import com.ruoyi.common.area.domain.model.AreaDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "DestinationAreaVo", description = "目的地")
public class DestinationAreaVo {
    @ApiModelProperty("导航id")
    private Long id;
    @ApiModelProperty("导航名称")
    private String name;
    @ApiModelProperty("区域集合")
    private List<AreaDTO> areaDTOList;
    @ApiModelProperty("排序")
    private Integer sort;
}
