package com.ruoyi.service.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel(value = "InvitingAchievementsVo")
public class InvitingAchievementsVo {
    @ApiModelProperty("查看人数")
    private Integer viewNumber;
    @ApiModelProperty("下单人数")
    private Integer placeOrderNumber;
    @ApiModelProperty("商品id")
    private Long productId;
    @ApiModelProperty("商品名称")
    private String productName;
    @ApiModelProperty("产品编号")
    private String productNo;
    @ApiModelProperty("创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    @ApiModelProperty("商品图片url")
    private String productImageUrl;
}
