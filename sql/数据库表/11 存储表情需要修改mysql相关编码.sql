-- 数据库编码修改
ALTER DATABASE qinglv CHARACTER SET utf8mb4;

-- 表与字段的编码修改
ALTER TABLE tfp_chat_group_person_info CONVERT TO CHARACTER SET utf8mb4;
ALTER TABLE qinglv.tfp_chat_group_person_info MODIFY COLUMN chat_info VARCHAR(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '聊天信息';

-- 表与字段的编码修改
ALTER TABLE tfp_find_partner_chat_group_person_info CONVERT TO CHARACTER SET utf8mb4;
ALTER TABLE qinglv.tfp_find_partner_chat_group_person_info MODIFY COLUMN chat_info VARCHAR(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '聊天信息';
