package com.ruoyi.service.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enuma.DeleteEnum;
import com.ruoyi.common.enuma.WantToGoTypeEnum;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.DcListUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.service.dto.ListProductSelectDto;
import com.ruoyi.service.dto.WantToGoDto;
import com.ruoyi.service.service.ITfpProductService;
import com.ruoyi.service.vo.ListProductVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpProductWantToGoMapper;
import com.ruoyi.service.domain.TfpProductWantToGo;
import com.ruoyi.service.service.ITfpProductWantToGoService;
import org.springframework.transaction.annotation.Transactional;

import static com.ruoyi.common.utils.PageUtils.startPage;

/**
 * 商品想去Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-12-26
 */
@Service
@Transactional
public class TfpProductWantToGoServiceImpl implements ITfpProductWantToGoService 
{
    @Autowired
    private TfpProductWantToGoMapper tfpProductWantToGoMapper;
    @Autowired
    private ITfpProductService productService;

    /**
     * 查询商品想去
     * 
     * @param id 商品想去主键
     * @return 商品想去
     */
    @Override
    public TfpProductWantToGo selectTfpProductWantToGoById(Long id)
    {
        return tfpProductWantToGoMapper.selectTfpProductWantToGoById(id);
    }

    /**
     * 查询商品想去列表
     * 
     * @param tfpProductWantToGo 商品想去
     * @return 商品想去
     */
    @Override
    public List<TfpProductWantToGo> selectTfpProductWantToGoList(TfpProductWantToGo tfpProductWantToGo)
    {
        return tfpProductWantToGoMapper.selectTfpProductWantToGoList(tfpProductWantToGo);
    }

    /**
     * 新增商品想去
     * 
     * @param tfpProductWantToGo 商品想去
     * @return 结果
     */
    @Override
    public int insertTfpProductWantToGo(TfpProductWantToGo tfpProductWantToGo)
    {
        tfpProductWantToGo.setCreateTime(DateUtils.getNowDate());
        return tfpProductWantToGoMapper.insertTfpProductWantToGo(tfpProductWantToGo);
    }

    /**
     * 修改商品想去
     * 
     * @param tfpProductWantToGo 商品想去
     * @return 结果
     */
    @Override
    public int updateTfpProductWantToGo(TfpProductWantToGo tfpProductWantToGo)
    {
        tfpProductWantToGo.setUpdateTime(DateUtils.getNowDate());
        return tfpProductWantToGoMapper.updateTfpProductWantToGo(tfpProductWantToGo);
    }

    /**
     * 批量删除商品想去
     * 
     * @param ids 需要删除的商品想去主键
     * @return 结果
     */
    @Override
    public int deleteTfpProductWantToGoByIds(Long[] ids)
    {
        return tfpProductWantToGoMapper.deleteTfpProductWantToGoByIds(ids);
    }

    /**
     * 删除商品想去信息
     * 
     * @param id 商品想去主键
     * @return 结果
     */
    @Override
    public int deleteTfpProductWantToGoById(Long id)
    {
        return tfpProductWantToGoMapper.deleteTfpProductWantToGoById(id);
    }

    @Override
    public TfpProductWantToGo selectTfpProductWantToGo(TfpProductWantToGo param){
        return tfpProductWantToGoMapper.selectTfpProductWantToGo(param);
    }

    /**
     * 商品想去
     * @param wantToGoDto
     * @return
     */
    @Override
    public int wantToGo(WantToGoDto wantToGoDto) {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        if (Objects.isNull(loginUser)){
            throw new RuntimeException("用户信息不存在");
        }

        Long id = wantToGoDto.getId();
        if (Objects.isNull(id)){
            throw new RuntimeException("商品id不能为空");
        }

        TfpProductWantToGo param = new TfpProductWantToGo();
        param.setProductId(id);
        param.setUserId(loginUser.getUserId());

        Integer wantToGoType = wantToGoDto.getWantToGoType();
        if (Objects.equals(wantToGoType, WantToGoTypeEnum.WANT_TO_GO.getValue())) {
            TfpProductWantToGo tfpProductWantToGo = tfpProductWantToGoMapper.selectTfpProductWantToGo(param);
            if (Objects.isNull(tfpProductWantToGo)){
                tfpProductWantToGo = new TfpProductWantToGo();
                tfpProductWantToGo.setProductId(wantToGoDto.getId());
                tfpProductWantToGo.setUserId(loginUser.getUserId());
                tfpProductWantToGo.setDelFlag(DeleteEnum.EXIST.getValue());
                tfpProductWantToGo.setCreateUserId(loginUser.getUserId());
                tfpProductWantToGo.setCreateBy(loginUser.getUsername());
                tfpProductWantToGo.setCreateTime(DateUtils.getNowDate());
                tfpProductWantToGoMapper.insertTfpProductWantToGo(tfpProductWantToGo);
            }
        }

        if (Objects.equals(wantToGoType, WantToGoTypeEnum.CANCEL.getValue())){
            tfpProductWantToGoMapper.deleteTfpProductWantToGo(param);
        }

        return 1;
    }

    /**
     * 小程序-我的想去的商品
     * @return
     */
    @Override
    public List<ListProductVo> myWantToProductList() {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = loginUser.getUserId();

        startPage();
        List<ListProductVo> listProductVoList = tfpProductWantToGoMapper.myWantToProductList(userId);
        if (DcListUtils.isNotEmpty(listProductVoList)){
            List<Long> productIdList = listProductVoList.stream().map(ListProductVo::getId).collect(Collectors.toList());

            ListProductSelectDto listProductSelectDto = new ListProductSelectDto();
            listProductSelectDto.setIdList(productIdList);
            List<ListProductVo> productInfoList = productService.handleListProductVo(listProductSelectDto);
            Map<Long, ListProductVo> productInfoMap = productInfoList.stream().collect(Collectors.toMap(ListProductVo::getId, ListProductVo -> ListProductVo));

            for (ListProductVo listProductVo : listProductVoList) {
                ListProductVo productInfo = productInfoMap.get(listProductVo.getId());
                BeanUtils.copyProperties(productInfo, listProductVo);
            }
        }

        return listProductVoList;
    }
}
