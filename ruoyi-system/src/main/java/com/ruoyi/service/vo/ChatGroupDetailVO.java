package com.ruoyi.service.vo;

import lombok.Data;

import java.util.List;

@Data
public class ChatGroupDetailVO {

    private String GroupName;

    private List<ChatGroupPersonVO> personVOList;


}
