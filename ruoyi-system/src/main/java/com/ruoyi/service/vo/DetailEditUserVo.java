package com.ruoyi.service.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.service.dto.UserImageDto;
import com.ruoyi.service.dto.UserLabelDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@ApiModel(value = "DetailEditUserVo", description = "编辑用户详情")
public class DetailEditUserVo {
    @ApiModelProperty("导航id")
    private Long id;
    @ApiModelProperty("用户昵称")
    private String nickName;
    @ApiModelProperty("用户性别（0男 1女 2未知）")
    private String sex;
    @ApiModelProperty("生日")
    private Date birthday;
    @ApiModelProperty("展示生日")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date showBirthday;
    @ApiModelProperty("星座编码")
    private String constellationCode;
    @ApiModelProperty("星座名称")
    private String constellationName;
    @ApiModelProperty("实名认证状态 0-未实名认证 1-已实名认证")
    private Integer realNameAuthStatus;
    @ApiModelProperty("个人简介")
    private String personalProfile;
    @ApiModelProperty("用户标签集合")
    private List<UserLabelDto> userLabelList;
    @ApiModelProperty("用户图片集合")
    private List<UserImageDto> userImageList;
    @ApiModelProperty("手机号码")
    private String phonenumber;

    public Date getShowBirthday(){
        return birthday;
    }
}
