package com.ruoyi.web.controller.service;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.service.vo.CareUserInfoVo;
import com.ruoyi.service.vo.FansUserInfoVo;
import com.ruoyi.service.vo.SysUserCareVo;
import com.ruoyi.service.vo.UserVo;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.SysUserCare;
import com.ruoyi.service.service.ISysUserCareService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 关注Controller
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@RestController
@RequestMapping("/service/userCare")
public class SysUserCareController extends BaseController
{
    @Autowired
    private ISysUserCareService sysUserCareService;

    /**
     * 查询关注列表
     */
    @PreAuthorize("@ss.hasPermi('service:userCare:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysUserCare sysUserCare)
    {
        startPage();
        List<SysUserCareVo> list = sysUserCareService.selectSysUserCareVoList(sysUserCare);
        return getDataTable(list);
    }

    /**
     * 导出关注列表
     */
    @PreAuthorize("@ss.hasPermi('service:userCare:export')")
    @Log(title = "关注", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysUserCare sysUserCare)
    {
        List<SysUserCare> list = sysUserCareService.selectSysUserCareList(sysUserCare);
        ExcelUtil<SysUserCare> util = new ExcelUtil<SysUserCare>(SysUserCare.class);
        util.exportExcel(response, list, "关注数据");
    }

    /**
     * 获取关注详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:userCare:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(sysUserCareService.selectSysUserCareById(id));
    }

    /**
     * 新增关注
     */
    @PreAuthorize("@ss.hasPermi('service:userCare:add')")
    @Log(title = "关注", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysUserCare sysUserCare)
    {
        return toAjax(sysUserCareService.insertSysUserCare(sysUserCare));
    }

    /**
     * 修改关注
     */
    @PreAuthorize("@ss.hasPermi('service:userCare:edit')")
    @Log(title = "关注", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysUserCare sysUserCare)
    {
        return toAjax(sysUserCareService.updateSysUserCare(sysUserCare));
    }

    /**
     * 删除关注
     */
    @PreAuthorize("@ss.hasPermi('service:userCare:remove')")
    @Log(title = "关注", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(sysUserCareService.deleteSysUserCareByIds(ids));
    }


    @PreAuthorize("@ss.hasPermi('service:userCare:list')")
    @PostMapping("/ownerUserSelect")
    @ResponseBody
    public R<List<UserVo>> ownerUserSelect()
    {
        return R.ok(sysUserCareService.ownerUserSelect());
    }

    @PreAuthorize("@ss.hasPermi('service:userCare:list')")
    @PostMapping("/fansUserSelect")
    @ResponseBody
    public R<List<UserVo>> fansUserSelect()
    {
        return R.ok(sysUserCareService.fansUserSelect());
    }

    @ApiOperation("小程序-关注列表")
    @PostMapping("/careUserInfoList")
    @ResponseBody
    public TableDataInfo<CareUserInfoVo> careUserInfoList(@RequestParam("userId") Long userId)
    {
        return getDataTable(sysUserCareService.careUserInfoList(userId));
    }

    @ApiOperation("小程序-粉丝列表")
    @PostMapping("/fansUserInfoList")
    @ResponseBody
    public TableDataInfo<FansUserInfoVo> fansUserInfoList(@RequestParam("userId") Long userId)
    {
        return getDataTable(sysUserCareService.fansUserInfoList(userId));
    }

    @ApiOperation("小程序-关注用户")
    @PostMapping("/careUser")
    public R careUser(@RequestParam("userId") Long userId)
    {
        return R.ok(sysUserCareService.careUser(userId));
    }

    @ApiOperation("小程序-取消关注用户")
    @PostMapping("/cancelCareUser")
    @ResponseBody
    public R cancelCareUser(@RequestParam("userId") Long userId)
    {
        return R.ok(sysUserCareService.cancelCareUser(userId));
    }
}
