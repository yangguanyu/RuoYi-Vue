package com.ruoyi.web.controller.service;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.service.dto.ApplyDrawMoneyDto;
import com.ruoyi.service.dto.AuditDto;
import com.ruoyi.service.dto.DrawMoneyAuditListDto;
import com.ruoyi.service.vo.DetailDrawMoneyVo;
import com.ruoyi.service.vo.DrawMoneyAuditListVo;
import com.ruoyi.service.vo.DrawMoneyDetailVo;
import com.ruoyi.service.vo.DrawMoneyListVo;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.TfpDrawMoney;
import com.ruoyi.service.service.ITfpDrawMoneyService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 提款Controller
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@RestController
@RequestMapping("/service/drawMoney")
public class TfpDrawMoneyController extends BaseController
{
    @Autowired
    private ITfpDrawMoneyService tfpDrawMoneyService;

    /**
     * 查询提款列表
     */
    @PreAuthorize("@ss.hasPermi('service:drawMoney:list')")
    @GetMapping("/list")
    public TableDataInfo list(TfpDrawMoney tfpDrawMoney)
    {
        startPage();
        List<TfpDrawMoney> list = tfpDrawMoneyService.selectTfpDrawMoneyList(tfpDrawMoney);
        return getDataTable(list);
    }

    /**
     * 导出提款列表
     */
    @PreAuthorize("@ss.hasPermi('service:drawMoney:export')")
    @Log(title = "提款", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TfpDrawMoney tfpDrawMoney)
    {
        List<TfpDrawMoney> list = tfpDrawMoneyService.selectTfpDrawMoneyList(tfpDrawMoney);
        ExcelUtil<TfpDrawMoney> util = new ExcelUtil<TfpDrawMoney>(TfpDrawMoney.class);
        util.exportExcel(response, list, "提款数据");
    }

    /**
     * 获取提款详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:drawMoney:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tfpDrawMoneyService.selectTfpDrawMoneyById(id));
    }

    /**
     * 新增提款
     */
    @PreAuthorize("@ss.hasPermi('service:drawMoney:add')")
    @Log(title = "提款", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TfpDrawMoney tfpDrawMoney)
    {
        return toAjax(tfpDrawMoneyService.insertTfpDrawMoney(tfpDrawMoney));
    }

    /**
     * 修改提款
     */
    @PreAuthorize("@ss.hasPermi('service:drawMoney:edit')")
    @Log(title = "提款", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TfpDrawMoney tfpDrawMoney)
    {
        return toAjax(tfpDrawMoneyService.updateTfpDrawMoney(tfpDrawMoney));
    }

    /**
     * 删除提款
     */
    @PreAuthorize("@ss.hasPermi('service:drawMoney:remove')")
    @Log(title = "提款", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tfpDrawMoneyService.deleteTfpDrawMoneyByIds(ids));
    }

    @ApiOperation("小程序-提现列表")
    @PostMapping("/drawMoneyList")
    @ResponseBody
    public TableDataInfo<DrawMoneyListVo> drawMoneyList(){
        return getDataTable(tfpDrawMoneyService.drawMoneyList());
    }

    @ApiOperation("小程序-提现申请")
    @PostMapping("/applyDrawMoney")
    @ResponseBody
    public R<Long> applyDrawMoney(@RequestBody ApplyDrawMoneyDto applyDrawMoneyDto){
        return R.ok(tfpDrawMoneyService.applyDrawMoney(applyDrawMoneyDto));
    }

    @ApiOperation("小程序-提现详情")
    @PostMapping("/detailDrawMoney")
    @ResponseBody
    public R<DetailDrawMoneyVo> detailDrawMoney(@RequestParam("id") Long id){
        return R.ok(tfpDrawMoneyService.detailDrawMoney(id));
    }

    @ApiOperation("后台-审核提现")
    @PostMapping("/auditDrawMoney")
    @ResponseBody
    public R auditDrawMoney(@RequestBody AuditDto auditDto){
        return R.ok(tfpDrawMoneyService.auditDrawMoney(auditDto));
    }

    @ApiOperation("后台-提现审核列表")
    @PostMapping("/drawMoneyAuditList")
    @ResponseBody
    public TableDataInfo<DrawMoneyAuditListVo> drawMoneyAuditList(@RequestBody DrawMoneyAuditListDto drawMoneyAuditListDto){
        return getDataTable(tfpDrawMoneyService.drawMoneyAuditList(drawMoneyAuditListDto));
    }

    @ApiOperation("后台-提现详情")
    @PostMapping("/drawMoneyDetail")
    @ResponseBody
    public R<DrawMoneyDetailVo> drawMoneyDetail(@RequestParam("id") Long id){
        return R.ok(tfpDrawMoneyService.drawMoneyDetail(id));
    }
}
