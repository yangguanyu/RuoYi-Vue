package com.ruoyi.service.vo;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 通程前后航段信息对象 tfp_flight_segment_detail
 * 
 * @author ruoyi
 * @date 2024-04-10
 */
@Data
public class TfpFlightSegmentDetailVo extends BaseEntity
{
    /** 主键 */
    private Long id;

    /** 航班信息表id */
    private Long flightId;

    /** 航班号 */
    private String flightNo;

    /** 出发城市三字码 */
    private String depCode;

    /** 起飞城市名称 */
    private String depCity;

    /** 出发机场名称 */
    private String depAirport;

    /** 抵达城市三字码 */
    private String arrCode;

    /** 抵达城市名称 */
    private String arrCity;

    /** 抵达机场名称 */
    private String arrAirport;

    /** 航段 */
    private String segment;

    /** 出发日期 */
    private String depDate;

    /** 出发时间 */
    private String depTime;

    /** 抵达日期 */
    private String arrDate;

    /** 抵达时间 */
    private String arrTime;

    /** 航司 */
    private String carrier;

    /** 出发航站 */
    private String depTerm;

    /** 抵达航站 */
    private String arrTerm;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    private Long createUserId;

    /** 更新者id */
    private Long updateUserId;

}
