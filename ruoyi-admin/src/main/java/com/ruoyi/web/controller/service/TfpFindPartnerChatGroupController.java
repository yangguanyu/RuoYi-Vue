package com.ruoyi.web.controller.service;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.service.dto.TfpFindPartnerChatGroupDto;
import com.ruoyi.service.vo.FindPartnerDetailVo;
import com.ruoyi.service.vo.TfpFindPartnerChatGroupVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.TfpFindPartnerChatGroup;
import com.ruoyi.service.service.ITfpFindPartnerChatGroupService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 找搭子聊天群组Controller
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@RestController
@RequestMapping("/service/findPartnerChatGroup")
public class TfpFindPartnerChatGroupController extends BaseController
{
    @Autowired
    private ITfpFindPartnerChatGroupService tfpFindPartnerChatGroupService;

    /**
     * 查询找搭子聊天群组列表
     */
    @PreAuthorize("@ss.hasPermi('service:findPartnerChatGroup:list')")
    @GetMapping("/list")
    public TableDataInfo list(TfpFindPartnerChatGroupDto tfpFindPartnerChatGroupDto)
    {
        startPage();
        List<TfpFindPartnerChatGroupVo> list = tfpFindPartnerChatGroupService.selectTfpFindPartnerChatGroupVoList(tfpFindPartnerChatGroupDto);
        return getDataTable(list);
    }

    /**
     * 导出找搭子聊天群组列表
     */
    @PreAuthorize("@ss.hasPermi('service:findPartnerChatGroup:export')")
    @Log(title = "找搭子聊天群组", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TfpFindPartnerChatGroup tfpFindPartnerChatGroup)
    {
        List<TfpFindPartnerChatGroup> list = tfpFindPartnerChatGroupService.selectTfpFindPartnerChatGroupList(tfpFindPartnerChatGroup);
        ExcelUtil<TfpFindPartnerChatGroup> util = new ExcelUtil<TfpFindPartnerChatGroup>(TfpFindPartnerChatGroup.class);
        util.exportExcel(response, list, "找搭子聊天群组数据");
    }

    /**
     * 获取找搭子聊天群组详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:findPartnerChatGroup:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tfpFindPartnerChatGroupService.selectTfpFindPartnerChatGroupById(id));
    }

    /**
     * 新增找搭子聊天群组
     */
    @PreAuthorize("@ss.hasPermi('service:findPartnerChatGroup:add')")
    @Log(title = "找搭子聊天群组", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TfpFindPartnerChatGroup tfpFindPartnerChatGroup)
    {
        return toAjax(tfpFindPartnerChatGroupService.insertTfpFindPartnerChatGroup(tfpFindPartnerChatGroup));
    }

    /**
     * 修改找搭子聊天群组
     */
    @PreAuthorize("@ss.hasPermi('service:findPartnerChatGroup:edit')")
    @Log(title = "找搭子聊天群组", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TfpFindPartnerChatGroup tfpFindPartnerChatGroup)
    {
        return toAjax(tfpFindPartnerChatGroupService.updateTfpFindPartnerChatGroup(tfpFindPartnerChatGroup));
    }

    /**
     * 删除找搭子聊天群组
     */
    @PreAuthorize("@ss.hasPermi('service:findPartnerChatGroup:remove')")
    @Log(title = "找搭子聊天群组", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tfpFindPartnerChatGroupService.deleteTfpFindPartnerChatGroupByIds(ids));
    }

    @PreAuthorize("@ss.hasPermi('service:findPartnerChatGroup:query')")
    @GetMapping(value = "/detail/{id}")
    public R<FindPartnerDetailVo> detail(@PathVariable("id") Long id)
    {
        return R.ok(tfpFindPartnerChatGroupService.detail(id));
    }
}
