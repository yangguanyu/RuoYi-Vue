package com.ruoyi.service.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 订单对象 tfp_order
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@Data
public class TfpOrderVo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 订单编号 */
    private String orderNo;

    /** 供应商id */
    private Long supplyId;

    private String supplyName;

    /** 商品ID */
    private Long productId;

    private String productContent;

    /** 售卖单价 */
    private BigDecimal salePrice;

    /** 商品数量 */
    private Long quantities;

    /** 订单金额 */
    private BigDecimal finalAmount;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date goDate;

    private String goPlaceName;

    /** 订单买家备注(预留) */
    private String buyerRemark;

    /** 订单卖家备注（预留） */
    private String sellerRemark;

    /** 订单状态 0-待支付 1-已完成 2-待确认(已支付) 3-待出行 4-申请退款-审核中 5-申请退款-退款成功 6-过期 7-取消 */
    private Integer status;

    /** 商品快照id */
    private String snapshotId;

    /** 是否可售后（0：不可以，1：可以） */
    private Integer afterServiceAble;

    /** 预支付交易会话标识 */
    private String prepayId;

    /** 预支付交易会话过期时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date expireDate;

    /** 出行方式 0-组队出行 1-独立出行 */
    private Integer travelType;

    /** 群组id */
    private Long chatGroupId;

    /** 是否可以申请退款 0-否 1-是 */
    private Integer applyRefundFlag;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    private Long createUserId;

    /** 更新者id */
    private Long updateUserId;

    /** 支付时间 */
    private Date payTime;;

    /** 出行完成时间 */
    private Date travelCompleteTime;;

    /** 退款截止时间 */
    private Date refundEndTime;

    /** 分享用户id */
    private Long shareUserId;

    /** 机票价格 */
    private Integer goAirTicketPrice;

    /** 航班返程价格 */
    private Integer backAirTicketPrice;

    /** 是否选择低价机票 0-否 1-是 */
    private Integer lowPriceAirTicketFlag;
}
