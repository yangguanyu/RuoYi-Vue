package com.ruoyi.service.domain.flight.param;

import lombok.Data;

@Data
public class PayParam {
    // 平台订单号
    private String orderNo;

    // 商家订单号，暂不支持
    private String outOrderNo;

    // 支付方式：1表示余额支付，2表示支付宝自动支付（需绑定支付宝账号），3表示汇付支付（需绑定汇付账号）
    private Integer payType;

    // 授权可支付的营业员，可以填写主账号
    private String payerLoginName;

    // 订单结算价，可以不传，以生成订单的结算价支付PaymentInfoResp
    private Double totalPay;

}
