package com.ruoyi.service.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("DrawMoneyBankCardInfoVo")
public class DrawMoneyBankCardInfoVo {
    @ApiModelProperty("账户姓名")
    private String userName;
    @ApiModelProperty("银行卡号")
    private String bankCardNumber;
    @ApiModelProperty("银行名称")
    private String bankName;
    @ApiModelProperty("手机号")
    private String phoneNumber;
}
