package com.ruoyi.service.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@ApiModel(value = "RefundInfoVo", description = "退款信息")
public class RefundInfoVo {
    @ApiModelProperty("退款编号")
    private String serialNo;
    @ApiModelProperty("申请退款时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date applyTime;
    @ApiModelProperty("申请退款原因")
    private String reason;
    @ApiModelProperty("退款金额")
    private BigDecimal refundAmount;
    @ApiModelProperty("退款状态（1：退款审核中，2：审核成功-退款中，3：审核失败-退款失败，4：退款失败，5：退款成功）")
    private Integer refundStatus;
    @ApiModelProperty("申请退款驳回的原因")
    private String rejectReason;
    @ApiModelProperty("退款时间")
    @JsonFormat(pattern = "yyyy年MM月dd日")
    private Date refundTime;
}
