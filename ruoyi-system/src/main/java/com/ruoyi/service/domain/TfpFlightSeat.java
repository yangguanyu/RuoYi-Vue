package com.ruoyi.service.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 舱位信息对象 tfp_flight_seat
 * 
 * @author ruoyi
 * @date 2024-04-10
 */
public class TfpFlightSeat extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 航班信息表id */
    @Excel(name = "航班信息表id")
    private Long flightId;

    /** 舱位编码 */
    @Excel(name = "舱位编码")
    private String seatCode;

    /** 儿童舱位编码 */
    @Excel(name = "儿童舱位编码")
    private String chdSeatCode;

    /** 舱位状态 0-9 或者 英文字母 */
    @Excel(name = "舱位状态 0-9 或者 英文字母")
    private String seatStatus;

    /** 儿童舱位状态 1-9代表剩余座位数，A代表座位数大于9 */
    @Excel(name = "儿童舱位状态 1-9代表剩余座位数，A代表座位数大于9")
    private String chdSeatStatus;

    /** 婴儿舱位编码 */
    @Excel(name = "婴儿舱位编码")
    private String infSeatCode;

    /** 婴儿舱位状态 1-9代表剩余座位数，A代表座位数大于9 */
    @Excel(name = "婴儿舱位状态 1-9代表剩余座位数，A代表座位数大于9")
    private String infSeatStatus;

    /** 服务级别 1-头等舱 2-商务舱 3-经济舱 4-舒适A舱 5-超级经济舱 6-高端经济舱 7-尊享经济 8-超值经济 9-超值公务 10-超值头等 11-舒适经济 12-优惠商务 13-明珠经济 14-超值商务 15-空中商务舱 16-公务舱产品舱 */
    @Excel(name = "服务级别 1-头等舱 2-商务舱 3-经济舱 4-舒适A舱 5-超级经济舱 6-高端经济舱 7-尊享经济 8-超值经济 9-超值公务 10-超值头等 11-舒适经济 12-优惠商务 13-明珠经济 14-超值商务 15-空中商务舱 16-公务舱产品舱")
    private Integer cabinClass;

    /** 舱位说明 */
    @Excel(name = "舱位说明")
    private String seatMsg;

    /** 是否特价舱位 1-普通 3-特价 */
    @Excel(name = "是否特价舱位 1-普通 3-特价")
    private Integer seatType;

    /** 折扣 */
    @Excel(name = "折扣")
    private Double discount;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    @Excel(name = "创建者id")
    private Long createUserId;

    /** 更新者id */
    @Excel(name = "更新者id")
    private Long updateUserId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setFlightId(Long flightId) 
    {
        this.flightId = flightId;
    }

    public Long getFlightId() 
    {
        return flightId;
    }
    public void setSeatCode(String seatCode) 
    {
        this.seatCode = seatCode;
    }

    public String getSeatCode() 
    {
        return seatCode;
    }
    public void setChdSeatCode(String chdSeatCode) 
    {
        this.chdSeatCode = chdSeatCode;
    }

    public String getChdSeatCode() 
    {
        return chdSeatCode;
    }
    public void setSeatStatus(String seatStatus) 
    {
        this.seatStatus = seatStatus;
    }

    public String getSeatStatus() 
    {
        return seatStatus;
    }
    public void setChdSeatStatus(String chdSeatStatus) 
    {
        this.chdSeatStatus = chdSeatStatus;
    }

    public String getChdSeatStatus() 
    {
        return chdSeatStatus;
    }
    public void setInfSeatCode(String infSeatCode) 
    {
        this.infSeatCode = infSeatCode;
    }

    public String getInfSeatCode() 
    {
        return infSeatCode;
    }
    public void setInfSeatStatus(String infSeatStatus) 
    {
        this.infSeatStatus = infSeatStatus;
    }

    public String getInfSeatStatus() 
    {
        return infSeatStatus;
    }
    public void setCabinClass(Integer cabinClass) 
    {
        this.cabinClass = cabinClass;
    }

    public Integer getCabinClass() 
    {
        return cabinClass;
    }
    public void setSeatMsg(String seatMsg) 
    {
        this.seatMsg = seatMsg;
    }

    public String getSeatMsg() 
    {
        return seatMsg;
    }
    public void setSeatType(Integer seatType) 
    {
        this.seatType = seatType;
    }

    public Integer getSeatType() 
    {
        return seatType;
    }
    public void setDiscount(Double discount)
    {
        this.discount = discount;
    }

    public Double getDiscount()
    {
        return discount;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }
    public void setUpdateUserId(Long updateUserId) 
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId() 
    {
        return updateUserId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("flightId", getFlightId())
            .append("seatCode", getSeatCode())
            .append("chdSeatCode", getChdSeatCode())
            .append("seatStatus", getSeatStatus())
            .append("chdSeatStatus", getChdSeatStatus())
            .append("infSeatCode", getInfSeatCode())
            .append("infSeatStatus", getInfSeatStatus())
            .append("cabinClass", getCabinClass())
            .append("seatMsg", getSeatMsg())
            .append("seatType", getSeatType())
            .append("discount", getDiscount())
            .append("delFlag", getDelFlag())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
