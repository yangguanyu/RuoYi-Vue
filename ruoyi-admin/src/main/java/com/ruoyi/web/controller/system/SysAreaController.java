package com.ruoyi.web.controller.system;

import java.util.List;

import com.ruoyi.common.area.domain.entity.SysArea;
import com.ruoyi.common.area.domain.model.AreaTreeDTO;
import com.ruoyi.common.area.domain.model.DepartureAreaDTO;
import com.ruoyi.common.core.domain.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.service.ISysAreaService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 地区Controller
 * 
 * @author ruoyi
 * @date 2023-10-15
 */
@Api("区域controller")
@Controller
@RequestMapping("/system/area")
@Slf4j
@ApiOperation("区域")
public class SysAreaController extends BaseController
{
    private String prefix = "system/area";

    @Autowired
    private ISysAreaService sysAreaService;

    @GetMapping()
    public String area()
    {
        return prefix + "/area";
    }

    /**
     * 查询地区列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SysArea sysArea)
    {
        startPage();
        List<SysArea> list = sysAreaService.selectSysAreaList(sysArea);
        return getDataTable(list);
    }

    /**
     * 导出地区列表
     */
    @Log(title = "地区", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(SysArea sysArea)
    {
        List<SysArea> list = sysAreaService.selectSysAreaList(sysArea);
        ExcelUtil<SysArea> util = new ExcelUtil<SysArea>(SysArea.class);
        return util.exportExcel(list, "地区数据");
    }

    /**
     * 新增地区
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存地区
     */
    @Log(title = "地区", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(SysArea sysArea)
    {
        return toAjax(sysAreaService.insertSysArea(sysArea));
    }

    /**
     * 修改地区
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        SysArea sysArea = sysAreaService.selectSysAreaById(id);
        mmap.put("sysArea", sysArea);
        return prefix + "/edit";
    }

    /**
     * 修改保存地区
     */
    @Log(title = "地区", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SysArea sysArea)
    {
        return toAjax(sysAreaService.updateSysArea(sysArea));
    }

    /**
     * 删除地区
     */
    @Log(title = "地区", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(sysAreaService.deleteSysAreaByIds(ids));
    }

    /**
     * 获取出发地
     * @return
     */
    @ApiOperation("小程序-获取出发地")
    @PostMapping("/getDepartureArea")
    @ResponseBody
    public R<DepartureAreaDTO> getDepartureArea(){
        log.info("获取出发地");
        return R.ok(sysAreaService.getDepartureArea());
    }

    @ApiOperation("小程序-查询获取出发地区域名称列表")
    @PostMapping("/searchDepartureAreaNameByKey")
    @ResponseBody
    public R<List<String>> searchDepartureAreaNameByKey(@RequestParam("key") String key){
        return R.ok(sysAreaService.searchDepartureAreaNameByKey(key));
    }


    /**
     * 获取业务区域 二级联动
     * @return
     */
    @ApiOperation("获取业务区域")
    @GetMapping("/getServiceAreaList")
    @ResponseBody
    public R<List<AreaTreeDTO>> getServiceAreaList(){
        log.info("获取业务区域");
        return R.ok(sysAreaService.getServiceAreaList());
    }

    /**
     * 获取省市区域 二级联动
     * @return
     */
    @ApiOperation("获取省市区域")
    @GetMapping("/getProvinceUrbanList")
    @ResponseBody
    public R<List<AreaTreeDTO>> getProvinceUrbanList(){
        log.info("获取省市区域");
        boolean containAreaFlag = false;
        return R.ok(sysAreaService.getAreaList(containAreaFlag));
    }

    /**
     * 获取省市区区域 三级联动
     * @return
     */
    @ApiOperation("获取省市区区域")
    @GetMapping("/getProvinceUrbanAreaList")
    @ResponseBody
    public R<List<AreaTreeDTO>> getProvinceUrbanAreaList(){
        log.info("获取省市区区域");
        boolean containAreaFlag = true;
        return R.ok(sysAreaService.getAreaList(containAreaFlag));
    }
}
