package com.ruoyi.service.excel;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class UserIncomeDetailExportExcel {
    @Excel(name = "结算单编号")
    private String settlementNumber;
    @Excel(name = "产品编号")
    private String productNo;
    @Excel(name = "产品类型名称")
    private String categoryName;
    @Excel(name = "分享者")
    private String realName;
    @Excel(name = "本单收益（元）")
    private BigDecimal price;
    @Excel(name = "关联订单编号")
    private String orderNo;
    @Excel(name = "是否已过售后期")
    private String afterSalesTimeFlagName;
    @Excel(name = "结算状态")
    private String settlementReqStatusName;

    private Integer afterSalesTimeFlag;
    private Integer settlementReqStatus;
}
