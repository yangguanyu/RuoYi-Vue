package com.ruoyi.service.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "DetailTravelRuleVo")
public class DetailTravelRuleVo {
    @ApiModelProperty("主键")
    private Long id;
    @ApiModelProperty("规则类型")
    private String travelTypeName;
    @ApiModelProperty("版本号")
    private String version;
    @ApiModelProperty("操作人")
    private String realName;
    @ApiModelProperty("文案")
    private String text;
    @ApiModelProperty("类型 0-旅行方式")
    private Integer type;
}
