package com.ruoyi.service.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "OrderListComateInfoVo")
public class OrderListComateInfoVo {
    @ApiModelProperty("姓名")
    private String realName;
    @ApiModelProperty("身份证号")
    private String cardNum;
    @ApiModelProperty("手机号")
    private String phonenumber;
}
