package com.ruoyi.service.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
@ApiModel(value = "CreateOrderDetailVo", description = "生成订单详情")
public class CreateOrderDetailVo {
    @ApiModelProperty("商品id")
    private Long productId;
    @ApiModelProperty("商品名称")
    private String productName;
    @ApiModelProperty("商品图片")
    private String productImage;
    @ApiModelProperty("出发时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date goDate;

    @ApiModelProperty("服务类型id")
    private Long categoryId;
    @ApiModelProperty("服务类型名称")
    private String categoryName;
    @ApiModelProperty("线路亮点集合")
    private List<String> routeHighlightList;

    @ApiModelProperty("价格")
    private BigDecimal price;

    @ApiModelProperty("聊天群组名称")
    private String ChatGroupName;

    @ApiModelProperty("历史出行人信息集合")
    private List<ComateVo> historyComateInfoList;
}
