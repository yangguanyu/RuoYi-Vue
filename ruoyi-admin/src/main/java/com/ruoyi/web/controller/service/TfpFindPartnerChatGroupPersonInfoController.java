package com.ruoyi.web.controller.service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.utils.bean.BeanUtils;
import com.ruoyi.service.dto.ChatGroupPersonInfoDto;
import com.ruoyi.service.dto.TfpChatGroupPersonInfoDto;
import com.ruoyi.service.dto.TfpFindPartnerChatGroupPersonInfoDto;
import com.ruoyi.service.vo.ChatInfoVo;
import com.ruoyi.service.vo.TfpChatGroupPersonInfoVo;
import com.ruoyi.service.vo.TfpFindPartnerChatGroupPersonInfoVo;
import com.ruoyi.web.controller.service.form.group.ChatGroupInfoSearchForm;
import com.ruoyi.web.controller.service.form.group.ChatGroupPersonInfoForm;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.TfpFindPartnerChatGroupPersonInfo;
import com.ruoyi.service.service.ITfpFindPartnerChatGroupPersonInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 找搭子聊天群组成员聊天信息Controller
 *
 * @author ruoyi
 * @date 2023-10-31
 */
@RestController
@RequestMapping("/service/findPartnerChatGroupPersonInfo")
public class TfpFindPartnerChatGroupPersonInfoController extends BaseController
{
    @Autowired
    private ITfpFindPartnerChatGroupPersonInfoService tfpFindPartnerChatGroupPersonInfoService;

    /**
     * 查询找搭子聊天群组成员聊天信息列表
     */
    @PreAuthorize("@ss.hasPermi('service:findPartnerChatGroupPersonInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(TfpFindPartnerChatGroupPersonInfoDto tfpFindPartnerChatGroupPersonInfoDto)
    {
        startPage();
        List<TfpFindPartnerChatGroupPersonInfoVo> list = tfpFindPartnerChatGroupPersonInfoService.selectTfpFindPartnerChatGroupPersonInfoVoList(tfpFindPartnerChatGroupPersonInfoDto);
        return getDataTable(list);
    }

    /**
     * 导出找搭子聊天群组成员聊天信息列表
     */
    @PreAuthorize("@ss.hasPermi('service:findPartnerChatGroupPersonInfo:export')")
    @Log(title = "找搭子聊天群组成员聊天信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TfpFindPartnerChatGroupPersonInfo tfpFindPartnerChatGroupPersonInfo)
    {
        List<TfpFindPartnerChatGroupPersonInfo> list = tfpFindPartnerChatGroupPersonInfoService.selectTfpFindPartnerChatGroupPersonInfoList(tfpFindPartnerChatGroupPersonInfo);
        ExcelUtil<TfpFindPartnerChatGroupPersonInfo> util = new ExcelUtil<TfpFindPartnerChatGroupPersonInfo>(TfpFindPartnerChatGroupPersonInfo.class);
        util.exportExcel(response, list, "找搭子聊天群组成员聊天信息数据");
    }

    /**
     * 获取找搭子聊天群组成员聊天信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:findPartnerChatGroupPersonInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tfpFindPartnerChatGroupPersonInfoService.selectTfpFindPartnerChatGroupPersonInfoById(id));
    }

    /**
     * 新增找搭子聊天群组成员聊天信息
     */
    @PreAuthorize("@ss.hasPermi('service:findPartnerChatGroupPersonInfo:add')")
    @Log(title = "找搭子聊天群组成员聊天信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TfpFindPartnerChatGroupPersonInfo tfpFindPartnerChatGroupPersonInfo)
    {
        return toAjax(tfpFindPartnerChatGroupPersonInfoService.insertTfpFindPartnerChatGroupPersonInfo(tfpFindPartnerChatGroupPersonInfo));
    }

    /**
     * 修改找搭子聊天群组成员聊天信息
     */
    @PreAuthorize("@ss.hasPermi('service:findPartnerChatGroupPersonInfo:edit')")
    @Log(title = "找搭子聊天群组成员聊天信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TfpFindPartnerChatGroupPersonInfo tfpFindPartnerChatGroupPersonInfo)
    {
        return toAjax(tfpFindPartnerChatGroupPersonInfoService.updateTfpFindPartnerChatGroupPersonInfo(tfpFindPartnerChatGroupPersonInfo));
    }

    /**
     * 删除找搭子聊天群组成员聊天信息
     */
    @PreAuthorize("@ss.hasPermi('service:findPartnerChatGroupPersonInfo:remove')")
    @Log(title = "找搭子聊天群组成员聊天信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tfpFindPartnerChatGroupPersonInfoService.deleteTfpFindPartnerChatGroupPersonInfoByIds(ids));
    }

    @PreAuthorize("@ss.hasPermi('service:findPartnerChatGroupPersonInfo:list')")
    @GetMapping("/chatList")
    public TableDataInfo chatList(TfpFindPartnerChatGroupPersonInfoDto tfpFindPartnerChatGroupPersonInfoDto)
    {
        startPage();
        List<ChatInfoVo> list = tfpFindPartnerChatGroupPersonInfoService.chatList(tfpFindPartnerChatGroupPersonInfoDto.getChatGroupId());
        return getDataTable(list);
    }

    @PostMapping("/applet/add")
    @ApiOperation("小程序-新增聊天群组成员聊天信息")
    public AjaxResult appletChatInfoAdd(@RequestBody ChatGroupPersonInfoForm form) {
        ChatGroupPersonInfoDto chatGroupPersonInfoDto = new ChatGroupPersonInfoDto();
        BeanUtils.copyProperties(form, chatGroupPersonInfoDto);
        return toAjax(tfpFindPartnerChatGroupPersonInfoService.appletChatInfoAdd(chatGroupPersonInfoDto));
    }

    @PostMapping("/applet/chatInfoList")
    @ApiOperation("小程序-获取聊天列表")
    public R<List<TfpFindPartnerChatGroupPersonInfoVo>> appletChatInfoList(@RequestBody ChatGroupInfoSearchForm form)
    {
        TfpChatGroupPersonInfoDto tfpChatGroupPersonInfoDto = new TfpChatGroupPersonInfoDto();
        BeanUtils.copyProperties(form,tfpChatGroupPersonInfoDto);
        tfpChatGroupPersonInfoDto.setChatPersonInfoId(form.getCurrentId());
        List<TfpFindPartnerChatGroupPersonInfoVo> list = tfpFindPartnerChatGroupPersonInfoService.selectAppletInfoVoList(tfpChatGroupPersonInfoDto);
        //处理：不返回传的id
        Long currentId = form.getCurrentId();
        if(Objects.nonNull(currentId)){
            list = list.stream().filter(e->!( e.getId().equals(currentId) )).collect(Collectors.toList());
        }
        return R.ok(list);
    }
}
