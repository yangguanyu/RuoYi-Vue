package com.ruoyi.common.enuma;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ActStatusEnum implements BaseEnum {
    UN_RECONCILED(0, "未对账"),
    SUCCESS(1, "成功"),
    TRANSACTION_AMOUNT_EXCEPTION(2, "交易金额异常"),
    TRANSACTION_AMOUNT_STATUS_EXCEPTION(3, "交易状态异常")
    ;

    private Integer value;
    private String desc;
}
