package com.ruoyi.service.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@ApiModel(value = "JoinChatGroupVo", description = "加入的群组vo")
public class JoinChatGroupVo {
    @ApiModelProperty("群组id")
    private Long id;
    @ApiModelProperty("商品id")
    private Long productId;
    @ApiModelProperty("服务类型id 1-全国散 2-一家一团 3-自营 4-全国散-小包团")
    private Long categoryId;
    @ApiModelProperty("群组名称")
    private String name;
    @ApiModelProperty("图片")
    private String image;
    @ApiModelProperty("创建日期")
    @JsonFormat(pattern = "MM-dd")
    private Date goDate;
    @ApiModelProperty("报名人数")
    private Integer signUpPersonNumber;
    @ApiModelProperty("限制人数")
    private Integer limitPersonNumber;
    @ApiModelProperty("报名人员头像url集合")
    private List<String> signUpPersonHeadPortraitUrlList;
    @ApiModelProperty("商品名称")
    private String productName;
}
