package com.ruoyi.service.mapper;

import java.util.Date;
import java.util.List;
import com.ruoyi.service.domain.SysUserVisitorRecord;
import com.ruoyi.service.vo.VisitorVo;
import org.apache.ibatis.annotations.Param;

/**
 * 访客记录Mapper接口
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public interface SysUserVisitorRecordMapper 
{
    /**
     * 查询访客记录
     * 
     * @param id 访客记录主键
     * @return 访客记录
     */
    public SysUserVisitorRecord selectSysUserVisitorRecordById(Long id);

    /**
     * 查询访客记录列表
     * 
     * @param sysUserVisitorRecord 访客记录
     * @return 访客记录集合
     */
    public List<SysUserVisitorRecord> selectSysUserVisitorRecordList(SysUserVisitorRecord sysUserVisitorRecord);

    /**
     * 新增访客记录
     * 
     * @param sysUserVisitorRecord 访客记录
     * @return 结果
     */
    public int insertSysUserVisitorRecord(SysUserVisitorRecord sysUserVisitorRecord);

    /**
     * 修改访客记录
     * 
     * @param sysUserVisitorRecord 访客记录
     * @return 结果
     */
    public int updateSysUserVisitorRecord(SysUserVisitorRecord sysUserVisitorRecord);

    /**
     * 删除访客记录
     * 
     * @param id 访客记录主键
     * @return 结果
     */
    public int deleteSysUserVisitorRecordById(Long id);

    /**
     * 批量删除访客记录
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysUserVisitorRecordByIds(Long[] ids);

    Integer selectTodayVisitorNumber(@Param("userId") Long userId, @Param("startDate") Date startDate, @Param("endDate") Date endDate);

    List<VisitorVo> visitorList(@Param("userId") Long userId);

    List<SysUserVisitorRecord> selectVisitorInfoToday(@Param("ownerUserId") Long ownerUserId, @Param("visitorUserId") Long visitorUserId, @Param("startDate") Date startDate, @Param("endDate") Date endDate);
}
