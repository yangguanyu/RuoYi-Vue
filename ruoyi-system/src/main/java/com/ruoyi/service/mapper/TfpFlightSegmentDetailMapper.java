package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.TfpFlightSegmentDetail;
import org.apache.ibatis.annotations.Param;

/**
 * 通程前后航段信息Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-10
 */
public interface TfpFlightSegmentDetailMapper 
{
    /**
     * 查询通程前后航段信息
     * 
     * @param id 通程前后航段信息主键
     * @return 通程前后航段信息
     */
    public TfpFlightSegmentDetail selectTfpFlightSegmentDetailById(Long id);

    /**
     * 查询通程前后航段信息列表
     * 
     * @param tfpFlightSegmentDetail 通程前后航段信息
     * @return 通程前后航段信息集合
     */
    public List<TfpFlightSegmentDetail> selectTfpFlightSegmentDetailList(TfpFlightSegmentDetail tfpFlightSegmentDetail);

    /**
     * 新增通程前后航段信息
     * 
     * @param tfpFlightSegmentDetail 通程前后航段信息
     * @return 结果
     */
    public int insertTfpFlightSegmentDetail(TfpFlightSegmentDetail tfpFlightSegmentDetail);

    /**
     * 修改通程前后航段信息
     * 
     * @param tfpFlightSegmentDetail 通程前后航段信息
     * @return 结果
     */
    public int updateTfpFlightSegmentDetail(TfpFlightSegmentDetail tfpFlightSegmentDetail);

    /**
     * 删除通程前后航段信息
     * 
     * @param id 通程前后航段信息主键
     * @return 结果
     */
    public int deleteTfpFlightSegmentDetailById(Long id);

    /**
     * 批量删除通程前后航段信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpFlightSegmentDetailByIds(Long[] ids);

    void deleteTfpFlightSegmentDetailByFlightId(@Param("flightId") Long flightId);
}
