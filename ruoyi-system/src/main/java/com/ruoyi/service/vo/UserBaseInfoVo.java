package com.ruoyi.service.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("UserBaseInfoVo")
public class UserBaseInfoVo {
    @ApiModelProperty("用户id")
    private Long userId;
    @ApiModelProperty("用户昵称")
    private String nickName;
    @ApiModelProperty("身份id")
    private Long identityId;
    @ApiModelProperty("身份名称")
    private String identityName;
    @ApiModelProperty("实名认证状态 0-未实名认证 1-已实名认证")
    private Integer realNameAuthStatus;
    @ApiModelProperty("用户ID-召唤使者id")
    private String uid;
    @ApiModelProperty("用户头像")
    private String userHeadImage;
}
