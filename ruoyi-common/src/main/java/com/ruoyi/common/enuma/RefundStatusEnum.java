package com.ruoyi.common.enuma;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RefundStatusEnum implements BaseEnum {

    REFUND_AUDITING(1, "退款审核中"),
    AUDIT_PASS_REFUNDING(2, "审核成功-退款中"),
    AUDIT_FAIL_REFUND_FAIL(3, "审核失败-退款失败"),
    REFUND_FAIL(4, "退款失败"),
    REFUND_SUCCESS(5, "退款成功")
    ;

    private Integer value;
    private String desc;
}
