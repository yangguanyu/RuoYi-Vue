package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Slf4j
@Data
@ApiModel(value = "WxSharingLinksDTO")
public class WxSharingLinksDTO implements Serializable, Cloneable {

    @NotNull(message = "scene不可为空！！")
    @ApiModelProperty(value = "最大32个可见字符，只支持数字，大小写英文以及部分特殊字符：!#$&'()*+,/:;=?@-._~，其它字符请自行编码为合法字符（因不支持%，中文无法使用 urlencode 处理，请使用其他编码方式）默认：inviteId=%s", required = true)
    private String scene;

    @ApiModelProperty(value = "必须是已经发布的小程序存在的页面（否则报错），例如 pages/index/index, 根路径前不要填加 /,不能携带参数（参数请放在scene字段里），如果不填写这个字段，默认 pages/homePage/homePage")
    private String page;

    @ApiModelProperty(value = "二维码的宽度，单位 px，最小 280px，最大 1280px 默认 430")
    private Integer width;

    @ApiModelProperty(value = "自动配置线条颜色，如果颜色依然是黑色，则说明不建议配置主色调，默认 false")
    private Boolean auto_color;

    @ApiModelProperty(value = "auto_color 为 false 时生效，使用 rgb 设置颜色 例如 {\"r\":\"xxx\",\"g\":\"xxx\",\"b\":\"xxx\"} 十进制表示 ")
    private String line_color;

    @ApiModelProperty(value = "是否需要透明底色，为 true 时，生成透明底色的小程序  默认 false")
    private Boolean is_hyaline;

    @ApiModelProperty(value = "要打开的小程序版本。正式版为 \"release\"，体验版为 \"trial\"，开发版为 \"develop\" open-gateway处理")
    private String env_version;

    @ApiModelProperty(value = "默认是true，检查page 是否存在，为 true 时 page 必须是已经发布的小程序存在的页面（否则报错）；为 false 时允许小程序未发布或者 page 不存在， 但page 有数量上限（60000个）请勿滥用。")
    private boolean check_path;
}
