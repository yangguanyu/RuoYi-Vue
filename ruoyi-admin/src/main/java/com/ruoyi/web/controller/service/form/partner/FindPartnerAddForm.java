package com.ruoyi.web.controller.service.form.partner;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.web.controller.service.form.attachment.AttachmentForm;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 找搭子对象 tfp_find_partner
 *
 * @author ruoyi
 * @date 2023-11-01
 */
@Data
@ApiModel("FindPartnerAddForm-找搭子添加搭子-小程序")
public class FindPartnerAddForm extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty(value = "主键")
    private Long id;

    /** 标题 */
    @ApiModelProperty(value = "标题")
    @Length(max = 50,message = "标题不能超过50个字")
    private String title;

    /** 描述 */
    @ApiModelProperty(value = "描述")
    private String activityDesc;

    /** 类型code */
    @ApiModelProperty(value = "类型code")
    private String typeCode;

    /** 类型名称 */
    @ApiModelProperty(value = "类型名称")
    private String typeName;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "开始时间")
    @NotNull(message = "开始时间不能为空")
    private Date startTime;

    /** 报名截止时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "报名截止时间")
    @NotNull(message = "报名截止时间不能为空")
    private Date signUpEndTime;

    /** 活动人数 */
    @ApiModelProperty(value = "活动人数")
    private Long activityPersonNumber;

    /** 活动费用 */
    @ApiModelProperty(value = "活动费用")
    private BigDecimal activityPrice;

    /** 活动地点 */
    @ApiModelProperty(value = "活动地点")
    private String activityPlace;

    @ApiModelProperty(value = "附件列表")
    @Valid
    @NotEmpty(message = "请上传图片")
    private List<AttachmentForm> attachmentList;

    @ApiModelProperty(value = "活动地点详情")
    private String activityPlaceDetail;

    @ApiModelProperty(value = "价格类型编码，免费，线下付，发起人代付，前端传给我字典的dictCode")
    private Long priceCode;

}
