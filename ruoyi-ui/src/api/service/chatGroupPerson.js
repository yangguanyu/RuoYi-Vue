import request from '@/utils/request'

// 查询聊天群组成员列表
export function listChatGroupPerson(query) {
  return request({
    url: '/service/chatGroupPerson/list',
    method: 'get',
    params: query
  })
}

// 查询聊天群组成员详细
export function getChatGroupPerson(id) {
  return request({
    url: '/service/chatGroupPerson/' + id,
    method: 'get'
  })
}

// 新增聊天群组成员
export function addChatGroupPerson(data) {
  return request({
    url: '/service/chatGroupPerson',
    method: 'post',
    data: data
  })
}

// 修改聊天群组成员
export function updateChatGroupPerson(data) {
  return request({
    url: '/service/chatGroupPerson',
    method: 'put',
    data: data
  })
}

// 删除聊天群组成员
export function delChatGroupPerson(id) {
  return request({
    url: '/service/chatGroupPerson/' + id,
    method: 'delete'
  })
}
