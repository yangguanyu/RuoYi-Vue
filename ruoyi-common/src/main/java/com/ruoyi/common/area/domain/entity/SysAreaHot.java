package com.ruoyi.common.area.domain.entity;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 地区热门推荐对象 sys_area_hot
 * 
 * @author ruoyi
 * @date 2023-10-17
 */
public class SysAreaHot extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 区域id */
    @Excel(name = "区域id")
    private Long areaId;

    /** 区域名称 */
    @Excel(name = "区域名称")
    private String areaName;

    /** 区域父级id */
    @Excel(name = "区域父级id")
    private Long parentAreaId;

    /** 区域父级名称 */
    @Excel(name = "区域父级名称")
    private String parentAreaName;

    /** 拼音 */
    @Excel(name = "拼音")
    private String pinyin;

    /** 国家类型 0-中国 1-其他 */
    @Excel(name = "国家类型 0-中国 1-其他")
    private Integer countryType;

    /** 排序 */
    @Excel(name = "排序")
    private Integer sort;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    @Excel(name = "创建者id")
    private Long createUserId;

    /** 更新者id */
    @Excel(name = "更新者id")
    private Long updateUserId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setAreaId(Long areaId) 
    {
        this.areaId = areaId;
    }

    public Long getAreaId() 
    {
        return areaId;
    }
    public void setAreaName(String areaName) 
    {
        this.areaName = areaName;
    }

    public String getAreaName() 
    {
        return areaName;
    }
    public void setParentAreaId(Long parentAreaId) 
    {
        this.parentAreaId = parentAreaId;
    }

    public Long getParentAreaId() 
    {
        return parentAreaId;
    }
    public void setParentAreaName(String parentAreaName) 
    {
        this.parentAreaName = parentAreaName;
    }

    public String getParentAreaName() 
    {
        return parentAreaName;
    }
    public void setPinyin(String pinyin) 
    {
        this.pinyin = pinyin;
    }

    public String getPinyin() 
    {
        return pinyin;
    }
    public void setCountryType(Integer countryType) 
    {
        this.countryType = countryType;
    }

    public Integer getCountryType() 
    {
        return countryType;
    }
    public void setSort(Integer sort)
    {
        this.sort = sort;
    }

    public Integer getSort()
    {
        return sort;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }
    public void setUpdateUserId(Long updateUserId) 
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId() 
    {
        return updateUserId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("areaId", getAreaId())
            .append("areaName", getAreaName())
            .append("parentAreaId", getParentAreaId())
            .append("parentAreaName", getParentAreaName())
            .append("pinyin", getPinyin())
            .append("countryType", getCountryType())
            .append("sort", getSort())
            .append("delFlag", getDelFlag())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
