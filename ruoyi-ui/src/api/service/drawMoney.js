import request from '@/utils/request'

// 提现详情
export function getDrawMoneyDetail(id) {
  return request({
    url: `/service/drawMoney/drawMoneyDetail?id=${id}`,
    method: 'post',
  })
}

// 查询提现审批列表
export function drawMoneyAuditList(query) {
  return request({
    url: '/service/drawMoney/drawMoneyAuditList',
    method: 'post',
    data: query
  })
}


// 提现审核
export function auditDrawMoney(query) {
  return request({
    url: '/service/drawMoney/auditDrawMoney',
    method: 'post',
    data: query
  })
}

// 佣金管理列表
export function userIncomeDetail(query) {
  return request({
    url: '/service/userIncomeDetail/userIncomeDetailManageList',
    method: 'post',
    data: query
  })
}


// 查询提款详细
export function getDrawMoney(id) {
  return request({
    url: '/service/drawMoney/' + id,
    method: 'get'
  })
}

// 新增提款
export function addDrawMoney(data) {
  return request({
    url: '/service/drawMoney',
    method: 'post',
    data: data
  })
}

// 修改提款
export function updateDrawMoney(data) {
  return request({
    url: '/service/drawMoney',
    method: 'put',
    data: data
  })
}

// 删除提款
export function delDrawMoney(id) {
  return request({
    url: '/service/drawMoney/' + id,
    method: 'delete'
  })
}
