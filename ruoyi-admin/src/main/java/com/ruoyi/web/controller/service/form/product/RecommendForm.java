package com.ruoyi.web.controller.service.form.product;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 商品主对象 tfp_product
 *
 * @author ruoyi
 * @date 2023-11-01
 */
@Data
@ApiModel("RecommendForm-热门推荐的form")
public class RecommendForm {

    /** 业务地区(1级) 能够选多个地区 下拉 */
    @ApiModelProperty(value = "业务地区(1级) 能够选多个地区 下拉")
    private String parentAreaName;

    /** 业务地区(2级) 能够选多个地区 下拉 */
    @ApiModelProperty(value = "业务地区(2级) 能够选多个地区 下拉")
    private String areaName;

    /** 服务类型id 散拼 单团 单项 下拉（怎么和咱们的全国散，一家一团对应） */
    @ApiModelProperty(value = "服务类型id 散拼 单团 单项 下拉")
    private Long categoryId;

    /** 服务类型名称 散拼 单团 单项 下拉（怎么和咱们的全国散，一家一团对应） */
    @ApiModelProperty(value = "服务类型名称 散拼 单团 单项 下拉")
    private String categoryName;

    /** 商品名称 */
    @ApiModelProperty(value = "商品名称")
    private String productName;

    /** 商品编号 */
    @ApiModelProperty(value = "商品编号")
    private String productNo;

    /** 供应商名称 */
    @ApiModelProperty(value = "供应商名称")
    private String supplierName;

    /** 供应商id */
    @ApiModelProperty(value = "供应商id")
    private Long supplierId;


}
