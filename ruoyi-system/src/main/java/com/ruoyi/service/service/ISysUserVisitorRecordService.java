package com.ruoyi.service.service;

import java.util.List;
import com.ruoyi.service.domain.SysUserVisitorRecord;
import com.ruoyi.service.vo.MyVisitorListVo;
import com.ruoyi.service.vo.VisitorVo;

/**
 * 访客记录Service接口
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public interface ISysUserVisitorRecordService 
{
    /**
     * 查询访客记录
     * 
     * @param id 访客记录主键
     * @return 访客记录
     */
    public SysUserVisitorRecord selectSysUserVisitorRecordById(Long id);

    /**
     * 查询访客记录列表
     * 
     * @param sysUserVisitorRecord 访客记录
     * @return 访客记录集合
     */
    public List<SysUserVisitorRecord> selectSysUserVisitorRecordList(SysUserVisitorRecord sysUserVisitorRecord);

    /**
     * 新增访客记录
     * 
     * @param sysUserVisitorRecord 访客记录
     * @return 结果
     */
    public int insertSysUserVisitorRecord(SysUserVisitorRecord sysUserVisitorRecord);

    /**
     * 修改访客记录
     * 
     * @param sysUserVisitorRecord 访客记录
     * @return 结果
     */
    public int updateSysUserVisitorRecord(SysUserVisitorRecord sysUserVisitorRecord);

    /**
     * 批量删除访客记录
     * 
     * @param ids 需要删除的访客记录主键集合
     * @return 结果
     */
    public int deleteSysUserVisitorRecordByIds(Long[] ids);

    /**
     * 删除访客记录信息
     * 
     * @param id 访客记录主键
     * @return 结果
     */
    public int deleteSysUserVisitorRecordById(Long id);

    /**
     * 今日访客个数
     * @param userId
     * @return
     */
    Integer selectTodayVisitorNumber(Long userId);

    /**
     * 小程序-我的访客列表
     * @return
     */
    List<VisitorVo> visitorList(Long userId);

    void logUserVisitorRecord(Long userId);
}
