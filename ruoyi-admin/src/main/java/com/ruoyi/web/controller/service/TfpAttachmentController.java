package com.ruoyi.web.controller.service;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.service.dto.SaveSystemImageDto;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.core.domain.entity.TfpAttachment;
import com.ruoyi.service.service.ITfpAttachmentService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 附件Controller
 *
 * @author ruoyi
 * @date 2023-12-25
 */
@RestController
@RequestMapping("/service/attachment")
public class TfpAttachmentController extends BaseController
{
    @Autowired
    private ITfpAttachmentService tfpAttachmentService;

    /**
     * 查询附件列表
     */
    @PreAuthorize("@ss.hasPermi('service:attachment:list')")
    @GetMapping("/list")
    public TableDataInfo list(TfpAttachment tfpAttachment)
    {
        startPage();
        List<TfpAttachment> list = tfpAttachmentService.selectTfpAttachmentList(tfpAttachment);
        return getDataTable(list);
    }

    /**
     * 导出附件列表
     */
    @PreAuthorize("@ss.hasPermi('service:attachment:export')")
    @Log(title = "附件", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TfpAttachment tfpAttachment)
    {
        List<TfpAttachment> list = tfpAttachmentService.selectTfpAttachmentList(tfpAttachment);
        ExcelUtil<TfpAttachment> util = new ExcelUtil<TfpAttachment>(TfpAttachment.class);
        util.exportExcel(response, list, "附件数据");
    }

    /**
     * 获取附件详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:attachment:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tfpAttachmentService.selectTfpAttachmentById(id));
    }

    /**
     * 新增附件
     */
    @PreAuthorize("@ss.hasPermi('service:attachment:add')")
    @Log(title = "附件", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TfpAttachment tfpAttachment)
    {
        return toAjax(tfpAttachmentService.insertTfpAttachment(tfpAttachment));
    }

    /**
     * 修改附件
     */
    @PreAuthorize("@ss.hasPermi('service:attachment:edit')")
    @Log(title = "附件", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TfpAttachment tfpAttachment)
    {
        return toAjax(tfpAttachmentService.updateTfpAttachment(tfpAttachment));
    }

    /**
     * 删除附件
     */
    @PreAuthorize("@ss.hasPermi('service:attachment:remove')")
    @Log(title = "附件", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tfpAttachmentService.deleteTfpAttachmentByIds(ids));
    }

    @ApiOperation("存储系统图片")
    @PostMapping("/saveSystemImage")
    @ResponseBody
    public R saveSystemImage(@RequestBody List<SaveSystemImageDto> saveSystemImageDtoList){
        return R.ok(tfpAttachmentService.saveSystemImage(saveSystemImageDtoList));
    }
}
