package com.ruoyi.service.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "MyVisitorListVo", description = "我的访客vo")
public class MyVisitorListVo {
    @ApiModelProperty("访问时间")
    private String visitDate;
    @ApiModelProperty("访客集合")
    private List<VisitorVo> visitorList;
}
