-- 1.航班信息表（Flight）:
CREATE TABLE `tfp_flight` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `flight_no` VARCHAR(10) COMMENT '航班号 MU5318',
  `dep_time` VARCHAR(50) COMMENT '起飞时间 yyyy-MM-dd hh:mm:ss',
  `dep_code` VARCHAR(10) COMMENT '起飞机场三字码',
  `arr_code` VARCHAR(10) COMMENT '到达机场',
  `arr_time` VARCHAR(50) COMMENT '抵达时间 yyyy-MM-dd hh:mm:ss',
  `dep_jetquay` VARCHAR(255) COMMENT '出发航站楼 T1',
  `arr_jetquay` VARCHAR(255) COMMENT '抵达航站楼 T1',
  `stop_num` varchar(10) COMMENT '经停次数 数字代表次数 0-直达 1-经停1次',
  `plane_type` VARCHAR(255) COMMENT '机型',
  `plane_size` TINYINT COMMENT '飞机大小 1-大 2-中 3-小',
  `plane_cn_name` VARCHAR(255) COMMENT '中文机型描述',
  `distance` VARCHAR(255) COMMENT '飞行里程 800',
  `base_prics` INT COMMENT '基础价格',
  `meal` VARCHAR(255) COMMENT '餐食标识',
  PRIMARY KEY (`id`)
) COMMENT='航班信息表';

-- 2.经停信息表（StopInfo）:
CREATE TABLE `tfp_flight_stop_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `flight_id` bigint(20) COMMENT '航班信息表id',
  `city` VARCHAR(10) COMMENT '城市三字码',
  `city_name` VARCHAR(255) COMMENT '城市名称',
  `city_type` VARCHAR(50) COMMENT '城市类型',
  `dep_time` VARCHAR(50) COMMENT '起飞时间',
  `arr_time` VARCHAR(50) COMMENT '降落时间',
  PRIMARY KEY (`id`)
) COMMENT='经停信息表';

-- 3.舱位信息表（Seat）:
CREATE TABLE `tfp_flight_seat` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `flight_id` bigint(20) COMMENT '航班信息表id',
  `seat_code` VARCHAR(10) COMMENT '舱位编码',
  `chd_seat_code` VARCHAR(10) COMMENT '儿童舱位编码',
  `seat_status` VARCHAR(10) COMMENT '舱位状态 0-9 或者 英文字母',
  `chd_seat_status` VARCHAR(10) COMMENT '儿童舱位状态 1-9代表剩余座位数，A代表座位数大于9',
  `inf_seat_code` VARCHAR(10) COMMENT '婴儿舱位编码',
  `inf_seat_status` VARCHAR(10) COMMENT '婴儿舱位状态 1-9代表剩余座位数，A代表座位数大于9',
  `cabin_class` TINYINT COMMENT '服务级别 1-头等舱 2-商务舱 3-经济舱 4-舒适A舱 5-超级经济舱 6-高端经济舱 7-尊享经济 8-超值经济 9-超值公务 10-超值头等 11-舒适经济 12-优惠商务 13-明珠经济 14-超值商务 15-空中商务舱 16-公务舱产品舱',
  `seat_msg` VARCHAR(255) COMMENT '舱位说明',
  `seat_type` TINYINT COMMENT '是否特价舱位 1-普通 3-特价',
  `discount` DOUBLE COMMENT '折扣',
  PRIMARY KEY (`id`)
) COMMENT='舱位信息表';


-- 4.通程舱位对应信息表（SeatDetail）:
CREATE TABLE `tfp_flight_seat_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `flight_id` bigint(20) COMMENT '航班信息表id',
  `seat_id` bigint(20) COMMENT '舱位信息表id',
  `dep_code` VARCHAR(10) COMMENT '出发三字码',
  `arr_code` VARCHAR(10) COMMENT '抵达三字码',
  `seat_code` VARCHAR(10) COMMENT '座位编码',
  `seat_class_status` VARCHAR(10) COMMENT '座位状态',
  `ticket_price` DOUBLE COMMENT '票价',
  PRIMARY KEY (`id`)
) COMMENT='通程舱位对应信息表';

-- 5.政策信息表（Policy）:
CREATE TABLE `tfp_flight_policy` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `flight_id` bigint(20) COMMENT '航班信息表id',
  `seat_id` bigint(20) COMMENT '舱位信息表id',
  `policy_id` VARCHAR(255) COMMENT '产品ID',
  `product_type` VARCHAR(255) COMMENT '产品类型 商旅优选：1、2、3、4、12、13、9、 低价推荐：41、42、43、44、45、26、 官网：21、22、23、24、25、6 生成订单需传入出生日期：4、41、42、43、44、45、21、22、23、24、25、6、9、26',
  `certificates` boolean COMMENT '证件限制 false-不限 true-有限制',
  `certificates_list` varchar(500) COMMENT '证件',
  `work_time` varchar(50) COMMENT '出票工作时间',
  `vt_work_time` varchar(50) COMMENT '退票工作时间',
  `comment` varchar(500) COMMENT '产品备注',
  `apply_passenger` TINYINT COMMENT '适用乘客类型 1-成人 2-儿童 3-成人+儿童 4-成人+婴儿 5-成人+儿童+婴儿',
  `voucher` TINYINT COMMENT '报销凭证类型 1-行程单 2-发票 3-电子发票 4-行程单+发票 5-不提供报销凭证',
  `latest_algorithm` TINYINT COMMENT '最晚出票时间计算方式 1-支付后 2-起飞前',
  `latest_ticket` INT COMMENT '最晚出票时间（分钟）',
  `age_limit` VARCHAR(50) COMMENT '产品年龄限制',
  `number_limit` VARCHAR(50) COMMENT '人数限制',
  `description` varchar(500) COMMENT '通程产品服务说明',
  PRIMARY KEY (`id`)
) COMMENT='政策信息表';

-- 6.价格数据表（PriceData）:
CREATE TABLE `tfp_flight_price_data` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `flight_id` bigint(20) COMMENT '航班信息表id',
  `seat_id` bigint(20) COMMENT '舱位信息表id',
  `policy_id` bigint(20) COMMENT '政策信息表id',
  `crew_type` VARCHAR(10) COMMENT '乘机人类型 1-成人 2-儿童 3-婴儿(暂不使用)',
  `price` INT COMMENT '票面价',
  `settlement` DOUBLE COMMENT '单人结算价 （price*（1- commisionPoint%）- commisionMoney）不含税',
  `airport_tax` INT COMMENT '单人机建费',
  `fuel_tax` INT COMMENT '单人燃油费',
  `commision_point` DOUBLE COMMENT '返点 0.1=0.1%',
  `commision_money` DOUBLE COMMENT '定额',
  `pay_fee` DOUBLE COMMENT '支付手续费',
  `is_switch_pnr` TINYINT COMMENT '是否换编码出票 1-原编码出票 2-需要换编码出票',
  `policy_type` VARCHAR(50) COMMENT '政策类型（BSP或B2B）',
  `ticket_speed` VARCHAR(50) COMMENT '出票效率',
  PRIMARY KEY (`id`)
) COMMENT='价格数据表';

-- 7.通程前后航段信息表（SegmentDetail）:
CREATE TABLE `tfp_flight_segment_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `flight_id` bigint(20) COMMENT '航班信息表id',
  `flight_no` VARCHAR(50) COMMENT '航班号',
  `dep_code` VARCHAR(10) COMMENT '出发城市三字码',
  `dep_city` VARCHAR(255) COMMENT '起飞城市名称',
  `dep_airport` VARCHAR(255) COMMENT '出发机场名称',
  `arr_code` VARCHAR(10) COMMENT '抵达城市三字码',
  `arr_city` VARCHAR(255) COMMENT '抵达城市名称',
  `arr_airport` VARCHAR(255) COMMENT '抵达机场名称',
  `segment` VARCHAR(10) COMMENT '航段',
  `dep_date` VARCHAR(50) COMMENT '出发日期',
  `dep_time` VARCHAR(50) COMMENT '出发时间',
  `arr_date` VARCHAR(50) COMMENT '抵达日期',
  `arr_time` VARCHAR(50) COMMENT '抵达时间',
  `carrier` VARCHAR(255) COMMENT '航司',
  `dep_term` VARCHAR(255) COMMENT '出发航站',
  `arr_term` VARCHAR(255) COMMENT '抵达航站',
  PRIMARY KEY (`id`)
) COMMENT='通程前后航段信息表';

CREATE TABLE `tfp_flight_date_price` (
 `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
 `price` decimal(10,0) DEFAULT NULL COMMENT '价格(根据规则获取)',
 `departure_time` varchar(20) DEFAULT NULL COMMENT '航班日期(yyyy-MM-dd)',
 `departure_airport_code` varchar(5) DEFAULT NULL COMMENT '起飞城市三字码',
 `departure_airport_name` varchar(50) DEFAULT NULL COMMENT '起飞城市名称',
 `arrival_airport_code` varchar(5) DEFAULT NULL COMMENT '到达城市三字码',
 `arrival_airport_name` varchar(50) DEFAULT NULL COMMENT '到达城市名称',
 `flight_id` bigint(20) DEFAULT NULL COMMENT '主键'
) COMMENT='航班日期价格';
    
-- tfp_flight
ALTER TABLE tfp_flight
	ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';

-- tfp_flight_stop_info
ALTER TABLE tfp_flight_stop_info
	ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';
	
-- tfp_flight_seat_detail
ALTER TABLE tfp_flight_seat_detail
	ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';
	
-- tfp_flight_seat
ALTER TABLE tfp_flight_seat
	ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';
	
-- tfp_flight_policy
ALTER TABLE tfp_flight_policy
	ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';
	
-- tfp_flight_price_data
ALTER TABLE tfp_flight_price_data
	ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';
	
-- tfp_flight_segment_detail
ALTER TABLE tfp_flight_segment_detail
	ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';

-- tfp_flight_date_price
ALTER TABLE tfp_flight_date_price
    ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';