package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "ApplyRefundDto", description = "申请退款dto")
public class ApplyRefundDto {
    @ApiModelProperty("订单id")
    private Long orderId;
    @ApiModelProperty("申请退款原因")
    private String reason;
    @ApiModelProperty("申请退款说明")
    private String illustrate;
}
