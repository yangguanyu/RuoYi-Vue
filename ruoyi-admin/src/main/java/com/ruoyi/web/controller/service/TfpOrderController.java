package com.ruoyi.web.controller.service;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.service.dto.*;
import com.ruoyi.service.excel.OrderListExportExcel;
import com.ruoyi.service.vo.*;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.TfpOrder;
import com.ruoyi.service.service.ITfpOrderService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 订单Controller
 *
 * @author ruoyi
 * @date 2023-10-31
 */
@RestController
@RequestMapping("/service/order")
@ApiOperation("订单")
public class TfpOrderController extends BaseController
{
    @Autowired
    private ITfpOrderService tfpOrderService;

    /**
     * 查询订单列表
     */
    @PreAuthorize("@ss.hasPermi('service:order:list')")
    @GetMapping("/list")
    public TableDataInfo list(TfpOrderDto tfpOrderDto)
    {
        startPage();
        List<TfpOrderVo> list = tfpOrderService.selectTfpOrderVoList(tfpOrderDto);
        return getDataTable(list);
    }

    /**
     * 导出订单列表
     */
    @PreAuthorize("@ss.hasPermi('service:order:export')")
    @Log(title = "订单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TfpOrder tfpOrder)
    {
        List<TfpOrder> list = tfpOrderService.selectTfpOrderList(tfpOrder);
        ExcelUtil<TfpOrder> util = new ExcelUtil<TfpOrder>(TfpOrder.class);
        util.exportExcel(response, list, "订单数据");
    }

    /**
     * 获取订单详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:order:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tfpOrderService.selectTfpOrderById(id));
    }

    /**
     * 新增订单
     */
    @PreAuthorize("@ss.hasPermi('service:order:add')")
    @Log(title = "订单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TfpOrder tfpOrder)
    {

        return toAjax(tfpOrderService.insertTfpOrder(tfpOrder));
    }

    /**
     * 修改订单
     */
    @PreAuthorize("@ss.hasPermi('service:order:edit')")
    @Log(title = "订单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TfpOrder tfpOrder)
    {
        return toAjax(tfpOrderService.updateTfpOrder(tfpOrder));
    }

    /**
     * 删除订单
     */
    @PreAuthorize("@ss.hasPermi('service:order:remove')")
    @Log(title = "订单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tfpOrderService.deleteTfpOrderByIds(ids));
    }

    @PreAuthorize("@ss.hasPermi('service:order:list')")
    @PostMapping("/supplySelect")
    public R<List<SupplierVo>> supplySelect()
    {
        return R.ok(tfpOrderService.supplySelect());
    }


    @ApiOperation("小程序-创建订单详情")
    @PostMapping("/createOrderDetail")
    @ResponseBody
    public R<CreateOrderDetailVo> createOrderDetail(@RequestBody CreateOrderDetailDto createOrderDetailDto){
        return R.ok(tfpOrderService.createOrderDetail(createOrderDetailDto));
    }

    @ApiOperation("小程序-创建订单")
    @PostMapping("/createOrder")
    @ResponseBody
    public R<CreateOrderVo> createOrder(@RequestBody CreateOrderDto createOrderDto){
        return R.ok(tfpOrderService.createOrder(createOrderDto));
    }

    @ApiOperation("小程序-取消订单")
    @PostMapping("/cancelOrder")
    @ResponseBody
    public R cancelOrder(@RequestParam("orderId") Long orderId){
        return R.ok(tfpOrderService.cancelOrder(orderId));
    }

    @ApiOperation("小程序-我的订单集合")
    @PostMapping("/myOrderList")
    @ResponseBody
    public TableDataInfo<MyOrderListVo> myOrderList(@RequestBody MyOrderListDto myOrderListDto){
        return getDataTable(tfpOrderService.myOrderList(myOrderListDto));
    }

    @ApiOperation("小程序-订单详情")
    @PostMapping("/orderDetail")
    @ResponseBody
    public R<OrderDetailVo> orderDetail(@RequestParam("orderId") Long orderId){
        return R.ok(tfpOrderService.orderDetail(orderId));
    }

    @ApiOperation("小程序-测试支付成功后业务处理")
    @PostMapping("/testPaySuccess")
    @ResponseBody
    public R testPaySuccess(@RequestParam("orderNo") String orderNo){
        return R.ok(tfpOrderService.handlePaySuccessAfterInfo(orderNo));
    }

    @ApiOperation("小程序-价格明细")
    @PostMapping("/priceDetail")
    @ResponseBody
    public R<PriceDetailVo> priceDetail(@RequestBody PriceDetailDto priceDetailDto){
        return R.ok(tfpOrderService.priceDetail(priceDetailDto));
    }

    @ApiOperation("小程序-获取前端调起支付参数")
    @PostMapping("/getRequestPayment")
    @ResponseBody
    public R<RequestPaymentVo> getRequestPayment(@RequestParam("orderId") Long orderId){
        return R.ok(tfpOrderService.getRequestPayment(orderId));
    }

    @ApiOperation("小程序-完成出行")
    @PostMapping("/completeTravel")
    @ResponseBody
    public R completeTravel(@RequestParam("id") Long id){
        return R.ok(tfpOrderService.completeTravel(id, null));
    }

    @ApiOperation("后台-订单列表")
    @PostMapping("/orderList")
    @ResponseBody
    public TableDataInfo<OrderListVo> orderList(@RequestBody OrderListDto orderListDto){
        return getDataTable(tfpOrderService.orderList(orderListDto));
    }

    @ApiOperation("后台-确认支付订单")
    @PostMapping("/confirmPayOrder")
    @ResponseBody
    public R confirmPayOrder(@RequestBody ConfirmPayOrderDto confirmPayOrderDto){
        return R.ok(tfpOrderService.confirmPayOrder(confirmPayOrderDto));
    }

    @ApiOperation("后台-订单列表导出")
    @PostMapping("/orderListExport")
    public void orderListExport(HttpServletResponse response, ExportDto exportDto)
    {
        List<OrderListExportExcel> list = tfpOrderService.orderListExport(exportDto);
        ExcelUtil<OrderListExportExcel> util = new ExcelUtil<OrderListExportExcel>(OrderListExportExcel.class);
        util.exportExcel(response, list, "订单列表数据");
    }

    @ApiOperation("后台-订单详情")
    @PostMapping("/orderDetailInfo")
    @ResponseBody
    public R<OrderDetailInfoVo> orderDetailInfo(@RequestParam("id") Long id){
        return R.ok(tfpOrderService.orderDetailInfo(id));
    }
}
