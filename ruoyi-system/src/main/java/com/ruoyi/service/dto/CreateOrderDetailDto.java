package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel(value = "CreateOrderDetailDto", description = "创建订单详情dto")
public class CreateOrderDetailDto {
    @ApiModelProperty("商品id")
    private Long productId;
    @ApiModelProperty("聊天群id")
    private Long chatGroupId;
    @ApiModelProperty("出行方式 0-组队出行 1-独立出行")
    private Integer travelType;
    @ApiModelProperty("出发时间")
    private Date goDate;
    @ApiModelProperty("出发地名称")
    private String goPlaceName;
    @ApiModelProperty("是否选择低价机票 0-否 1-是")
    private Integer lowPriceAirTicketFlag;
}
