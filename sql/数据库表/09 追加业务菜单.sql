-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('银行', '5', '1', 'bank', 'service/bank/index', 1, 0, 'C', '0', '0', 'service:bank:list', '#', 'admin', sysdate(), '', null, '银行菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('银行查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'service:bank:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('银行新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'service:bank:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('银行修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'service:bank:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('银行删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'service:bank:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('银行导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'service:bank:export',       '#', 'admin', sysdate(), '', null, '');
-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('聊天群组', '5', '1', 'chatGroup', 'service/chatGroup/index', 1, 0, 'C', '0', '0', 'service:chatGroup:list', '#', 'admin', sysdate(), '', null, '聊天群组菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('聊天群组查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'service:chatGroup:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('聊天群组新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'service:chatGroup:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('聊天群组修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'service:chatGroup:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('聊天群组删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'service:chatGroup:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('聊天群组导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'service:chatGroup:export',       '#', 'admin', sysdate(), '', null, '');
-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('聊天群组成员聊天信息', '5', '1', 'chatGroupPersonInfo', 'service/chatGroupPersonInfo/index', 1, 0, 'C', '0', '0', 'service:chatGroupPersonInfo:list', '#', 'admin', sysdate(), '', null, '聊天群组成员聊天信息菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('聊天群组成员聊天信息查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'service:chatGroupPersonInfo:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('聊天群组成员聊天信息新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'service:chatGroupPersonInfo:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('聊天群组成员聊天信息修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'service:chatGroupPersonInfo:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('聊天群组成员聊天信息删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'service:chatGroupPersonInfo:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('聊天群组成员聊天信息导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'service:chatGroupPersonInfo:export',       '#', 'admin', sysdate(), '', null, '');
-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('聊天群组成员', '5', '1', 'chatGroupPerson', 'service/chatGroupPerson/index', 1, 0, 'C', '0', '0', 'service:chatGroupPerson:list', '#', 'admin', sysdate(), '', null, '聊天群组成员菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('聊天群组成员查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'service:chatGroupPerson:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('聊天群组成员新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'service:chatGroupPerson:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('聊天群组成员修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'service:chatGroupPerson:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('聊天群组成员删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'service:chatGroupPerson:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('聊天群组成员导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'service:chatGroupPerson:export',       '#', 'admin', sysdate(), '', null, '');
-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('同行人-独立出行', '5', '1', 'comate', 'service/comate/index', 1, 0, 'C', '0', '0', 'service:comate:list', '#', 'admin', sysdate(), '', null, '同行人-独立出行菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('同行人-独立出行查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'service:comate:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('同行人-独立出行新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'service:comate:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('同行人-独立出行修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'service:comate:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('同行人-独立出行删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'service:comate:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('同行人-独立出行导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'service:comate:export',       '#', 'admin', sysdate(), '', null, '');
-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('同行人和订单关联', '5', '1', 'comateOrder', 'service/comateOrder/index', 1, 0, 'C', '0', '0', 'service:comateOrder:list', '#', 'admin', sysdate(), '', null, '同行人和订单关联菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('同行人和订单关联查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'service:comateOrder:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('同行人和订单关联新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'service:comateOrder:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('同行人和订单关联修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'service:comateOrder:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('同行人和订单关联删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'service:comateOrder:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('同行人和订单关联导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'service:comateOrder:export',       '#', 'admin', sysdate(), '', null, '');
-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('提款', '5', '1', 'drawMoney', 'service/drawMoney/index', 1, 0, 'C', '0', '0', 'service:drawMoney:list', '#', 'admin', sysdate(), '', null, '提款菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('提款查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'service:drawMoney:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('提款新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'service:drawMoney:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('提款修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'service:drawMoney:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('提款删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'service:drawMoney:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('提款导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'service:drawMoney:export',       '#', 'admin', sysdate(), '', null, '');
-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('每日价格', '5', '1', 'everyDayPrice', 'service/everyDayPrice/index', 1, 0, 'C', '0', '0', 'service:everyDayPrice:list', '#', 'admin', sysdate(), '', null, '每日价格菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('每日价格查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'service:everyDayPrice:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('每日价格新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'service:everyDayPrice:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('每日价格修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'service:everyDayPrice:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('每日价格删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'service:everyDayPrice:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('每日价格导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'service:everyDayPrice:export',       '#', 'admin', sysdate(), '', null, '');
-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('找搭子聊天群组', '5', '1', 'findPartnerChatGroup', 'service/findPartnerChatGroup/index', 1, 0, 'C', '0', '0', 'service:findPartnerChatGroup:list', '#', 'admin', sysdate(), '', null, '找搭子聊天群组菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('找搭子聊天群组查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'service:findPartnerChatGroup:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('找搭子聊天群组新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'service:findPartnerChatGroup:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('找搭子聊天群组修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'service:findPartnerChatGroup:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('找搭子聊天群组删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'service:findPartnerChatGroup:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('找搭子聊天群组导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'service:findPartnerChatGroup:export',       '#', 'admin', sysdate(), '', null, '');
-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('找搭子聊天群组成员聊天信息', '5', '1', 'findPartnerChatGroupPersonInfo', 'service/findPartnerChatGroupPersonInfo/index', 1, 0, 'C', '0', '0', 'service:findPartnerChatGroupPersonInfo:list', '#', 'admin', sysdate(), '', null, '找搭子聊天群组成员聊天信息菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('找搭子聊天群组成员聊天信息查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'service:findPartnerChatGroupPersonInfo:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('找搭子聊天群组成员聊天信息新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'service:findPartnerChatGroupPersonInfo:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('找搭子聊天群组成员聊天信息修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'service:findPartnerChatGroupPersonInfo:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('找搭子聊天群组成员聊天信息删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'service:findPartnerChatGroupPersonInfo:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('找搭子聊天群组成员聊天信息导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'service:findPartnerChatGroupPersonInfo:export',       '#', 'admin', sysdate(), '', null, '');
-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('找搭子聊天群组成员', '5', '1', 'findPartnerChatGroupPerson', 'service/findPartnerChatGroupPerson/index', 1, 0, 'C', '0', '0', 'service:findPartnerChatGroupPerson:list', '#', 'admin', sysdate(), '', null, '找搭子聊天群组成员菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('找搭子聊天群组成员查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'service:findPartnerChatGroupPerson:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('找搭子聊天群组成员新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'service:findPartnerChatGroupPerson:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('找搭子聊天群组成员修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'service:findPartnerChatGroupPerson:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('找搭子聊天群组成员删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'service:findPartnerChatGroupPerson:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('找搭子聊天群组成员导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'service:findPartnerChatGroupPerson:export',       '#', 'admin', sysdate(), '', null, '');
-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('找搭子', '5', '1', 'findPartner', 'service/findPartner/index', 1, 0, 'C', '0', '0', 'service:findPartner:list', '#', 'admin', sysdate(), '', null, '找搭子菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('找搭子查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'service:findPartner:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('找搭子新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'service:findPartner:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('找搭子修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'service:findPartner:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('找搭子删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'service:findPartner:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('找搭子导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'service:findPartner:export',       '#', 'admin', sysdate(), '', null, '');
-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('找搭子报名', '5', '1', 'findPartnerSignUp', 'service/findPartnerSignUp/index', 1, 0, 'C', '0', '0', 'service:findPartnerSignUp:list', '#', 'admin', sysdate(), '', null, '找搭子报名菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('找搭子报名查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'service:findPartnerSignUp:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('找搭子报名新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'service:findPartnerSignUp:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('找搭子报名修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'service:findPartnerSignUp:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('找搭子报名删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'service:findPartnerSignUp:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('找搭子报名导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'service:findPartnerSignUp:export',       '#', 'admin', sysdate(), '', null, '');
-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户身份', '5', '1', 'identity', 'service/identity/index', 1, 0, 'C', '0', '0', 'service:identity:list', '#', 'admin', sysdate(), '', null, '用户身份菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户身份查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'service:identity:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户身份新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'service:identity:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户身份修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'service:identity:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户身份删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'service:identity:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户身份导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'service:identity:export',       '#', 'admin', sysdate(), '', null, '');
-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('标签', '5', '1', 'label', 'service/label/index', 1, 0, 'C', '0', '0', 'service:label:list', '#', 'admin', sysdate(), '', null, '标签菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('标签查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'service:label:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('标签新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'service:label:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('标签修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'service:label:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('标签删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'service:label:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('标签导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'service:label:export',       '#', 'admin', sysdate(), '', null, '');
-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('订单', '5', '1', 'order', 'service/order/index', 1, 0, 'C', '0', '0', 'service:order:list', '#', 'admin', sysdate(), '', null, '订单菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('订单查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'service:order:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('订单新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'service:order:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('订单修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'service:order:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('订单删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'service:order:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('订单导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'service:order:export',       '#', 'admin', sysdate(), '', null, '');
-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('商品主', '5', '1', 'product', 'service/product/index', 1, 0, 'C', '0', '0', 'service:product:list', '#', 'admin', sysdate(), '', null, '商品主菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('商品主查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'service:product:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('商品主新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'service:product:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('商品主修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'service:product:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('商品主删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'service:product:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('商品主导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'service:product:export',       '#', 'admin', sysdate(), '', null, '');
-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('商品快照', '5', '1', 'productSnapshot', 'service/productSnapshot/index', 1, 0, 'C', '0', '0', 'service:productSnapshot:list', '#', 'admin', sysdate(), '', null, '商品快照菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('商品快照查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'service:productSnapshot:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('商品快照新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'service:productSnapshot:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('商品快照修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'service:productSnapshot:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('商品快照删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'service:productSnapshot:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('商品快照导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'service:productSnapshot:export',       '#', 'admin', sysdate(), '', null, '');
-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('商品辅', '5', '1', 'productSub', 'service/productSub/index', 1, 0, 'C', '0', '0', 'service:productSub:list', '#', 'admin', sysdate(), '', null, '商品辅菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('商品辅查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'service:productSub:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('商品辅新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'service:productSub:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('商品辅修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'service:productSub:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('商品辅删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'service:productSub:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('商品辅导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'service:productSub:export',       '#', 'admin', sysdate(), '', null, '');
-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('购物店数量', '5', '1', 'purchaseShopNumber', 'service/purchaseShopNumber/index', 1, 0, 'C', '0', '0', 'service:purchaseShopNumber:list', '#', 'admin', sysdate(), '', null, '购物店数量菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('购物店数量查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'service:purchaseShopNumber:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('购物店数量新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'service:purchaseShopNumber:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('购物店数量修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'service:purchaseShopNumber:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('购物店数量删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'service:purchaseShopNumber:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('购物店数量导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'service:purchaseShopNumber:export',       '#', 'admin', sysdate(), '', null, '');
-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('退款', '5', '1', 'refund', 'service/refund/index', 1, 0, 'C', '0', '0', 'service:refund:list', '#', 'admin', sysdate(), '', null, '退款菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('退款查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'service:refund:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('退款新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'service:refund:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('退款修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'service:refund:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('退款删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'service:refund:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('退款导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'service:refund:export',       '#', 'admin', sysdate(), '', null, '');
-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('自费项目', '5', '1', 'selfFundedProject', 'service/selfFundedProject/index', 1, 0, 'C', '0', '0', 'service:selfFundedProject:list', '#', 'admin', sysdate(), '', null, '自费项目菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('自费项目查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'service:selfFundedProject:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('自费项目新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'service:selfFundedProject:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('自费项目修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'service:selfFundedProject:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('自费项目删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'service:selfFundedProject:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('自费项目导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'service:selfFundedProject:export',       '#', 'admin', sysdate(), '', null, '');
-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('供应商信息', '5', '1', 'supplier', 'service/supplier/index', 1, 0, 'C', '0', '0', 'service:supplier:list', '#', 'admin', sysdate(), '', null, '供应商信息菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('供应商信息查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'service:supplier:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('供应商信息新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'service:supplier:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('供应商信息修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'service:supplier:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('供应商信息删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'service:supplier:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('供应商信息导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'service:supplier:export',       '#', 'admin', sysdate(), '', null, '');
-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户银行卡', '5', '1', 'userBankCard', 'service/userBankCard/index', 1, 0, 'C', '0', '0', 'service:userBankCard:list', '#', 'admin', sysdate(), '', null, '用户银行卡菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户银行卡查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'service:userBankCard:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户银行卡新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'service:userBankCard:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户银行卡修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'service:userBankCard:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户银行卡删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'service:userBankCard:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户银行卡导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'service:userBankCard:export',       '#', 'admin', sysdate(), '', null, '');
-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('关注', '5', '1', 'userCare', 'service/userCare/index', 1, 0, 'C', '0', '0', 'service:userCare:list', '#', 'admin', sysdate(), '', null, '关注菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('关注查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'service:userCare:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('关注新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'service:userCare:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('关注修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'service:userCare:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('关注删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'service:userCare:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('关注导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'service:userCare:export',       '#', 'admin', sysdate(), '', null, '');
-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户图片', '5', '1', 'userIcon', 'service/userIcon/index', 1, 0, 'C', '0', '0', 'service:userIcon:list', '#', 'admin', sysdate(), '', null, '用户图片菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户图片查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'service:userIcon:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户图片新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'service:userIcon:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户图片修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'service:userIcon:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户图片删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'service:userIcon:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户图片导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'service:userIcon:export',       '#', 'admin', sysdate(), '', null, '');
-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户收益', '5', '1', 'userIncomeDetail', 'service/userIncomeDetail/index', 1, 0, 'C', '0', '0', 'service:userIncomeDetail:list', '#', 'admin', sysdate(), '', null, '用户收益菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户收益查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'service:userIncomeDetail:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户收益新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'service:userIncomeDetail:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户收益修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'service:userIncomeDetail:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户收益删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'service:userIncomeDetail:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户收益导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'service:userIncomeDetail:export',       '#', 'admin', sysdate(), '', null, '');
-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户收益', '5', '1', 'userIncome', 'service/userIncome/index', 1, 0, 'C', '0', '0', 'service:userIncome:list', '#', 'admin', sysdate(), '', null, '用户收益菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户收益查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'service:userIncome:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户收益新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'service:userIncome:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户收益修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'service:userIncome:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户收益删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'service:userIncome:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户收益导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'service:userIncome:export',       '#', 'admin', sysdate(), '', null, '');
-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户个性标签', '5', '1', 'userLabel', 'service/userLabel/index', 1, 0, 'C', '0', '0', 'service:userLabel:list', '#', 'admin', sysdate(), '', null, '用户个性标签菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户个性标签查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'service:userLabel:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户个性标签新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'service:userLabel:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户个性标签修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'service:userLabel:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户个性标签删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'service:userLabel:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户个性标签导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'service:userLabel:export',       '#', 'admin', sysdate(), '', null, '');
-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('访客记录', '5', '1', 'userVisitorRecord', 'service/userVisitorRecord/index', 1, 0, 'C', '0', '0', 'service:userVisitorRecord:list', '#', 'admin', sysdate(), '', null, '访客记录菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('访客记录查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'service:userVisitorRecord:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('访客记录新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'service:userVisitorRecord:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('访客记录修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'service:userVisitorRecord:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('访客记录删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'service:userVisitorRecord:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('访客记录导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'service:userVisitorRecord:export',       '#', 'admin', sysdate(), '', null, '');
