package com.ruoyi.service.vo;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 通程舱位对应信息对象 tfp_flight_seat_detail
 * 
 * @author ruoyi
 * @date 2024-04-10
 */
@Data
public class TfpFlightSeatDetailVo extends BaseEntity
{
    /** 主键 */
    private Long id;

    /** 航班信息表id */
    private Long flightId;

    /** 舱位信息表id */
    private Long seatId;

    /** 出发三字码 */
    private String depCode;

    /** 抵达三字码 */
    private String arrCode;

    /** 座位编码 */
    private String seatCode;

    /** 座位状态 */
    private String seatClassStatus;

    /** 票价 */
    private Double ticketPrice;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    private Long createUserId;

    /** 更新者id */
    private Long updateUserId;


}
