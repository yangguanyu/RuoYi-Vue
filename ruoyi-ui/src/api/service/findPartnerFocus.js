import request from '@/utils/request'

// 查询找搭子关注列表
export function listFindPartnerFocus(query) {
  return request({
    url: '/service/findPartnerFocus/list',
    method: 'get',
    params: query
  })
}

// 查询找搭子关注详细
export function getFindPartnerFocus(id) {
  return request({
    url: '/service/findPartnerFocus/' + id,
    method: 'get'
  })
}

// 新增找搭子关注
export function addFindPartnerFocus(data) {
  return request({
    url: '/service/findPartnerFocus',
    method: 'post',
    data: data
  })
}

// 修改找搭子关注
export function updateFindPartnerFocus(data) {
  return request({
    url: '/service/findPartnerFocus',
    method: 'put',
    data: data
  })
}

// 删除找搭子关注
export function delFindPartnerFocus(id) {
  return request({
    url: '/service/findPartnerFocus/' + id,
    method: 'delete'
  })
}
