package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "ListTravelRuleDto")
public class ListTravelRuleDto {
    @ApiModelProperty("规则类型 0-组队出行 1-独立出行")
    private Integer travelType;
    @ApiModelProperty("版本号")
    private String version;
    @ApiModelProperty("类型 0-旅行方式")
    private Integer type;
}
