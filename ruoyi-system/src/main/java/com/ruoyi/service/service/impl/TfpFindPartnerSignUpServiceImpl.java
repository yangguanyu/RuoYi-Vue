package com.ruoyi.service.service.impl;

import java.util.*;
import java.util.stream.Collectors;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enuma.*;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.DcListUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.core.domain.entity.TfpAttachment;
import com.ruoyi.service.domain.TfpFindPartner;
import com.ruoyi.service.domain.TfpFindPartnerChatGroup;
import com.ruoyi.service.domain.TfpFindPartnerChatGroupPerson;
import com.ruoyi.service.dto.ListFindPartnerSignUpDto;
import com.ruoyi.service.service.*;
import com.ruoyi.service.vo.*;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpFindPartnerSignUpMapper;
import com.ruoyi.service.domain.TfpFindPartnerSignUp;
import org.springframework.transaction.annotation.Transactional;

import static com.ruoyi.common.utils.PageUtils.startPage;

/**
 * 找搭子Service业务层处理
 *
 * @author ruoyi
 * @date 2023-10-31
 */
@Service
@Transactional
public class TfpFindPartnerSignUpServiceImpl implements ITfpFindPartnerSignUpService
{
    @Autowired
    private TfpFindPartnerSignUpMapper tfpFindPartnerSignUpMapper;
    @Autowired
    private ITfpFindPartnerService findPartnerService;
    @Autowired
    private ISysUserService userService;
    @Autowired
    private ITfpAttachmentService attachmentService;
    @Autowired
    private ITfpFindPartnerChatGroupPersonService findPartnerChatGroupPersonService;
    @Autowired
    private ITfpFindPartnerChatGroupService findPartnerChatGroupService;

    /**
     * 查询找搭子
     *
     * @param id 找搭子主键
     * @return 找搭子
     */
    @Override
    public TfpFindPartnerSignUp selectTfpFindPartnerSignUpById(Long id)
    {
        return tfpFindPartnerSignUpMapper.selectTfpFindPartnerSignUpById(id);
    }

    /**
     * 查询找搭子列表
     *
     * @param tfpFindPartnerSignUp 找搭子
     * @return 找搭子
     */
    @Override
    public List<TfpFindPartnerSignUp> selectTfpFindPartnerSignUpList(TfpFindPartnerSignUp tfpFindPartnerSignUp)
    {
        return tfpFindPartnerSignUpMapper.selectTfpFindPartnerSignUpList(tfpFindPartnerSignUp);
    }

    @Override
    public List<TfpFindPartnerSignUpVo> selectTfpFindPartnerSignUpVoList(TfpFindPartnerSignUp tfpFindPartnerSignUp)
    {
        List<TfpFindPartnerSignUpVo> list = new ArrayList<>();
        List<TfpFindPartnerSignUp> tfpFindPartnerSignUpList = tfpFindPartnerSignUpMapper.selectTfpFindPartnerSignUpList(tfpFindPartnerSignUp);
        if (tfpFindPartnerSignUpList != null && tfpFindPartnerSignUpList.size() > 0){
            List<Long> signUpUserIdList = new ArrayList<>();
            List<Long> findPartnerIdList = new ArrayList<>();
            for (TfpFindPartnerSignUp findPartnerSignUp : tfpFindPartnerSignUpList) {
                Long signUpUserId = findPartnerSignUp.getSignUpUserId();
                if (!signUpUserIdList.contains(signUpUserId)){
                    signUpUserIdList.add(signUpUserId);
                }

                Long findPartnerId = findPartnerSignUp.getFindPartnerId();
                if (!findPartnerIdList.contains(findPartnerId)){
                    findPartnerIdList.add(findPartnerId);
                }
            }

            Map<Long, String> titleMap = new HashMap<>();
            if (findPartnerIdList.size() > 0){
                List<TfpFindPartner> findPartnerList = findPartnerService.selectTfpFindPartnerListByIdList(findPartnerIdList);
                if (findPartnerList != null && findPartnerList.size() > 0){
                    titleMap = findPartnerList.stream().distinct().collect(Collectors.toMap(TfpFindPartner::getId, TfpFindPartner::getTitle));
                }
            }

            Map<Long, String> nickNameMap = userService.getNickNameMap(signUpUserIdList);

            for (TfpFindPartnerSignUp findPartnerSignUp : tfpFindPartnerSignUpList) {
                TfpFindPartnerSignUpVo findPartnerSignUpVo = new TfpFindPartnerSignUpVo();
                BeanUtils.copyProperties(findPartnerSignUp, findPartnerSignUpVo);
                findPartnerSignUpVo.setTitle(titleMap.get(findPartnerSignUp.getFindPartnerId()));
                findPartnerSignUpVo.setSignUpNickName(nickNameMap.get(findPartnerSignUp.getSignUpUserId()));
                list.add(findPartnerSignUpVo);
            }
        }

        return list;
    }

    /**
     * 新增找搭子
     *
     * @param tfpFindPartnerSignUp 找搭子
     * @return 结果
     */
    @Override
    public int insertTfpFindPartnerSignUp(TfpFindPartnerSignUp tfpFindPartnerSignUp)
    {
        tfpFindPartnerSignUp.setCreateTime(DateUtils.getNowDate());
        return tfpFindPartnerSignUpMapper.insertTfpFindPartnerSignUp(tfpFindPartnerSignUp);
    }

    /**
     * 修改找搭子
     *
     * @param tfpFindPartnerSignUp 找搭子
     * @return 结果
     */
    @Override
    public int updateTfpFindPartnerSignUp(TfpFindPartnerSignUp tfpFindPartnerSignUp)
    {
        tfpFindPartnerSignUp.setUpdateTime(DateUtils.getNowDate());
        return tfpFindPartnerSignUpMapper.updateTfpFindPartnerSignUp(tfpFindPartnerSignUp);
    }

    /**
     * 批量删除找搭子
     *
     * @param ids 需要删除的找搭子主键
     * @return 结果
     */
    @Override
    public int deleteTfpFindPartnerSignUpByIds(Long[] ids)
    {
        return tfpFindPartnerSignUpMapper.deleteTfpFindPartnerSignUpByIds(ids);
    }

    /**
     * 删除找搭子信息
     *
     * @param id 找搭子主键
     * @return 结果
     */
    @Override
    public int deleteTfpFindPartnerSignUpById(Long id)
    {
        return tfpFindPartnerSignUpMapper.deleteTfpFindPartnerSignUpById(id);
    }

    @Override
    public List<UserVo> signUpUserSelect() {
        List<Long> signUpUserIdList = tfpFindPartnerSignUpMapper.getFindPartnerSignUpUserIdList();
        return userService.handleUserVo(signUpUserIdList);
    }

    @Override
    public List<TfpFindPartnerSignUp> selectListByFindPartnerId(Long findPartnerId) {
        return tfpFindPartnerSignUpMapper.selectListByFindPartnerId(findPartnerId);
    }

    @Override
    public List<TfpFindPartnerSignUp> selectListByFindPartnerIdAndAuditStatus(Long findPartnerId,Integer auditStatus) {
        return tfpFindPartnerSignUpMapper.selectListByFindPartnerIdAndAuditStatus(findPartnerId,auditStatus);
    }

    @Override
    public List<TfpFindPartnerSignUp> selectListByFindPartnerIdListAndAuditStatus(List<Long> findPartnerIdList,Integer auditStatus) {
        return tfpFindPartnerSignUpMapper.selectListByFindPartnerIdListAndAuditStatus(findPartnerIdList,auditStatus);
    }

    @Override
    public List<FindPartnerSignUpVo> selectSignUpPersonNumberByFindPartnerIdList(List<Long> findPartnerIdList) {
        return tfpFindPartnerSignUpMapper.selectSignUpPersonNumberByFindPartnerIdList(findPartnerIdList);
    }

    @Override
    public List<ListFindPartnerSignUpVo> listFindPartnerSignUp(ListFindPartnerSignUpDto listFindPartnerSignUpDto) {
        Long findPartnerId = listFindPartnerSignUpDto.getFindPartnerId();
        if (Objects.isNull(findPartnerId)){
            throw new RuntimeException("找搭子id不能为空");
        }

        Integer status = listFindPartnerSignUpDto.getStatus();
        if (!Objects.equals(0, status) && !Objects.equals(1, status)){
            throw new RuntimeException("确认状态错误");
        }

        startPage();
        List<ListFindPartnerSignUpVo> list = tfpFindPartnerSignUpMapper.listFindPartnerSignUp(findPartnerId, status);
        if (DcListUtils.isNotEmpty(list)){
            List<Long> userIdList = list.stream().map(ListFindPartnerSignUpVo::getUserId).collect(Collectors.toList());

            // 用户信息 头像
            Map<Long, String> nickNameMap = userService.getNickNameMap(userIdList);
            Map<Long, String> userHeadImageMap = attachmentService.getUserHeadImageMap(userIdList);

            for (ListFindPartnerSignUpVo listFindPartnerSignUpVo : list) {
                Long userId = listFindPartnerSignUpVo.getUserId();

                listFindPartnerSignUpVo.setUserId(userId);
                listFindPartnerSignUpVo.setNickName(nickNameMap.get(userId));
                listFindPartnerSignUpVo.setHeadImage(userHeadImageMap.get(userId));
            }
        }

        return list;
    }

    @Override
    public DetailFindPartnerSignUpVo detailFindPartnerSignUp(Long id) {
        DetailFindPartnerSignUpVo detailFindPartnerSignUpVo = new DetailFindPartnerSignUpVo();

        TfpFindPartnerSignUp tfpFindPartnerSignUp = tfpFindPartnerSignUpMapper.selectTfpFindPartnerSignUpById(id);
        BeanUtils.copyProperties(tfpFindPartnerSignUp, detailFindPartnerSignUpVo);

        Long signUpUserId = tfpFindPartnerSignUp.getSignUpUserId();
        SysUser sysUser = userService.selectUserById(signUpUserId);
        List<TfpAttachment> attachmentList = attachmentService.selectListByBusinessId(signUpUserId, BusinessTypeEnum.USER.getValue(), BusinessSubTypeEnum.USER_HEAD_PORTRAIT.getValue());

        detailFindPartnerSignUpVo.setNickName(sysUser.getNickName());
        detailFindPartnerSignUpVo.setHeadImage(attachmentList.get(0).getUrl());
        detailFindPartnerSignUpVo.setStatus(tfpFindPartnerSignUp.getAuditStatus());
        return detailFindPartnerSignUpVo;
    }

    /**
     * 小程序-确认搭子报名
     * @param id
     * @return
     */
    @Override
    public int auditPassSignUp(Long id) {
        LoginUser loginUser = SecurityUtils.getLoginUser();

        TfpFindPartnerSignUp tfpFindPartnerSignUp = tfpFindPartnerSignUpMapper.selectTfpFindPartnerSignUpById(id);
        tfpFindPartnerSignUp.setAuditStatus(AuditStatusEnum.AUDIT_PASS.getValue());
        tfpFindPartnerSignUp.setUpdateUserId(loginUser.getUserId());
        tfpFindPartnerSignUp.setUpdateBy(loginUser.getUsername());
        tfpFindPartnerSignUp.setUpdateTime(DateUtils.getNowDate());
        tfpFindPartnerSignUpMapper.updateTfpFindPartnerSignUp(tfpFindPartnerSignUp);
        return 1;
    }

    @Override
    public List<TfpFindPartnerSignUp> selectListBySignUpUserId(Long userId) {
        return tfpFindPartnerSignUpMapper.selectListBySignUpUserId(userId);
    }

    @Override
    public int appletAdd(TfpFindPartnerSignUp tfpFindPartnerSignUp) {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = loginUser.getUserId();

        tfpFindPartnerSignUp.setAuditStatus(AuditStatusEnum.WAIT_AUDIT.getValue());
        int result = tfpFindPartnerSignUpMapper.insertTfpFindPartnerSignUp(tfpFindPartnerSignUp);

        TfpFindPartnerChatGroup tfpFindPartnerChatGroup = findPartnerChatGroupService.selectByFindPartnerId(tfpFindPartnerSignUp.getFindPartnerId());

        TfpFindPartnerChatGroupPerson findPartnerChatGroupPerson = findPartnerChatGroupPersonService.selectByGroupIdAndUserId(tfpFindPartnerChatGroup.getId(), userId);
        if (Objects.isNull(findPartnerChatGroupPerson)){
            TfpFindPartnerChatGroupPerson tfpFindPartnerChatGroupPerson = new TfpFindPartnerChatGroupPerson();
            tfpFindPartnerChatGroupPerson.setChatGroupId(tfpFindPartnerChatGroup.getId());
            tfpFindPartnerChatGroupPerson.setUserId(userId);
            tfpFindPartnerChatGroupPerson.setUserType(ChatUserTypeEnum.UN_APPLIED.getValue());
            tfpFindPartnerChatGroupPerson.setDelFlag(DeleteEnum.EXIST.getValue());
            tfpFindPartnerChatGroupPerson.setCreateUserId(loginUser.getUserId());
            tfpFindPartnerChatGroupPerson.setCreateBy(loginUser.getUsername());
            tfpFindPartnerChatGroupPerson.setCreateTime(DateUtils.getNowDate());
            findPartnerChatGroupPersonService.insertTfpFindPartnerChatGroupPerson(tfpFindPartnerChatGroupPerson);
        }

        return result;
    }
}
