package com.ruoyi.service.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpLabelMapper;
import com.ruoyi.service.domain.TfpLabel;
import com.ruoyi.service.service.ITfpLabelService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 标签Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@Service
@Transactional
public class TfpLabelServiceImpl implements ITfpLabelService 
{
    @Autowired
    private TfpLabelMapper tfpLabelMapper;

    /**
     * 查询标签
     * 
     * @param id 标签主键
     * @return 标签
     */
    @Override
    public TfpLabel selectTfpLabelById(Long id)
    {
        return tfpLabelMapper.selectTfpLabelById(id);
    }

    /**
     * 查询标签列表
     * 
     * @param tfpLabel 标签
     * @return 标签
     */
    @Override
    public List<TfpLabel> selectTfpLabelList(TfpLabel tfpLabel)
    {
        return tfpLabelMapper.selectTfpLabelList(tfpLabel);
    }

    /**
     * 新增标签
     * 
     * @param tfpLabel 标签
     * @return 结果
     */
    @Override
    public int insertTfpLabel(TfpLabel tfpLabel)
    {
        tfpLabel.setCreateTime(DateUtils.getNowDate());
        return tfpLabelMapper.insertTfpLabel(tfpLabel);
    }

    /**
     * 修改标签
     * 
     * @param tfpLabel 标签
     * @return 结果
     */
    @Override
    public int updateTfpLabel(TfpLabel tfpLabel)
    {
        tfpLabel.setUpdateTime(DateUtils.getNowDate());
        return tfpLabelMapper.updateTfpLabel(tfpLabel);
    }

    /**
     * 批量删除标签
     * 
     * @param ids 需要删除的标签主键
     * @return 结果
     */
    @Override
    public int deleteTfpLabelByIds(Long[] ids)
    {
        return tfpLabelMapper.deleteTfpLabelByIds(ids);
    }

    /**
     * 删除标签信息
     * 
     * @param id 标签主键
     * @return 结果
     */
    @Override
    public int deleteTfpLabelById(Long id)
    {
        return tfpLabelMapper.deleteTfpLabelById(id);
    }

    /**
     * 根据type查询label列表
     * @param labelType
     * @return
     */
    @Override
    public List<TfpLabel> getLabelInfoByType(Long labelType) {
        return tfpLabelMapper.getLabelInfoByType(labelType);
    }
}
