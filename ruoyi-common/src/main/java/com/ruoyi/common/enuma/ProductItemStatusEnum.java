package com.ruoyi.common.enuma;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ProductItemStatusEnum implements BaseEnum {


    WAIT_AUDIT(0, "待审核"),
    AUDIT_PASS(1, "审核通过"),
    AUDIT_NO_PASS(2, "审核不通过"),
    SAVE(3, "草稿"),
    ;


    private Integer value;
    private String desc;
}
