package com.ruoyi.service.service;

import java.util.List;
import com.ruoyi.service.domain.SysUserCare;
import com.ruoyi.service.vo.CareUserInfoVo;
import com.ruoyi.service.vo.FansUserInfoVo;
import com.ruoyi.service.vo.SysUserCareVo;
import com.ruoyi.service.vo.UserVo;

/**
 * 关注Service接口
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public interface ISysUserCareService 
{
    /**
     * 查询关注
     * 
     * @param id 关注主键
     * @return 关注
     */
    public SysUserCare selectSysUserCareById(Long id);

    /**
     * 查询关注列表
     * 
     * @param sysUserCare 关注
     * @return 关注集合
     */
    public List<SysUserCare> selectSysUserCareList(SysUserCare sysUserCare);

    List<SysUserCareVo> selectSysUserCareVoList(SysUserCare sysUserCare);

    /**
     * 新增关注
     * 
     * @param sysUserCare 关注
     * @return 结果
     */
    public int insertSysUserCare(SysUserCare sysUserCare);

    /**
     * 修改关注
     * 
     * @param sysUserCare 关注
     * @return 结果
     */
    public int updateSysUserCare(SysUserCare sysUserCare);

    /**
     * 批量删除关注
     * 
     * @param ids 需要删除的关注主键集合
     * @return 结果
     */
    public int deleteSysUserCareByIds(Long[] ids);

    /**
     * 删除关注信息
     * 
     * @param id 关注主键
     * @return 结果
     */
    public int deleteSysUserCareById(Long id);

    List<UserVo> ownerUserSelect();

    List<UserVo> fansUserSelect();

    /**
     * 关注个数
     * @param userId
     * @return
     */
    Integer selectCareNumber(Long userId);

    /**
     * 粉丝个数
     * @param userId
     * @return
     */
    Integer selectFansNumber(Long userId);

    /**
     * 小程序-我的关注列表
     * @return
     */
    List<CareUserInfoVo> careUserInfoList(Long userId);

    /**
     * 小程序-我的粉丝列表
     * @return
     */
    List<FansUserInfoVo> fansUserInfoList(Long userId);

    /**
     * 小程序-关注用户
     * @param userId
     * @return
     */
    int careUser(Long userId);

    /**
     * 小程序-取消关注用户
     * @param userId
     * @return
     */
    int cancelCareUser(Long userId);
}
