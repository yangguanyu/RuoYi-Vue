package com.ruoyi.service.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(value = "PriceDetailVo")
public class PriceDetailVo {
    @ApiModelProperty("售卖单价")
    private BigDecimal salePrice;
    @ApiModelProperty("人数")
    private Integer personNumber;
    @ApiModelProperty("售卖总价")
    private BigDecimal finalAmount;
}
