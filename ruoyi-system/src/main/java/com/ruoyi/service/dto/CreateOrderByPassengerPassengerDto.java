package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("CreateOrderByPassengerPassengerDto")
public class CreateOrderByPassengerPassengerDto {
    @ApiModelProperty("姓名")
    private String name;
    @ApiModelProperty("性别 1, 男 2, 女")
    private Integer sex;
    @ApiModelProperty("生日（格式：yyyy-MM-dd） 儿童为必填 产品类型为4、41、42、43、44、45、21、22、23、24、25必传入出生年月 新增证件类型必填")
    private String birthday;
    @ApiModelProperty("证件类型 新增证件类型必填 1-身份证、港澳台居民居住证 (18位) 4-无法识别证件，不支持自动值机(最长18位) 11-护照 12-电子护照 13-军官证、警官证、士兵证等军方证件 14-海员证 15-港澳台来往通行证 16-外交部签发的驻华外交人员证 17-十六周岁以下（户口簿、学生证、出生证明、户口所在地公安出具证明）；民航局规定的其它有效乘机的身份证件 18-外国人永久居留证、出入境证")
    private Integer identityType;
    @ApiModelProperty("国籍二字码，例如：CN 新增证件类型必填")
    private String nationality;
    @ApiModelProperty("发证国家二字码，例如：CN 新增证件类型必填")
    private String certificateCountry;
    @ApiModelProperty("证件有效期，格式：yyyy-MM-dd 新增证件类型必填")
    private String certificateUsefulLife;
    @ApiModelProperty("乘客性别，MALE代表男，FEMALE代表女 新增证件类型必填")
    private String gender;
    @ApiModelProperty("证件号码")
    private String identityNo;
    @ApiModelProperty("乘机人类型，1成人，2儿童，3婴儿（暂不使用）")
    private Integer crewType;
    @ApiModelProperty("电话区号，例如：86")
    private String preNum;
    @ApiModelProperty("乘客手机号（11位）")
    private String phoneNum;
    @ApiModelProperty("用于存储以乘客拆单用户")
    private String sonSheNo;
    @ApiModelProperty("证件有效期 2018-10-12")
    private String effectiveDate;
    @ApiModelProperty("随成人姓名")
    private String followAdtName;
    @ApiModelProperty("随成人证件号")
    private String followIdentityNo;
    @ApiModelProperty("跟随成人舱位，单独儿置必项")
    private String followAdtSeat;
    @ApiModelProperty("跟随成人票号，单独儿置必项")
    private String followAdtPnrNo;
    @ApiModelProperty("跟随成人清码，单独儿置必项")
    private String followAdtTicketNo;
}
