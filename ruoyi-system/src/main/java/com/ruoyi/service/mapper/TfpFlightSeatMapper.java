package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.TfpFlightSeat;
import org.apache.ibatis.annotations.Param;

/**
 * 舱位信息Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-10
 */
public interface TfpFlightSeatMapper 
{
    /**
     * 查询舱位信息
     * 
     * @param id 舱位信息主键
     * @return 舱位信息
     */
    public TfpFlightSeat selectTfpFlightSeatById(Long id);

    /**
     * 查询舱位信息列表
     * 
     * @param tfpFlightSeat 舱位信息
     * @return 舱位信息集合
     */
    public List<TfpFlightSeat> selectTfpFlightSeatList(TfpFlightSeat tfpFlightSeat);

    /**
     * 新增舱位信息
     * 
     * @param tfpFlightSeat 舱位信息
     * @return 结果
     */
    public int insertTfpFlightSeat(TfpFlightSeat tfpFlightSeat);

    /**
     * 修改舱位信息
     * 
     * @param tfpFlightSeat 舱位信息
     * @return 结果
     */
    public int updateTfpFlightSeat(TfpFlightSeat tfpFlightSeat);

    /**
     * 删除舱位信息
     * 
     * @param id 舱位信息主键
     * @return 结果
     */
    public int deleteTfpFlightSeatById(Long id);

    /**
     * 批量删除舱位信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpFlightSeatByIds(Long[] ids);

    void deleteTfpFlightSeatByFlightId(@Param("flightId") Long flightId);
}
