package com.ruoyi.service.service;

import java.util.List;
import com.ruoyi.service.domain.AirportCodes;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2024-04-28
 */
public interface IAirportCodesService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public AirportCodes selectAirportCodesById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param airportCodes 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<AirportCodes> selectAirportCodesList(AirportCodes airportCodes);

    /**
     * 新增【请填写功能名称】
     * 
     * @param airportCodes 【请填写功能名称】
     * @return 结果
     */
    public int insertAirportCodes(AirportCodes airportCodes);

    /**
     * 修改【请填写功能名称】
     * 
     * @param airportCodes 【请填写功能名称】
     * @return 结果
     */
    public int updateAirportCodes(AirportCodes airportCodes);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteAirportCodesByIds(Long[] ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteAirportCodesById(Long id);

    String getThreeCode(String cityName);
}
