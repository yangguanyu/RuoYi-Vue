package com.ruoyi.service.domain.flight.base;

import lombok.Data;

@Data
public class FlightBaseParam<T> {
    // 返回是否接受Gzip
    //    默认true
    //    暂时仅支持返回GZip
    private Boolean rsIsGzip = Boolean.TRUE;

    // 用户自定义唯一
    //请求表示用于bug查询记录log
    //小于，等于32长度字符串
    private String rqIdentification;

    //时间戳例：1505034290399
    private Long timeStamp;

    //
    //第三方系统名称,可以不传
    private String thirdUsername;

    //业务参数
    private T rqData;
}
