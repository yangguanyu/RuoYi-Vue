package com.ruoyi.service.service;

import java.math.BigDecimal;
import java.util.List;

import com.ruoyi.service.domain.TfpOrder;
import com.ruoyi.service.domain.TfpUserIncomeDetail;
import com.ruoyi.service.dto.SettlementDetailListDto;
import com.ruoyi.service.dto.ExportDto;
import com.ruoyi.service.dto.UserIncomeDetailManageListDto;
import com.ruoyi.service.dto.ViewShareInfoDto;
import com.ruoyi.service.excel.UserIncomeDetailExportExcel;
import com.ruoyi.service.vo.InvitingAchievementsVo;
import com.ruoyi.service.vo.SettlementDetailListVo;
import com.ruoyi.service.vo.UserIncomeDetailListVo;
import com.ruoyi.service.vo.UserIncomeDetailManageListVo;

/**
 * 用户收益明细Service接口
 * 
 * @author ruoyi
 * @date 2024-01-16
 */
public interface ITfpUserIncomeDetailService 
{
    /**
     * 查询用户收益明细
     * 
     * @param id 用户收益明细主键
     * @return 用户收益明细
     */
    public TfpUserIncomeDetail selectTfpUserIncomeDetailById(Long id);

    /**
     * 查询用户收益明细列表
     * 
     * @param tfpUserIncomeDetail 用户收益明细
     * @return 用户收益明细集合
     */
    public List<TfpUserIncomeDetail> selectTfpUserIncomeDetailList(TfpUserIncomeDetail tfpUserIncomeDetail);

    /**
     * 新增用户收益明细
     * 
     * @param tfpUserIncomeDetail 用户收益明细
     * @return 结果
     */
    public int insertTfpUserIncomeDetail(TfpUserIncomeDetail tfpUserIncomeDetail);

    /**
     * 修改用户收益明细
     * 
     * @param tfpUserIncomeDetail 用户收益明细
     * @return 结果
     */
    public int updateTfpUserIncomeDetail(TfpUserIncomeDetail tfpUserIncomeDetail);

    /**
     * 批量删除用户收益明细
     * 
     * @param ids 需要删除的用户收益明细主键集合
     * @return 结果
     */
    public int deleteTfpUserIncomeDetailByIds(Long[] ids);

    /**
     * 删除用户收益明细信息
     * 
     * @param id 用户收益明细主键
     * @return 结果
     */
    public int deleteTfpUserIncomeDetailById(Long id);

    int viewShareInfo(ViewShareInfoDto viewShareInfoDto);

    void updateShareInfo(Long shareUserId, Integer sourceType, Long sourceId, Integer status, Long orderId, String orderNumber, BigDecimal sourcePrice);

    void handleUserIncomeAfterCancelOrder(String orderNumber);

    /**
     * 获取累计收益(元)
     * @param userId
     * @return
     */
    BigDecimal getCumulativeIncomeByUserId(Long userId);

    BigDecimal getUserCumulativeIncome();

    List<UserIncomeDetailListVo> userIncomeDetailList();

    List<SettlementDetailListVo> settlementDetailList(SettlementDetailListDto settlementDetailListDto);

    List<InvitingAchievementsVo> invitingAchievements();

    List<UserIncomeDetailManageListVo> userIncomeDetailManageList(UserIncomeDetailManageListDto userIncomeDetailManageListDto);

    List<UserIncomeDetailExportExcel> userIncomeDetailManageListExport(ExportDto exportDto);

    void handleSettlementCompleteTask(List<Long> orderIdList);
}
