package com.ruoyi.web.controller.service;

import java.util.List;
import java.util.Objects;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enuma.ChatGroupPersonUserTypeEnum;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.bean.CustomizeBeanCopier;
import com.ruoyi.service.vo.ChatGroupPersonIdnetityVO;
import com.ruoyi.web.controller.service.form.group.ChatGroupPersonForm;
import com.ruoyi.web.controller.service.form.group.GroupIdentityPersonForm;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.TfpChatGroupPerson;
import com.ruoyi.service.service.ITfpChatGroupPersonService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 聊天群组成员Controller
 *
 * @author ruoyi
 * @date 2023-10-31
 */
@RestController
@RequestMapping("/service/chatGroupPerson")
public class TfpChatGroupPersonController extends BaseController
{
    @Autowired
    private ITfpChatGroupPersonService tfpChatGroupPersonService;

    /**
     * 查询聊天群组成员列表
     */
    @PreAuthorize("@ss.hasPermi('service:chatGroupPerson:list')")
    @GetMapping("/list")
    public TableDataInfo list(TfpChatGroupPerson tfpChatGroupPerson)
    {
        startPage();
        List<TfpChatGroupPerson> list = tfpChatGroupPersonService.selectTfpChatGroupPersonList(tfpChatGroupPerson);
        return getDataTable(list);
    }

    /**
     * 导出聊天群组成员列表
     */
    @PreAuthorize("@ss.hasPermi('service:chatGroupPerson:export')")
    @Log(title = "聊天群组成员", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TfpChatGroupPerson tfpChatGroupPerson)
    {
        List<TfpChatGroupPerson> list = tfpChatGroupPersonService.selectTfpChatGroupPersonList(tfpChatGroupPerson);
        ExcelUtil<TfpChatGroupPerson> util = new ExcelUtil<TfpChatGroupPerson>(TfpChatGroupPerson.class);
        util.exportExcel(response, list, "聊天群组成员数据");
    }

    /**
     * 获取聊天群组成员详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:chatGroupPerson:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tfpChatGroupPersonService.selectTfpChatGroupPersonById(id));
    }

    /**
     * 新增聊天群组成员
     */
    @PreAuthorize("@ss.hasPermi('service:chatGroupPerson:add')")
    @Log(title = "聊天群组成员", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TfpChatGroupPerson tfpChatGroupPerson)
    {
        return toAjax(tfpChatGroupPersonService.insertTfpChatGroupPerson(tfpChatGroupPerson));
    }

    /**
     * 修改聊天群组成员
     */
    @PreAuthorize("@ss.hasPermi('service:chatGroupPerson:edit')")
    @Log(title = "聊天群组成员", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TfpChatGroupPerson tfpChatGroupPerson)
    {
        return toAjax(tfpChatGroupPersonService.updateTfpChatGroupPerson(tfpChatGroupPerson));
    }

    /**
     * 删除聊天群组成员
     */
    @PreAuthorize("@ss.hasPermi('service:chatGroupPerson:remove')")
    @Log(title = "聊天群组成员", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tfpChatGroupPersonService.deleteTfpChatGroupPersonByIds(ids));
    }

    @ApiOperation("我想去，并且添加进一个群组（假如有群组）")
    @PostMapping("/applet/add")
    public AjaxResult appletAdd(@RequestBody ChatGroupPersonForm form)
    {
        LoginUser loginUser = getLoginUser();
        TfpChatGroupPerson tfpChatGroupPerson = new TfpChatGroupPerson();
        BeanUtils.copyProperties(form,tfpChatGroupPerson);
        tfpChatGroupPerson.setUpdateUserId(loginUser.getUserId());
        tfpChatGroupPerson.setCreateUserId(loginUser.getUserId());
        tfpChatGroupPerson.setCreateTime(DateUtils.getNowDate());
        tfpChatGroupPerson.setUpdateTime(DateUtils.getNowDate());
        tfpChatGroupPerson.setCreateBy(loginUser.getUsername());
        return toAjax(tfpChatGroupPersonService.appletInsert(tfpChatGroupPerson));
    }

    @ApiOperation("查询在群组里什么身份")
    @PostMapping("/applet/person/identity")
    public R<ChatGroupPersonIdnetityVO> personIdentity(@RequestBody GroupIdentityPersonForm form)
    {
        LoginUser loginUser = getLoginUser();
//        ChatGroupPersonUserTypeEnum
        TfpChatGroupPerson tfpChatGroupPerson = tfpChatGroupPersonService.selectOneByUserIdAndGroupId(loginUser.getUserId(), form.getChatGroupId());
        ChatGroupPersonIdnetityVO chatGroupPersonIdnetityVO = CustomizeBeanCopier.copyProperties(tfpChatGroupPerson, ChatGroupPersonIdnetityVO.class);
        if(Objects.isNull(chatGroupPersonIdnetityVO)){
            chatGroupPersonIdnetityVO = new ChatGroupPersonIdnetityVO();
            chatGroupPersonIdnetityVO.setUserType(ChatGroupPersonUserTypeEnum.NOT_SIGN_UP.getValue());
            chatGroupPersonIdnetityVO.setUserId(loginUser.getUserId());
            chatGroupPersonIdnetityVO.setChatGroupId(form.getChatGroupId());
        }
        return R.ok(chatGroupPersonIdnetityVO);
    }
}
