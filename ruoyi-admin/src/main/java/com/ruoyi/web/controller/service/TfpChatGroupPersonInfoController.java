package com.ruoyi.web.controller.service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.bean.BeanUtils;
import com.ruoyi.service.dto.ChatGroupPersonInfoDto;
import com.ruoyi.service.dto.TfpChatGroupPersonInfoDto;
import com.ruoyi.common.enuma.DeleteEnum;
import com.ruoyi.service.vo.TfpChatGroupPersonInfoVo;
import com.ruoyi.service.vo.UserVo;
import com.ruoyi.web.controller.service.form.group.ChatGroupInfoSearchForm;
import com.ruoyi.web.controller.service.form.group.ChatGroupPersonInfoForm;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.TfpChatGroupPersonInfo;
import com.ruoyi.service.service.ITfpChatGroupPersonInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 聊天群组成员聊天信息Controller
 *
 * @author ruoyi
 * @date 2023-10-31
 */
@RestController
@RequestMapping("/service/chatGroupPersonInfo")
public class TfpChatGroupPersonInfoController extends BaseController
{
    @Autowired
    private ITfpChatGroupPersonInfoService tfpChatGroupPersonInfoService;

    /**
     * 查询聊天群组成员聊天信息列表
     */
    @PreAuthorize("@ss.hasPermi('service:chatGroupPersonInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(TfpChatGroupPersonInfoDto tfpChatGroupPersonInfoDto)
    {
        List<TfpChatGroupPersonInfoVo> list = tfpChatGroupPersonInfoService.selectTfpChatGroupPersonInfoVoList(tfpChatGroupPersonInfoDto);
        return getDataTable(list);
    }

    /**
     * 导出聊天群组成员聊天信息列表
     */
    @PreAuthorize("@ss.hasPermi('service:chatGroupPersonInfo:export')")
    @Log(title = "聊天群组成员聊天信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TfpChatGroupPersonInfo tfpChatGroupPersonInfo)
    {
        List<TfpChatGroupPersonInfo> list = tfpChatGroupPersonInfoService.selectTfpChatGroupPersonInfoList(tfpChatGroupPersonInfo);
        ExcelUtil<TfpChatGroupPersonInfo> util = new ExcelUtil<TfpChatGroupPersonInfo>(TfpChatGroupPersonInfo.class);
        util.exportExcel(response, list, "聊天群组成员聊天信息数据");
    }

    /**
     * 获取聊天群组成员聊天信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:chatGroupPersonInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tfpChatGroupPersonInfoService.selectTfpChatGroupPersonInfoById(id));
    }

    /**
     * 新增聊天群组成员聊天信息
     */
    @PreAuthorize("@ss.hasPermi('service:chatGroupPersonInfo:add')")
    @Log(title = "聊天群组成员聊天信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TfpChatGroupPersonInfo tfpChatGroupPersonInfo)
    {
        return toAjax(tfpChatGroupPersonInfoService.insertTfpChatGroupPersonInfo(tfpChatGroupPersonInfo));
    }



    /**
     * 修改聊天群组成员聊天信息
     */
    @PreAuthorize("@ss.hasPermi('service:chatGroupPersonInfo:edit')")
    @Log(title = "聊天群组成员聊天信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TfpChatGroupPersonInfo tfpChatGroupPersonInfo)
    {
        return toAjax(tfpChatGroupPersonInfoService.updateTfpChatGroupPersonInfo(tfpChatGroupPersonInfo));
    }

    /**
     * 删除聊天群组成员聊天信息
     */
    @PreAuthorize("@ss.hasPermi('service:chatGroupPersonInfo:remove')")
    @Log(title = "聊天群组成员聊天信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tfpChatGroupPersonInfoService.deleteTfpChatGroupPersonInfoByIds(ids));
    }

    @PreAuthorize("@ss.hasPermi('service:chatGroupPersonInfo:list')")
    @PostMapping("/chatGroupPersonSelect")
    public R<List<UserVo>> chatGroupPersonSelect()
    {
        return R.ok(tfpChatGroupPersonInfoService.chatGroupPersonSelect());
    }




    /**
     * 新增聊天群组成员聊天信息
     */
    //@PreAuthorize("@ss.hasPermi('service:chatGroupPersonInfo:add')")
    @PostMapping("/applet/add")
    @ApiOperation("小程序-新增聊天群组成员聊天信息")
    public AjaxResult appletChatInfoAdd(@RequestBody ChatGroupPersonInfoForm form) {
        ChatGroupPersonInfoDto chatGroupPersonInfoDto = new ChatGroupPersonInfoDto();
        BeanUtils.copyProperties(form, chatGroupPersonInfoDto);
        return toAjax(tfpChatGroupPersonInfoService.appletChatInfoAdd(chatGroupPersonInfoDto));
    }


    @PostMapping("/applet/chatInfoList")
    @ApiOperation("小程序-获取聊天列表")
    public R<List<TfpChatGroupPersonInfoVo>> appletChatInfoList(@RequestBody ChatGroupInfoSearchForm form)
    {
        TfpChatGroupPersonInfoDto tfpChatGroupPersonInfoDto = new TfpChatGroupPersonInfoDto();
        BeanUtils.copyProperties(form,tfpChatGroupPersonInfoDto);
        tfpChatGroupPersonInfoDto.setChatPersonInfoId(form.getCurrentId());
        List<TfpChatGroupPersonInfoVo> list = tfpChatGroupPersonInfoService.selectAppletInfoVoList(tfpChatGroupPersonInfoDto);
        //处理：不返回传的id
        Long currentId = form.getCurrentId();
        if(Objects.nonNull(currentId)){
            list = list.stream().filter(e->!( e.getId().equals(currentId) )).collect(Collectors.toList());
        }
        return R.ok(list);
    }


}
