package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.TfpProductSnapshot;
import com.ruoyi.service.vo.MyProductSnapshotListVo;
import org.apache.ibatis.annotations.Param;

/**
 * 商品快照Mapper接口
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public interface TfpProductSnapshotMapper 
{
    /**
     * 查询商品快照
     * 
     * @param id 商品快照主键
     * @return 商品快照
     */
    public TfpProductSnapshot selectTfpProductSnapshotById(Long id);

    /**
     * 查询商品快照列表
     * 
     * @param tfpProductSnapshot 商品快照
     * @return 商品快照集合
     */
    public List<TfpProductSnapshot> selectTfpProductSnapshotList(TfpProductSnapshot tfpProductSnapshot);

    /**
     * 新增商品快照
     * 
     * @param tfpProductSnapshot 商品快照
     * @return 结果
     */
    public int insertTfpProductSnapshot(TfpProductSnapshot tfpProductSnapshot);

    /**
     * 修改商品快照
     * 
     * @param tfpProductSnapshot 商品快照
     * @return 结果
     */
    public int updateTfpProductSnapshot(TfpProductSnapshot tfpProductSnapshot);

    /**
     * 删除商品快照
     * 
     * @param id 商品快照主键
     * @return 结果
     */
    public int deleteTfpProductSnapshotById(Long id);

    /**
     * 批量删除商品快照
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpProductSnapshotByIds(Long[] ids);

    List<TfpProductSnapshot> selectTfpProductSnapshotByIdList(@Param("idList") List<Long> idList);

    TfpProductSnapshot selectTfpProductSnapshotByProductIdAndUserId(@Param("productId") Long productId, @Param("createUserId") Long createUserId);

    List<MyProductSnapshotListVo> myProductSnapshotList(@Param("userId") Long userId);

}
