package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Data
@ApiModel(value = "DepartureDateDto")
public class DepartureDateDto {
    @NotNull(message = "选择出发日期开始不能为空")
    private Date departureDateStart;

    @NotNull(message = "选择出发日期截止不能为空")
    private Date departureDateEnd;

    @NotNull(message = "成人零售价不能为空")
    private BigDecimal adultRetailPrice;
}
