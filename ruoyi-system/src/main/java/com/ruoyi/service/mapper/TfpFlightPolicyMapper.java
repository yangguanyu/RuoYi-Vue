package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.TfpFlightPolicy;
import org.apache.ibatis.annotations.Param;

/**
 * 政策信息Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-10
 */
public interface TfpFlightPolicyMapper 
{
    /**
     * 查询政策信息
     * 
     * @param id 政策信息主键
     * @return 政策信息
     */
    public TfpFlightPolicy selectTfpFlightPolicyById(Long id);

    /**
     * 查询政策信息列表
     * 
     * @param tfpFlightPolicy 政策信息
     * @return 政策信息集合
     */
    public List<TfpFlightPolicy> selectTfpFlightPolicyList(TfpFlightPolicy tfpFlightPolicy);

    /**
     * 新增政策信息
     * 
     * @param tfpFlightPolicy 政策信息
     * @return 结果
     */
    public int insertTfpFlightPolicy(TfpFlightPolicy tfpFlightPolicy);

    /**
     * 修改政策信息
     * 
     * @param tfpFlightPolicy 政策信息
     * @return 结果
     */
    public int updateTfpFlightPolicy(TfpFlightPolicy tfpFlightPolicy);

    /**
     * 删除政策信息
     * 
     * @param id 政策信息主键
     * @return 结果
     */
    public int deleteTfpFlightPolicyById(Long id);

    /**
     * 批量删除政策信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpFlightPolicyByIds(Long[] ids);

    void deleteTfpFlightPolicyByFlightId(@Param("flightId") Long flightId);
}
