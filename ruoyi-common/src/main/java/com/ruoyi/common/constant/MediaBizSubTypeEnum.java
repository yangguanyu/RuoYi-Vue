package com.ruoyi.common.constant;

/**
 * <p>多媒体信息业务类型</p>
 */
public enum MediaBizSubTypeEnum {

    /**
     * 项目模块类型(1:项目 2:更新项目进度 3:任务 4:风险)
     */
    PRODUCT_SHARE(MediaBizTypeEnum.PRODUCT, 100001, "商品分享"),
    ;

    private MediaBizTypeEnum bizType;

    private int subType;

    private String desc;

    MediaBizSubTypeEnum(MediaBizTypeEnum bizType, int type, String desc) {
        this.bizType = bizType;
        this.subType = type;
        this.desc = desc;
    }

    public MediaBizTypeEnum getBizType() {
        return bizType;
    }

    public void setBizType(MediaBizTypeEnum bizType) {
        this.bizType = bizType;
    }

    public int getSubType() {
        return subType;
    }

    public String getTypeStr() {
        return String.valueOf(subType);
    }

    public void setSubType(int subType) {
        this.subType = subType;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
