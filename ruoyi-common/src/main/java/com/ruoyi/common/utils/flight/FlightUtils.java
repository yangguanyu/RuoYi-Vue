package com.ruoyi.common.utils.flight;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public class FlightUtils {
    public static String generateSignature(String requestJson, String userSecurityCode) {
        String combinedString = requestJson + userSecurityCode;
        try {
            // 创建MD5 Hash
            MessageDigest md = MessageDigest.getInstance("MD5");
            // 对拼接后的字符串进行MD5加密
            md.update(combinedString.getBytes());
            // 将加密后的字节数组转换为十六进制字符串
            byte[] encrypted = md.digest();
            StringBuilder sb = new StringBuilder();
            for (byte b : encrypted) {
                sb.append(String.format("%02x", b));
            }
            // 返回32位小写字符串
            return sb.toString().toLowerCase();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("MD5 algorithm not found", e);
        }
    }

}
