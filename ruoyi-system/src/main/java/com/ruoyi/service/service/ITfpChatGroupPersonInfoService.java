package com.ruoyi.service.service;

import java.util.List;
import com.ruoyi.service.domain.TfpChatGroupPersonInfo;
import com.ruoyi.service.dto.ChatGroupPersonInfoDto;
import com.ruoyi.service.dto.TfpChatGroupPersonInfoDto;
import com.ruoyi.service.vo.TfpChatGroupPersonInfoVo;
import com.ruoyi.service.vo.UserVo;

/**
 * 聊天群组成员聊天信息Service接口
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public interface ITfpChatGroupPersonInfoService 
{
    /**
     * 查询聊天群组成员聊天信息
     * 
     * @param id 聊天群组成员聊天信息主键
     * @return 聊天群组成员聊天信息
     */
    public TfpChatGroupPersonInfo selectTfpChatGroupPersonInfoById(Long id);

    /**
     * 查询聊天群组成员聊天信息列表
     * 
     * @param tfpChatGroupPersonInfo 聊天群组成员聊天信息
     * @return 聊天群组成员聊天信息集合
     */
    public List<TfpChatGroupPersonInfo> selectTfpChatGroupPersonInfoList(TfpChatGroupPersonInfo tfpChatGroupPersonInfo);

    List<TfpChatGroupPersonInfoVo> selectTfpChatGroupPersonInfoVoList(TfpChatGroupPersonInfoDto tfpChatGroupPersonInfoDto);

    List<TfpChatGroupPersonInfoVo> selectAppletInfoVoList(TfpChatGroupPersonInfoDto tfpChatGroupPersonInfoDto);
    /**
     * 新增聊天群组成员聊天信息
     * 
     * @param tfpChatGroupPersonInfo 聊天群组成员聊天信息
     * @return 结果
     */
    public int insertTfpChatGroupPersonInfo(TfpChatGroupPersonInfo tfpChatGroupPersonInfo);

    /**
     * 修改聊天群组成员聊天信息
     * 
     * @param tfpChatGroupPersonInfo 聊天群组成员聊天信息
     * @return 结果
     */
    public int updateTfpChatGroupPersonInfo(TfpChatGroupPersonInfo tfpChatGroupPersonInfo);

    /**
     * 批量删除聊天群组成员聊天信息
     * 
     * @param ids 需要删除的聊天群组成员聊天信息主键集合
     * @return 结果
     */
    public int deleteTfpChatGroupPersonInfoByIds(Long[] ids);

    /**
     * 删除聊天群组成员聊天信息信息
     * 
     * @param id 聊天群组成员聊天信息主键
     * @return 结果
     */
    public int deleteTfpChatGroupPersonInfoById(Long id);

    List<UserVo> chatGroupPersonSelect();

    int appletChatInfoAdd(ChatGroupPersonInfoDto chatGroupPersonInfoDto);
}
