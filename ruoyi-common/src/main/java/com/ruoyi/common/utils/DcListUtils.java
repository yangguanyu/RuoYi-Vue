package com.ruoyi.common.utils;

import java.util.List;

public class DcListUtils {
    public static boolean isNotEmpty(List list){
        if (list != null && list.size() > 0){
            return true;
        } else {
            return false;
        }
    }

    public static boolean isEmpty(List list){
        return !isNotEmpty(list);
    }
}
