package com.ruoyi.service.excel;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;

import java.util.Date;

@Data
public class ListTravelRuleExcel {
    @Excel(name = "版本号")
    private String version;
    @Excel(name = "规则类型")
    private String travelTypeName;
    @Excel(name = "上传时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date createTime;
    @Excel(name = "操作人")
    private String realName;

    @JsonIgnore
    private Long userId;
    @JsonIgnore
    private String userName;
    @JsonIgnore
    private Integer travelType;
}
