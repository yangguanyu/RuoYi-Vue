package com.ruoyi.service.domain.flight.res.orderdetail;

import lombok.Data;

import java.util.List;

@Data
public class PayDataResp {
    // 订单支付总金额
    private Double totalPay;

    // 票面总价
    private Integer totalPrice;

    // 订单支付手续费
    private Double payCharge;

    // 燃油总计
    private Integer totalFuelTax;

    // 机建总计
    private Integer totalAirportTax;

    // 支付连接
    private String payUrl;

    // 支付交易号
    private String paytradeNo;

    // 付款账号
    private String payerAccount;

    // 支付完成时间
    private Integer payTime;

    // 乘客手机号
    private String phoneNum;

    // 第三方交易号
    private String thirdPartyTransaction;
}
