package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.TfpProductSub;
import org.apache.ibatis.annotations.Param;

/**
 * 商品辅Mapper接口
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public interface TfpProductSubMapper 
{
    /**
     * 查询商品辅
     * 
     * @param id 商品辅主键
     * @return 商品辅
     */
    public TfpProductSub selectTfpProductSubById(Long id);

    /**
     * 查询商品辅列表
     * 
     * @param tfpProductSub 商品辅
     * @return 商品辅集合
     */
    public List<TfpProductSub> selectTfpProductSubList(TfpProductSub tfpProductSub);

    /**
     * 新增商品辅
     * 
     * @param tfpProductSub 商品辅
     * @return 结果
     */
    public int insertTfpProductSub(TfpProductSub tfpProductSub);

    /**
     * 修改商品辅
     * 
     * @param tfpProductSub 商品辅
     * @return 结果
     */
    public int updateTfpProductSub(TfpProductSub tfpProductSub);

    /**
     * 删除商品辅
     * 
     * @param id 商品辅主键
     * @return 结果
     */
    public int deleteTfpProductSubById(Long id);

    /**
     * 批量删除商品辅
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpProductSubByIds(Long[] ids);

    TfpProductSub selectTfpProductSubByProductId(@Param("productId") Long productId);

    void deleteTfpProductSubByProductIds(@Param("productIdList") List<Long> productIdList);
}
