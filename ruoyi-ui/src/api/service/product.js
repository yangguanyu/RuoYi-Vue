import request from '@/utils/request'
import requestNew from '@/utils/requestNew'

// 查询商品主列表
export function listProduct(query) {
  return request({
    url: '/service/product/list',
    method: 'get',
    params: query
  })
}

// 验证航班城市编码是否存在
export function judgeAirportCodesExists(cityName) {
  return request({
    url: `/service/airportCodes/judgeAirportCodesExists?cityName=${cityName}`,
    method: 'post',
  })
}

// 查询商品主详细
export function getProduct(id) {
  return request({
    url: '/service/product/' + id,
    method: 'get'
  })
}

// 新增商品主
export function addProduct(data) {
  return request({
    url: '/service/product/add',
    method: 'post',
    data: data
  })
}

// 修改商品主
export function updateProduct(data) {
  return request({
    url: '/service/product/update',
    method: 'post',
    data: data
  })
}

// 删除商品主
export function delProduct(id) {
  return request({
    url: '/service/product/delete/' + id,
    method: 'delete'
  })
}


// 获取业务区域
export function serviceAreaTreeSelect() {
  return request({
    url: '/system/area/getServiceAreaList',
    method: 'get'
  })
}

// 获取省市区域
export function provinceUrbanTreeSelect() {
  return request({
    url: '/system/area/getProvinceUrbanList',
    method: 'get'
  })
}

export function categorySelect(){
  return request({
    url: '/service/label/getLabelInfoByType/1',
    method: 'get'
  })
}

export function supplierSelect(){
  return request({
    url: '/service/supplier/supplierSelect',
    method: 'get'
  })
}

export function cooperativeBranchCompanySelect(){
  return request({
    url: '/service/cooperativeBranchCompany/cooperativeBranchCompanySelect',
    method: 'get'
  })
}

export function customerServiceSelect(){
  return request({
    url: '/service/customerService/customerServiceSelect',
    method: 'get'
  })
}

export function dicDataSelect(query){
  return request({
    url: '/system/dict/data/dicDataSelect',
    method: 'post',
    data: query
  })
}

export function detail(id){
  return request({
    url: '/service/product/detail/' + id,
    method: 'get'
  })
}

// export function uploadImg(query){
//   return requestNew({
//     url: '/uploadQingLv',
//     method: 'post',
//     data: query
//   })
// }

export function operationShelives(query){
  return request({
    url: '/service/product/operationShelives',
    method: 'post',
    data: query
  })
}
