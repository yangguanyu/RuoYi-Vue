import request from '@/utils/request'

// 查询用户个性标签列表
export function listUserLabel(query) {
  return request({
    url: '/service/userLabel/list',
    method: 'get',
    params: query
  })
}

// 查询用户个性标签详细
export function getUserLabel(id) {
  return request({
    url: '/service/userLabel/' + id,
    method: 'get'
  })
}

// 新增用户个性标签
export function addUserLabel(data) {
  return request({
    url: '/service/userLabel',
    method: 'post',
    data: data
  })
}

// 修改用户个性标签
export function updateUserLabel(data) {
  return request({
    url: '/service/userLabel',
    method: 'put',
    data: data
  })
}

// 删除用户个性标签
export function delUserLabel(id) {
  return request({
    url: '/service/userLabel/' + id,
    method: 'delete'
  })
}
