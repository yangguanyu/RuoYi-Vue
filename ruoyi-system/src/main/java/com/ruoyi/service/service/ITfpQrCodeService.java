package com.ruoyi.service.service;

import java.util.List;
import com.ruoyi.service.domain.TfpQrCode;
import com.ruoyi.service.dto.GetQrCodeDto;

/**
 * 二维码Service接口
 * 
 * @author ruoyi
 * @date 2023-12-25
 */
public interface ITfpQrCodeService 
{
    /**
     * 查询二维码
     * 
     * @param id 二维码主键
     * @return 二维码
     */
    public TfpQrCode selectTfpQrCodeById(Long id);

    /**
     * 查询二维码列表
     * 
     * @param tfpQrCode 二维码
     * @return 二维码集合
     */
    public List<TfpQrCode> selectTfpQrCodeList(TfpQrCode tfpQrCode);

    /**
     * 新增二维码
     * 
     * @param tfpQrCode 二维码
     * @return 结果
     */
    public int insertTfpQrCode(TfpQrCode tfpQrCode);

    /**
     * 修改二维码
     * 
     * @param tfpQrCode 二维码
     * @return 结果
     */
    public int updateTfpQrCode(TfpQrCode tfpQrCode);

    /**
     * 批量删除二维码
     * 
     * @param ids 需要删除的二维码主键集合
     * @return 结果
     */
    public int deleteTfpQrCodeByIds(Long[] ids);

    /**
     * 删除二维码信息
     * 
     * @param id 二维码主键
     * @return 结果
     */
    public int deleteTfpQrCodeById(Long id);

    /**
     * 获取二维码
     * @param detQrCodeDto
     * @return
     */
    public String getQrCode(GetQrCodeDto detQrCodeDto);
}
