package com.ruoyi.service.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(value = "SettlementDetailListVo")
public class SettlementDetailListVo {
    @ApiModelProperty("结算单号")
    private String settlementNumber;
    @ApiModelProperty("预计收益")
    private BigDecimal projectedIncomePrice;
    @ApiModelProperty("实际收益")
    private BigDecimal price;
    @ApiModelProperty("等级名称")
    private String levelName;
    @ApiModelProperty("状态 0- 受邀人查看 1-结算佣金/已完成 2-受邀人下单 3-受邀人付款 4-出行完成")
    private Integer status;

    @JsonIgnore
    private Long userIncomeId;
    @JsonIgnore
    private BigDecimal sourcePrice;
    @JsonIgnore
    private Long productId;
    @JsonIgnore
    private Long orderId;
}
