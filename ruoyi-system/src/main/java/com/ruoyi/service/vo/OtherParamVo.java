package com.ruoyi.service.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel(value = "OtherParamVo")
public class OtherParamVo {
    @ApiModelProperty("出行方式 0-组队出行 1-独立出行")
    private Integer travelType;
    @ApiModelProperty("出发时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date goDate;
    @ApiModelProperty("聊天群id")
    private Long chatGroupId;
}
