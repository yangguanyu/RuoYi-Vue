package com.ruoyi.service.service;

import java.util.List;
import com.ruoyi.service.domain.TfpUserShareLog;
import com.ruoyi.service.dto.RecordUserShareLogDto;
import com.ruoyi.service.vo.InvitingAchievementsVo;
import com.ruoyi.service.vo.ShareProductLogVo;

/**
 * 用户分享日志Service接口
 * 
 * @author ruoyi
 * @date 2024-01-25
 */
public interface ITfpUserShareLogService 
{
    /**
     * 查询用户分享日志
     * 
     * @param id 用户分享日志主键
     * @return 用户分享日志
     */
    public TfpUserShareLog selectTfpUserShareLogById(Long id);

    /**
     * 查询用户分享日志列表
     * 
     * @param tfpUserShareLog 用户分享日志
     * @return 用户分享日志集合
     */
    public List<TfpUserShareLog> selectTfpUserShareLogList(TfpUserShareLog tfpUserShareLog);

    /**
     * 新增用户分享日志
     * 
     * @param tfpUserShareLog 用户分享日志
     * @return 结果
     */
    public int insertTfpUserShareLog(TfpUserShareLog tfpUserShareLog);

    /**
     * 修改用户分享日志
     * 
     * @param tfpUserShareLog 用户分享日志
     * @return 结果
     */
    public int updateTfpUserShareLog(TfpUserShareLog tfpUserShareLog);

    /**
     * 批量删除用户分享日志
     * 
     * @param ids 需要删除的用户分享日志主键集合
     * @return 结果
     */
    public int deleteTfpUserShareLogByIds(Long[] ids);

    /**
     * 删除用户分享日志信息
     * 
     * @param id 用户分享日志主键
     * @return 结果
     */
    public int deleteTfpUserShareLogById(Long id);

    int recordUserShareLog(RecordUserShareLogDto recordUserShareLogDto);

    List<ShareProductLogVo> selectShareProductLogList(Long userId);

    List<InvitingAchievementsVo> invitingAchievements(Long userId);
}
