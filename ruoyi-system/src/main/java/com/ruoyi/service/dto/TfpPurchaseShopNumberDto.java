package com.ruoyi.service.dto;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 购物店数量对象 tfp_purchase_shop_number
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@Data
public class TfpPurchaseShopNumberDto extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 商品表id */
    private Long productId;

    /** 地点名称 */
    private String areaName;

    /** 购物场所名称 */
    private String purchasePlaceName;

    /** 主要商品信息 */
    private String mainProductInfo;

    /** 最长停留时间 */
    private String maxResidenceTime;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    private Long createUserId;

    /** 更新者id */
    private Long updateUserId;
}
