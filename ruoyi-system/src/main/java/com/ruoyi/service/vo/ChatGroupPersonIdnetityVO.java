package com.ruoyi.service.vo;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 聊天群组成员对象 tfp_chat_group_person
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@Data
@ApiModel(value = "ChatGroupPersonIdnetityVO", description = "人员在群组里身份信息")
public class ChatGroupPersonIdnetityVO
{
    private static final long serialVersionUID = 1L;


    /** 聊天群组id */
    @ApiModelProperty(name = "聊天群组id")
    private Long chatGroupId;

    /** 商品id */
    @ApiModelProperty(name = "商品id")
    private Long productId;

    /** 用户id */
    @ApiModelProperty(name = "用户id")
    private Long userId;

    /** 用户类型 0-发起者 1-已报名 2-未报名 */
    @ApiModelProperty(name = "用户类型 0-发起者 1-已报名 2-未报名")
    private Integer userType;



}
