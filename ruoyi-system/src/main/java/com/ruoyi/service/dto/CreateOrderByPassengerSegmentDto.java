package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("CreateOrderByPassengerSegmentDto")
public class CreateOrderByPassengerSegmentDto {
    @ApiModelProperty("航班号")
    private String flightNo;
    @ApiModelProperty("航班时间(格式：yyyy-MM-dd HH:mm)")
    private String depDate;
    @ApiModelProperty("航班抵达时间(格式：yyyy-MM-dd HH:mm)")
    private String arrDate;
    @ApiModelProperty("出发如：SZX（机场三字码）")
    private String depCode;
    @ApiModelProperty("抵达如：SZX (机场三字码)")
    private String arrCode;
    @ApiModelProperty("舱位。大写如：Y")
    private String cabin;
    @ApiModelProperty("携带儿童为必填。儿童舱位。大写如：Y")
    private String chdCabin;
    @ApiModelProperty("婴儿跟随成人舱位")
    private String infCabin;
}
