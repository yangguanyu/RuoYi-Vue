package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.area.domain.entity.SysAreaHot;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysAreaHotMapper;
import com.ruoyi.system.service.ISysAreaHotService;
import com.ruoyi.common.core.text.Convert;

/**
 * 地区热门推荐Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-10-17
 */
@Service
public class SysAreaHotServiceImpl implements ISysAreaHotService 
{
    @Autowired
    private SysAreaHotMapper sysAreaHotMapper;

    /**
     * 查询地区热门推荐
     * 
     * @param id 地区热门推荐主键
     * @return 地区热门推荐
     */
    @Override
    public SysAreaHot selectSysAreaHotById(Long id)
    {
        return sysAreaHotMapper.selectSysAreaHotById(id);
    }

    /**
     * 查询地区热门推荐列表
     * 
     * @param sysAreaHot 地区热门推荐
     * @return 地区热门推荐
     */
    @Override
    public List<SysAreaHot> selectSysAreaHotList(SysAreaHot sysAreaHot)
    {
        return sysAreaHotMapper.selectSysAreaHotList(sysAreaHot);
    }

    /**
     * 新增地区热门推荐
     * 
     * @param sysAreaHot 地区热门推荐
     * @return 结果
     */
    @Override
    public int insertSysAreaHot(SysAreaHot sysAreaHot)
    {
        sysAreaHot.setCreateTime(DateUtils.getNowDate());
        return sysAreaHotMapper.insertSysAreaHot(sysAreaHot);
    }

    /**
     * 修改地区热门推荐
     * 
     * @param sysAreaHot 地区热门推荐
     * @return 结果
     */
    @Override
    public int updateSysAreaHot(SysAreaHot sysAreaHot)
    {
        sysAreaHot.setUpdateTime(DateUtils.getNowDate());
        return sysAreaHotMapper.updateSysAreaHot(sysAreaHot);
    }

    /**
     * 批量删除地区热门推荐
     * 
     * @param ids 需要删除的地区热门推荐主键
     * @return 结果
     */
    @Override
    public int deleteSysAreaHotByIds(String ids)
    {
        return sysAreaHotMapper.deleteSysAreaHotByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除地区热门推荐信息
     * 
     * @param id 地区热门推荐主键
     * @return 结果
     */
    @Override
    public int deleteSysAreaHotById(Long id)
    {
        return sysAreaHotMapper.deleteSysAreaHotById(id);
    }

    /**
     * 通过国家类型获取热门区域信息
     * @param countryType
     * @return
     */
    @Override
    public List<SysAreaHot> getAreaHotListByCountryType(Integer countryType){
        SysAreaHot sysAreaHot = new SysAreaHot();
        sysAreaHot.setCountryType(countryType);
        return sysAreaHotMapper.selectSysAreaHotList(sysAreaHot);
    }
}
