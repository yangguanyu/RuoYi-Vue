package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "GetOrCreateQrCodeDTO")
public class GetOrCreateQrCodeDTO {
    @ApiModelProperty(value = "业务id")
    private Long businessId;
    @ApiModelProperty(value = "业务类型")
    private String businessType;
    @ApiModelProperty(value = "业务子类型")
    private String businessSubType;
    @ApiModelProperty(value = "生成二维码微信参数")
    private WxSharingLinksReqDTO wxSharingLinksReqDTO;
}
