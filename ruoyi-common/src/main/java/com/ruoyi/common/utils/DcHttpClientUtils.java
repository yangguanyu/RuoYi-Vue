package com.ruoyi.common.utils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.Objects;

public class DcHttpClientUtils {

    public static String httpPost(String url, String param, String userName, String sign){
        HttpPost httpPost = new HttpPost(url);

        if (Objects.nonNull(userName)){
            httpPost.setHeader("USERNAME", userName);
        }

        if (Objects.nonNull(sign)){
            httpPost.setHeader("SIGN", sign);
        }

        CloseableHttpClient httpClient = HttpClients.createDefault();
        try {
            // json方式
            StringEntity entity = new StringEntity(param,"utf-8");//解决中文乱码问题
            entity.setContentEncoding("UTF-8");
            entity.setContentType("application/json");
            httpPost.setEntity(entity);
            HttpResponse resp = httpClient.execute(httpPost);
            if(resp.getStatusLine().getStatusCode() == 200) {
                HttpEntity he = resp.getEntity();
                return EntityUtils.toString(he,"UTF-8");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
