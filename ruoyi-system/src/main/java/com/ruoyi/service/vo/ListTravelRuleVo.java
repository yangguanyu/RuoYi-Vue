package com.ruoyi.service.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel(value = "ListTravelRuleVo")
public class ListTravelRuleVo {
    @ApiModelProperty("主键")
    private Long id;
    @ApiModelProperty("类型 0-旅行方式")
    private Integer type;
    @ApiModelProperty("规则类型 0-组队出行 1-独立出行")
    private Integer travelType;
    @ApiModelProperty("规则类型名称")
    private String travelTypeName;
    @ApiModelProperty("版本号")
    private String version;
    @ApiModelProperty("上传时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date createTime;
    @ApiModelProperty("操作人")
    private String realName;

    @JsonIgnore
    private Long userId;
    @JsonIgnore
    private String userName;
}
