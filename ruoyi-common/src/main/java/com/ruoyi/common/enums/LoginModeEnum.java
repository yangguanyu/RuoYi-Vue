package com.ruoyi.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 登录方式
 *
 * @author
 * @date
 */
@Getter
@AllArgsConstructor
public enum LoginModeEnum {

    /**
     * 密码模式
     */
    PASSWORD(0, "密码模式"),

    /**
     * 证书登录模式
     */
    CERTIFICATION(1, "证书登录模式"),

    /**
     * 微信登录模式
     */
    WX(2, "微信登录模式");

    private Integer value;
    private String desc;
}
