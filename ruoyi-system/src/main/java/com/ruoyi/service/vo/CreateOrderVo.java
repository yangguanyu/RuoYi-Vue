package com.ruoyi.service.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(value = "CreateOrderVo", description = "订单支付vo")
public class CreateOrderVo {
    @ApiModelProperty("订单id")
    private Long orderId;
    @ApiModelProperty("订单金额")
    private BigDecimal finalAmount;
    @ApiModelProperty("预支付交易会话标识")
    private String prepayId;
    @ApiModelProperty("订单编码/微信支付outTradeNo")
    private String orderNo;
    @ApiModelProperty("前端支付需要参数")
    private RequestPaymentVo requestPayment;
}
