package com.ruoyi.service.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel("ServiceDicDataVo")
public class ServiceDicDataVo {
    @ApiModelProperty("类型")
    private String type;
    @ApiModelProperty("字典信息")
    private List<DicDataVo> dicDataVoList;
}
