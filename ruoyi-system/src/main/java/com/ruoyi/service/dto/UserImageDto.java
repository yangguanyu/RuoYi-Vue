package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("UserImageDto")
public class UserImageDto {
    @ApiModelProperty("图片url")
    private String url;
    @ApiModelProperty("是否是头像 0-否 1-是")
    private Integer headSculptureFlag;
}
