package com.ruoyi.service.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
@ApiModel(value = "ProductDetailVo", description = "商品详情")
public class ProductDetailVo {
    @ApiModelProperty("商品id")
    private Long id;

    @ApiModelProperty("商品名称")
    private String productName;
    @ApiModelProperty("商品编号")
    private String productNo;
    @ApiModelProperty("价格")
    private BigDecimal price;
    @ApiModelProperty("服务类型id 1-全国散 2-一家一团 3-自营 4-全国散-小包团")
    private Long categoryId;
    @ApiModelProperty("服务类型名称")
    private String categoryName;
    @ApiModelProperty("想去人数")
    private Integer wantToGoPersonNumber;

    @ApiModelProperty("行程天数 几天")
    private Integer dayNum;
    @ApiModelProperty("行程天数 几晚")
    private Integer nightNum;
    @ApiModelProperty("出发地名称")
    private String goPlaceName;
    @ApiModelProperty("目的地")
    private String backPlaceName;
    @ApiModelProperty("商品归属")
    private Integer commoditiesType;
    @ApiModelProperty("商品归属名称")
    private String commoditiesTypeName;

    @ApiModelProperty("线路亮点集合")
    private List<String> routeHighlightList;

    @ApiModelProperty("是否想去 0-否 1-是")
    private Integer wantToGoFlag;

    @ApiModelProperty("聊天群信息集合 根据服务类型决定多个群组还是单个群组")
    private List<ChatGroupInfoVo> chatGroupInfoList;

    @ApiModelProperty("日期价格集合")
    private List<DateMonthVO> datePriceVoList;

    @ApiModelProperty("外部日期集合，价格列表，一共10个")
    private List<DateCalendarVo> outPriceVoList;

    @ApiModelProperty("想去人数头像url集合")
    private List<String> wanToGoPersonHeadPortraitUrlList;

    @ApiModelProperty("商品图片url集合")
    private List<String> productImageUrlList;
    @ApiModelProperty("商品线路亮点url")
    private List<String> routeHighlightImageUrlList;
    @ApiModelProperty("商品线路亮点url--待删除")
    private String routeHighlightImageUrl;
    @ApiModelProperty("商品封面图片url集合")
    private List<String> productCoverImageUrlList;

    @ApiModelProperty("商品描述")
    private String productDescribe;

    @ApiModelProperty("线路类型 0-出发地参团 1-目的地参团")
    private String lineTypeCode;

    @ApiModelProperty("是否选择低价机票服务 0-否 1-是")
    private Integer lowPriceAirTicketServiceFlag;
}
