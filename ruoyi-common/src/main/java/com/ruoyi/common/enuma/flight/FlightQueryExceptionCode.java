package com.ruoyi.common.enuma.flight;

import com.ruoyi.common.enuma.BaseStrEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum FlightQueryExceptionCode implements BaseStrEnum {
    FLIGHT_NOT_FOUND("010101", "未能查询到符合要求的航班"),
    ROUTE_NOT_OPEN("010102", "未能查询到符合要求的航班，航线尚未开通"),
    INVALID_ORIGIN_OR_DESTINATION("010104", "您查询的出发地或抵达地无效");

    private final String value;
    private final String desc;


}
