package com.ruoyi.service.service.impl;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.*;
import java.util.stream.Collectors;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enuma.*;
import com.ruoyi.common.exception.BusinessException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.DcDateUtils;
import com.ruoyi.common.utils.DcListUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.core.domain.entity.TfpAttachment;
import com.ruoyi.service.domain.TfpOrder;
import com.ruoyi.service.domain.TfpProduct;
import com.ruoyi.service.domain.TfpUserIncome;
import com.ruoyi.service.dto.SettlementDetailListDto;
import com.ruoyi.service.dto.ExportDto;
import com.ruoyi.service.dto.UserIncomeDetailManageListDto;
import com.ruoyi.service.dto.ViewShareInfoDto;
import com.ruoyi.service.excel.UserIncomeDetailExportExcel;
import com.ruoyi.service.service.*;
import com.ruoyi.service.vo.*;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpUserIncomeDetailMapper;
import com.ruoyi.service.domain.TfpUserIncomeDetail;
import org.springframework.transaction.annotation.Transactional;

import static com.ruoyi.common.utils.PageUtils.startPage;

/**
 * 用户收益明细Service业务层处理
 *
 * @author ruoyi
 * @date 2024-01-16
 */
@Service
@Transactional
public class TfpUserIncomeDetailServiceImpl implements ITfpUserIncomeDetailService
{
    @Autowired
    private TfpUserIncomeDetailMapper tfpUserIncomeDetailMapper;
    @Autowired
    private ITfpUserIncomeService userIncomeService;
    @Autowired
    private ITfpUserShareLogService userShareLogService;
    @Autowired
    private ITfpProductService productService;
    @Autowired
    private ITfpAttachmentService attachmentService;
    @Autowired
    private ITfpOrderService orderService;
    @Autowired
    private ISysUserService userService;
    @Autowired
    private ITfpProductDepartureDateService productDepartureDateService;

    @Value("${level.code.one.percent}")
    private BigDecimal levelCodeOnePercent;

    /**
     * 查询用户收益明细
     *
     * @param id 用户收益明细主键
     * @return 用户收益明细
     */
    @Override
    public TfpUserIncomeDetail selectTfpUserIncomeDetailById(Long id)
    {
        return tfpUserIncomeDetailMapper.selectTfpUserIncomeDetailById(id);
    }

    /**
     * 查询用户收益明细列表
     *
     * @param tfpUserIncomeDetail 用户收益明细
     * @return 用户收益明细
     */
    @Override
    public List<TfpUserIncomeDetail> selectTfpUserIncomeDetailList(TfpUserIncomeDetail tfpUserIncomeDetail)
    {
        return tfpUserIncomeDetailMapper.selectTfpUserIncomeDetailList(tfpUserIncomeDetail);
    }

    /**
     * 新增用户收益明细
     *
     * @param tfpUserIncomeDetail 用户收益明细
     * @return 结果
     */
    @Override
    public int insertTfpUserIncomeDetail(TfpUserIncomeDetail tfpUserIncomeDetail)
    {
        tfpUserIncomeDetail.setCreateTime(DateUtils.getNowDate());
        return tfpUserIncomeDetailMapper.insertTfpUserIncomeDetail(tfpUserIncomeDetail);
    }

    /**
     * 修改用户收益明细
     *
     * @param tfpUserIncomeDetail 用户收益明细
     * @return 结果
     */
    @Override
    public int updateTfpUserIncomeDetail(TfpUserIncomeDetail tfpUserIncomeDetail)
    {
        tfpUserIncomeDetail.setUpdateTime(DateUtils.getNowDate());
        return tfpUserIncomeDetailMapper.updateTfpUserIncomeDetail(tfpUserIncomeDetail);
    }

    /**
     * 批量删除用户收益明细
     *
     * @param ids 需要删除的用户收益明细主键
     * @return 结果
     */
    @Override
    public int deleteTfpUserIncomeDetailByIds(Long[] ids)
    {
        return tfpUserIncomeDetailMapper.deleteTfpUserIncomeDetailByIds(ids);
    }

    /**
     * 删除用户收益明细信息
     *
     * @param id 用户收益明细主键
     * @return 结果
     */
    @Override
    public int deleteTfpUserIncomeDetailById(Long id)
    {
        return tfpUserIncomeDetailMapper.deleteTfpUserIncomeDetailById(id);
    }

    /**
     * 小程序-受邀人查看分享信息
     * @param viewShareInfoDto
     * @return
     */
    @Override
    public int viewShareInfo(ViewShareInfoDto viewShareInfoDto) {
        Long shareUserId = viewShareInfoDto.getShareUserId();
        if (Objects.isNull(shareUserId)){
            throw new RuntimeException("分享用户id不能为空");
        }

        LoginUser loginUser = SecurityUtils.getLoginUser();

        Integer sourceType = viewShareInfoDto.getSourceType();
        Long sourceId = viewShareInfoDto.getSourceId();

        TfpUserIncomeDetail param = new TfpUserIncomeDetail();
        param.setUserId(shareUserId);
        param.setSourceType(sourceType);
        param.setSourceId(sourceId);
        param.setInviteeUserId(loginUser.getUserId());
        TfpUserIncomeDetail userIncomeDetail = tfpUserIncomeDetailMapper.getTfpUserIncomeDetailBySourceInfoOfOrderNumberIsNull(param);
        if (Objects.nonNull(userIncomeDetail)){
            userIncomeDetail.setViewTime(DateUtils.getNowDate());
            userIncomeDetail.setUpdateUserId(loginUser.getUserId());
            userIncomeDetail.setUpdateBy(loginUser.getUsername());
            userIncomeDetail.setUpdateTime(DateUtils.getNowDate());
            tfpUserIncomeDetailMapper.updateTfpUserIncomeDetail(userIncomeDetail);
        } else {
            TfpUserIncome userIncome = userIncomeService.getUserIncomeByUserId(shareUserId);

            userIncomeDetail = new TfpUserIncomeDetail();
            userIncomeDetail.setUserIncomeId(userIncome.getId());
            userIncomeDetail.setUserId(shareUserId);
            userIncomeDetail.setInviteeUserId(loginUser.getUserId());
            userIncomeDetail.setSourceType(sourceType);
            userIncomeDetail.setSourceId(sourceId);
            userIncomeDetail.setStatus(UserIncomeDetailStatusEnum.VIEW.getValue());
            userIncomeDetail.setViewTime(DateUtils.getNowDate());
            userIncomeDetail.setDelFlag(DeleteEnum.EXIST.getValue());
            userIncomeDetail.setCreateUserId(loginUser.getUserId());
            userIncomeDetail.setCreateBy(loginUser.getUsername());
            userIncomeDetail.setCreateTime(DateUtils.getNowDate());
            tfpUserIncomeDetailMapper.insertTfpUserIncomeDetail(userIncomeDetail);
        }

        return 1;
    }

    @Override
    public void updateShareInfo(Long shareUserId, Integer sourceType, Long sourceId, Integer status, Long orderId, String orderNumber, BigDecimal sourcePrice){
        List<Integer> statusList = new ArrayList<>();
        statusList.add(UserIncomeDetailStatusEnum.SETTLEMENT_COMMISSION.getValue());
        statusList.add(UserIncomeDetailStatusEnum.PLACE_ORDER.getValue());
        statusList.add(UserIncomeDetailStatusEnum.PAY.getValue());
        statusList.add(UserIncomeDetailStatusEnum.TRAVEL_COMPLETE.getValue());

        if (statusList.contains(status)){
            TfpUserIncomeDetail userIncomeDetail;
            if (Objects.equals(UserIncomeDetailStatusEnum.PLACE_ORDER.getValue(), status)){
                LoginUser loginUser = SecurityUtils.getLoginUser();
                TfpUserIncomeDetail param = new TfpUserIncomeDetail();
                param.setUserId(shareUserId);
                param.setSourceType(sourceType);
                param.setSourceId(sourceId);
                param.setInviteeUserId(loginUser.getUserId());
                userIncomeDetail = tfpUserIncomeDetailMapper.getTfpUserIncomeDetailBySourceInfoOfOrderNumberIsNull(param);

                // 结算单号待处理
                String settlementNumber = this.createSettlementNumber(userIncomeDetail.getId());

                // 受邀人下单
                userIncomeDetail.setStatus(UserIncomeDetailStatusEnum.PLACE_ORDER.getValue());
                userIncomeDetail.setOrderId(orderId);
                userIncomeDetail.setOrderNumber(orderNumber);
                userIncomeDetail.setSettlementNumber(settlementNumber);
                userIncomeDetail.setSourcePrice(sourcePrice);
                userIncomeDetail.setPlaceOrderTime(DateUtils.getNowDate());
            } else {
                TfpUserIncomeDetail param = new TfpUserIncomeDetail();
                param.setOrderId(orderId);
                param.setDelFlag(DeleteEnum.EXIST.getValue());
                List<TfpUserIncomeDetail> userIncomeDetailList = tfpUserIncomeDetailMapper.selectTfpUserIncomeDetailList(param);
                if (DcListUtils.isEmpty(userIncomeDetailList)){
                    return;
                }

                userIncomeDetail = userIncomeDetailList.get(0);

                if (Objects.equals(UserIncomeDetailStatusEnum.PAY.getValue(), status)){
                    // 受邀人付款
                    userIncomeDetail.setStatus(UserIncomeDetailStatusEnum.PAY.getValue());
                    userIncomeDetail.setPayTime(DateUtils.getNowDate());
                    userIncomeDetail.setUpdateBy("支付业务回调");
                } else {
                    if (Objects.equals(UserIncomeDetailStatusEnum.SETTLEMENT_COMMISSION.getValue(), status)){
                        LoginUser loginUser = SecurityUtils.getLoginUser();

                        // 结算佣金/已完成
                        userIncomeDetail.setStatus(UserIncomeDetailStatusEnum.SETTLEMENT_COMMISSION.getValue());
                        userIncomeDetail.setSettlementCommissionTime(DateUtils.getNowDate());
                        userIncomeDetail.setUpdateUserId(loginUser.getUserId());
                        userIncomeDetail.setUpdateBy(loginUser.getUsername());
                    }

                    if (Objects.equals(UserIncomeDetailStatusEnum.TRAVEL_COMPLETE.getValue(), status)){
                        // 出行完成
                        TfpUserIncome userIncome = userIncomeService.selectTfpUserIncomeById(userIncomeDetail.getUserIncomeId());
                        String levelCode = userIncome.getLevelCode();

                        TfpOrder tfpOrder = orderService.selectTfpOrderById(orderId);
                        BigDecimal price = this.handleUserIncomeDetailPrice(levelCode, tfpOrder.getProductId(), tfpOrder.getGoDate());
                        userIncomeDetail.setPrice(price);
                        userIncomeDetail.setStatus(UserIncomeDetailStatusEnum.TRAVEL_COMPLETE.getValue());
                        userIncomeDetail.setTravelCompleteTime(DateUtils.getNowDate());
                        userIncomeDetail.setUpdateBy("完成订单定时业务");
                    }
                }
            }

            userIncomeDetail.setUpdateTime(DateUtils.getNowDate());
            tfpUserIncomeDetailMapper.updateTfpUserIncomeDetail(userIncomeDetail);
        }
    }

    @Override
    public void handleUserIncomeAfterCancelOrder(String orderNumber) {
        TfpUserIncomeDetail param = new TfpUserIncomeDetail();
        param.setOrderNumber(orderNumber);
        param.setDelFlag(DeleteEnum.EXIST.getValue());
        List<TfpUserIncomeDetail> userIncomeDetailList = tfpUserIncomeDetailMapper.selectTfpUserIncomeDetailList(param);
        if (DcListUtils.isNotEmpty(userIncomeDetailList)){
            TfpUserIncomeDetail newTfpUserIncomeDetail = new TfpUserIncomeDetail();

            TfpUserIncomeDetail tfpUserIncomeDetail = userIncomeDetailList.get(0);
            BeanUtils.copyProperties(tfpUserIncomeDetail, newTfpUserIncomeDetail);

            // 重新添加数据
            newTfpUserIncomeDetail.setId(null);
            newTfpUserIncomeDetail.setOrderId(null);
            newTfpUserIncomeDetail.setOrderNumber(null);
            newTfpUserIncomeDetail.setSettlementNumber(null);
            newTfpUserIncomeDetail.setStatus(UserIncomeDetailStatusEnum.VIEW.getValue());
            tfpUserIncomeDetailMapper.insertTfpUserIncomeDetail(newTfpUserIncomeDetail);

            // 删除旧数据
            tfpUserIncomeDetailMapper.deleteTfpUserIncomeDetailById(tfpUserIncomeDetail.getId());
        }
    }

    /**
     * 获取累计收益(元)
     * @param userId
     * @return
     */
    @Override
    public BigDecimal getCumulativeIncomeByUserId(Long userId) {
        return tfpUserIncomeDetailMapper.getCumulativeIncomeByUserId(userId);
    }

    @Override
    public BigDecimal getUserCumulativeIncome() {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = loginUser.getUserId();
        BigDecimal userCumulativeIncome = tfpUserIncomeDetailMapper.getCumulativeIncomeByUserId(userId);
        return Objects.isNull(userCumulativeIncome) ? BigDecimal.ZERO : userCumulativeIncome;
    }

    /**
     * 小程序-用户收益列表
     * @return
     */
    @Override
    public List<UserIncomeDetailListVo> userIncomeDetailList() {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = loginUser.getUserId();

        startPage();
        return tfpUserIncomeDetailMapper.getUserIncomeDetailListByUserId(userId);
    }

    /**
     * 小程序-结算明细
     * @param settlementDetailListDto
     * @return
     */
    @Override
    public List<SettlementDetailListVo> settlementDetailList(SettlementDetailListDto settlementDetailListDto) {
        Integer settlementType = settlementDetailListDto.getSettlementType();
        if (Objects.nonNull(settlementType) && !Objects.equals(settlementType, 0) && !Objects.equals(settlementType, 1)){
            throw new BusinessException("传参数值有误");
        }

        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = loginUser.getUserId();

        List<Long> defineOrderIdList = new ArrayList<>();
        if (Objects.nonNull(settlementType)) {
            List<TfpOrder> orderList = orderService.queryOverRefundTimeCompleteOrderList(userId);
            if (DcListUtils.isNotEmpty(orderList)){
                defineOrderIdList = orderList.stream().map(TfpOrder::getId).collect(Collectors.toList());
            }
        }

        startPage();
        List<SettlementDetailListVo> settlementDetailListVoList = tfpUserIncomeDetailMapper.settlementDetailList(userId, settlementType, defineOrderIdList);
        if (DcListUtils.isNotEmpty(settlementDetailListVoList)){
            Map<Long, Date> goDateMap = new HashMap<>();
            List<Long> orderIdList = settlementDetailListVoList.stream().map(SettlementDetailListVo::getOrderId).distinct().collect(Collectors.toList());
            if (DcListUtils.isNotEmpty(orderIdList)){
                List<TfpOrder> tfpOrderList = orderService.selectTfpOrderByIdList(orderIdList);
                if (DcListUtils.isNotEmpty(tfpOrderList)){
                    goDateMap = tfpOrderList.stream().collect(Collectors.toMap(TfpOrder::getId, TfpOrder::getGoDate));
                }
            }

            Long userIncomeId = settlementDetailListVoList.get(0).getUserIncomeId();
            TfpUserIncome userIncome = userIncomeService.selectTfpUserIncomeById(userIncomeId);
            String levelCode = userIncome.getLevelCode();
            String levelName = userIncome.getLevelName();

            for (SettlementDetailListVo settlementDetailListVo : settlementDetailListVoList) {
                Date goDate = goDateMap.get(settlementDetailListVo.getOrderId());

                BigDecimal projectedIncomePrice = this.handleUserIncomeDetailPrice(levelCode, settlementDetailListVo.getProductId(), goDate);

                settlementDetailListVo.setProjectedIncomePrice(projectedIncomePrice);
                settlementDetailListVo.setLevelName(levelName);

                if (Objects.equals(UserIncomeDetailStatusEnum.SETTLEMENT_COMMISSION.getValue(), settlementDetailListVo.getStatus())){
                    settlementDetailListVo.setPrice(settlementDetailListVo.getPrice());
                }
            }
        }

        return settlementDetailListVoList;
    }

    /**
     * 小程序-邀请战绩
     * @return
     */
    @Override
    public List<InvitingAchievementsVo> invitingAchievements() {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = loginUser.getUserId();

        startPage();
        List<InvitingAchievementsVo> invitingAchievementsVoList = userShareLogService.invitingAchievements(userId);
        if (DcListUtils.isNotEmpty(invitingAchievementsVoList)){
            List<Long> productIdList = invitingAchievementsVoList.stream().map(InvitingAchievementsVo::getProductId).collect(Collectors.toList());

            Map<Long, TfpProduct> productMap;
            List<TfpProduct> productList = productService.selectTfpProductListByIdList(productIdList);
            if (DcListUtils.isNotEmpty(productList)){
                productMap = productList.stream().collect(Collectors.toMap(TfpProduct::getId, TfpProduct -> TfpProduct));
            } else {
                productMap = new HashMap<>();
            }

            Map<Long, List<TfpAttachment>> productImageMap;
            List<TfpAttachment> attachmentList = attachmentService.selectList(productIdList, null, BusinessSubTypeEnum.PRODUCT_IMAGE.getValue());
            if (DcListUtils.isNotEmpty(attachmentList)){
                productImageMap = attachmentList.stream().collect(Collectors.groupingBy(TfpAttachment::getBusinessId));
            } else {
                productImageMap = new HashMap<>();
            }

            Map<Long, Integer> viewMap;
            List<UserIncomeStatisticsInfo> viewProductList = tfpUserIncomeDetailMapper.selectViewProductList(userId, productIdList);
            if (DcListUtils.isNotEmpty(viewProductList)){
                viewMap = viewProductList.stream().collect(Collectors.toMap(UserIncomeStatisticsInfo::getSourceId, UserIncomeStatisticsInfo::getViewNumber));
            } else {
                viewMap = new HashMap<>();
            }

            Map<Long, Integer> placeOrderMap;
            List<UserIncomeStatisticsInfo> placeOrderProductList = tfpUserIncomeDetailMapper.selectPlaceOrderProductList(userId, productIdList);
            if (DcListUtils.isNotEmpty(placeOrderProductList)){
                placeOrderMap = placeOrderProductList.stream().collect(Collectors.toMap(UserIncomeStatisticsInfo::getSourceId, UserIncomeStatisticsInfo::getPlaceOrderNumber));
            } else {
                placeOrderMap = new HashMap<>();
            }

            for (InvitingAchievementsVo invitingAchievementsVo : invitingAchievementsVoList) {
                Long productId = invitingAchievementsVo.getProductId();
                TfpProduct tfpProduct = productMap.get(productId);

                Integer viewNumber = viewMap.get(productId);
                Integer placeOrderNumber = placeOrderMap.get(productId);
                invitingAchievementsVo.setViewNumber(Objects.isNull(viewNumber) ? 0 : viewNumber);
                invitingAchievementsVo.setPlaceOrderNumber(Objects.isNull(placeOrderNumber) ? 0 : placeOrderNumber);
                invitingAchievementsVo.setProductName(tfpProduct.getProductName());
                invitingAchievementsVo.setProductNo(tfpProduct.getProductNo());

                List<TfpAttachment> productAttachmentList = productImageMap.get(productId);
                if (DcListUtils.isNotEmpty(productAttachmentList)){
                    invitingAchievementsVo.setProductImageUrl(productAttachmentList.get(0).getUrl());
                }
            }
        }

        return invitingAchievementsVoList;
    }

    @Override
    public List<UserIncomeDetailManageListVo> userIncomeDetailManageList(UserIncomeDetailManageListDto userIncomeDetailManageListDto) {
        Long categoryId = userIncomeDetailManageListDto.getCategoryId();
        String productNo = userIncomeDetailManageListDto.getProductNo();

        List<Long> productIdList = new ArrayList<>();
        if (Objects.nonNull(categoryId) || Objects.nonNull(productNo)){
            TfpProduct param = new TfpProduct();
            param.setCategoryId(categoryId);
            param.setProductNo(productNo);
            param.setDelFlag(DeleteEnum.EXIST.getValue());
            List<TfpProduct> tfpProductList = productService.selectTfpProductList(param);

            if (DcListUtils.isNotEmpty(tfpProductList)){
                productIdList = tfpProductList.stream().map(TfpProduct::getId).collect(Collectors.toList());
            } else {
                return new ArrayList<>();
            }
        }

        List<Long> orderIdList = new ArrayList<>();;
        Integer afterSalesTimeFlag = userIncomeDetailManageListDto.getAfterSalesTimeFlag();

        if (DcListUtils.isNotEmpty(productIdList) || Objects.nonNull(afterSalesTimeFlag)){
            List<TfpOrder> orderList = orderService.queryUserIncomeDetailOrderList(productIdList, afterSalesTimeFlag);
            if (DcListUtils.isNotEmpty(orderList)){
                orderIdList = orderList.stream().map(TfpOrder::getId).collect(Collectors.toList());
            }
        }

        startPage();
        List<UserIncomeDetailManageListVo> userIncomeDetailManageListVoList = tfpUserIncomeDetailMapper.userIncomeDetailManageList(userIncomeDetailManageListDto, productIdList, orderIdList);
        this.handleUserIncomeDetailManageList(userIncomeDetailManageListVoList);
        return userIncomeDetailManageListVoList;
    }

    @Override
    public List<UserIncomeDetailExportExcel> userIncomeDetailManageListExport(ExportDto exportDto) {
        Integer allExportFlag = exportDto.getAllExportFlag();
        if (Objects.isNull(allExportFlag)){
            throw new BusinessException("请选择导出的类型");
        }

        List<Long> idList = exportDto.getIdList();
        if (Objects.equals(YesOrNoFlagEnum.NO.getValue(), allExportFlag) && DcListUtils.isEmpty(idList)){
            throw new BusinessException("请选择需要导出的数据");
        }

        List<Long> orderIdList = new ArrayList<>();
        List<TfpOrder> orderList = orderService.queryUserIncomeDetailOrderList(new ArrayList<>(), null);
        if (DcListUtils.isNotEmpty(orderList)){
            orderIdList = orderList.stream().map(TfpOrder::getId).collect(Collectors.toList());
        }

        List<UserIncomeDetailManageListVo> userIncomeDetailManageListVoList = tfpUserIncomeDetailMapper.exportUserIncomeDetailManageList(exportDto, orderIdList);
        this.handleUserIncomeDetailManageList(userIncomeDetailManageListVoList);

        List<UserIncomeDetailExportExcel> userIncomeDetailExportExcelList = new ArrayList<>();
        if (DcListUtils.isNotEmpty(userIncomeDetailManageListVoList)){
            for (UserIncomeDetailManageListVo userIncomeDetailManageListVo : userIncomeDetailManageListVoList) {
                UserIncomeDetailExportExcel userIncomeDetailExportExcel = new UserIncomeDetailExportExcel();
                BeanUtils.copyProperties(userIncomeDetailManageListVo, userIncomeDetailExportExcel);

                userIncomeDetailExportExcel.setAfterSalesTimeFlagName(BaseEnum.getDescByCode(YesOrNoFlagEnum.class, userIncomeDetailExportExcel.getAfterSalesTimeFlag()));
                if (Objects.equals(YesOrNoFlagEnum.YES.getValue(), userIncomeDetailExportExcel.getSettlementReqStatus())){
                    userIncomeDetailExportExcel.setSettlementReqStatusName("已结算");
                } else {
                    userIncomeDetailExportExcel.setSettlementReqStatusName("未结算");
                }

                userIncomeDetailExportExcelList.add(userIncomeDetailExportExcel);
            }
        }

        return userIncomeDetailExportExcelList;
    }

    @Override
    public void handleSettlementCompleteTask(List<Long> orderIdList) {
        if (DcListUtils.isNotEmpty(orderIdList)){
            List<TfpUserIncomeDetail> tfpUserIncomeDetailList = tfpUserIncomeDetailMapper.selectOverRefundTimeCompleteSettlementList(orderIdList);
            if (DcListUtils.isNotEmpty(tfpUserIncomeDetailList)){
                for (TfpUserIncomeDetail tfpUserIncomeDetail : tfpUserIncomeDetailList) {
                    tfpUserIncomeDetail.setStatus(UserIncomeDetailStatusEnum.SETTLEMENT_COMMISSION.getValue());
                    tfpUserIncomeDetail.setSettlementCommissionTime(new Date());
                    tfpUserIncomeDetailMapper.updateTfpUserIncomeDetail(tfpUserIncomeDetail);

                    BigDecimal price = tfpUserIncomeDetail.getPrice();
                    if (Objects.nonNull(price)){
                        Long userIncomeId = tfpUserIncomeDetail.getUserIncomeId();
                        TfpUserIncome userIncome = userIncomeService.selectTfpUserIncomeById(userIncomeId);
                        BigDecimal myRemainingPrice = userIncome.getRemainingPrice();
                        BigDecimal remainingPrice = myRemainingPrice.add(price);
                        userIncome.setRemainingPrice(remainingPrice);
                        userIncomeService.updateTfpUserIncome(userIncome);
                    }
                }
            }
        }
    }

    private void handleUserIncomeDetailManageList(List<UserIncomeDetailManageListVo> userIncomeDetailManageListVoList){
        if (DcListUtils.isNotEmpty(userIncomeDetailManageListVoList)){
            List<Long> productIdList = new ArrayList<>();
            List<Long> orderIdList = new ArrayList<>();

            List<Long> userIdList = new ArrayList<>();
            for (UserIncomeDetailManageListVo userIncomeDetailManageListVo : userIncomeDetailManageListVoList) {
                Long productId = userIncomeDetailManageListVo.getProductId();
                if (!productIdList.contains(productId)){
                    productIdList.add(productId);
                }

                Long orderId = userIncomeDetailManageListVo.getOrderId();
                if (!orderIdList.contains(orderId)){
                    orderIdList.add(orderId);
                }

                Long userId = userIncomeDetailManageListVo.getUserId();
                if (!userIdList.contains(userId)){
                    userIdList.add(userId);
                }
            }

            List<TfpProduct> tfpProductList = productService.selectTfpProductListByIdList(productIdList);
            Map<Long, TfpProduct> productMap;
            if (DcListUtils.isNotEmpty(tfpProductList)){
                productMap = tfpProductList.stream().collect(Collectors.toMap(TfpProduct::getId, TfpProduct -> TfpProduct));
            } else {
                productMap = new HashMap<>();
            }

            List<TfpOrder> orderList = orderService.selectTfpOrderByIdList(orderIdList);
            Map<Long, TfpOrder> orderMap;
            if (DcListUtils.isNotEmpty(orderList)){
                orderMap = orderList.stream().collect(Collectors.toMap(TfpOrder::getId, TfpOrder -> TfpOrder));
            } else {
                orderMap = new HashMap<>();
            }

            List<SysUser> userList = userService.selectUserInfoByUserIdList(userIdList);
            Map<Long, SysUser> userMap;
            if (DcListUtils.isNotEmpty(userList)){
                userMap = userList.stream().collect(Collectors.toMap(SysUser::getUserId, SysUser -> SysUser));
            } else {
                userMap = new HashMap<>();
            }

            for (UserIncomeDetailManageListVo userIncomeDetailManageListVo : userIncomeDetailManageListVoList) {
                TfpProduct tfpProduct = productMap.get(userIncomeDetailManageListVo.getProductId());
                if (Objects.nonNull(tfpProduct)){
                    userIncomeDetailManageListVo.setProductNo(tfpProduct.getProductNo());
                    userIncomeDetailManageListVo.setCategoryName(tfpProduct.getCategoryName());
                }

                TfpOrder tfpOrder = orderMap.get(userIncomeDetailManageListVo.getOrderId());
                if (Objects.nonNull(tfpOrder)){
                    userIncomeDetailManageListVo.setOrderNo(tfpOrder.getOrderNo());

                    Integer status = tfpOrder.getStatus();
                    Date refundEndTime = tfpOrder.getRefundEndTime();

                    Integer settlementReqStatus;
                    if (Objects.equals(status, OrderStatusEnum.COMPLETE.getValue()) && Objects.nonNull(refundEndTime)
                        && refundEndTime.before(new Date())){
                        settlementReqStatus = SettlementStatusEnum.SETTLEMENT.getValue();
                    } else {
                        settlementReqStatus = SettlementStatusEnum.NOT_SETTLEMENT.getValue();
                    }

                    Integer afterSalesTimeFlag;
                    if (Objects.nonNull(refundEndTime) && refundEndTime.before(new Date())){
                        afterSalesTimeFlag = YesOrNoFlagEnum.YES.getValue();
                    } else {
                        afterSalesTimeFlag = YesOrNoFlagEnum.NO.getValue();
                    }

                    userIncomeDetailManageListVo.setAfterSalesTimeFlag(afterSalesTimeFlag);
                    userIncomeDetailManageListVo.setSettlementReqStatus(settlementReqStatus);
                }

                SysUser sysUser = userMap.get(userIncomeDetailManageListVo.getUserId());
                if (Objects.nonNull(sysUser)){
                    userIncomeDetailManageListVo.setRealName(sysUser.getRealName());
                }

                BigDecimal price = userIncomeDetailManageListVo.getPrice();
                if (Objects.isNull(price)){
                    userIncomeDetailManageListVo.setPrice(BigDecimal.ZERO);
                }
            }
        }
    }

    private BigDecimal handleUserIncomeDetailPrice(String levelCode, Long productId, Date goDate){
        BigDecimal projectedIncomePrice = BigDecimal.ZERO;
        BigDecimal adultRetailPrice = productDepartureDateService.getAdultRetailPriceByProductIdAndDate(productId, goDate);

        // 业务待处理
        if (Objects.nonNull(adultRetailPrice)){
            if (Objects.equals(IncomeLevelCodeEnum.ONE.getValue(), levelCode)){
                projectedIncomePrice = adultRetailPrice.multiply(levelCodeOnePercent);
            }
        }

        return projectedIncomePrice;
    }

    private String createSettlementNumber(Long id) {
        String settlementNumber;
        // yyyyMMddHHmmss
        DateFormat yMdDateFormat = DcDateUtils.getDcDateFormat(DcDateUtils.yMdHms);
        String DateFormatStr = yMdDateFormat.format(new Date());

        int idLength = id.toString().length();
        if (idLength > 6){
            settlementNumber = DateFormatStr + String.format("%0" + idLength + "d", id);
        } else {
            settlementNumber = DateFormatStr + String.format("%06d", id);
        }

        return settlementNumber;
    }
}
