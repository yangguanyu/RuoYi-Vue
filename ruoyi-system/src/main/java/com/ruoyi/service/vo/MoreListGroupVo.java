package com.ruoyi.service.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@ApiModel
public class MoreListGroupVo {
    @ApiModelProperty("日期")
    private Date monthDay;
    @ApiModelProperty("展示日期 xxxx年xx月")
    private String monthDayStr;
    @ApiModelProperty("聊天室集合")
    private List<MoreListMonthGroupVo> monthGroupList;
}
