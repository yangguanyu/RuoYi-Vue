package com.ruoyi.system.service;

import com.ruoyi.common.area.domain.entity.SysArea;
import com.ruoyi.common.area.domain.model.AreaTreeDTO;
import com.ruoyi.common.area.domain.model.DepartureAreaDTO;

import java.util.List;

/**
 * 地区Service接口
 * 
 * @author ruoyi
 * @date 2023-10-15
 */
public interface ISysAreaService 
{
    /**
     * 查询地区
     * 
     * @param id 地区主键
     * @return 地区
     */
    public SysArea selectSysAreaById(Long id);

    /**
     * 查询地区列表
     * 
     * @param sysArea 地区
     * @return 地区集合
     */
    public List<SysArea> selectSysAreaList(SysArea sysArea);

    /**
     * 新增地区
     * 
     * @param sysArea 地区
     * @return 结果
     */
    public int insertSysArea(SysArea sysArea);

    /**
     * 修改地区
     * 
     * @param sysArea 地区
     * @return 结果
     */
    public int updateSysArea(SysArea sysArea);

    /**
     * 批量删除地区
     * 
     * @param ids 需要删除的地区主键集合
     * @return 结果
     */
    public int deleteSysAreaByIds(String ids);

    /**
     * 删除地区信息
     * 
     * @param id 地区主键
     * @return 结果
     */
    public int deleteSysAreaById(Long id);

    /**
     * 获取出发地
     */
    DepartureAreaDTO getDepartureArea();

    /**
     * 获取业务区域
     * @return
     */
    List<AreaTreeDTO> getServiceAreaList();

    /**
     * 获取省市区域
     * @return
     */
    List<AreaTreeDTO> getAreaList(boolean containAreaFlag);

    /**
     * 小程序-查询获取出发地区域名称列表
      * @param key
     * @return
     */
    List<String> searchDepartureAreaNameByKey(String key);
}
