package com.ruoyi.service.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 用户个性标签对象 sys_user_label
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public class SysUserLabel extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 标签类型code */
    @Excel(name = "标签类型code")
    private String labelTypeCode;

    /** 标签类型名称 */
    @Excel(name = "标签类型名称")
    private String labelTypeName;

    /** 标签code */
    @Excel(name = "标签code")
    private String labelCode;

    /** 标签名称 */
    @Excel(name = "标签名称")
    private String labelName;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    @Excel(name = "创建者id")
    private Long createUserId;

    /** 更新者id */
    @Excel(name = "更新者id")
    private Long updateUserId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setLabelTypeCode(String labelTypeCode) 
    {
        this.labelTypeCode = labelTypeCode;
    }

    public String getLabelTypeCode() 
    {
        return labelTypeCode;
    }
    public void setLabelTypeName(String labelTypeName) 
    {
        this.labelTypeName = labelTypeName;
    }

    public String getLabelTypeName() 
    {
        return labelTypeName;
    }
    public void setLabelCode(String labelCode) 
    {
        this.labelCode = labelCode;
    }

    public String getLabelCode() 
    {
        return labelCode;
    }
    public void setLabelName(String labelName) 
    {
        this.labelName = labelName;
    }

    public String getLabelName() 
    {
        return labelName;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }
    public void setUpdateUserId(Long updateUserId) 
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId() 
    {
        return updateUserId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("labelTypeCode", getLabelTypeCode())
            .append("labelTypeName", getLabelTypeName())
            .append("labelCode", getLabelCode())
            .append("labelName", getLabelName())
            .append("delFlag", getDelFlag())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
