package com.ruoyi.web.controller.service;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.service.dto.WantToGoDto;
import com.ruoyi.service.vo.ListProductVo;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.TfpProductWantToGo;
import com.ruoyi.service.service.ITfpProductWantToGoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商品想去Controller
 * 
 * @author ruoyi
 * @date 2023-12-26
 */
@RestController
@RequestMapping("/service/productWantToGo")
@ApiOperation("商品想去")
public class TfpProductWantToGoController extends BaseController
{
    @Autowired
    private ITfpProductWantToGoService tfpProductWantToGoService;

    /**
     * 查询商品想去列表
     */
    @PreAuthorize("@ss.hasPermi('service:productWantToGo:list')")
    @GetMapping("/list")
    public TableDataInfo list(TfpProductWantToGo tfpProductWantToGo)
    {
        startPage();
        List<TfpProductWantToGo> list = tfpProductWantToGoService.selectTfpProductWantToGoList(tfpProductWantToGo);
        return getDataTable(list);
    }

    /**
     * 导出商品想去列表
     */
    @PreAuthorize("@ss.hasPermi('service:productWantToGo:export')")
    @Log(title = "商品想去", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TfpProductWantToGo tfpProductWantToGo)
    {
        List<TfpProductWantToGo> list = tfpProductWantToGoService.selectTfpProductWantToGoList(tfpProductWantToGo);
        ExcelUtil<TfpProductWantToGo> util = new ExcelUtil<TfpProductWantToGo>(TfpProductWantToGo.class);
        util.exportExcel(response, list, "商品想去数据");
    }

    /**
     * 获取商品想去详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:productWantToGo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tfpProductWantToGoService.selectTfpProductWantToGoById(id));
    }

    /**
     * 新增商品想去
     */
    @PreAuthorize("@ss.hasPermi('service:productWantToGo:add')")
    @Log(title = "商品想去", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TfpProductWantToGo tfpProductWantToGo)
    {
        return toAjax(tfpProductWantToGoService.insertTfpProductWantToGo(tfpProductWantToGo));
    }

    /**
     * 修改商品想去
     */
    @PreAuthorize("@ss.hasPermi('service:productWantToGo:edit')")
    @Log(title = "商品想去", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TfpProductWantToGo tfpProductWantToGo)
    {
        return toAjax(tfpProductWantToGoService.updateTfpProductWantToGo(tfpProductWantToGo));
    }

    /**
     * 删除商品想去
     */
    @PreAuthorize("@ss.hasPermi('service:productWantToGo:remove')")
    @Log(title = "商品想去", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tfpProductWantToGoService.deleteTfpProductWantToGoByIds(ids));
    }

    /**
     * 小程序-商品想去/取消想去
     * @param wantToGoDto
     * @return
     */
    @ApiOperation("小程序-商品想去/取消想去")
    @PostMapping("/wantToGo")
    @ResponseBody
    public R wantToGo(@RequestBody WantToGoDto wantToGoDto){
        return R.ok(tfpProductWantToGoService.wantToGo(wantToGoDto));
    }

    @ApiOperation("小程序-我的想去的商品")
    @PostMapping("/myWantToProductList")
    @ResponseBody
    public TableDataInfo<ListProductVo> myWantToProductList(){
        return getDataTable(tfpProductWantToGoService.myWantToProductList());
    }
}
