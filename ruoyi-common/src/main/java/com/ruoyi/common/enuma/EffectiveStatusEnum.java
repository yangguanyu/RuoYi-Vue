package com.ruoyi.common.enuma;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EffectiveStatusEnum implements BaseEnum{
    NO_EFFECTIVE(0, "失效"),
    EFFECTIVE(1, "有效")
    ;

    private Integer value;
    private String desc;
}
