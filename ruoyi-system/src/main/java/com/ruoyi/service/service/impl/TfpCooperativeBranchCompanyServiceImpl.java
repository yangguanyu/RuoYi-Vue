package com.ruoyi.service.service.impl;

import java.util.*;
import java.util.stream.Collectors;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.DcListUtils;
import com.ruoyi.service.domain.TfpProductCbc;
import com.ruoyi.service.service.ITfpProductCbcService;
import com.ruoyi.service.vo.SupplierVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpCooperativeBranchCompanyMapper;
import com.ruoyi.service.domain.TfpCooperativeBranchCompany;
import com.ruoyi.service.service.ITfpCooperativeBranchCompanyService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 合作分公司Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-12-12
 */
@Service
@Transactional
public class TfpCooperativeBranchCompanyServiceImpl implements ITfpCooperativeBranchCompanyService 
{
    @Autowired
    private TfpCooperativeBranchCompanyMapper tfpCooperativeBranchCompanyMapper;
    @Autowired
    private ITfpProductCbcService productCbcService;

    /**
     * 查询合作分公司
     * 
     * @param id 合作分公司主键
     * @return 合作分公司
     */
    @Override
    public TfpCooperativeBranchCompany selectTfpCooperativeBranchCompanyById(Long id)
    {
        return tfpCooperativeBranchCompanyMapper.selectTfpCooperativeBranchCompanyById(id);
    }

    /**
     * 查询合作分公司列表
     * 
     * @param tfpCooperativeBranchCompany 合作分公司
     * @return 合作分公司
     */
    @Override
    public List<TfpCooperativeBranchCompany> selectTfpCooperativeBranchCompanyList(TfpCooperativeBranchCompany tfpCooperativeBranchCompany)
    {
        return tfpCooperativeBranchCompanyMapper.selectTfpCooperativeBranchCompanyList(tfpCooperativeBranchCompany);
    }

    /**
     * 新增合作分公司
     * 
     * @param tfpCooperativeBranchCompany 合作分公司
     * @return 结果
     */
    @Override
    public int insertTfpCooperativeBranchCompany(TfpCooperativeBranchCompany tfpCooperativeBranchCompany)
    {
        tfpCooperativeBranchCompany.setCreateTime(DateUtils.getNowDate());
        return tfpCooperativeBranchCompanyMapper.insertTfpCooperativeBranchCompany(tfpCooperativeBranchCompany);
    }

    /**
     * 修改合作分公司
     * 
     * @param tfpCooperativeBranchCompany 合作分公司
     * @return 结果
     */
    @Override
    public int updateTfpCooperativeBranchCompany(TfpCooperativeBranchCompany tfpCooperativeBranchCompany)
    {
        tfpCooperativeBranchCompany.setUpdateTime(DateUtils.getNowDate());
        return tfpCooperativeBranchCompanyMapper.updateTfpCooperativeBranchCompany(tfpCooperativeBranchCompany);
    }

    /**
     * 批量删除合作分公司
     * 
     * @param ids 需要删除的合作分公司主键
     * @return 结果
     */
    @Override
    public int deleteTfpCooperativeBranchCompanyByIds(Long[] ids)
    {
        return tfpCooperativeBranchCompanyMapper.deleteTfpCooperativeBranchCompanyByIds(ids);
    }

    /**
     * 删除合作分公司信息
     * 
     * @param id 合作分公司主键
     * @return 结果
     */
    @Override
    public int deleteTfpCooperativeBranchCompanyById(Long id)
    {
        return tfpCooperativeBranchCompanyMapper.deleteTfpCooperativeBranchCompanyById(id);
    }

    @Override
    public List<SupplierVo> cooperativeBranchCompanySelect() {
        return tfpCooperativeBranchCompanyMapper.cooperativeBranchCompanySelect();
    }

    @Override
    public Map<Long, List<TfpCooperativeBranchCompany>> selectCooperativeBranchCompanyMapByProductIdList(List<Long> productIdList) {
        Map<Long, List<TfpCooperativeBranchCompany>> cooperativeBranchCompanyMap = new HashMap<>();

        List<TfpProductCbc> tfpProductCbcList = productCbcService.selectTfpProductCbcByProductIdList(productIdList);
        if (DcListUtils.isNotEmpty(tfpProductCbcList)){
            List<Long> cbcIdList = tfpProductCbcList.stream().map(TfpProductCbc::getCbcId).distinct().collect(Collectors.toList());
            List<TfpCooperativeBranchCompany> tfpCooperativeBranchCompanyList = tfpCooperativeBranchCompanyMapper.selectTfpCooperativeBranchCompanyByIdList(cbcIdList);

            Map<Long, TfpCooperativeBranchCompany> tfpCooperativeBranchCompanyMap;
            if (DcListUtils.isNotEmpty(tfpCooperativeBranchCompanyList)){
                tfpCooperativeBranchCompanyMap = tfpCooperativeBranchCompanyList.stream().collect(Collectors.toMap(TfpCooperativeBranchCompany::getId, TfpCooperativeBranchCompany -> TfpCooperativeBranchCompany));
            } else {
                tfpCooperativeBranchCompanyMap = new HashMap<>();
            }

            for (TfpProductCbc tfpProductCbc : tfpProductCbcList) {
                Long productId = tfpProductCbc.getProductId();

                TfpCooperativeBranchCompany tfpCooperativeBranchCompany = tfpCooperativeBranchCompanyMap.get(tfpProductCbc.getCbcId());
                if (Objects.nonNull(tfpCooperativeBranchCompany)){
                    List<TfpCooperativeBranchCompany> tfpCooperativeBranchCompanyMapList = cooperativeBranchCompanyMap.get(productId);
                    if (tfpCooperativeBranchCompanyMapList == null){
                        tfpCooperativeBranchCompanyMapList = new ArrayList<>();
                    }

                    tfpCooperativeBranchCompanyMapList.add(tfpCooperativeBranchCompany);
                    cooperativeBranchCompanyMap.put(productId, tfpCooperativeBranchCompanyMapList);
                }
            }
        }

        return cooperativeBranchCompanyMap;
    }
}
