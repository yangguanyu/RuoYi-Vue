package com.ruoyi.common.enuma;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum YesOrNoFlagEnum implements BaseEnum {

    NO(0, "否"),
    YES(1, "是")
    ;

    private Integer value;
    private String desc;
}
