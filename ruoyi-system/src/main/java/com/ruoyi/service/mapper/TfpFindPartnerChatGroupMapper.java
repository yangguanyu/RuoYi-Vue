package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.TfpFindPartnerChatGroup;
import com.ruoyi.service.dto.TfpFindPartnerChatGroupDto;
import com.ruoyi.service.vo.TfpFindPartnerChatGroupVo;
import org.apache.ibatis.annotations.Param;

/**
 * 找搭子聊天群组Mapper接口
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public interface TfpFindPartnerChatGroupMapper 
{
    /**
     * 查询找搭子聊天群组
     * 
     * @param id 找搭子聊天群组主键
     * @return 找搭子聊天群组
     */
    public TfpFindPartnerChatGroup selectTfpFindPartnerChatGroupById(Long id);

    /**
     * 查询找搭子聊天群组列表
     * 
     * @param tfpFindPartnerChatGroup 找搭子聊天群组
     * @return 找搭子聊天群组集合
     */
    public List<TfpFindPartnerChatGroup> selectTfpFindPartnerChatGroupList(TfpFindPartnerChatGroup tfpFindPartnerChatGroup);

    /**
     * 新增找搭子聊天群组
     * 
     * @param tfpFindPartnerChatGroup 找搭子聊天群组
     * @return 结果
     */
    public int insertTfpFindPartnerChatGroup(TfpFindPartnerChatGroup tfpFindPartnerChatGroup);

    /**
     * 修改找搭子聊天群组
     * 
     * @param tfpFindPartnerChatGroup 找搭子聊天群组
     * @return 结果
     */
    public int updateTfpFindPartnerChatGroup(TfpFindPartnerChatGroup tfpFindPartnerChatGroup);

    /**
     * 删除找搭子聊天群组
     * 
     * @param id 找搭子聊天群组主键
     * @return 结果
     */
    public int deleteTfpFindPartnerChatGroupById(Long id);

    /**
     * 批量删除找搭子聊天群组
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpFindPartnerChatGroupByIds(Long[] ids);

    List<TfpFindPartnerChatGroupVo> selectTfpFindPartnerChatGroupVoList(TfpFindPartnerChatGroupDto tfpFindPartnerChatGroupDto);

    List<TfpFindPartnerChatGroup> selectTfpFindPartnerChatGroupListByIdList(@Param("idList") List<Long> idList);

    TfpFindPartnerChatGroup selectByFindPartnerId(@Param("findPartnerId") Long findPartnerId);

    List<TfpFindPartnerChatGroup> selectTfpFindPartnerChatGroupListByFindPartnerIdList(@Param("findPartnerIdList") List<Long> findPartnerIdList);
}
