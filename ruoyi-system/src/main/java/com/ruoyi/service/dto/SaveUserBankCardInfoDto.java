package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "SaveUserBankCardInfoDto", description = "保存用户银行卡信息")
public class SaveUserBankCardInfoDto {
    @ApiModelProperty("银行名称")
    private String bankName;
    @ApiModelProperty("银行卡号")
    private String bankCardNumber;
    @ApiModelProperty("开户网点")
    private String accountOpeningBranch;
    @ApiModelProperty("账户姓名")
    private String userName;
    @ApiModelProperty("预留电话")
    private String phoneNumber;
}
