package com.ruoyi.service.domain.flight.param;

import lombok.Data;

import java.util.List;

/**
 * 航班搜索参数类
 */
@Data
public class FlightSearchParam {

    /**
     * 航程类型
     */
    private String routeType;

    /**
     * 舱位等级，暂未启用
     */
    private Integer cabinClass;

    /**
     * 是否直飞：1=直飞，2=其他。默认直飞
     */
    private Integer directFlight;

    /**
     * 航空公司二字码，例如CA
     */
    private String airline;

    /**
     * 航程航线数据
     */
    private List<Segment> segments;

    /**
     * 乘客类型
     * 1. 成人
     * 2. 儿童
     * 3. 成人+儿童
     * 4. 成人+婴儿
     * 5. 成人1儿童+婴儿
     * 目前不支持婴儿查询
     */
    private Integer passengerType;

    /**
     * 0不限制，1不换编
     * 建议填0，1不换编可能查询无航班数据
     */
    private Integer needPnr;

    @Data
    public class Segment {
        /**
         * 航班日期(yyyy-MM-dd)
         */
        private String departureTime;

        /**
         * 起飞城市或机场三字码
         */
        private String departureAirport;

        /**
         * 到达城市机场三字码
         */
        private String arrivalAirport;
    }
}