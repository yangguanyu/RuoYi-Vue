import request from '@/utils/request'

// 查询自费项目列表
export function listSelfFundedProject(query) {
  return request({
    url: '/service/selfFundedProject/list',
    method: 'get',
    params: query
  })
}

// 查询自费项目详细
export function getSelfFundedProject(id) {
  return request({
    url: '/service/selfFundedProject/' + id,
    method: 'get'
  })
}

// 新增自费项目
export function addSelfFundedProject(data) {
  return request({
    url: '/service/selfFundedProject',
    method: 'post',
    data: data
  })
}

// 修改自费项目
export function updateSelfFundedProject(data) {
  return request({
    url: '/service/selfFundedProject',
    method: 'put',
    data: data
  })
}

// 删除自费项目
export function delSelfFundedProject(id) {
  return request({
    url: '/service/selfFundedProject/' + id,
    method: 'delete'
  })
}
