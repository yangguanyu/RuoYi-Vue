package com.ruoyi.service.service;

import java.util.List;
import com.ruoyi.service.domain.TfpUserBankCard;
import com.ruoyi.service.dto.SaveUserBankCardInfoDto;
import com.ruoyi.service.vo.DetailUserBankCardVo;

/**
 * 用户银行卡Service接口
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public interface ITfpUserBankCardService 
{
    /**
     * 查询用户银行卡
     * 
     * @param id 用户银行卡主键
     * @return 用户银行卡
     */
    public TfpUserBankCard selectTfpUserBankCardById(Long id);

    /**
     * 查询用户银行卡列表
     * 
     * @param tfpUserBankCard 用户银行卡
     * @return 用户银行卡集合
     */
    public List<TfpUserBankCard> selectTfpUserBankCardList(TfpUserBankCard tfpUserBankCard);

    /**
     * 新增用户银行卡
     * 
     * @param tfpUserBankCard 用户银行卡
     * @return 结果
     */
    public int insertTfpUserBankCard(TfpUserBankCard tfpUserBankCard);

    /**
     * 修改用户银行卡
     * 
     * @param tfpUserBankCard 用户银行卡
     * @return 结果
     */
    public int updateTfpUserBankCard(TfpUserBankCard tfpUserBankCard);

    /**
     * 批量删除用户银行卡
     * 
     * @param ids 需要删除的用户银行卡主键集合
     * @return 结果
     */
    public int deleteTfpUserBankCardByIds(Long[] ids);

    /**
     * 删除用户银行卡信息
     * 
     * @param id 用户银行卡主键
     * @return 结果
     */
    public int deleteTfpUserBankCardById(Long id);

    int saveUserBankCardInfo(SaveUserBankCardInfoDto saveUserBankCardInfoDto);

    DetailUserBankCardVo detailUserBankCard();

    boolean existsUserBankCardFlag(Long userId);

    /**
     * 小程序-用户银行卡是否存在
     * @return
     */
    Boolean existsUserBankCardFlag();

    TfpUserBankCard getMyUserBankCardInfo();

    List<TfpUserBankCard> selectTfpUserBankCardListByIdList(List<Long> idList);
}
