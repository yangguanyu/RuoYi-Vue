package com.ruoyi.service.vo;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.List;

/**
 * 政策信息对象 tfp_flight_policy
 * 
 * @author ruoyi
 * @date 2024-04-10
 */
@Data
public class TfpFlightPolicyVo extends BaseEntity
{
    /** 主键 */
    private Long id;

    /** 航班信息表id */
    private Long flightId;

    /** 舱位信息表id */
    private Long seatId;

    /** 产品ID */
    private String policyId;

    /** 产品类型 商旅优选：1、2、3、4、12、13、9、 低价推荐：41、42、43、44、45、26、 官网：21、22、23、24、25、6 生成订单需传入出生日期：4、41、42、43、44、45、21、22、23、24、25、6、9、26 */
    private String productType;

    /** 证件限制 false-不限 true-有限制 */
    private Boolean certificates;

    /** 证件 */
    private String certificatesList;

    /** 出票工作时间 */
    private String workTime;

    /** 退票工作时间 */
    private String vtWorkTime;

    /** 产品备注 */
    private String comment;

    /** 适用乘客类型 1-成人 2-儿童 3-成人+儿童 4-成人+婴儿 5-成人+儿童+婴儿 */
    private Integer applyPassenger;

    /** 报销凭证类型 1-行程单 2-发票 3-电子发票 4-行程单+发票 5-不提供报销凭证 */
    private Integer voucher;

    /** 最晚出票时间计算方式 1-支付后 2-起飞前 */
    private Integer latestAlgorithm;

    /** 最晚出票时间（分钟） */
    private Integer latestTicket;

    /** 产品年龄限制 */
    private String ageLimit;

    /** 人数限制 */
    private String numberLimit;

    /** 通程产品服务说明 */
    private String description;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    private Long createUserId;

    /** 更新者id */
    private Long updateUserId;

    // 价格信息
    private List<TfpFlightPriceDataVo> priceDatas;
    // 通程舱位对应信息
    private List<TfpFlightSeatDetailVo> seatDetails;
}
