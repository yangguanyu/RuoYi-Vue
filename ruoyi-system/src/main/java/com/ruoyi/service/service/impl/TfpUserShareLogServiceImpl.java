package com.ruoyi.service.service.impl;

import java.util.List;

import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enuma.DeleteEnum;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.service.dto.RecordUserShareLogDto;
import com.ruoyi.service.vo.InvitingAchievementsVo;
import com.ruoyi.service.vo.ShareProductLogVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpUserShareLogMapper;
import com.ruoyi.service.domain.TfpUserShareLog;
import com.ruoyi.service.service.ITfpUserShareLogService;

/**
 * 用户分享日志Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-25
 */
@Service
public class TfpUserShareLogServiceImpl implements ITfpUserShareLogService 
{
    @Autowired
    private TfpUserShareLogMapper tfpUserShareLogMapper;

    /**
     * 查询用户分享日志
     * 
     * @param id 用户分享日志主键
     * @return 用户分享日志
     */
    @Override
    public TfpUserShareLog selectTfpUserShareLogById(Long id)
    {
        return tfpUserShareLogMapper.selectTfpUserShareLogById(id);
    }

    /**
     * 查询用户分享日志列表
     * 
     * @param tfpUserShareLog 用户分享日志
     * @return 用户分享日志
     */
    @Override
    public List<TfpUserShareLog> selectTfpUserShareLogList(TfpUserShareLog tfpUserShareLog)
    {
        return tfpUserShareLogMapper.selectTfpUserShareLogList(tfpUserShareLog);
    }

    /**
     * 新增用户分享日志
     * 
     * @param tfpUserShareLog 用户分享日志
     * @return 结果
     */
    @Override
    public int insertTfpUserShareLog(TfpUserShareLog tfpUserShareLog)
    {
        tfpUserShareLog.setCreateTime(DateUtils.getNowDate());
        return tfpUserShareLogMapper.insertTfpUserShareLog(tfpUserShareLog);
    }

    /**
     * 修改用户分享日志
     * 
     * @param tfpUserShareLog 用户分享日志
     * @return 结果
     */
    @Override
    public int updateTfpUserShareLog(TfpUserShareLog tfpUserShareLog)
    {
        tfpUserShareLog.setUpdateTime(DateUtils.getNowDate());
        return tfpUserShareLogMapper.updateTfpUserShareLog(tfpUserShareLog);
    }

    /**
     * 批量删除用户分享日志
     * 
     * @param ids 需要删除的用户分享日志主键
     * @return 结果
     */
    @Override
    public int deleteTfpUserShareLogByIds(Long[] ids)
    {
        return tfpUserShareLogMapper.deleteTfpUserShareLogByIds(ids);
    }

    /**
     * 删除用户分享日志信息
     * 
     * @param id 用户分享日志主键
     * @return 结果
     */
    @Override
    public int deleteTfpUserShareLogById(Long id)
    {
        return tfpUserShareLogMapper.deleteTfpUserShareLogById(id);
    }

    /**
     * 小程序-记录用户分享日志
     * @param recordUserShareLogDto
     * @return
     */
    @Override
    public int recordUserShareLog(RecordUserShareLogDto recordUserShareLogDto) {
        LoginUser loginUser = SecurityUtils.getLoginUser();

        TfpUserShareLog userShareLog = new TfpUserShareLog();
        userShareLog.setUserId(loginUser.getUserId());
        userShareLog.setType(recordUserShareLogDto.getType());
        userShareLog.setBusinessId(recordUserShareLogDto.getBusinessId());
        userShareLog.setDelFlag(DeleteEnum.EXIST.getValue());
        userShareLog.setCreateUserId(loginUser.getUserId());
        userShareLog.setCreateBy(loginUser.getUsername());
        userShareLog.setCreateTime(DateUtils.getNowDate());
        tfpUserShareLogMapper.insertTfpUserShareLog(userShareLog);
        return 1;
    }

    @Override
    public List<ShareProductLogVo> selectShareProductLogList(Long userId) {
        return tfpUserShareLogMapper.selectShareProductLogList(userId);
    }

    @Override
    public List<InvitingAchievementsVo> invitingAchievements(Long userId) {
        return tfpUserShareLogMapper.invitingAchievements(userId);
    }
}
