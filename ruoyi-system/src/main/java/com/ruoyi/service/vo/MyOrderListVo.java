package com.ruoyi.service.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@ApiModel(value = "MyOrderListVo")
public class MyOrderListVo {
    @ApiModelProperty("订单id")
    private Long orderId;
    @ApiModelProperty("订单编号")
    private String orderNo;
    @ApiModelProperty("订单状态 0-待支付 1-已完成 2-待确认(已支付) 3-待出行 4-申请退款-审核中 5-申请退款-退款成功 6-过期 7-取消")
    private Integer status;
    @ApiModelProperty("商品名称")
    private String productName;
    @ApiModelProperty("商品图片")
    private String productImage;
    @ApiModelProperty("出发时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date goDate;
    @ApiModelProperty("订单金额")
    private BigDecimal finalAmount;
    @ApiModelProperty("超时时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date expireDate;

    @ApiModelProperty("服务类型id 1-全国散 2-一家一团 3-自营 4-全国散-小包团")
    private Long categoryId;
    @ApiModelProperty("出行方式 0-组队出行 1-独立出行")
    private Integer travelType;

    @ApiModelProperty("聊天群信息")
    private OrderChatGroupInfoVo chatGroupInfo;

    @JsonIgnore
    private Long id;
    @JsonIgnore
    private Long productId;
    @JsonIgnore
    private Long chatGroupId;
}
