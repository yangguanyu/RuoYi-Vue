package com.ruoyi.service.dto;

import com.ruoyi.service.domain.*;
import com.ruoyi.service.vo.ProductSnapshotDetailVo;
import lombok.Data;

import java.util.List;

@Data
public class SnapshotTextDto {
    private TfpProduct tfpProduct;
    private TfpProductSub tfpProductSub;
    private List<TfpPurchaseShopNumber> tfpPurchaseShopNumberList;
    private List<TfpSelfFundedProject> tfpSelfFundedProjectList;
    private List<TfpProductCbc> tfpProductCbcList;
    private List<TfpEveryDayPrice> tfpEveryDayPriceList;
    private List<TfpProductWantToGo> tfpProductWantToGoList;

    private ProductSnapshotDetailVo productSnapshotDetailVo;
}
