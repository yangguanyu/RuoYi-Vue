package com.ruoyi.service.service.impl;

import java.util.*;
import java.util.stream.Collectors;

import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enuma.BusinessSubTypeEnum;
import com.ruoyi.common.enuma.BusinessTypeEnum;
import com.ruoyi.common.enuma.DeleteEnum;
import com.ruoyi.common.enuma.YesOrNoFlagEnum;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.DcListUtils;
import com.ruoyi.common.utils.DcUrlUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.service.dto.SaveSystemImageDto;
import com.ruoyi.service.dto.UserImageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpAttachmentMapper;
import com.ruoyi.common.core.domain.entity.TfpAttachment;
import com.ruoyi.service.service.ITfpAttachmentService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 附件Service业务层处理
 *
 * @author ruoyi
 * @date 2023-12-25
 */
@Service
@Transactional
public class TfpAttachmentServiceImpl implements ITfpAttachmentService
{
    @Autowired
    private TfpAttachmentMapper tfpAttachmentMapper;

    @Value("${domain.name.file}")
    private String domainNameFile;
    @Value("${image.url}")
    private String imageUrl;

    /**
     * 查询附件
     *
     * @param id 附件主键
     * @return 附件
     */
    @Override
    public TfpAttachment selectTfpAttachmentById(Long id)
    {
        TfpAttachment tfpAttachment = tfpAttachmentMapper.selectTfpAttachmentById(id);
        this.handleGetUrl(tfpAttachment);
        return tfpAttachment;
    }



    /**
     * 查询附件列表
     *
     * @param tfpAttachment 附件
     * @return 附件
     */
    @Override
    public List<TfpAttachment> selectTfpAttachmentList(TfpAttachment tfpAttachment)
    {
        List<TfpAttachment> tfpAttachmentList = tfpAttachmentMapper.selectTfpAttachmentList(tfpAttachment);
        this.handleGetListUrl(tfpAttachmentList);
        return tfpAttachmentList;
    }

    /**
     * 新增附件
     *
     * @param tfpAttachment 附件
     * @return 结果
     */
    @Override
    public int insertTfpAttachment(TfpAttachment tfpAttachment)
    {
        this.handleSaveUrl(tfpAttachment);
        tfpAttachment.setCreateTime(DateUtils.getNowDate());
        return tfpAttachmentMapper.insertTfpAttachment(tfpAttachment);
    }

    /**
     * 修改附件
     *
     * @param tfpAttachment 附件
     * @return 结果
     */
    @Override
    public int updateTfpAttachment(TfpAttachment tfpAttachment)
    {
        this.handleSaveUrl(tfpAttachment);
        tfpAttachment.setUpdateTime(DateUtils.getNowDate());
        return tfpAttachmentMapper.updateTfpAttachment(tfpAttachment);
    }

    /**
     * 批量删除附件
     *
     * @param ids 需要删除的附件主键
     * @return 结果
     */
    @Override
    public int deleteTfpAttachmentByIds(Long[] ids)
    {
        return tfpAttachmentMapper.deleteTfpAttachmentByIds(ids);
    }

    /**
     * 删除附件信息
     *
     * @param id 附件主键
     * @return 结果
     */
    @Override
    public int deleteTfpAttachmentById(Long id)
    {
        return tfpAttachmentMapper.deleteTfpAttachmentById(id);
    }

    @Override
    public List<TfpAttachment> selectListByBusinessId(Long businessId, String businessType, String businessSubType){
        List<TfpAttachment> tfpAttachmentList = tfpAttachmentMapper.selectListByBusinessId(businessId, businessType, businessSubType);
        this.handleGetListUrl(tfpAttachmentList);
        return tfpAttachmentList;
    }

    @Override
    public List<TfpAttachment> selectList(List<Long> businessIdList, String businessType, String businessSubType){
        List<TfpAttachment> tfpAttachmentList = tfpAttachmentMapper.selectList(businessIdList, businessType, businessSubType);
        this.handleGetListUrl(tfpAttachmentList);
        return tfpAttachmentList;
    }

    /**
     * 保存用户图片
     * @param userId
     * @param userImageList
     */
    @Override
    public void saveUserImage(Long userId, List<UserImageDto> userImageList) {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        tfpAttachmentMapper.deleteUserImageNotHeaderImage(userId);
        if (DcListUtils.isNotEmpty(userImageList)){
            for (UserImageDto userImageDto : userImageList) {
                String url = userImageDto.getUrl();
                String extend = DcUrlUtils.getExtend(url);

                String name = "用户图片" + extend;
                String businessSubType = BusinessSubTypeEnum.USER_IMAGE.getValue();
                Integer headSculptureFlag = userImageDto.getHeadSculptureFlag();
                if (Objects.equals(headSculptureFlag, YesOrNoFlagEnum.YES.getValue())){
                    name = "用户头像图片" + extend;
                    businessSubType = BusinessSubTypeEnum.USER_HEAD_PORTRAIT.getValue();

                    List<TfpAttachment> attachmentList = this.selectListByBusinessId(userId, BusinessTypeEnum.USER.getValue(), BusinessSubTypeEnum.USER_HEAD_PORTRAIT.getValue());
                    if (DcListUtils.isNotEmpty(attachmentList)){
                        TfpAttachment attachment = attachmentList.get(0);
                        attachment.setUrl(url);
                        attachment.setUpdateUserId(loginUser.getUserId());
                        attachment.setUpdateBy(loginUser.getUsername());
                        attachment.setUpdateTime(DateUtils.getNowDate());
                        this.updateTfpAttachment(attachment);

                        continue;
                    }
                }

                TfpAttachment attachment = new TfpAttachment();
                attachment.setBusinessId(userId);
                attachment.setBusinessType(BusinessTypeEnum.USER.getValue());
                attachment.setBusinessSubType(businessSubType);
                attachment.setName(name);
                attachment.setUrl(url);
                attachment.setDelFlag(DeleteEnum.EXIST.getValue());
                attachment.setCreateUserId(loginUser.getUserId());
                attachment.setCreateBy(loginUser.getUsername());
                attachment.setCreateTime(DateUtils.getNowDate());
                this.insertTfpAttachment(attachment);
            }
        }
    }

    /**
     * 获取用户头像图片map
     * @param userIdList
     * @return
     */
    @Override
    public Map<Long, String> getUserHeadImageMap(List<Long> userIdList){
        Map<Long, String> userHeadImageMap = new HashMap<>();
        List<TfpAttachment> attachmentList = this.selectList(userIdList, BusinessTypeEnum.USER.getValue(), BusinessSubTypeEnum.USER_HEAD_PORTRAIT.getValue());
        if (DcListUtils.isNotEmpty(attachmentList)){
            userHeadImageMap = attachmentList.stream().collect(Collectors.toMap(TfpAttachment::getBusinessId, TfpAttachment::getUrl));
        }

        return userHeadImageMap;
    }

    @Override
    public void deleteTfpAttachmentByBusinessId(Long businessId, String businessType, String businessSubType) {
        if (Objects.nonNull(businessId)){
            tfpAttachmentMapper.deleteTfpAttachmentByBusinessId(businessId, businessType, businessSubType);
        }
    }

    @Override
    public void saveImageList(Long businessId, String name, List<String> urlList, String businessType, String businessSubType){
        if (DcListUtils.isNotEmpty(urlList) && Objects.nonNull(businessId) && Objects.nonNull(businessType) && Objects.nonNull(businessSubType)){
            LoginUser loginUser = SecurityUtils.getLoginUser();
            for (String url : urlList) {
                String extend = DcUrlUtils.getExtend(url);
                if (!name.endsWith(extend)){
                    name += extend;
                }

                TfpAttachment attachment = new TfpAttachment();
                attachment.setBusinessId(businessId);
                attachment.setBusinessType(businessType);
                attachment.setBusinessSubType(businessSubType);
                attachment.setName(name);
                attachment.setUrl(url);
                attachment.setDelFlag(DeleteEnum.EXIST.getValue());
                attachment.setCreateUserId(loginUser.getUserId());
                attachment.setCreateBy(loginUser.getUsername());
                attachment.setCreateTime(DateUtils.getNowDate());
                this.insertTfpAttachment(attachment);
            }
        }
    }

    @Override
    public int saveSystemImage(List<SaveSystemImageDto> saveSystemImageDtoList) {
        for (SaveSystemImageDto saveSystemImageDto : saveSystemImageDtoList) {
            TfpAttachment attachment = new TfpAttachment();
            attachment.setName("默认系统图片." + saveSystemImageDto.getSuffix());
            attachment.setUrl(imageUrl.replace("replace1", saveSystemImageDto.getFileId()).replace("replace2", saveSystemImageDto.getSuffix()));
            attachment.setBusinessId(1L);
            attachment.setBusinessType("system");
            attachment.setBusinessSubType("system_default_image");
            attachment.setCreateTime(new Date());
            attachment.setCreateUserId(1L);
            attachment.setCreateBy("admin");
            this.insertTfpAttachment(attachment);
        }

        return 1;
    }

    private void handleGetUrl(TfpAttachment tfpAttachment){
        if (Objects.nonNull(tfpAttachment)){
            tfpAttachment.setUrl(domainNameFile + tfpAttachment.getUrl());
        }
    }

    private void handleGetListUrl(List<TfpAttachment> tfpAttachmentList){
        if (DcListUtils.isNotEmpty(tfpAttachmentList)){
            for (TfpAttachment attachment : tfpAttachmentList) {
                this.handleGetUrl(attachment);
            }
        }
    }

    private void handleSaveUrl(TfpAttachment tfpAttachment){
        if (Objects.nonNull(tfpAttachment)){
            String url = tfpAttachment.getUrl();
            if (url.contains("/download") && !url.startsWith("/download")){
                tfpAttachment.setUrl(url.substring(url.indexOf("/download")));
            }
        }
    }
}
