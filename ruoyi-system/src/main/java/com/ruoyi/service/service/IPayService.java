package com.ruoyi.service.service;

import com.wechat.pay.java.core.exception.ValidationException;
import com.wechat.pay.java.service.partnerpayments.jsapi.model.Transaction;

import java.math.BigDecimal;
import java.util.Date;

public interface IPayService {
    String createWeChatOrder(String outTradeNo, BigDecimal finalAmount, String description, Date expireDate);

    /**
     * 微信支付回调业务处理
     * @param transaction
     */
    void payNotify(Transaction transaction) throws ValidationException;
}
