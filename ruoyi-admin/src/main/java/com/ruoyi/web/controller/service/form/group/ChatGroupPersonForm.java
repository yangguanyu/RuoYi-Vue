package com.ruoyi.web.controller.service.form.group;

import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.validation.constraints.NotNull;

/**
 * 聊天群组成员对象 tfp_chat_group_person
 *
 * @author ruoyi
 * @date 2023-10-31
 */
@ApiModel(value = "ChatGroupPersonForm",description = "添加群组成员-点击我想去的时候")
public class ChatGroupPersonForm extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 聊天群组id */
    @ApiModelProperty("群组id")
    @NotNull(message = "群组id不能为空")
    private Long chatGroupId;

    /** 用户id */
    @ApiModelProperty("用户id")
    @NotNull(message = "用户id不能为空")
    private Long userId;

    /** 用户类型 0-发起者 1-已报名 2-未报名 */
    @ApiModelProperty("用户类型 0-发起者 1-已报名 2-未报名")
    private Integer userType;

    @ApiModelProperty("商品id")
    @NotNull(message = "商品id不能为空")
    private Long productId;


    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setChatGroupId(Long chatGroupId)
    {
        this.chatGroupId = chatGroupId;
    }

    public Long getChatGroupId()
    {
        return chatGroupId;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }

    public void setUserType(Integer userType)
    {
        this.userType = userType;
    }

    public Integer getUserType()
    {
        return userType;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("chatGroupId", getChatGroupId())
            .append("userId", getUserId())
            .append("userType", getUserType())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
