package com.ruoyi.common.enuma;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TravelTypeEnum implements BaseEnum{
    TEAM_TRAVEL(0, "组队出行"),
    INDEPENDENT_TRAVEL(1, "独立出行")
    ;

    private Integer value;
    private String desc;
}
