package com.ruoyi.common.core.domain.wx;

import lombok.Data;

/**
 * 统一登录返回结果-身份部分
 *
 * @author soonphe
 * @since 1.0
 */
@Data
public class UserSupplierIdentiyEntity {
    /**
     * 身份名称
     */
    private String name;
    /**
     * 身份id
     */
    private Long identityId;

}
