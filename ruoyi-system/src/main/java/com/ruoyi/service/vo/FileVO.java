package com.ruoyi.service.vo;

import lombok.Data;

@Data
public class FileVO {
	// 文件id,后续下载使用
	private String fileId;
	// 文件的原始名称
	private String fileOriginalName;
	// 文件后缀名称
	private String suffix;
}
