package com.ruoyi.service.service.impl;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;
import java.util.concurrent.TimeUnit;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.common.constant.CommonConstants;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enuma.DeleteEnum;
import com.ruoyi.common.utils.*;
import com.ruoyi.service.domain.AccessTokenEntity;
import com.ruoyi.common.core.domain.entity.TfpAttachment;
import com.ruoyi.service.domain.TfpQrCodeInfo;
import com.ruoyi.service.dto.*;
import com.ruoyi.service.service.ITfpAttachmentService;
import com.ruoyi.service.service.ITfpQrCodeInfoService;
import com.ruoyi.service.vo.FileReturnVO;
import com.ruoyi.service.vo.FileVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpQrCodeMapper;
import com.ruoyi.service.domain.TfpQrCode;
import com.ruoyi.service.service.ITfpQrCodeService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

/**
 * 二维码Service业务层处理
 *
 * @author ruoyi
 * @date 2023-12-25
 */
@Service
@Transactional
@Slf4j
public class TfpQrCodeServiceImpl implements ITfpQrCodeService
{
    @Value("${secret}")
    private String secretKey;
    @Value("${appid}")
    private String appId;
    @Value("${tengxun.qrcode.version:release}")
    private String tengxunQrcodeVersion;
    @Value("${upload.url}")
    private String uploadUrl;
    @Value("${image.url}")
    private String imageUrl;
    @Value("${domain.name.file}")
    private String domainNameFile;

    /**
     * 过期时间(s)
     */
    public static final Integer CAPTCHA_EXPIRE_TIME = 7000;
    /**
     * 获取小程序token的key
     */
    public static final String CAPTCHA_TOKEN = "App:accessToken";

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private TfpQrCodeMapper tfpQrCodeMapper;
    @Autowired
    private ITfpAttachmentService attachmentService;
    @Autowired
    private ITfpQrCodeInfoService qrCodeInfoService;

    /**
     * 查询二维码
     *
     * @param id 二维码主键
     * @return 二维码
     */
    @Override
    public TfpQrCode selectTfpQrCodeById(Long id)
    {
        return tfpQrCodeMapper.selectTfpQrCodeById(id);
    }

    /**
     * 查询二维码列表
     *
     * @param tfpQrCode 二维码
     * @return 二维码
     */
    @Override
    public List<TfpQrCode> selectTfpQrCodeList(TfpQrCode tfpQrCode)
    {
        return tfpQrCodeMapper.selectTfpQrCodeList(tfpQrCode);
    }

    /**
     * 新增二维码
     *
     * @param tfpQrCode 二维码
     * @return 结果
     */
    @Override
    public int insertTfpQrCode(TfpQrCode tfpQrCode)
    {
        tfpQrCode.setCreateTime(DateUtils.getNowDate());
        return tfpQrCodeMapper.insertTfpQrCode(tfpQrCode);
    }

    /**
     * 修改二维码
     *
     * @param tfpQrCode 二维码
     * @return 结果
     */
    @Override
    public int updateTfpQrCode(TfpQrCode tfpQrCode)
    {
        tfpQrCode.setUpdateTime(DateUtils.getNowDate());
        return tfpQrCodeMapper.updateTfpQrCode(tfpQrCode);
    }

    /**
     * 批量删除二维码
     *
     * @param ids 需要删除的二维码主键
     * @return 结果
     */
    @Override
    public int deleteTfpQrCodeByIds(Long[] ids)
    {
        return tfpQrCodeMapper.deleteTfpQrCodeByIds(ids);
    }

    /**
     * 删除二维码信息
     *
     * @param id 二维码主键
     * @return 结果
     */
    @Override
    public int deleteTfpQrCodeById(Long id)
    {
        return tfpQrCodeMapper.deleteTfpQrCodeById(id);
    }

    /**
     * 获取二维码
     * @param detQrCodeDto
     * @return
     */
    @Override
    public String getQrCode(GetQrCodeDto detQrCodeDto) {
        String type = detQrCodeDto.getType();
        TfpQrCode tfpQrCode = tfpQrCodeMapper.selectTfpQrCodeByType(type);
        if (tfpQrCode == null){
            throw new RuntimeException("二维码查询失败");
        }

        String page = tfpQrCode.getPage();
        Long businessId = detQrCodeDto.getBusinessId();
        String businessType = tfpQrCode.getBusinessType();
        String businessSubType = tfpQrCode.getBusinessSubType();

        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = Objects.nonNull(loginUser) ? loginUser.getUserId() : null;

        Long qrId;

        TfpQrCodeInfo param = new TfpQrCodeInfo();
        param.setType(type);
        param.setShareUserId(userId);
        param.setBusinessId(businessId);
        param.setDelFlag(DeleteEnum.EXIST.getValue());
        List<TfpQrCodeInfo> tfpQrCodeInfoList = qrCodeInfoService.selectTfpQrCodeInfoList(param);
        if (DcListUtils.isNotEmpty(tfpQrCodeInfoList)){
            qrId = tfpQrCodeInfoList.get(0).getId();
        } else {
            TfpQrCodeInfo tfpQrCodeInfo = new TfpQrCodeInfo();
            tfpQrCodeInfo.setType(type);
            tfpQrCodeInfo.setShareUserId(userId);
            tfpQrCodeInfo.setBusinessId(businessId);
            tfpQrCodeInfo.setDelFlag(DeleteEnum.EXIST.getValue());
            tfpQrCodeInfo.setCreateUserId(loginUser.getUserId());
            tfpQrCodeInfo.setCreateBy(loginUser.getUsername());
            tfpQrCodeInfo.setCreateTime(DateUtils.getNowDate());

            OtherParamDto otherParam = detQrCodeDto.getOtherParam();
            if (Objects.nonNull(otherParam)){
                tfpQrCodeInfo.setOtherParam(JSON.toJSONString(otherParam));
            }

            qrCodeInfoService.insertTfpQrCodeInfo(tfpQrCodeInfo);

            qrId = tfpQrCodeInfo.getId();
        }

        String scene = "qrId" + "=" + qrId;

        GetOrCreateQrCodeDTO getOrCreateQrCodeDTO = this.initGetOrCreateQrCodeDTO(page, scene, qrId, businessType, businessSubType);
        return this.getOrCreateQrCode(getOrCreateQrCodeDTO);
    }


    /**
     * 初始化二维码请求DTO
     * @param page
     * @param scene
     * @param businessId
     * @param businessType
     * @param businessSubType
     * @return
     */
    private GetOrCreateQrCodeDTO initGetOrCreateQrCodeDTO(String page, String scene, Long businessId, String businessType, String businessSubType){
        WxSharingLinksReqDTO wxSharingLinksReqDTO = new WxSharingLinksReqDTO();
        wxSharingLinksReqDTO.setWidth(CommonConstants.HUNDRED);
        wxSharingLinksReqDTO.setPage(page);
        wxSharingLinksReqDTO.setScene(scene);
        // TODO 正式发布时，注释掉
//        wxSharingLinksReqDTO.setCheck_path(false);

        GetOrCreateQrCodeDTO getOrCreateQrCodeDTO = new GetOrCreateQrCodeDTO();
        getOrCreateQrCodeDTO.setBusinessId(businessId);
        getOrCreateQrCodeDTO.setBusinessType(businessType);
        getOrCreateQrCodeDTO.setBusinessSubType(businessSubType);
        getOrCreateQrCodeDTO.setWxSharingLinksReqDTO(wxSharingLinksReqDTO);
        return getOrCreateQrCodeDTO;
    }

    /**
     * 获取或生成二维码
     * @param getOrCreateQrCodeDTO
     * @return
     */
    public String getOrCreateQrCode(GetOrCreateQrCodeDTO getOrCreateQrCodeDTO) {
        // 查询历史URL
        TfpAttachment param = new TfpAttachment();
        param.setBusinessId(getOrCreateQrCodeDTO.getBusinessId());
        param.setBusinessType(getOrCreateQrCodeDTO.getBusinessType());
        param.setBusinessSubType(getOrCreateQrCodeDTO.getBusinessSubType());
        param.setDelFlag(DeleteEnum.EXIST.getValue());
        List<TfpAttachment> attachmentList = attachmentService.selectTfpAttachmentList(param);
        if (DcListUtils.isNotEmpty(attachmentList)){
            TfpAttachment attachment = attachmentList.get(0);
            return attachment.getUrl();
        }

        // 获取二维码信息
        WxSharingLinksReqDTO wxSharingLinksReqDTO = getOrCreateQrCodeDTO.getWxSharingLinksReqDTO();
        WxSharingLinksDTO wxSharingLinksDTO = new WxSharingLinksDTO();
        BeanUtils.copyProperties(wxSharingLinksReqDTO, wxSharingLinksDTO);
        byte[] sharingImageByte = this.getSharingImage(wxSharingLinksDTO);
        if (Objects.isNull(sharingImageByte)) return StringUtils.EMPTY;

        // 上传附件，获取图片id
        String fileName = "二维码.png";
        String content = DcFileUploadUtils.uploadFile(uploadUrl, fileName, sharingImageByte);

        FileReturnVO fileReturnVO = JSONObject.parseObject(content, FileReturnVO.class);
        FileVO fileVO = fileReturnVO.getData();

        // 存储图片信息
        LoginUser loginUser = SecurityUtils.getLoginUser();

        String url = domainNameFile + imageUrl.replace("replace1", fileVO.getFileId()).replace("replace2", fileName.substring(fileName.lastIndexOf(".") + 1));

        TfpAttachment attachment = new TfpAttachment();
        attachment.setName(fileName);
        attachment.setUrl(url);
        attachment.setBusinessId(getOrCreateQrCodeDTO.getBusinessId());
        attachment.setBusinessType(getOrCreateQrCodeDTO.getBusinessType());
        attachment.setBusinessSubType(getOrCreateQrCodeDTO.getBusinessSubType());
        attachment.setCreateTime(new Date());
        if (Objects.nonNull(loginUser)){
            attachment.setCreateUserId(loginUser.getUserId());
            attachment.setCreateBy(loginUser.getUsername());
        }
        attachmentService.insertTfpAttachment(attachment);

        return url;
    }


    public byte[] getSharingImage(WxSharingLinksDTO wSharingLinks) {
        //首先获取ACCESS_TOKEN
        String access_token = getAccess_token(appId, secretKey, CAPTCHA_TOKEN);
        if (StringUtils.isEmpty(access_token)){
            throw new RuntimeException("请稍后重试！！");
        }

        String createQrCodeUrl = "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=" + access_token;

        wSharingLinks.setEnv_version(tengxunQrcodeVersion);

        PrintWriter out = null;
        InputStream in = null;
        try {
            URL realUrl = new URL(createQrCodeUrl);
            // 打开和URL之间的连接
            URLConnection conn = realUrl.openConnection();
            // 设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent",
                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            // 发送POST请求必须设置如下两行
            conn.setDoOutput(true);
            conn.setDoInput(true);
            // 获取URLConnection对象对应的输出流
            out = new PrintWriter(conn.getOutputStream());
            // 发送请求参数,利用connection的输出流，去写数据到connection中，我的参数数据流出我的电脑内存到connection中，让connection把参数帮我传到URL中去请求。
            log.info("wSharingLinks:{}", JSON.toJSON(wSharingLinks));
            out.print(JSON.toJSON(wSharingLinks));
            // flush输出流的缓冲
            out.flush();
            //获取URL的connection中的输入流，这个输入流就是我请求的url返回的数据,返回的数据在这个输入流中，流入我内存，我将从此流中读取数据。
            in = conn.getInputStream();
            //定义个空字节数组
            byte[] data = null;
            // 读取图片字节数组
            try {
                //创建字节数组输出流作为中转仓库，等待被写入数据
                ByteArrayOutputStream swapStream = new ByteArrayOutputStream();
                byte[] buff = new byte[100];
                int rc = 0;
                while ((rc = in.read(buff, 0, 100)) > 0) {
                    //向中转的输出流循环写出输入流中的数据
                    swapStream.write(buff, 0, rc);
                }
                //此时connection的输入流返回给我们的请求结果数据已经被完全地写入了我们定义的中转输出流swapStream中
                data = swapStream.toByteArray();
            } catch (IOException e) {
                log.error("error：" + e);
            } finally {
                if (in != null) {
                    try {
                        in.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return data;
        } catch (Exception e) {
            log.error("发送 POST 请求出现异常！" + e);
        }

        // 使用finally块来关闭输出流、输入流
        finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return new byte[0];
    }

    /*
     * 获取access_token
     * appid和appsecret到小程序后台获取，当然也可以让小程序开发人员给你传过来
     * apptype 1.小程序，2.微信公众号
     * */
    public String getAccess_token(String appid, String appsecret,String apptype) {
        log.info("获取token参数appid:{}",appid);
        log.info("获取token参数appsecret:{}",appsecret);
        log.info("获取token参数apptype:{}",apptype);
        String wxtoken = redisTemplate.opsForValue().get(apptype);
        log.info("走redis获取token:{}",wxtoken);
        if(Objects.isNull(wxtoken)){
            log.info("未走redis获取token开始:{}",wxtoken);
            //获取access_token
            String wurl = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential" +
                    "&appid=" + appid + "&secret=" + appsecret;
            RestTemplate restTemplate = new RestTemplate();
            String json = restTemplate.getForObject(wurl, String.class);
            AccessTokenEntity accessToken = JSONObject.parseObject(json, AccessTokenEntity.class);
            log.info("获取token返回值:{}", JSONObject.from(accessToken));

            wxtoken= accessToken.getAccess_token();
            redisTemplate.opsForValue().set(apptype, wxtoken);
            redisTemplate.expire(apptype, CAPTCHA_EXPIRE_TIME, TimeUnit.SECONDS);
            log.info("未走redis获取token结束:{}",redisTemplate.opsForValue().get(apptype));

        }
        return wxtoken;
    }
}
