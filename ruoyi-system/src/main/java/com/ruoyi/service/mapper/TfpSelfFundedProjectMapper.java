package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.TfpSelfFundedProject;
import org.apache.ibatis.annotations.Param;

/**
 * 自费项目Mapper接口
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public interface TfpSelfFundedProjectMapper 
{
    /**
     * 查询自费项目
     * 
     * @param id 自费项目主键
     * @return 自费项目
     */
    public TfpSelfFundedProject selectTfpSelfFundedProjectById(Long id);

    /**
     * 查询自费项目列表
     * 
     * @param tfpSelfFundedProject 自费项目
     * @return 自费项目集合
     */
    public List<TfpSelfFundedProject> selectTfpSelfFundedProjectList(TfpSelfFundedProject tfpSelfFundedProject);

    /**
     * 新增自费项目
     * 
     * @param tfpSelfFundedProject 自费项目
     * @return 结果
     */
    public int insertTfpSelfFundedProject(TfpSelfFundedProject tfpSelfFundedProject);

    /**
     * 修改自费项目
     * 
     * @param tfpSelfFundedProject 自费项目
     * @return 结果
     */
    public int updateTfpSelfFundedProject(TfpSelfFundedProject tfpSelfFundedProject);

    /**
     * 删除自费项目
     * 
     * @param id 自费项目主键
     * @return 结果
     */
    public int deleteTfpSelfFundedProjectById(Long id);

    /**
     * 批量删除自费项目
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpSelfFundedProjectByIds(Long[] ids);

    void deleteByProductId(@Param("productId") Long productId);

    List<TfpSelfFundedProject> selectListByProductId(@Param("productId") Long productId);

    void deleteTfpSelfFundedProjectByProductIds(@Param("productIdList") List<Long> productIdList);
}
