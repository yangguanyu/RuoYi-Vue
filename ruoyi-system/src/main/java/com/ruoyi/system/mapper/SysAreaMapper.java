package com.ruoyi.system.mapper;

import com.ruoyi.common.area.domain.entity.SysArea;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 地区Mapper接口
 * 
 * @author ruoyi
 * @date 2023-10-15
 */
public interface SysAreaMapper 
{
    /**
     * 查询地区
     * 
     * @param id 地区主键
     * @return 地区
     */
    public SysArea selectSysAreaById(Long id);

    /**
     * 查询地区列表
     * 
     * @param sysArea 地区
     * @return 地区集合
     */
    public List<SysArea> selectSysAreaList(SysArea sysArea);

    /**
     * 新增地区
     * 
     * @param sysArea 地区
     * @return 结果
     */
    public int insertSysArea(SysArea sysArea);

    /**
     * 修改地区
     * 
     * @param sysArea 地区
     * @return 结果
     */
    public int updateSysArea(SysArea sysArea);

    /**
     * 删除地区
     * 
     * @param id 地区主键
     * @return 结果
     */
    public int deleteSysAreaById(Long id);

    /**
     * 批量删除地区
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysAreaByIds(String[] ids);

    List<SysArea> selectSysAreaListByParentIdList(@Param("list") List<Long> parentIdList);

    List<SysArea> selectListByTypeAndCountryType(@Param("type") Integer type, @Param("countryType") String countryType);

    List<String> searchDepartureAreaNameByKey(@Param("key") String key);
}
