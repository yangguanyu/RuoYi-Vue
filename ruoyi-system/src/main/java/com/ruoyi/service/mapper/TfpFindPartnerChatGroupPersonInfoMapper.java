package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.TfpFindPartnerChatGroupPersonInfo;
import com.ruoyi.service.dto.TfpChatGroupPersonInfoDto;
import com.ruoyi.service.dto.TfpFindPartnerChatGroupPersonInfoDto;
import com.ruoyi.service.vo.TfpChatGroupPersonInfoVo;
import com.ruoyi.service.vo.TfpFindPartnerChatGroupPersonInfoVo;
import org.apache.ibatis.annotations.Param;

/**
 * 找搭子聊天群组成员聊天信息Mapper接口
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public interface TfpFindPartnerChatGroupPersonInfoMapper 
{
    /**
     * 查询找搭子聊天群组成员聊天信息
     * 
     * @param id 找搭子聊天群组成员聊天信息主键
     * @return 找搭子聊天群组成员聊天信息
     */
    public TfpFindPartnerChatGroupPersonInfo selectTfpFindPartnerChatGroupPersonInfoById(Long id);

    /**
     * 查询找搭子聊天群组成员聊天信息列表
     * 
     * @param tfpFindPartnerChatGroupPersonInfo 找搭子聊天群组成员聊天信息
     * @return 找搭子聊天群组成员聊天信息集合
     */
    public List<TfpFindPartnerChatGroupPersonInfo> selectTfpFindPartnerChatGroupPersonInfoList(TfpFindPartnerChatGroupPersonInfo tfpFindPartnerChatGroupPersonInfo);

    /**
     * 新增找搭子聊天群组成员聊天信息
     * 
     * @param tfpFindPartnerChatGroupPersonInfo 找搭子聊天群组成员聊天信息
     * @return 结果
     */
    public int insertTfpFindPartnerChatGroupPersonInfo(TfpFindPartnerChatGroupPersonInfo tfpFindPartnerChatGroupPersonInfo);

    /**
     * 修改找搭子聊天群组成员聊天信息
     * 
     * @param tfpFindPartnerChatGroupPersonInfo 找搭子聊天群组成员聊天信息
     * @return 结果
     */
    public int updateTfpFindPartnerChatGroupPersonInfo(TfpFindPartnerChatGroupPersonInfo tfpFindPartnerChatGroupPersonInfo);

    /**
     * 删除找搭子聊天群组成员聊天信息
     * 
     * @param id 找搭子聊天群组成员聊天信息主键
     * @return 结果
     */
    public int deleteTfpFindPartnerChatGroupPersonInfoById(Long id);

    /**
     * 批量删除找搭子聊天群组成员聊天信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpFindPartnerChatGroupPersonInfoByIds(Long[] ids);

    List<TfpFindPartnerChatGroupPersonInfoVo> selectTfpFindPartnerChatGroupPersonInfoDtoList(TfpFindPartnerChatGroupPersonInfoDto tfpFindPartnerChatGroupPersonInfoDto);

    List<TfpFindPartnerChatGroupPersonInfo> selectListByFindPartnerChatGroupId(@Param("findPartnerChatGroupId") Long findPartnerChatGroupId);

    List<TfpFindPartnerChatGroupPersonInfoVo> selectAppletInfoList(TfpChatGroupPersonInfoDto tfpChatGroupPersonInfoDto);
}
