package com.ruoyi.web.controller.service;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.utils.bean.CustomizeBeanCopier;
import com.ruoyi.service.dto.ListFindPartnerSignUpDto;
import com.ruoyi.service.vo.*;
import com.ruoyi.web.controller.service.form.partner.FindPartnerSignUpAddForm;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.TfpFindPartnerSignUp;
import com.ruoyi.service.service.ITfpFindPartnerSignUpService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 找搭子Controller
 *
 * @author ruoyi
 * @date 2023-10-31
 */
@RestController
@RequestMapping("/service/findPartnerSignUp")
public class TfpFindPartnerSignUpController extends BaseController
{
    @Autowired
    private ITfpFindPartnerSignUpService tfpFindPartnerSignUpService;

    /**
     * 查询找搭子列表
     */
    @PreAuthorize("@ss.hasPermi('service:findPartnerSignUp:list')")
    @GetMapping("/list")
    public TableDataInfo list(TfpFindPartnerSignUp tfpFindPartnerSignUp)
    {
        startPage();
        List<TfpFindPartnerSignUpVo> list = tfpFindPartnerSignUpService.selectTfpFindPartnerSignUpVoList(tfpFindPartnerSignUp);
        return getDataTable(list);
    }

    /**
     * 导出找搭子列表
     */
    @PreAuthorize("@ss.hasPermi('service:findPartnerSignUp:export')")
    @Log(title = "找搭子", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TfpFindPartnerSignUp tfpFindPartnerSignUp)
    {
        List<TfpFindPartnerSignUp> list = tfpFindPartnerSignUpService.selectTfpFindPartnerSignUpList(tfpFindPartnerSignUp);
        ExcelUtil<TfpFindPartnerSignUp> util = new ExcelUtil<TfpFindPartnerSignUp>(TfpFindPartnerSignUp.class);
        util.exportExcel(response, list, "找搭子数据");
    }

    /**
     * 获取找搭子详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:findPartnerSignUp:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tfpFindPartnerSignUpService.selectTfpFindPartnerSignUpById(id));
    }

    /**
     * 新增找搭子
     */
    @PreAuthorize("@ss.hasPermi('service:findPartnerSignUp:add')")
    @Log(title = "找搭子", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TfpFindPartnerSignUp tfpFindPartnerSignUp)
    {
        return toAjax(tfpFindPartnerSignUpService.insertTfpFindPartnerSignUp(tfpFindPartnerSignUp));
    }

    /**
     * 修改找搭子
     */
    @PreAuthorize("@ss.hasPermi('service:findPartnerSignUp:edit')")
    @Log(title = "找搭子", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TfpFindPartnerSignUp tfpFindPartnerSignUp)
    {
        return toAjax(tfpFindPartnerSignUpService.updateTfpFindPartnerSignUp(tfpFindPartnerSignUp));
    }

    /**
     * 删除找搭子
     */
    @PreAuthorize("@ss.hasPermi('service:findPartnerSignUp:remove')")
    @Log(title = "找搭子", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tfpFindPartnerSignUpService.deleteTfpFindPartnerSignUpByIds(ids));
    }

    @PreAuthorize("@ss.hasPermi('service:findPartnerSignUp:list')")
    @PostMapping("/signUpUserSelect")
    public R<List<UserVo>> signUpUserSelect()
    {
        return R.ok(tfpFindPartnerSignUpService.signUpUserSelect());
    }

    @ApiOperation("小程序-搭子报名列表-已上车")
    @PostMapping("/listFindPartnerSignUp")
    @ResponseBody
    public TableDataInfo<ListFindPartnerSignUpVo> listFindPartnerSignUp(@RequestBody ListFindPartnerSignUpDto listFindPartnerSignUpDto)
    {
        return getDataTable(tfpFindPartnerSignUpService.listFindPartnerSignUp(listFindPartnerSignUpDto));
    }

    @ApiOperation("小程序-搭子报名详情")
    @PostMapping("/detailFindPartnerSignUp")
    @ResponseBody
    public R<DetailFindPartnerSignUpVo> detailFindPartnerSignUp(@RequestParam("id") Long id)
    {
        return R.ok(tfpFindPartnerSignUpService.detailFindPartnerSignUp(id));
    }

    @ApiOperation("小程序-确认搭子报名")
    @PostMapping("/auditPassSignUp")
    @ResponseBody
    public R auditPassSignUp(@RequestParam("id") Long id) {
        return R.ok(tfpFindPartnerSignUpService.auditPassSignUp(id));
    }

    @ApiOperation("小程序-报名上车")
    @PostMapping("/applet/add")
    public AjaxResult appletAdd(@RequestBody FindPartnerSignUpAddForm form) {
        TfpFindPartnerSignUp tfpFindPartnerSignUp = CustomizeBeanCopier.copyProperties(form, TfpFindPartnerSignUp.class);
        int result = tfpFindPartnerSignUpService.appletAdd(tfpFindPartnerSignUp);
        return toAjax(result);
    }
}
