package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.TfpFindPartner;
import com.ruoyi.service.dto.TfpFindPartnerDto;
import com.ruoyi.service.vo.InitiateFindPartnerVo;
import com.ruoyi.service.vo.MyJoinFindPartnerVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 找搭子Mapper接口
 *
 * @author ruoyi
 * @date 2023-11-01
 */
@Mapper
public interface TfpFindPartnerMapper
{
    /**
     * 查询找搭子
     *
     * @param id 找搭子主键
     * @return 找搭子
     */
    public TfpFindPartner selectTfpFindPartnerById(Long id);

    /**
     * 查询找搭子列表
     *
     * @param tfpFindPartner 找搭子
     * @return 找搭子集合
     */
    public List<TfpFindPartner> selectTfpFindPartnerList(TfpFindPartnerDto tfpFindPartner);

    public List<TfpFindPartner> selectProgressList(TfpFindPartnerDto tfpFindPartner);

    /**
     * 新增找搭子
     *
     * @param tfpFindPartner 找搭子
     * @return 结果
     */
    public int insertTfpFindPartner(TfpFindPartner tfpFindPartner);

    /**
     * 修改找搭子
     *
     * @param tfpFindPartner 找搭子
     * @return 结果
     */
    public int updateTfpFindPartner(TfpFindPartner tfpFindPartner);

    /**
     * 删除找搭子
     *
     * @param id 找搭子主键
     * @return 结果
     */
    public int deleteTfpFindPartnerById(Long id);

    /**
     * 批量删除找搭子
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpFindPartnerByIds(Long[] ids);

    List<TfpFindPartner> selectTfpFindPartnerListByIdList(@Param("idList") List<Long> idList);

    List<InitiateFindPartnerVo> initiateFindPartnerList(@Param("userId") Long userId);

    List<MyJoinFindPartnerVo> myJoinFindPartnerList(@Param("idList") List<Long> idList);

    List<TfpFindPartner> selectOverAuditTimeFindPartnerList();
}
