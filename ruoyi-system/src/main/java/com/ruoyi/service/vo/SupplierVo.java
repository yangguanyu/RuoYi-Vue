package com.ruoyi.service.vo;

import lombok.Data;

@Data
public class SupplierVo {
    private Long id;
    private String name;
}
