package com.ruoyi.service.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel(value = "MyJoinFindPartnerVo", description = "我参与的搭子")
public class MyJoinFindPartnerVo {
    @ApiModelProperty("搭子id")
    private Long id;
    @ApiModelProperty("创建搭子用户昵称")
    private String nickName;
    @ApiModelProperty("创建搭子用户头像图片")
    private String headImage;
    @ApiModelProperty("标题")
    private String title;
    @ApiModelProperty("图片")
    private String image;
    @ApiModelProperty("展示状态")
    private String showStatus;
    @ApiModelProperty("搭子聊天室id")
    private Long groupId;
    @ApiModelProperty("报名人数")
    private Integer signUpPersonNumber;
    @ApiModelProperty("类型名称")
    private String typeName;
    @ApiModelProperty("审核状态 0-待审核/等待发起人确认 1-审核通过/报名成功 2-审核未通过")
    private Integer auditStatus;

    @JsonIgnore
    private Long createUserId;
    @JsonIgnore
    private Date startTime;
    @JsonIgnore
    private Date signUpEndTime;
    @JsonIgnore
    private Integer status;
}
