package com.ruoyi.service.service.impl;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.*;
import java.util.stream.Collectors;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.entity.TfpAttachment;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enuma.*;
import com.ruoyi.common.exception.BusinessException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.DcDateUtils;
import com.ruoyi.common.utils.DcListUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.service.domain.TfpUserBankCard;
import com.ruoyi.service.domain.TfpUserIncome;
import com.ruoyi.service.dto.ApplyDrawMoneyDto;
import com.ruoyi.service.dto.AuditDto;
import com.ruoyi.service.dto.DrawMoneyAuditListDto;
import com.ruoyi.service.service.ITfpAttachmentService;
import com.ruoyi.service.service.ITfpUserBankCardService;
import com.ruoyi.service.service.ITfpUserIncomeService;
import com.ruoyi.service.vo.*;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpDrawMoneyMapper;
import com.ruoyi.service.domain.TfpDrawMoney;
import com.ruoyi.service.service.ITfpDrawMoneyService;
import org.springframework.transaction.annotation.Transactional;

import static com.ruoyi.common.utils.PageUtils.startPage;

/**
 * 提款Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@Service
@Transactional
public class TfpDrawMoneyServiceImpl implements ITfpDrawMoneyService 
{
    @Autowired
    private TfpDrawMoneyMapper tfpDrawMoneyMapper;
    @Autowired
    private ITfpUserIncomeService userIncomeService;
    @Autowired
    private ITfpUserBankCardService userBankCardService;
    @Autowired
    private ITfpAttachmentService attachmentService;
    @Autowired
    private ISysUserService userService;

    @Value("${draw.price.limit.every.day}")
    private Integer drawPriceLimitEveryDay;

    /**
     * 查询提款
     * 
     * @param id 提款主键
     * @return 提款
     */
    @Override
    public TfpDrawMoney selectTfpDrawMoneyById(Long id)
    {
        return tfpDrawMoneyMapper.selectTfpDrawMoneyById(id);
    }

    /**
     * 查询提款列表
     * 
     * @param tfpDrawMoney 提款
     * @return 提款
     */
    @Override
    public List<TfpDrawMoney> selectTfpDrawMoneyList(TfpDrawMoney tfpDrawMoney)
    {
        return tfpDrawMoneyMapper.selectTfpDrawMoneyList(tfpDrawMoney);
    }

    /**
     * 新增提款
     * 
     * @param tfpDrawMoney 提款
     * @return 结果
     */
    @Override
    public int insertTfpDrawMoney(TfpDrawMoney tfpDrawMoney)
    {
        tfpDrawMoney.setCreateTime(DateUtils.getNowDate());
        return tfpDrawMoneyMapper.insertTfpDrawMoney(tfpDrawMoney);
    }

    /**
     * 修改提款
     * 
     * @param tfpDrawMoney 提款
     * @return 结果
     */
    @Override
    public int updateTfpDrawMoney(TfpDrawMoney tfpDrawMoney)
    {
        tfpDrawMoney.setUpdateTime(DateUtils.getNowDate());
        return tfpDrawMoneyMapper.updateTfpDrawMoney(tfpDrawMoney);
    }

    /**
     * 批量删除提款
     * 
     * @param ids 需要删除的提款主键
     * @return 结果
     */
    @Override
    public int deleteTfpDrawMoneyByIds(Long[] ids)
    {
        return tfpDrawMoneyMapper.deleteTfpDrawMoneyByIds(ids);
    }

    /**
     * 删除提款信息
     * 
     * @param id 提款主键
     * @return 结果
     */
    @Override
    public int deleteTfpDrawMoneyById(Long id)
    {
        return tfpDrawMoneyMapper.deleteTfpDrawMoneyById(id);
    }

    /**
     * 获取提现记录数量
     * @param userId
     * @return
     */
    @Override
    public Integer getDrawMoneyRecordSizeByUserId(Long userId) {
        return tfpDrawMoneyMapper.getDrawMoneyRecordSizeByUserId(userId);
    }

    /**
     * 小程序-提现列表
     * @return
     */
    @Override
    public List<DrawMoneyListVo> drawMoneyList() {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = loginUser.getUserId();

        startPage();
        List<DrawMoneyListVo> drawMoneyListVoList = tfpDrawMoneyMapper.drawMoneyList(userId);
        if (DcListUtils.isNotEmpty(drawMoneyListVoList)){
            for (DrawMoneyListVo drawMoneyListVo : drawMoneyListVoList) {
                String bankCardNumber = drawMoneyListVo.getBankCardNumber();
                drawMoneyListVo.setBankCardNumber(this.encryBankCardNumber(bankCardNumber));
            }
        }

        return drawMoneyListVoList;
    }

    /**
     * 小程序-提现申请
     * @param applyDrawMoneyDto
     * @return
     */
    @Override
    public Long applyDrawMoney(ApplyDrawMoneyDto applyDrawMoneyDto) {
        boolean flag = this.judgeLimitEveryDay();
        if (!flag){
            throw new RuntimeException("今日提现次数已达上限，请明日在进行操作");
        }

        TfpUserBankCard userBankCard = userBankCardService.getMyUserBankCardInfo();
        if (Objects.isNull(userBankCard)){
            throw new RuntimeException("未绑定银行卡，无法提现");
        }

        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = loginUser.getUserId();

        TfpUserIncome userIncome = userIncomeService.getUserIncomeByUserId(userId);
        BigDecimal myRemainingPrice = userIncome.getRemainingPrice();
        if (myRemainingPrice.compareTo(BigDecimal.ZERO)< 1){
            throw new RuntimeException("余额不足，无法提现");
        }

        BigDecimal drawMoneyPrice = null;
        Integer applyDrawMoneyType = applyDrawMoneyDto.getApplyDrawMoneyType();
        if (Objects.equals(ApplyDrawMoneyTypeEnum.PART.getValue(), applyDrawMoneyType)){
            drawMoneyPrice = applyDrawMoneyDto.getDrawMoneyPrice();
            if (myRemainingPrice.compareTo(drawMoneyPrice) == -1){
                throw new RuntimeException("余额不足，无法提现");
            }
        }

        if (Objects.equals(ApplyDrawMoneyTypeEnum.ALL.getValue(), applyDrawMoneyType)){
            drawMoneyPrice = myRemainingPrice;
        }


        TfpDrawMoney tfpDrawMoney = new TfpDrawMoney();
        tfpDrawMoney.setUserId(userId);
        tfpDrawMoney.setUserBankCardId(userBankCard.getId());
        tfpDrawMoney.setBankCardNumber(userBankCard.getBankCardNumber());
        tfpDrawMoney.setBankId(userBankCard.getBankId());
        tfpDrawMoney.setBankName(userBankCard.getBankName());
        tfpDrawMoney.setDrawMoneyPrice(drawMoneyPrice);
        tfpDrawMoney.setStatus(DrawMoneyStatusEnum.APPLY.getValue());

        tfpDrawMoney.setDelFlag(DeleteEnum.EXIST.getValue());
        tfpDrawMoney.setCreateUserId(userId);
        tfpDrawMoney.setCreateBy(loginUser.getUsername());
        tfpDrawMoney.setCreateTime(DateUtils.getNowDate());
        tfpDrawMoneyMapper.insertTfpDrawMoney(tfpDrawMoney);

        Long id = tfpDrawMoney.getId();
        String drawMoneyNumber = this.createDrawMoneyNumber(id);
        tfpDrawMoney.setDrawMoneyNumber(drawMoneyNumber);
        tfpDrawMoneyMapper.updateTfpDrawMoney(tfpDrawMoney);

        // 用户账户修改
        BigDecimal remainingPrice = myRemainingPrice.subtract(drawMoneyPrice);
        userIncome.setRemainingPrice(remainingPrice);
        userIncome.setUpdateUserId(loginUser.getUserId());
        userIncome.setUpdateBy(loginUser.getUsername());
        userIncome.setUpdateTime(DateUtils.getNowDate());
        userIncomeService.updateTfpUserIncome(userIncome);


        return id;
    }

    /**
     * 小程序-提现详情
     * @param id
     * @return
     */
    @Override
    public DetailDrawMoneyVo detailDrawMoney(Long id) {
        TfpDrawMoney tfpDrawMoney = tfpDrawMoneyMapper.selectTfpDrawMoneyById(id);
        if (Objects.isNull(tfpDrawMoney)){
            throw new RuntimeException("提现信息不存在");
        }

        DetailDrawMoneyVo detailDrawMoneyVo = new DetailDrawMoneyVo();
        BeanUtils.copyProperties(tfpDrawMoney, detailDrawMoneyVo);

        String bankCardNumber = tfpDrawMoney.getBankCardNumber();
        detailDrawMoneyVo.setBankCardNumber(bankCardNumber);
        return detailDrawMoneyVo;
    }

    /**
     * 小程序-审核提现
     * @param auditDto
     * @return
     */
    @Override
    public int auditDrawMoney(AuditDto auditDto) {
        Integer auditStatus = auditDto.getAuditStatus();
        if (!Objects.equals(AuditStatusEnum.AUDIT_PASS.getValue(), auditStatus) && !Objects.equals(AuditStatusEnum.AUDIT_NO_PASS.getValue(), auditStatus)){
            throw new BusinessException("审核状态有误");
        }

        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = loginUser.getUserId();

        Long id = auditDto.getId();
        TfpDrawMoney tfpDrawMoney = tfpDrawMoneyMapper.selectTfpDrawMoneyById(id);

        tfpDrawMoney.setStatus(auditStatus);
        if (Objects.equals(AuditStatusEnum.AUDIT_NO_PASS.getValue(), auditStatus)){
            tfpDrawMoney.setReason(auditDto.getReason());
        } else {
            List<String> urlList = auditDto.getUrlList();
            if (DcListUtils.isNotEmpty(urlList)){
                String name = "打款凭证";
                attachmentService.saveImageList(id, name, urlList, BusinessTypeEnum.DRAW_MONEY.getValue(), BusinessSubTypeEnum.DRAW_MONEY_SETTLEMENT_VOUCHER.getValue());
            }
        }

        tfpDrawMoney.setUpdateUserId(userId);
        tfpDrawMoney.setUpdateBy(loginUser.getUsername());
        tfpDrawMoney.setUpdateTime(DateUtils.getNowDate());
        tfpDrawMoneyMapper.updateTfpDrawMoney(tfpDrawMoney);

        if (Objects.equals(AuditStatusEnum.AUDIT_NO_PASS.getValue(), auditStatus)){
            TfpUserIncome userIncome = userIncomeService.getUserIncomeByUserId(tfpDrawMoney.getUserId());
            BigDecimal myRemainingPrice = userIncome.getRemainingPrice();

            BigDecimal remainingPrice = myRemainingPrice.add(tfpDrawMoney.getDrawMoneyPrice());
            userIncome.setRemainingPrice(remainingPrice);
            userIncome.setUpdateUserId(loginUser.getUserId());
            userIncome.setUpdateBy(loginUser.getUsername());
            userIncome.setUpdateTime(DateUtils.getNowDate());
            userIncomeService.updateTfpUserIncome(userIncome);
        }

        return 1;
    }

    @Override
    public List<DrawMoneyAuditListVo> drawMoneyAuditList(DrawMoneyAuditListDto drawMoneyAuditListDto) {
        startPage();
        List<DrawMoneyAuditListVo> drawMoneyAuditListVoList = tfpDrawMoneyMapper.drawMoneyAuditList(drawMoneyAuditListDto);
        if (DcListUtils.isNotEmpty(drawMoneyAuditListVoList)){
            List<Long> userBankCardIdList = new ArrayList<>();
            List<Long> userIdList = new ArrayList<>();

            for (DrawMoneyAuditListVo drawMoneyAuditListVo : drawMoneyAuditListVoList) {
                Long userBankCardId = drawMoneyAuditListVo.getUserBankCardId();
                if (!userBankCardIdList.contains(userBankCardId)){
                    userBankCardIdList.add(userBankCardId);
                }

                Long createUserId = drawMoneyAuditListVo.getCreateUserId();
                if (!userIdList.contains(createUserId)){
                    userIdList.add(createUserId);
                }
            }

            List<SysUser> sysUserList = userService.selectUserInfoByUserIdList(userIdList);
            Map<Long, String> userMap;
            if (DcListUtils.isNotEmpty(sysUserList)){
                userMap = sysUserList.stream().collect(Collectors.toMap(SysUser::getUserId, SysUser::getRealName));
            } else {
                userMap = new HashMap<>();
            }

            List<TfpUserBankCard> userBankCardList = userBankCardService.selectTfpUserBankCardListByIdList(userBankCardIdList);
            Map<Long, TfpUserBankCard> userBankCardMap;
            if (DcListUtils.isNotEmpty(sysUserList)){
                userBankCardMap = userBankCardList.stream().collect(Collectors.toMap(TfpUserBankCard::getId, TfpUserBankCard -> TfpUserBankCard));
            } else {
                userBankCardMap = new HashMap<>();
            }

            for (DrawMoneyAuditListVo drawMoneyAuditListVo : drawMoneyAuditListVoList) {
                drawMoneyAuditListVo.setRealName(userMap.get(drawMoneyAuditListVo.getCreateUserId()));

                TfpUserBankCard userBankCard = userBankCardMap.get(drawMoneyAuditListVo.getUserBankCardId());
                if (Objects.nonNull(userBankCard)){
                    DrawMoneyBankCardInfoVo drawMoneyBankCardInfo = new DrawMoneyBankCardInfoVo();
                    BeanUtils.copyProperties(userBankCard, drawMoneyBankCardInfo);
                    drawMoneyAuditListVo.setDrawMoneyBankCardInfo(drawMoneyBankCardInfo);
                }
            }
        }

        return drawMoneyAuditListVoList;
    }

    /**
     * 后台-提现详情
     * @param id
     * @return
     */
    @Override
    public DrawMoneyDetailVo drawMoneyDetail(Long id) {
        TfpDrawMoney tfpDrawMoney = tfpDrawMoneyMapper.selectTfpDrawMoneyById(id);
        if (Objects.isNull(tfpDrawMoney)){
            throw new BusinessException("提款信息不存在");
        }

        DrawMoneyDetailVo drawMoneyDetailVo = new DrawMoneyDetailVo();
        BeanUtils.copyProperties(tfpDrawMoney, drawMoneyDetailVo);

        Long createUserId = tfpDrawMoney.getCreateUserId();
        SysUser sysUser = userService.selectUserById(createUserId);

        drawMoneyDetailVo.setRealName(sysUser.getRealName());

        Long userBankCardId = tfpDrawMoney.getUserBankCardId();
        TfpUserBankCard userBankCard = userBankCardService.selectTfpUserBankCardById(userBankCardId);
        if (Objects.nonNull(userBankCard)){
            DrawMoneyBankCardInfoVo drawMoneyBankCardInfo = new DrawMoneyBankCardInfoVo();
            BeanUtils.copyProperties(userBankCard, drawMoneyBankCardInfo);
            drawMoneyDetailVo.setDrawMoneyBankCardInfo(drawMoneyBankCardInfo);
        }

        List<TfpAttachment> tfpAttachmentList = attachmentService.selectListByBusinessId(id, BusinessTypeEnum.DRAW_MONEY.getValue(), BusinessSubTypeEnum.DRAW_MONEY_SETTLEMENT_VOUCHER.getValue());
        if (DcListUtils.isNotEmpty(tfpAttachmentList)){
            List<String> urlList = tfpAttachmentList.stream().map(TfpAttachment::getUrl).collect(Collectors.toList());
            drawMoneyDetailVo.setUrlList(urlList);
        }

        return drawMoneyDetailVo;
    }

    /**
     * 用户提现总额
     * @return
     */
    @Override
    public BigDecimal getUserCumulativeIncome() {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = loginUser.getUserId();
        BigDecimal userCumulativeIncome = tfpDrawMoneyMapper.getCumulativeIncomeByUserId(userId);
        return Objects.isNull(userCumulativeIncome) ? BigDecimal.ZERO : userCumulativeIncome;
    }

    @Override
    public List<UserIncomeDetailListVo> userIncomeDetailList() {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = loginUser.getUserId();

        startPage();
        return tfpDrawMoneyMapper.getUserIncomeDetailListByUserId(userId);
    }

    /**
     * 加密银行卡号
     * @param bankCardNumber
     * @return
     */
    private String encryBankCardNumber(String bankCardNumber){
        int length = bankCardNumber.length();
        StringBuffer encryBankCardNumber = new StringBuffer();
        for (int i = 0; i < length - 4; i++) {
            encryBankCardNumber.append("*");
        }

        encryBankCardNumber.append(bankCardNumber.substring(length - 4));
        return encryBankCardNumber.toString();
    }

    /**
     * 判断每日限制次数 true:未超过,今日可提现 false:已超过，今日不可提现
     * @return
     */
    private boolean judgeLimitEveryDay(){
        boolean flag = true;
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = loginUser.getUserId();

        Date now = new Date();
        Date startDate = DcDateUtils.handelDateToDayStart(now);
        Date endDate = DcDateUtils.handelDateToDayEnd(now);

        List<TfpDrawMoney> drawMoneyList = tfpDrawMoneyMapper.selectTfpDrawMoneyListByNow(userId, startDate, endDate);
        if (DcListUtils.isNotEmpty(drawMoneyList) && drawMoneyList.size() >= drawPriceLimitEveryDay){
            flag = false;
        }

        return flag;
    }

    private String createDrawMoneyNumber(Long id) {
        String drawMoneyNumber;
        // yyyyMMddHHmmss
        DateFormat yMdDateFormat = DcDateUtils.getDcDateFormat(DcDateUtils.yMdHms);
        String DateFormatStr = yMdDateFormat.format(new Date());

        int idLength = id.toString().length();
        if (idLength > 6){
            drawMoneyNumber = DateFormatStr + String.format("%0" + idLength + "d", id);
        } else {
            drawMoneyNumber = DateFormatStr + String.format("%06d", id);
        }

        return drawMoneyNumber;
    }
}
