package com.ruoyi.common.enuma.flight;

import com.ruoyi.common.enuma.BaseStrEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum LowestPriceExceptionCode implements BaseStrEnum {
    START_DATE_MUST_BE_EARLIER("041501", "开始日期必须早于结束时间"),
    INTERVAL_EXCEEDS_THIRTY_DAYS("041502", "时间间隔不能超过30天"),
    REQUEST_TIMEOUT_OR_EMPTY("011503", "请求超时或数据为空");

    private final String value;
    private final String desc;
}
