package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel(value = "TfpTravelRuleDto")
public class TfpTravelRuleDto {
    @ApiModelProperty("规则类型 0-组队出行 1-独立出行")
    private Integer travelType;

    @ApiModelProperty("版本号")
    @NotNull(message = "版本号不能为空")
    private String version;

    @ApiModelProperty("文案描述")
    @NotNull(message = "文案描述不能为空")
    private String text;

    @ApiModelProperty("类型 0-旅行方式")
    @NotNull(message = "类型不能为空")
    private Integer type;
}
