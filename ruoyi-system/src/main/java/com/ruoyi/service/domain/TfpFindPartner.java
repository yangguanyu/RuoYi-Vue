package com.ruoyi.service.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.entity.TfpAttachment;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 找搭子对象 tfp_find_partner
 *
 * @author ruoyi
 * @date 2023-11-01
 */
public class TfpFindPartner extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 标题 */
    @Excel(name = "标题")
    private String title;

    /** 描述 */
    @Excel(name = "描述")
    private String activityDesc;

    /** 类型code */
    @Excel(name = "类型code")
    private String typeCode;

    /** 类型名称 */
    @Excel(name = "类型名称")
    private String typeName;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startTime;

    /** 报名截止时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "报名截止时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date signUpEndTime;

    /** 活动人数 */
    @Excel(name = "活动人数")
    private Long activityPersonNumber;

    /** 活动费用 */
    @Excel(name = "活动费用")
    private BigDecimal activityPrice;

    /** 活动地点 */
    @Excel(name = "活动地点")
    private String activityPlace;

    /** 活动地点详情 */
    @Excel(name = "活动地点详情")
    private String activityPlaceDetail;

    /** 找搭子发布和撤销等状态 0-未发布 1-审核通过 2-审核未通过 3-已发布待审核 4-因疑似非法而下架 */
    @Excel(name = "找搭子发布和撤销等状态 0-未发布 1-审核通过 2-审核未通过 3-已发布待审核 4-因疑似非法而下架")
    private Integer status;

    //找搭子状态 1：找搭子进行中 2：即将发车 3:已结束
    private Integer activityStatus;

    /** 审核不通过原因 */
    @Excel(name = "审核不通过原因")
    private String reason;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    @Excel(name = "创建者id")
    private Long createUserId;

    /** 更新者id */
    @Excel(name = "更新者id")
    private Long updateUserId;

    //创建人联系电话
    private String phonenumber;

    private List<TfpAttachment> attachmentList;

    //报名人数
    private Integer signUpCount=0;

    //状态名称
    private String statusName;

    //价格类型编码，免费。线下付，发起人代付
    private Long priceCode;

    public Long getPriceCode() {
        return priceCode;
    }

    public void setPriceCode(Long priceCode) {
        this.priceCode = priceCode;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public Integer getSignUpCount() {
        return signUpCount;
    }

    public void setSignUpCount(Integer signUpCount) {
        this.signUpCount = signUpCount;
    }

    public List<TfpAttachment> getAttachmentList() {
        return attachmentList;
    }

    public void setAttachmentList(List<TfpAttachment> attachmentList) {
        this.attachmentList = attachmentList;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getTitle()
    {
        return title;
    }

    public String getActivityDesc() {
        return activityDesc;
    }

    public void setActivityDesc(String activityDesc) {
        this.activityDesc = activityDesc;
    }

    public void setTypeCode(String typeCode)
    {
        this.typeCode = typeCode;
    }

    public String getTypeCode()
    {
        return typeCode;
    }
    public void setTypeName(String typeName)
    {
        this.typeName = typeName;
    }

    public String getTypeName()
    {
        return typeName;
    }
    public void setStartTime(Date startTime)
    {
        this.startTime = startTime;
    }

    public Date getStartTime()
    {
        return startTime;
    }
    public void setSignUpEndTime(Date signUpEndTime)
    {
        this.signUpEndTime = signUpEndTime;
    }

    public Date getSignUpEndTime()
    {
        return signUpEndTime;
    }
    public void setActivityPersonNumber(Long activityPersonNumber)
    {
        this.activityPersonNumber = activityPersonNumber;
    }

    public Long getActivityPersonNumber()
    {
        return activityPersonNumber;
    }
    public void setActivityPrice(BigDecimal activityPrice)
    {
        this.activityPrice = activityPrice;
    }

    public BigDecimal getActivityPrice()
    {
        return activityPrice;
    }
    public void setActivityPlace(String activityPlace)
    {
        this.activityPlace = activityPlace;
    }

    public String getActivityPlace()
    {
        return activityPlace;
    }

    public String getActivityPlaceDetail() {
        return activityPlaceDetail;
    }

    public void setActivityPlaceDetail(String activityPlaceDetail) {
        this.activityPlaceDetail = activityPlaceDetail;
    }

    public void setStatus(Integer status)
    {
        this.status = status;
    }

    public Integer getStatus()
    {
        return status;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }
    public void setCreateUserId(Long createUserId)
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId()
    {
        return createUserId;
    }
    public void setUpdateUserId(Long updateUserId)
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId()
    {
        return updateUserId;
    }

    public Integer getActivityStatus() {
        return activityStatus;
    }

    public void setActivityStatus(Integer activityStatus) {
        this.activityStatus = activityStatus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("title", getTitle())
            .append("activityDesc", getActivityDesc())
            .append("typeCode", getTypeCode())
            .append("typeName", getTypeName())
            .append("startTime", getStartTime())
            .append("signUpEndTime", getSignUpEndTime())
            .append("activityPersonNumber", getActivityPersonNumber())
            .append("activityPrice", getActivityPrice())
            .append("activityPlace", getActivityPlace())
            .append("activityPlaceDetail", getActivityPlaceDetail())
            .append("status", getStatus())
            .append("reason", getReason())
            .append("delFlag", getDelFlag())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
