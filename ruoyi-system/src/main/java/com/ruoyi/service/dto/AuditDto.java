package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "AuditDto")
public class AuditDto {
    @ApiModelProperty("审核业务id")
    private Long id;
    @ApiModelProperty("审核状态 1-审核通过 2-审核不通过")
    private Integer auditStatus;
    @ApiModelProperty("审核失败原因")
    private String reason;
    @ApiModelProperty("凭证")
    private List<String> urlList;
}
