drop table sys_user_wx;
create table sys_user_wx (
         id           bigint(20)      not null auto_increment    comment 'id',
         user_id         bigint(20)      not null                 comment '用户id',
         union_id         varchar(50)     default null             comment '微信union id',
         open_id         varchar(50)     default null                 comment '微信 open id',
         nick_name            varchar(20)     default null               comment '昵称',
         login_name             varchar(20)     default null               comment '登录用户名',
         phonenumber             varchar(20)     default null               comment '联系电话',
         app_desc            varchar(50)         default null               comment '微信应用描述',
         del_flag          char(1)         default '0'                comment '删除标志（0代表存在 2代表删除）',
         create_user_id         bigint(20)      default null                 comment '创建者id',
         create_by         varchar(64)     default ''                 comment '创建者',
         create_time 	    datetime                                   comment '创建时间',
         update_user_id         bigint(20)      default null                 comment '更新者id',
         update_by         varchar(64)     default ''                 comment '更新者',
         update_time       datetime                                   comment '更新时间',
         primary key (id)
) engine=innodb auto_increment=200 comment = '用户微信表';

# 初始化小程序用户dept 和 role
INSERT INTO sys_dept(dept_id, parent_id, dept_name, ancestors, order_num, leader, phone, email, STATUS, del_flag, create_by, create_time)
VALUES(3, 0, '小程序用户', 0, 0, 'admin', NULL, NULL, 0, 0, 'admin', NOW());

INSERT INTO sys_role_dept(role_id, dept_id) VALUES(2, 3);