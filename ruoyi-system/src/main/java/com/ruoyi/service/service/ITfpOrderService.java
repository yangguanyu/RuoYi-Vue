package com.ruoyi.service.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import com.ruoyi.service.domain.TfpOrder;
import com.ruoyi.service.domain.TfpProduct;
import com.ruoyi.service.dto.*;
import com.ruoyi.service.excel.OrderListExportExcel;
import com.ruoyi.service.vo.*;

/**
 * 订单Service接口
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public interface ITfpOrderService 
{
    /**
     * 查询订单
     * 
     * @param id 订单主键
     * @return 订单
     */
    public TfpOrder selectTfpOrderById(Long id);

    /**
     * 查询订单列表
     * 
     * @param tfpOrder 订单
     * @return 订单集合
     */
    public List<TfpOrder> selectTfpOrderList(TfpOrder tfpOrder);

    List<TfpOrderVo> selectTfpOrderVoList(TfpOrderDto tfpOrderDto);

    /**
     * 新增订单
     * 
     * @param tfpOrder 订单
     * @return 结果
     */
    public int insertTfpOrder(TfpOrder tfpOrder);

    /**
     * 修改订单
     * 
     * @param tfpOrder 订单
     * @return 结果
     */
    public int updateTfpOrder(TfpOrder tfpOrder);

    /**
     * 批量删除订单
     * 
     * @param ids 需要删除的订单主键集合
     * @return 结果
     */
    public int deleteTfpOrderByIds(Long[] ids);

    /**
     * 删除订单信息
     * 
     * @param id 订单主键
     * @return 结果
     */
    public int deleteTfpOrderById(Long id);

    List<SupplierVo> supplySelect();

    /**
     * 创建订单详情
     * @param createOrderDetailDto
     * @return
     */
    CreateOrderDetailVo createOrderDetail(CreateOrderDetailDto createOrderDetailDto);

    /**
     * 创建订单
     * @param createOrderDto
     * @return
     */
    CreateOrderVo createOrder(CreateOrderDto createOrderDto);

    /**
     * 查询商品的订单信息集合
     * @param productId
     * @param statusList
     * @return
     */
    List<TfpOrder> selectListByProductId(Long productId, List<Integer> statusList);

    /**
     * 查询商品的订单信息集合
     * @param productIdList
     * @param statusList
     * @return
     */
    List<TfpOrder> selectListByProductIdList(List<Long> productIdList, List<Integer> statusList);

    int cancelOrder(Long orderId);

    List<MyOrderListVo> myOrderList(MyOrderListDto myOrderListDto);

    OrderDetailVo orderDetail(Long orderId);

    List<TfpOrder> selectTfpOrderByIdList(List<Long> idList);

    List<String> getRouteHighlightList(Long productId);

    int handlePaySuccessAfterInfo(String orderNo);

    PriceDetailVo priceDetail(PriceDetailDto priceDetailDto);

    AirTicketPriceVo getAirTicketPrice(Integer lowPriceAirTicketFlag, Date goDate, TfpProduct tfpProduct);

    RequestPaymentVo getRequestPayment(Long orderId);

    void handleExpireOrder();

    List<TfpOrder> selectNotDownShelivesOrderList(Long productId);

    List<OrderListVo> orderList(OrderListDto orderListDto);

    int confirmPayOrder(ConfirmPayOrderDto confirmPayOrderDto);

    List<TfpOrder> queryUserIncomeDetailOrderList(List<Long> productIdList, Integer afterSalesTimeFlag);

    List<OrderListExportExcel> orderListExport(ExportDto exportDto);

    void completeTravelTimeTask();

    List<TfpOrder> selectOverRefundTimeCompleteOrderList();

    int completeTravel(Long id, TfpOrder tfpOrder);

    OrderDetailInfoVo orderDetailInfo(Long id);

    List<TfpOrder> queryOverRefundTimeCompleteOrderList(Long shareUserId);
}
