package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.SysUserLabel;
import org.apache.ibatis.annotations.Param;

/**
 * 用户个性标签Mapper接口
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public interface SysUserLabelMapper 
{
    /**
     * 查询用户个性标签
     * 
     * @param id 用户个性标签主键
     * @return 用户个性标签
     */
    public SysUserLabel selectSysUserLabelById(Long id);

    /**
     * 查询用户个性标签列表
     * 
     * @param sysUserLabel 用户个性标签
     * @return 用户个性标签集合
     */
    public List<SysUserLabel> selectSysUserLabelList(SysUserLabel sysUserLabel);

    /**
     * 新增用户个性标签
     * 
     * @param sysUserLabel 用户个性标签
     * @return 结果
     */
    public int insertSysUserLabel(SysUserLabel sysUserLabel);

    /**
     * 修改用户个性标签
     * 
     * @param sysUserLabel 用户个性标签
     * @return 结果
     */
    public int updateSysUserLabel(SysUserLabel sysUserLabel);

    /**
     * 删除用户个性标签
     * 
     * @param id 用户个性标签主键
     * @return 结果
     */
    public int deleteSysUserLabelById(Long id);

    /**
     * 批量删除用户个性标签
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysUserLabelByIds(Long[] ids);

    void deleteSysUserLabelByUserId(@Param("userId") Long userId);
}
