package com.ruoyi.web.controller.service;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.TfpComateOrder;
import com.ruoyi.service.service.ITfpComateOrderService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 同行人和订单关联Controller
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@RestController
@RequestMapping("/service/comateOrder")
public class TfpComateOrderController extends BaseController
{
    @Autowired
    private ITfpComateOrderService tfpComateOrderService;

    /**
     * 查询同行人和订单关联列表
     */
    @PreAuthorize("@ss.hasPermi('service:comateOrder:list')")
    @GetMapping("/list")
    public TableDataInfo list(TfpComateOrder tfpComateOrder)
    {
        startPage();
        List<TfpComateOrder> list = tfpComateOrderService.selectTfpComateOrderList(tfpComateOrder);
        return getDataTable(list);
    }

    /**
     * 导出同行人和订单关联列表
     */
    @PreAuthorize("@ss.hasPermi('service:comateOrder:export')")
    @Log(title = "同行人和订单关联", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TfpComateOrder tfpComateOrder)
    {
        List<TfpComateOrder> list = tfpComateOrderService.selectTfpComateOrderList(tfpComateOrder);
        ExcelUtil<TfpComateOrder> util = new ExcelUtil<TfpComateOrder>(TfpComateOrder.class);
        util.exportExcel(response, list, "同行人和订单关联数据");
    }

    /**
     * 获取同行人和订单关联详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:comateOrder:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tfpComateOrderService.selectTfpComateOrderById(id));
    }

    /**
     * 新增同行人和订单关联
     */
    @PreAuthorize("@ss.hasPermi('service:comateOrder:add')")
    @Log(title = "同行人和订单关联", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TfpComateOrder tfpComateOrder)
    {
        return toAjax(tfpComateOrderService.insertTfpComateOrder(tfpComateOrder));
    }

    /**
     * 修改同行人和订单关联
     */
    @PreAuthorize("@ss.hasPermi('service:comateOrder:edit')")
    @Log(title = "同行人和订单关联", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TfpComateOrder tfpComateOrder)
    {
        return toAjax(tfpComateOrderService.updateTfpComateOrder(tfpComateOrder));
    }

    /**
     * 删除同行人和订单关联
     */
    @PreAuthorize("@ss.hasPermi('service:comateOrder:remove')")
    @Log(title = "同行人和订单关联", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tfpComateOrderService.deleteTfpComateOrderByIds(ids));
    }
}
