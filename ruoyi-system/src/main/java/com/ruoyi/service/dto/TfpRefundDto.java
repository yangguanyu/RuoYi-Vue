package com.ruoyi.service.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 退款对象 tfp_refund
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@Data
public class TfpRefundDto extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 商品ID */
    private Long productId;

    private String productName;
    private boolean productNameFlag;
    private List<Long> productIdList;

    /** 订单ID */
    private Long orderId;

    /** 订单编号 */
    private String orderNo;

    /** 接收退款用户 */
    private Long refundUserId;

    /** 1：全额退款   2：部分退款 */
    private Integer refundType;

    /** 退款金额 */
    private BigDecimal refundAmount;

    /** 退款时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date refundTime;

    /** 退款状态（1：退款审核中，2：审核成功-退款中，3：审核失败-退款失败，4：退款失败，5：退款成功） */
    private Integer refundStatus;

    /** 申请退款原因 */
    private String reason;

    /** 申请退款驳回的原因 */
    private String rejectReason;

    /** 退款失败原因 */
    private String failReason;

    /** 对账状态（0-未对账 1-成功 2-交易金额异常 3-交易状态异常) */
    private Integer actStatus;

    /** 对账时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date actTime;

    /** 账户信息（预留） */
    private String accountInfo;

    /** 退款信息（预留） */
    private String refundInfo;

    /** 退款单流水号 */
    private String serialNo;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    private Long createUserId;

    /** 更新者id */
    private Long updateUserId;
}
