package com.ruoyi.service.domain.flight.res.orderdetail;

import lombok.Data;

@Data
public class PassengerResp {
    // 姓名
    private String name;

    // 性别，1表示男，2表示女
    private Integer sex;

    // 生日
    private Integer birthday;

    // 证件类型，1表示身份证，2表示护照，3表示军人证等，4表示无法识别证件
    private Integer identityType;

    // 证件号码
    private String identityNo;

    // 乘机人类型，1表示成人，2表示儿童，3表示婴儿（暂不使用）
    private Integer crewType;

    // 乘客手机号
    private String phoneNum;

    // 乘客状态，1表示无退票无变更，2表示有变更有退票，3表示有变更，4表示有退票
    private Integer crewStatus;

    // 如果订单出票则展示票号
    private String ticketNo;

    // 用于存储以乘客拆单用户
    private String sonSheeNo;
}
