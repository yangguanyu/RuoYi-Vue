package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.TfpUserShareLog;
import com.ruoyi.service.vo.InvitingAchievementsVo;
import com.ruoyi.service.vo.ShareProductLogVo;
import org.apache.ibatis.annotations.Param;

/**
 * 用户分享日志Mapper接口
 * 
 * @author ruoyi
 * @date 2024-01-25
 */
public interface TfpUserShareLogMapper 
{
    /**
     * 查询用户分享日志
     * 
     * @param id 用户分享日志主键
     * @return 用户分享日志
     */
    public TfpUserShareLog selectTfpUserShareLogById(Long id);

    /**
     * 查询用户分享日志列表
     * 
     * @param tfpUserShareLog 用户分享日志
     * @return 用户分享日志集合
     */
    public List<TfpUserShareLog> selectTfpUserShareLogList(TfpUserShareLog tfpUserShareLog);

    /**
     * 新增用户分享日志
     * 
     * @param tfpUserShareLog 用户分享日志
     * @return 结果
     */
    public int insertTfpUserShareLog(TfpUserShareLog tfpUserShareLog);

    /**
     * 修改用户分享日志
     * 
     * @param tfpUserShareLog 用户分享日志
     * @return 结果
     */
    public int updateTfpUserShareLog(TfpUserShareLog tfpUserShareLog);

    /**
     * 删除用户分享日志
     * 
     * @param id 用户分享日志主键
     * @return 结果
     */
    public int deleteTfpUserShareLogById(Long id);

    /**
     * 批量删除用户分享日志
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpUserShareLogByIds(Long[] ids);

    List<ShareProductLogVo> selectShareProductLogList(@Param("userId") Long userId);

    List<InvitingAchievementsVo> invitingAchievements(@Param("userId") Long userId);
}
