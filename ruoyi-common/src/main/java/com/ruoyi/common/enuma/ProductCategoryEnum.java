package com.ruoyi.common.enuma;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 角色类型
 *
 * @author
 * @date
 * @time
 */
@Getter
@AllArgsConstructor
public enum ProductCategoryEnum implements BaseLongEnum {

    // 全国散
    WHOLE_COUNTRY(1L, "畅游"),
    // 一家一团
    ONE_FAMILY(2L, "精选线路"),
    // 自营
    SELF(3L, "官方推荐"),
    // 全国散-小包团
    WHOLE_COUNTRY_SMALL(4L, "小团畅游"),


    ;


    private Long value;
    private String desc;
}
