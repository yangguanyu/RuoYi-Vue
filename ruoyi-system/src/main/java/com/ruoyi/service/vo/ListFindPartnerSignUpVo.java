package com.ruoyi.service.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("ListFindPartnerSignUpVo")
public class ListFindPartnerSignUpVo {
    @ApiModelProperty("报名id")
    private Long id;
    @ApiModelProperty("用户id")
    private Long userId;
    @ApiModelProperty("用户昵称")
    private String nickName;
    @ApiModelProperty("头像图片")
    private String headImage;
    @ApiModelProperty("状态 0-待确认 1-已确认")
    private Integer status;

    private Long signUpUserId;
}
