package com.ruoyi.service.service;

import java.util.List;
import com.ruoyi.service.domain.flight.res.FlightSearchResp;
import com.ruoyi.service.vo.TfpFlightVo;

/**
 * 航班信息Service接口
 * 
 * @author ruoyi
 * @date 2024-04-10
 */
public interface ITfpFlightService 
{

    List<TfpFlightVo> saveFlightInfo(List<FlightSearchResp> flightSearchRespList);
}
