package com.ruoyi.service.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * banner对象 tfp_general_banner
 * 
 * @author ruoyi
 * @date 2023-11-01
 */
public class TfpGeneralBanner extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** banner标题 */
    @Excel(name = "banner标题")
    private String bannerTitle;

    /** 广告位 */
    @Excel(name = "广告位")
    private Integer position;

    /** banner图片URL */
    @Excel(name = "banner图片URL")
    private String imgUrl;

    /** C端类型：1web 2小程序 3APP */
    @Excel(name = "C端类型：1web 2小程序 3APP")
    private Integer source;

    /** 业务类型 */
    @Excel(name = "业务类型")
    private Integer busiType;

    /** 跳转URL */
    @Excel(name = "跳转URL")
    private String goUrl;

    /** banner状态1开启2关闭  */
    @Excel(name = "banner状态1开启2关闭 ")
    private Integer status;

    /** 排序码 */
    @Excel(name = "排序码")
    private Integer sortNum;

    /** 生效时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "生效时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date beginTime;

    /** 背景颜色 */
    @Excel(name = "背景颜色")
    private String bgColor;

    /** 宽 */
    @Excel(name = "宽")
    private String width;

    /** 高 */
    @Excel(name = "高")
    private String height;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    @Excel(name = "创建者id")
    private Long createUserId;

    /** 更新者id */
    @Excel(name = "更新者id")
    private Long updateUserId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setBannerTitle(String bannerTitle) 
    {
        this.bannerTitle = bannerTitle;
    }

    public String getBannerTitle() 
    {
        return bannerTitle;
    }
    public void setPosition(Integer position) 
    {
        this.position = position;
    }

    public Integer getPosition() 
    {
        return position;
    }
    public void setImgUrl(String imgUrl) 
    {
        this.imgUrl = imgUrl;
    }

    public String getImgUrl() 
    {
        return imgUrl;
    }
    public void setSource(Integer source) 
    {
        this.source = source;
    }

    public Integer getSource() 
    {
        return source;
    }
    public void setBusiType(Integer busiType) 
    {
        this.busiType = busiType;
    }

    public Integer getBusiType() 
    {
        return busiType;
    }
    public void setGoUrl(String goUrl) 
    {
        this.goUrl = goUrl;
    }

    public String getGoUrl() 
    {
        return goUrl;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setSortNum(Integer sortNum) 
    {
        this.sortNum = sortNum;
    }

    public Integer getSortNum() 
    {
        return sortNum;
    }
    public void setBeginTime(Date beginTime) 
    {
        this.beginTime = beginTime;
    }

    public Date getBeginTime() 
    {
        return beginTime;
    }
    public void setBgColor(String bgColor) 
    {
        this.bgColor = bgColor;
    }

    public String getBgColor() 
    {
        return bgColor;
    }
    public void setWidth(String width) 
    {
        this.width = width;
    }

    public String getWidth() 
    {
        return width;
    }
    public void setHeight(String height) 
    {
        this.height = height;
    }

    public String getHeight() 
    {
        return height;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }
    public void setUpdateUserId(Long updateUserId) 
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId() 
    {
        return updateUserId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("bannerTitle", getBannerTitle())
            .append("position", getPosition())
            .append("imgUrl", getImgUrl())
            .append("source", getSource())
            .append("busiType", getBusiType())
            .append("goUrl", getGoUrl())
            .append("status", getStatus())
            .append("sortNum", getSortNum())
            .append("beginTime", getBeginTime())
            .append("bgColor", getBgColor())
            .append("width", getWidth())
            .append("height", getHeight())
            .append("remark", getRemark())
            .append("delFlag", getDelFlag())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
