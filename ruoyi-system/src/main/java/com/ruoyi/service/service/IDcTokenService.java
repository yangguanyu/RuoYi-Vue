package com.ruoyi.service.service;

import com.ruoyi.common.core.domain.model.LoginUser;

public interface IDcTokenService {
    void refreshToken(LoginUser loginUser);
}
