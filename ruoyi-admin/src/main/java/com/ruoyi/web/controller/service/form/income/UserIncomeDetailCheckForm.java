package com.ruoyi.web.controller.service.form.income;

import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 用户收益对象 tfp_user_income_detail
 *
 * @author ruoyi
 * @date 2023-10-31
 */
@Data
@ApiModel(value = "UserIncomeDetailCheckForm", description = "此form保存：用户查看分享人分享的商品-做一个记录")
public class UserIncomeDetailCheckForm extends BaseEntity
{

    /** 主键 */
    @ApiModelProperty(hidden = true,value = "主键")
    private Long id;

    /** 用户id */
    @ApiModelProperty(value = "用户id,也就是分享人")
    private Long userId;

    /** 用户收益主表id */
    @ApiModelProperty(hidden = true,name = "用户收益主表id")
    private Long userIncomeId;

    /** 受邀人id */
    @ApiModelProperty(value = "受邀人用户id,也就是查看人")
    private Long inviteeUserId;

//    /** 金额 */
//    @Excel(name = "金额")
//    private BigDecimal price;
//
//    /** 状态 0- 受邀人查看 1-结算佣金/已完成 2-受邀人下单 3-受邀人付款 4-出行完成 */
//    @Excel(name = "状态 0- 受邀人查看 1-结算佣金/已完成 2-受邀人下单 3-受邀人付款 4-出行完成")
//    private Integer status;
//
//    /** 结算单号 */
//    @Excel(name = "结算单号")
//    private String orderNumber;
//
//    /** 完成时间 */
//    @JsonFormat(pattern = "yyyy-MM-dd")
//    @Excel(name = "完成时间", width = 30, dateFormat = "yyyy-MM-dd")
//    private Date completeTime;
//
//    /** 删除标志（0代表存在 2代表删除） */
//    private String delFlag;
//
//    /** 创建者id */
//    @Excel(name = "创建者id")
//    private Long createUserId;
//
//    /** 更新者id */
//    @Excel(name = "更新者id")
//    private Long updateUserId;

    /** 产品id */
    @ApiModelProperty(value = "产品id")
    private Long productId;

}
