package com.ruoyi.common.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class DcFileUploadUtils {
    public static String uploadFile(String uploadUrl, String fileName, byte[] bytes){
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.addBinaryBody("uploadfile", bytes, ContentType.DEFAULT_BINARY, fileName);
        return DcFileUploadUtils.upload(uploadUrl, builder);
    }

    public static String uploadFile(String uploadUrl, String fileName, File file){
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.addBinaryBody("uploadfile", file, ContentType.DEFAULT_BINARY, fileName);
        return DcFileUploadUtils.upload(uploadUrl, builder);
    }

    public static String uploadFile(String uploadUrl, String fileName, InputStream inputStream){
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.addBinaryBody("uploadfile", inputStream, ContentType.DEFAULT_BINARY, fileName);
        return DcFileUploadUtils.upload(uploadUrl, builder);
    }

    private static String upload(String uploadUrl, MultipartEntityBuilder builder){
        HttpPost httpPost = new HttpPost(uploadUrl);
        // 解决中文乱码
        builder.setMode(HttpMultipartMode.RFC6532);
        httpPost.setEntity(builder.build());

        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse response = null;
        String content = null;
        try {
            // 执行请求用execute方法，content用来帮我们附带上额外信息
            response = httpClient.execute(httpPost);
            // 得到相应实体、包括响应头以及相应内容
            HttpEntity resultEntity = response.getEntity();
            // 得到response的内容
            content = EntityUtils.toString(resultEntity);
            EntityUtils.consume(resultEntity);

            System.out.println(content);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (response != null) {
                try {
                    response.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return content;
    }
}
