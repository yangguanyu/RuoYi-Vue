package com.ruoyi.system.service;

import com.ruoyi.common.core.domain.entity.SysUserWx;

import java.util.List;

/**
 * 用户微信Service接口
 * 
 * @author ruoyi
 * @date 2023-10-09
 */
public interface ISysUserWxService 
{
    /**
     * 查询用户微信
     * 
     * @param id 用户微信主键
     * @return 用户微信
     */
    public SysUserWx selectSysUserWxById(Long id);

    /**
     * 查询用户微信列表
     * 
     * @param sysUserWx 用户微信
     * @return 用户微信集合
     */
    public List<SysUserWx> selectSysUserWxList(SysUserWx sysUserWx);

    /**
     * 新增用户微信
     * 
     * @param sysUserWx 用户微信
     * @return 结果
     */
    public int insertSysUserWx(SysUserWx sysUserWx);

    /**
     * 修改用户微信
     * 
     * @param sysUserWx 用户微信
     * @return 结果
     */
    public int updateSysUserWx(SysUserWx sysUserWx);

    /**
     * 批量删除用户微信
     * 
     * @param ids 需要删除的用户微信主键集合
     * @return 结果
     */
    public int deleteSysUserWxByIds(String ids);

    /**
     * 删除用户微信信息
     * 
     * @param id 用户微信主键
     * @return 结果
     */
    public int deleteSysUserWxById(Long id);

    SysUserWx findSysUserWxByUserId(Long userId);
}
