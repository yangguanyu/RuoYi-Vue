package com.ruoyi.service.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 航班信息对象 tfp_flight
 * 
 * @author ruoyi
 * @date 2024-04-10
 */
public class TfpFlight extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 航班号 MU5318 */
    @Excel(name = "航班号 MU5318")
    private String flightNo;

    /** 起飞时间 yyyy-MM-dd hh:mm:ss */
    @Excel(name = "起飞时间 yyyy-MM-dd hh:mm:ss")
    private String depTime;

    /** 起飞机场三字码 */
    @Excel(name = "起飞机场三字码")
    private String depCode;

    /** 到达机场 */
    @Excel(name = "到达机场")
    private String arrCode;

    /** 抵达时间 yyyy-MM-dd hh:mm:ss */
    @Excel(name = "抵达时间 yyyy-MM-dd hh:mm:ss")
    private String arrTime;

    /** 出发航站楼 T1 */
    @Excel(name = "出发航站楼 T1")
    private String depJetquay;

    /** 抵达航站楼 T1 */
    @Excel(name = "抵达航站楼 T1")
    private String arrJetquay;

    /** 经停次数 数字代表次数 0-直达 1-经停1次 */
    @Excel(name = "经停次数 数字代表次数 0-直达 1-经停1次")
    private String stopNum;

    /** 机型 */
    @Excel(name = "机型")
    private String planeType;

    /** 飞机大小 1-大 2-中 3-小 */
    @Excel(name = "飞机大小 1-大 2-中 3-小")
    private Integer planeSize;

    /** 中文机型描述 */
    @Excel(name = "中文机型描述")
    private String planeCnName;

    /** 飞行里程 800 */
    @Excel(name = "飞行里程 800")
    private String distance;

    /** 基础价格 */
    @Excel(name = "基础价格")
    private Integer basePrics;

    /** 餐食标识 */
    @Excel(name = "餐食标识")
    private String meal;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    @Excel(name = "创建者id")
    private Long createUserId;

    /** 更新者id */
    @Excel(name = "更新者id")
    private Long updateUserId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setFlightNo(String flightNo) 
    {
        this.flightNo = flightNo;
    }

    public String getFlightNo() 
    {
        return flightNo;
    }
    public void setDepTime(String depTime) 
    {
        this.depTime = depTime;
    }

    public String getDepTime() 
    {
        return depTime;
    }
    public void setDepCode(String depCode) 
    {
        this.depCode = depCode;
    }

    public String getDepCode() 
    {
        return depCode;
    }
    public void setArrCode(String arrCode) 
    {
        this.arrCode = arrCode;
    }

    public String getArrCode() 
    {
        return arrCode;
    }
    public void setArrTime(String arrTime) 
    {
        this.arrTime = arrTime;
    }

    public String getArrTime() 
    {
        return arrTime;
    }
    public void setDepJetquay(String depJetquay) 
    {
        this.depJetquay = depJetquay;
    }

    public String getDepJetquay() 
    {
        return depJetquay;
    }
    public void setArrJetquay(String arrJetquay) 
    {
        this.arrJetquay = arrJetquay;
    }

    public String getArrJetquay() 
    {
        return arrJetquay;
    }
    public void setStopNum(String stopNum)
    {
        this.stopNum = stopNum;
    }

    public String getStopNum()
    {
        return stopNum;
    }
    public void setPlaneType(String planeType) 
    {
        this.planeType = planeType;
    }

    public String getPlaneType() 
    {
        return planeType;
    }
    public void setPlaneSize(Integer planeSize) 
    {
        this.planeSize = planeSize;
    }

    public Integer getPlaneSize() 
    {
        return planeSize;
    }
    public void setPlaneCnName(String planeCnName) 
    {
        this.planeCnName = planeCnName;
    }

    public String getPlaneCnName() 
    {
        return planeCnName;
    }
    public void setDistance(String distance) 
    {
        this.distance = distance;
    }

    public String getDistance() 
    {
        return distance;
    }
    public void setBasePrics(Integer basePrics)
    {
        this.basePrics = basePrics;
    }

    public Integer getBasePrics()
    {
        return basePrics;
    }
    public void setMeal(String meal) 
    {
        this.meal = meal;
    }

    public String getMeal() 
    {
        return meal;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }
    public void setUpdateUserId(Long updateUserId) 
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId() 
    {
        return updateUserId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("flightNo", getFlightNo())
            .append("depTime", getDepTime())
            .append("depCode", getDepCode())
            .append("arrCode", getArrCode())
            .append("arrTime", getArrTime())
            .append("depJetquay", getDepJetquay())
            .append("arrJetquay", getArrJetquay())
            .append("stopNum", getStopNum())
            .append("planeType", getPlaneType())
            .append("planeSize", getPlaneSize())
            .append("planeCnName", getPlaneCnName())
            .append("distance", getDistance())
            .append("basePrics", getBasePrics())
            .append("meal", getMeal())
            .append("delFlag", getDelFlag())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
