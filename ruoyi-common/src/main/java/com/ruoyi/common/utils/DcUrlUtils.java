package com.ruoyi.common.utils;

import java.util.Objects;

public class DcUrlUtils {
    public static String getExtend(String url){
        String extend = "";
        if (Objects.nonNull(url) && url.contains("suffix")){
            String suffix = url.substring(url.lastIndexOf("suffix"));
            String[] suffixSplit = suffix.split("=");
            if (suffixSplit.length > 1){
                extend = "." + suffixSplit[1];
            }
        }

        return extend;
    }
}
