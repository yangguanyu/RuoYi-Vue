package com.ruoyi.web.controller.service;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.SysUserLabel;
import com.ruoyi.service.service.ISysUserLabelService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 用户个性标签Controller
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@RestController
@RequestMapping("/service/userLabel")
public class SysUserLabelController extends BaseController
{
    @Autowired
    private ISysUserLabelService sysUserLabelService;

    /**
     * 查询用户个性标签列表
     */
    @PreAuthorize("@ss.hasPermi('service:userLabel:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysUserLabel sysUserLabel)
    {
        startPage();
        List<SysUserLabel> list = sysUserLabelService.selectSysUserLabelList(sysUserLabel);
        return getDataTable(list);
    }

    /**
     * 导出用户个性标签列表
     */
    @PreAuthorize("@ss.hasPermi('service:userLabel:export')")
    @Log(title = "用户个性标签", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysUserLabel sysUserLabel)
    {
        List<SysUserLabel> list = sysUserLabelService.selectSysUserLabelList(sysUserLabel);
        ExcelUtil<SysUserLabel> util = new ExcelUtil<SysUserLabel>(SysUserLabel.class);
        util.exportExcel(response, list, "用户个性标签数据");
    }

    /**
     * 获取用户个性标签详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:userLabel:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(sysUserLabelService.selectSysUserLabelById(id));
    }

    /**
     * 新增用户个性标签
     */
    @PreAuthorize("@ss.hasPermi('service:userLabel:add')")
    @Log(title = "用户个性标签", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysUserLabel sysUserLabel)
    {
        return toAjax(sysUserLabelService.insertSysUserLabel(sysUserLabel));
    }

    /**
     * 修改用户个性标签
     */
    @PreAuthorize("@ss.hasPermi('service:userLabel:edit')")
    @Log(title = "用户个性标签", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysUserLabel sysUserLabel)
    {
        return toAjax(sysUserLabelService.updateSysUserLabel(sysUserLabel));
    }

    /**
     * 删除用户个性标签
     */
    @PreAuthorize("@ss.hasPermi('service:userLabel:remove')")
    @Log(title = "用户个性标签", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(sysUserLabelService.deleteSysUserLabelByIds(ids));
    }
}
