ALTER TABLE tfp_product
    ADD `customer_service_id` bigint(20) COMMENT '客服id' AFTER id,
    ADD `sales_status` int(1) COMMENT '产品上下架状态字段 0-新增待上架 1-已上架 2-下架待审批 3-已下架' AFTER score;

ALTER TABLE sys_user_icon
    ADD `upload_head_image_flag` int(1) COMMENT '是否本人状态审核 0-未上传 1-已上传未审核 2-已上传审核通过' AFTER head_sculpture_flag;

ALTER TABLE tfp_find_partner
    ADD `status` int(1) COMMENT '找搭子发布和撤销等状态 0-未发布 1-审核通过 2-审核未通过 3-已发布待审核 4-因疑似非法而下架' AFTER activity_place;

ALTER TABLE tfp_supplier
    ADD `supplier_bank_account_name` varchar(50) COMMENT '供应商银行账户名' AFTER phone_number,
    ADD `supplier_bank_account_number` varchar(50) COMMENT '供应商银行账号' AFTER supplier_bank_account_name,
    ADD `account_opening_bank_name` varchar(50) COMMENT '开户行' AFTER supplier_bank_account_number,
    ADD `settlement_method` int(2) COMMENT '供应商结算方式 0-先款 1-先定金出行后结清尾款 2-月付 3-季度付 4-年付' AFTER account_opening_bank_name;

-- banner表
CREATE TABLE `tfp_general_banner` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `banner_title` varchar(100) DEFAULT NULL COMMENT 'banner标题',
  `position` int(4) DEFAULT NULL COMMENT '广告位',
  `img_url` varchar(255) DEFAULT NULL COMMENT 'banner图片URL',
  `source` tinyint(4) DEFAULT NULL COMMENT 'C端类型：1web 2小程序 3APP',
  `busi_type` int(4) DEFAULT NULL COMMENT '业务类型',
  `go_url` varchar(255) DEFAULT NULL COMMENT '跳转URL',
  `status` int(4) DEFAULT '0' COMMENT 'banner状态1开启2关闭 ',
  `sort_num` int(4) DEFAULT NULL COMMENT '排序码',
  `begin_time` datetime DEFAULT NULL COMMENT '生效时间',
  `bg_color` varchar(40) DEFAULT NULL COMMENT '背景颜色',
  `width` varchar(40) DEFAULT NULL COMMENT '宽',
  `height` varchar(40) DEFAULT NULL COMMENT '高',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) COMMENT='banner表';

-- 客服表
CREATE TABLE `tfp_customer_service` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `wechat_number` varchar(50) COMMENT '客服微信号',
  `wechat_qr_code` varchar(50) COMMENT '客服微信二维码',
  `telephone` varchar(50) COMMENT '客服电话',
  `nick_name` varchar(50) COMMENT '客服昵称',
  `supplier_id` bigint(20) COMMENT '归属供应商等',
  `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
PRIMARY KEY (`id`)
) COMMENT='客服表';

-- 售后反馈表
CREATE TABLE `tfp_after_sales_feedback` (
    `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
    `user_id` bigint(20) COMMENT '反馈用户id',
    `content` varchar(500) COMMENT '反馈内容',
    `product_id` bigint(20) COMMENT '商品id',
    `order_id` bigint(20) COMMENT '订单id',
    `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
    `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
    `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
    `create_time` datetime DEFAULT NULL COMMENT '创建时间',
    `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
    `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
    `update_time` datetime DEFAULT NULL COMMENT '更新时间',
PRIMARY KEY (`id`)
) COMMENT='售后反馈表';

-- 用户肖像信息表
CREATE TABLE `tfp_user_portrait_information` (
    `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
    `user_id` bigint(20) COMMENT '反馈用户id',
    `type` int(4) COMMENT '行为类型',
    `content` varchar(500) COMMENT '行为内容',
    `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
    `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
    `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
    `create_time` datetime DEFAULT NULL COMMENT '创建时间',
    `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
    `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
    `update_time` datetime DEFAULT NULL COMMENT '更新时间',
PRIMARY KEY (`id`)
) COMMENT='用户肖像信息表';

ALTER TABLE tfp_find_partner
    ADD `activity_status` int(4) COMMENT '找搭子状态 1：找搭子进行中 2：即将发车 3:已结束';


