package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.common.core.domain.entity.TfpAttachment;
import org.apache.ibatis.annotations.Param;

/**
 * 附件Mapper接口
 *
 * @author ruoyi
 * @date 2023-12-25
 */
public interface TfpAttachmentMapper
{
    /**
     * 查询附件
     *
     * @param id 附件主键
     * @return 附件
     */
    public TfpAttachment selectTfpAttachmentById(Long id);

    /**
     * 查询附件列表
     *
     * @param tfpAttachment 附件
     * @return 附件集合
     */
    public List<TfpAttachment> selectTfpAttachmentList(TfpAttachment tfpAttachment);

    /**
     * 新增附件
     *
     * @param tfpAttachment 附件
     * @return 结果
     */
    public int insertTfpAttachment(TfpAttachment tfpAttachment);

    /**
     * 修改附件
     *
     * @param tfpAttachment 附件
     * @return 结果
     */
    public int updateTfpAttachment(TfpAttachment tfpAttachment);

    /**
     * 删除附件
     *
     * @param id 附件主键
     * @return 结果
     */
    public int deleteTfpAttachmentById(Long id);

    /**
     * 批量删除附件
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpAttachmentByIds(Long[] ids);

    List<TfpAttachment> selectListByBusinessId(@Param("businessId") Long businessId, @Param("businessType") String businessType, @Param("businessSubType") String businessSubType);

    List<TfpAttachment> selectList(@Param("businessIdList") List<Long> businessIdList, @Param("businessType") String businessType, @Param("businessSubType") String businessSubType);

    void deleteTfpAttachmentByBusinessId(@Param("businessId") Long businessId, @Param("businessType") String businessType, @Param("businessSubType") String businessSubType);

    void deleteUserImageNotHeaderImage(@Param("businessId") Long businessId);
}
