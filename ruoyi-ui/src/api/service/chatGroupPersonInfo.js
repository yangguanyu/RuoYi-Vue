import request from '@/utils/request'

// 查询聊天群组成员聊天信息列表
export function listChatGroupPersonInfo(query) {
  return request({
    url: '/service/chatGroupPersonInfo/list',
    method: 'get',
    params: query
  })
}

// 查询聊天群组成员聊天信息详细
export function getChatGroupPersonInfo(id) {
  return request({
    url: '/service/chatGroupPersonInfo/' + id,
    method: 'get'
  })
}

// 新增聊天群组成员聊天信息
export function addChatGroupPersonInfo(data) {
  return request({
    url: '/service/chatGroupPersonInfo',
    method: 'post',
    data: data
  })
}

// 修改聊天群组成员聊天信息
export function updateChatGroupPersonInfo(data) {
  return request({
    url: '/service/chatGroupPersonInfo',
    method: 'put',
    data: data
  })
}

// 删除聊天群组成员聊天信息
export function delChatGroupPersonInfo(id) {
  return request({
    url: '/service/chatGroupPersonInfo/' + id,
    method: 'delete'
  })
}

export function chatGroupPersonSelect(){
  return request({
    url: '/service/chatGroupPersonInfo/chatGroupPersonSelect',
    method: 'post'
  })
}
