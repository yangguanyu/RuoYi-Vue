package com.ruoyi.service.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(value = "AirTicketPriceVo")
public class AirTicketPriceVo {
    @ApiModelProperty("航班总价格")
    private BigDecimal totalAirTicketPrice;
    @ApiModelProperty("航班去程价格")
    private BigDecimal goAirTicketPrice;
    @ApiModelProperty("航班返程价格")
    private BigDecimal backAirTicketPrice;
}
