package com.ruoyi.service.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;

/**
 * 聊天群组对象 tfp_chat_group
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public class TfpChatGroup extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 聊天群组名称 */
    @Excel(name = "聊天群组名称")
    private String name;

    /** 商品id */
    @Excel(name = "商品id")
    private Long productId;

    /** 限制人数 */
    @Excel(name = "限制人数")
    private Integer limitPersonNumber;

    /** 报名人数 */
    @Excel(name = "报名人数")
    private Integer signUpPersonNumber;

    @Excel(name = "出发日期")
    private Date goDate;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    @Excel(name = "创建者id")
    private Long createUserId;

    /** 更新者id */
    @Excel(name = "更新者id")
    private Long updateUserId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setProductId(Long productId) 
    {
        this.productId = productId;
    }

    public Long getProductId() 
    {
        return productId;
    }
    public void setLimitPersonNumber(Integer limitPersonNumber)
    {
        this.limitPersonNumber = limitPersonNumber;
    }

    public Integer getLimitPersonNumber()
    {
        return limitPersonNumber;
    }

    public Integer getSignUpPersonNumber() {
        return signUpPersonNumber;
    }

    public void setSignUpPersonNumber(Integer signUpPersonNumber) {
        this.signUpPersonNumber = signUpPersonNumber;
    }

    public Date getGoDate() {
        return goDate;
    }

    public void setGoDate(Date goDate) {
        this.goDate = goDate;
    }

    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }
    public void setUpdateUserId(Long updateUserId) 
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId() 
    {
        return updateUserId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("productId", getProductId())
            .append("limitPersonNumber", getLimitPersonNumber())
            .append("signUpPersonNumber", getSignUpPersonNumber())
            .append("goDate", getGoDate())
            .append("delFlag", getDelFlag())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
