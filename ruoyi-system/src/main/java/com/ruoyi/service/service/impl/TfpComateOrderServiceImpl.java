package com.ruoyi.service.service.impl;

import java.util.ArrayList;
import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpComateOrderMapper;
import com.ruoyi.service.domain.TfpComateOrder;
import com.ruoyi.service.service.ITfpComateOrderService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 同行人和订单关联Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@Service
@Transactional
public class TfpComateOrderServiceImpl implements ITfpComateOrderService 
{
    @Autowired
    private TfpComateOrderMapper tfpComateOrderMapper;

    /**
     * 查询同行人和订单关联
     * 
     * @param id 同行人和订单关联主键
     * @return 同行人和订单关联
     */
    @Override
    public TfpComateOrder selectTfpComateOrderById(Long id)
    {
        return tfpComateOrderMapper.selectTfpComateOrderById(id);
    }

    /**
     * 查询同行人和订单关联列表
     * 
     * @param tfpComateOrder 同行人和订单关联
     * @return 同行人和订单关联
     */
    @Override
    public List<TfpComateOrder> selectTfpComateOrderList(TfpComateOrder tfpComateOrder)
    {
        return tfpComateOrderMapper.selectTfpComateOrderList(tfpComateOrder);
    }

    /**
     * 新增同行人和订单关联
     * 
     * @param tfpComateOrder 同行人和订单关联
     * @return 结果
     */
    @Override
    public int insertTfpComateOrder(TfpComateOrder tfpComateOrder)
    {
        tfpComateOrder.setCreateTime(DateUtils.getNowDate());
        return tfpComateOrderMapper.insertTfpComateOrder(tfpComateOrder);
    }

    /**
     * 修改同行人和订单关联
     * 
     * @param tfpComateOrder 同行人和订单关联
     * @return 结果
     */
    @Override
    public int updateTfpComateOrder(TfpComateOrder tfpComateOrder)
    {
        tfpComateOrder.setUpdateTime(DateUtils.getNowDate());
        return tfpComateOrderMapper.updateTfpComateOrder(tfpComateOrder);
    }

    /**
     * 批量删除同行人和订单关联
     * 
     * @param ids 需要删除的同行人和订单关联主键
     * @return 结果
     */
    @Override
    public int deleteTfpComateOrderByIds(Long[] ids)
    {
        return tfpComateOrderMapper.deleteTfpComateOrderByIds(ids);
    }

    /**
     * 删除同行人和订单关联信息
     * 
     * @param id 同行人和订单关联主键
     * @return 结果
     */
    @Override
    public int deleteTfpComateOrderById(Long id)
    {
        return tfpComateOrderMapper.deleteTfpComateOrderById(id);
    }

    @Override
    public List<TfpComateOrder> selectTfpComateOrderByMateIdList(List<Long> mateIdList) {
        return tfpComateOrderMapper.selectTfpComateOrderByMateIdList(mateIdList);
    }

    @Override
    public List<TfpComateOrder> selectTfpComateOrderListByOrderId(Long orderId) {
        List<Long> orderIdList = new ArrayList<>();
        orderIdList.add(orderId);
        return this.selectTfpComateOrderListByOrderIdList(orderIdList);
    }

    @Override
    public List<TfpComateOrder> selectTfpComateOrderListByOrderIdList(List<Long> orderIdList) {
        return tfpComateOrderMapper.selectTfpComateOrderListByOrderIdList(orderIdList);
    }
}
