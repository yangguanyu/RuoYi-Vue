package com.ruoyi.service.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "FindPartnerSignUpVo")
public class FindPartnerSignUpVo {
    @ApiModelProperty("找搭子id")
    private Long findPartnerId;
    @ApiModelProperty("报名人数")
    private Integer signUpPersonNumber;
}
