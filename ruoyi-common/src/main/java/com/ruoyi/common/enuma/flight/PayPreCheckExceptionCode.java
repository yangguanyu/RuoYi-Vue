package com.ruoyi.common.enuma.flight;

import com.ruoyi.common.enuma.BaseStrEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PayPreCheckExceptionCode implements BaseStrEnum {
    PRICE_CHANGE("011402", "价格变动"),
    INVALID_REQUEST("011601", "无效的请求"),
    ONLY_PAY_THIS_COMPANY_ORDER("011604", "只能支付本公司的订单"),
    POLICY_HANG_UP_CANNOT_PAY("011605", "政策挂起不能支付!"),
    NO_EXTERNAL_SEAT("011602", "无外放座位"),
    ORDER_NOT_EXIST("011603", "订单不存在");

    private String value;
    private String desc;


}
