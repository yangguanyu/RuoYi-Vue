package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.TfpFindPartnerSignUp;
import com.ruoyi.service.vo.FindPartnerSignUpVo;
import com.ruoyi.service.vo.ListFindPartnerSignUpVo;
import org.apache.ibatis.annotations.Param;

/**
 * 找搭子Mapper接口
 *
 * @author ruoyi
 * @date 2023-10-31
 */
public interface TfpFindPartnerSignUpMapper
{
    /**
     * 查询找搭子
     *
     * @param id 找搭子主键
     * @return 找搭子
     */
    public TfpFindPartnerSignUp selectTfpFindPartnerSignUpById(Long id);

    /**
     * 查询找搭子列表
     *
     * @param tfpFindPartnerSignUp 找搭子
     * @return 找搭子集合
     */
    public List<TfpFindPartnerSignUp> selectTfpFindPartnerSignUpList(TfpFindPartnerSignUp tfpFindPartnerSignUp);

    /**
     * 新增找搭子
     *
     * @param tfpFindPartnerSignUp 找搭子
     * @return 结果
     */
    public int insertTfpFindPartnerSignUp(TfpFindPartnerSignUp tfpFindPartnerSignUp);

    /**
     * 修改找搭子
     *
     * @param tfpFindPartnerSignUp 找搭子
     * @return 结果
     */
    public int updateTfpFindPartnerSignUp(TfpFindPartnerSignUp tfpFindPartnerSignUp);

    /**
     * 删除找搭子
     *
     * @param id 找搭子主键
     * @return 结果
     */
    public int deleteTfpFindPartnerSignUpById(Long id);

    /**
     * 批量删除找搭子
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpFindPartnerSignUpByIds(Long[] ids);

    List<Long> getFindPartnerSignUpUserIdList();

    List<TfpFindPartnerSignUp> selectListByFindPartnerId(@Param("findPartnerId") Long findPartnerId);

    List<TfpFindPartnerSignUp> selectListByFindPartnerIdAndAuditStatus(@Param("findPartnerId") Long findPartnerId,@Param("auditStatus") Integer auditStatus);

    List<TfpFindPartnerSignUp> selectListByFindPartnerIdListAndAuditStatus(@Param("findPartnerIdList") List<Long> findPartnerIdList, @Param("auditStatus") Integer auditStatus);

    List<FindPartnerSignUpVo> selectSignUpPersonNumberByFindPartnerIdList(@Param("findPartnerIdList") List<Long> findPartnerIdList);

    List<TfpFindPartnerSignUp> selectListBySignUpUserId(@Param("userId") Long userId);

    List<ListFindPartnerSignUpVo> listFindPartnerSignUp(@Param("findPartnerId") Long findPartnerId, @Param("auditStatus") Integer auditStatus);
}
