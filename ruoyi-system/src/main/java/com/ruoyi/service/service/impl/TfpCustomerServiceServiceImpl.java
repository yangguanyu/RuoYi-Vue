package com.ruoyi.service.service.impl;

import java.util.*;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.DcListUtils;
import com.ruoyi.service.vo.CustomerServiceInfoVo;
import com.ruoyi.service.vo.CustomerServiceVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpCustomerServiceMapper;
import com.ruoyi.service.domain.TfpCustomerService;
import com.ruoyi.service.service.ITfpCustomerServiceService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 客服Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-11-01
 */
@Service
@Transactional
public class TfpCustomerServiceServiceImpl implements ITfpCustomerServiceService 
{
    @Autowired
    private TfpCustomerServiceMapper tfpCustomerServiceMapper;

    /**
     * 查询客服
     * 
     * @param id 客服主键
     * @return 客服
     */
    @Override
    public TfpCustomerService selectTfpCustomerServiceById(Long id)
    {
        return tfpCustomerServiceMapper.selectTfpCustomerServiceById(id);
    }

    /**
     * 查询客服列表
     * 
     * @param tfpCustomerService 客服
     * @return 客服
     */
    @Override
    public List<TfpCustomerService> selectTfpCustomerServiceList(TfpCustomerService tfpCustomerService)
    {
        return tfpCustomerServiceMapper.selectTfpCustomerServiceList(tfpCustomerService);
    }

    /**
     * 新增客服
     * 
     * @param tfpCustomerService 客服
     * @return 结果
     */
    @Override
    public int insertTfpCustomerService(TfpCustomerService tfpCustomerService)
    {
        tfpCustomerService.setCreateTime(DateUtils.getNowDate());
        return tfpCustomerServiceMapper.insertTfpCustomerService(tfpCustomerService);
    }

    /**
     * 修改客服
     * 
     * @param tfpCustomerService 客服
     * @return 结果
     */
    @Override
    public int updateTfpCustomerService(TfpCustomerService tfpCustomerService)
    {
        tfpCustomerService.setUpdateTime(DateUtils.getNowDate());
        return tfpCustomerServiceMapper.updateTfpCustomerService(tfpCustomerService);
    }

    /**
     * 批量删除客服
     * 
     * @param ids 需要删除的客服主键
     * @return 结果
     */
    @Override
    public int deleteTfpCustomerServiceByIds(Long[] ids)
    {
        return tfpCustomerServiceMapper.deleteTfpCustomerServiceByIds(ids);
    }

    /**
     * 删除客服信息
     * 
     * @param id 客服主键
     * @return 结果
     */
    @Override
    public int deleteTfpCustomerServiceById(Long id)
    {
        return tfpCustomerServiceMapper.deleteTfpCustomerServiceById(id);
    }

    @Override
    public List<CustomerServiceVo> customerServiceSelect() {
        List<CustomerServiceVo> customerServiceVoList = new ArrayList<>();

        Map<Long, List<CustomerServiceInfoVo>> customerServiceVoMap = new HashMap<>();

        TfpCustomerService query = new TfpCustomerService();
        List<TfpCustomerService> tfpCustomerServiceList = tfpCustomerServiceMapper.selectTfpCustomerServiceList(query);
        if (DcListUtils.isNotEmpty(tfpCustomerServiceList)){
            for (TfpCustomerService tfpCustomerService : tfpCustomerServiceList) {
                Long supplierId = tfpCustomerService.getSupplierId();

                List<CustomerServiceInfoVo> customerServiceInfoVoList = customerServiceVoMap.get(supplierId);
                if (customerServiceInfoVoList == null){
                    customerServiceInfoVoList = new ArrayList<>();
                }

                CustomerServiceInfoVo customerServiceInfoVo = new CustomerServiceInfoVo();
                customerServiceInfoVo.setId(tfpCustomerService.getId());
                customerServiceInfoVo.setTelephone(tfpCustomerService.getTelephone());
                customerServiceInfoVoList.add(customerServiceInfoVo);

                customerServiceVoMap.put(supplierId, customerServiceInfoVoList);
            }
        }

        if (customerServiceVoMap.size() > 0){
            Set<Map.Entry<Long, List<CustomerServiceInfoVo>>> customerServiceVoEntries = customerServiceVoMap.entrySet();
            for (Map.Entry<Long, List<CustomerServiceInfoVo>> customerServiceVoEntrie : customerServiceVoEntries) {
                CustomerServiceVo customerServiceVo = new CustomerServiceVo();
                customerServiceVo.setSupplierId(customerServiceVoEntrie.getKey());
                customerServiceVo.setCustomerServiceInfoVoList(customerServiceVoEntrie.getValue());
                customerServiceVoList.add(customerServiceVo);
            }
        }

        return customerServiceVoList;
    }
}
