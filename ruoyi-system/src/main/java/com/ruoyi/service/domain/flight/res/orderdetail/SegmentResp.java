package com.ruoyi.service.domain.flight.res.orderdetail;

import lombok.Data;

@Data
public class SegmentResp {
    // 航班号
    private String flightNo;

    // 航班日期 (格式: YYYY-MM-DD HH:MM:SS)
    private String depDate;

    // 航班抵达日期 (格式: YYYY-MM-DD HH:MM:SS)
    private String arrDate;

    // 出发机场三字码，例如: SZX
    private String depCode;

    // 抵达机场三字码，例如: SZX
    private String arrCode;

    // 舱位，例如: F
    private String cabin;

    // 儿童舱位，例如: C
    private String chdCabin;
}
