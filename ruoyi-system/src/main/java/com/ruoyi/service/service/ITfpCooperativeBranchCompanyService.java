package com.ruoyi.service.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.service.domain.TfpCooperativeBranchCompany;
import com.ruoyi.service.vo.SupplierVo;

/**
 * 合作分公司Service接口
 * 
 * @author ruoyi
 * @date 2023-12-12
 */
public interface ITfpCooperativeBranchCompanyService 
{
    /**
     * 查询合作分公司
     * 
     * @param id 合作分公司主键
     * @return 合作分公司
     */
    public TfpCooperativeBranchCompany selectTfpCooperativeBranchCompanyById(Long id);

    /**
     * 查询合作分公司列表
     * 
     * @param tfpCooperativeBranchCompany 合作分公司
     * @return 合作分公司集合
     */
    public List<TfpCooperativeBranchCompany> selectTfpCooperativeBranchCompanyList(TfpCooperativeBranchCompany tfpCooperativeBranchCompany);

    /**
     * 新增合作分公司
     * 
     * @param tfpCooperativeBranchCompany 合作分公司
     * @return 结果
     */
    public int insertTfpCooperativeBranchCompany(TfpCooperativeBranchCompany tfpCooperativeBranchCompany);

    /**
     * 修改合作分公司
     * 
     * @param tfpCooperativeBranchCompany 合作分公司
     * @return 结果
     */
    public int updateTfpCooperativeBranchCompany(TfpCooperativeBranchCompany tfpCooperativeBranchCompany);

    /**
     * 批量删除合作分公司
     * 
     * @param ids 需要删除的合作分公司主键集合
     * @return 结果
     */
    public int deleteTfpCooperativeBranchCompanyByIds(Long[] ids);

    /**
     * 删除合作分公司信息
     * 
     * @param id 合作分公司主键
     * @return 结果
     */
    public int deleteTfpCooperativeBranchCompanyById(Long id);

    List<SupplierVo> cooperativeBranchCompanySelect();

    Map<Long, List<TfpCooperativeBranchCompany>> selectCooperativeBranchCompanyMapByProductIdList(List<Long> productIdList);
}
