package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel(value = "RealNameAuthenticationDto")
public class RealNameAuthenticationDto {
    @ApiModelProperty(value = "真实姓名")
    @NotNull(message = "真实姓名不能为空")
    private String realName;
    @ApiModelProperty(value = "证件号码")
    @NotNull(message = "证件号码不能为空")
    private String cardNum;
    @ApiModelProperty(value = "手机号码")
    @NotNull(message = "手机号码不能为空")
    private String phonenumber;
}
