package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.TfpFlight;

/**
 * 航班信息Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-10
 */
public interface TfpFlightMapper 
{
    /**
     * 查询航班信息
     * 
     * @param id 航班信息主键
     * @return 航班信息
     */
    public TfpFlight selectTfpFlightById(Long id);

    /**
     * 查询航班信息列表
     * 
     * @param tfpFlight 航班信息
     * @return 航班信息集合
     */
    public List<TfpFlight> selectTfpFlightList(TfpFlight tfpFlight);

    /**
     * 新增航班信息
     * 
     * @param tfpFlight 航班信息
     * @return 结果
     */
    public int insertTfpFlight(TfpFlight tfpFlight);

    /**
     * 修改航班信息
     * 
     * @param tfpFlight 航班信息
     * @return 结果
     */
    public int updateTfpFlight(TfpFlight tfpFlight);

    /**
     * 删除航班信息
     * 
     * @param id 航班信息主键
     * @return 结果
     */
    public int deleteTfpFlightById(Long id);

    /**
     * 批量删除航班信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpFlightByIds(Long[] ids);
}
