package com.ruoyi.common.enuma;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum WantToGoTypeEnum implements BaseEnum {

    CANCEL(0, "取消"),
    WANT_TO_GO(1, "想去")
    ;

    private Integer value;
    private String desc;
}
