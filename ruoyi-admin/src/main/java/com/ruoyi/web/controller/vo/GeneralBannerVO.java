package com.ruoyi.web.controller.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * banner对象 tfp_general_banner
 *
 * @author ruoyi
 * @date 2023-11-01
 */
@ApiModel("GeneralBannerVO")
@Data
public class GeneralBannerVO
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty(value = "主键 ")
    private Long id;
    /** banner标题 */
    @ApiModelProperty(value = "banner标题 ")
    private String bannerTitle;



    /** C端类型：1web 2小程序 3APP */
    @ApiModelProperty(value = "C端类型：1web 2小程序 3APP ")
    private Integer source;

    /** 业务类型 */
    @ApiModelProperty(value = "业务类型")
    private Integer busiType;

    /** 跳转URL */
    @ApiModelProperty(value = "跳转URL")
    private String goUrl;

    /** banner状态1开启2关闭  */
    @ApiModelProperty(value = "banner状态1开启2关闭 ")
    private Integer status;

    /** 排序码 */
    @ApiModelProperty(value = "排序码")
    private Integer sortNum;

    /** 生效时间 */
//    @JsonFormat(pattern = "yyyy-MM-dd")
//    @ApiModelProperty(value = "生效时间")
//    private Date beginTime;

    /** 背景颜色 */
    @ApiModelProperty(value = "背景颜色")
    private String bgColor;

    /** 广告位 */
    @ApiModelProperty(value = "广告位")
    private Integer position;

    /** banner图片URL */
    @ApiModelProperty(value = "banner图片URL")
    private String imgUrl;



    /** 生效时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "生效时间")
    private Date beginTime;



    /** 宽 */
    @ApiModelProperty(value = "宽")
    private String width;

    /** 高 */
    @ApiModelProperty(value = "高")
    private String height;



}
