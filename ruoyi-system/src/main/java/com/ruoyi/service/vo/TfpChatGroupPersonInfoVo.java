package com.ruoyi.service.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ruoyi.common.enuma.ChatUserTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 聊天群组成员聊天信息对象 tfp_chat_group_person_info
 *
 * @author ruoyi
 * @date 2023-10-31
 */
@Data
@ApiModel
public class TfpChatGroupPersonInfoVo {
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty("id")
    private Long id;

    /** 聊天群组id */
    @ApiModelProperty("chatGroupId")
    private Long chatGroupId;

    @ApiModelProperty("聊天群组名称")
    private String chatGroupName;

    /** 聊天群组成员id */
    @JsonIgnore
    private Long chatGroupPersonId;
    @JsonIgnore
    private String chatGroupPersonName;

    /** 聊天信息 */
    @ApiModelProperty("聊天信息")
    private String chatInfo;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    @ApiModelProperty("用户id")
    private Long createUserId;

    /** 更新者id */
    private Long updateUserId;

    @ApiModelProperty("创建人姓名或昵称")
    private String createBy;

    @ApiModelProperty("是否是自己")
    private Boolean selfFlag = Boolean.FALSE;

    @ApiModelProperty("0：发起者；1：已报名；2：未报名")
    private Integer identity = ChatUserTypeEnum.UN_APPLIED.getValue();

    @ApiModelProperty("用户头像")
    private String avatar;

    private Date createTime;
    private String updateBy;
    private Date updateTime;

    @ApiModelProperty("用户信息-含头像")
    private UserVo userVo;
}
