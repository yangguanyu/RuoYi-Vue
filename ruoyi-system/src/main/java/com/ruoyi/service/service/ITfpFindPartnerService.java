package com.ruoyi.service.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.service.domain.TfpFindPartner;
import com.ruoyi.service.domain.TfpFindPartnerSignUp;
import com.ruoyi.service.dto.AuditDto;
import com.ruoyi.service.dto.TfpFindPartnerDto;
import com.ruoyi.service.vo.FindPartnerDetailAppletVo;
import com.ruoyi.service.vo.FindPartnerDetailVo;
import com.ruoyi.service.vo.InitiateFindPartnerVo;
import com.ruoyi.service.vo.MyJoinFindPartnerVo;

/**
 * 找搭子Service接口
 *
 * @author ruoyi
 * @date 2023-11-01
 */
public interface ITfpFindPartnerService
{
    /**
     * 查询找搭子
     *
     * @param id 找搭子主键
     * @return 找搭子
     */
    public TfpFindPartner selectTfpFindPartnerById(Long id);

    /**
     * 查询找搭子列表
     *
     * @param tfpFindPartner 找搭子
     * @return 找搭子集合
     */
    public List<TfpFindPartner> selectTfpFindPartnerList(TfpFindPartnerDto tfpFindPartner);

    public List<TfpFindPartner> selectManageFindPartnerList(TfpFindPartnerDto tfpFindPartner);

    /**
     * 查询找搭子列表-进行中
     *
     * @param tfpFindPartner 找搭子
     * @return 找搭子集合
     */
    List<TfpFindPartner> selectProgressList(TfpFindPartnerDto tfpFindPartner);

    /**
     * 新增找搭子
     *
     * @param tfpFindPartner 找搭子
     * @return 结果
     */
    public int insertTfpFindPartner(TfpFindPartner tfpFindPartner);


    int appletInsert(TfpFindPartner tfpFindPartner);

    int appletUpdate(TfpFindPartner tfpFindPartner);
    /**
     * 修改找搭子
     *
     * @param tfpFindPartner 找搭子
     * @return 结果
     */
    public int updateTfpFindPartner(TfpFindPartner tfpFindPartner);

    /*/**
     * @Author yangguanyu
     * @Description :后台-审核
     * @Date 21:12 2024/1/14
     * @Param [com.ruoyi.service.domain.TfpFindPartner]
     * @return int
     **/
    int approve(TfpFindPartner tfpFindPartner);

    /**
     * 批量删除找搭子
     *
     * @param ids 需要删除的找搭子主键集合
     * @return 结果
     */
    public int deleteTfpFindPartnerByIds(Long[] ids);

    /**
     * 删除找搭子信息
     *
     * @param id 找搭子主键
     * @return 结果
     */
    public int deleteTfpFindPartnerById(Long id);

    List<TfpFindPartner> selectTfpFindPartnerListByIdList(List<Long> idList);

    FindPartnerDetailVo detail(Long id);

    /*/**
     * @Author yangguanyu
     * @Description :小程序-找搭子详情
     * @Date 22:13 2023/12/30
     * @Param [java.lang.Long]
     * @return com.ruoyi.service.vo.FindPartnerDetailVo
     **/
    FindPartnerDetailAppletVo appletDetail(Long id);

    /**
     * 小程序-我发起的搭子
     * @return
     */
    List<InitiateFindPartnerVo> initiateFindPartnerList(Long userId);

    List<MyJoinFindPartnerVo> myJoinFindPartnerList();

    int auditFindPartner(AuditDto auditDto);

    Map<Long, List<TfpFindPartnerSignUp>> getSignUpAuditPassListByIdList(List<Long> idList);

    List<TfpFindPartner> selectOverAuditTimeFindPartnerList();
}
