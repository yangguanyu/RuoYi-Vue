package com.ruoyi.service.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enuma.DeleteEnum;
import com.ruoyi.common.enuma.YesOrNoFlagEnum;
import com.ruoyi.common.exception.BusinessException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.DcDateUtils;
import com.ruoyi.common.utils.DcListUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.service.domain.TfpEveryDayPrice;
import com.ruoyi.service.dto.DepartureDateDto;
import com.ruoyi.service.vo.JudgeDateTravelFlagVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpProductDepartureDateMapper;
import com.ruoyi.service.domain.TfpProductDepartureDate;
import com.ruoyi.service.service.ITfpProductDepartureDateService;

/**
 * 商品班期Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-17
 */
@Service
public class TfpProductDepartureDateServiceImpl implements ITfpProductDepartureDateService 
{
    @Autowired
    private TfpProductDepartureDateMapper tfpProductDepartureDateMapper;

    /**
     * 查询商品班期
     * 
     * @param id 商品班期主键
     * @return 商品班期
     */
    @Override
    public TfpProductDepartureDate selectTfpProductDepartureDateById(Long id)
    {
        return tfpProductDepartureDateMapper.selectTfpProductDepartureDateById(id);
    }

    /**
     * 查询商品班期列表
     * 
     * @param tfpProductDepartureDate 商品班期
     * @return 商品班期
     */
    @Override
    public List<TfpProductDepartureDate> selectTfpProductDepartureDateList(TfpProductDepartureDate tfpProductDepartureDate)
    {
        return tfpProductDepartureDateMapper.selectTfpProductDepartureDateList(tfpProductDepartureDate);
    }

    /**
     * 新增商品班期
     * 
     * @param tfpProductDepartureDate 商品班期
     * @return 结果
     */
    @Override
    public int insertTfpProductDepartureDate(TfpProductDepartureDate tfpProductDepartureDate)
    {
        tfpProductDepartureDate.setCreateTime(DateUtils.getNowDate());
        return tfpProductDepartureDateMapper.insertTfpProductDepartureDate(tfpProductDepartureDate);
    }

    /**
     * 修改商品班期
     * 
     * @param tfpProductDepartureDate 商品班期
     * @return 结果
     */
    @Override
    public int updateTfpProductDepartureDate(TfpProductDepartureDate tfpProductDepartureDate)
    {
        tfpProductDepartureDate.setUpdateTime(DateUtils.getNowDate());
        return tfpProductDepartureDateMapper.updateTfpProductDepartureDate(tfpProductDepartureDate);
    }

    /**
     * 批量删除商品班期
     * 
     * @param ids 需要删除的商品班期主键
     * @return 结果
     */
    @Override
    public int deleteTfpProductDepartureDateByIds(Long[] ids)
    {
        return tfpProductDepartureDateMapper.deleteTfpProductDepartureDateByIds(ids);
    }

    /**
     * 删除商品班期信息
     * 
     * @param id 商品班期主键
     * @return 结果
     */
    @Override
    public int deleteTfpProductDepartureDateById(Long id)
    {
        return tfpProductDepartureDateMapper.deleteTfpProductDepartureDateById(id);
    }

    @Override
    public void deleteByProductId(Long productId) {
        tfpProductDepartureDateMapper.deleteByProductId(productId);
    }

    @Override
    public void deleteTfpProductDepartureDateByProductIds(List<Long> productIdList) {
        tfpProductDepartureDateMapper.deleteTfpProductDepartureDateByProductIds(productIdList);
    }

    @Override
    public void saveDepartureDateList(Long productId, List<DepartureDateDto> departureDateList) {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        for (DepartureDateDto departureDateDto : departureDateList) {
            TfpProductDepartureDate tfpProductDepartureDate = new TfpProductDepartureDate();
            tfpProductDepartureDate.setProductId(productId);

            BeanUtils.copyProperties(departureDateDto, tfpProductDepartureDate);

            tfpProductDepartureDate.setDelFlag(DeleteEnum.EXIST.getValue());
            tfpProductDepartureDate.setCreateUserId(loginUser.getUserId());
            tfpProductDepartureDate.setCreateBy(loginUser.getUsername());
            tfpProductDepartureDate.setCreateTime(DateUtils.getNowDate());
            tfpProductDepartureDateMapper.insertTfpProductDepartureDate(tfpProductDepartureDate);
        }
    }

    @Override
    public List<TfpProductDepartureDate> selectTfpProductDepartureDateByProductIdList(List<Long> productIdList) {
        return tfpProductDepartureDateMapper.selectTfpProductDepartureDateByProductIdList(productIdList);
    }

    /**
     * 获取该日期的成人零售价
     * @param productId
     * @param date
     * @return
     */
    @Override
    public BigDecimal getAdultRetailPriceByProductIdAndDate(Long productId, Date date){
        if (Objects.isNull(date)){
            return BigDecimal.ZERO;
        }

        List<TfpProductDepartureDate> tfpProductDepartureDateList = tfpProductDepartureDateMapper.selectTfpProductDepartureDateByProductId(productId);
        if (DcListUtils.isEmpty(tfpProductDepartureDateList)){
            throw new BusinessException("该产品没有设定团期信息");
        }

        JudgeDateTravelFlagVo judgeDateTravelFlagVo = this.judgeDateTravelFlag(date, tfpProductDepartureDateList);
        return judgeDateTravelFlagVo.getAdultRetailPrice();
    }

    /**
     * 判断是否可以出行
     * @param judgeDate
     * @param tfpProductDepartureDateList
     * @return
     */
    @Override
    public JudgeDateTravelFlagVo judgeDateTravelFlag(Date judgeDate, List<TfpProductDepartureDate> tfpProductDepartureDateList){
        // 是否可以出行 0-否 1-是
        Integer travelFlag = YesOrNoFlagEnum.NO.getValue();
        BigDecimal adultRetailPrice = null;
        for (TfpProductDepartureDate tfpProductDepartureDate : tfpProductDepartureDateList) {
            Date departureDateStart = DcDateUtils.handelDateToDayStart(tfpProductDepartureDate.getDepartureDateStart());
            Date departureDateEnd = DcDateUtils.handelDateToDayEnd(tfpProductDepartureDate.getDepartureDateEnd());

            boolean startEqualsFlag = judgeDate.equals(departureDateStart);
            boolean endEqualsFlag = judgeDate.equals(departureDateEnd);
            boolean betweenFlag = judgeDate.after(departureDateStart) && judgeDate.before(departureDateEnd);
            if (startEqualsFlag || endEqualsFlag || betweenFlag){
                travelFlag = YesOrNoFlagEnum.YES.getValue();
                adultRetailPrice = tfpProductDepartureDate.getAdultRetailPrice();
                break;
            }
        }

        JudgeDateTravelFlagVo judgeDateTravelFlagVo = new JudgeDateTravelFlagVo();
        judgeDateTravelFlagVo.setTravelFlag(travelFlag);
        judgeDateTravelFlagVo.setAdultRetailPrice(adultRetailPrice);
        return judgeDateTravelFlagVo;
    }
}
