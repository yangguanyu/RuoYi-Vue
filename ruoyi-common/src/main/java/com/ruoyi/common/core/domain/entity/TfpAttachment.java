package com.ruoyi.common.core.domain.entity;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 附件对象 tfp_attachment
 *
 * @author ruoyi
 * @date 2023-12-25
 */
public class TfpAttachment extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 业务id */
    @Excel(name = "业务id")
    private Long businessId;

    /** 业务类型 */
    @Excel(name = "业务类型")
    private String businessType;

    /** 业务子类型 */
    @Excel(name = "业务子类型")
    private String businessSubType;

    /** 附件名称 */
    @Excel(name = "附件名称")
    private String name;

    /** 附件Url */
    @Excel(name = "附件Url")
    private String url;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    @Excel(name = "创建者id")
    private Long createUserId;

    /** 更新者id */
    @Excel(name = "更新者id")
    private Long updateUserId;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setBusinessId(Long businessId)
    {
        this.businessId = businessId;
    }

    public Long getBusinessId()
    {
        return businessId;
    }
    public void setBusinessType(String businessType)
    {
        this.businessType = businessType;
    }

    public String getBusinessType()
    {
        return businessType;
    }
    public void setBusinessSubType(String businessSubType)
    {
        this.businessSubType = businessSubType;
    }

    public String getBusinessSubType()
    {
        return businessSubType;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getUrl()
    {
        return url;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }
    public void setCreateUserId(Long createUserId)
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId()
    {
        return createUserId;
    }
    public void setUpdateUserId(Long updateUserId)
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId()
    {
        return updateUserId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("businessId", getBusinessId())
            .append("businessType", getBusinessType())
            .append("businessSubType", getBusinessSubType())
            .append("name", getName())
            .append("url", getUrl())
            .append("delFlag", getDelFlag())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
