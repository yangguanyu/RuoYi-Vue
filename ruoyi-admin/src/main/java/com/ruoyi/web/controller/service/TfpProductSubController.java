package com.ruoyi.web.controller.service;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.TfpProductSub;
import com.ruoyi.service.service.ITfpProductSubService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商品辅Controller
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@RestController
@RequestMapping("/service/productSub")
public class TfpProductSubController extends BaseController
{
    @Autowired
    private ITfpProductSubService tfpProductSubService;

    /**
     * 查询商品辅列表
     */
    @PreAuthorize("@ss.hasPermi('service:productSub:list')")
    @GetMapping("/list")
    public TableDataInfo list(TfpProductSub tfpProductSub)
    {
        startPage();
        List<TfpProductSub> list = tfpProductSubService.selectTfpProductSubList(tfpProductSub);
        return getDataTable(list);
    }

    /**
     * 导出商品辅列表
     */
    @PreAuthorize("@ss.hasPermi('service:productSub:export')")
    @Log(title = "商品辅", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TfpProductSub tfpProductSub)
    {
        List<TfpProductSub> list = tfpProductSubService.selectTfpProductSubList(tfpProductSub);
        ExcelUtil<TfpProductSub> util = new ExcelUtil<TfpProductSub>(TfpProductSub.class);
        util.exportExcel(response, list, "商品辅数据");
    }

    /**
     * 获取商品辅详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:productSub:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tfpProductSubService.selectTfpProductSubById(id));
    }

    /**
     * 新增商品辅
     */
    @PreAuthorize("@ss.hasPermi('service:productSub:add')")
    @Log(title = "商品辅", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TfpProductSub tfpProductSub)
    {
        return toAjax(tfpProductSubService.insertTfpProductSub(tfpProductSub));
    }

    /**
     * 修改商品辅
     */
    @PreAuthorize("@ss.hasPermi('service:productSub:edit')")
    @Log(title = "商品辅", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TfpProductSub tfpProductSub)
    {
        return toAjax(tfpProductSubService.updateTfpProductSub(tfpProductSub));
    }

    /**
     * 删除商品辅
     */
    @PreAuthorize("@ss.hasPermi('service:productSub:remove')")
    @Log(title = "商品辅", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tfpProductSubService.deleteTfpProductSubByIds(ids));
    }
}
