package com.ruoyi.service.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "DateMonthVO", description = "日期价格-最外层")
public class DateMonthVO {
    @ApiModelProperty("月份")
    private String month;
    @ApiModelProperty("价格")
    private List<DateCalendarVo> list;
}
