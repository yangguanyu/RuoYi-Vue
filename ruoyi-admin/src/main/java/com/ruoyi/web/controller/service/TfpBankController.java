package com.ruoyi.web.controller.service;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.TfpBank;
import com.ruoyi.service.service.ITfpBankService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 银行Controller
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@RestController
@RequestMapping("/service/bank")
public class TfpBankController extends BaseController
{
    @Autowired
    private ITfpBankService tfpBankService;

    /**
     * 查询银行列表
     */
    @PreAuthorize("@ss.hasPermi('service:bank:list')")
    @GetMapping("/list")
    public TableDataInfo list(TfpBank tfpBank)
    {
        startPage();
        List<TfpBank> list = tfpBankService.selectTfpBankList(tfpBank);
        return getDataTable(list);
    }

    /**
     * 导出银行列表
     */
    @PreAuthorize("@ss.hasPermi('service:bank:export')")
    @Log(title = "银行", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TfpBank tfpBank)
    {
        List<TfpBank> list = tfpBankService.selectTfpBankList(tfpBank);
        ExcelUtil<TfpBank> util = new ExcelUtil<TfpBank>(TfpBank.class);
        util.exportExcel(response, list, "银行数据");
    }

    /**
     * 获取银行详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:bank:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tfpBankService.selectTfpBankById(id));
    }

    /**
     * 新增银行
     */
    @PreAuthorize("@ss.hasPermi('service:bank:add')")
    @Log(title = "银行", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TfpBank tfpBank)
    {
        return toAjax(tfpBankService.insertTfpBank(tfpBank));
    }

    /**
     * 修改银行
     */
    @PreAuthorize("@ss.hasPermi('service:bank:edit')")
    @Log(title = "银行", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TfpBank tfpBank)
    {
        return toAjax(tfpBankService.updateTfpBank(tfpBank));
    }

    /**
     * 删除银行
     */
    @PreAuthorize("@ss.hasPermi('service:bank:remove')")
    @Log(title = "银行", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tfpBankService.deleteTfpBankByIds(ids));
    }
}
