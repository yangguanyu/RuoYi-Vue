package com.ruoyi.service.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 通程舱位对应信息对象 tfp_flight_seat_detail
 * 
 * @author ruoyi
 * @date 2024-04-10
 */
public class TfpFlightSeatDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 航班信息表id */
    @Excel(name = "航班信息表id")
    private Long flightId;

    /** 舱位信息表id */
    @Excel(name = "舱位信息表id")
    private Long seatId;

    /** 出发三字码 */
    @Excel(name = "出发三字码")
    private String depCode;

    /** 抵达三字码 */
    @Excel(name = "抵达三字码")
    private String arrCode;

    /** 座位编码 */
    @Excel(name = "座位编码")
    private String seatCode;

    /** 座位状态 */
    @Excel(name = "座位状态")
    private String seatClassStatus;

    /** 票价 */
    @Excel(name = "票价")
    private Double ticketPrice;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    @Excel(name = "创建者id")
    private Long createUserId;

    /** 更新者id */
    @Excel(name = "更新者id")
    private Long updateUserId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setFlightId(Long flightId) 
    {
        this.flightId = flightId;
    }

    public Long getFlightId() 
    {
        return flightId;
    }
    public void setSeatId(Long seatId) 
    {
        this.seatId = seatId;
    }

    public Long getSeatId() 
    {
        return seatId;
    }
    public void setDepCode(String depCode) 
    {
        this.depCode = depCode;
    }

    public String getDepCode() 
    {
        return depCode;
    }
    public void setArrCode(String arrCode) 
    {
        this.arrCode = arrCode;
    }

    public String getArrCode() 
    {
        return arrCode;
    }
    public void setSeatCode(String seatCode) 
    {
        this.seatCode = seatCode;
    }

    public String getSeatCode() 
    {
        return seatCode;
    }
    public void setSeatClassStatus(String seatClassStatus) 
    {
        this.seatClassStatus = seatClassStatus;
    }

    public String getSeatClassStatus() 
    {
        return seatClassStatus;
    }
    public void setTicketPrice(Double ticketPrice)
    {
        this.ticketPrice = ticketPrice;
    }

    public Double getTicketPrice()
    {
        return ticketPrice;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }
    public void setUpdateUserId(Long updateUserId) 
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId() 
    {
        return updateUserId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("flightId", getFlightId())
            .append("seatId", getSeatId())
            .append("depCode", getDepCode())
            .append("arrCode", getArrCode())
            .append("seatCode", getSeatCode())
            .append("seatClassStatus", getSeatClassStatus())
            .append("ticketPrice", getTicketPrice())
            .append("delFlag", getDelFlag())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
