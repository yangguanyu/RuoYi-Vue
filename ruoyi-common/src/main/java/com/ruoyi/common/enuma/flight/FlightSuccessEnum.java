package com.ruoyi.common.enuma.flight;

import com.ruoyi.common.enuma.BaseStrEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum FlightSuccessEnum implements BaseStrEnum {
    SUCCESS("000000", "成功"),
    ;

    private final String value;
    private final String desc;


}
