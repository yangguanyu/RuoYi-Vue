package com.ruoyi.service.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel("UserInfoDetailVo")
public class UserInfoDetailVo {
    @ApiModelProperty("用户id")
    private Long userId;
    @ApiModelProperty("用户昵称")
    private String nickName;
    @ApiModelProperty("实名认证状态 0-未实名认证 1-已实名认证")
    private Integer realNameAuthStatus;
    @ApiModelProperty("用户性别（0男 1女 2未知）")
    private String sex;
    @ApiModelProperty("个人简介")
    private String personalProfile;
    @ApiModelProperty("用户标签集合")
    private List<String> userLabelNameList;
    @ApiModelProperty("关注个数")
    private Integer careNumber;
    @ApiModelProperty("粉丝个数")
    private Integer fansNumber;
    @ApiModelProperty("今日访客个数")
    private Integer todayVisitorNumber;
    @ApiModelProperty("用户图片id集合")
    private List<String> userImageList;
    @ApiModelProperty("是否关注 0-否 1-是")
    private Integer userCareFlag;
}
