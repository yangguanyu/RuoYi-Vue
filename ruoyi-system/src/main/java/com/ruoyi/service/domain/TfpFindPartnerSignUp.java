package com.ruoyi.service.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 找搭子对象 tfp_find_partner_sign_up
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public class TfpFindPartnerSignUp extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 找搭子id */
    @Excel(name = "找搭子id")
    private Long findPartnerId;

    /** 报名用户id */
    @Excel(name = "报名用户id")
    private Long signUpUserId;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String phoneNumber;

    /** 性别 0-男生 1-女生 */
    @Excel(name = "性别 0-男生 1-女生")
    private Integer sex;

    /** 自我介绍 */
    @Excel(name = "自我介绍")
    private String selfIntroductio;

    @Excel(name = "审核状态 0-待审核 1-审核通过 2-审核未通过")
    private Integer auditStatus;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    @Excel(name = "创建者id")
    private Long createUserId;

    /** 更新者id */
    @Excel(name = "更新者id")
    private Long updateUserId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setFindPartnerId(Long findPartnerId) 
    {
        this.findPartnerId = findPartnerId;
    }

    public Long getFindPartnerId() 
    {
        return findPartnerId;
    }
    public void setSignUpUserId(Long signUpUserId) 
    {
        this.signUpUserId = signUpUserId;
    }

    public Long getSignUpUserId() 
    {
        return signUpUserId;
    }
    public void setPhoneNumber(String phoneNumber) 
    {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber() 
    {
        return phoneNumber;
    }
    public void setSex(Integer sex) 
    {
        this.sex = sex;
    }

    public Integer getSex() 
    {
        return sex;
    }
    public void setSelfIntroductio(String selfIntroductio) 
    {
        this.selfIntroductio = selfIntroductio;
    }

    public String getSelfIntroductio() 
    {
        return selfIntroductio;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }
    public void setUpdateUserId(Long updateUserId) 
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId() 
    {
        return updateUserId;
    }

    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("findPartnerId", getFindPartnerId())
            .append("signUpUserId", getSignUpUserId())
            .append("phoneNumber", getPhoneNumber())
            .append("sex", getSex())
            .append("selfIntroductio", getSelfIntroductio())
            .append("auditStatus", getAuditStatus())
            .append("delFlag", getDelFlag())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
