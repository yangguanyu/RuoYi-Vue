package com.ruoyi.service.vo;

import lombok.Data;

@Data
public class WxGetPhoneNumberVo {
    private Integer errcode;
    private String errmsg;
    private WxPhoneInfoVo phone_info;
}
