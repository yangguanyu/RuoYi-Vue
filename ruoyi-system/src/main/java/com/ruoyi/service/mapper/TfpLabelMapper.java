package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.TfpLabel;
import org.apache.ibatis.annotations.Param;

/**
 * 标签Mapper接口
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public interface TfpLabelMapper 
{
    /**
     * 查询标签
     * 
     * @param id 标签主键
     * @return 标签
     */
    public TfpLabel selectTfpLabelById(Long id);

    /**
     * 查询标签列表
     * 
     * @param tfpLabel 标签
     * @return 标签集合
     */
    public List<TfpLabel> selectTfpLabelList(TfpLabel tfpLabel);

    /**
     * 新增标签
     * 
     * @param tfpLabel 标签
     * @return 结果
     */
    public int insertTfpLabel(TfpLabel tfpLabel);

    /**
     * 修改标签
     * 
     * @param tfpLabel 标签
     * @return 结果
     */
    public int updateTfpLabel(TfpLabel tfpLabel);

    /**
     * 删除标签
     * 
     * @param id 标签主键
     * @return 结果
     */
    public int deleteTfpLabelById(Long id);

    /**
     * 批量删除标签
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpLabelByIds(Long[] ids);

    /**
     * 根据type查询label列表
     * @param labelType
     * @return
     */
    List<TfpLabel> getLabelInfoByType(@Param("labelType") Long labelType);
}
