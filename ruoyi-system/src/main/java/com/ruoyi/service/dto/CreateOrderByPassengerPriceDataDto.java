package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("CreateOrderByPassengerPriceDataDto")
public class CreateOrderByPassengerPriceDataDto {
    @ApiModelProperty("乘机人类型，1成人，2儿童，3婴儿（暂不使用）")
    private String crewType;
    @ApiModelProperty("票面价")
    private Integer price;
    @ApiModelProperty("机建")
    private Integer airportTax;
    @ApiModelProperty("燃油费")
    private Integer fuelTax;
    @ApiModelProperty("返点 0.1 = 0.1%")
    private Double commissionPoint;
    @ApiModelProperty("定额")
    private Double commissionMoney;
    @ApiModelProperty("支付手续费")
    private Double payFee;
}
