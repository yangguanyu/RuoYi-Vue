package com.ruoyi.service.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 客服对象 tfp_customer_service
 * 
 * @author ruoyi
 * @date 2023-11-01
 */
public class TfpCustomerService extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 客服微信号 */
    @Excel(name = "客服微信号")
    private String wechatNumber;

    /** 客服微信二维码 */
    @Excel(name = "客服微信二维码")
    private String wechatQrCode;

    /** 客服电话 */
    @Excel(name = "客服电话")
    private String telephone;

    /** 客服昵称 */
    @Excel(name = "客服昵称")
    private String nickName;

    /** 归属供应商等 */
    @Excel(name = "归属供应商等")
    private Long supplierId;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    @Excel(name = "创建者id")
    private Long createUserId;

    /** 更新者id */
    @Excel(name = "更新者id")
    private Long updateUserId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setWechatNumber(String wechatNumber) 
    {
        this.wechatNumber = wechatNumber;
    }

    public String getWechatNumber() 
    {
        return wechatNumber;
    }
    public void setWechatQrCode(String wechatQrCode) 
    {
        this.wechatQrCode = wechatQrCode;
    }

    public String getWechatQrCode() 
    {
        return wechatQrCode;
    }
    public void setTelephone(String telephone) 
    {
        this.telephone = telephone;
    }

    public String getTelephone() 
    {
        return telephone;
    }
    public void setNickName(String nickName) 
    {
        this.nickName = nickName;
    }

    public String getNickName() 
    {
        return nickName;
    }
    public void setSupplierId(Long supplierId) 
    {
        this.supplierId = supplierId;
    }

    public Long getSupplierId() 
    {
        return supplierId;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }
    public void setUpdateUserId(Long updateUserId) 
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId() 
    {
        return updateUserId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("wechatNumber", getWechatNumber())
            .append("wechatQrCode", getWechatQrCode())
            .append("telephone", getTelephone())
            .append("nickName", getNickName())
            .append("supplierId", getSupplierId())
            .append("delFlag", getDelFlag())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
