package com.ruoyi.service.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.service.vo.SupplierVo;
import com.ruoyi.service.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpSupplierMapper;
import com.ruoyi.service.domain.TfpSupplier;
import com.ruoyi.service.service.ITfpSupplierService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 供应商信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-11-01
 */
@Service
@Transactional
public class TfpSupplierServiceImpl implements ITfpSupplierService 
{
    @Autowired
    private TfpSupplierMapper tfpSupplierMapper;

    /**
     * 查询供应商信息
     * 
     * @param id 供应商信息主键
     * @return 供应商信息
     */
    @Override
    public TfpSupplier selectTfpSupplierById(Long id)
    {
        return tfpSupplierMapper.selectTfpSupplierById(id);
    }

    /**
     * 查询供应商信息列表
     * 
     * @param tfpSupplier 供应商信息
     * @return 供应商信息
     */
    @Override
    public List<TfpSupplier> selectTfpSupplierList(TfpSupplier tfpSupplier)
    {
        return tfpSupplierMapper.selectTfpSupplierList(tfpSupplier);
    }

    /**
     * 新增供应商信息
     * 
     * @param tfpSupplier 供应商信息
     * @return 结果
     */
    @Override
    public int insertTfpSupplier(TfpSupplier tfpSupplier)
    {
        tfpSupplier.setCreateTime(DateUtils.getNowDate());
        return tfpSupplierMapper.insertTfpSupplier(tfpSupplier);
    }

    /**
     * 修改供应商信息
     * 
     * @param tfpSupplier 供应商信息
     * @return 结果
     */
    @Override
    public int updateTfpSupplier(TfpSupplier tfpSupplier)
    {
        tfpSupplier.setUpdateTime(DateUtils.getNowDate());
        return tfpSupplierMapper.updateTfpSupplier(tfpSupplier);
    }

    /**
     * 批量删除供应商信息
     * 
     * @param ids 需要删除的供应商信息主键
     * @return 结果
     */
    @Override
    public int deleteTfpSupplierByIds(Long[] ids)
    {
        return tfpSupplierMapper.deleteTfpSupplierByIds(ids);
    }

    /**
     * 删除供应商信息信息
     * 
     * @param id 供应商信息主键
     * @return 结果
     */
    @Override
    public int deleteTfpSupplierById(Long id)
    {
        return tfpSupplierMapper.deleteTfpSupplierById(id);
    }

    @Override
    public List<TfpSupplier> selectSupplerInfoBySupplierIdList(List<Long> supplierIdList) {
        return tfpSupplierMapper.selectSupplerInfoBySupplierIdList(supplierIdList);
    }

    @Override
    public List<SupplierVo> handleSupplierVo(List<Long> supplierIdList) {
        List<SupplierVo> supplierVoList = new ArrayList<>();
        List<TfpSupplier> supplierList = this.selectSupplerInfoBySupplierIdList(supplierIdList);
        if (supplierList != null && supplierList.size() > 0){
            for (TfpSupplier supplier : supplierList) {
                SupplierVo supplierVo = new SupplierVo();
                supplierVo.setId(supplier.getId());
                supplierVo.setName(supplier.getSupplierName());
                supplierVoList.add(supplierVo);
            }
        }

        return supplierVoList;
    }

    @Override
    public List<SupplierVo> supplierSelect() {
        return tfpSupplierMapper.supplierSelect();
    }
}
