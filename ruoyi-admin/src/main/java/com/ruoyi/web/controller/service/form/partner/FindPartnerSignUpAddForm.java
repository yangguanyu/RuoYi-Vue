package com.ruoyi.web.controller.service.form.partner;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 找搭子上车 tfp_find_partner_sign_up
 *
 * @author ruoyi
 * @date 2023-10-31
 */
@ApiModel("FindPartnerSignUpAddForm-找搭子上车")
@Data
public class FindPartnerSignUpAddForm {

    /** 找搭子id */
    @ApiModelProperty(name = "找搭子id")
    private Long findPartnerId;

    /** 报名用户id */
    @ApiModelProperty(name = "报名用户id")
    private Long signUpUserId;

    /** 联系电话 */
    @ApiModelProperty(name = "联系电话")
    private String phoneNumber;

    /** 性别 0-男生 1-女生 */
    @ApiModelProperty(name = "性别 0-男生 1-女生")
    private Integer sex;

    /** 自我介绍 */
    @ApiModelProperty(name = "自我介绍")
    private String selfIntroductio;

    @ApiModelProperty(name = "审核状态 0-待审核 1-审核通过 2-审核未通过")
    private Integer auditStatus;



}
