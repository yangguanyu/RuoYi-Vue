package com.ruoyi.system.service.impl;

import java.util.*;
import java.util.stream.Collectors;

import com.ruoyi.common.core.domain.entity.SysDictType;
import com.ruoyi.common.utils.DcListUtils;
import com.ruoyi.service.vo.DicDataVo;
import com.ruoyi.service.vo.ServiceDicDataVo;
import com.ruoyi.system.service.ISysDictTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.common.core.domain.entity.SysDictData;
import com.ruoyi.common.utils.DictUtils;
import com.ruoyi.system.mapper.SysDictDataMapper;
import com.ruoyi.system.service.ISysDictDataService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 字典 业务层处理
 * 
 * @author ruoyi
 */
@Service
@Transactional
public class SysDictDataServiceImpl implements ISysDictDataService
{
    @Autowired
    private SysDictDataMapper dictDataMapper;

    /**
     * 根据条件分页查询字典数据
     * 
     * @param dictData 字典数据信息
     * @return 字典数据集合信息
     */
    @Override
    public List<SysDictData> selectDictDataList(SysDictData dictData)
    {
        return dictDataMapper.selectDictDataList(dictData);
    }

    /**
     * 根据字典类型和字典键值查询字典数据信息
     * 
     * @param dictType 字典类型
     * @param dictValue 字典键值
     * @return 字典标签
     */
    @Override
    public String selectDictLabel(String dictType, String dictValue)
    {
        return dictDataMapper.selectDictLabel(dictType, dictValue);
    }

    /**
     * 根据字典数据ID查询信息
     * 
     * @param dictCode 字典数据ID
     * @return 字典数据
     */
    @Override
    public SysDictData selectDictDataById(Long dictCode)
    {
        return dictDataMapper.selectDictDataById(dictCode);
    }

    /**
     * 批量删除字典数据信息
     * 
     * @param dictCodes 需要删除的字典数据ID
     */
    @Override
    public void deleteDictDataByIds(Long[] dictCodes)
    {
        for (Long dictCode : dictCodes)
        {
            SysDictData data = selectDictDataById(dictCode);
            dictDataMapper.deleteDictDataById(dictCode);
            List<SysDictData> dictDatas = dictDataMapper.selectDictDataByType(data.getDictType());
            DictUtils.setDictCache(data.getDictType(), dictDatas);
        }
    }

    /**
     * 新增保存字典数据信息
     * 
     * @param data 字典数据信息
     * @return 结果
     */
    @Override
    public int insertDictData(SysDictData data)
    {
        int row = dictDataMapper.insertDictData(data);
        if (row > 0)
        {
            List<SysDictData> dictDatas = dictDataMapper.selectDictDataByType(data.getDictType());
            DictUtils.setDictCache(data.getDictType(), dictDatas);
        }
        return row;
    }

    /**
     * 修改保存字典数据信息
     * 
     * @param data 字典数据信息
     * @return 结果
     */
    @Override
    public int updateDictData(SysDictData data)
    {
        int row = dictDataMapper.updateDictData(data);
        if (row > 0)
        {
            List<SysDictData> dictDatas = dictDataMapper.selectDictDataByType(data.getDictType());
            DictUtils.setDictCache(data.getDictType(), dictDatas);
        }
        return row;
    }

    @Override
    public List<ServiceDicDataVo> getServiceDicData(List<String> typeList) {
        List<ServiceDicDataVo> serviceDicDataVoList = new ArrayList<>();
        if (DcListUtils.isNotEmpty(typeList)){
            List<DicDataVo> dictDataList = dictDataMapper.selectDictDataByTypeList(typeList);
            if (DcListUtils.isNotEmpty(dictDataList)){
                Map<String, List<DicDataVo>> dictDataMap = dictDataList.stream().collect(Collectors.groupingBy(DicDataVo::getType));
                Set<Map.Entry<String, List<DicDataVo>>> entries = dictDataMap.entrySet();
                for (Map.Entry<String, List<DicDataVo>> entry : entries) {
                    ServiceDicDataVo serviceDicDataVo = new ServiceDicDataVo();
                    serviceDicDataVo.setType(entry.getKey());

                    List<DicDataVo> dicDataVoList = entry.getValue();
                    if (DcListUtils.isNotEmpty(dicDataVoList)){
                        Map<Long, List<DicDataVo>> sonDicDataMap = new HashMap<>();
                        Iterator<DicDataVo> iterator = dicDataVoList.iterator();
                        while (iterator.hasNext()){
                            DicDataVo dicDataVo = iterator.next();
                            Long parentCode = dicDataVo.getParentCode();
                            if (Objects.nonNull(parentCode)){
                                List<DicDataVo> sonDicDataVoList = sonDicDataMap.get(parentCode);
                                if (sonDicDataVoList == null){
                                    sonDicDataVoList = new ArrayList<>();
                                }

                                sonDicDataVoList.add(dicDataVo);
                                sonDicDataMap.put(parentCode, sonDicDataVoList);

                                iterator.remove();
                            }
                        }

                        if (sonDicDataMap.size() > 0){
                            this.handleParentSonList(dicDataVoList, sonDicDataMap);
                        }
                    }

                    serviceDicDataVo.setDicDataVoList(dicDataVoList);
                    serviceDicDataVoList.add(serviceDicDataVo);
                }
            }
        }

        return serviceDicDataVoList;
    }

    @Autowired
    private ISysDictTypeService dictTypeService;

    @Override
    public int test() {
        List<String> list = new ArrayList<>();
//        两级 list.add("[data类型]:[data名称]:[父级名称]:[子集名称1 子集名称2 子集名称3 子集名称4 子集名称5]");
//        一级 list.add("[data类型]:[data名称]:[子集名称1 子集名称2 子集名称3 子集名称4 子集名称5]");
//        list.add("user_interest:兴趣:兴趣:每年至少一次旅行 ktc歌王 喜欢垂钓 知晓中华上下五千年 茶农 朋友圈摄影 运动神经满分 球场少不了我");
//        list.add("refund_personal_reason:个人原因:个人原因:行程有变，无法出行 未找到结伴友人，不想去了");
//        list.add("refund_merchant_reason:商家原因:商家原因:商家确认无库存 符合不可抗力情况，无法出行 商家迟迟不确认订单");
//        list.add("refund_other_reason:其他原因:其他原因:其他原因");

        if (DcListUtils.isNotEmpty(list)){
            for (String str : list) {
                String[] arrys = str.split(":");
                int length = arrys.length;
                if (length == 3 || length == 4){
                    String type = arrys[0];
                    String name = arrys[1];
                    SysDictType dictType = new SysDictType();
                    dictType.setDictName(name);
                    dictType.setDictType(type);
                    dictType.setStatus("0");
                    dictType.setCreateBy("duanc");
                    dictType.setCreateTime(new Date());
                    dictTypeService.insertDictType(dictType);

                    String sons = null;
                    if (length == 3){
                        sons = arrys[2];
                    }

                    Long parentDictCode = null;
                    if (length == 4){
                        sons = arrys[3];

                        String parentStr = arrys[2];

                        SysDictData dictData = new SysDictData();
                        dictData.setDictLabel(parentStr);
                        dictData.setDictValue("0");
                        dictData.setDictSort(1L);
                        dictData.setDictType(type);
                        dictData.setIsDefault("N");
                        dictData.setStatus("0");
                        dictData.setCreateBy("duanc");
                        dictData.setCreateTime(new Date());
                        dictDataMapper.insertDictData(dictData);

                        parentDictCode = dictData.getDictCode();
                    }

                    String[] sonArrys = sons.split(" ");
                    int size = sonArrys.length;
                    for (int i = 0; i < size; i++) {
                        SysDictData dictData = new SysDictData();
                        dictData.setParentCode(parentDictCode);
                        dictData.setDictLabel(sonArrys[i]);
                        dictData.setDictValue(i + "");
                        dictData.setDictSort(new Long(i + 1));
                        dictData.setDictType(type);
                        dictData.setIsDefault("N");
                        dictData.setStatus("0");
                        dictData.setCreateBy("duanc");
                        dictData.setCreateTime(new Date());
                        dictDataMapper.insertDictData(dictData);
                    }
                }
            }
        }
        return 1;
    }

    private void handleParentSonList(List<DicDataVo> dicDataVoList, Map<Long, List<DicDataVo>> sonDicDataMap){
        if (DcListUtils.isNotEmpty(dicDataVoList)){
            for (DicDataVo dicDataVo : dicDataVoList) {
                Long dictCode = dicDataVo.getDictCode();
                List<DicDataVo> sonDicDataVoList = sonDicDataMap.get(dictCode);
                if (DcListUtils.isNotEmpty(sonDicDataVoList)){
                    dicDataVo.setDicDataVoList(sonDicDataVoList);
                    sonDicDataMap.remove(dictCode);

                    this.handleParentSonList(sonDicDataVoList, sonDicDataMap);
                }
            }
        }
    }
}
