-- 聊天群组表
CREATE TABLE `tfp_chat_group` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(50) comment '聊天群组名称',
  `product_id` bigint(20) comment '商品id',
  `limit_person_number` int comment '限制人数',
  `sign_up_person_number` int comment '报名人数',
  `go_date` datetime comment '出发时间',
  PRIMARY KEY (`id`)
) COMMENT='聊天群组表';

-- 聊天群组成员表
CREATE TABLE `tfp_chat_group_person` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chat_group_id` bigint(20) comment '聊天群组id',
  `product_id` bigint(20) comment '商品id',
  `user_id` bigint(20) comment '用户id',
  `user_type` int(4) comment '用户类型 0-发起者 1-已报名 2-未报名',
  PRIMARY KEY (`id`)
) COMMENT='聊天群组成员表';

-- 聊天群组成员聊天信息表
CREATE TABLE `tfp_chat_group_person_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chat_group_id` bigint(20) comment '聊天群组id',
  `chat_group_person_id` bigint(20) comment '聊天群组成员id',
  `chat_info` varchar(300) COMMENT '聊天信息',
  PRIMARY KEY (`id`)
) COMMENT='聊天群组成员聊天信息表';