package com.ruoyi.service.vo;

import lombok.Data;

@Data
public class WxReturnInfoVo {
    private String errcode;
    private String errmsg;
}
