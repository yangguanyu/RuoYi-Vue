package com.ruoyi.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ruoyi.service.domain.TfpOrder;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
@ApiModel(value = "AuditRefundDto")
public class AuditRefundDto {
    @ApiModelProperty("审核退款订单id")
    private Long orderId;
    @ApiModelProperty("审核状态 1-审核通过 2-审核不通过")
    private Integer auditStatus;
    @ApiModelProperty("退款类型 1-全额退款   2-部分退款")
    private Integer refundType;
    @ApiModelProperty("退款金额 部分退款时填写")
    private BigDecimal refundAmount;
    @ApiModelProperty("凭证")
    private List<String> urlList;
    @ApiModelProperty("申请退款驳回的原因")
    private String rejectReason;

    // 退款原因
    @JsonIgnore
    private String reason;
    @JsonIgnore
    private TfpOrder tfpOrder;
}
