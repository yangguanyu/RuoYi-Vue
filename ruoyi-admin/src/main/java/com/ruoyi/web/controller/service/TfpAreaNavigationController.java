package com.ruoyi.web.controller.service;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.service.vo.DestinationAreaVo;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.TfpAreaNavigation;
import com.ruoyi.service.service.ITfpAreaNavigationService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 地区导航Controller
 * 
 * @author ruoyi
 * @date 2023-12-25
 */
@RestController
@RequestMapping("/service/areaNavigation")
public class TfpAreaNavigationController extends BaseController
{
    @Autowired
    private ITfpAreaNavigationService tfpAreaNavigationService;

    /**
     * 查询地区导航列表
     */
    @PreAuthorize("@ss.hasPermi('service:areaNavigation:list')")
    @GetMapping("/list")
    public TableDataInfo list(TfpAreaNavigation tfpAreaNavigation)
    {
        startPage();
        List<TfpAreaNavigation> list = tfpAreaNavigationService.selectTfpAreaNavigationList(tfpAreaNavigation);
        return getDataTable(list);
    }

    /**
     * 导出地区导航列表
     */
    @PreAuthorize("@ss.hasPermi('service:areaNavigation:export')")
    @Log(title = "地区导航", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TfpAreaNavigation tfpAreaNavigation)
    {
        List<TfpAreaNavigation> list = tfpAreaNavigationService.selectTfpAreaNavigationList(tfpAreaNavigation);
        ExcelUtil<TfpAreaNavigation> util = new ExcelUtil<TfpAreaNavigation>(TfpAreaNavigation.class);
        util.exportExcel(response, list, "地区导航数据");
    }

    /**
     * 获取地区导航详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:areaNavigation:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tfpAreaNavigationService.selectTfpAreaNavigationById(id));
    }

    /**
     * 新增地区导航
     */
    @PreAuthorize("@ss.hasPermi('service:areaNavigation:add')")
    @Log(title = "地区导航", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TfpAreaNavigation tfpAreaNavigation)
    {
        return toAjax(tfpAreaNavigationService.insertTfpAreaNavigation(tfpAreaNavigation));
    }

    /**
     * 修改地区导航
     */
    @PreAuthorize("@ss.hasPermi('service:areaNavigation:edit')")
    @Log(title = "地区导航", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TfpAreaNavigation tfpAreaNavigation)
    {
        return toAjax(tfpAreaNavigationService.updateTfpAreaNavigation(tfpAreaNavigation));
    }

    /**
     * 删除地区导航
     */
    @PreAuthorize("@ss.hasPermi('service:areaNavigation:remove')")
    @Log(title = "地区导航", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tfpAreaNavigationService.deleteTfpAreaNavigationByIds(ids));
    }

    @ApiOperation("小程序-获取目的地")
    @PostMapping("/getDestinationArea")
    @ResponseBody
    public R<List<DestinationAreaVo>> getDestinationArea(){
        return R.ok(tfpAreaNavigationService.getDestinationArea());
    }

    @ApiOperation("小程序-查询目的地区域名称列表")
    @PostMapping("/searchDestinationAreaNameByKey")
    @ResponseBody
    public R<List<String>> searchDestinationAreaNameByKey(@RequestParam("key") String key){
        return R.ok(tfpAreaNavigationService.searchDestinationAreaNameByKey(key));
    }
}
