package com.ruoyi.common.enuma.flight;

import com.ruoyi.common.enuma.BaseStrEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TicketChangeExceptionCode implements BaseStrEnum {
    FLIGHT_NUMBER_REQUIRED("010907", "航班号不能为空"),
    OPERATION_NOT_ALLOWED("010906", "NO_ALLOWED"),
    NAME_REQUIRED("010909", "name不能为空"),
    IDENTITY_INFORMATION_REQUIRED("010908", "证件信息不能为空"),
    INVALID_PARAMETER("010910", "参数不正确"),
    NOT_SUPPORTED_CHANGE("010901", "暂不支持改签"),
    CHECK_PARAMETER_OR_CONTACT_MANAGER("010912", "请检查参数或联系客服经理"),
    ORDER_ALREADY_REFUNDED("010911", "此订单已退票"),
    COMMENT_REQUIRED("010903", "comment不能为空"),
    ORDER_NOT_FOUND("010914", "未找到对应的订单"),
    NOT_SUPPORTED_CONFIRMATION("010902", "暂不支持航变确认"),
    CHANGE_ORDER_STATUS_CANCELLABLE("010913", "变更单状态不可取消"),
    REFUNDING_TICKET_CHANGE_NOT_ALLOWED("010905", "退款中的客票，不能申请变更"),
    CHANGE_ORDER_NOT_FOUND("010916", "未找到对应的变更订单"),
    TICKET_ALREADY_CHANGED_OR_IN_PROGRESS("010904", "客票已变更或正在变更中"),
    NO_RIGHTS_TO_OPERATE_ORDER("010915", "无权操作订单");

    private final String value;
    private final String desc;


}
