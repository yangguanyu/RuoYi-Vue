package com.ruoyi.web.controller.service;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.service.domain.flight.res.CheckSeatAndPriceResp;
import com.ruoyi.service.domain.flight.res.CreateOrderByPassengerResp;
import com.ruoyi.service.domain.flight.res.FlightSearchResp;
import com.ruoyi.service.dto.CheckSeatAndPriceParamDto;
import com.ruoyi.service.dto.CreateOrderByPassengerDto;
import com.ruoyi.service.dto.FlightSearchParamDto;
import com.ruoyi.service.service.IFlightService;
import com.ruoyi.service.vo.TfpFlightVo;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/service/flight")
public class FlightController extends BaseController {
    @Autowired
    private IFlightService flightService;

    @ApiOperation("航班查询接口")
    @PostMapping("/searchFlight")
    @ResponseBody
    public R<List<TfpFlightVo>> searchFlight(@RequestBody FlightSearchParamDto flightSearchParamDto){
        return R.ok(flightService.searchFlight(flightSearchParamDto));
    }

    @ApiOperation("验证舱位价格接口")
    @PostMapping("/checkSeatAndPrice")
    @ResponseBody
    public R<CheckSeatAndPriceResp> checkSeatAndPrice(@RequestBody CheckSeatAndPriceParamDto CheckSeatAndPriceParamDto){
        return R.ok(flightService.checkSeatAndPrice(CheckSeatAndPriceParamDto));
    }

    @ApiOperation("机票预订接口")
    @PostMapping("/createOrderByPassenger")
    @ResponseBody
    public R<CreateOrderByPassengerResp> createOrderByPassenger(@RequestBody CreateOrderByPassengerDto createOrderByPassengerDto){
        return R.ok(flightService.createOrderByPassenger(createOrderByPassengerDto));
    }
}
