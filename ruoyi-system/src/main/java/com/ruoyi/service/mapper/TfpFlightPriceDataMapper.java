package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.TfpFlightPriceData;
import org.apache.ibatis.annotations.Param;

/**
 * 价格数据Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-10
 */
public interface TfpFlightPriceDataMapper 
{
    /**
     * 查询价格数据
     * 
     * @param id 价格数据主键
     * @return 价格数据
     */
    public TfpFlightPriceData selectTfpFlightPriceDataById(Long id);

    /**
     * 查询价格数据列表
     * 
     * @param tfpFlightPriceData 价格数据
     * @return 价格数据集合
     */
    public List<TfpFlightPriceData> selectTfpFlightPriceDataList(TfpFlightPriceData tfpFlightPriceData);

    /**
     * 新增价格数据
     * 
     * @param tfpFlightPriceData 价格数据
     * @return 结果
     */
    public int insertTfpFlightPriceData(TfpFlightPriceData tfpFlightPriceData);

    /**
     * 修改价格数据
     * 
     * @param tfpFlightPriceData 价格数据
     * @return 结果
     */
    public int updateTfpFlightPriceData(TfpFlightPriceData tfpFlightPriceData);

    /**
     * 删除价格数据
     * 
     * @param id 价格数据主键
     * @return 结果
     */
    public int deleteTfpFlightPriceDataById(Long id);

    /**
     * 批量删除价格数据
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpFlightPriceDataByIds(Long[] ids);

    void deleteTfpFlightPriceDataByFlightId(@Param("flightId") Long flightId);
}
