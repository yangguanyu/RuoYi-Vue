package com.ruoyi.service.domain.flight.param;

import lombok.Data;

import java.util.List;

@Data
public class CheckSeatAndPriceParam {
    // 航程类型 OW 单程，RT 往返，MS 多程？ 联程？ 缺口 目前仅支持单程
    private String routeType;
//    // 乘客类型: 1.成人、2. 儿童、3.成人+儿童 4. 成人+婴儿 5. 成人+儿童+婴儿 当日前不支持包含婴儿
//    private Integer passengerType;
    // 产品ID
    private String policyId;
    // 产品类型
    private Integer productType;
    // 航程航线数据
    private List<Segment> segmentList;

    @Data
    public class Segment {
        // 航班日期(yyyy-MM-dd)
        private String depDate;
        // 航班到达日期(yyyy-MM-dd)
        private String arrDate;
        // 必须机场三字码
        private String depCode;
        // 必须机场三字码
        private String arrCode;
        // 航班号：CA1180
        private String flightNo;
        // 舱位如：Y
        private String cabin;
        // 坐位状态 A-0
        private String cabinStatus;
        // 乘客类型 1.成人 2.儿童 3.婴儿暂不支持
        private Integer passengerType;
        // 票面价格
        private Integer price;
        // 单人结算价 (price * (1 - commisionPoint%) - commisionMoney) 不含税
        private Double settlement;
        // 单人机建
        private Integer airportTax;
        // 单人燃油费
        private Integer fuelTax;
    }
}
