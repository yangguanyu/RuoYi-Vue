package com.ruoyi.system.mapper;

import com.ruoyi.common.core.domain.entity.SysUserWx;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户微信Mapper接口
 * 
 * @author ruoyi
 * @date 2023-10-09
 */
public interface SysUserWxMapper 
{
    /**
     * 查询用户微信
     * 
     * @param id 用户微信主键
     * @return 用户微信
     */
    public SysUserWx selectSysUserWxById(Long id);

    /**
     * 查询用户微信列表
     * 
     * @param sysUserWx 用户微信
     * @return 用户微信集合
     */
    public List<SysUserWx> selectSysUserWxList(SysUserWx sysUserWx);

    /**
     * 新增用户微信
     * 
     * @param sysUserWx 用户微信
     * @return 结果
     */
    public int insertSysUserWx(SysUserWx sysUserWx);

    /**
     * 修改用户微信
     * 
     * @param sysUserWx 用户微信
     * @return 结果
     */
    public int updateSysUserWx(SysUserWx sysUserWx);

    /**
     * 删除用户微信
     * 
     * @param id 用户微信主键
     * @return 结果
     */
    public int deleteSysUserWxById(Long id);

    /**
     * 批量删除用户微信
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysUserWxByIds(String[] ids);

    public List<SysUserWx> findSysUserWxByUnionId(@Param("unionId") String unionId);

    public List<SysUserWx> findSysUserWxByOpenId(@Param("openId") String openId);

    SysUserWx findSysUserWxByUserId(@Param("userId") Long userId);

    SysUserWx selectSysUserWxByOpenId(@Param("openId") String openid);
}
