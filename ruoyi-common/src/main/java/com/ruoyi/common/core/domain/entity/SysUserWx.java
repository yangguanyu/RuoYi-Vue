package com.ruoyi.common.core.domain.entity;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 用户微信对象 sys_user_wx
 *
 * @author ruoyi
 * @date 2023-10-10
 */
public class SysUserWx extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 微信union id */
    @Excel(name = "微信union id")
    private String unionId;

    /** 微信 open id */
    @Excel(name = "微信 open id")
    private String openId;

    /** 昵称 */
    @Excel(name = "昵称")
    private String nickName;

    /** 登录用户名 */
    @Excel(name = "登录用户名")
    private String loginName;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String phonenumber;

    /** 微信应用描述 */
    @Excel(name = "微信应用描述")
    private String appDesc;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    @Excel(name = "创建者id")
    private Long createUserId;

    /** 更新者id */
    @Excel(name = "更新者id")
    private Long updateUserId;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setUnionId(String unionId)
    {
        this.unionId = unionId;
    }

    public String getUnionId()
    {
        return unionId;
    }
    public void setOpenId(String openId)
    {
        this.openId = openId;
    }

    public String getOpenId()
    {
        return openId;
    }
    public void setNickName(String nickName)
    {
        this.nickName = nickName;
    }

    public String getNickName()
    {
        return nickName;
    }
    public void setLoginName(String loginName)
    {
        this.loginName = loginName;
    }

    public String getLoginName()
    {
        return loginName;
    }
    public void setPhonenumber(String phonenumber)
    {
        this.phonenumber = phonenumber;
    }

    public String getPhonenumber()
    {
        return phonenumber;
    }
    public void setAppDesc(String appDesc)
    {
        this.appDesc = appDesc;
    }

    public String getAppDesc()
    {
        return appDesc;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }
    public void setCreateUserId(Long createUserId)
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId()
    {
        return createUserId;
    }
    public void setUpdateUserId(Long updateUserId)
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId()
    {
        return updateUserId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("userId", getUserId())
                .append("unionId", getUnionId())
                .append("openId", getOpenId())
                .append("nickName", getNickName())
                .append("loginName", getLoginName())
                .append("phonenumber", getPhonenumber())
                .append("appDesc", getAppDesc())
                .append("delFlag", getDelFlag())
                .append("createUserId", getCreateUserId())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateUserId", getUpdateUserId())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}
