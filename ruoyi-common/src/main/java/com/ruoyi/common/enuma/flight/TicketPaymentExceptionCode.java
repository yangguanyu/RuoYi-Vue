package com.ruoyi.common.enuma.flight;

import com.ruoyi.common.enuma.BaseStrEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TicketPaymentExceptionCode  implements BaseStrEnum {
    ORDER_PAYMENT_FAILED("010510", "订单支付失败，请检查订单状态"),
    INVALID_REQUEST("010501", "无效的请求"),
    NON_EXISTENT_SALESPERSON("010512", "不存在的营业员"),
    ORDER_ALREADY_PAID("010511", "order has been paid, cannot be paid again"),
    NO_ORDER_DATA("010513", "无订单数据"),
    ORDER_DOES_NOT_EXIST("010503", "该订单不存在，请检查订单号"),
    ORDER_ALREADY_PAID_CANNOT_PAY_AGAIN("010514", "该订单已支付．不可再次支付"),
    INVALID_REQUEST_DUPLICATE("010502", "无效的请求"),
    PAYMENT_TIMEOUT("010513", "支付超时。请检查订单状态"),
    INSUFFICIENT_BALANCE("010516", "账户余额不足"),
    INSUFFICIENT_SEATS("010504", "座位不足"),
    UNPAYABLE("010518", "不可支付"),
    PAYMENT_FAILED("010517", "验仓失败"),
    PAYMENT_TIMEOUT_DUPLICATE("010509", "支付超时。请检查订单状态"),
    ORDER_DOES_NOT_EXIST_DUPLICATE("010508", "该订单不存在，请检查订单号");

    private final String value;
    private final String desc;
}
