package com.ruoyi.service.service;

import java.util.List;
import com.ruoyi.service.domain.TfpProductWantToGo;
import com.ruoyi.service.dto.WantToGoDto;
import com.ruoyi.service.vo.ListProductVo;

/**
 * 商品想去Service接口
 * 
 * @author ruoyi
 * @date 2023-12-26
 */
public interface ITfpProductWantToGoService 
{
    /**
     * 查询商品想去
     * 
     * @param id 商品想去主键
     * @return 商品想去
     */
    public TfpProductWantToGo selectTfpProductWantToGoById(Long id);

    /**
     * 查询商品想去列表
     * 
     * @param tfpProductWantToGo 商品想去
     * @return 商品想去集合
     */
    public List<TfpProductWantToGo> selectTfpProductWantToGoList(TfpProductWantToGo tfpProductWantToGo);

    /**
     * 新增商品想去
     * 
     * @param tfpProductWantToGo 商品想去
     * @return 结果
     */
    public int insertTfpProductWantToGo(TfpProductWantToGo tfpProductWantToGo);

    /**
     * 修改商品想去
     * 
     * @param tfpProductWantToGo 商品想去
     * @return 结果
     */
    public int updateTfpProductWantToGo(TfpProductWantToGo tfpProductWantToGo);

    /**
     * 批量删除商品想去
     * 
     * @param ids 需要删除的商品想去主键集合
     * @return 结果
     */
    public int deleteTfpProductWantToGoByIds(Long[] ids);

    /**
     * 删除商品想去信息
     * 
     * @param id 商品想去主键
     * @return 结果
     */
    public int deleteTfpProductWantToGoById(Long id);

    TfpProductWantToGo selectTfpProductWantToGo(TfpProductWantToGo param);

    /**
     * 商品想去
     * @param wantToGoDto
     */
    int wantToGo(WantToGoDto wantToGoDto);

    List<ListProductVo> myWantToProductList();
}
