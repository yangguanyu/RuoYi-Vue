package com.ruoyi.service.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
@ApiModel(value = "OrderDetailVo", description = "订单详情")
public class OrderDetailVo {
    @ApiModelProperty("订单id")
    private Long orderId;
    @ApiModelProperty("商品id")
    private Long productId;
    @ApiModelProperty("商品名称")
    private String productName;
    @ApiModelProperty("商品图片")
    private String productImage;
    @ApiModelProperty("订单状态 0-待支付 1-已完成 2-待确认(已支付) 3-待出行 4-申请退款-审核中 5-申请退款-退款成功 6-过期 7-取消")
    private Integer status;
    @ApiModelProperty("超时时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date expireDate;

    @ApiModelProperty("服务类型id")
    private Long categoryId;
    @ApiModelProperty("服务类型名称")
    private String categoryName;
    @ApiModelProperty("线路亮点集合")
    private List<String> routeHighlightList;

    @ApiModelProperty("订单编号")
    private String orderNo;
    @ApiModelProperty("创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    @ApiModelProperty("支付金额")
    private BigDecimal finalAmount;

    @ApiModelProperty("出发时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date goDate;
    @ApiModelProperty("出发地")
    private String goPlaceName;
    @ApiModelProperty("同行人姓名集合")
    private List<String> mateNameList;
    @ApiModelProperty("快照id")
    private Long snapshotId;
    @ApiModelProperty("聊天群组id")
    private Long ChatGroupId;
    @ApiModelProperty("聊天群组名称")
    private String ChatGroupName;
    @ApiModelProperty("报名人数")
    private Integer signUpPersonNumber;
    @ApiModelProperty("限制人数")
    private Integer limitPersonNumber;
    @ApiModelProperty("报名人员头像url集合")
    private List<String> signUpPersonHeadPortraitUrlList;

    @ApiModelProperty("退款信息")
    private RefundInfoVo refundInfo;

    @ApiModelProperty("出行方式 0-组队出行 1-独立出行")
    private Integer travelType;

    @ApiModelProperty("是否可以申请退款 0-否 1-是")
    private Integer applyRefundFlag;

    @ApiModelProperty("是否选择低价机票服务 0-否 1-是")
    private Integer lowPriceAirTicketServiceFlag;
}
