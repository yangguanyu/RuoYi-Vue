package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "SaveSystemImageDto")
public class SaveSystemImageDto {
    @ApiModelProperty("fileId")
    private String fileId;
    @ApiModelProperty("后缀名")
    private String suffix;
}
