package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.TfpProduct;
import com.ruoyi.service.dto.ListProductSelectDto;
import com.ruoyi.service.vo.ListProductVo;
import com.ruoyi.service.vo.TfpProductVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 商品主Mapper接口
 *
 * @author ruoyi
 * @date 2023-11-01
 */
@Mapper
public interface TfpProductMapper
{
    /**
     * 查询商品主
     *
     * @param id 商品主主键
     * @return 商品主
     */
    public TfpProduct selectTfpProductById(Long id);

    /**
     * 查询商品主列表
     *
     * @param tfpProduct 商品主
     * @return 商品主集合
     */
    public List<TfpProduct> selectTfpProductList(TfpProduct tfpProduct);

    List<TfpProductVo> selectTfpProductVoList(TfpProduct tfpProduct);

    /**
     * 热门推荐列表
     * @param tfpProduct
     * @return
     */
    List<TfpProductVo> selectHotList(TfpProduct tfpProduct);

    /**
     * 新增商品主
     *
     * @param tfpProduct 商品主
     * @return 结果
     */
    public int insertTfpProduct(TfpProduct tfpProduct);

    /**
     * 修改商品主
     *
     * @param tfpProduct 商品主
     * @return 结果
     */
    public int updateTfpProduct(TfpProduct tfpProduct);

    /**
     * 删除商品主
     *
     * @param id 商品主主键
     * @return 结果
     */
    public int deleteTfpProductById(Long id);

    /**
     * 批量删除商品主
     *
     * @param idList 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpProductByIds(@Param("idList") List<Long> idList);

    List<TfpProduct> selectTfpProductListByIdList(@Param("idList") List<Long> idList);

    List<ListProductVo> listProductSelect(ListProductSelectDto listProductSelectDto);
}
