package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@ApiModel("EditUserInfoDto")
public class EditUserInfoDto {
    @ApiModelProperty("用户id")
    private Long userId;
    @ApiModelProperty("用户昵称")
    private String nickName;
    @ApiModelProperty("用户性别（0男 1女 2未知）")
    private String sex;
    @ApiModelProperty("生日")
    private Date birthday;
    @ApiModelProperty("星座编码")
    private String constellationCode;
    @ApiModelProperty("星座名称")
    private String constellationName;
    @ApiModelProperty("实名认证状态 0-未实名认证 1-已实名认证")
    private Integer realNameAuthStatus;
    @ApiModelProperty("个人简介")
    private String personalProfile;
    @ApiModelProperty("用户标签集合")
    private List<UserLabelDto> userLabelList;
    @ApiModelProperty("用户图片集合")
    private List<UserImageDto> userImageList;
    @ApiModelProperty("手机号码")
    private String phonenumber;
}
