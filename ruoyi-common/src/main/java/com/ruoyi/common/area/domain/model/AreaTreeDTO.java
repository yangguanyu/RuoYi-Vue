package com.ruoyi.common.area.domain.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class AreaTreeDTO {
    public AreaTreeDTO(Long id, String label) {
        this.id = id;
        this.label = label;
    }

    private Long id;
    private String label;
    private List<AreaTreeDTO> children;
}
