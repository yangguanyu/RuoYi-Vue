package com.ruoyi.service.service.impl;

import java.util.*;
import java.util.stream.Collectors;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enuma.BusinessSubTypeEnum;
import com.ruoyi.common.enuma.BusinessTypeEnum;
import com.ruoyi.common.enuma.DeleteEnum;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.DcListUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.core.domain.entity.TfpAttachment;
import com.ruoyi.service.service.ITfpAttachmentService;
import com.ruoyi.service.vo.CareUserInfoVo;
import com.ruoyi.service.vo.FansUserInfoVo;
import com.ruoyi.service.vo.SysUserCareVo;
import com.ruoyi.service.vo.UserVo;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.SysUserCareMapper;
import com.ruoyi.service.domain.SysUserCare;
import com.ruoyi.service.service.ISysUserCareService;
import org.springframework.transaction.annotation.Transactional;

import static com.ruoyi.common.utils.PageUtils.startPage;

/**
 * 关注Service业务层处理
 *
 * @author ruoyi
 * @date 2023-10-31
 */
@Service
@Transactional
public class SysUserCareServiceImpl implements ISysUserCareService
{
    @Autowired
    private SysUserCareMapper sysUserCareMapper;
    @Autowired
    private ISysUserService sysUserService;
    @Autowired
    private ITfpAttachmentService attachmentService;

    /**
     * 查询关注
     *
     * @param id 关注主键
     * @return 关注
     */
    @Override
    public SysUserCare selectSysUserCareById(Long id)
    {
        return sysUserCareMapper.selectSysUserCareById(id);
    }

    /**
     * 查询关注列表
     *
     * @param sysUserCare 关注
     * @return 关注
     */
    @Override
    public List<SysUserCare> selectSysUserCareList(SysUserCare sysUserCare)
    {
        return sysUserCareMapper.selectSysUserCareList(sysUserCare);
    }

    @Override
    public List<SysUserCareVo> selectSysUserCareVoList(SysUserCare sysUserCare) {
        List<SysUserCareVo> sysUserCareVoList = new ArrayList<>();

        List<SysUserCare> sysUserCareList = sysUserCareMapper.selectSysUserCareList(sysUserCare);
        if (sysUserCareList != null && sysUserCareList.size() > 0){
            List<Long> userIdList = new ArrayList<>();
            for (SysUserCare userCare : sysUserCareList) {
                Long ownerUserId = userCare.getOwnerUserId();
                if (!userIdList.contains(ownerUserId)){
                    userIdList.add(ownerUserId);
                }

                Long fansUserId = userCare.getFansUserId();
                if (!userIdList.contains(fansUserId)){
                    userIdList.add(fansUserId);
                }

                SysUserCareVo userCareVo = new SysUserCareVo();
                BeanUtils.copyProperties(userCare, userCareVo);
                sysUserCareVoList.add(userCareVo);
            }

            Map<Long, String> nickNameMap = sysUserService.getNickNameMap(userIdList);

            for (SysUserCareVo userCareVo : sysUserCareVoList) {
                userCareVo.setOwnerNickName(nickNameMap.get(userCareVo.getOwnerUserId()));
                userCareVo.setFansNickName(nickNameMap.get(userCareVo.getFansUserId()));
            }
        }

        return sysUserCareVoList;
    }

    /**
     * 新增关注
     *
     * @param sysUserCare 关注
     * @return 结果
     */
    @Override
    public int insertSysUserCare(SysUserCare sysUserCare)
    {
        sysUserCare.setCreateTime(DateUtils.getNowDate());
        return sysUserCareMapper.insertSysUserCare(sysUserCare);
    }

    /**
     * 修改关注
     *
     * @param sysUserCare 关注
     * @return 结果
     */
    @Override
    public int updateSysUserCare(SysUserCare sysUserCare)
    {
        sysUserCare.setUpdateTime(DateUtils.getNowDate());
        return sysUserCareMapper.updateSysUserCare(sysUserCare);
    }

    /**
     * 批量删除关注
     *
     * @param ids 需要删除的关注主键
     * @return 结果
     */
    @Override
    public int deleteSysUserCareByIds(Long[] ids)
    {
        return sysUserCareMapper.deleteSysUserCareByIds(ids);
    }

    /**
     * 删除关注信息
     *
     * @param id 关注主键
     * @return 结果
     */
    @Override
    public int deleteSysUserCareById(Long id)
    {
        return sysUserCareMapper.deleteSysUserCareById(id);
    }

    @Override
    public List<UserVo> ownerUserSelect() {
        List<Long> userIdList = sysUserCareMapper.selectOwnerUserIdList();
        return sysUserService.handleUserVo(userIdList);
    }

    @Override
    public List<UserVo> fansUserSelect() {
        List<Long> userIdList = sysUserCareMapper.selectFansUserIdList();
        return sysUserService.handleUserVo(userIdList);
    }

    @Override
    public Integer selectCareNumber(Long userId) {
        return sysUserCareMapper.selectCareNumber(userId);
    }

    @Override
    public Integer selectFansNumber(Long userId) {
        return sysUserCareMapper.selectFansNumber(userId);
    }

    /**
     * 小程序-关注列表
     * @return
     */
    @Override
    public List<CareUserInfoVo> careUserInfoList(Long userId) {
        if (Objects.isNull(userId)){
            LoginUser loginUser = SecurityUtils.getLoginUser();
            userId = loginUser.getUserId();
        }

        startPage();
        List<CareUserInfoVo> careUserInfoVoList = sysUserCareMapper.careUserInfoList(userId);
        if (DcListUtils.isNotEmpty(careUserInfoVoList)){
            List<Long> userIdList = careUserInfoVoList.stream().map(CareUserInfoVo::getUserId).collect(Collectors.toList());

            Map<Long, String> nickNameMap = sysUserService.getNickNameMap(userIdList);
            Map<Long, String> userHeadImageMap = attachmentService.getUserHeadImageMap(userIdList);

            for (CareUserInfoVo careUserInfoVo : careUserInfoVoList) {
                careUserInfoVo.setNickName(nickNameMap.get(careUserInfoVo.getUserId()));
                careUserInfoVo.setUserHeadImage(userHeadImageMap.get(careUserInfoVo.getUserId()));
            }
        }

        return careUserInfoVoList;
    }

    /**
     * 小程序-粉丝列表
     * @return
     */
    @Override
    public List<FansUserInfoVo> fansUserInfoList(Long userId) {
        if (Objects.isNull(userId)){
            LoginUser loginUser = SecurityUtils.getLoginUser();
            userId = loginUser.getUserId();
        }

        startPage();
        List<FansUserInfoVo> careUserInfoVoList = sysUserCareMapper.fansUserInfoList(userId);
        if (DcListUtils.isNotEmpty(careUserInfoVoList)){
            List<Long> userIdList = careUserInfoVoList.stream().map(FansUserInfoVo::getUserId).collect(Collectors.toList());

            Map<Long, String> nickNameMap = sysUserService.getNickNameMap(userIdList);
            Map<Long, String> userHeadImageMap = attachmentService.getUserHeadImageMap(userIdList);

            for (FansUserInfoVo fansUserInfoVo : careUserInfoVoList) {
                fansUserInfoVo.setNickName(nickNameMap.get(fansUserInfoVo.getUserId()));
                fansUserInfoVo.setUserHeadImage(userHeadImageMap.get(fansUserInfoVo.getUserId()));
            }
        }

        return careUserInfoVoList;
    }

    /**
     * 小程序-关注用户
     * @param userId
     * @return
     */
    @Override
    public int careUser(Long userId) {
        if (Objects.isNull(userId)){
            throw new RuntimeException("用户id不能为空");
        }

        SysUser sysUser = sysUserService.selectUserById(userId);
        if (Objects.isNull(sysUser)){
            throw new RuntimeException("关注的用户不存在");
        }


        LoginUser loginUser = SecurityUtils.getLoginUser();
        if (Objects.isNull(loginUser)){
            throw new RuntimeException("用户未登录");
        }

        Long loginUserId = loginUser.getUserId();
        SysUserCare param = new SysUserCare();
        param.setOwnerUserId(userId);
        param.setFansUserId(loginUserId);
        param.setDelFlag(DeleteEnum.EXIST.getValue());
        List<SysUserCare> sysUserCareList = sysUserCareMapper.selectSysUserCareList(param);
        if (DcListUtils.isEmpty(sysUserCareList)){
            Integer mutualAttentionFlag = 0;

            param.setOwnerUserId(loginUserId);
            param.setFansUserId(userId);
            param.setDelFlag(DeleteEnum.EXIST.getValue());
            List<SysUserCare> userCareList = sysUserCareMapper.selectSysUserCareList(param);
            if (DcListUtils.isNotEmpty(userCareList)){
                mutualAttentionFlag = 1;

                SysUserCare userCare = userCareList.get(0);
                userCare.setMutualAttentionFlag(mutualAttentionFlag);
                userCare.setUpdateUserId(loginUser.getUserId());
                userCare.setUpdateBy(loginUser.getUsername());
                userCare.setUpdateTime(DateUtils.getNowDate());
                sysUserCareMapper.updateSysUserCare(userCare);
            }

            SysUserCare userCare = new SysUserCare();
            userCare.setOwnerUserId(userId);
            userCare.setFansUserId(loginUserId);
            userCare.setMutualAttentionFlag(mutualAttentionFlag);
            userCare.setDelFlag(DeleteEnum.EXIST.getValue());
            userCare.setCreateUserId(loginUser.getUserId());
            userCare.setCreateBy(loginUser.getUsername());
            userCare.setCreateTime(DateUtils.getNowDate());
            sysUserCareMapper.insertSysUserCare(userCare);
        }

        return 1;
    }

    @Override
    public int cancelCareUser(Long userId) {
        if (Objects.isNull(userId)){
            throw new RuntimeException("用户id不能为空");
        }

        SysUser sysUser = sysUserService.selectUserById(userId);
        if (Objects.isNull(sysUser)){
            throw new RuntimeException("取消关注的用户不存在");
        }

        LoginUser loginUser = SecurityUtils.getLoginUser();
        if (Objects.isNull(loginUser)){
            throw new RuntimeException("用户未登录");
        }

        Long loginUserId = loginUser.getUserId();
        SysUserCare param = new SysUserCare();
        param.setOwnerUserId(userId);
        param.setFansUserId(loginUserId);
        param.setDelFlag(DeleteEnum.EXIST.getValue());
        List<SysUserCare> sysUserCareList = sysUserCareMapper.selectSysUserCareList(param);
        if (DcListUtils.isNotEmpty(sysUserCareList)){
            for (SysUserCare userCare : sysUserCareList) {
                sysUserCareMapper.deleteSysUserCareById(userCare.getId());
            }

            param.setOwnerUserId(loginUserId);
            param.setFansUserId(userId);
            param.setDelFlag(DeleteEnum.EXIST.getValue());
            List<SysUserCare> userCareList = sysUserCareMapper.selectSysUserCareList(param);
            if (DcListUtils.isNotEmpty(userCareList)){
                SysUserCare userCare = userCareList.get(0);
                userCare.setMutualAttentionFlag(0);
                userCare.setUpdateUserId(loginUser.getUserId());
                userCare.setUpdateBy(loginUser.getUsername());
                userCare.setUpdateTime(DateUtils.getNowDate());
                sysUserCareMapper.updateSysUserCare(userCare);
            }
        }

        return 1;
    }
}
