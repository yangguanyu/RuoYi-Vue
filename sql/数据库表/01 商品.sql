-- 商品主表
CREATE TABLE `tfp_product` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  
  `commodities_type` int(1) COMMENT '商品归属 0-国内 1-海外',
  `parent_area_id` bigint(20) COMMENT '业务地区(1级) 能够选多个地区 下拉',
  `parent_area_name` varchar(50) COMMENT '业务地区(1级) 能够选多个地区 下拉',
  `area_id` bigint(20) COMMENT '业务地区(2级) 能够选多个地区 下拉',
  `area_name` varchar(50) COMMENT '业务地区(2级) 能够选多个地区 下拉',
  `city_id` bigint(20) COMMENT '业务地区(3级) 能够选多个地区 下拉',
  `city_name` varchar(50) COMMENT '业务地区(3级) 能够选多个地区 下拉',
  
  `category_id` bigint(20) COMMENT '服务类型id 散拼 单团 单项 下拉（怎么和咱们的全国散，一家一团对应）',
  `category_name` varchar(50) COMMENT '服务类型名称 散拼 单团 单项 下拉（怎么和咱们的全国散，一家一团对应）',
  
  `line_type_code` varchar(50) COMMENT '线路类型 出发地参团 目的地参团 字典code',
  `line_type_name` varchar(50) COMMENT '线路类型 出发地参团 目的地参团 字典name',
  `product_name` varchar(255) COMMENT '商品名称',
  `product_no` varchar(50) COMMENT '商品编号',
  `supplier_name` varchar(50) COMMENT '供应商名称',
  `supplier_id` bigint(20) COMMENT '供应商id',
  
  `age` int(4) COMMENT '儿童标准',
  `children_age_limit_min` int(4) COMMENT '儿童标准-年龄-最小值',
  `children_age_limit_max` int(4) COMMENT '儿童标准-年龄-最大值',

  `go_place_id` bigint(20) COMMENT '出发城市id',
  `go_place_name` varchar(50) COMMENT '出发城市名称',

  `back_place_id` bigint(20) COMMENT '返回城市id',
  `back_place_name` varchar(50) COMMENT '返回城市名称',
  
  `go_traffic_code` varchar(50) COMMENT '字典 去程交通 飞机 火车 轮船 汽车 高铁 其它',
  `go_traffic_name` varchar(50) COMMENT '去程交通 飞机 火车 轮船 汽车 高铁 其它',
  `back_traffic_code` varchar(50) COMMENT 'dict 回程交通',
  `back_traffic_name` varchar(50) COMMENT '回程交通',
  
  `day_num` int(4) COMMENT '行程天数 几天',
  `night_num` int(4) COMMENT '行程天数 几晚',
  
  `is_insurance` int(4) COMMENT '是否包含保险 单选框 0-否 1-是',
  `insurance_company` varchar(50) COMMENT '保险公司',
  `insurance_type_code` varchar(50) COMMENT '险种code：人身意外险 旅游团队先',
  `insurance_type_name` varchar(50) COMMENT '险种名称：人身意外险 旅游团队先',

  `item_status` int(1) COMMENT '商品状态 0-待审核 1-审核通过 2-审核不通过 3-草稿',
  
  `air_company_code` varchar(50) COMMENT '航空公司code 下拉 需要爬数据',
  `air_company_name` varchar(50) COMMENT '航空公司名称 下拉 需要爬数据',

  `purchase_flag` int(1) COMMENT '是否有购物 0-否 1-是',
  `self_funded_project_flag` int(1) COMMENT '是否自费项目 0-否 1-是',

  `early_deadline_day` int(4) COMMENT '提前截止天数',

  `confirmation_type` int(4) COMMENT '资源确认方式 1：二次确认  2：即时确认',

  `low_price_air_ticket_service_flag` int(1) COMMENT '是否选择低价机票服务 0-否 1-是',
  `air_ticket_price_query_complete_flag` int(1) COMMENT '机票价格是否已经查询完成 0-否 1-是',
  `submit_air_ticket_price_query_flag` int(1) COMMENT '提交后机票价格是否已经查询 0-否 1-是',
  `sales_status` int(1) DEFAULT NULL COMMENT '产品上下架状态字段 0-新增待上架 1-已上架 2-下架待审批 3-已下架',
  `sales_date` datetime DEFAULT NULL COMMENT '上架日期',
  `air_ticket_price_query_date` datetime DEFAULT NULL COMMENT '机票价格查询时间',

  `score` int(11) COMMENT '分值，定时任务10分钟',

  `product_describe` varchar(500) COMMENT '商品描述',
  PRIMARY KEY (`id`)
) COMMENT='商品主表';

-- 商品辅表
CREATE TABLE `tfp_product_sub` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  
  `product_id` bigint(20) COMMENT '主表id',
  `route_highlight` varchar(500) COMMENT '线路亮点',
  `age_intro` varchar(500) COMMENT '儿童标准说明',
  `traffic_instructions` varchar(500) COMMENT '交通说明',
  `remark` varchar(500) COMMENT '备注',
  `insurance_content` varchar(500) COMMENT '保险具体内容',
  PRIMARY KEY (`id`)
) COMMENT='商品辅表';

-- 商品班期表
CREATE TABLE `tfp_product_departure_date` (
   `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',

   `product_id` bigint(20) COMMENT '主表id',
   `departure_date_start` datetime NOT NULL COMMENT '班期开始',
   `departure_date_end` datetime NOT NULL COMMENT '班期截止',
   PRIMARY KEY (`id`)
) COMMENT='商品班期表';

-- 购物店数量表
CREATE TABLE `tfp_purchase_shop_number` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',

  `product_id` bigint(20) COMMENT '商品表id',
  `area_name` varchar(60) COMMENT '地点名称',
  `purchase_place_name` varchar(50) COMMENT '购物场所名称',
  `main_product_info` varchar(300) COMMENT '主要商品信息',
  `max_residence_time` varchar(50) COMMENT '最长停留时间',
  `remark` varchar(100) COMMENT '其它说明',
  PRIMARY KEY (`id`)
) COMMENT='购物店数量表';


-- 自费项目
CREATE TABLE `tfp_self_funded_project` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  
  `product_id` bigint(20) COMMENT '商品表id',
  `area_name` varchar(50) COMMENT '地点名称',
  `name_and_content` varchar(300) COMMENT '项目名称和内容',
  `price` decimal(20,2) COMMENT '费用（元）',
  `duration` int COMMENT '项目时长（分钟）',
  `remark` varchar(100) COMMENT '其它说明',
 
  PRIMARY KEY (`id`)
) COMMENT='自费项目';

-- 每日价格表
CREATE TABLE `tfp_every_day_price` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `product_id` bigint(20) COMMENT '商品id',

  `type` int(2) COMMENT '类型 0-标准 1-特殊(特殊日期)',
  `special_date` datetime COMMENT '特殊日期',

  `expected_person_number` varchar(50) COMMENT '计划出团人数',
  `expected_number` int COMMENT '预收数',
  `min_person_number` int COMMENT '最低成团人数',

  `adult_cost_price` decimal(20,2) COMMENT '成人成本价',
  `children_cost_price` decimal(20,2) COMMENT '儿童成本价',
  `adult_retail_price` decimal(20,2) COMMENT '建议成人零售价',
  `children_retail_price` decimal(20,2) COMMENT '建议儿童零售价',
  `single_supplement_cost_price` decimal(20,2) COMMENT '单房差成本价',
  `deposit` decimal(20,2) COMMENT '订金',

  `self_prepared_signature_cost_price` decimal(20,2) COMMENT '自备签成本价',
  `upgrade_cost_price` decimal(20,2) COMMENT '升舱成本价',
  `refusal_cost_price` decimal(20,2) COMMENT '拒签成本价',
  `visa_fee_cost_price` decimal(20,2) COMMENT '签证费成本价',
  `special_addition_cost_price` decimal(20,2) COMMENT '特殊加项成本价',
  `special_deductions_cost_price` decimal(20,2) COMMENT '特殊减项成本价',
  `student_cost_price` decimal(20,2) COMMENT '学生成本价',
  `old_people_cost_price` decimal(20,2) COMMENT '老年人成本价',

  PRIMARY KEY (`id`)
) COMMENT='每日价格表';

-- 供应商信息表
CREATE TABLE `tfp_supplier` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  
  `supplier_name` varchar(50) COMMENT '供应商名称',
  `unified_identification_code` varchar(30) COMMENT '统一识别编码',
  `supplier_address` varchar(100) COMMENT '供应商地址',
  `province_id` bigint(20) COMMENT '省的id',
  `province_name` varchar(50) COMMENT '省的名称',
  `urban_id` bigint(20) COMMENT '市的id',
  `urban_name` varchar(50) COMMENT '市的名称',
  `area_id` bigint(20) COMMENT '区的id',
  `area_name` varchar(50) COMMENT '区的名称',
  `introduce` varchar(100) COMMENT '介绍',
  `phone_number` varchar(20) COMMENT '咨询电话',
  PRIMARY KEY (`id`)
) COMMENT='供应商信息表';

-- 标签表
CREATE TABLE `tfp_label` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  
  `label_code` varchar(30) COMMENT '标签code',
  `label_name` varchar(50) COMMENT '标签名称',
  `label_type` tinyint COMMENT '标签类型 1商品 2订单 (目前只有商品用)',
  `remark` varchar(200) COMMENT '标签备注',
  PRIMARY KEY (`id`)
) COMMENT='标签表';

-- 合作分公司表
CREATE TABLE `tfp_cooperative_branch_company` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(50) NOT NULL COMMENT '分公司名称',
  PRIMARY KEY (`id`)
) COMMENT='合作分公司表';

-- 商品、合作分公司中间表
CREATE TABLE `tfp_product_cbc` (
    `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
    `product_id` bigint(20) DEFAULT NULL COMMENT '商品id',
    `cbc_id` bigint(20) DEFAULT NULL COMMENT '合作分公司id',
    PRIMARY KEY (`id`)
) COMMENT='商品、合作分公司中间表';


-- 地区导航表
CREATE TABLE `tfp_area_navigation` (
     `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
     `parent_id` bigint(20) COMMENT '父级id',

     `area_id` bigint(20) COMMENT '区域id',
     `name` varchar(50) COMMENT '导航/区域名称',
     `image` varchar(100) COMMENT '图片',
     `type` int(1) COMMENT '类型 0-导航 1-区域',
     `sort` int(11) COMMENT '排序',
     PRIMARY KEY (`id`)
) COMMENT='地区导航表';

-- 地区热门推荐表
CREATE TABLE `sys_area_hot` (
    `id` bigint(20) NOT NULL AUTO_INCREMENT,
    `area_id` bigint(20) DEFAULT NULL COMMENT '区域id',
    `area_name` varchar(40) COLLATE utf8_bin DEFAULT NULL COMMENT '区域名称',
    `parent_area_id` bigint(20) DEFAULT NULL COMMENT '区域父级id',
    `parent_area_name` varchar(40) COLLATE utf8_bin DEFAULT NULL COMMENT '区域父级名称',
    `pinyin` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '拼音',
    `country_type` tinyint(4) DEFAULT NULL COMMENT '国家类型 0-中国 1-其他',
    `sort` int(11) DEFAULT NULL COMMENT '排序',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='地区热门推荐表';

-- 二维码表
CREATE TABLE `tfp_qr_code` (
      `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
      `type` varchar(50) NOT NULL COMMENT '二维码type',
      `page` varchar(100) NOT NULL COMMENT 'page',
      `business_type` varchar(50) NOT NULL COMMENT '业务类型',
      `business_sub_ype` varchar(50) NOT NULL COMMENT '业务子类型',
      PRIMARY KEY (`id`)
) COMMENT='二维码表';

-- 二维码业务数据表
CREATE TABLE `tfp_qr_code_info` (
       `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
       `type` varchar(50) NOT NULL COMMENT '二维码type',
       `share_user_id` bigint(20) NOT NULL COMMENT '分享用户id',
       `business_id` bigint(20) NOT NULL COMMENT '商品id',
       `other_param` varchar(2000) COMMENT '其他参数序列化',
       PRIMARY KEY (`id`)
) COMMENT='二维码业务数据表';

-- 用户分享日志表
CREATE TABLE `tfp_user_share_log` (
        `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
        `user_id` bigint(20) NOT NULL COMMENT '分享用户id',
        `type` varchar(50) NOT NULL COMMENT '类型 0-商品 1-搭子',
        `business_id` bigint(20) NOT NULL COMMENT '对应业务id',
        PRIMARY KEY (`id`)
) COMMENT='用户分享日志表';

-- 附件表
CREATE TABLE `tfp_attachment` (
       `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
       `business_id` bigint(20) NOT NULL COMMENT '业务id',
       `business_type` varchar(50) NOT NULL COMMENT '业务类型',
       `business_sub_type` varchar(50) NOT NULL COMMENT '业务子类型',
       `name` varchar(100) NOT NULL COMMENT '附件名称',
       `url` varchar(150) NOT NULL COMMENT '附件Url',
       PRIMARY KEY (`id`)
) COMMENT='附件表';

-- 商品想去表
CREATE TABLE `tfp_product_want_to_go` (
      `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
      `product_id` bigint(20) NOT NULL COMMENT '商品id',
      `user_id` bigint(20) NOT NULL COMMENT '用户id',
      PRIMARY KEY (`id`)
) COMMENT='商品想去表';