package com.ruoyi.service.vo;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 经停信息对象 tfp_flight_stop_info
 * 
 * @author ruoyi
 * @date 2024-04-10
 */
@Data
public class TfpFlightStopInfoVo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 航班信息表id */
    private Long flightId;

    /** 城市三字码 */
    private String city;

    /** 城市名称 */
    private String cityName;

    /** 城市类型 */
    private String cityType;

    /** 起飞时间 */
    private String depTime;

    /** 降落时间 */
    private String arrTime;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    private Long createUserId;

    /** 更新者id */
    private Long updateUserId;
}
