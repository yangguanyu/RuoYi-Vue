package com.ruoyi.web.controller.service.form.partner;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.web.controller.service.form.attachment.AttachmentForm;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 找搭子对象 tfp_find_partner
 *
 * @author ruoyi
 * @date 2023-11-01
 */
@Data
@ApiModel("FindPartnerUpdForm-后台-审核找搭子")
public class FindPartnerUpdForm
{

    /** 主键 */
    @ApiModelProperty(value = "id")
    @NotNull(message = "请传入ID")
    private Long id;
    @ApiModelProperty(value = "找搭子发布和撤销等状态 0-未发布 1-审核通过 2-审核未通过 3-已发布待审核 4-因疑似非法而下架")
    @NotNull(message = "请传入状态")
    private Integer status;

    @ApiModelProperty(value = "审核不通过原因")
    private String reason;


}
