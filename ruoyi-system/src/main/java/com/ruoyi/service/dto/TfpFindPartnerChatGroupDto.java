package com.ruoyi.service.dto;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.List;

/**
 * 找搭子聊天群组对象 tfp_find_partner_chat_group
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@Data
public class TfpFindPartnerChatGroupDto extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 聊天群组名称 */
    private String name;

    /** 找搭子id */
    private List<Long> findPartnerIdList;

    private boolean findPartnerTitleFlag;

    /** 找搭子名称 */
    private String findPartnerTitle;

    /** 限制人数 */
    private Integer limitPersonNumber;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    private Long createUserId;

    /** 更新者id */
    private Long updateUserId;
}
