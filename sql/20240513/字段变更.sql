ALTER TABLE tfp_every_day_price
    ADD `starting_point_price` decimal(20,2) COMMENT '起点价格' AFTER old_people_cost_price;

ALTER TABLE tfp_product_departure_date
    ADD `adult_retail_price` decimal(20,2) COMMENT '成人零售价' AFTER departure_date_end;

UPDATE tfp_product_departure_date d SET d.adult_retail_price=(
    SELECT adult_retail_price FROM tfp_every_day_price p WHERE p.product_id=d.product_id AND p.type=0
) WHERE d.adult_retail_price IS NULL;

UPDATE tfp_every_day_price SET starting_point_price=adult_retail_price WHERE starting_point_price IS NULL;

SELECT d.adult_retail_price,d.* FROM tfp_product_departure_date d;
SELECT p.starting_point_price,p.* FROM tfp_every_day_price p;