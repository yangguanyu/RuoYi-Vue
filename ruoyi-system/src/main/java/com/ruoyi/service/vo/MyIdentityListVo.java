package com.ruoyi.service.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("MyIdentityListVo")
public class MyIdentityListVo {
    @ApiModelProperty("身份id")
    private Long id;
    @ApiModelProperty("身份名称")
    private String identityName;
}
