package com.ruoyi.service.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import com.ruoyi.service.domain.TfpProductDepartureDate;
import com.ruoyi.service.dto.DepartureDateDto;
import com.ruoyi.service.vo.JudgeDateTravelFlagVo;

/**
 * 商品班期Service接口
 * 
 * @author ruoyi
 * @date 2024-04-17
 */
public interface ITfpProductDepartureDateService 
{
    /**
     * 查询商品班期
     * 
     * @param id 商品班期主键
     * @return 商品班期
     */
    public TfpProductDepartureDate selectTfpProductDepartureDateById(Long id);

    /**
     * 查询商品班期列表
     * 
     * @param tfpProductDepartureDate 商品班期
     * @return 商品班期集合
     */
    public List<TfpProductDepartureDate> selectTfpProductDepartureDateList(TfpProductDepartureDate tfpProductDepartureDate);

    /**
     * 新增商品班期
     * 
     * @param tfpProductDepartureDate 商品班期
     * @return 结果
     */
    public int insertTfpProductDepartureDate(TfpProductDepartureDate tfpProductDepartureDate);

    /**
     * 修改商品班期
     * 
     * @param tfpProductDepartureDate 商品班期
     * @return 结果
     */
    public int updateTfpProductDepartureDate(TfpProductDepartureDate tfpProductDepartureDate);

    /**
     * 批量删除商品班期
     * 
     * @param ids 需要删除的商品班期主键集合
     * @return 结果
     */
    public int deleteTfpProductDepartureDateByIds(Long[] ids);

    /**
     * 删除商品班期信息
     * 
     * @param id 商品班期主键
     * @return 结果
     */
    public int deleteTfpProductDepartureDateById(Long id);

    void deleteByProductId(Long productId);

    void deleteTfpProductDepartureDateByProductIds(List<Long> productIdList);

    void saveDepartureDateList(Long productId, List<DepartureDateDto> departureDateList);

    List<TfpProductDepartureDate> selectTfpProductDepartureDateByProductIdList(List<Long> productIdList);

    BigDecimal getAdultRetailPriceByProductIdAndDate(Long productId, Date date);

    JudgeDateTravelFlagVo judgeDateTravelFlag(Date judgeDate, List<TfpProductDepartureDate> tfpProductDepartureDateList);
}
