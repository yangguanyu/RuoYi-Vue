package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Map;

@Data
@ApiModel(value = "GetQrCodeDto", description = "获取二维码")
public class GetQrCodeDto {
    @ApiModelProperty("二维码类型")
    private String type;
    @ApiModelProperty("业务id")
    private Long businessId;
    @ApiModelProperty("其他参数")
    private OtherParamDto otherParam;
}
