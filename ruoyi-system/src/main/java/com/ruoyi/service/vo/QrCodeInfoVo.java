package com.ruoyi.service.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "QrCodeInfoVo")
public class QrCodeInfoVo {
    @ApiModelProperty("业务id")
    private Long businessId;
    @ApiModelProperty("分享用户id")
    private Long shareUserId;
    @ApiModelProperty("其他参数")
    private OtherParamVo otherParam;
}
