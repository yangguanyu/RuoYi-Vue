package com.ruoyi.service.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

@Data
@ApiModel("PartnerGroupVO-搭子按照日期分组")
public class PartnerGroupVO {

    //格式类似于06.06
    private String groupDay;

    private List<TfpFindPartnerVO> partnerList;

    // [
    // {groupDay:09.09,partnerList:[{},{},{}]},
    // {groupDay:09.10,partnerList:[{},{},{}]},
    // {groupDay:09.11,partnerList:[{},{},{}]}
    // ]
}
