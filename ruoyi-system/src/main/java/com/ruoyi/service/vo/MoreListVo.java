package com.ruoyi.service.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel
public class MoreListVo {
    @ApiModelProperty("产品名称")
    private String productName;
    @ApiModelProperty("群组集合")
    private List<MoreListGroupVo> moreGroupList;
}
