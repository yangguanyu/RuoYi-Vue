package com.ruoyi.service.service;

import java.util.List;
import com.ruoyi.service.domain.TfpProductSub;

/**
 * 商品辅Service接口
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public interface ITfpProductSubService 
{
    /**
     * 查询商品辅
     * 
     * @param id 商品辅主键
     * @return 商品辅
     */
    public TfpProductSub selectTfpProductSubById(Long id);

    /**
     * 查询商品辅列表
     * 
     * @param tfpProductSub 商品辅
     * @return 商品辅集合
     */
    public List<TfpProductSub> selectTfpProductSubList(TfpProductSub tfpProductSub);

    /**
     * 新增商品辅
     * 
     * @param tfpProductSub 商品辅
     * @return 结果
     */
    public int insertTfpProductSub(TfpProductSub tfpProductSub);

    /**
     * 修改商品辅
     * 
     * @param tfpProductSub 商品辅
     * @return 结果
     */
    public int updateTfpProductSub(TfpProductSub tfpProductSub);

    /**
     * 批量删除商品辅
     * 
     * @param ids 需要删除的商品辅主键集合
     * @return 结果
     */
    public int deleteTfpProductSubByIds(Long[] ids);

    /**
     * 删除商品辅信息
     * 
     * @param id 商品辅主键
     * @return 结果
     */
    public int deleteTfpProductSubById(Long id);

    TfpProductSub selectTfpProductSubByProductId(Long productId);

    void deleteTfpProductSubByProductIds(List<Long> productIdList);
}
