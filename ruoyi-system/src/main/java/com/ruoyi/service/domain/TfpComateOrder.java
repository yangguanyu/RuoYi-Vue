package com.ruoyi.service.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 同行人和订单关联对象 tfp_comate_order
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public class TfpComateOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 订单ID */
    @Excel(name = "订单ID")
    private Long orderId;

    /** 订单ID */
    @Excel(name = "订单编码")
    private String orderNo;

    /** 同行人表主键 */
    @Excel(name = "同行人表主键")
    private Long mateId;

    /** 同行人姓名 */
    @Excel(name = "同行人姓名")
    private String mateName;

    /** 证件类型 1.身份证 2.台胞证 3.港澳通行证 4.护照 5.士兵证 */
    @Excel(name = "证件类型 1.身份证 2.台胞证 3.港澳通行证 4.护照 5.士兵证")
    private Integer mateCardType;

    /** 证件号码 */
    @Excel(name = "证件号码")
    private String mateCardNum;

    /** 手机号码 */
    @Excel(name = "手机号码")
    private String matePhone;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    @Excel(name = "创建者id")
    private Long createUserId;

    /** 更新者id */
    @Excel(name = "更新者id")
    private Long updateUserId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }
    public void setOrderId(Long orderId) 
    {
        this.orderId = orderId;
    }

    public Long getOrderId() 
    {
        return orderId;
    }
    public void setMateId(Long mateId)
    {
        this.mateId = mateId;
    }

    public Long getMateId()
    {
        return mateId;
    }

    public String getMateName() {
        return mateName;
    }

    public void setMateName(String mateName) {
        this.mateName = mateName;
    }

    public Integer getMateCardType() {
        return mateCardType;
    }

    public void setMateCardType(Integer mateCardType) {
        this.mateCardType = mateCardType;
    }

    public String getMateCardNum() {
        return mateCardNum;
    }

    public void setMateCardNum(String mateCardNum) {
        this.mateCardNum = mateCardNum;
    }

    public String getMatePhone() {
        return matePhone;
    }

    public void setMatePhone(String matePhone) {
        this.matePhone = matePhone;
    }

    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }
    public void setUpdateUserId(Long updateUserId) 
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId() 
    {
        return updateUserId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("orderNo", getOrderNo())
            .append("orderId", getOrderId())
            .append("mateId", getMateId())
            .append("mateName", getMateName())
            .append("mateCardType", getMateCardType())
            .append("mateCardNum", getMateCardNum())
            .append("matePhone", getMatePhone())
            .append("remark", getRemark())
            .append("delFlag", getDelFlag())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
