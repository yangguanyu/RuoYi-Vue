package com.ruoyi.service.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 同行人-独立出行对象 tfp_comate
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public class TfpComate extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 主用户id */
    @Excel(name = "主用户id")
    private Long mainUserId;

    /** 同行人姓名 */
    @Excel(name = "同行人姓名")
    private String mateName;

    /** 证件类型 1.身份证 2.台胞证 3.港澳通行证 4.护照 5.士兵证 */
    @Excel(name = "证件类型 1.身份证 2.台胞证 3.港澳通行证 4.护照 5.士兵证")
    private Integer mateCardType;

    /** 证件号码 */
    @Excel(name = "证件号码")
    private String mateCardNum;

    /** 手机号码 */
    @Excel(name = "手机号码")
    private String matePhone;

    /** 此人的user_id(备用) */
    @Excel(name = "此人的user_id(备用)")
    private Long viceUserId;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    @Excel(name = "创建者id")
    private Long createUserId;

    /** 更新者id */
    @Excel(name = "更新者id")
    private Long updateUserId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setMainUserId(Long mainUserId) 
    {
        this.mainUserId = mainUserId;
    }

    public Long getMainUserId() 
    {
        return mainUserId;
    }
    public void setMateName(String mateName) 
    {
        this.mateName = mateName;
    }

    public String getMateName() 
    {
        return mateName;
    }
    public void setMateCardType(Integer mateCardType)
    {
        this.mateCardType = mateCardType;
    }

    public Integer getMateCardType()
    {
        return mateCardType;
    }
    public void setMateCardNum(String mateCardNum) 
    {
        this.mateCardNum = mateCardNum;
    }

    public String getMateCardNum() 
    {
        return mateCardNum;
    }
    public void setMatePhone(String matePhone) 
    {
        this.matePhone = matePhone;
    }

    public String getMatePhone() 
    {
        return matePhone;
    }
    public void setViceUserId(Long viceUserId) 
    {
        this.viceUserId = viceUserId;
    }

    public Long getViceUserId() 
    {
        return viceUserId;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }
    public void setUpdateUserId(Long updateUserId) 
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId() 
    {
        return updateUserId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("mainUserId", getMainUserId())
            .append("mateName", getMateName())
            .append("mateCardType", getMateCardType())
            .append("mateCardNum", getMateCardNum())
            .append("matePhone", getMatePhone())
            .append("viceUserId", getViceUserId())
            .append("remark", getRemark())
            .append("delFlag", getDelFlag())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
