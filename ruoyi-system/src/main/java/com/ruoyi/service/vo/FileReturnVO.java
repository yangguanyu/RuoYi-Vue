package com.ruoyi.service.vo;

import lombok.Data;

@Data
public class FileReturnVO {
    // 是否成功
    private boolean success;
    // 编码
    private Integer code;
    // 信息
    private String msg;
    // 文件vo
    private FileVO data;
}
