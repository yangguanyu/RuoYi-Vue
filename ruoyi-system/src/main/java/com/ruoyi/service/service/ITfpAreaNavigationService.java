package com.ruoyi.service.service;

import java.util.List;
import com.ruoyi.service.domain.TfpAreaNavigation;
import com.ruoyi.service.vo.DestinationAreaVo;

/**
 * 地区导航Service接口
 * 
 * @author ruoyi
 * @date 2023-12-25
 */
public interface ITfpAreaNavigationService 
{
    /**
     * 查询地区导航
     * 
     * @param id 地区导航主键
     * @return 地区导航
     */
    public TfpAreaNavigation selectTfpAreaNavigationById(Long id);

    /**
     * 查询地区导航列表
     * 
     * @param tfpAreaNavigation 地区导航
     * @return 地区导航集合
     */
    public List<TfpAreaNavigation> selectTfpAreaNavigationList(TfpAreaNavigation tfpAreaNavigation);

    /**
     * 新增地区导航
     * 
     * @param tfpAreaNavigation 地区导航
     * @return 结果
     */
    public int insertTfpAreaNavigation(TfpAreaNavigation tfpAreaNavigation);

    /**
     * 修改地区导航
     * 
     * @param tfpAreaNavigation 地区导航
     * @return 结果
     */
    public int updateTfpAreaNavigation(TfpAreaNavigation tfpAreaNavigation);

    /**
     * 批量删除地区导航
     * 
     * @param ids 需要删除的地区导航主键集合
     * @return 结果
     */
    public int deleteTfpAreaNavigationByIds(Long[] ids);

    /**
     * 删除地区导航信息
     * 
     * @param id 地区导航主键
     * @return 结果
     */
    public int deleteTfpAreaNavigationById(Long id);

    /**
     * 获取目的地
     * @return
     */
    List<DestinationAreaVo> getDestinationArea();

    /**
     * 查询目的地区域名称列表
     * @param key
     * @return
     */
    List<String> searchDestinationAreaNameByKey(String key);
}
