package com.ruoyi.service.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.common.core.domain.entity.TfpAttachment;
import com.ruoyi.service.dto.SaveSystemImageDto;
import com.ruoyi.service.dto.UserImageDto;

/**
 * 附件Service接口
 *
 * @author ruoyi
 * @date 2023-12-25
 */
public interface ITfpAttachmentService
{
    /**
     * 查询附件
     *
     * @param id 附件主键
     * @return 附件
     */
    public TfpAttachment selectTfpAttachmentById(Long id);

    /**
     * 查询附件列表
     *
     * @param tfpAttachment 附件
     * @return 附件集合
     */
    public List<TfpAttachment> selectTfpAttachmentList(TfpAttachment tfpAttachment);

    /**
     * 新增附件
     *
     * @param tfpAttachment 附件
     * @return 结果
     */
    public int insertTfpAttachment(TfpAttachment tfpAttachment);

    /**
     * 修改附件
     *
     * @param tfpAttachment 附件
     * @return 结果
     */
    public int updateTfpAttachment(TfpAttachment tfpAttachment);

    /**
     * 批量删除附件
     *
     * @param ids 需要删除的附件主键集合
     * @return 结果
     */
    public int deleteTfpAttachmentByIds(Long[] ids);

    /**
     * 删除附件信息
     *
     * @param id 附件主键
     * @return 结果
     */
    public int deleteTfpAttachmentById(Long id);

    List<TfpAttachment> selectListByBusinessId(Long businessId, String businessType, String businessSubType);

    List<TfpAttachment> selectList(List<Long> businessIdList, String businessType, String businessSubType);

    /**
     * 保存用户图片
     * @param userId
     * @param userImageList
     */
    void saveUserImage(Long userId, List<UserImageDto> userImageList);

    /**
     * 获取用户头像图片map
     * @param userIdList
     * @return
     */
    Map<Long, String> getUserHeadImageMap(List<Long> userIdList);

    void deleteTfpAttachmentByBusinessId(Long businessId, String businessType, String businessSubType);

    void saveImageList(Long businessId, String name, List<String> urlList, String businessType, String businessSubType);

    int saveSystemImage(List<SaveSystemImageDto> saveSystemImageDtoList);
}
