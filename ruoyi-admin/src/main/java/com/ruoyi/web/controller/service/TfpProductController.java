package com.ruoyi.web.controller.service;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.enuma.SaleStatusEnum;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.bean.CustomizeBeanCopier;
import com.ruoyi.service.dto.OperationShelivesDto;
import com.ruoyi.service.dto.TfpProductDto;
import com.ruoyi.service.vo.*;
import com.ruoyi.web.controller.service.form.product.RecommendForm;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.compress.utils.Lists;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.TfpProduct;
import com.ruoyi.service.service.ITfpProductService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商品主表Controller
 *
 * @author ruoyi
 * @date 2023-11-01
 */
@RestController
@RequestMapping("/service/product")
@ApiOperation("商品")
public class TfpProductController extends BaseController
{
    @Autowired
    private ITfpProductService tfpProductService;

    /**
     * 查询商品主表列表
     */
    @PreAuthorize("@ss.hasPermi('service:product:list')")
    @GetMapping("/list")
    public TableDataInfo list(TfpProduct tfpProduct)
    {
        startPage();
        List<TfpProductVo> list = tfpProductService.selectTfpProductVoList(tfpProduct);
        return getDataTable(list);
    }

    /**
     * 导出商品主表列表
     */
    @PreAuthorize("@ss.hasPermi('service:product:export')")
    @Log(title = "商品主表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TfpProduct tfpProduct)
    {
        List<TfpProduct> list = tfpProductService.selectTfpProductList(tfpProduct);
        ExcelUtil<TfpProduct> util = new ExcelUtil<TfpProduct>(TfpProduct.class);
        util.exportExcel(response, list, "商品主表数据");
    }

    /**
     * 获取商品主表详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:product:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tfpProductService.selectTfpProductById(id));
    }

    /**
     * 新增商品主表
     */
    @PreAuthorize("@ss.hasPermi('service:product:add')")
    @Log(title = "商品主表", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@RequestBody @Validated TfpProductDto tfpProductDto)
    {
        return toAjax(tfpProductService.insertTfpProduct(tfpProductDto));
    }

    /**
     * 修改商品主表
     */
    @PreAuthorize("@ss.hasPermi('service:product:edit')")
    @Log(title = "商品主表", businessType = BusinessType.UPDATE)
    @PostMapping("/update")
    public AjaxResult update(@RequestBody TfpProductDto tfpProductDto)
    {
        return toAjax(tfpProductService.updateTfpProduct(tfpProductDto));
    }

    /**
     * 删除商品主表
     */
    @PreAuthorize("@ss.hasPermi('service:product:remove')")
    @Log(title = "商品主表", businessType = BusinessType.DELETE)
	@DeleteMapping("/delete/{ids}")
    public R remove(@PathVariable Long[] ids)
    {
        return R.ok(tfpProductService.deleteTfpProductByIds(ids));
    }

    /**
     * 详情
     * @param id
     * @return
     */
    @PreAuthorize("@ss.hasPermi('service:product:query')")
    @GetMapping(value = "/detail/{id}")
    @ApiOperation("后台-详情")
    public R<TfpProductDetailVo> detail(@PathVariable("id") Long id)
    {
        return R.ok(tfpProductService.detail(id));
    }

    /**
     * 目的地商品信息列表
     * @param backPlaceName
     * @return
     */
    @ApiOperation("小程序-目的地商品信息列表")
    @PostMapping("/listDestinationProductSelect")
    @ResponseBody
    public TableDataInfo<ListProductVo> listDestinationProductSelect(@RequestParam("backPlaceName") String backPlaceName){
        // 业务逻辑变更 此处传参为业务区域cityId
        String cityName = backPlaceName;
        List<ListProductVo> list = tfpProductService.listDestinationProductSelect(cityName);
        return getDataTable(list);
    }

    @ApiOperation("小程序-商品详情")
    @PostMapping("/productDetail")
    @ResponseBody
    public R<ProductDetailVo> productDetail(@RequestParam("id") Long id){
        return R.ok(tfpProductService.productDetail(id, null));
    }

    @ApiOperation("小程序-获取推荐列表")
    @PostMapping("/applet/list")
    public TableDataInfo<RecommendProductVo> appletList(@RequestBody  RecommendForm form) {
        TfpProduct tfpProduct = CustomizeBeanCopier.copyProperties(form, TfpProduct.class);
        startPage();
        tfpProduct.setSalesStatus(SaleStatusEnum.ON_SHELIVES.getValue());
        tfpProduct.setTaskDateTime(DateUtils.getRecommendDate());
        tfpProduct.setSortByScore(Boolean.TRUE);
        List<TfpProductVo> list = tfpProductService.selectAppletHotRecommendList(tfpProduct);
        if(CollectionUtils.isEmpty(list)){
            return getDataTable(Lists.newArrayList());
        }else {
            TableDataInfo dataTable = getDataTable(list);
            List<TfpProductVo> rows = dataTable.getRows();
            List<RecommendProductVo> recommendProductVoList = CustomizeBeanCopier.copyPropertiesOfList(rows, RecommendProductVo.class);
            dataTable.setRows(recommendProductVoList);
            return dataTable;
        }
    }

    //此处多一个接口是为了将来和推荐列表分开
    @ApiOperation("小程序-商品搜索")
    @PostMapping("/applet/search")
    public TableDataInfo appletSearch(@RequestBody  RecommendForm form) {
        TfpProduct tfpProduct = CustomizeBeanCopier.copyProperties(form, TfpProduct.class);
        startPage();
        tfpProduct.setSalesStatus(SaleStatusEnum.ON_SHELIVES.getValue());
        tfpProduct.setTaskDateTime(DateUtils.getRecommendDate());
        tfpProduct.setSortByScore(Boolean.TRUE);
        List<TfpProductVo> list = tfpProductService.selectAppletHotRecommendList(tfpProduct);
        if(CollectionUtils.isEmpty(list)){
            return getDataTable(list);
        }else {
            TableDataInfo dataTable = getDataTable(list);
            List<RecommendProductVo> recommendProductVoList = CustomizeBeanCopier.copyPropertiesOfList(list, RecommendProductVo.class);
            dataTable.setRows(recommendProductVoList);
            return dataTable;
        }
    }

    @ApiOperation("小程序-首页轮播图")
    @PostMapping("/homePageCarouselChart")
    @ResponseBody
    public R<List<String>> homePageCarouselChart(){
        return R.ok(tfpProductService.homePageCarouselChart());
    }

    @ApiOperation("商品上架/下架")
    @PostMapping("/operationShelives")
    @ResponseBody
    public R operationShelives(@RequestBody OperationShelivesDto operationShelivesDto){
        return R.ok(tfpProductService.operationShelives(operationShelivesDto));
    }

}
