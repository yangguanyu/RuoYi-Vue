package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.TfpUserPortraitInformation;

/**
 * 用户肖像信息Mapper接口
 * 
 * @author ruoyi
 * @date 2023-11-01
 */
public interface TfpUserPortraitInformationMapper 
{
    /**
     * 查询用户肖像信息
     * 
     * @param id 用户肖像信息主键
     * @return 用户肖像信息
     */
    public TfpUserPortraitInformation selectTfpUserPortraitInformationById(Long id);

    /**
     * 查询用户肖像信息列表
     * 
     * @param tfpUserPortraitInformation 用户肖像信息
     * @return 用户肖像信息集合
     */
    public List<TfpUserPortraitInformation> selectTfpUserPortraitInformationList(TfpUserPortraitInformation tfpUserPortraitInformation);

    /**
     * 新增用户肖像信息
     * 
     * @param tfpUserPortraitInformation 用户肖像信息
     * @return 结果
     */
    public int insertTfpUserPortraitInformation(TfpUserPortraitInformation tfpUserPortraitInformation);

    /**
     * 修改用户肖像信息
     * 
     * @param tfpUserPortraitInformation 用户肖像信息
     * @return 结果
     */
    public int updateTfpUserPortraitInformation(TfpUserPortraitInformation tfpUserPortraitInformation);

    /**
     * 删除用户肖像信息
     * 
     * @param id 用户肖像信息主键
     * @return 结果
     */
    public int deleteTfpUserPortraitInformationById(Long id);

    /**
     * 批量删除用户肖像信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpUserPortraitInformationByIds(Long[] ids);
}
