package com.ruoyi.service.service;

import java.util.List;
import com.ruoyi.service.domain.TfpBank;

/**
 * 银行Service接口
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public interface ITfpBankService 
{
    /**
     * 查询银行
     * 
     * @param id 银行主键
     * @return 银行
     */
    public TfpBank selectTfpBankById(Long id);

    /**
     * 查询银行列表
     * 
     * @param tfpBank 银行
     * @return 银行集合
     */
    public List<TfpBank> selectTfpBankList(TfpBank tfpBank);

    /**
     * 新增银行
     * 
     * @param tfpBank 银行
     * @return 结果
     */
    public int insertTfpBank(TfpBank tfpBank);

    /**
     * 修改银行
     * 
     * @param tfpBank 银行
     * @return 结果
     */
    public int updateTfpBank(TfpBank tfpBank);

    /**
     * 批量删除银行
     * 
     * @param ids 需要删除的银行主键集合
     * @return 结果
     */
    public int deleteTfpBankByIds(Long[] ids);

    /**
     * 删除银行信息
     * 
     * @param id 银行主键
     * @return 结果
     */
    public int deleteTfpBankById(Long id);
}
