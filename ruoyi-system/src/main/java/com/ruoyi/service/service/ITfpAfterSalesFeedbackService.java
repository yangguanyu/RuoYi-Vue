package com.ruoyi.service.service;

import java.util.List;
import com.ruoyi.service.domain.TfpAfterSalesFeedback;

/**
 * 售后反馈Service接口
 * 
 * @author ruoyi
 * @date 2023-11-01
 */
public interface ITfpAfterSalesFeedbackService 
{
    /**
     * 查询售后反馈
     * 
     * @param id 售后反馈主键
     * @return 售后反馈
     */
    public TfpAfterSalesFeedback selectTfpAfterSalesFeedbackById(Long id);

    /**
     * 查询售后反馈列表
     * 
     * @param tfpAfterSalesFeedback 售后反馈
     * @return 售后反馈集合
     */
    public List<TfpAfterSalesFeedback> selectTfpAfterSalesFeedbackList(TfpAfterSalesFeedback tfpAfterSalesFeedback);

    /**
     * 新增售后反馈
     * 
     * @param tfpAfterSalesFeedback 售后反馈
     * @return 结果
     */
    public int insertTfpAfterSalesFeedback(TfpAfterSalesFeedback tfpAfterSalesFeedback);

    /**
     * 修改售后反馈
     * 
     * @param tfpAfterSalesFeedback 售后反馈
     * @return 结果
     */
    public int updateTfpAfterSalesFeedback(TfpAfterSalesFeedback tfpAfterSalesFeedback);

    /**
     * 批量删除售后反馈
     * 
     * @param ids 需要删除的售后反馈主键集合
     * @return 结果
     */
    public int deleteTfpAfterSalesFeedbackByIds(Long[] ids);

    /**
     * 删除售后反馈信息
     * 
     * @param id 售后反馈主键
     * @return 结果
     */
    public int deleteTfpAfterSalesFeedbackById(Long id);
}
