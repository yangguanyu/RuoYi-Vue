package com.ruoyi.service.dto;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.List;

/**
 * 聊天群组对象 tfp_chat_group
 *
 * @author ruoyi
 * @date 2023-10-31
 */
@Data
public class ChatGroupStatisticsDto{

    /** 主键 */
    private Long id;


    /** 商品id */
    private Long productId;
    //用户id
    private Long userId;

    //用户类型 0-发起者 1-已报名 2-未报名
    private Integer userType;

}
