package com.ruoyi.web.controller.service;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.service.dto.GetQrCodeDto;
import com.ruoyi.service.vo.QrCodeInfoVo;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.TfpQrCodeInfo;
import com.ruoyi.service.service.ITfpQrCodeInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 二维码业务数据Controller
 * 
 * @author ruoyi
 * @date 2024-01-19
 */
@RestController
@RequestMapping("/service/qrCodeInfo")
public class TfpQrCodeInfoController extends BaseController
{
    @Autowired
    private ITfpQrCodeInfoService tfpQrCodeInfoService;

    /**
     * 查询二维码业务数据列表
     */
    @PreAuthorize("@ss.hasPermi('service:qrCodeInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(TfpQrCodeInfo tfpQrCodeInfo)
    {
        startPage();
        List<TfpQrCodeInfo> list = tfpQrCodeInfoService.selectTfpQrCodeInfoList(tfpQrCodeInfo);
        return getDataTable(list);
    }

    /**
     * 导出二维码业务数据列表
     */
    @PreAuthorize("@ss.hasPermi('service:qrCodeInfo:export')")
    @Log(title = "二维码业务数据", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TfpQrCodeInfo tfpQrCodeInfo)
    {
        List<TfpQrCodeInfo> list = tfpQrCodeInfoService.selectTfpQrCodeInfoList(tfpQrCodeInfo);
        ExcelUtil<TfpQrCodeInfo> util = new ExcelUtil<TfpQrCodeInfo>(TfpQrCodeInfo.class);
        util.exportExcel(response, list, "二维码业务数据数据");
    }

    /**
     * 获取二维码业务数据详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:qrCodeInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tfpQrCodeInfoService.selectTfpQrCodeInfoById(id));
    }

    /**
     * 新增二维码业务数据
     */
    @PreAuthorize("@ss.hasPermi('service:qrCodeInfo:add')")
    @Log(title = "二维码业务数据", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TfpQrCodeInfo tfpQrCodeInfo)
    {
        return toAjax(tfpQrCodeInfoService.insertTfpQrCodeInfo(tfpQrCodeInfo));
    }

    /**
     * 修改二维码业务数据
     */
    @PreAuthorize("@ss.hasPermi('service:qrCodeInfo:edit')")
    @Log(title = "二维码业务数据", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TfpQrCodeInfo tfpQrCodeInfo)
    {
        return toAjax(tfpQrCodeInfoService.updateTfpQrCodeInfo(tfpQrCodeInfo));
    }

    /**
     * 删除二维码业务数据
     */
    @PreAuthorize("@ss.hasPermi('service:qrCodeInfo:remove')")
    @Log(title = "二维码业务数据", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tfpQrCodeInfoService.deleteTfpQrCodeInfoByIds(ids));
    }

    @ApiOperation("小程序-获取二维码信息")
    @PostMapping("/getQrCodeInfo")
    @ResponseBody
    public R<QrCodeInfoVo> getQrCodeInfo(@RequestParam Long qrId){
        return R.ok(tfpQrCodeInfoService.getQrCodeInfo(qrId));
    }
}
