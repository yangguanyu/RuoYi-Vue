package com.ruoyi.service.vo;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 价格数据对象 tfp_flight_price_data
 * 
 * @author ruoyi
 * @date 2024-04-10
 */
@Data
public class TfpFlightPriceDataVo extends BaseEntity
{
    /** 主键 */
    private Long id;

    /** 航班信息表id */
    private Long flightId;

    /** 舱位信息表id */
    private Long seatId;

    /** 政策信息表id */
    private Long policyId;

    /** 乘机人类型 1-成人 2-儿童 3-婴儿(暂不使用) */
    private String crewType;

    /** 票面价 */
    private Integer price;

    /** 单人结算价 （price*（1- commisionPoint%）- commisionMoney）不含税 */
    private Double settlement;

    /** 单人机建费 */
    private Integer airportTax;

    /** 单人燃油费 */
    private Integer fuelTax;

    /** 返点 0.1=0.1% */
    private Double commisionPoint;

    /** 定额 */
    private Double commisionMoney;

    /** 支付手续费 */
    private Double payFee;

    /** 是否换编码出票 1-原编码出票 2-需要换编码出票 */
    private Integer isSwitchPnr;

    /** 政策类型（BSP或B2B） */
    private String policyType;

    /** 出票效率 */
    private String ticketSpeed;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    private Long createUserId;

    /** 更新者id */
    private Long updateUserId;
}
