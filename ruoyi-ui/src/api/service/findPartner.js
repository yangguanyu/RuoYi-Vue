import request from '@/utils/request'


// 查询找搭子列表
export function listFindPartner(query) {
  return request({
    url: '/service/findPartner/list',
    method: 'get',
    params: query
  })
}

// 查询找搭子详细
export function getFindPartner(id) {
  return request({
    url: '/service/findPartner/' + id,
    method: 'get'
  })
}

// 新增找搭子
export function addFindPartner(data) {
  return request({
    url: '/service/findPartner',
    method: 'post',
    data: data
  })
}

// 新增找搭子
export function audit(data) {
  return request({
    url: '/service/findPartner/approve',
    method: 'post',
    data: data
  })
}

// 修改找搭子
export function updateFindPartner(data) {
  return request({
    url: '/service/findPartner',
    method: 'put',
    data: data
  })
}

// 删除找搭子
export function delFindPartner(id) {
  return request({
    url: '/service/findPartner/' + id,
    method: 'delete'
  })
}

// 类型下拉信息
export function typeSelect(){
  return request({
    url: '/system/dict/data/type/tfp_find_partner_type',
    method: 'get'
  })
}

export function detailFindPartner(id) {
  return request({
    url: '/service/findPartner/applet/detail/' + id,
    method: 'get',
  })
}
