package com.ruoyi.service.service;

import java.util.List;
import com.ruoyi.service.domain.TfpFindPartnerSignUp;
import com.ruoyi.service.dto.ListFindPartnerSignUpDto;
import com.ruoyi.service.vo.*;

/**
 * 找搭子Service接口
 *
 * @author ruoyi
 * @date 2023-10-31
 */
public interface ITfpFindPartnerSignUpService
{
    /**
     * 查询找搭子
     *
     * @param id 找搭子主键
     * @return 找搭子
     */
    public TfpFindPartnerSignUp selectTfpFindPartnerSignUpById(Long id);

    /**
     * 查询找搭子列表
     *
     * @param tfpFindPartnerSignUp 找搭子
     * @return 找搭子集合
     */
    public List<TfpFindPartnerSignUp> selectTfpFindPartnerSignUpList(TfpFindPartnerSignUp tfpFindPartnerSignUp);

    public List<TfpFindPartnerSignUpVo> selectTfpFindPartnerSignUpVoList(TfpFindPartnerSignUp tfpFindPartnerSignUp);

    /**
     * 新增找搭子
     *
     * @param tfpFindPartnerSignUp 找搭子
     * @return 结果
     */
    public int insertTfpFindPartnerSignUp(TfpFindPartnerSignUp tfpFindPartnerSignUp);

    /**
     * 修改找搭子
     *
     * @param tfpFindPartnerSignUp 找搭子
     * @return 结果
     */
    public int updateTfpFindPartnerSignUp(TfpFindPartnerSignUp tfpFindPartnerSignUp);

    /**
     * 批量删除找搭子
     *
     * @param ids 需要删除的找搭子主键集合
     * @return 结果
     */
    public int deleteTfpFindPartnerSignUpByIds(Long[] ids);

    /**
     * 删除找搭子信息
     *
     * @param id 找搭子主键
     * @return 结果
     */
    public int deleteTfpFindPartnerSignUpById(Long id);

    List<UserVo> signUpUserSelect();

    List<TfpFindPartnerSignUp> selectListByFindPartnerId(Long findPartnerId);

    List<TfpFindPartnerSignUp> selectListByFindPartnerIdAndAuditStatus(Long findPartnerId,Integer auditStatus);

    List<TfpFindPartnerSignUp> selectListByFindPartnerIdListAndAuditStatus(List<Long> findPartnerIdList, Integer auditStatus);

    List<FindPartnerSignUpVo> selectSignUpPersonNumberByFindPartnerIdList(List<Long> findPartnerIdList);

    List<ListFindPartnerSignUpVo> listFindPartnerSignUp(ListFindPartnerSignUpDto listFindPartnerSignUpDto);

    DetailFindPartnerSignUpVo detailFindPartnerSignUp(Long id);

    int auditPassSignUp(Long id);

    List<TfpFindPartnerSignUp> selectListBySignUpUserId(Long userId);

    int appletAdd(TfpFindPartnerSignUp tfpFindPartnerSignUp);
}
