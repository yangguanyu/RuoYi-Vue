package com.ruoyi.service.service;

import java.util.List;

import com.ruoyi.service.domain.TfpCustomerService;
import com.ruoyi.service.vo.CustomerServiceVo;

/**
 * 客服Service接口
 * 
 * @author ruoyi
 * @date 2023-11-01
 */
public interface ITfpCustomerServiceService 
{
    /**
     * 查询客服
     * 
     * @param id 客服主键
     * @return 客服
     */
    public TfpCustomerService selectTfpCustomerServiceById(Long id);

    /**
     * 查询客服列表
     * 
     * @param tfpCustomerService 客服
     * @return 客服集合
     */
    public List<TfpCustomerService> selectTfpCustomerServiceList(TfpCustomerService tfpCustomerService);

    /**
     * 新增客服
     * 
     * @param tfpCustomerService 客服
     * @return 结果
     */
    public int insertTfpCustomerService(TfpCustomerService tfpCustomerService);

    /**
     * 修改客服
     * 
     * @param tfpCustomerService 客服
     * @return 结果
     */
    public int updateTfpCustomerService(TfpCustomerService tfpCustomerService);

    /**
     * 批量删除客服
     * 
     * @param ids 需要删除的客服主键集合
     * @return 结果
     */
    public int deleteTfpCustomerServiceByIds(Long[] ids);

    /**
     * 删除客服信息
     * 
     * @param id 客服主键
     * @return 结果
     */
    public int deleteTfpCustomerServiceById(Long id);

    List<CustomerServiceVo> customerServiceSelect();
}
