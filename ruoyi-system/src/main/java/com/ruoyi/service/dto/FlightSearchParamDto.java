package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel("FlightSearchParamDto")
public class FlightSearchParamDto {
    @ApiModelProperty("航程类型 OW 单程，RT 往返，MS 多程？ 联程？ 缺口 目前仅支持单程")
    private String routeType;

    @ApiModelProperty("舱位等级，暂未启用")
    private Integer cabinClass;

    @ApiModelProperty("是否直飞：1=直飞，2=其他。默认直飞")
    private Integer directFlight;

    @ApiModelProperty("航空公司二字码，例如CA")
    private String airline;

    @ApiModelProperty("乘客类型 1.成人、2.儿童、3.成人+儿童 查询成人时passengerType=1，会返回支持儿童产品的儿童舱位及价格")
    private Integer passengerType;

    @ApiModelProperty("建议填0，1不换编可能查询无航班数据")
    private Integer needPnr;

    @ApiModelProperty("航程航线数据")
    private List<FlightSearchParamSegmentDto> segments;
}
