package com.ruoyi.service.vo;

import lombok.Data;

@Data
public class WxWatermarkVo {
    private String timestamp;
    private String appid;
}
