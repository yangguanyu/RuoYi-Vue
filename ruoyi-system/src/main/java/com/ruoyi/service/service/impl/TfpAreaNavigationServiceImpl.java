package com.ruoyi.service.service.impl;

import java.util.*;
import java.util.stream.Collectors;

import com.ruoyi.common.area.domain.model.AreaDTO;
import com.ruoyi.common.core.domain.entity.TfpAttachment;
import com.ruoyi.common.enuma.AreaNavigationTypeEnum;
import com.ruoyi.common.enuma.BusinessSubTypeEnum;
import com.ruoyi.common.enuma.BusinessTypeEnum;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.DcListUtils;
import com.ruoyi.service.service.ITfpAttachmentService;
import com.ruoyi.service.vo.DestinationAreaVo;
import com.ruoyi.system.service.ISysAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpAreaNavigationMapper;
import com.ruoyi.service.domain.TfpAreaNavigation;
import com.ruoyi.service.service.ITfpAreaNavigationService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 地区导航Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-12-25
 */
@Service
@Transactional
public class TfpAreaNavigationServiceImpl implements ITfpAreaNavigationService 
{
    @Autowired
    private TfpAreaNavigationMapper tfpAreaNavigationMapper;
    @Autowired
    private ITfpAttachmentService tfpAttachmentService;

    /**
     * 查询地区导航
     * 
     * @param id 地区导航主键
     * @return 地区导航
     */
    @Override
    public TfpAreaNavigation selectTfpAreaNavigationById(Long id)
    {
        return tfpAreaNavigationMapper.selectTfpAreaNavigationById(id);
    }

    /**
     * 查询地区导航列表
     * 
     * @param tfpAreaNavigation 地区导航
     * @return 地区导航
     */
    @Override
    public List<TfpAreaNavigation> selectTfpAreaNavigationList(TfpAreaNavigation tfpAreaNavigation)
    {
        return tfpAreaNavigationMapper.selectTfpAreaNavigationList(tfpAreaNavigation);
    }

    /**
     * 新增地区导航
     * 
     * @param tfpAreaNavigation 地区导航
     * @return 结果
     */
    @Override
    public int insertTfpAreaNavigation(TfpAreaNavigation tfpAreaNavigation)
    {
        tfpAreaNavigation.setCreateTime(DateUtils.getNowDate());
        return tfpAreaNavigationMapper.insertTfpAreaNavigation(tfpAreaNavigation);
    }

    /**
     * 修改地区导航
     * 
     * @param tfpAreaNavigation 地区导航
     * @return 结果
     */
    @Override
    public int updateTfpAreaNavigation(TfpAreaNavigation tfpAreaNavigation)
    {
        tfpAreaNavigation.setUpdateTime(DateUtils.getNowDate());
        return tfpAreaNavigationMapper.updateTfpAreaNavigation(tfpAreaNavigation);
    }

    /**
     * 批量删除地区导航
     * 
     * @param ids 需要删除的地区导航主键
     * @return 结果
     */
    @Override
    public int deleteTfpAreaNavigationByIds(Long[] ids)
    {
        return tfpAreaNavigationMapper.deleteTfpAreaNavigationByIds(ids);
    }

    /**
     * 删除地区导航信息
     * 
     * @param id 地区导航主键
     * @return 结果
     */
    @Override
    public int deleteTfpAreaNavigationById(Long id)
    {
        return tfpAreaNavigationMapper.deleteTfpAreaNavigationById(id);
    }

    /**
     * 获取目的地
     * @return
     */
    @Override
    public List<DestinationAreaVo> getDestinationArea() {
        List<DestinationAreaVo> destinationAreaVoList = new ArrayList<>();

        Map<Long, List<AreaDTO>> areaMap = new HashMap<>();

        List<TfpAreaNavigation> tfpAreaNavigationList = tfpAreaNavigationMapper.selectTfpAreaNavigationList(new TfpAreaNavigation());
        if (DcListUtils.isNotEmpty(tfpAreaNavigationList)){
            List<Long> areaImageBusinessIdList = tfpAreaNavigationList.stream()
                    .filter(tfpAreaNavigation -> Objects.equals(tfpAreaNavigation.getType(), AreaNavigationTypeEnum.AREA.getValue()))
                    .map(TfpAreaNavigation::getId).collect(Collectors.toList());

            Map<Long, List<TfpAttachment>> areaImageMap = new HashMap<>();
            if (DcListUtils.isNotEmpty(areaImageBusinessIdList)){
                List<TfpAttachment> areaImageList = tfpAttachmentService.selectList(areaImageBusinessIdList, BusinessTypeEnum.AREA.getValue(), BusinessSubTypeEnum.AREA_IMAGE.getValue());
                if (DcListUtils.isNotEmpty(areaImageList)){
                    areaImageMap = areaImageList.stream().collect(Collectors.groupingBy(TfpAttachment::getBusinessId));
                }
            }

            for (TfpAreaNavigation areaNavigation : tfpAreaNavigationList) {
                Integer type = areaNavigation.getType();
                if (Objects.equals(type, AreaNavigationTypeEnum.NAVIGATION.getValue())){
                    DestinationAreaVo destinationAreaVo = new DestinationAreaVo();
                    destinationAreaVo.setId(areaNavigation.getId());
                    destinationAreaVo.setName(areaNavigation.getName());
                    destinationAreaVo.setSort(areaNavigation.getSort());
                    destinationAreaVoList.add(destinationAreaVo);
                }

                if (Objects.equals(type, AreaNavigationTypeEnum.AREA.getValue())){
                    Long parentId = areaNavigation.getParentId();
                    List<AreaDTO> areaDTOList = areaMap.get(parentId);
                    if (areaDTOList == null){
                        areaDTOList = new ArrayList<>();
                    }

                    AreaDTO areaDTO = new AreaDTO();
                    areaDTO.setId(areaNavigation.getAreaId());
                    areaDTO.setName(areaNavigation.getName());
                    areaDTO.setShowName(areaNavigation.getName());
                    areaDTO.setSort(areaNavigation.getSort());

                    List<TfpAttachment> tfpAttachmentList = areaImageMap.get(areaNavigation.getId());
                    if (DcListUtils.isNotEmpty(tfpAttachmentList)){
                        // 现在区域图片为1张
                        areaDTO.setImage(tfpAttachmentList.get(0).getUrl());
                    }

                    areaDTOList.add(areaDTO);

                    areaMap.put(parentId, areaDTOList);
                }
            }

            if (DcListUtils.isNotEmpty(destinationAreaVoList)){
                for (DestinationAreaVo destinationAreaVo : destinationAreaVoList) {
                    destinationAreaVo.setAreaDTOList(areaMap.get(destinationAreaVo.getId()));
                }
            }
        }

        return destinationAreaVoList;
    }

    /**
     * 查询目的地区域名称列表
     * @param key
     * @return
     */
    @Override
    public List<String> searchDestinationAreaNameByKey(String key) {
        return tfpAreaNavigationMapper.searchDestinationAreaNameByKey(key);
    }
}
