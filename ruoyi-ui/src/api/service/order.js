import request from '@/utils/request'

// 查询订单列表
export function listOrder(query) {
  return request({
    url: '/service/order/orderList',
    method: 'post',
    data: query
  })
}

// 待确认 确认接口
export function confirmPayOrder(query) {
  return request({
    url: '/service/order/confirmPayOrder',
    method: 'post',
    data: query
  })
}

// 退款待审核 审核接口
export function auditRefund(query) {
  return request({
    url: '/service/refund/auditRefund',
    method: 'post',
    data: query
  })
}

// 查询订单详细
export function getOrder(id) {
  return request({
    url: '/service/order/' + id,
    method: 'get'
  })
}

// 查询订单列表详细
export function getOrderDetailInfo(id) {
  return request({
    url: `/service/order/orderDetailInfo?id=${id}`,
    method: 'post'
  })
}

// 新增订单
export function addOrder(data) {
  return request({
    url: '/service/order',
    method: 'post',
    data: data
  })
}

// 修改订单
export function updateOrder(data) {
  return request({
    url: '/service/order',
    method: 'put',
    data: data
  })
}

// 删除订单
export function delOrder(id) {
  return request({
    url: '/service/order/' + id,
    method: 'delete'
  })
}

export function supplySelect() {
  return request({
    url: '/service/order/supplySelect',
    method: 'post'
  })
}
