package com.ruoyi.service.service.impl;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.*;
import java.util.stream.Collectors;

import com.ruoyi.common.enuma.DeleteEnum;
import com.ruoyi.common.enuma.GoAndBackFlagEnum;
import com.ruoyi.common.exception.BusinessException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.DcDateUtils;
import com.ruoyi.common.utils.DcListUtils;
import com.ruoyi.service.domain.TfpProduct;
import com.ruoyi.service.domain.TfpProductDepartureDate;
import com.ruoyi.service.dto.FlightSearchParamDto;
import com.ruoyi.service.dto.FlightSearchParamSegmentDto;
import com.ruoyi.service.service.*;
import com.ruoyi.service.vo.TfpFlightPolicyVo;
import com.ruoyi.service.vo.TfpFlightPriceDataVo;
import com.ruoyi.service.vo.TfpFlightSeatVo;
import com.ruoyi.service.vo.TfpFlightVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpFlightDatePriceMapper;
import com.ruoyi.service.domain.TfpFlightDatePrice;
import org.springframework.transaction.annotation.Transactional;

/**
 * 航班日期价格Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-29
 */
@Service
@Transactional
public class TfpFlightDatePriceServiceImpl implements ITfpFlightDatePriceService 
{
    @Autowired
    private TfpFlightDatePriceMapper tfpFlightDatePriceMapper;

    @Autowired
    private IAirportCodesService airportCodesService;
    @Autowired
    private IFlightService flightService;

    /**
     * 查询航班日期价格
     * 
     * @param id 航班日期价格主键
     * @return 航班日期价格
     */
    @Override
    public TfpFlightDatePrice selectTfpFlightDatePriceById(Long id)
    {
        return tfpFlightDatePriceMapper.selectTfpFlightDatePriceById(id);
    }

    /**
     * 查询航班日期价格列表
     * 
     * @param tfpFlightDatePrice 航班日期价格
     * @return 航班日期价格
     */
    @Override
    public List<TfpFlightDatePrice> selectTfpFlightDatePriceList(TfpFlightDatePrice tfpFlightDatePrice)
    {
        return tfpFlightDatePriceMapper.selectTfpFlightDatePriceList(tfpFlightDatePrice);
    }

    /**
     * 新增航班日期价格
     * 
     * @param tfpFlightDatePrice 航班日期价格
     * @return 结果
     */
    @Override
    public int insertTfpFlightDatePrice(TfpFlightDatePrice tfpFlightDatePrice)
    {
        tfpFlightDatePrice.setCreateTime(DateUtils.getNowDate());
        return tfpFlightDatePriceMapper.insertTfpFlightDatePrice(tfpFlightDatePrice);
    }

    /**
     * 修改航班日期价格
     * 
     * @param tfpFlightDatePrice 航班日期价格
     * @return 结果
     */
    @Override
    public int updateTfpFlightDatePrice(TfpFlightDatePrice tfpFlightDatePrice)
    {
        tfpFlightDatePrice.setUpdateTime(DateUtils.getNowDate());
        return tfpFlightDatePriceMapper.updateTfpFlightDatePrice(tfpFlightDatePrice);
    }

    /**
     * 批量删除航班日期价格
     * 
     * @param ids 需要删除的航班日期价格主键
     * @return 结果
     */
    @Override
    public int deleteTfpFlightDatePriceByIds(Long[] ids)
    {
        return tfpFlightDatePriceMapper.deleteTfpFlightDatePriceByIds(ids);
    }

    @Override
    public TfpFlightDatePrice selectTfpFlightDatePriceByTimeAndName(String departureTime, String departureAirportName, String arrivalAirportName){
        return tfpFlightDatePriceMapper.selectTfpFlightDatePriceByTimeAndName(departureTime, departureAirportName, arrivalAirportName);
    }

    /**
     * 删除航班日期价格信息
     * 
     * @param id 航班日期价格主键
     * @return 结果
     */
    @Override
    public int deleteTfpFlightDatePriceById(Long id)
    {
        return tfpFlightDatePriceMapper.deleteTfpFlightDatePriceById(id);
    }

    @Override
    public void handleFlightDatePrice(TfpProduct tfpProduct, List<TfpProductDepartureDate> tfpProductDepartureDateList) throws Exception{
        List<Date> departureTimeList = this.handleProductDepartureTimeAfterNow(tfpProductDepartureDateList);
        if (DcListUtils.isNotEmpty(departureTimeList)){
            Integer dayNum = tfpProduct.getDayNum();

            // 去程：固定北京出发  抵达目的地：取目的地参团-出发城市。
            String departureAirportName = "北京";
            String arrivalAirportName = tfpProduct.getGoPlaceName();
            String departureAirportCode = airportCodesService.getThreeCode(departureAirportName);

            String arrivalAirportCode = airportCodesService.getThreeCode(arrivalAirportName);
            if (Objects.isNull(arrivalAirportCode)){
                throw new BusinessException("当前城市(" + arrivalAirportName + ")暂不支持，无法提供低价机票服务");
            }

            // 返程：取目的地参团-返回城市出发 抵达目的地固定北京
            String backArrivalAirportName = "北京";
            String backArrivalAirportCode = departureAirportCode;
            String backDepartureAirportName = tfpProduct.getBackPlaceName();
            String backDepartureAirportCode = airportCodesService.getThreeCode(backDepartureAirportName);
            if (Objects.isNull(backDepartureAirportCode)){
                throw new BusinessException("当前城市(" + backDepartureAirportName + ")暂不支持，无法提供低价机票服务");
            }

            Calendar calendar = Calendar.getInstance();
            for (Date departureDate : departureTimeList) {
                calendar.setTime(departureDate);
                calendar.add(Calendar.DAY_OF_MONTH, dayNum - 1);
                Date backDepartureDate = calendar.getTime();

                // goAndBackFlag 0:去程 1:返程
                DateFormat dcDateFormat = DcDateUtils.getDcDateFormat(DcDateUtils.yyyyMMdd);
                String departureTime = dcDateFormat.format(departureDate);
                this.handleFlightInfo(GoAndBackFlagEnum.GO.getValue(), departureTime, departureAirportCode, departureAirportName, arrivalAirportCode, arrivalAirportName);

                String backDepartureTime = dcDateFormat.format(backDepartureDate);
                this.handleFlightInfo(GoAndBackFlagEnum.BACK.getValue(), backDepartureTime, backDepartureAirportCode, backDepartureAirportName, backArrivalAirportCode, backArrivalAirportName);
            }
        }
    }

    @Override
    public List<TfpFlightDatePrice> getTfpFlightDatePriceListByGoAndBackPlaceAndDateList(Integer goAndBackFlag, TfpProduct tfpProduct, List<String> departureTimeList){
        String departureAirportName = null;
        String arrivalAirportName = null;
        if (Objects.equals(GoAndBackFlagEnum.GO.getValue(), goAndBackFlag)){
            // 去程：固定北京出发  抵达目的地：取目的地参团-出发城市。
            departureAirportName = "北京";
            arrivalAirportName = tfpProduct.getGoPlaceName();
        }

        if (Objects.equals(GoAndBackFlagEnum.BACK.getValue(), goAndBackFlag)){
            // 返程：取目的地参团-返回城市出发 抵达目的地固定北京
            departureAirportName = tfpProduct.getBackPlaceName();
            arrivalAirportName = "北京";
        }

        List<TfpFlightDatePrice> flightDatePriceList = tfpFlightDatePriceMapper.getTfpFlightDatePriceListByGoAndBackPlaceAndDateList(departureTimeList, departureAirportName, arrivalAirportName);
        return flightDatePriceList;
    }

    private void handleFlightInfo(Integer goAndBackFlag, String departureTime, String departureAirportCode, String departureAirportName, String arrivalAirportCode, String arrivalAirportName) throws Exception{
        // 查询航班数据
        FlightSearchParamDto flightSearchParamDto = new FlightSearchParamDto();
        flightSearchParamDto.setRouteType("OW");
        flightSearchParamDto.setAirline("");
        flightSearchParamDto.setPassengerType(1);
        flightSearchParamDto.setNeedPnr(0);

        List<FlightSearchParamSegmentDto> segments = new ArrayList<>();
        FlightSearchParamSegmentDto segment = new FlightSearchParamSegmentDto();
        segment.setDepartureTime(departureTime);
        segment.setDepartureAirport(departureAirportCode);
        segment.setArrivalAirport(arrivalAirportCode);
        segments.add(segment);
        flightSearchParamDto.setSegments(segments);

        DateFormat dcDateFormat = DcDateUtils.getDcDateFormat(DcDateUtils.yyyyMMddHHmm);
        Calendar calendar = Calendar.getInstance();

        Map<Integer, Long> priceFlightIdMap = new HashMap<>();
        BigDecimal price = null;

        List<TfpFlightVo> tfpFlightVoList = null;
        try {
            tfpFlightVoList = flightService.searchFlight(flightSearchParamDto);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (DcListUtils.isNotEmpty(tfpFlightVoList)){
            List<Integer> priceList = new ArrayList<>();
            for (TfpFlightVo tfpFlightVo : tfpFlightVoList) {
                // 例:2024-04-12 13:30
                String depTime = tfpFlightVo.getDepTime();
                Date depDate = dcDateFormat.parse(depTime);
                calendar.setTime(depDate);

                Integer hour = calendar.get(Calendar.HOUR_OF_DAY);
                Integer min = calendar.get(Calendar.MINUTE);

                // goAndBackFlag 0:去程 1:返程
                if (Objects.equals(0, goAndBackFlag)){
                    // 去程 去程出发时间：早5:00---9:50
                    if (hour < 5 && hour > 10){
                        continue;
                    }

                    if (hour == 9 && min > 50){
                        continue;
                    }
                } else {
                    // 返程 返程出发时间：晚18:00--22:50
                    if (hour < 18 && hour > 23){
                        continue;
                    }

                    if (hour == 22 && min > 50){
                        continue;
                    }
                }

                List<TfpFlightSeatVo> seatItems = tfpFlightVo.getSeatItems();
                for (TfpFlightSeatVo seatItem : seatItems) {
                    TfpFlightPolicyVo policys = seatItem.getPolicys();
                    List<TfpFlightPriceDataVo> priceDatas = policys.getPriceDatas();
                    for (TfpFlightPriceDataVo priceData : priceDatas) {
                        // 单程机票价=票面价+单人机建+单人燃油(成人)
                        String crewType = priceData.getCrewType();
                        if (Objects.equals(crewType, "1")){
                            // 成人
                            Integer seatPrice = priceData.getPrice();
                            // 单人机建
                            Integer airportTax = priceData.getAirportTax();
                            //单人燃油费
                            Integer fuelTax = priceData.getFuelTax();

                            Integer totalPrice = seatPrice + airportTax + fuelTax;
                            if (!priceList.contains(totalPrice)){
                                priceList.add(totalPrice);
                            }

                            priceFlightIdMap.put(totalPrice, priceData.getFlightId());
                        }
                    }
                }
            }

            if (DcListUtils.isNotEmpty(priceList)){
                priceList = priceList.stream().sorted().collect(Collectors.toList());
                int size = priceList.size();
                if (size == 1 || size == 2){
                    price = new BigDecimal(priceList.get(0));
                } else {
                    price = new BigDecimal(priceList.get(1));
                }
            }
        }

        if (Objects.nonNull(price)){
            TfpFlightDatePrice flightDatePrice = tfpFlightDatePriceMapper.selectTfpFlightDatePriceByTimeAndName(departureTime, departureAirportName, arrivalAirportName);
            if (Objects.isNull(flightDatePrice)){
                flightDatePrice = new TfpFlightDatePrice();
                flightDatePrice.setPrice(price);
                flightDatePrice.setDepartureTime(departureTime);
                flightDatePrice.setDepartureAirportCode(departureAirportCode);
                flightDatePrice.setDepartureAirportName(departureAirportName);
                flightDatePrice.setArrivalAirportCode(arrivalAirportCode);
                flightDatePrice.setArrivalAirportName(arrivalAirportName);
                flightDatePrice.setFlightId(priceFlightIdMap.get(price));

                flightDatePrice.setDelFlag(DeleteEnum.EXIST.getValue());
                flightDatePrice.setCreateUserId(1L);
                flightDatePrice.setCreateBy("admin");
                flightDatePrice.setCreateTime(DateUtils.getNowDate());
                tfpFlightDatePriceMapper.insertTfpFlightDatePrice(flightDatePrice);
            } else {
                flightDatePrice.setPrice(price);
                flightDatePrice.setFlightId(priceFlightIdMap.get(price));

                flightDatePrice.setUpdateUserId(1L);
                flightDatePrice.setUpdateBy("admin");
                flightDatePrice.setUpdateTime(DateUtils.getNowDate());
                tfpFlightDatePriceMapper.updateTfpFlightDatePrice(flightDatePrice);
            }
        }
    }

    private List<Date> handleProductDepartureTimeAfterNow(List<TfpProductDepartureDate> tfpProductDepartureDateList){
        List<Date> departureTimeList = new ArrayList<>();

        if (DcListUtils.isNotEmpty(tfpProductDepartureDateList)){
            Calendar calendar = Calendar.getInstance();
            Date now = DcDateUtils.handelDateToDayStart(new Date());
            calendar.setTime(now);
            int nowMonth = calendar.get(Calendar.MONTH);

            for (TfpProductDepartureDate tfpProductDepartureDate : tfpProductDepartureDateList) {
                Date departureDateEnd = DcDateUtils.handelDateToDayStart(tfpProductDepartureDate.getDepartureDateEnd());
                if (now.after(departureDateEnd)){
                    continue;
                }

                int length;
                Date departureDateStart = DcDateUtils.handelDateToDayStart(tfpProductDepartureDate.getDepartureDateStart());
                if (now.after(departureDateStart)) {
                    // 当前日期在团期开始和截止时间之间
                    length = new Long((departureDateEnd.getTime() - now.getTime()) / DcDateUtils.ONE_DAY).intValue() + 1;
                    calendar.setTime(now);
                } else {
                    // 当前日期在团期开始时间之前
                    length = new Long((departureDateEnd.getTime() - departureDateStart.getTime()) / DcDateUtils.ONE_DAY).intValue() + 1;
                    calendar.setTime(departureDateStart);
                }

                for (int i = 0; i < length; i++) {
                    if (i != 0){
                        calendar.add(Calendar.DAY_OF_MONTH, 1);
                    }

                    // 仅处理仅2个月的
                    int month = calendar.get(Calendar.MONTH);
                    if (month - nowMonth > 1){
                        break;
                    }

                    Date departureTime = calendar.getTime();
                    if (!departureTimeList.contains(departureTime)){
                        departureTimeList.add(departureTime);
                    }
                }
            }
        }

        return departureTimeList;
    }
}
