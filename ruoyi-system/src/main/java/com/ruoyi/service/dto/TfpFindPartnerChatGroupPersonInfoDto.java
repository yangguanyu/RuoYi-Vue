package com.ruoyi.service.dto;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.List;

/**
 * 找搭子聊天群组成员聊天信息对象 tfp_find_partner_chat_group_person_info
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@Data
public class TfpFindPartnerChatGroupPersonInfoDto extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 聊天群组id */
    private Long chatGroupId;

    private boolean chatGroupNameFlag;
    private String chatGroupName;
    private List<Long> chatGroupIdList;

    /** 聊天群组成员id */
    private Long chatGroupPersonId;

    private boolean chatGroupPersonUserNameFlag;

    /** 聊天群组成员用户Id */
    private Long chatGroupPersonUserId;
    private String chatGroupPersonUserName;
    private List<Long> chatGroupPersonUserIdList;

    /** 聊天信息 */
    private String chatInfo;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    private Long createUserId;

    /** 更新者id */
    private Long updateUserId;

}
