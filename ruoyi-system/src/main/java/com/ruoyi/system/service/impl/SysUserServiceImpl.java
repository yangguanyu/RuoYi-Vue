package com.ruoyi.system.service.impl;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.validation.Validator;

import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enuma.*;
import com.ruoyi.common.exception.BusinessException;
import com.ruoyi.common.utils.*;
import com.ruoyi.service.domain.SysIdentity;
import com.ruoyi.service.domain.SysUserCare;
import com.ruoyi.service.domain.SysUserLabel;
import com.ruoyi.common.core.domain.entity.TfpAttachment;
import com.ruoyi.service.domain.TfpComate;
import com.ruoyi.service.dto.*;
import com.ruoyi.service.service.*;
import com.ruoyi.service.vo.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.entity.SysRole;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.bean.BeanValidators;
import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.system.domain.SysPost;
import com.ruoyi.system.domain.SysUserPost;
import com.ruoyi.system.domain.SysUserRole;
import com.ruoyi.system.mapper.SysPostMapper;
import com.ruoyi.system.mapper.SysRoleMapper;
import com.ruoyi.system.mapper.SysUserMapper;
import com.ruoyi.system.mapper.SysUserPostMapper;
import com.ruoyi.system.mapper.SysUserRoleMapper;
import com.ruoyi.system.service.ISysConfigService;
import com.ruoyi.system.service.ISysUserService;

/**
 * 用户 业务层处理
 *
 * @author ruoyi
 */
@Service
public class SysUserServiceImpl implements ISysUserService
{
    private static final Logger log = LoggerFactory.getLogger(SysUserServiceImpl.class);

    @Autowired
    private SysUserMapper userMapper;

    @Autowired
    private SysRoleMapper roleMapper;

    @Autowired
    private SysPostMapper postMapper;

    @Autowired
    private SysUserRoleMapper userRoleMapper;

    @Autowired
    private SysUserPostMapper userPostMapper;

    @Autowired
    private ISysConfigService configService;

    @Autowired
    protected Validator validator;

    @Autowired
    private ITfpAttachmentService attachmentService;
    @Autowired
    private ISysUserLabelService userLabelService;
    @Autowired
    private ISysUserCareService userCareService;
    @Autowired
    private ISysUserVisitorRecordService userVisitorRecordService;
    @Autowired
    private ISysIdentityService identityService;
    @Autowired
    private ITfpComateService comateService;

    @Autowired
    private IDcTokenService tokenService;

    @Value("${default.avatar}")
    private String defaultAvatar;


    /**
     * 根据条件分页查询用户列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    @Override
    @DataScope(deptAlias = "d", userAlias = "u")
    public List<SysUser> selectUserList(SysUser user)
    {
        return userMapper.selectUserList(user);
    }

    /**
     * 根据条件分页查询已分配用户角色列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    @Override
    @DataScope(deptAlias = "d", userAlias = "u")
    public List<SysUser> selectAllocatedList(SysUser user)
    {
        return userMapper.selectAllocatedList(user);
    }

    /**
     * 根据条件分页查询未分配用户角色列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    @Override
    @DataScope(deptAlias = "d", userAlias = "u")
    public List<SysUser> selectUnallocatedList(SysUser user)
    {
        return userMapper.selectUnallocatedList(user);
    }

    /**
     * 通过用户名查询用户
     *
     * @param userName 用户名
     * @return 用户对象信息
     */
    @Override
    public SysUser selectUserByUserName(String userName)
    {
        return userMapper.selectUserByUserName(userName);
    }

    /**
     * 通过用户ID查询用户
     *
     * @param userId 用户ID
     * @return 用户对象信息
     */
    @Override
    public SysUser selectUserById(Long userId)
    {
        return userMapper.selectUserById(userId);
    }

    /**
     * 查询用户所属角色组
     *
     * @param userName 用户名
     * @return 结果
     */
    @Override
    public String selectUserRoleGroup(String userName)
    {
        List<SysRole> list = roleMapper.selectRolesByUserName(userName);
        if (CollectionUtils.isEmpty(list))
        {
            return StringUtils.EMPTY;
        }
        return list.stream().map(SysRole::getRoleName).collect(Collectors.joining(","));
    }

    /**
     * 查询用户所属岗位组
     *
     * @param userName 用户名
     * @return 结果
     */
    @Override
    public String selectUserPostGroup(String userName)
    {
        List<SysPost> list = postMapper.selectPostsByUserName(userName);
        if (CollectionUtils.isEmpty(list))
        {
            return StringUtils.EMPTY;
        }
        return list.stream().map(SysPost::getPostName).collect(Collectors.joining(","));
    }

    /**
     * 校验用户名称是否唯一
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    public boolean checkUserNameUnique(SysUser user)
    {
        Long userId = StringUtils.isNull(user.getUserId()) ? -1L : user.getUserId();
        SysUser info = userMapper.checkUserNameUnique(user.getUserName());
        if (StringUtils.isNotNull(info) && info.getUserId().longValue() != userId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验手机号码是否唯一
     *
     * @param user 用户信息
     * @return
     */
    @Override
    public boolean checkPhoneUnique(SysUser user)
    {
        Long userId = StringUtils.isNull(user.getUserId()) ? -1L : user.getUserId();
        SysUser info = userMapper.checkPhoneUnique(user.getPhonenumber());
        if (StringUtils.isNotNull(info) && info.getUserId().longValue() != userId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }
    @Override
    public SysUser getUserInfoByPhoneNumber(String mobile)
    {
        SysUser info = userMapper.checkPhoneUnique(mobile);
        return info;
    }

    /**
     * 校验email是否唯一
     *
     * @param user 用户信息
     * @return
     */
    @Override
    public boolean checkEmailUnique(SysUser user)
    {
        Long userId = StringUtils.isNull(user.getUserId()) ? -1L : user.getUserId();
        SysUser info = userMapper.checkEmailUnique(user.getEmail());
        if (StringUtils.isNotNull(info) && info.getUserId().longValue() != userId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验用户是否允许操作
     *
     * @param user 用户信息
     */
    @Override
    public void checkUserAllowed(SysUser user)
    {
        if (StringUtils.isNotNull(user.getUserId()) && user.isAdmin())
        {
            throw new ServiceException("不允许操作超级管理员用户");
        }
    }

    /**
     * 校验用户是否有数据权限
     *
     * @param userId 用户id
     */
    @Override
    public void checkUserDataScope(Long userId)
    {
        if (!SysUser.isAdmin(SecurityUtils.getUserId()))
        {
            SysUser user = new SysUser();
            user.setUserId(userId);
            List<SysUser> users = SpringUtils.getAopProxy(this).selectUserList(user);
            if (StringUtils.isEmpty(users))
            {
                throw new ServiceException("没有权限访问用户数据！");
            }
        }
    }

    /**
     * 新增保存用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    @Transactional
    public int insertUser(SysUser user)
    {
        // 新增用户信息
        int rows = userMapper.insertUser(user);
        // 新增用户岗位关联
        insertUserPost(user);
        // 新增用户与角色管理
        insertUserRole(user);
        return rows;
    }

    /**
     * 注册用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    public boolean registerUser(SysUser user)
    {
        return userMapper.insertUser(user) > 0;
    }

    /**
     * 修改保存用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    @Transactional
    public int updateUser(SysUser user)
    {
        Long userId = user.getUserId();
        // 删除用户与角色关联
        userRoleMapper.deleteUserRoleByUserId(userId);
        // 新增用户与角色管理
        insertUserRole(user);
        // 删除用户与岗位关联
        userPostMapper.deleteUserPostByUserId(userId);
        // 新增用户与岗位管理
        insertUserPost(user);
        return userMapper.updateUser(user);
    }

    /**
     * 用户授权角色
     *
     * @param userId 用户ID
     * @param roleIds 角色组
     */
    @Override
    @Transactional
    public void insertUserAuth(Long userId, Long[] roleIds)
    {
        userRoleMapper.deleteUserRoleByUserId(userId);
        insertUserRole(userId, roleIds);
    }

    /**
     * 修改用户状态
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    public int updateUserStatus(SysUser user)
    {
        return userMapper.updateUser(user);
    }

    /**
     * 修改用户基本信息
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    public int updateUserProfile(SysUser user)
    {
        return userMapper.updateUser(user);
    }

    /**
     * 修改用户头像
     *
     * @param userName 用户名
     * @param avatar 头像地址
     * @return 结果
     */
    @Override
    public boolean updateUserAvatar(String userName, String avatar)
    {
        return userMapper.updateUserAvatar(userName, avatar) > 0;
    }

    /**
     * 重置用户密码
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    public int resetPwd(SysUser user)
    {
        return userMapper.updateUser(user);
    }

    /**
     * 重置用户密码
     *
     * @param userName 用户名
     * @param password 密码
     * @return 结果
     */
    @Override
    public int resetUserPwd(String userName, String password)
    {
        return userMapper.resetUserPwd(userName, password);
    }

    /**
     * 新增用户角色信息
     *
     * @param user 用户对象
     */
    public void insertUserRole(SysUser user)
    {
        this.insertUserRole(user.getUserId(), user.getRoleIds());
    }

    /**
     * 新增用户岗位信息
     *
     * @param user 用户对象
     */
    public void insertUserPost(SysUser user)
    {
        Long[] posts = user.getPostIds();
        if (StringUtils.isNotEmpty(posts))
        {
            // 新增用户与岗位管理
            List<SysUserPost> list = new ArrayList<SysUserPost>(posts.length);
            for (Long postId : posts)
            {
                SysUserPost up = new SysUserPost();
                up.setUserId(user.getUserId());
                up.setPostId(postId);
                list.add(up);
            }
            userPostMapper.batchUserPost(list);
        }
    }

    /**
     * 新增用户角色信息
     *
     * @param userId 用户ID
     * @param roleIds 角色组
     */
    public void insertUserRole(Long userId, Long[] roleIds)
    {
        if (StringUtils.isNotEmpty(roleIds))
        {
            // 新增用户与角色管理
            List<SysUserRole> list = new ArrayList<SysUserRole>(roleIds.length);
            for (Long roleId : roleIds)
            {
                SysUserRole ur = new SysUserRole();
                ur.setUserId(userId);
                ur.setRoleId(roleId);
                list.add(ur);
            }
            userRoleMapper.batchUserRole(list);
        }
    }

    /**
     * 通过用户ID删除用户
     *
     * @param userId 用户ID
     * @return 结果
     */
    @Override
    @Transactional
    public int deleteUserById(Long userId)
    {
        // 删除用户与角色关联
        userRoleMapper.deleteUserRoleByUserId(userId);
        // 删除用户与岗位表
        userPostMapper.deleteUserPostByUserId(userId);
        return userMapper.deleteUserById(userId);
    }

    /**
     * 批量删除用户信息
     *
     * @param userIds 需要删除的用户ID
     * @return 结果
     */
    @Override
    @Transactional
    public int deleteUserByIds(Long[] userIds)
    {
        for (Long userId : userIds)
        {
            checkUserAllowed(new SysUser(userId));
            checkUserDataScope(userId);
        }
        // 删除用户与角色关联
        userRoleMapper.deleteUserRole(userIds);
        // 删除用户与岗位关联
        userPostMapper.deleteUserPost(userIds);
        return userMapper.deleteUserByIds(userIds);
    }

    /**
     * 导入用户数据
     *
     * @param userList 用户数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importUser(List<SysUser> userList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(userList) || userList.size() == 0)
        {
            throw new ServiceException("导入用户数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        String password = configService.selectConfigByKey("sys.user.initPassword");
        for (SysUser user : userList)
        {
            try
            {
                // 验证是否存在这个用户
                SysUser u = userMapper.selectUserByUserName(user.getUserName());
                if (StringUtils.isNull(u))
                {
                    BeanValidators.validateWithException(validator, user);
                    user.setPassword(SecurityUtils.encryptPassword(password));
                    user.setCreateBy(operName);
                    userMapper.insertUser(user);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、账号 " + user.getUserName() + " 导入成功");
                }
                else if (isUpdateSupport)
                {
                    BeanValidators.validateWithException(validator, user);
                    checkUserAllowed(u);
                    checkUserDataScope(u.getUserId());
                    user.setUserId(u.getUserId());
                    user.setUpdateBy(operName);
                    userMapper.updateUser(user);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、账号 " + user.getUserName() + " 更新成功");
                }
                else
                {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、账号 " + user.getUserName() + " 已存在");
                }
            }
            catch (Exception e)
            {
                failureNum++;
                String msg = "<br/>" + failureNum + "、账号 " + user.getUserName() + " 导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0)
        {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        }
        else
        {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

    @Override
    public List<SysUser> selectUserInfoByUserIdList(List<Long> userIdList) {
        return userMapper.selectUserInfoByUserIdList(userIdList);
    }

    @Override
    public List<UserVo> handleUserVo(List<Long> userIdList) {
        List<UserVo> userVoList = new ArrayList<>();
        List<SysUser> sysUserList = this.selectUserInfoByUserIdList(userIdList);
        if (sysUserList != null && sysUserList.size() > 0){
            for (SysUser sysUser : sysUserList) {
                UserVo userVo = new UserVo();
                userVo.setId(sysUser.getUserId());
                userVo.setName(sysUser.getNickName());
                userVoList.add(userVo);
            }
        }

        return userVoList;
    }

    @Override
    public List<UserVo> handleUserAndAvatarVo(List<Long> userIdList) {
        List<UserVo> userVoList = new ArrayList<>();
        Map<Long, SysUser> userInfoAndAvatar = this.getUserInfoAndAvatar(userIdList);
        Collection<SysUser> sysUserList = userInfoAndAvatar.values();
        if (sysUserList != null && sysUserList.size() > 0){
            for (SysUser sysUser : sysUserList) {
                if(Objects.isNull(sysUser)){
                    continue;
                }
                UserVo userVo = new UserVo();
                userVo.setId(sysUser.getUserId());
                userVo.setName(sysUser.getUserName());
                if(Objects.nonNull(sysUser.getAttachment())){
                    userVo.setAvatar(sysUser.getAttachment().getUrl());
                }
                userVoList.add(userVo);
            }
        }

        return userVoList;
    }

    /*/**
     * @Author yangguanyu
     * @Description :获取人员信息和头像信息，请注意头像可能为null,使用时请注意空指针判断
     * @Date 21:47 2024/2/13
     * @Param [java.util.List<java.lang.Long>]
     * @return java.util.Map<java.lang.Long,com.ruoyi.common.core.domain.entity.SysUser>
     **/
    public Map<Long, SysUser> getUserInfoAndAvatar(List<Long> userList){
        List<SysUser> sysUsers = selectUserInfoByUserIdList(userList);
        List<TfpAttachment> avatarList = getHeadPortraitListByUserIdList(userList);
        if(!CollectionUtils.isEmpty(avatarList)){
            Map<Long, TfpAttachment> avatarMap = avatarList.stream().collect(Collectors.toMap(TfpAttachment::getBusinessId, Function.identity(), (k1, k2) -> k1));
            for (SysUser user : sysUsers) {
                TfpAttachment tfpAttachment = avatarMap.get(user.getUserId());
                if(Objects.isNull(tfpAttachment)){
                    tfpAttachment = new TfpAttachment();
                    tfpAttachment.setName("默认头像");
                    tfpAttachment.setUrl(defaultAvatar);
                    tfpAttachment.setBusinessSubType(BusinessSubTypeEnum.USER_HEAD_PORTRAIT.getValue());
                }else {
                    if(StringUtils.isEmpty(tfpAttachment.getUrl())){
                        tfpAttachment.setUrl(defaultAvatar);
                    }
                }
                user.setAttachment(tfpAttachment);
                user.setAvatar(tfpAttachment.getUrl());
            }
        }
        Map<Long, SysUser> userMap = new HashMap<>();
        if(org.apache.commons.collections4.CollectionUtils.isNotEmpty(sysUsers)){
            userMap = sysUsers.stream().collect(Collectors.toMap(SysUser::getUserId, Function.identity(),(k1, k2)->k1));
        }

        return userMap;
    }

    /*/**
     * @Author yangguanyu
     * @Description :获取人员信息，此处没头像，有头像的在上班
     * @Date 21:47 2024/2/13
     * @Param [java.util.List<java.lang.Long>]
     * @return java.util.Map<java.lang.Long,com.ruoyi.common.core.domain.entity.SysUser>
     **/
    public Map<Long, SysUser> getUserInfoTransMap(List<Long> userList){
        List<SysUser> sysUsers = selectUserInfoByUserIdList(userList);
        Map<Long, SysUser> userMap = new HashMap<>();
        if(org.apache.commons.collections4.CollectionUtils.isNotEmpty(sysUsers)){
            userMap = sysUsers.stream().collect(Collectors.toMap(SysUser::getUserId, Function.identity(),(k1, k2)->k1));
        }
        return userMap;
    }

    /**
     * 通过userIdList获取头像集合
     * @param userIdList
     * @return
     */
    @Override
    public List<TfpAttachment> getHeadPortraitListByUserIdList(List<Long> userIdList){
        String businessSubType = BusinessSubTypeEnum.USER_HEAD_PORTRAIT.getValue();
        return  attachmentService.selectList(userIdList, null, businessSubType);
    }

    @Override
    public UserInfoDetailVo userInfoDetail(Long userId) {
        // 访问他人 是:true 否:false
        boolean visitOtherFlag = true;
        if (Objects.isNull(userId)){
            LoginUser loginUser = SecurityUtils.getLoginUser();
            userId = loginUser.getUserId();
            visitOtherFlag = false;
        }

        SysUser sysUser = userMapper.selectUserById(userId);
        if (Objects.isNull(sysUser)){
            throw new RuntimeException("用户信息不存在");
        }

        UserInfoDetailVo userInfoDetailVo = new UserInfoDetailVo();
        BeanUtils.copyProperties(sysUser, userInfoDetailVo);

        List<SysUserLabel> sysUserLabelList = this.getUserLabelList(userId);
        if (DcListUtils.isNotEmpty(sysUserLabelList)){
            List<String> userLabelNameList = sysUserLabelList.stream().map(SysUserLabel::getLabelName).collect(Collectors.toList());
            userInfoDetailVo.setUserLabelNameList(userLabelNameList);
        }

        // 关注个数
        Integer careNumber = userCareService.selectCareNumber(userId);
        // 粉丝个数
        Integer fansNumber = userCareService.selectFansNumber(userId);
        // 今日访客个数
        Integer todayVisitorNumber = userVisitorRecordService.selectTodayVisitorNumber(userId);

        userInfoDetailVo.setCareNumber(careNumber);
        userInfoDetailVo.setFansNumber(fansNumber);
        userInfoDetailVo.setTodayVisitorNumber(todayVisitorNumber);

        // 用户图片
        List<TfpAttachment> attachmentList = attachmentService.selectListByBusinessId(userId, BusinessTypeEnum.USER.getValue(), null);
        if (DcListUtils.isNotEmpty(attachmentList)){
            List<String> userImageList = new ArrayList<>();
            for (TfpAttachment attachment : attachmentList) {
                if (BusinessSubTypeEnum.USER_HEAD_PORTRAIT.getValue().equals(attachment.getBusinessSubType())){
                    userImageList.add(0, attachment.getUrl());
                } else {
                    userImageList.add(attachment.getUrl());
                }
            }

            userInfoDetailVo.setUserImageList(userImageList);
        }

        if (visitOtherFlag){
            // 添加访客记录
            userVisitorRecordService.logUserVisitorRecord(userId);

            LoginUser loginUser = SecurityUtils.getLoginUser();
            Long loginUserId = loginUser.getUserId();

            // 是否关注该用户
            SysUserCare param = new SysUserCare();
            param.setOwnerUserId(userId);
            param.setFansUserId(loginUserId);
            param.setDelFlag(DeleteEnum.EXIST.getValue());
            List<SysUserCare> sysUserCareList = userCareService.selectSysUserCareList(param);
            if (DcListUtils.isNotEmpty(sysUserCareList)){
                userInfoDetailVo.setUserCareFlag(YesOrNoFlagEnum.YES.getValue());
            } else {
                userInfoDetailVo.setUserCareFlag(YesOrNoFlagEnum.NO.getValue());
            }
        }

        return userInfoDetailVo;
    }

    @Override
    public int editUserInfo(EditUserInfoDto editUserInfoDto) {
        String nickName = editUserInfoDto.getNickName().trim();
        if (nickName.contains(" ")){
            throw new BusinessException("昵称不能包含空格");
        }

        if (!DcValidateUtils.validateUserName(nickName)){
            throw new BusinessException("昵称格式错误");
        }

        int length = nickName.length();
        if (length < 2 || length > 15){
            throw new BusinessException("昵称在2~15个字符");
        }

        String phonenumber = editUserInfoDto.getPhonenumber();
        if (Objects.nonNull(phonenumber) && !DcValidateUtils.validatePhone(phonenumber)){
            throw new BusinessException("手机号格式错误");
        }

        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = loginUser.getUserId();

        if (!Objects.equals(userId, editUserInfoDto.getUserId())){
            throw new RuntimeException("用户id与当前登录信息不一致");
        }

        SysUser sysUser = userMapper.selectUserById(userId);
        sysUser.setNickName(nickName);
        sysUser.setSex(editUserInfoDto.getSex());
        sysUser.setBirthday(editUserInfoDto.getBirthday());
        sysUser.setConstellationCode(editUserInfoDto.getConstellationCode());
        sysUser.setConstellationName(editUserInfoDto.getConstellationName());
        // TODO 是否实名认证
        sysUser.setRealNameAuthStatus(editUserInfoDto.getRealNameAuthStatus());
        sysUser.setPersonalProfile(editUserInfoDto.getPersonalProfile());

        if (Objects.nonNull(phonenumber)) {
            sysUser.setPhonenumber(phonenumber);
        }

        sysUser.setUpdateBy(sysUser.getUserName());
        sysUser.setUpdateTime(new Date());
        userMapper.updateUser(sysUser);

        // 保存用户标签
        userLabelService.saveList(userId, editUserInfoDto.getUserLabelList());

        // 保存用户图片
        attachmentService.saveUserImage(userId, editUserInfoDto.getUserImageList());

        // 刷新令牌
        loginUser.setUser(sysUser);
        tokenService.refreshToken(loginUser);

        return 1;
    }

    @Override
    public DetailEditUserVo detailEditUser() {
        DetailEditUserVo detailEditUserVo = new DetailEditUserVo();

        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = loginUser.getUserId();

        SysUser sysUser = userMapper.selectUserById(userId);
        BeanUtils.copyProperties(sysUser, detailEditUserVo);

        List<SysUserLabel> sysUserLabelList = this.getUserLabelList(userId);
        if (DcListUtils.isNotEmpty(sysUserLabelList)){
            List<UserLabelDto> userLabelDtoList = new ArrayList<>();
            for (SysUserLabel sysUserLabel : sysUserLabelList) {
                UserLabelDto userLabelDto = new UserLabelDto();
                BeanUtils.copyProperties(sysUserLabel, userLabelDto);
                userLabelDtoList.add(userLabelDto);
            }

            detailEditUserVo.setUserLabelList(userLabelDtoList);
        }

        // 用户图片
        List<TfpAttachment> attachmentList = attachmentService.selectListByBusinessId(userId, BusinessTypeEnum.USER.getValue(), null);
        if (DcListUtils.isNotEmpty(attachmentList)){
            List<UserImageDto> userImageList = new ArrayList<>();
            for (TfpAttachment attachment : attachmentList) {
                UserImageDto userImageDto = new UserImageDto();

                Integer headSculptureFlag = 0;
                if (BusinessSubTypeEnum.USER_HEAD_PORTRAIT.getValue().equals(attachment.getBusinessSubType())){
                    headSculptureFlag = 1;
                }

                userImageDto.setHeadSculptureFlag(headSculptureFlag);
                userImageDto.setUrl(attachment.getUrl());


                if (BusinessSubTypeEnum.USER_HEAD_PORTRAIT.getValue().equals(attachment.getBusinessSubType())){
                    userImageList.add(0, userImageDto);
                } else {
                    userImageList.add(userImageDto);
                }
            }
            detailEditUserVo.setUserImageList(userImageList);
        }

        return detailEditUserVo;
    }

    @Override
    public UserBaseInfoVo getUserBaseInfo() {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        SysUser user = loginUser.getUser();
        Long identityId = user.getIdentityId();
        SysIdentity identity = identityService.selectSysIdentityById(identityId);

        UserBaseInfoVo userBaseInfoVo = new UserBaseInfoVo();
        userBaseInfoVo.setUserId(user.getUserId());
        userBaseInfoVo.setNickName(user.getNickName());
        userBaseInfoVo.setIdentityId(identity.getId());
        userBaseInfoVo.setIdentityName(identity.getIdentityName());
        userBaseInfoVo.setRealNameAuthStatus(user.getRealNameAuthStatus());
        userBaseInfoVo.setUid(user.getUid());

        List<Long> userIdList = new ArrayList<>();
        userIdList.add(loginUser.getUserId());
        List<TfpAttachment> headPortraitList = this.getHeadPortraitListByUserIdList(userIdList);
        if (DcListUtils.isNotEmpty(headPortraitList)){
            userBaseInfoVo.setUserHeadImage(headPortraitList.get(0).getUrl());
        }

        return userBaseInfoVo;
    }

    /**
     * 小程序-编辑用户单独的信息
     * @param editSeparateUserInfoDto
     * @return
     */
    @Override
    public int editSeparateUserInfo(EditSeparateUserInfoDto editSeparateUserInfoDto) {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = loginUser.getUserId();

        if (!Objects.equals(userId, editSeparateUserInfoDto.getUserId())){
            throw new RuntimeException("用户id与当前登录信息不一致");
        }

        String nickName = editSeparateUserInfoDto.getNickName();
        if (Objects.nonNull(nickName)){
            SysUser sysUser = userMapper.selectUserById(userId);
            sysUser.setNickName(nickName);
            sysUser.setUpdateBy(sysUser.getUserName());
            sysUser.setUpdateTime(new Date());
            userMapper.updateUser(sysUser);

            // 刷新令牌
            loginUser.setUser(sysUser);
            tokenService.refreshToken(loginUser);
        }

        String headImageUrl = editSeparateUserInfoDto.getHeadImageUrl();
        if (Objects.nonNull(headImageUrl)){
            // 用户头像文件
            List<TfpAttachment> attachmentList = attachmentService.selectListByBusinessId(userId, BusinessTypeEnum.USER.getValue(), BusinessSubTypeEnum.USER_HEAD_PORTRAIT.getValue());
            if (DcListUtils.isNotEmpty(attachmentList)){
                TfpAttachment attachment = attachmentList.get(0);
                attachment.setUrl(headImageUrl);
                attachment.setUpdateUserId(loginUser.getUserId());
                attachment.setUpdateBy(loginUser.getUsername());
                attachment.setUpdateTime(DateUtils.getNowDate());
                attachmentService.updateTfpAttachment(attachment);
            } else {
                String extend = DcUrlUtils.getExtend(headImageUrl);
                String name = "用户头像图片" + extend;
                String businessSubType = BusinessSubTypeEnum.USER_HEAD_PORTRAIT.getValue();

                TfpAttachment attachment = new TfpAttachment();
                attachment.setBusinessId(userId);
                attachment.setBusinessType(BusinessTypeEnum.USER.getValue());
                attachment.setBusinessSubType(businessSubType);
                attachment.setName(name);
                attachment.setUrl(headImageUrl);
                attachment.setDelFlag(DeleteEnum.EXIST.getValue());
                attachment.setCreateUserId(loginUser.getUserId());
                attachment.setCreateBy(loginUser.getUsername());
                attachment.setCreateTime(DateUtils.getNowDate());
                attachmentService.insertTfpAttachment(attachment);
            }
        }

        return 1;
    }

    @Override
    public int realNameAuthentication(RealNameAuthenticationDto realNameAuthenticationDto) {
        String cardNum = realNameAuthenticationDto.getCardNum();
        if (!DcValidateUtils.validateIdCard(cardNum)){
            throw new BusinessException("身份证格式错误");
        }

        String phonenumber = realNameAuthenticationDto.getPhonenumber();
        if (!DcValidateUtils.validatePhone(phonenumber)){
            throw new BusinessException("手机号格式错误");
        }

        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = loginUser.getUserId();

        SysUser sysUser = userMapper.selectUserById(userId);

        // 字典 card_type 默认身份证 1
        String cardType = UserCardTypeEnum.ID_CARD.getValue().toString();

        sysUser.setRealName(realNameAuthenticationDto.getRealName());
        sysUser.setCardType(cardType);
        sysUser.setCardNum(realNameAuthenticationDto.getCardNum());
        sysUser.setRealNamePhone(realNameAuthenticationDto.getPhonenumber());
        sysUser.setRealNameAuthStatus(YesOrNoFlagEnum.YES.getValue());
        sysUser.setUpdateBy(loginUser.getUsername());
        sysUser.setUpdateTime(DateUtils.getNowDate());
        userMapper.updateUser(sysUser);

        // 追加同行人信息
        TfpComate tfpComate = new TfpComate();
        tfpComate.setMainUserId(userId);
        tfpComate.setViceUserId(userId);
        tfpComate.setMateName(realNameAuthenticationDto.getRealName());
        tfpComate.setMateCardType(UserCardTypeEnum.ID_CARD.getValue());
        tfpComate.setMateCardNum(realNameAuthenticationDto.getCardNum());
        tfpComate.setMatePhone(realNameAuthenticationDto.getPhonenumber());
        tfpComate.setDelFlag(DeleteEnum.EXIST.getValue());
        tfpComate.setCreateUserId(loginUser.getUserId());
        tfpComate.setCreateBy(loginUser.getUsername());
        tfpComate.setCreateTime(DateUtils.getNowDate());
        comateService.insertTfpComate(tfpComate);

        // 刷新令牌
        loginUser.setUser(sysUser);
        tokenService.refreshToken(loginUser);

        return 1;
    }

    @Override
    public int realNameAuthenticationEdit(RealNameAuthenticationEditDto realNameAuthenticationEditDto) {
        String phonenumber = realNameAuthenticationEditDto.getPhonenumber();
        if (!DcValidateUtils.validatePhone(phonenumber)){
            throw new BusinessException("手机号格式错误");
        }

        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = loginUser.getUserId();

        SysUser sysUser = userMapper.selectUserById(userId);
        sysUser.setRealNamePhone(realNameAuthenticationEditDto.getPhonenumber());
        sysUser.setUpdateBy(loginUser.getUsername());
        sysUser.setUpdateTime(DateUtils.getNowDate());
        userMapper.updateUser(sysUser);

        return 1;
    }

    @Override
    public RealNameAuthenticationInfoVo realNameAuthenticationInfo() {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = loginUser.getUserId();

        SysUser sysUser = userMapper.selectUserById(userId);

        RealNameAuthenticationInfoVo realNameAuthenticationInfoVo = new RealNameAuthenticationInfoVo();
        realNameAuthenticationInfoVo.setRealName(sysUser.getRealName());
        realNameAuthenticationInfoVo.setCardNum(sysUser.getCardNum());
        realNameAuthenticationInfoVo.setPhonenumber(sysUser.getRealNamePhone());
        return realNameAuthenticationInfoVo;
    }

    @Override
    public Map<Long, String> getNickNameMap(List<Long> userIdList){
        Map<Long, String> nickNameMap = new HashMap<>();
        List<SysUser> userList = userMapper.selectUserInfoByUserIdList(userIdList);
        if (DcListUtils.isNotEmpty(userList)){
            nickNameMap = userList.stream().collect(Collectors.toMap(SysUser::getUserId, SysUser::getNickName));
        }

        return nickNameMap;
    }

    @Override
    public Map<Long, String> getRealNameMap(List<Long> userIdList) {
        Map<Long, String> realNameMap = new HashMap<>();
        List<SysUser> userList = userMapper.selectUserInfoByUserIdList(userIdList);
        if (DcListUtils.isNotEmpty(userList)){
            for (SysUser sysUser : userList) {
                realNameMap.put(sysUser.getUserId(), sysUser.getRealName());
            }
        }

        return realNameMap;
    }

    private List<SysUserLabel> getUserLabelList(Long userId){
        SysUserLabel param = new SysUserLabel();
        param.setUserId(userId);
        param.setDelFlag("0");
        List<SysUserLabel> sysUserLabelList = userLabelService.selectSysUserLabelList(param);
        return sysUserLabelList;
    }
}
