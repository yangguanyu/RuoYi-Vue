package com.ruoyi.common.enuma;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 角色类型
 *
 * @author
 * @date
 * @time
 */
@Getter
@AllArgsConstructor
public enum UserCardTypeEnum implements BaseEnum {
    ID_CARD(1, "身份证"),
    TAI_WAN_COMPATRIOT_CERTIFICATE(2, "台胞证"),
    GANG_AO_PASSPORT(3, "港澳通行证"),
    PASSPORT(4, "护照"),
    SOLDIER_CERTIFICATE(5, "士兵证"),
    ;


    private Integer value;
    private String desc;
}
