package com.ruoyi.common.enuma;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 角色类型
 *
 * @author
 * @date
 * @time
 */
@Getter
@AllArgsConstructor
public enum DeleteEnum implements BaseStrEnum {


    EXIST("0", "未删除"),



    DELETE("2", "已删除")
    ;


    private String value;
    private String desc;
}
