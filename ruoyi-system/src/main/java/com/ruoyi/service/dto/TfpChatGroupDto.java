package com.ruoyi.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 聊天群组对象 tfp_chat_group
 *
 * @author ruoyi
 * @date 2023-10-31
 */
@Data
@ApiModel("TfpChatGroupDto")
public class TfpChatGroupDto extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 聊天群组名称 */
    private String name;

    /** 商品id */
    @ApiModelProperty("商品id")
    private Long productId;

    @ApiModelProperty("出发日期")
    private Date goDate;

    /** 商品名称 */
    private String productName;

    private boolean productNameFlag;

    private List<Long> productIdList;

    /** 限制人数 */
    private Integer limitPersonNumber;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    private Long createUserId;

    /** 更新者id */
    private Long updateUserId;

    /** 超过当前时间 */
//    private Boolean curFlag;

    @JsonIgnore
    private Date goDateStart;
    @JsonIgnore
    private Date goDateEnd;
}
