package com.ruoyi.common.enuma;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

/**
 * 角色类型
 *
 * @author
 * @date
 * @time
 */
@Getter
@AllArgsConstructor
public enum FindPartnerStatusEnum implements BaseEnum {

    UN_PUBLISH(0, "未发布"),

    PASS(1, "审核通过"),

    REJECT(2, "审核未通过"),

    PUBLISH_AND_PRE_AUTH(3, "已发布待审核"),

    ILLEGA(4, "因疑似非法而下架"),

    ;


    private Integer value;
    private String desc;



}
