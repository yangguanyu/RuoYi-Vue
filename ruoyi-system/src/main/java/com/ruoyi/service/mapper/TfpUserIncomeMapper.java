package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.TfpUserIncome;
import org.apache.ibatis.annotations.Param;

/**
 * 用户收益Mapper接口
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public interface TfpUserIncomeMapper 
{
    /**
     * 查询用户收益
     * 
     * @param id 用户收益主键
     * @return 用户收益
     */
    public TfpUserIncome selectTfpUserIncomeById(Long id);

    /**
     * 查询用户收益列表
     * 
     * @param tfpUserIncome 用户收益
     * @return 用户收益集合
     */
    public List<TfpUserIncome> selectTfpUserIncomeList(TfpUserIncome tfpUserIncome);

    /**
     * 新增用户收益
     * 
     * @param tfpUserIncome 用户收益
     * @return 结果
     */
    public int insertTfpUserIncome(TfpUserIncome tfpUserIncome);

    /**
     * 修改用户收益
     * 
     * @param tfpUserIncome 用户收益
     * @return 结果
     */
    public int updateTfpUserIncome(TfpUserIncome tfpUserIncome);

    /**
     * 删除用户收益
     * 
     * @param id 用户收益主键
     * @return 结果
     */
    public int deleteTfpUserIncomeById(Long id);

    /**
     * 批量删除用户收益
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpUserIncomeByIds(Long[] ids);

    TfpUserIncome getUserIncomeByUserId(@Param("userId") Long userId);
}
