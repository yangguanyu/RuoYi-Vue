import request from '@/utils/request'

// 查询同行人和订单关联列表
export function listComateOrder(query) {
  return request({
    url: '/service/comateOrder/list',
    method: 'get',
    params: query
  })
}

// 查询同行人和订单关联详细
export function getComateOrder(id) {
  return request({
    url: '/service/comateOrder/' + id,
    method: 'get'
  })
}

// 新增同行人和订单关联
export function addComateOrder(data) {
  return request({
    url: '/service/comateOrder',
    method: 'post',
    data: data
  })
}

// 修改同行人和订单关联
export function updateComateOrder(data) {
  return request({
    url: '/service/comateOrder',
    method: 'put',
    data: data
  })
}

// 删除同行人和订单关联
export function delComateOrder(id) {
  return request({
    url: '/service/comateOrder/' + id,
    method: 'delete'
  })
}
