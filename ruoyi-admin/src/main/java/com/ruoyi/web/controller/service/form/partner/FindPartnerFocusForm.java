package com.ruoyi.web.controller.service.form.partner;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 找搭子关注对象 tfp_find_partner_focus
 *
 * @author yangguanyu
 * @date 2024-01-12
 */
@ApiModel("FindPartnerFocusForm-找搭子关注添加")
@Data
public class FindPartnerFocusForm {


    /** 用户id */
    @ApiModelProperty(value = "用户id")
    private Long userId;

    /** 找搭子id */
    @ApiModelProperty(value = "找搭子id")
    private Long findPartnerId;

}
