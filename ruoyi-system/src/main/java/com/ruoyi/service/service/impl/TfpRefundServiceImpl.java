package com.ruoyi.service.service.impl;

import java.text.DateFormat;
import java.util.*;
import java.util.stream.Collectors;

import com.ruoyi.common.core.domain.entity.TfpAttachment;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enuma.*;
import com.ruoyi.common.exception.BusinessException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.DcDateUtils;
import com.ruoyi.common.utils.DcListUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.service.domain.*;
import com.ruoyi.service.dto.ApplyRefundDto;
import com.ruoyi.service.dto.AuditRefundDto;
import com.ruoyi.service.dto.TfpRefundDto;
import com.ruoyi.service.service.*;
import com.ruoyi.service.vo.ApplyRefundDetailVo;
import com.ruoyi.service.vo.RefundDetailVo;
import com.ruoyi.service.vo.TfpRefundVo;
import com.ruoyi.service.vo.UserVo;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpRefundMapper;
import org.springframework.transaction.annotation.Transactional;

/**
 * 退款Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@Service
@Transactional
public class TfpRefundServiceImpl implements ITfpRefundService 
{
    @Autowired
    private TfpRefundMapper tfpRefundMapper;
    @Autowired
    private ISysUserService sysUserService;
    @Autowired
    private ITfpProductService tfpProductService;
    @Autowired
    private ITfpOrderService orderService;
    @Autowired
    private ITfpAttachmentService attachmentService;
    @Autowired
    private ITfpChatGroupService chatGroupService;
    @Autowired
    private ITfpComateOrderService tfpComateOrderService;
    @Autowired
    private ITfpComateService tfpComateService;

    /**
     * 查询退款
     * 
     * @param id 退款主键
     * @return 退款
     */
    @Override
    public TfpRefund selectTfpRefundById(Long id)
    {
        return tfpRefundMapper.selectTfpRefundById(id);
    }

    /**
     * 查询退款列表
     * 
     * @param tfpRefund 退款
     * @return 退款
     */
    @Override
    public List<TfpRefund> selectTfpRefundList(TfpRefund tfpRefund)
    {
        return tfpRefundMapper.selectTfpRefundList(tfpRefund);
    }

    @Override
    public List<TfpRefundVo> selectTfpRefundVoList(TfpRefundDto tfpRefundDto) {
        String productName = tfpRefundDto.getProductName();
        if (productName != null && !productName.trim().equals("")){
            tfpRefundDto.setProductNameFlag(true);

            TfpProduct tfpProduct = new TfpProduct();
            tfpProduct.setProductName(productName);
            List<TfpProduct> tfpProductList = tfpProductService.selectTfpProductList(tfpProduct);
            if (tfpProductList == null || tfpProductList.size() == 0){
                return new ArrayList<>();
            }

            List<Long> productIdList = tfpProductList.stream().map(TfpProduct::getId).collect(Collectors.toList());
            tfpRefundDto.setProductIdList(productIdList);
        }

        List<TfpRefundVo> tfpRefundVoList = tfpRefundMapper.selectTfpRefundVoList(tfpRefundDto);
        if (tfpRefundVoList != null && tfpRefundVoList.size() > 0){
            List<Long> refundUserIdList = new ArrayList<>();
            for (TfpRefundVo tfpRefundVo : tfpRefundVoList) {
                Long refundUserId = tfpRefundVo.getRefundUserId();
                if (!refundUserIdList.contains(refundUserId)){
                    refundUserIdList.add(refundUserId);
                }
            }

            Map<Long, String> nickNameMap = sysUserService.getNickNameMap(refundUserIdList);

            for (TfpRefundVo tfpRefundVo : tfpRefundVoList) {
                tfpRefundVo.setRefundNickName(nickNameMap.get(tfpRefundVo.getRefundUserId()));
            }
        }

        return tfpRefundVoList;
    }

    /**
     * 新增退款
     * 
     * @param tfpRefund 退款
     * @return 结果
     */
    @Override
    public int insertTfpRefund(TfpRefund tfpRefund)
    {
        tfpRefund.setCreateTime(DateUtils.getNowDate());
        return tfpRefundMapper.insertTfpRefund(tfpRefund);
    }

    /**
     * 修改退款
     * 
     * @param tfpRefund 退款
     * @return 结果
     */
    @Override
    public int updateTfpRefund(TfpRefund tfpRefund)
    {
        tfpRefund.setUpdateTime(DateUtils.getNowDate());
        return tfpRefundMapper.updateTfpRefund(tfpRefund);
    }

    /**
     * 批量删除退款
     * 
     * @param ids 需要删除的退款主键
     * @return 结果
     */
    @Override
    public int deleteTfpRefundByIds(Long[] ids)
    {
        return tfpRefundMapper.deleteTfpRefundByIds(ids);
    }

    /**
     * 删除退款信息
     * 
     * @param id 退款主键
     * @return 结果
     */
    @Override
    public int deleteTfpRefundById(Long id)
    {
        return tfpRefundMapper.deleteTfpRefundById(id);
    }

    @Override
    public List<UserVo> refundUserSelect() {
        List<Long> refundUserIdList = tfpRefundMapper.selectRefundUserIdList();
        return sysUserService.handleUserVo(refundUserIdList);
    }

    @Override
    public ApplyRefundDetailVo applyRefundDetail(Long orderId) {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = loginUser.getUserId();

        TfpOrder tfpOrder = orderService.selectTfpOrderById(orderId);
        Integer status = tfpOrder.getStatus();

        List<Integer> canRefundStatusList = new ArrayList<>();
        canRefundStatusList.add(OrderStatusEnum.WAIT_CONFIRMED.getValue());
        canRefundStatusList.add(OrderStatusEnum.WAIT_TRAVEL.getValue());
        canRefundStatusList.add(OrderStatusEnum.COMPLETE.getValue());

        if (!canRefundStatusList.contains(status)) {
            throw new RuntimeException("当前订单不支持申请退款");
        }

        ApplyRefundDetailVo applyRefundDetailVo = new ApplyRefundDetailVo();

        Long productId = tfpOrder.getProductId();
        TfpProduct tfpProduct = tfpProductService.selectTfpProductById(productId);

        List<String> routeHighlightList = orderService.getRouteHighlightList(productId);

        Long chatGroupId = tfpOrder.getChatGroupId();
        if (Objects.nonNull(chatGroupId)){
            TfpChatGroup tfpChatGroup = chatGroupService.selectTfpChatGroupById(chatGroupId);
            applyRefundDetailVo.setChatGroupName(tfpChatGroup.getName());
        }

        // 商品图片
        List<TfpAttachment> attachmentList = attachmentService.selectListByBusinessId(productId, BusinessTypeEnum.PRODUCT.getValue(), BusinessSubTypeEnum.PRODUCT_IMAGE.getValue());
        if (DcListUtils.isNotEmpty(attachmentList)){
            List<String> productImageUrlList = new ArrayList<>();
            for (TfpAttachment attachment : attachmentList) {
                String url = attachment.getUrl();
                productImageUrlList.add(url);
            }

            applyRefundDetailVo.setProductImageUrlList(productImageUrlList);
        }

        applyRefundDetailVo.setOrderId(orderId);
        applyRefundDetailVo.setTravelType(tfpOrder.getTravelType());
        applyRefundDetailVo.setProductName(tfpProduct.getProductName());
        applyRefundDetailVo.setCategoryId(tfpProduct.getCategoryId());
        applyRefundDetailVo.setCategoryName(tfpProduct.getCategoryName());
        applyRefundDetailVo.setRouteHighlightList(routeHighlightList);
        applyRefundDetailVo.setOrderNo(tfpOrder.getOrderNo());
        applyRefundDetailVo.setFinalAmount(tfpOrder.getFinalAmount());
        applyRefundDetailVo.setGoDate(tfpOrder.getGoDate());
        return applyRefundDetailVo;
    }

    /**
     * 小程序-申请退款
     * @param applyRefundDto
     * @return
     */
    @Override
    public int applyRefund(ApplyRefundDto applyRefundDto) {
        Long orderId = applyRefundDto.getOrderId();
        TfpRefund tfpRefund = tfpRefundMapper.selectWaitAuditTfpRefundByOrderId(orderId);
        if (Objects.nonNull(tfpRefund)){
            throw new BusinessException("当前订单的退款信息正在审核中，请勿重复操作");
        }

        TfpOrder tfpOrder = orderService.selectTfpOrderById(orderId);
        Integer applyRefundFlag = tfpOrder.getApplyRefundFlag();
        if (Objects.equals(applyRefundFlag, YesOrNoFlagEnum.NO.getValue())){
            throw new BusinessException("该订单不支持二次退款申请");
        }

        Integer status = tfpOrder.getStatus();

        List<Integer> canRefundStatusList = new ArrayList<>();
        canRefundStatusList.add(OrderStatusEnum.WAIT_CONFIRMED.getValue());
        canRefundStatusList.add(OrderStatusEnum.WAIT_TRAVEL.getValue());
        canRefundStatusList.add(OrderStatusEnum.COMPLETE.getValue());

        if (!canRefundStatusList.contains(status)) {
            throw new RuntimeException("当前订单状态不支持支付");
        }

        if (Objects.equals(OrderStatusEnum.COMPLETE.getValue(), status)){
            Date refundEndTime = tfpOrder.getRefundEndTime();
            if (new Date().after(refundEndTime)){
                throw new RuntimeException("已过退款截止时间，不能进行退款申请");
            }
        }

        LoginUser loginUser = SecurityUtils.getLoginUser();

        tfpRefund = new TfpRefund();
        tfpRefund.setProductId(tfpOrder.getProductId());
        tfpRefund.setOrderId(orderId);
        tfpRefund.setOrderNo(tfpOrder.getOrderNo());
        tfpRefund.setOrderPrice(tfpOrder.getFinalAmount());
        tfpRefund.setRefundUserId(tfpOrder.getCreateUserId());
        tfpRefund.setRefundType(RefundTypeEnum.ALL_REFUND.getValue());
        tfpRefund.setRefundAmount(tfpOrder.getFinalAmount());
        tfpRefund.setRefundStatus(RefundStatusEnum.REFUND_AUDITING.getValue());
        tfpRefund.setReason(applyRefundDto.getReason());
        tfpRefund.setIllustrate(applyRefundDto.getIllustrate());
        tfpRefund.setActStatus(ActStatusEnum.UN_RECONCILED.getValue());
        tfpRefund.setDelFlag(DeleteEnum.EXIST.getValue());
        tfpRefund.setCreateUserId(loginUser.getUserId());
        tfpRefund.setCreateBy(loginUser.getUsername());
        tfpRefund.setCreateTime(DateUtils.getNowDate());
        tfpRefundMapper.insertTfpRefund(tfpRefund);

        // 退款编码
        String serialNo = this.createSerialNo(tfpRefund.getId());
        tfpRefund.setSerialNo(serialNo);
        tfpRefundMapper.updateTfpRefund(tfpRefund);

        tfpOrder.setRefundTempStatus(status);
        tfpOrder.setStatus(OrderStatusEnum.APPLY_REFUND_AUDITING.getValue());
        tfpOrder.setUpdateUserId(loginUser.getUserId());
        tfpOrder.setUpdateBy(loginUser.getUsername());
        tfpOrder.setUpdateTime(DateUtils.getNowDate());
        orderService.updateTfpOrder(tfpOrder);

        return 1;
    }

    /**
     * 小程序-退款详情
     * @param id
     * @return
     */
    @Override
    public RefundDetailVo refundDetail(Long id) {
        RefundDetailVo refundDetailVo = new RefundDetailVo();

        TfpRefund tfpRefund = tfpRefundMapper.selectTfpRefundById(id);
        Long orderId = tfpRefund.getOrderId();
        Long productId = tfpRefund.getProductId();

        TfpOrder tfpOrder = orderService.selectTfpOrderById(orderId);
        TfpProduct tfpProduct = tfpProductService.selectTfpProductById(productId);

        List<String> routeHighlightList = orderService.getRouteHighlightList(productId);

        TfpComateOrder param = new TfpComateOrder();
        param.setOrderId(orderId);
        param.setDelFlag(DeleteEnum.EXIST.getValue());
        List<TfpComateOrder> tfpComateOrderList = tfpComateOrderService.selectTfpComateOrderList(param);
        if (DcListUtils.isNotEmpty(tfpComateOrderList)){
            List<Long> mateIdList = tfpComateOrderList.stream().map(TfpComateOrder::getMateId).collect(Collectors.toList());
            List<TfpComate> tfpComateList = tfpComateService.selectTfpComateByIdList(mateIdList);
            if (DcListUtils.isNotEmpty(tfpComateList)){
                List<String> mateNameList = tfpComateList.stream().map(TfpComate::getMateName).collect(Collectors.toList());
                refundDetailVo.setMateNameList(mateNameList);
            }
        }

        refundDetailVo.setId(tfpRefund.getId());
        refundDetailVo.setProductName(tfpProduct.getProductName());
        refundDetailVo.setCategoryId(tfpProduct.getCategoryId());
        refundDetailVo.setCategoryName(tfpProduct.getCategoryName());
        refundDetailVo.setRouteHighlightList(routeHighlightList);
        refundDetailVo.setOrderNo(tfpOrder.getOrderNo());
        refundDetailVo.setSnapshotId(tfpOrder.getSnapshotId());
        refundDetailVo.setCreateTime(tfpOrder.getCreateTime());
        refundDetailVo.setFinalAmount(tfpOrder.getFinalAmount());
        refundDetailVo.setSerialNo(tfpRefund.getSerialNo());
        refundDetailVo.setApplyTime(tfpRefund.getCreateTime());
        refundDetailVo.setReason(tfpRefund.getReason());
        refundDetailVo.setRefundAmount(tfpRefund.getRefundAmount());
        refundDetailVo.setGoDate(tfpOrder.getGoDate());
        refundDetailVo.setGoPlaceName(tfpOrder.getGoPlaceName());
        refundDetailVo.setRefundTime(tfpRefund.getRefundTime());
        return refundDetailVo;
    }

    @Override
    public int auditRefund(AuditRefundDto auditRefundDto) {
        Integer auditStatus = auditRefundDto.getAuditStatus();
        if (!Objects.equals(AuditStatusEnum.AUDIT_PASS.getValue(), auditStatus) && !Objects.equals(AuditStatusEnum.AUDIT_NO_PASS.getValue(), auditStatus)){
            throw new BusinessException("审核状态有误");
        }

        Integer refundType = auditRefundDto.getRefundType();
        if (Objects.equals(RefundTypeEnum.PART_REFUND.getValue(), refundType)) {
            if (Objects.isNull(auditRefundDto.getRefundAmount())){
                throw new BusinessException("部分退款时，请填写退款金额");
            }
        }

        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = loginUser.getUserId();

        Long id;

        TfpOrder tfpOrder;
        Long orderId = auditRefundDto.getOrderId();
        if (Objects.nonNull(orderId)){
            tfpOrder = orderService.selectTfpOrderById(orderId);

            if (Objects.equals(AuditStatusEnum.AUDIT_NO_PASS.getValue(), auditStatus)){
                Integer refundTempStatus = tfpOrder.getRefundTempStatus();
                if (Objects.equals(refundTempStatus, OrderStatusEnum.WAIT_CONFIRMED.getValue())){
                    tfpOrder.setStatus(OrderStatusEnum.WAIT_TRAVEL.getValue());
                } else {
                    tfpOrder.setStatus(refundTempStatus);
                }
            } else {
                tfpOrder.setStatus(OrderStatusEnum.APPLY_REFUND_SUCCESS.getValue());
            }

            tfpOrder.setApplyRefundFlag(YesOrNoFlagEnum.NO.getValue());
            tfpOrder.setUpdateUserId(loginUser.getUserId());
            tfpOrder.setUpdateBy(loginUser.getUsername());
            tfpOrder.setUpdateTime(DateUtils.getNowDate());
            orderService.updateTfpOrder(tfpOrder);

            TfpRefund tfpRefund = tfpRefundMapper.selectWaitAuditTfpRefundByOrderId(orderId);
            id = tfpRefund.getId();

            if (Objects.equals(AuditStatusEnum.AUDIT_NO_PASS.getValue(), auditStatus)){
                tfpRefund.setRejectReason(auditRefundDto.getRejectReason());
                tfpRefund.setRefundStatus(RefundStatusEnum.AUDIT_FAIL_REFUND_FAIL.getValue());
            } else {
                tfpRefund.setRefundStatus(RefundStatusEnum.REFUND_SUCCESS.getValue());
                // 审核通过时间为退款时间
                tfpRefund.setRefundTime(DateUtils.getNowDate());
            }

            tfpRefund.setRefundType(refundType);
            if (Objects.equals(RefundTypeEnum.PART_REFUND.getValue(), refundType)) {
                tfpRefund.setRefundAmount(auditRefundDto.getRefundAmount());
            }

            tfpRefund.setUpdateUserId(userId);
            tfpRefund.setUpdateBy(loginUser.getUsername());
            tfpRefund.setUpdateTime(DateUtils.getNowDate());
            tfpRefundMapper.updateTfpRefund(tfpRefund);
        } else {
            tfpOrder = auditRefundDto.getTfpOrder();

            TfpRefund tfpRefund = new TfpRefund();
            tfpRefund.setProductId(tfpOrder.getProductId());
            tfpRefund.setOrderId(tfpOrder.getId());
            tfpRefund.setOrderNo(tfpOrder.getOrderNo());
            tfpRefund.setOrderPrice(tfpOrder.getFinalAmount());
            tfpRefund.setRefundUserId(tfpOrder.getCreateUserId());
            tfpRefund.setRefundType(refundType);
            tfpRefund.setRefundStatus(RefundStatusEnum.REFUND_SUCCESS.getValue());
            tfpRefund.setRefundTime(DateUtils.getNowDate());

            tfpRefund.setRefundAmount(tfpOrder.getFinalAmount());
            if (Objects.equals(RefundTypeEnum.PART_REFUND.getValue(), refundType)) {
                tfpRefund.setRefundAmount(auditRefundDto.getRefundAmount());
            }

            tfpRefund.setReason("其他原因");
            tfpRefund.setIllustrate(auditRefundDto.getReason());
            tfpRefund.setActStatus(ActStatusEnum.UN_RECONCILED.getValue());
            tfpRefund.setDelFlag(DeleteEnum.EXIST.getValue());
            tfpRefund.setCreateUserId(loginUser.getUserId());
            tfpRefund.setCreateBy(loginUser.getUsername());
            tfpRefund.setCreateTime(DateUtils.getNowDate());
            tfpRefundMapper.insertTfpRefund(tfpRefund);

            id = tfpRefund.getId();

            // 退款编码
            String serialNo = this.createSerialNo(id);
            tfpRefund.setSerialNo(serialNo);
            tfpRefundMapper.updateTfpRefund(tfpRefund);
        }

        List<String> urlList = auditRefundDto.getUrlList();
        if (DcListUtils.isNotEmpty(urlList)){
            String name = "退款凭证";
            attachmentService.saveImageList(id, name, urlList, BusinessTypeEnum.REFUND.getValue(), BusinessSubTypeEnum.REFUND_VOUCHER.getValue());
        }

        return 1;
    }

    @Override
    public List<TfpRefund> selectCompleteTfpRefundListByRefundType(Integer refundType) {
        return tfpRefundMapper.selectCompleteTfpRefundListByRefundType(refundType);
    }

    @Override
    public List<TfpRefund> selectCompleteTfpRefundListByOrderIdList(List<Long> orderIdList) {
        return tfpRefundMapper.selectCompleteTfpRefundListByOrderIdList(orderIdList);
    }

    @Override
    public TfpRefund selectCompleteTfpRefundByOrderId(Long orderId) {
        return tfpRefundMapper.selectCompleteTfpRefundByOrderId(orderId);
    }

    private String createSerialNo(Long id) {
        String SerialNo;
        // yyyyMMddHHmmss
        DateFormat yMdDateFormat = DcDateUtils.getDcDateFormat(DcDateUtils.yMdHms);
        String DateFormatStr = yMdDateFormat.format(new Date());

        int idLength = id.toString().length();
        if (idLength > 6){
            SerialNo = DateFormatStr + String.format("%0" + idLength + "d", id);
        } else {
            SerialNo = DateFormatStr + String.format("%06d", id);
        }

        return SerialNo;
    }
}
