package com.ruoyi.service.vo;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 同行人-独立出行对象 tfp_comate
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@Data
public class TfpComateVo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 主用户id */
    private Long mainUserId;

    private String mainNickName;

    private String orderNo;

    /** 同行人姓名 */
    private String mateName;

    /** 证件类型 1.身份证 2.台胞证 3.港澳通行证 4.护照 5.士兵证 */
    private Integer mateCardType;

    /** 证件号码 */
    private String mateCardNum;

    /** 手机号码 */
    private String matePhone;

    /** 此人的user_id(备用) */
    private Long viceUserId;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    private Long createUserId;

    /** 更新者id */
    private Long updateUserId;

}
