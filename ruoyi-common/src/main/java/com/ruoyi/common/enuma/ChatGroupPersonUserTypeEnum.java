package com.ruoyi.common.enuma;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ChatGroupPersonUserTypeEnum implements BaseEnum {
    INITIATOR(0, "发起者"),

    SIGN_UP(1, "已报名"),

    NOT_SIGN_UP(2, "未报名")
    ;


    private Integer value;
    private String desc;
}
