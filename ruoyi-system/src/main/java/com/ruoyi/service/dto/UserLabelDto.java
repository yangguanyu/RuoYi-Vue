package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("UserLabelDto")
public class UserLabelDto {
    @ApiModelProperty("标签类型code")
    private String labelTypeCode;
    @ApiModelProperty("标签类型名称")
    private String labelTypeName;
    @ApiModelProperty("标签code")
    private String labelCode;
    @ApiModelProperty("标签名称")
    private String labelName;
}
