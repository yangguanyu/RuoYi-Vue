package com.ruoyi.service.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 关注对象 sys_user_care
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public class SysUserCare extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 主人用户id */
    @Excel(name = "主人用户id")
    private Long ownerUserId;

    /** 粉丝用户id */
    @Excel(name = "粉丝用户id")
    private Long fansUserId;

    /** 是否互相关注 0-否 1-是 */
    @Excel(name = "是否互相关注 0-否 1-是")
    private Integer mutualAttentionFlag;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    @Excel(name = "创建者id")
    private Long createUserId;

    /** 更新者id */
    @Excel(name = "更新者id")
    private Long updateUserId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setOwnerUserId(Long ownerUserId) 
    {
        this.ownerUserId = ownerUserId;
    }

    public Long getOwnerUserId() 
    {
        return ownerUserId;
    }
    public void setFansUserId(Long fansUserId) 
    {
        this.fansUserId = fansUserId;
    }

    public Long getFansUserId() 
    {
        return fansUserId;
    }
    public void setMutualAttentionFlag(Integer mutualAttentionFlag) 
    {
        this.mutualAttentionFlag = mutualAttentionFlag;
    }

    public Integer getMutualAttentionFlag() 
    {
        return mutualAttentionFlag;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }
    public void setUpdateUserId(Long updateUserId) 
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId() 
    {
        return updateUserId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("ownerUserId", getOwnerUserId())
            .append("fansUserId", getFansUserId())
            .append("mutualAttentionFlag", getMutualAttentionFlag())
            .append("delFlag", getDelFlag())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
