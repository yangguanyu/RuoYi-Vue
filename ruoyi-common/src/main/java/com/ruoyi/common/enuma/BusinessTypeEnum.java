package com.ruoyi.common.enuma;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum BusinessTypeEnum implements BaseStrEnum {
    SYSTEM("system", "系统图片"),
    Carousel_Chart("carousel_chart", "轮播图"),
    USER("user", "用户"),
    FIND_PARTNER("partner", "找搭子"),
    PRODUCT("product", "商品"),
    DRAW_MONEY("draw_money", "提款"),
    REFUND("refund", "退款"),
    AREA("area", "区域"),
    ;

    private String value;
    private String desc;
}
