package com.ruoyi.service.service.impl;

import java.util.*;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enuma.DeleteEnum;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.DcListUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.service.service.IDcTokenService;
import com.ruoyi.service.vo.MyIdentityListVo;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.SysIdentityMapper;
import com.ruoyi.service.domain.SysIdentity;
import com.ruoyi.service.service.ISysIdentityService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 用户身份Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@Service
@Transactional
public class SysIdentityServiceImpl implements ISysIdentityService 
{
    @Autowired
    private SysIdentityMapper sysIdentityMapper;
    @Autowired
    private ISysUserService userService;
    @Autowired
    private IDcTokenService tokenService;

    /**
     * 查询用户身份
     * 
     * @param id 用户身份主键
     * @return 用户身份
     */
    @Override
    public SysIdentity selectSysIdentityById(Long id)
    {
        return sysIdentityMapper.selectSysIdentityById(id);
    }

    /**
     * 查询用户身份列表
     * 
     * @param sysIdentity 用户身份
     * @return 用户身份
     */
    @Override
    public List<SysIdentity> selectSysIdentityList(SysIdentity sysIdentity)
    {
        return sysIdentityMapper.selectSysIdentityList(sysIdentity);
    }

    /**
     * 新增用户身份
     * 
     * @param sysIdentity 用户身份
     * @return 结果
     */
    @Override
    public int insertSysIdentity(SysIdentity sysIdentity)
    {
        sysIdentity.setCreateTime(DateUtils.getNowDate());
        return sysIdentityMapper.insertSysIdentity(sysIdentity);
    }

    /**
     * 修改用户身份
     * 
     * @param sysIdentity 用户身份
     * @return 结果
     */
    @Override
    public int updateSysIdentity(SysIdentity sysIdentity)
    {
        sysIdentity.setUpdateTime(DateUtils.getNowDate());
        return sysIdentityMapper.updateSysIdentity(sysIdentity);
    }

    /**
     * 批量删除用户身份
     * 
     * @param ids 需要删除的用户身份主键
     * @return 结果
     */
    @Override
    public int deleteSysIdentityByIds(Long[] ids)
    {
        return sysIdentityMapper.deleteSysIdentityByIds(ids);
    }

    /**
     * 删除用户身份信息
     * 
     * @param id 用户身份主键
     * @return 结果
     */
    @Override
    public int deleteSysIdentityById(Long id)
    {
        return sysIdentityMapper.deleteSysIdentityById(id);
    }

    @Override
    public Long initIdentity(Long userId, String userName){
        String defaultIdentityCode = "0";
        Long defaultIdentityId = null;

        Map<String, String> defaultIdentityMap = new HashMap<>();
        defaultIdentityMap.put("0", "旅行者");
        defaultIdentityMap.put("1", "推广使者");

        Set<Map.Entry<String, String>> entries = defaultIdentityMap.entrySet();
        for (Map.Entry<String, String> entry : entries) {
            String key = entry.getKey();

            SysIdentity sysIdentity = new SysIdentity();
            sysIdentity.setUserId(userId);
            sysIdentity.setIdentityCode(key);
            sysIdentity.setIdentityName(entry.getValue());
            sysIdentity.setDelFlag(DeleteEnum.EXIST.getValue());
            sysIdentity.setCreateUserId(userId);
            sysIdentity.setCreateBy(userName);
            sysIdentity.setCreateTime(DateUtils.getNowDate());
            sysIdentityMapper.insertSysIdentity(sysIdentity);

            if (Objects.equals(defaultIdentityCode, key)){
                defaultIdentityId = sysIdentity.getId();
            }
        }

        return defaultIdentityId;
    }

    /**
     * 小程序-切换身份
     * @param id
     * @return
     */
    @Override
    public int switchIdentity(Long id) {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = loginUser.getUserId();

        // 更新最新身份id
        SysUser sysUser = userService.selectUserById(userId);
        sysUser.setIdentityId(id);
        sysUser.setUpdateBy(loginUser.getUsername());
        sysUser.setUpdateTime(new Date());
        userService.updateUser(sysUser);

        // 刷新令牌
        loginUser.setIdentityId(id);
        loginUser.setUser(sysUser);
        tokenService.refreshToken(loginUser);

        return 1;
    }

    /**
     * 小程序-我的身份列表
     * @return
     */
    @Override
    public List<MyIdentityListVo> myIdentityList() {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long userId = loginUser.getUserId();

        List<MyIdentityListVo> myIdentityListVoList = new ArrayList<>();

        SysIdentity param = new SysIdentity();
        param.setUserId(userId);
        param.setDelFlag(DeleteEnum.EXIST.getValue());
        List<SysIdentity> sysIdentitieList = sysIdentityMapper.selectSysIdentityList(param);
        if (DcListUtils.isNotEmpty(sysIdentitieList)){
            for (SysIdentity sysIdentity : sysIdentitieList) {
                MyIdentityListVo myIdentityListVo = new MyIdentityListVo();
                myIdentityListVo.setId(sysIdentity.getId());
                myIdentityListVo.setIdentityName(sysIdentity.getIdentityName());
                myIdentityListVoList.add(myIdentityListVo);
            }
        }

        return myIdentityListVoList;
    }
}
