-- tfp_product
ALTER TABLE tfp_product
	ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';

-- tfp_product_sub
ALTER TABLE tfp_product_sub
	ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';

-- tfp_purchase_shop_number
ALTER TABLE tfp_purchase_shop_number
	ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';

-- tfp_self_funded_project
ALTER TABLE tfp_self_funded_project
	ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';

-- tfp_every_day_price
ALTER TABLE tfp_every_day_price
	ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';

-- tfp_supplier
ALTER TABLE tfp_supplier
	ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';

-- tfp_label
ALTER TABLE tfp_label
	ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';

-- tfp_order
ALTER TABLE tfp_order
	ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';

-- tfp_comate
ALTER TABLE tfp_comate
	ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';

-- tfp_comate_order
ALTER TABLE tfp_comate_order
	ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';

-- tfp_product_snapshot
ALTER TABLE tfp_product_snapshot
	ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';

-- tfp_refund
ALTER TABLE tfp_refund
	ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';

-- sys_identity
ALTER TABLE sys_identity
	ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';

-- sys_user_label
ALTER TABLE sys_user_label
	ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';

-- sys_user_icon
ALTER TABLE sys_user_icon
	ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';

-- sys_user_care
ALTER TABLE sys_user_care
	ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';

-- sys_user_visitor_record
ALTER TABLE sys_user_visitor_record
	ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';

-- tfp_chat_group
ALTER TABLE tfp_chat_group
	ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';

-- tfp_chat_group_person
ALTER TABLE tfp_chat_group_person
	ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';

-- tfp_chat_group_person_info
ALTER TABLE tfp_chat_group_person_info
	ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';

-- tfp_user_income
ALTER TABLE tfp_user_income
	ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';

-- tfp_user_income_detail
ALTER TABLE tfp_user_income_detail
	ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';

-- tfp_bank
ALTER TABLE tfp_bank
	ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';

-- tfp_user_bank_card
ALTER TABLE tfp_user_bank_card
	ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';

-- tfp_draw_money
ALTER TABLE tfp_draw_money
	ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';

-- tfp_find_partner
ALTER TABLE tfp_find_partner
	ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';

-- tfp_find_partner_sign_up
ALTER TABLE tfp_find_partner_sign_up
	ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';

-- tfp_find_partner_chat_group
ALTER TABLE tfp_find_partner_chat_group
	ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';

-- tfp_find_partner_chat_group_person
ALTER TABLE tfp_find_partner_chat_group_person
	ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';

-- tfp_find_partner_chat_group_person_info
ALTER TABLE tfp_find_partner_chat_group_person_info
	ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';

-- tfp_cooperative_branch_company
ALTER TABLE tfp_cooperative_branch_company
    ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';

-- tfp_area_navigation
ALTER TABLE tfp_area_navigation
    ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';

-- tfp_qr_code
ALTER TABLE tfp_qr_code
    ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';

-- tfp_qr_code_info
ALTER TABLE tfp_qr_code_info
    ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';

-- tfp_user_share_log
ALTER TABLE tfp_user_share_log
    ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';

-- tfp_attachment
ALTER TABLE tfp_attachment
    ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';

-- tfp_product_want_to_go
ALTER TABLE tfp_product_want_to_go
    ADD `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` datetime DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` bigint(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` datetime DEFAULT NULL COMMENT '更新时间';

-- tfp_product_departure_date
ALTER TABLE tfp_product_departure_date
    ADD `del_flag` CHAR(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
	ADD `create_user_id` BIGINT(20) DEFAULT NULL COMMENT '创建者id',
	ADD `create_by` VARCHAR(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
	ADD `create_time` DATETIME DEFAULT NULL COMMENT '创建时间',
	ADD `update_user_id` BIGINT(20) DEFAULT NULL COMMENT '更新者id',
	ADD `update_by` VARCHAR(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
	ADD `update_time` DATETIME DEFAULT NULL COMMENT '更新时间';