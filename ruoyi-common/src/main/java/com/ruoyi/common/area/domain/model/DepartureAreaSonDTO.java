package com.ruoyi.common.area.domain.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "DepartureAreaSonDTO", description = "出发地子信息")
public class DepartureAreaSonDTO {
    @ApiModelProperty("热门推荐地(hot)/字母(A-Z)")
    private String key;
    @ApiModelProperty("title")
    private String title;
    @ApiModelProperty("区域集合")
    private List<AreaDTO> areaDTOList;
}
