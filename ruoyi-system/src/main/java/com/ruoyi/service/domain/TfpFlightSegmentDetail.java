package com.ruoyi.service.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 通程前后航段信息对象 tfp_flight_segment_detail
 * 
 * @author ruoyi
 * @date 2024-04-10
 */
public class TfpFlightSegmentDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 航班信息表id */
    @Excel(name = "航班信息表id")
    private Long flightId;

    /** 航班号 */
    @Excel(name = "航班号")
    private String flightNo;

    /** 出发城市三字码 */
    @Excel(name = "出发城市三字码")
    private String depCode;

    /** 起飞城市名称 */
    @Excel(name = "起飞城市名称")
    private String depCity;

    /** 出发机场名称 */
    @Excel(name = "出发机场名称")
    private String depAirport;

    /** 抵达城市三字码 */
    @Excel(name = "抵达城市三字码")
    private String arrCode;

    /** 抵达城市名称 */
    @Excel(name = "抵达城市名称")
    private String arrCity;

    /** 抵达机场名称 */
    @Excel(name = "抵达机场名称")
    private String arrAirport;

    /** 航段 */
    @Excel(name = "航段")
    private String segment;

    /** 出发日期 */
    @Excel(name = "出发日期")
    private String depDate;

    /** 出发时间 */
    @Excel(name = "出发时间")
    private String depTime;

    /** 抵达日期 */
    @Excel(name = "抵达日期")
    private String arrDate;

    /** 抵达时间 */
    @Excel(name = "抵达时间")
    private String arrTime;

    /** 航司 */
    @Excel(name = "航司")
    private String carrier;

    /** 出发航站 */
    @Excel(name = "出发航站")
    private String depTerm;

    /** 抵达航站 */
    @Excel(name = "抵达航站")
    private String arrTerm;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    @Excel(name = "创建者id")
    private Long createUserId;

    /** 更新者id */
    @Excel(name = "更新者id")
    private Long updateUserId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setFlightId(Long flightId) 
    {
        this.flightId = flightId;
    }

    public Long getFlightId() 
    {
        return flightId;
    }
    public void setFlightNo(String flightNo) 
    {
        this.flightNo = flightNo;
    }

    public String getFlightNo() 
    {
        return flightNo;
    }
    public void setDepCode(String depCode) 
    {
        this.depCode = depCode;
    }

    public String getDepCode() 
    {
        return depCode;
    }
    public void setDepCity(String depCity) 
    {
        this.depCity = depCity;
    }

    public String getDepCity() 
    {
        return depCity;
    }
    public void setDepAirport(String depAirport) 
    {
        this.depAirport = depAirport;
    }

    public String getDepAirport() 
    {
        return depAirport;
    }
    public void setArrCode(String arrCode) 
    {
        this.arrCode = arrCode;
    }

    public String getArrCode() 
    {
        return arrCode;
    }
    public void setArrCity(String arrCity) 
    {
        this.arrCity = arrCity;
    }

    public String getArrCity() 
    {
        return arrCity;
    }
    public void setArrAirport(String arrAirport) 
    {
        this.arrAirport = arrAirport;
    }

    public String getArrAirport() 
    {
        return arrAirport;
    }
    public void setSegment(String segment) 
    {
        this.segment = segment;
    }

    public String getSegment() 
    {
        return segment;
    }
    public void setDepDate(String depDate) 
    {
        this.depDate = depDate;
    }

    public String getDepDate() 
    {
        return depDate;
    }
    public void setDepTime(String depTime) 
    {
        this.depTime = depTime;
    }

    public String getDepTime() 
    {
        return depTime;
    }
    public void setArrDate(String arrDate) 
    {
        this.arrDate = arrDate;
    }

    public String getArrDate() 
    {
        return arrDate;
    }
    public void setArrTime(String arrTime) 
    {
        this.arrTime = arrTime;
    }

    public String getArrTime() 
    {
        return arrTime;
    }
    public void setCarrier(String carrier) 
    {
        this.carrier = carrier;
    }

    public String getCarrier() 
    {
        return carrier;
    }
    public void setDepTerm(String depTerm) 
    {
        this.depTerm = depTerm;
    }

    public String getDepTerm() 
    {
        return depTerm;
    }
    public void setArrTerm(String arrTerm) 
    {
        this.arrTerm = arrTerm;
    }

    public String getArrTerm() 
    {
        return arrTerm;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }
    public void setUpdateUserId(Long updateUserId) 
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId() 
    {
        return updateUserId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("flightId", getFlightId())
            .append("flightNo", getFlightNo())
            .append("depCode", getDepCode())
            .append("depCity", getDepCity())
            .append("depAirport", getDepAirport())
            .append("arrCode", getArrCode())
            .append("arrCity", getArrCity())
            .append("arrAirport", getArrAirport())
            .append("segment", getSegment())
            .append("depDate", getDepDate())
            .append("depTime", getDepTime())
            .append("arrDate", getArrDate())
            .append("arrTime", getArrTime())
            .append("carrier", getCarrier())
            .append("depTerm", getDepTerm())
            .append("arrTerm", getArrTerm())
            .append("delFlag", getDelFlag())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
