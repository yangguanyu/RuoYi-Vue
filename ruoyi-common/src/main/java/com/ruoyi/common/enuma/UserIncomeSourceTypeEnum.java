package com.ruoyi.common.enuma;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum UserIncomeSourceTypeEnum implements BaseEnum {
    PRODUCT(0, "商品"),
    FIND_PARTNER(1, "搭子")
    ;

    private Integer value;
    private String desc;
}
