package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.TfpQrCodeInfo;

/**
 * 二维码业务数据Mapper接口
 * 
 * @author ruoyi
 * @date 2024-01-19
 */
public interface TfpQrCodeInfoMapper 
{
    /**
     * 查询二维码业务数据
     * 
     * @param id 二维码业务数据主键
     * @return 二维码业务数据
     */
    public TfpQrCodeInfo selectTfpQrCodeInfoById(Long id);

    /**
     * 查询二维码业务数据列表
     * 
     * @param tfpQrCodeInfo 二维码业务数据
     * @return 二维码业务数据集合
     */
    public List<TfpQrCodeInfo> selectTfpQrCodeInfoList(TfpQrCodeInfo tfpQrCodeInfo);

    /**
     * 新增二维码业务数据
     * 
     * @param tfpQrCodeInfo 二维码业务数据
     * @return 结果
     */
    public int insertTfpQrCodeInfo(TfpQrCodeInfo tfpQrCodeInfo);

    /**
     * 修改二维码业务数据
     * 
     * @param tfpQrCodeInfo 二维码业务数据
     * @return 结果
     */
    public int updateTfpQrCodeInfo(TfpQrCodeInfo tfpQrCodeInfo);

    /**
     * 删除二维码业务数据
     * 
     * @param id 二维码业务数据主键
     * @return 结果
     */
    public int deleteTfpQrCodeInfoById(Long id);

    /**
     * 批量删除二维码业务数据
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpQrCodeInfoByIds(Long[] ids);
}
