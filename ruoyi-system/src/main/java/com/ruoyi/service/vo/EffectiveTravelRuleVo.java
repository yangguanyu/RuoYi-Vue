package com.ruoyi.service.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "EffectiveTravelRuleVo")
public class EffectiveTravelRuleVo {
    @ApiModelProperty("规则id")
    private Long ruleId;
    @ApiModelProperty("规则类型 0-组队出行 1-独立出行")
    private Integer travelType;
    @ApiModelProperty("文案")
    private String text;
}
