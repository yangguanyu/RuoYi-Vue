package com.ruoyi.service.vo;

import lombok.Data;

import java.util.List;

@Data
public class CustomerServiceVo {
    private Long supplierId;
    private List<CustomerServiceInfoVo> customerServiceInfoVoList;
}
