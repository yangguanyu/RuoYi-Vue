package com.ruoyi.service.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.github.pagehelper.Page;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 商品主对象 tfp_product
 *
 * @author ruoyi
 * @date 2023-11-01
 */
@Data
@ApiModel("RecommendProductVo 推荐商品返回VO")
public class RecommendProductVo
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 客服id */
    private Long customerServiceId;

    /** 商品归属 0-国内 1-海外 */
    private Integer commoditiesType;

    /** 业务地区(1级) 能够选多个地区 下拉 */
    private Long parentAreaId;

    /** 业务地区(1级) 能够选多个地区 下拉 */
    @ApiModelProperty("业务地区(1级) 能够选多个地区")
    private String parentAreaName;

    /** 业务地区(2级) 能够选多个地区 下拉 */
    private Long areaId;

    /** 业务地区(2级) 能够选多个地区 下拉 */
    @ApiModelProperty("业务地区(2级) 能够选多个地区")
    private String areaName;

    /** 服务类型id 散拼 单团 单项 下拉（怎么和咱们的全国散，一家一团对应） */
    private Long categoryId;

    /** 服务类型名称 散拼 单团 单项 下拉（怎么和咱们的全国散，一家一团对应） */
    @ApiModelProperty("服务类型名称 散拼 单团 单项 下拉（怎么和咱们的全国散，一家一团对应）")
    private String categoryName;

    /** 商品名称 */
    @ApiModelProperty("商品名称")
    private String productName;

    /** 商品编号 */
    @ApiModelProperty("商品编号")
    private String productNo;


    /** 行程天数 几天 */
    @ApiModelProperty("行程天数 几天")
    private Integer dayNum;

    /** 行程天数 几晚 */
    @ApiModelProperty("行程天数 几晚")
    private Integer nightNum;


    /** 团期开始 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date departureDateStart;

    /** 团期截止 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date departureDateEnd;

    /** 分值，定时任务10分钟 */
    @ApiModelProperty("分值，定时任务10分钟")
    private Long score;

    @ApiModelProperty("报名人数")
    private Integer signUpPersonNumber;


    @ApiModelProperty("报名人员头像url集合")
    private List<String> signUpPersonHeadPortraitUrlList;

    @ApiModelProperty("商品封面图片url集合")
    private List<String> productCoverImageUrlList;

}
