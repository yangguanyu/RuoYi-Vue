import request from '@/utils/request'

// 查询用户收益列表
export function listUserIncomeDetail(query) {
  return request({
    url: '/service/userIncomeDetail/list',
    method: 'get',
    params: query
  })
}

// 查询用户收益详细
export function getUserIncomeDetail(id) {
  return request({
    url: '/service/userIncomeDetail/' + id,
    method: 'get'
  })
}

// 新增用户收益
export function addUserIncomeDetail(data) {
  return request({
    url: '/service/userIncomeDetail',
    method: 'post',
    data: data
  })
}

// 修改用户收益
export function updateUserIncomeDetail(data) {
  return request({
    url: '/service/userIncomeDetail',
    method: 'put',
    data: data
  })
}

// 删除用户收益
export function delUserIncomeDetail(id) {
  return request({
    url: '/service/userIncomeDetail/' + id,
    method: 'delete'
  })
}
