package com.ruoyi.service.service;

import java.util.List;

import com.ruoyi.service.domain.TfpProduct;
import com.ruoyi.service.domain.TfpSelfFundedProject;
import com.ruoyi.service.dto.TfpSelfFundedProjectDto;

/**
 * 自费项目Service接口
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public interface ITfpSelfFundedProjectService 
{
    /**
     * 查询自费项目
     * 
     * @param id 自费项目主键
     * @return 自费项目
     */
    public TfpSelfFundedProject selectTfpSelfFundedProjectById(Long id);

    /**
     * 查询自费项目列表
     * 
     * @param tfpSelfFundedProject 自费项目
     * @return 自费项目集合
     */
    public List<TfpSelfFundedProject> selectTfpSelfFundedProjectList(TfpSelfFundedProject tfpSelfFundedProject);

    /**
     * 新增自费项目
     * 
     * @param tfpSelfFundedProject 自费项目
     * @return 结果
     */
    public int insertTfpSelfFundedProject(TfpSelfFundedProject tfpSelfFundedProject);

    /**
     * 修改自费项目
     * 
     * @param tfpSelfFundedProject 自费项目
     * @return 结果
     */
    public int updateTfpSelfFundedProject(TfpSelfFundedProject tfpSelfFundedProject);

    /**
     * 批量删除自费项目
     * 
     * @param ids 需要删除的自费项目主键集合
     * @return 结果
     */
    public int deleteTfpSelfFundedProjectByIds(Long[] ids);

    /**
     * 删除自费项目信息
     * 
     * @param id 自费项目主键
     * @return 结果
     */
    public int deleteTfpSelfFundedProjectById(Long id);

    void deleteByProductId(Long productId);

    void saveList(Long productId, List<TfpSelfFundedProjectDto> selfFundedProjectList);

    List<TfpSelfFundedProject> selectListByProductId(Long productId);

    void deleteTfpSelfFundedProjectByProductIds(List<Long> productIdList);
}
