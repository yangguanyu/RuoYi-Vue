package com.ruoyi.service.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "UserVo", description = "用户信息")
public class UserVo {
    @ApiModelProperty("说话人的id")
    private Long id;
    @ApiModelProperty("昵称")
    private String name;
    @ApiModelProperty("头像")
    private String avatar;

}
