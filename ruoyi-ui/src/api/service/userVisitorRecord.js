import request from '@/utils/request'

// 查询访客记录列表
export function listUserVisitorRecord(query) {
  return request({
    url: '/service/userVisitorRecord/list',
    method: 'get',
    params: query
  })
}

// 查询访客记录详细
export function getUserVisitorRecord(id) {
  return request({
    url: '/service/userVisitorRecord/' + id,
    method: 'get'
  })
}

// 新增访客记录
export function addUserVisitorRecord(data) {
  return request({
    url: '/service/userVisitorRecord',
    method: 'post',
    data: data
  })
}

// 修改访客记录
export function updateUserVisitorRecord(data) {
  return request({
    url: '/service/userVisitorRecord',
    method: 'put',
    data: data
  })
}

// 删除访客记录
export function delUserVisitorRecord(id) {
  return request({
    url: '/service/userVisitorRecord/' + id,
    method: 'delete'
  })
}
