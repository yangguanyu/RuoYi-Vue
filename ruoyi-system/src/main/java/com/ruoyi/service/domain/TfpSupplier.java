package com.ruoyi.service.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 供应商信息对象 tfp_supplier
 * 
 * @author ruoyi
 * @date 2023-11-01
 */
public class TfpSupplier extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 供应商名称 */
    @Excel(name = "供应商名称")
    private String supplierName;

    /** 统一识别编码 */
    @Excel(name = "统一识别编码")
    private String unifiedIdentificationCode;

    /** 供应商地址 */
    @Excel(name = "供应商地址")
    private String supplierAddress;

    /** 省的id */
    @Excel(name = "省的id")
    private Long provinceId;

    /** 省的名称 */
    @Excel(name = "省的名称")
    private String provinceName;

    /** 市的id */
    @Excel(name = "市的id")
    private Long urbanId;

    /** 市的名称 */
    @Excel(name = "市的名称")
    private String urbanName;

    /** 区的id */
    @Excel(name = "区的id")
    private Long areaId;

    /** 区的名称 */
    @Excel(name = "区的名称")
    private String areaName;

    /** 介绍 */
    @Excel(name = "介绍")
    private String introduce;

    /** 咨询电话 */
    @Excel(name = "咨询电话")
    private String phoneNumber;

    /** 供应商银行账户名 */
    @Excel(name = "供应商银行账户名")
    private String supplierBankAccountName;

    /** 供应商银行账号 */
    @Excel(name = "供应商银行账号")
    private String supplierBankAccountNumber;

    /** 开户行 */
    @Excel(name = "开户行")
    private String accountOpeningBankName;

    /** 供应商结算方式 0-先款 1-先定金出行后结清尾款 2-月付 3-季度付 4-年付 */
    @Excel(name = "供应商结算方式 0-先款 1-先定金出行后结清尾款 2-月付 3-季度付 4-年付")
    private Integer settlementMethod;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    @Excel(name = "创建者id")
    private Long createUserId;

    /** 更新者id */
    @Excel(name = "更新者id")
    private Long updateUserId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setSupplierName(String supplierName) 
    {
        this.supplierName = supplierName;
    }

    public String getSupplierName() 
    {
        return supplierName;
    }
    public void setUnifiedIdentificationCode(String unifiedIdentificationCode) 
    {
        this.unifiedIdentificationCode = unifiedIdentificationCode;
    }

    public String getUnifiedIdentificationCode() 
    {
        return unifiedIdentificationCode;
    }
    public void setSupplierAddress(String supplierAddress) 
    {
        this.supplierAddress = supplierAddress;
    }

    public String getSupplierAddress() 
    {
        return supplierAddress;
    }
    public void setProvinceId(Long provinceId) 
    {
        this.provinceId = provinceId;
    }

    public Long getProvinceId() 
    {
        return provinceId;
    }
    public void setProvinceName(String provinceName) 
    {
        this.provinceName = provinceName;
    }

    public String getProvinceName() 
    {
        return provinceName;
    }
    public void setUrbanId(Long urbanId) 
    {
        this.urbanId = urbanId;
    }

    public Long getUrbanId() 
    {
        return urbanId;
    }
    public void setUrbanName(String urbanName) 
    {
        this.urbanName = urbanName;
    }

    public String getUrbanName() 
    {
        return urbanName;
    }
    public void setAreaId(Long areaId) 
    {
        this.areaId = areaId;
    }

    public Long getAreaId() 
    {
        return areaId;
    }
    public void setAreaName(String areaName) 
    {
        this.areaName = areaName;
    }

    public String getAreaName() 
    {
        return areaName;
    }
    public void setIntroduce(String introduce) 
    {
        this.introduce = introduce;
    }

    public String getIntroduce() 
    {
        return introduce;
    }
    public void setPhoneNumber(String phoneNumber) 
    {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber() 
    {
        return phoneNumber;
    }
    public void setSupplierBankAccountName(String supplierBankAccountName) 
    {
        this.supplierBankAccountName = supplierBankAccountName;
    }

    public String getSupplierBankAccountName() 
    {
        return supplierBankAccountName;
    }
    public void setSupplierBankAccountNumber(String supplierBankAccountNumber) 
    {
        this.supplierBankAccountNumber = supplierBankAccountNumber;
    }

    public String getSupplierBankAccountNumber() 
    {
        return supplierBankAccountNumber;
    }
    public void setAccountOpeningBankName(String accountOpeningBankName) 
    {
        this.accountOpeningBankName = accountOpeningBankName;
    }

    public String getAccountOpeningBankName() 
    {
        return accountOpeningBankName;
    }
    public void setSettlementMethod(Integer settlementMethod) 
    {
        this.settlementMethod = settlementMethod;
    }

    public Integer getSettlementMethod() 
    {
        return settlementMethod;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }
    public void setUpdateUserId(Long updateUserId) 
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId() 
    {
        return updateUserId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("supplierName", getSupplierName())
            .append("unifiedIdentificationCode", getUnifiedIdentificationCode())
            .append("supplierAddress", getSupplierAddress())
            .append("provinceId", getProvinceId())
            .append("provinceName", getProvinceName())
            .append("urbanId", getUrbanId())
            .append("urbanName", getUrbanName())
            .append("areaId", getAreaId())
            .append("areaName", getAreaName())
            .append("introduce", getIntroduce())
            .append("phoneNumber", getPhoneNumber())
            .append("supplierBankAccountName", getSupplierBankAccountName())
            .append("supplierBankAccountNumber", getSupplierBankAccountNumber())
            .append("accountOpeningBankName", getAccountOpeningBankName())
            .append("settlementMethod", getSettlementMethod())
            .append("delFlag", getDelFlag())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
