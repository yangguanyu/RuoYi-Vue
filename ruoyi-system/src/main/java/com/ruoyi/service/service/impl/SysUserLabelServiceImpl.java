package com.ruoyi.service.service.impl;

import java.util.List;

import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enuma.DeleteEnum;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.DcListUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.service.dto.UserLabelDto;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.SysUserLabelMapper;
import com.ruoyi.service.domain.SysUserLabel;
import com.ruoyi.service.service.ISysUserLabelService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 用户个性标签Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@Service
@Transactional
public class SysUserLabelServiceImpl implements ISysUserLabelService 
{
    @Autowired
    private SysUserLabelMapper sysUserLabelMapper;

    /**
     * 查询用户个性标签
     * 
     * @param id 用户个性标签主键
     * @return 用户个性标签
     */
    @Override
    public SysUserLabel selectSysUserLabelById(Long id)
    {
        return sysUserLabelMapper.selectSysUserLabelById(id);
    }

    /**
     * 查询用户个性标签列表
     * 
     * @param sysUserLabel 用户个性标签
     * @return 用户个性标签
     */
    @Override
    public List<SysUserLabel> selectSysUserLabelList(SysUserLabel sysUserLabel)
    {
        return sysUserLabelMapper.selectSysUserLabelList(sysUserLabel);
    }

    /**
     * 新增用户个性标签
     * 
     * @param sysUserLabel 用户个性标签
     * @return 结果
     */
    @Override
    public int insertSysUserLabel(SysUserLabel sysUserLabel)
    {
        sysUserLabel.setCreateTime(DateUtils.getNowDate());
        return sysUserLabelMapper.insertSysUserLabel(sysUserLabel);
    }

    /**
     * 修改用户个性标签
     * 
     * @param sysUserLabel 用户个性标签
     * @return 结果
     */
    @Override
    public int updateSysUserLabel(SysUserLabel sysUserLabel)
    {
        sysUserLabel.setUpdateTime(DateUtils.getNowDate());
        return sysUserLabelMapper.updateSysUserLabel(sysUserLabel);
    }

    /**
     * 批量删除用户个性标签
     * 
     * @param ids 需要删除的用户个性标签主键
     * @return 结果
     */
    @Override
    public int deleteSysUserLabelByIds(Long[] ids)
    {
        return sysUserLabelMapper.deleteSysUserLabelByIds(ids);
    }

    /**
     * 删除用户个性标签信息
     * 
     * @param id 用户个性标签主键
     * @return 结果
     */
    @Override
    public int deleteSysUserLabelById(Long id)
    {
        return sysUserLabelMapper.deleteSysUserLabelById(id);
    }

    @Override
    public void saveList(Long userId, List<UserLabelDto> userLabelList) {
        sysUserLabelMapper.deleteSysUserLabelByUserId(userId);

        if (DcListUtils.isNotEmpty(userLabelList)){
            LoginUser loginUser = SecurityUtils.getLoginUser();

            for (UserLabelDto userLabelDto : userLabelList) {
                SysUserLabel sysUserLabel = new SysUserLabel();
                BeanUtils.copyProperties(userLabelDto, sysUserLabel);
                sysUserLabel.setUserId(userId);
                sysUserLabel.setDelFlag(DeleteEnum.EXIST.getValue());
                sysUserLabel.setCreateUserId(loginUser.getUserId());
                sysUserLabel.setCreateBy(loginUser.getUsername());
                sysUserLabel.setCreateTime(DateUtils.getNowDate());
                sysUserLabelMapper.insertSysUserLabel(sysUserLabel);
            }
        }
    }
}
