package com.ruoyi.common.enuma.flight;

import com.ruoyi.common.enuma.BaseStrEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RefundExceptionCode implements BaseStrEnum {
    PARAMETER_NAME_ERROR("011211", "参数名错误"),
    NOT_ALLOWED("011201", "NO_ALLOWED"),
    ATTACHMENT_PATH_REQUIRED("011210", "attachmentPath不能为空"),
    DIFFERENT_REFUND_TYPES("011204", "申请的退票类型不相同"),
    TICKET_NUMBER_MISMATCH("011205", "申请票号与订单票号不符"),
    ONGOING_TICKET_CHANGE("011213", "申请退度票的客票中有正在进行中的客票变更"),
    TICKET_EXPIRED("011202", "该客票已经过了有效期"),
    ONGOING_REFUND_PROCESS("011203", "申请退度票的客票中有正在进行中的客票退款单"),
    UPGRADE_REQUIRES_NEW_TICKET("011214", "升舱换开乘客需新票号申请退票"),
    ILLEGAL_PROFIT_PARAMETER("011208", "外部利润参数不合法"),
    MISSING_PARAMETER("011209", "缺少参数"),
    TICKET_STATUS_ABNORMAL("011206", "票号状态异常，不能提交退度票"),
    REQUIRED_PROFIT_REFUND("011207", "外部要求退款利润不能为空");

    private final String value;
    private final String desc;




}
