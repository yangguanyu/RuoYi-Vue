package com.ruoyi.service.service.impl;

import java.util.*;
import java.util.stream.Collectors;

import com.ruoyi.common.constant.CommonConstants;
import com.ruoyi.common.enuma.DeleteEnum;
import com.ruoyi.common.exception.BusinessException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.DcListUtils;
import com.ruoyi.common.utils.bean.CustomizeBeanCopier;
import com.ruoyi.service.domain.TfpChatGroup;
import com.ruoyi.service.domain.TfpProductWantToGo;
import com.ruoyi.service.service.ITfpChatGroupService;
import com.ruoyi.service.service.ITfpProductWantToGoService;
import com.ruoyi.service.vo.groupCountPersonVo;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpChatGroupPersonMapper;
import com.ruoyi.service.domain.TfpChatGroupPerson;
import com.ruoyi.service.service.ITfpChatGroupPersonService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 聊天群组成员Service业务层处理
 *
 * @author ruoyi
 * @date 2023-10-31
 */
@Service
@Transactional
public class TfpChatGroupPersonServiceImpl implements ITfpChatGroupPersonService
{
    @Autowired
    private TfpChatGroupPersonMapper tfpChatGroupPersonMapper;

    @Autowired
    private ITfpProductWantToGoService wantToGoService;

    @Autowired
    private ITfpChatGroupService chatGroupService;
    /**
     * 查询聊天群组成员
     *
     * @param id 聊天群组成员主键
     * @return 聊天群组成员
     */
    @Override
    public TfpChatGroupPerson selectTfpChatGroupPersonById(Long id)
    {
        return tfpChatGroupPersonMapper.selectTfpChatGroupPersonById(id);
    }

    /**
     * 查询聊天群组成员列表
     *
     * @param tfpChatGroupPerson 聊天群组成员
     * @return 聊天群组成员
     */
    @Override
    public List<TfpChatGroupPerson> selectTfpChatGroupPersonList(TfpChatGroupPerson tfpChatGroupPerson)
    {
        return tfpChatGroupPersonMapper.selectTfpChatGroupPersonList(tfpChatGroupPerson);
    }

    /**
     * 查看已报名的群组成员
     *
     * @param groupIds 群组id
     * @return 聊天群组成员
     */
    @Override
    public List<TfpChatGroupPerson> selectListByGroupIdAndSignUp(List<Long> groupIds)
    {
        return tfpChatGroupPersonMapper.selectListByGroupIdAndSignUp(groupIds);
    }


    @Override
    public TfpChatGroupPerson selectOneByUserIdAndGroupId(Long userId,Long groupId)
    {
        return tfpChatGroupPersonMapper.selectOneByUserIdAndGroupId(userId,groupId);
    }

    /**
     * 新增聊天群组成员
     *
     * @param tfpChatGroupPerson 聊天群组成员
     * @return 结果
     */
    @Override
    public int insertTfpChatGroupPerson(TfpChatGroupPerson tfpChatGroupPerson)
    {
        tfpChatGroupPerson.setCreateTime(DateUtils.getNowDate());
        return tfpChatGroupPersonMapper.insertTfpChatGroupPerson(tfpChatGroupPerson);
    }

    @Override
    public int appletInsert(TfpChatGroupPerson tfpChatGroupPerson)
    {
        //创建我想去的数据
        TfpProductWantToGo wannerGo = CustomizeBeanCopier.copyProperties(tfpChatGroupPerson,TfpProductWantToGo.class);
        wannerGo.setDelFlag(DeleteEnum.EXIST.getValue());
        int wantResult = wantToGoService.insertTfpProductWantToGo(wannerGo);
        //先查看是否有对应的群组
        TfpChatGroup group = new TfpChatGroup();
        group.setProductId(tfpChatGroupPerson.getProductId());
        List<TfpChatGroup> tfpChatGroups = chatGroupService.selectTfpChatGroupList(group);
        //假如群组，添加进去
        if(CollectionUtils.isEmpty(tfpChatGroups)){
            return wantResult;
        }
        Long chatGroupId = Long.valueOf(CommonConstants.ZERO);
        if(tfpChatGroups.size()> CommonConstants.ONE){
            //取出最新的群组
            Optional<TfpChatGroup> firstGroup = tfpChatGroups.stream().sorted(Comparator.comparing(TfpChatGroup::getCreateTime).reversed()).findFirst();
            if(firstGroup.isPresent()){
                chatGroupId = firstGroup.get().getId();
            }
        }else {
            chatGroupId = tfpChatGroups.get(CommonConstants.ZERO).getId();
        }
        if(Long.valueOf(CommonConstants.ZERO).equals(chatGroupId)){
            throw new BusinessException("未获取到对应的群组ID");
        }
        tfpChatGroupPerson.setCreateTime(DateUtils.getNowDate());
        return tfpChatGroupPersonMapper.insertTfpChatGroupPerson(tfpChatGroupPerson);
    }

    /**
     * 修改聊天群组成员
     *
     * @param tfpChatGroupPerson 聊天群组成员
     * @return 结果
     */
    @Override
    public int updateTfpChatGroupPerson(TfpChatGroupPerson tfpChatGroupPerson)
    {
        tfpChatGroupPerson.setUpdateTime(DateUtils.getNowDate());
        return tfpChatGroupPersonMapper.updateTfpChatGroupPerson(tfpChatGroupPerson);
    }

    /**
     * 批量删除聊天群组成员
     *
     * @param ids 需要删除的聊天群组成员主键
     * @return 结果
     */
    @Override
    public int deleteTfpChatGroupPersonByIds(Long[] ids)
    {
        return tfpChatGroupPersonMapper.deleteTfpChatGroupPersonByIds(ids);
    }

    /**
     * 删除聊天群组成员信息
     *
     * @param id 聊天群组成员主键
     * @return 结果
     */
    @Override
    public int deleteTfpChatGroupPersonById(Long id)
    {
        return tfpChatGroupPersonMapper.deleteTfpChatGroupPersonById(id);
    }

    @Override
    public List<TfpChatGroupPerson> selectTfpChatGroupPersonByIdList(List<Long> idList) {
        return tfpChatGroupPersonMapper.selectTfpChatGroupPersonByIdList(idList);
    }

    @Override
    public Map<Long,Integer>  selectCountPersonByGroupIdList(List<Long> idList) {
        Map<Long,Integer> map;
        List<groupCountPersonVo> groupCountPersonVoList = tfpChatGroupPersonMapper.selectCountPersonByGroupIdList(idList);
        if (DcListUtils.isNotEmpty(groupCountPersonVoList)){
            map = groupCountPersonVoList.stream().collect(Collectors.toMap(groupCountPersonVo::getGroupId, groupCountPersonVo::getNum));
        } else {
            map = new HashMap<>();
        }

        return map;
    }

    @Override
    public List<Long> selectChatGroupPersonIdByUserId(Long userId) {
        return tfpChatGroupPersonMapper.selectChatGroupPersonIdByUserId(userId);
    }

    @Override
    public List<TfpChatGroupPerson> selectTfpChatGroupPersonListByGroupIdList(List<Long> groupIdList) {
        return tfpChatGroupPersonMapper.selectTfpChatGroupPersonListByGroupIdList(groupIdList);
    }

    @Override
    public List<TfpChatGroupPerson> getSignUpPersonInfoListByProductIdList(List<Long> productIdList) {
        return tfpChatGroupPersonMapper.getSignUpPersonInfoListByProductIdList(productIdList);
    }

    @Override
    public TfpChatGroupPerson selectTfpChatGroupPersonByGroupIdAndUserId(Long groupId, Long userId) {
        return tfpChatGroupPersonMapper.selectTfpChatGroupPersonByGroupIdAndUserId(groupId, userId);
    }


}
