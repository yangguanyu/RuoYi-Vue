import request from '@/utils/request'

// 查询找搭子聊天群组成员聊天信息列表
export function listFindPartnerChatGroupPersonInfo(query) {
  return request({
    url: '/service/findPartnerChatGroupPersonInfo/list',
    method: 'get',
    params: query
  })
}

// 查询找搭子聊天群组成员聊天信息详细
export function getFindPartnerChatGroupPersonInfo(id) {
  return request({
    url: '/service/findPartnerChatGroupPersonInfo/' + id,
    method: 'get'
  })
}

// 新增找搭子聊天群组成员聊天信息
export function addFindPartnerChatGroupPersonInfo(data) {
  return request({
    url: '/service/findPartnerChatGroupPersonInfo',
    method: 'post',
    data: data
  })
}

// 修改找搭子聊天群组成员聊天信息
export function updateFindPartnerChatGroupPersonInfo(data) {
  return request({
    url: '/service/findPartnerChatGroupPersonInfo',
    method: 'put',
    data: data
  })
}

// 删除找搭子聊天群组成员聊天信息
export function delFindPartnerChatGroupPersonInfo(id) {
  return request({
    url: '/service/findPartnerChatGroupPersonInfo/' + id,
    method: 'delete'
  })
}
