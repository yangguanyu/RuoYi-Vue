package com.ruoyi.service.service;

import java.math.BigDecimal;
import java.util.List;
import com.ruoyi.service.domain.TfpDrawMoney;
import com.ruoyi.service.dto.ApplyDrawMoneyDto;
import com.ruoyi.service.dto.AuditDto;
import com.ruoyi.service.dto.DrawMoneyAuditListDto;
import com.ruoyi.service.vo.*;

/**
 * 提款Service接口
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public interface ITfpDrawMoneyService 
{
    /**
     * 查询提款
     * 
     * @param id 提款主键
     * @return 提款
     */
    public TfpDrawMoney selectTfpDrawMoneyById(Long id);

    /**
     * 查询提款列表
     * 
     * @param tfpDrawMoney 提款
     * @return 提款集合
     */
    public List<TfpDrawMoney> selectTfpDrawMoneyList(TfpDrawMoney tfpDrawMoney);

    /**
     * 新增提款
     * 
     * @param tfpDrawMoney 提款
     * @return 结果
     */
    public int insertTfpDrawMoney(TfpDrawMoney tfpDrawMoney);

    /**
     * 修改提款
     * 
     * @param tfpDrawMoney 提款
     * @return 结果
     */
    public int updateTfpDrawMoney(TfpDrawMoney tfpDrawMoney);

    /**
     * 批量删除提款
     * 
     * @param ids 需要删除的提款主键集合
     * @return 结果
     */
    public int deleteTfpDrawMoneyByIds(Long[] ids);

    /**
     * 删除提款信息
     * 
     * @param id 提款主键
     * @return 结果
     */
    public int deleteTfpDrawMoneyById(Long id);

    /**
     * 获取提现记录数量
     * @param userId
     * @return
     */
    Integer getDrawMoneyRecordSizeByUserId(Long userId);

    /**
     * 小程序-提现列表
     * @return
     */
    List<DrawMoneyListVo> drawMoneyList();

    Long applyDrawMoney(ApplyDrawMoneyDto applyDrawMoneyDto);

    DetailDrawMoneyVo detailDrawMoney(Long id);

    int auditDrawMoney(AuditDto auditDto);

    List<DrawMoneyAuditListVo> drawMoneyAuditList(DrawMoneyAuditListDto drawMoneyAuditListDto);

    DrawMoneyDetailVo drawMoneyDetail(Long id);

    BigDecimal getUserCumulativeIncome();

    List<UserIncomeDetailListVo> userIncomeDetailList();
}
