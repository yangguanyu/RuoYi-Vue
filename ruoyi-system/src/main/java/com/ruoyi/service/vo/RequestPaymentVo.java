package com.ruoyi.service.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(value = "RequestPaymentVo", description = "请求支付vo")
public class RequestPaymentVo {
    @ApiModelProperty("时间戳")
    private String timeStampStr;
    @ApiModelProperty("随机字符串")
    private String nonceStr;
    @ApiModelProperty("订单详情扩展字符串")
    private String packageStr;
    @ApiModelProperty("签名方式")
    private String signType;
    @ApiModelProperty("签名")
    private String paySign;
}
