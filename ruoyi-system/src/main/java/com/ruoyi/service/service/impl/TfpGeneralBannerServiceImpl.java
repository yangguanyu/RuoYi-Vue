package com.ruoyi.service.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpGeneralBannerMapper;
import com.ruoyi.service.domain.TfpGeneralBanner;
import com.ruoyi.service.service.ITfpGeneralBannerService;
import org.springframework.transaction.annotation.Transactional;

/**
 * bannerService业务层处理
 * 
 * @author ruoyi
 * @date 2023-11-01
 */
@Service
@Transactional
public class TfpGeneralBannerServiceImpl implements ITfpGeneralBannerService 
{
    @Autowired
    private TfpGeneralBannerMapper tfpGeneralBannerMapper;

    /**
     * 查询banner
     * 
     * @param id banner主键
     * @return banner
     */
    @Override
    public TfpGeneralBanner selectTfpGeneralBannerById(Long id)
    {
        return tfpGeneralBannerMapper.selectTfpGeneralBannerById(id);
    }

    /**
     * 查询banner列表
     * 
     * @param tfpGeneralBanner banner
     * @return banner
     */
    @Override
    public List<TfpGeneralBanner> selectTfpGeneralBannerList(TfpGeneralBanner tfpGeneralBanner)
    {
        return tfpGeneralBannerMapper.selectTfpGeneralBannerList(tfpGeneralBanner);
    }

    /**
     * 新增banner
     * 
     * @param tfpGeneralBanner banner
     * @return 结果
     */
    @Override
    public int insertTfpGeneralBanner(TfpGeneralBanner tfpGeneralBanner)
    {
        tfpGeneralBanner.setCreateTime(DateUtils.getNowDate());
        return tfpGeneralBannerMapper.insertTfpGeneralBanner(tfpGeneralBanner);
    }

    /**
     * 修改banner
     * 
     * @param tfpGeneralBanner banner
     * @return 结果
     */
    @Override
    public int updateTfpGeneralBanner(TfpGeneralBanner tfpGeneralBanner)
    {
        tfpGeneralBanner.setUpdateTime(DateUtils.getNowDate());
        return tfpGeneralBannerMapper.updateTfpGeneralBanner(tfpGeneralBanner);
    }

    /**
     * 批量删除banner
     * 
     * @param ids 需要删除的banner主键
     * @return 结果
     */
    @Override
    public int deleteTfpGeneralBannerByIds(Long[] ids)
    {
        return tfpGeneralBannerMapper.deleteTfpGeneralBannerByIds(ids);
    }

    /**
     * 删除banner信息
     * 
     * @param id banner主键
     * @return 结果
     */
    @Override
    public int deleteTfpGeneralBannerById(Long id)
    {
        return tfpGeneralBannerMapper.deleteTfpGeneralBannerById(id);
    }
}
