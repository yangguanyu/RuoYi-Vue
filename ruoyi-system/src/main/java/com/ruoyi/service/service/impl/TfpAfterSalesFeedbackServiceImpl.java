package com.ruoyi.service.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpAfterSalesFeedbackMapper;
import com.ruoyi.service.domain.TfpAfterSalesFeedback;
import com.ruoyi.service.service.ITfpAfterSalesFeedbackService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 售后反馈Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-11-01
 */
@Service
@Transactional
public class TfpAfterSalesFeedbackServiceImpl implements ITfpAfterSalesFeedbackService 
{
    @Autowired
    private TfpAfterSalesFeedbackMapper tfpAfterSalesFeedbackMapper;

    /**
     * 查询售后反馈
     * 
     * @param id 售后反馈主键
     * @return 售后反馈
     */
    @Override
    public TfpAfterSalesFeedback selectTfpAfterSalesFeedbackById(Long id)
    {
        return tfpAfterSalesFeedbackMapper.selectTfpAfterSalesFeedbackById(id);
    }

    /**
     * 查询售后反馈列表
     * 
     * @param tfpAfterSalesFeedback 售后反馈
     * @return 售后反馈
     */
    @Override
    public List<TfpAfterSalesFeedback> selectTfpAfterSalesFeedbackList(TfpAfterSalesFeedback tfpAfterSalesFeedback)
    {
        return tfpAfterSalesFeedbackMapper.selectTfpAfterSalesFeedbackList(tfpAfterSalesFeedback);
    }

    /**
     * 新增售后反馈
     * 
     * @param tfpAfterSalesFeedback 售后反馈
     * @return 结果
     */
    @Override
    public int insertTfpAfterSalesFeedback(TfpAfterSalesFeedback tfpAfterSalesFeedback)
    {
        tfpAfterSalesFeedback.setCreateTime(DateUtils.getNowDate());
        return tfpAfterSalesFeedbackMapper.insertTfpAfterSalesFeedback(tfpAfterSalesFeedback);
    }

    /**
     * 修改售后反馈
     * 
     * @param tfpAfterSalesFeedback 售后反馈
     * @return 结果
     */
    @Override
    public int updateTfpAfterSalesFeedback(TfpAfterSalesFeedback tfpAfterSalesFeedback)
    {
        tfpAfterSalesFeedback.setUpdateTime(DateUtils.getNowDate());
        return tfpAfterSalesFeedbackMapper.updateTfpAfterSalesFeedback(tfpAfterSalesFeedback);
    }

    /**
     * 批量删除售后反馈
     * 
     * @param ids 需要删除的售后反馈主键
     * @return 结果
     */
    @Override
    public int deleteTfpAfterSalesFeedbackByIds(Long[] ids)
    {
        return tfpAfterSalesFeedbackMapper.deleteTfpAfterSalesFeedbackByIds(ids);
    }

    /**
     * 删除售后反馈信息
     * 
     * @param id 售后反馈主键
     * @return 结果
     */
    @Override
    public int deleteTfpAfterSalesFeedbackById(Long id)
    {
        return tfpAfterSalesFeedbackMapper.deleteTfpAfterSalesFeedbackById(id);
    }
}
