package com.ruoyi.service.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 标签对象 tfp_label
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public class TfpLabel extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 标签code */
    @Excel(name = "标签code")
    private String labelCode;

    /** 标签名称 */
    @Excel(name = "标签名称")
    private String labelName;

    /** 标签类型 1商品 2订单 (目前只有商品用) */
    @Excel(name = "标签类型 1商品 2订单 (目前只有商品用)")
    private Integer labelType;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    @Excel(name = "创建者id")
    private Long createUserId;

    /** 更新者id */
    @Excel(name = "更新者id")
    private Long updateUserId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setLabelCode(String labelCode) 
    {
        this.labelCode = labelCode;
    }

    public String getLabelCode() 
    {
        return labelCode;
    }
    public void setLabelName(String labelName) 
    {
        this.labelName = labelName;
    }

    public String getLabelName() 
    {
        return labelName;
    }
    public void setLabelType(Integer labelType) 
    {
        this.labelType = labelType;
    }

    public Integer getLabelType() 
    {
        return labelType;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }
    public void setUpdateUserId(Long updateUserId) 
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId() 
    {
        return updateUserId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("labelCode", getLabelCode())
            .append("labelName", getLabelName())
            .append("labelType", getLabelType())
            .append("remark", getRemark())
            .append("delFlag", getDelFlag())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
