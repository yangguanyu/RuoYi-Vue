package com.ruoyi.service.excel;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.enuma.OrderStatusEnum;
import com.ruoyi.common.enuma.RefundTypeEnum;
import com.ruoyi.common.enuma.YesOrNoFlagEnum;
import com.ruoyi.service.vo.OrderListComateInfoVo;
import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Data
public class OrderListExportExcel {
    @Excel(name = "订单id")
    private Long id;
    @Excel(name = "订单编号")
    private String orderNo;
    @Excel(name = "出发时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date goDate;
    @Excel(name = "订单状态 0-待支付 1-已完成 2-待确认(已支付) 3-待出行 4-申请退款-审核中 5-申请退款-退款成功 6-过期 7-取消")
    private Integer status;
    @Excel(name = "订单金额")
    private BigDecimal finalAmount;

    @Excel(name = "商品名称")
    private String productName;
    @Excel(name = "出行人信息")
    private String comateUserInfo;

    @Excel(name = "支付时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date payTime;
    @Excel(name = "支付状态")
    private String payStatusName;
    @Excel(name = "退款类型")
    private String refundTypeName;

    // 支付状态 0-未支付 1-已支付
    private Integer payStatus;
    // 退款类型 0-退款待审核 1-整单退 2-部分退 3-否
    private Integer refundType;

    public Integer getPayStatus(){
        List<Integer> payStatusYesList = new ArrayList<>();
        payStatusYesList.add(OrderStatusEnum.COMPLETE.getValue());
        payStatusYesList.add(OrderStatusEnum.WAIT_CONFIRMED.getValue());
        payStatusYesList.add(OrderStatusEnum.WAIT_TRAVEL.getValue());
        payStatusYesList.add(OrderStatusEnum.APPLY_REFUND_AUDITING.getValue());
        payStatusYesList.add(OrderStatusEnum.APPLY_REFUND_SUCCESS.getValue());

        if (payStatusYesList.contains(this.status)){
            return YesOrNoFlagEnum.YES.getValue();
        } else {
            return YesOrNoFlagEnum.NO.getValue();
        }
    }

    public String getPayStatusName(){
        Integer payStatus = this.getPayStatus();
        if (Objects.equals(payStatus, YesOrNoFlagEnum.YES.getValue())){
            return "已支付";
        } else {
            return "未支付";
        }
    }

    public String getRefundTypeName(){
        if (Objects.equals(this.refundType, 0)){
            return "退款待审核";
        } else if (Objects.equals(this.refundType, RefundTypeEnum.ALL_REFUND.getValue())) {
            return "整单退";
        } else if (Objects.equals(this.refundType, RefundTypeEnum.PART_REFUND.getValue())) {
            return "部分退";
        } else {
            return "否";
        }
    }
}
