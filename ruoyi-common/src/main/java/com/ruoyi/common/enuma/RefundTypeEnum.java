package com.ruoyi.common.enuma;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RefundTypeEnum implements BaseEnum {

    ALL_REFUND(1, "全额退款"),
    PART_REFUND(2, "部分退款")
    ;

    private Integer value;
    private String desc;
}
