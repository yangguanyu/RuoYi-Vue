package com.ruoyi.service.service;

import java.util.List;
import com.ruoyi.service.domain.TfpSupplier;
import com.ruoyi.service.vo.SupplierVo;

/**
 * 供应商信息Service接口
 * 
 * @author ruoyi
 * @date 2023-11-01
 */
public interface ITfpSupplierService 
{
    /**
     * 查询供应商信息
     * 
     * @param id 供应商信息主键
     * @return 供应商信息
     */
    public TfpSupplier selectTfpSupplierById(Long id);

    /**
     * 查询供应商信息列表
     * 
     * @param tfpSupplier 供应商信息
     * @return 供应商信息集合
     */
    public List<TfpSupplier> selectTfpSupplierList(TfpSupplier tfpSupplier);

    /**
     * 新增供应商信息
     * 
     * @param tfpSupplier 供应商信息
     * @return 结果
     */
    public int insertTfpSupplier(TfpSupplier tfpSupplier);

    /**
     * 修改供应商信息
     * 
     * @param tfpSupplier 供应商信息
     * @return 结果
     */
    public int updateTfpSupplier(TfpSupplier tfpSupplier);

    /**
     * 批量删除供应商信息
     * 
     * @param ids 需要删除的供应商信息主键集合
     * @return 结果
     */
    public int deleteTfpSupplierByIds(Long[] ids);

    /**
     * 删除供应商信息信息
     * 
     * @param id 供应商信息主键
     * @return 结果
     */
    public int deleteTfpSupplierById(Long id);

    List<TfpSupplier> selectSupplerInfoBySupplierIdList(List<Long> supplierIdList);

    List<SupplierVo> handleSupplierVo(List<Long> supplierIdList);

    List<SupplierVo> supplierSelect();
}
