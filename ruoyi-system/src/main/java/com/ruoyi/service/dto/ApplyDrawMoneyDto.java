package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(value = "ApplyDrawMoneyDto")
public class ApplyDrawMoneyDto {
    @ApiModelProperty("申请提现类型 0-非全部提现 1-全部提现")
    private Integer applyDrawMoneyType;
    @ApiModelProperty("提现价格")
    private BigDecimal drawMoneyPrice;
}
