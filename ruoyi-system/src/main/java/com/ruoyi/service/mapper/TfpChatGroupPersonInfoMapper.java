package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.TfpChatGroupPersonInfo;
import com.ruoyi.service.dto.TfpChatGroupPersonInfoDto;
import com.ruoyi.service.vo.TfpChatGroupPersonInfoVo;

/**
 * 聊天群组成员聊天信息Mapper接口
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public interface TfpChatGroupPersonInfoMapper 
{
    /**
     * 查询聊天群组成员聊天信息
     * 
     * @param id 聊天群组成员聊天信息主键
     * @return 聊天群组成员聊天信息
     */
    public TfpChatGroupPersonInfo selectTfpChatGroupPersonInfoById(Long id);

    /**
     * 查询聊天群组成员聊天信息列表
     * 
     * @param tfpChatGroupPersonInfo 聊天群组成员聊天信息
     * @return 聊天群组成员聊天信息集合
     */
    public List<TfpChatGroupPersonInfo> selectTfpChatGroupPersonInfoList(TfpChatGroupPersonInfo tfpChatGroupPersonInfo);

    /**
     * 新增聊天群组成员聊天信息
     * 
     * @param tfpChatGroupPersonInfo 聊天群组成员聊天信息
     * @return 结果
     */
    public int insertTfpChatGroupPersonInfo(TfpChatGroupPersonInfo tfpChatGroupPersonInfo);

    /**
     * 修改聊天群组成员聊天信息
     * 
     * @param tfpChatGroupPersonInfo 聊天群组成员聊天信息
     * @return 结果
     */
    public int updateTfpChatGroupPersonInfo(TfpChatGroupPersonInfo tfpChatGroupPersonInfo);

    /**
     * 删除聊天群组成员聊天信息
     * 
     * @param id 聊天群组成员聊天信息主键
     * @return 结果
     */
    public int deleteTfpChatGroupPersonInfoById(Long id);

    /**
     * 批量删除聊天群组成员聊天信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTfpChatGroupPersonInfoByIds(Long[] ids);

    List<Long> getChatGroupPersonUserIdList();

    List<TfpChatGroupPersonInfoVo> selectTfpChatGroupPersonInfoVoList(TfpChatGroupPersonInfoDto tfpChatGroupPersonInfoDto);
    List<TfpChatGroupPersonInfoVo> selectAppletInfoList(TfpChatGroupPersonInfoDto tfpChatGroupPersonInfoDto);
}
