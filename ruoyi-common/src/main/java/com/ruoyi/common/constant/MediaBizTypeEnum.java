package com.ruoyi.common.constant;


import java.util.HashMap;
import java.util.Map;

/**
 * Created by yangguanyu on 2021/4/28.
 */
public enum MediaBizTypeEnum {
    PRODUCT(1000, "商品"),

    ;

    private int id;
    private String description;

    private static Map<Integer, MediaBizTypeEnum> MAP = new HashMap<>();

    static {
        for (MediaBizTypeEnum status : MediaBizTypeEnum.values()) {
            MAP.put(status.getId(), status);
        }
    }

    MediaBizTypeEnum(int id, String description) {
        this.id = id;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public static MediaBizTypeEnum getStatus(int status) {
        return MAP.get(status);
    }
}
