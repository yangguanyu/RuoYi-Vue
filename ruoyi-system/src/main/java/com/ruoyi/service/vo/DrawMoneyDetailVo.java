package com.ruoyi.service.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
@ApiModel(value = "DrawMoneyDetailVo")
public class DrawMoneyDetailVo {
    @ApiModelProperty("提现id")
    private Long id;
    @ApiModelProperty("提现编号")
    private String drawMoneyNumber;
    @ApiModelProperty("提现发起时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    @ApiModelProperty("提款金额")
    private BigDecimal drawMoneyPrice;
    @ApiModelProperty("状态 0-提交申请/审核中 1-提现完成 2-提现失败/审核不通过")
    private Integer status;
    @ApiModelProperty("操作人")
    private String realName;
    @ApiModelProperty("提现到卡")
    private DrawMoneyBankCardInfoVo drawMoneyBankCardInfo;

    @ApiModelProperty("支付凭证")
    private List<String> urlList;
    @ApiModelProperty("审核失败原因")
    private String reason;
}
