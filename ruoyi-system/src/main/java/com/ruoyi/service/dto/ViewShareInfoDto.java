package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "ViewShareInfoDto")
public class ViewShareInfoDto {
    @ApiModelProperty("收益来源类型 0-商品 1-搭子")
    private Integer sourceType;
    @ApiModelProperty("收益来源id")
    private Long sourceId;
    @ApiModelProperty("分享用户id")
    private Long shareUserId;
}
