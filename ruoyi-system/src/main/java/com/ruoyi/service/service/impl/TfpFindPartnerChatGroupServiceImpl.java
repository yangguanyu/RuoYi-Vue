package com.ruoyi.service.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enuma.ChatUserTypeEnum;
import com.ruoyi.common.enuma.DeleteEnum;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.DcListUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.service.domain.TfpFindPartner;
import com.ruoyi.service.domain.TfpFindPartnerChatGroupPerson;
import com.ruoyi.service.domain.TfpFindPartnerChatGroupPersonInfo;
import com.ruoyi.service.dto.TfpFindPartnerChatGroupDto;
import com.ruoyi.service.dto.TfpFindPartnerDto;
import com.ruoyi.service.service.ITfpFindPartnerChatGroupPersonInfoService;
import com.ruoyi.service.service.ITfpFindPartnerChatGroupPersonService;
import com.ruoyi.service.service.ITfpFindPartnerService;
import com.ruoyi.service.vo.FindPartnerDetailVo;
import com.ruoyi.service.vo.TfpFindPartnerChatGroupVo;
import com.ruoyi.service.vo.UserVo;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.TfpFindPartnerChatGroupMapper;
import com.ruoyi.service.domain.TfpFindPartnerChatGroup;
import com.ruoyi.service.service.ITfpFindPartnerChatGroupService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 找搭子聊天群组Service业务层处理
 *
 * @author ruoyi
 * @date 2023-10-31
 */
@Service
@Transactional
public class TfpFindPartnerChatGroupServiceImpl implements ITfpFindPartnerChatGroupService
{
    @Autowired
    private TfpFindPartnerChatGroupMapper tfpFindPartnerChatGroupMapper;
    @Autowired
    private ITfpFindPartnerService tfpFindPartnerService;
    @Autowired
    private ITfpFindPartnerChatGroupPersonService findPartnerChatGroupPersonService;
    @Autowired
    private ISysUserService userService;
    @Autowired
    private ITfpFindPartnerChatGroupPersonInfoService findPartnerChatGroupPersonInfoService;

    /**
     * 查询找搭子聊天群组
     *
     * @param id 找搭子聊天群组主键
     * @return 找搭子聊天群组
     */
    @Override
    public TfpFindPartnerChatGroup selectTfpFindPartnerChatGroupById(Long id)
    {
        return tfpFindPartnerChatGroupMapper.selectTfpFindPartnerChatGroupById(id);
    }

    @Override
    public List<TfpFindPartnerChatGroupVo> selectTfpFindPartnerChatGroupVoList(TfpFindPartnerChatGroupDto tfpFindPartnerChatGroupDto) {
        String findPartnerTitle = tfpFindPartnerChatGroupDto.getFindPartnerTitle();
        if (findPartnerTitle != null && !findPartnerTitle.trim().equals("")){
            tfpFindPartnerChatGroupDto.setFindPartnerTitleFlag(true);

            TfpFindPartnerDto tfpFindPartnerDto = new TfpFindPartnerDto();
            tfpFindPartnerDto.setTitle(findPartnerTitle);
            List<TfpFindPartner> tfpFindPartnerList = tfpFindPartnerService.selectTfpFindPartnerList(tfpFindPartnerDto);
            if (tfpFindPartnerList == null || tfpFindPartnerList.size() == 0){
                return new ArrayList<>();
            }

            List<Long> findPartnerIdList = tfpFindPartnerList.stream().map(TfpFindPartner::getId).collect(Collectors.toList());
            tfpFindPartnerChatGroupDto.setFindPartnerIdList(findPartnerIdList);
        }

        List<TfpFindPartnerChatGroupVo> findPartnerChatGroupVoList = tfpFindPartnerChatGroupMapper.selectTfpFindPartnerChatGroupVoList(tfpFindPartnerChatGroupDto);
        if (findPartnerChatGroupVoList != null && findPartnerChatGroupVoList.size() > 0){
            List<Long> findPartnerIdList = findPartnerChatGroupVoList.stream().map(TfpFindPartnerChatGroupVo::getFindPartnerId).collect(Collectors.toList());

            Map<Long, String> tfpFindPartnerMap = new HashMap<>();
            List<TfpFindPartner> tfpFindPartnerList = tfpFindPartnerService.selectTfpFindPartnerListByIdList(findPartnerIdList);
            if (tfpFindPartnerList != null && tfpFindPartnerList.size() > 0){
                tfpFindPartnerMap = tfpFindPartnerList.stream().collect(Collectors.toMap(TfpFindPartner::getId, TfpFindPartner::getTitle));
            }

            for (TfpFindPartnerChatGroupVo findPartnerChatGroupVo : findPartnerChatGroupVoList) {
                findPartnerChatGroupVo.setFindPartnerTitle(tfpFindPartnerMap.get(findPartnerChatGroupVo.getFindPartnerId()));
            }
        }

        return findPartnerChatGroupVoList;
    }

    /**
     * 查询找搭子聊天群组列表
     *
     * @param tfpFindPartnerChatGroup 找搭子聊天群组
     * @return 找搭子聊天群组
     */
    @Override
    public List<TfpFindPartnerChatGroup> selectTfpFindPartnerChatGroupList(TfpFindPartnerChatGroup tfpFindPartnerChatGroup)
    {
        return tfpFindPartnerChatGroupMapper.selectTfpFindPartnerChatGroupList(tfpFindPartnerChatGroup);
    }

    /**
     * 新增找搭子聊天群组
     *
     * @param tfpFindPartnerChatGroup 找搭子聊天群组
     * @return 结果
     */
    @Override
    public int insertTfpFindPartnerChatGroup(TfpFindPartnerChatGroup tfpFindPartnerChatGroup)
    {
        tfpFindPartnerChatGroup.setCreateTime(DateUtils.getNowDate());
        return tfpFindPartnerChatGroupMapper.insertTfpFindPartnerChatGroup(tfpFindPartnerChatGroup);
    }

    /**
     * 修改找搭子聊天群组
     *
     * @param tfpFindPartnerChatGroup 找搭子聊天群组
     * @return 结果
     */
    @Override
    public int updateTfpFindPartnerChatGroup(TfpFindPartnerChatGroup tfpFindPartnerChatGroup)
    {
        tfpFindPartnerChatGroup.setUpdateTime(DateUtils.getNowDate());
        return tfpFindPartnerChatGroupMapper.updateTfpFindPartnerChatGroup(tfpFindPartnerChatGroup);
    }

    /**
     * 批量删除找搭子聊天群组
     *
     * @param ids 需要删除的找搭子聊天群组主键
     * @return 结果
     */
    @Override
    public int deleteTfpFindPartnerChatGroupByIds(Long[] ids)
    {
        return tfpFindPartnerChatGroupMapper.deleteTfpFindPartnerChatGroupByIds(ids);
    }

    /**
     * 删除找搭子聊天群组信息
     *
     * @param id 找搭子聊天群组主键
     * @return 结果
     */
    @Override
    public int deleteTfpFindPartnerChatGroupById(Long id)
    {
        return tfpFindPartnerChatGroupMapper.deleteTfpFindPartnerChatGroupById(id);
    }

    @Override
    public List<TfpFindPartnerChatGroup> selectTfpFindPartnerChatGroupListByIdList(List<Long> idList) {
        return tfpFindPartnerChatGroupMapper.selectTfpFindPartnerChatGroupListByIdList(idList);
    }

    @Override
    public TfpFindPartnerChatGroup selectByFindPartnerId(Long findPartnerId) {
        return tfpFindPartnerChatGroupMapper.selectByFindPartnerId(findPartnerId);
    }

    @Override
    public FindPartnerDetailVo detail(Long id) {
        FindPartnerDetailVo findPartnerDetailVo = new FindPartnerDetailVo();

        TfpFindPartnerChatGroup tfpFindPartnerChatGroup = tfpFindPartnerChatGroupMapper.selectTfpFindPartnerChatGroupById(id);
        if (tfpFindPartnerChatGroup != null){
            findPartnerDetailVo.setFindPartnerChatGroupId(tfpFindPartnerChatGroup.getId());
            findPartnerDetailVo.setFindPartnerChatGroupName(tfpFindPartnerChatGroup.getName());

            List<TfpFindPartnerChatGroupPerson> findPartnerChatGroupPersonList = findPartnerChatGroupPersonService.selectListByFindPartnerChatGroupId(id);
            if (DcListUtils.isNotEmpty(findPartnerChatGroupPersonList)){
                List<Long> userIdList = findPartnerChatGroupPersonList.stream().map(TfpFindPartnerChatGroupPerson::getUserId).collect(Collectors.toList());
                List<UserVo> userVoList = userService.handleUserVo(userIdList);
                findPartnerDetailVo.setUserList(userVoList);
            }
        }

        return findPartnerDetailVo;
    }

    @Override
    public List<TfpFindPartnerChatGroup> selectTfpFindPartnerChatGroupListByFindPartnerIdList(List<Long> findPartnerIdList) {
        return tfpFindPartnerChatGroupMapper.selectTfpFindPartnerChatGroupListByFindPartnerIdList(findPartnerIdList);
    }

    @Override
    public void saveFindPartnerChatGroup(TfpFindPartner tfpFindPartner) {
        LoginUser loginUser = SecurityUtils.getLoginUser();


        TfpFindPartnerChatGroup tfpFindPartnerChatGroup = new TfpFindPartnerChatGroup();
        tfpFindPartnerChatGroup.setName(tfpFindPartner.getTitle()+"聊天室");
        tfpFindPartnerChatGroup.setFindPartnerId(tfpFindPartner.getId());
        tfpFindPartnerChatGroup.setDelFlag(DeleteEnum.EXIST.getValue());
        tfpFindPartnerChatGroup.setCreateUserId(loginUser.getUserId());
        tfpFindPartnerChatGroup.setCreateBy(loginUser.getUsername());
        tfpFindPartnerChatGroup.setCreateTime(DateUtils.getNowDate());
        tfpFindPartnerChatGroupMapper.insertTfpFindPartnerChatGroup(tfpFindPartnerChatGroup);

        TfpFindPartnerChatGroupPerson tfpFindPartnerChatGroupPerson = new TfpFindPartnerChatGroupPerson();
        tfpFindPartnerChatGroupPerson.setChatGroupId(tfpFindPartnerChatGroup.getId());
        tfpFindPartnerChatGroupPerson.setUserId(tfpFindPartner.getCreateUserId());
        tfpFindPartnerChatGroupPerson.setUserType(ChatUserTypeEnum.BOOT_MAN.getValue());
        tfpFindPartnerChatGroupPerson.setDelFlag(DeleteEnum.EXIST.getValue());
        tfpFindPartnerChatGroupPerson.setCreateUserId(loginUser.getUserId());
        tfpFindPartnerChatGroupPerson.setCreateBy(loginUser.getUsername());
        tfpFindPartnerChatGroupPerson.setCreateTime(DateUtils.getNowDate());
        findPartnerChatGroupPersonService.insertTfpFindPartnerChatGroupPerson(tfpFindPartnerChatGroupPerson);

        TfpFindPartnerChatGroupPersonInfo tfpFindPartnerChatGroupPersonInfo = new TfpFindPartnerChatGroupPersonInfo();
        tfpFindPartnerChatGroupPersonInfo.setChatGroupId(tfpFindPartnerChatGroup.getId());
        tfpFindPartnerChatGroupPersonInfo.setChatGroupPersonUserId(tfpFindPartner.getCreateUserId());
        tfpFindPartnerChatGroupPersonInfo.setChatGroupPersonId(tfpFindPartnerChatGroupPerson.getId());
        tfpFindPartnerChatGroupPersonInfo.setChatInfo("我正在找同频搭子，关于活动细节可以在这里向我询问呦！");
        tfpFindPartnerChatGroupPersonInfo.setDelFlag(DeleteEnum.EXIST.getValue());
        tfpFindPartnerChatGroupPersonInfo.setCreateUserId(loginUser.getUserId());
        tfpFindPartnerChatGroupPersonInfo.setCreateBy(loginUser.getUsername());
        tfpFindPartnerChatGroupPersonInfo.setCreateTime(DateUtils.getNowDate());
        findPartnerChatGroupPersonInfoService.insertTfpFindPartnerChatGroupPersonInfo(tfpFindPartnerChatGroupPersonInfo);
    }

    @Override
    public void saveFindPartnerChatGroupTimeTask(TfpFindPartner tfpFindPartner) {


        TfpFindPartnerChatGroup tfpFindPartnerChatGroup = new TfpFindPartnerChatGroup();
        tfpFindPartnerChatGroup.setName(tfpFindPartner.getTitle()+"聊天室");
        tfpFindPartnerChatGroup.setFindPartnerId(tfpFindPartner.getId());
        tfpFindPartnerChatGroup.setDelFlag(DeleteEnum.EXIST.getValue());
        tfpFindPartnerChatGroup.setCreateUserId(1L);
        tfpFindPartnerChatGroup.setCreateBy("管理员");
        tfpFindPartnerChatGroup.setCreateTime(DateUtils.getNowDate());
        tfpFindPartnerChatGroupMapper.insertTfpFindPartnerChatGroup(tfpFindPartnerChatGroup);

        TfpFindPartnerChatGroupPerson tfpFindPartnerChatGroupPerson = new TfpFindPartnerChatGroupPerson();
        tfpFindPartnerChatGroupPerson.setChatGroupId(tfpFindPartnerChatGroup.getId());
        tfpFindPartnerChatGroupPerson.setUserId(tfpFindPartner.getCreateUserId());
        tfpFindPartnerChatGroupPerson.setUserType(ChatUserTypeEnum.BOOT_MAN.getValue());
        tfpFindPartnerChatGroupPerson.setDelFlag(DeleteEnum.EXIST.getValue());
        tfpFindPartnerChatGroupPerson.setCreateUserId(1L);
        tfpFindPartnerChatGroupPerson.setCreateBy("管理员");
        tfpFindPartnerChatGroupPerson.setCreateTime(DateUtils.getNowDate());
        findPartnerChatGroupPersonService.insertTfpFindPartnerChatGroupPerson(tfpFindPartnerChatGroupPerson);
    }
}
