package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "MyOrderListDto")
public class MyOrderListDto {
    @ApiModelProperty("查询状态 0-待支付 1-已完成 2-待确认(已支付) 3-待出行 45-退款售后")
    private Integer status;
}
