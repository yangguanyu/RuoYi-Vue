package com.ruoyi.service.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 商品辅对象 tfp_product_sub
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
public class TfpProductSub extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 主表id */
    @Excel(name = "主表id")
    private Long productId;

    /** 线路亮点 */
    @Excel(name = "线路亮点")
    private String routeHighlight;

    /** 儿童标准说明 */
    @Excel(name = "儿童标准说明")
    private String ageIntro;

    /** 交通说明 */
    @Excel(name = "交通说明")
    private String trafficInstructions;

    /** 保险具体内容 */
    @Excel(name = "保险具体内容")
    private String insuranceContent;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 创建者id */
    @Excel(name = "创建者id")
    private Long createUserId;

    /** 更新者id */
    @Excel(name = "更新者id")
    private Long updateUserId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setProductId(Long productId) 
    {
        this.productId = productId;
    }

    public Long getProductId() 
    {
        return productId;
    }
    public void setRouteHighlight(String routeHighlight) 
    {
        this.routeHighlight = routeHighlight;
    }

    public String getRouteHighlight() 
    {
        return routeHighlight;
    }
    public void setAgeIntro(String ageIntro) 
    {
        this.ageIntro = ageIntro;
    }

    public String getAgeIntro() 
    {
        return ageIntro;
    }
    public void setTrafficInstructions(String trafficInstructions) 
    {
        this.trafficInstructions = trafficInstructions;
    }

    public String getTrafficInstructions() 
    {
        return trafficInstructions;
    }
    public void setInsuranceContent(String insuranceContent) 
    {
        this.insuranceContent = insuranceContent;
    }

    public String getInsuranceContent() 
    {
        return insuranceContent;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }
    public void setUpdateUserId(Long updateUserId) 
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId() 
    {
        return updateUserId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("productId", getProductId())
            .append("routeHighlight", getRouteHighlight())
            .append("ageIntro", getAgeIntro())
            .append("trafficInstructions", getTrafficInstructions())
            .append("remark", getRemark())
            .append("insuranceContent", getInsuranceContent())
            .append("delFlag", getDelFlag())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
