package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
@ApiModel(value = "CreateOrderDto", description = "创建订单dto")
public class CreateOrderDto {
    @ApiModelProperty("商品id")
    private Long productId;
    @ApiModelProperty("售卖单价")
    private BigDecimal salePrice;
    @ApiModelProperty("商品数量")
    private Integer quantities;
    @ApiModelProperty("聊天群id")
    private Long chatGroupId;
    @ApiModelProperty("出行方式 0-组队出行 1-独立出行")
    private Integer travelType;

    @ApiModelProperty("订单金额")
    private BigDecimal finalAmount;
    @ApiModelProperty("出发时间")
    private Date goDate;

    @ApiModelProperty("出发地名称")
    private String goPlaceName;
    @ApiModelProperty("出行人id 仅独立出行人传参")
    private List<Long> mateIdList;

    @ApiModelProperty("分享用户id")
    private Long shareUserId;

    @ApiModelProperty("是否选择低价机票 0-否 1-是")
    private Integer lowPriceAirTicketFlag;

    @ApiModelProperty("规则id")
    private Long ruleId;
}
