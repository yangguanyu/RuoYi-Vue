package com.ruoyi.web.controller.service;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.TfpSelfFundedProject;
import com.ruoyi.service.service.ITfpSelfFundedProjectService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 自费项目Controller
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@RestController
@RequestMapping("/service/selfFundedProject")
public class TfpSelfFundedProjectController extends BaseController
{
    @Autowired
    private ITfpSelfFundedProjectService tfpSelfFundedProjectService;

    /**
     * 查询自费项目列表
     */
    @PreAuthorize("@ss.hasPermi('service:selfFundedProject:list')")
    @GetMapping("/list")
    public TableDataInfo list(TfpSelfFundedProject tfpSelfFundedProject)
    {
        startPage();
        List<TfpSelfFundedProject> list = tfpSelfFundedProjectService.selectTfpSelfFundedProjectList(tfpSelfFundedProject);
        return getDataTable(list);
    }

    /**
     * 导出自费项目列表
     */
    @PreAuthorize("@ss.hasPermi('service:selfFundedProject:export')")
    @Log(title = "自费项目", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TfpSelfFundedProject tfpSelfFundedProject)
    {
        List<TfpSelfFundedProject> list = tfpSelfFundedProjectService.selectTfpSelfFundedProjectList(tfpSelfFundedProject);
        ExcelUtil<TfpSelfFundedProject> util = new ExcelUtil<TfpSelfFundedProject>(TfpSelfFundedProject.class);
        util.exportExcel(response, list, "自费项目数据");
    }

    /**
     * 获取自费项目详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:selfFundedProject:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tfpSelfFundedProjectService.selectTfpSelfFundedProjectById(id));
    }

    /**
     * 新增自费项目
     */
    @PreAuthorize("@ss.hasPermi('service:selfFundedProject:add')")
    @Log(title = "自费项目", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TfpSelfFundedProject tfpSelfFundedProject)
    {
        return toAjax(tfpSelfFundedProjectService.insertTfpSelfFundedProject(tfpSelfFundedProject));
    }

    /**
     * 修改自费项目
     */
    @PreAuthorize("@ss.hasPermi('service:selfFundedProject:edit')")
    @Log(title = "自费项目", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TfpSelfFundedProject tfpSelfFundedProject)
    {
        return toAjax(tfpSelfFundedProjectService.updateTfpSelfFundedProject(tfpSelfFundedProject));
    }

    /**
     * 删除自费项目
     */
    @PreAuthorize("@ss.hasPermi('service:selfFundedProject:remove')")
    @Log(title = "自费项目", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tfpSelfFundedProjectService.deleteTfpSelfFundedProjectByIds(ids));
    }
}
