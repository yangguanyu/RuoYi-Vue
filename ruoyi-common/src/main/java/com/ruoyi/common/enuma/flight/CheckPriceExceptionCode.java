package com.ruoyi.common.enuma.flight;

import com.ruoyi.common.enuma.BaseStrEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CheckPriceExceptionCode implements BaseStrEnum {
    PRICE_CHANGE("011402", "价格变动"),
    NO_POSITION("011401", "没有仓位");

    private String value;
    private String desc;


}
