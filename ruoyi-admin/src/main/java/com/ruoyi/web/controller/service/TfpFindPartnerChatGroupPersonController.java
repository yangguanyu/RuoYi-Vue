package com.ruoyi.web.controller.service;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.service.dto.TfpFindPartnerChatGroupPersonDto;
import com.ruoyi.service.vo.TfpFindPartnerChatGroupPersonVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.TfpFindPartnerChatGroupPerson;
import com.ruoyi.service.service.ITfpFindPartnerChatGroupPersonService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 找搭子聊天群组成员Controller
 * 
 * @author ruoyi
 * @date 2023-10-31
 */
@RestController
@RequestMapping("/service/findPartnerChatGroupPerson")
public class TfpFindPartnerChatGroupPersonController extends BaseController
{
    @Autowired
    private ITfpFindPartnerChatGroupPersonService tfpFindPartnerChatGroupPersonService;

    /**
     * 查询找搭子聊天群组成员列表
     */
    @PreAuthorize("@ss.hasPermi('service:findPartnerChatGroupPerson:list')")
    @GetMapping("/list")
    public TableDataInfo list(TfpFindPartnerChatGroupPersonDto tfpFindPartnerChatGroupPersonDto)
    {
        startPage();
        List<TfpFindPartnerChatGroupPersonVo> list = tfpFindPartnerChatGroupPersonService.selectTfpFindPartnerChatGroupPersonVoList(tfpFindPartnerChatGroupPersonDto);
        return getDataTable(list);
    }

    /**
     * 导出找搭子聊天群组成员列表
     */
    @PreAuthorize("@ss.hasPermi('service:findPartnerChatGroupPerson:export')")
    @Log(title = "找搭子聊天群组成员", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TfpFindPartnerChatGroupPerson tfpFindPartnerChatGroupPerson)
    {
        List<TfpFindPartnerChatGroupPerson> list = tfpFindPartnerChatGroupPersonService.selectTfpFindPartnerChatGroupPersonList(tfpFindPartnerChatGroupPerson);
        ExcelUtil<TfpFindPartnerChatGroupPerson> util = new ExcelUtil<TfpFindPartnerChatGroupPerson>(TfpFindPartnerChatGroupPerson.class);
        util.exportExcel(response, list, "找搭子聊天群组成员数据");
    }

    /**
     * 获取找搭子聊天群组成员详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:findPartnerChatGroupPerson:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tfpFindPartnerChatGroupPersonService.selectTfpFindPartnerChatGroupPersonById(id));
    }

    /**
     * 新增找搭子聊天群组成员
     */
    @PreAuthorize("@ss.hasPermi('service:findPartnerChatGroupPerson:add')")
    @Log(title = "找搭子聊天群组成员", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TfpFindPartnerChatGroupPerson tfpFindPartnerChatGroupPerson)
    {
        return toAjax(tfpFindPartnerChatGroupPersonService.insertTfpFindPartnerChatGroupPerson(tfpFindPartnerChatGroupPerson));
    }

    /**
     * 修改找搭子聊天群组成员
     */
    @PreAuthorize("@ss.hasPermi('service:findPartnerChatGroupPerson:edit')")
    @Log(title = "找搭子聊天群组成员", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TfpFindPartnerChatGroupPerson tfpFindPartnerChatGroupPerson)
    {
        return toAjax(tfpFindPartnerChatGroupPersonService.updateTfpFindPartnerChatGroupPerson(tfpFindPartnerChatGroupPerson));
    }

    /**
     * 删除找搭子聊天群组成员
     */
    @PreAuthorize("@ss.hasPermi('service:findPartnerChatGroupPerson:remove')")
    @Log(title = "找搭子聊天群组成员", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tfpFindPartnerChatGroupPersonService.deleteTfpFindPartnerChatGroupPersonByIds(ids));
    }
}
