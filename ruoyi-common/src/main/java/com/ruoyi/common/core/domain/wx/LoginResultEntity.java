package com.ruoyi.common.core.domain.wx;

import lombok.Data;

import java.util.List;

/**
 * 统一登录返回结果
 *
 * @author soonphe
 * @since 1.0
 */
@Data
public class LoginResultEntity {
    /**
     * 登录是否成功
     */
    private boolean success;
    /**
     * 成功返回 token
     */
    private String token;
    /**
     * 错误信息
     */
    private String message;
    /**
     * 是否中断后续登录流程
     */
    private boolean interrupted;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 租户权限拒绝
     * 当此属性为true时，代表当前用户还没有登陆到该租户的权限
     */
    private boolean tenantPermissionDeny = false;
    /**
     * 是否是首次登录
     */
    private Boolean isFirstLogin = false;
    /**
     * 用户统一标识
     */
    private String unionid;

    //是否包含供应商信息
    private Boolean supplierFlag = false;

    private List<UserSupplierIdentiyEntity> userSupplierIdentiy;
}
