package com.ruoyi.service.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 商品、合作分公司中间对象 tfp_product_cbc
 * 
 * @author ruoyi
 * @date 2023-12-12
 */
public class TfpProductCbc extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 商品id */
    @Excel(name = "商品id")
    private Long productId;

    /** 合作分公司id */
    @Excel(name = "合作分公司id")
    private Long cbcId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setProductId(Long productId) 
    {
        this.productId = productId;
    }

    public Long getProductId() 
    {
        return productId;
    }
    public void setCbcId(Long cbcId) 
    {
        this.cbcId = cbcId;
    }

    public Long getCbcId() 
    {
        return cbcId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("productId", getProductId())
            .append("cbcId", getCbcId())
            .toString();
    }
}
