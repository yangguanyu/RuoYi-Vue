package com.ruoyi.service.domain.flight.res.orderdetail;

import lombok.Data;

import java.util.List;

@Data
public class SubOrderResp {
    // 子订单号
    private String subOrderNo;

    // 订单创建时间 (格式: YYYY-MM-DD HH:MM:SS)
    private String createTime;

    // 编码 (支付后占座)
    private String pnrNo;

    // 原编码
    private String oldPnrNo;

    // 编码信息
    private String patTxt;

    // 乘客信息，类型为List<Passenger>
    private List<PassengerResp> passengers;

    // 运价信息，类型为List<PriceData>
    private List<PriceDataResp> priceData;

    // 子订单支付金额
    private Double subTotalPay;

    // 子订单状态
    private Integer status;
}
