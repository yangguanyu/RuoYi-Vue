package com.ruoyi.service.service;

import java.util.List;
import com.ruoyi.service.domain.TfpFindPartnerFocus;

/**
 * 找搭子关注Service接口
 * 
 * @author yangguanyu
 * @date 2024-01-12
 */
public interface ITfpFindPartnerFocusService 
{
    /**
     * 查询找搭子关注
     * 
     * @param id 找搭子关注主键
     * @return 找搭子关注
     */
    public TfpFindPartnerFocus selectTfpFindPartnerFocusById(Long id);

    /**
     * 查询找搭子关注列表
     * 
     * @param tfpFindPartnerFocus 找搭子关注
     * @return 找搭子关注集合
     */
    public List<TfpFindPartnerFocus> selectTfpFindPartnerFocusList(TfpFindPartnerFocus tfpFindPartnerFocus);

    /**
     * 新增找搭子关注
     * 
     * @param tfpFindPartnerFocus 找搭子关注
     * @return 结果
     */
    public int insertTfpFindPartnerFocus(TfpFindPartnerFocus tfpFindPartnerFocus);

    /**
     * 修改找搭子关注
     * 
     * @param tfpFindPartnerFocus 找搭子关注
     * @return 结果
     */
    public int updateTfpFindPartnerFocus(TfpFindPartnerFocus tfpFindPartnerFocus);

    /**
     * 批量删除找搭子关注
     * 
     * @param ids 需要删除的找搭子关注主键集合
     * @return 结果
     */
    public int deleteTfpFindPartnerFocusByIds(Long[] ids);

    /**
     * 删除找搭子关注信息
     * 
     * @param id 找搭子关注主键
     * @return 结果
     */
    public int deleteTfpFindPartnerFocusById(Long id);
}
