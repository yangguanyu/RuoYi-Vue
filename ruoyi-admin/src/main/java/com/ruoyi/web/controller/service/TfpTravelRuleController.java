package com.ruoyi.web.controller.service;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.service.dto.ExportDto;
import com.ruoyi.service.dto.ListTravelRuleDto;
import com.ruoyi.service.dto.TfpTravelRuleDto;
import com.ruoyi.service.excel.ListTravelRuleExcel;
import com.ruoyi.service.excel.OrderListExportExcel;
import com.ruoyi.service.vo.*;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.service.domain.TfpTravelRule;
import com.ruoyi.service.service.ITfpTravelRuleService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 旅行规则Controller
 * 
 * @author ruoyi
 * @date 2024-05-22
 */
@RestController
@RequestMapping("/service/travelRule")
public class TfpTravelRuleController extends BaseController
{
    @Autowired
    private ITfpTravelRuleService tfpTravelRuleService;

    /**
     * 查询旅行规则列表
     */
    @PreAuthorize("@ss.hasPermi('service:travelRule:list')")
    @GetMapping("/list")
    public TableDataInfo list(TfpTravelRule tfpTravelRule)
    {
        startPage();
        List<TfpTravelRule> list = tfpTravelRuleService.selectTfpTravelRuleList(tfpTravelRule);
        return getDataTable(list);
    }

    /**
     * 导出旅行规则列表
     */
    @PreAuthorize("@ss.hasPermi('service:travelRule:export')")
    @Log(title = "旅行规则", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TfpTravelRule tfpTravelRule)
    {
        List<TfpTravelRule> list = tfpTravelRuleService.selectTfpTravelRuleList(tfpTravelRule);
        ExcelUtil<TfpTravelRule> util = new ExcelUtil<TfpTravelRule>(TfpTravelRule.class);
        util.exportExcel(response, list, "旅行规则数据");
    }

    /**
     * 获取旅行规则详细信息
     */
    @PreAuthorize("@ss.hasPermi('service:travelRule:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tfpTravelRuleService.selectTfpTravelRuleById(id));
    }

    /**
     * 新增旅行规则
     */
    @PreAuthorize("@ss.hasPermi('service:travelRule:add')")
    @Log(title = "旅行规则", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TfpTravelRule tfpTravelRule)
    {
        return toAjax(tfpTravelRuleService.insertTfpTravelRule(tfpTravelRule));
    }

    /**
     * 修改旅行规则
     */
    @PreAuthorize("@ss.hasPermi('service:travelRule:edit')")
    @Log(title = "旅行规则", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TfpTravelRule tfpTravelRule)
    {
        return toAjax(tfpTravelRuleService.updateTfpTravelRule(tfpTravelRule));
    }

    /**
     * 删除旅行规则
     */
    @PreAuthorize("@ss.hasPermi('service:travelRule:remove')")
    @Log(title = "旅行规则", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tfpTravelRuleService.deleteTfpTravelRuleByIds(ids));
    }

    @ApiOperation("后台-规则配置列表")
    @GetMapping("/listTravelRule")
    public TableDataInfo<ListTravelRuleVo> listTravelRule(ListTravelRuleDto listTravelRuleDto){
        return getDataTable(tfpTravelRuleService.listTravelRule(listTravelRuleDto));
    }

    @ApiOperation("后台-保存规则配置")
    @PostMapping("/saveTravelRule")
    @ResponseBody
    public R saveTravelRule(@Validated @RequestBody TfpTravelRuleDto tfpTravelRuleDto){
        return R.ok(tfpTravelRuleService.saveTravelRule(tfpTravelRuleDto));
    }

    @ApiOperation("后台-详情")
    @GetMapping("/detailTravelRule/{id}")
    @ResponseBody
    public R<DetailTravelRuleVo> detailTravelRule(@PathVariable("id") Long id){
        return R.ok(tfpTravelRuleService.detailTravelRule(id));
    }

    @ApiOperation("后台-规则配置列表导出")
    @PostMapping("/travelRuleListExport")
    public void travelRuleListExport(HttpServletResponse response, ExportDto exportDto)
    {
        List<ListTravelRuleExcel> list = tfpTravelRuleService.travelRuleListExport(exportDto);
        ExcelUtil<ListTravelRuleExcel> util = new ExcelUtil<ListTravelRuleExcel>(ListTravelRuleExcel.class);
        util.exportExcel(response, list, "规则配置数据");
    }


    @ApiOperation("小程序-获取规则配置信息")
    @PostMapping("/getEffectiveTravelRuleList")
    @ResponseBody
    public R<List<EffectiveTravelRuleVo>> getEffectiveTravelRuleList(@RequestParam("type") Integer type){
        return R.ok(tfpTravelRuleService.getEffectiveTravelRuleList(type));
    }
}
