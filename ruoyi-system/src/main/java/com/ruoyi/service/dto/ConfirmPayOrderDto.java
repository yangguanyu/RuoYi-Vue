package com.ruoyi.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
@ApiModel(value = "ConfirmPayOrderDto")
public class ConfirmPayOrderDto {
    @ApiModelProperty("确认业务id")
    private Long id;
    @ApiModelProperty("确认状态 1-确认 2-取消")
    private Integer confirmStatus;
    @ApiModelProperty("退款类型 1-全额退款   2-部分退款")
    private Integer refundType;
    @ApiModelProperty("退款金额 部分退款时填写")
    private BigDecimal refundAmount;
    @ApiModelProperty("凭证")
    private List<String> urlList;
    @ApiModelProperty("退款原因")
    private String reason;
}
