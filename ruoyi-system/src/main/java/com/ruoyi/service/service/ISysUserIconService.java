package com.ruoyi.service.service;

import java.util.List;
import com.ruoyi.service.domain.SysUserIcon;

/**
 * 用户图片Service接口
 * 
 * @author ruoyi
 * @date 2023-11-01
 */
public interface ISysUserIconService 
{
    /**
     * 查询用户图片
     * 
     * @param id 用户图片主键
     * @return 用户图片
     */
    public SysUserIcon selectSysUserIconById(Long id);

    /**
     * 查询用户图片列表
     * 
     * @param sysUserIcon 用户图片
     * @return 用户图片集合
     */
    public List<SysUserIcon> selectSysUserIconList(SysUserIcon sysUserIcon);

    /**
     * 新增用户图片
     * 
     * @param sysUserIcon 用户图片
     * @return 结果
     */
    public int insertSysUserIcon(SysUserIcon sysUserIcon);

    /**
     * 修改用户图片
     * 
     * @param sysUserIcon 用户图片
     * @return 结果
     */
    public int updateSysUserIcon(SysUserIcon sysUserIcon);

    /**
     * 批量删除用户图片
     * 
     * @param ids 需要删除的用户图片主键集合
     * @return 结果
     */
    public int deleteSysUserIconByIds(Long[] ids);

    /**
     * 删除用户图片信息
     * 
     * @param id 用户图片主键
     * @return 结果
     */
    public int deleteSysUserIconById(Long id);
}
