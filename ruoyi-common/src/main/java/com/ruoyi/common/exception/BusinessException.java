package com.ruoyi.common.exception;

import lombok.Data;
import org.springframework.http.HttpStatus;

/**
 * 业务异常类
 * @author
 */
@Data
public class BusinessException extends RuntimeException{
    private Integer code;
    private String desc;

    public BusinessException(Integer code, String desc) {
        super(String.format("%s|%s", code, desc));
        this.code = code;
        this.desc = desc;
    }

    public BusinessException(String desc) {
        super(String.format("%s|%s", HttpStatus.INTERNAL_SERVER_ERROR.value(), desc));
        this.desc = desc;
    }
}
