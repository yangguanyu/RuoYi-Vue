package com.ruoyi.common.enuma;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum OrderStatusEnum implements BaseEnum {

    WAIT_PAY(0, "待支付"),
    COMPLETE(1, "已完成"),
    WAIT_CONFIRMED(2, "待确认(已支付)"),
    WAIT_TRAVEL(3, "待出行"),
    APPLY_REFUND_AUDITING(4, "申请退款-审核中"),
    APPLY_REFUND_SUCCESS(5, "申请退款-退款成功"),
    EXPIRE(6, "过期"),
    CANCEL(7, "取消订单")
    ;

    private Integer value;
    private String desc;
}
