package com.ruoyi.service.domain.flight.res;

import lombok.Data;

@Data
public class CheckSeatAndPriceResp {
    // 成人价
    private SeatAndPrice atdPrice;
    // 儿童价
    private SeatAndPrice chdPrice;
    // 婴儿价，暂不支持
    private SeatAndPrice infPrice;

    @Data
    public class SeatAndPrice {
        // 票面价
        private Double price;
        // 单人机建
        private Integer airportTax;
        // 单人燃油费
        private Integer fuelTax;
        // 舱位状态
        private String seatType;
        // 单人结算价 (price * (1 - commisionPoint%) - commisionMoney) 不含税
        private Double settlement;
    }
}
