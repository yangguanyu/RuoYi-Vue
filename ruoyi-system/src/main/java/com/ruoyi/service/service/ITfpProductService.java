package com.ruoyi.service.service;

import java.math.BigDecimal;
import java.util.List;
import com.ruoyi.service.domain.TfpProduct;
import com.ruoyi.service.dto.ListProductSelectDto;
import com.ruoyi.service.dto.OperationShelivesDto;
import com.ruoyi.service.dto.SnapshotTextDto;
import com.ruoyi.service.dto.TfpProductDto;
import com.ruoyi.service.vo.*;

/**
 * 商品主Service接口
 * 
 * @author ruoyi
 * @date 2023-11-01
 */
public interface ITfpProductService 
{
    /**
     * 查询商品主
     * 
     * @param id 商品主主键
     * @return 商品主
     */
    public TfpProduct selectTfpProductById(Long id);

    /**
     * 查询商品主列表
     * 
     * @param tfpProduct 商品主
     * @return 商品主集合
     */
    public List<TfpProduct> selectTfpProductList(TfpProduct tfpProduct);

    List<TfpProductVo> selectTfpProductVoList(TfpProduct tfpProduct);

    /**
     * 新增商品主
     * 
     * @param tfpProductDto 商品主
     * @return 结果
     */
    public int insertTfpProduct(TfpProductDto tfpProductDto);

    /**
     * 修改商品主
     * 
     * @param tfpProductDto 商品主
     * @return 结果
     */
    public int updateTfpProduct(TfpProductDto tfpProductDto);

    int updateTfpProduct(TfpProduct tfpProduct);

    /**
     * 批量删除商品主
     * 
     * @param ids 需要删除的商品主主键集合
     * @return 结果
     */
    public int deleteTfpProductByIds(Long[] ids);

    /**
     * 删除商品主信息
     * 
     * @param id 商品主主键
     * @return 结果
     */
    public int deleteTfpProductById(Long id);

    List<TfpProduct> selectTfpProductListByIdList(List<Long> idList);

    TfpProductDetailVo detail(Long id);

    /**
     * 目的地商品信息列表
     * @param cityName
     * @return
     */
    List<ListProductVo> listDestinationProductSelect(String cityName);

    List<ListProductVo> handleListProductVo(ListProductSelectDto listProductSelectDto);

    /**
     * 小程序-商品详情
     * @param id
     * @return
     */
     ProductDetailVo productDetail(Long id, SnapshotTextDto snapshotTextDto);

    List<TfpProductVo> selectAppletHotRecommendList(TfpProduct tfpProduct);

    List<String> homePageCarouselChart();

    ProductSnapshotDetailVo productSnapshotDetail(Long productId, SnapshotTextDto snapshotTextDto, Integer travelType, Long orderChatGroupId);

    int operationShelives(OperationShelivesDto operationShelivesDto);

    BigDecimal mathTotalPrice(BigDecimal adultRetailPrice, BigDecimal totalAirTicketPrice);
}
