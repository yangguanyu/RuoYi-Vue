package com.ruoyi.service.vo;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 附件对象 tfp_attachment
 *
 * @author ruoyi
 * @date 2023-12-25
 */
@ApiModel("图片VO-AttachmentVO")
@Data
public class AttachmentVO
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 业务id */
    @ApiModelProperty(value = "业务id")
    private Long businessId;

    /** 业务类型 */
    @ApiModelProperty(value = "业务类型")
    private String businessType;

    /** 业务子类型 */
    @ApiModelProperty(value = "业务子类型")
    private String businessSubType;

    /** 附件名称 */
    @ApiModelProperty(value = "附件名称")
    private String name;

    /** 附件Url */
    @ApiModelProperty(value = "附件Url")
    private String url;

}
