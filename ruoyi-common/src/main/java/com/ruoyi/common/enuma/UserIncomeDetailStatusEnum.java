package com.ruoyi.common.enuma;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum UserIncomeDetailStatusEnum implements BaseEnum {
    VIEW(0, "受邀人查看"),
    SETTLEMENT_COMMISSION(1, "结算佣金/已完成"),
    PLACE_ORDER(2, "受邀人下单"),
    PAY(3, "受邀人付款"),
    TRAVEL_COMPLETE(4, "出行完成")
    ;

    private Integer value;
    private String desc;
}
