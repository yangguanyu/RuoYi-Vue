import request from '@/utils/request'

// 查询商品辅列表
export function listProductSub(query) {
  return request({
    url: '/service/productSub/list',
    method: 'get',
    params: query
  })
}

// 查询商品辅详细
export function getProductSub(id) {
  return request({
    url: '/service/productSub/' + id,
    method: 'get'
  })
}

// 新增商品辅
export function addProductSub(data) {
  return request({
    url: '/service/productSub',
    method: 'post',
    data: data
  })
}

// 修改商品辅
export function updateProductSub(data) {
  return request({
    url: '/service/productSub',
    method: 'put',
    data: data
  })
}

// 删除商品辅
export function delProductSub(id) {
  return request({
    url: '/service/productSub/' + id,
    method: 'delete'
  })
}
